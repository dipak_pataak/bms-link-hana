-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 29, 2019 at 07:19 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bmslink`
--

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_shipment_data`
--

CREATE TABLE `b_to_b_shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `b_user_name` varchar(250) DEFAULT NULL,
  `b_user_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `b_to_b_shipment_data`
--
ALTER TABLE `b_to_b_shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `b_to_b_shipment_data`
--
ALTER TABLE `b_to_b_shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
