-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 29, 2019 at 07:19 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bmslink`
--

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_qutation_details`
--

CREATE TABLE `b_to_b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `b_to_b_level_qutation_details`
--
ALTER TABLE `b_to_b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `b_to_b_level_qutation_details`
--
ALTER TABLE `b_to_b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
