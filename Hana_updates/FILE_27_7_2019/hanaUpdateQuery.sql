
/*...... ADD CREATED_BY AND UPDATED_BY IN shipping_method TABLE ......*/
ALTER TABLE `shipping_method` ADD `created_by` INT(11) NOT NULL DEFAULT '0' AFTER `mode`, ADD `updated_by` INT(11) NOT NULL DEFAULT '0' AFTER `created_by`;

 /*...... UPDATE OLD B ID IN CREATED_AT AND UPDATED_AT ......*/
 UPDATE `shipping_method` SET `created_by` = '1';
 UPDATE `shipping_method` SET `updated_by` = '1';


 /*........ ADD CREATED_BY COLUMN IN mail_config_tbl TABLE ..........*/
 ALTER TABLE `mail_config_tbl` ADD `created_by` INT(11) NOT NULL AFTER `mailtype`;

 /*...... UPDATE OLD B ID IN CREATED_BY ......*/
 UPDATE `mail_config_tbl` SET `created_by` = '1';

/*........ ADD CREATED_BY COLUMN IN sms_gateway TABLE ..........*/
 ALTER TABLE `sms_gateway` ADD `created_by` INT(11) NOT NULL AFTER `status`, ADD `updated_by` INT(11) NOT NULL AFTER `created_by`;

  /*...... UPDATE OLD B ID IN CREATED_BY AND UPDATED BY......*/
 UPDATE `sms_gateway` SET `created_by` = '1',`updated_by` = '1';


/*........ ADD CREATED_BY and UPDATED BY COLUMN IN gallery_image_tbl TABLE ..........*/
 ALTER TABLE `gallery_image_tbl` ADD `created_by` INT(11) NOT NULL DEFAULT '0' AFTER `image`, ADD `updated_by` INT(11) NOT NULL DEFAULT '0' AFTER `created_by`;

  /*...... UPDATE OLD B ID IN CREATED_BY AND UPDATED BY......*/
 UPDATE `gallery_image_tbl` SET `created_by` = '1',`updated_by` = '1';

/*........ ADD CREATED_BY and UPDATED BY COLUMN IN gallery_tag_tbl TABLE ..........*/
 ALTER TABLE `gallery_tag_tbl` ADD `created_by` INT(11) NOT NULL DEFAULT '0' AFTER `updated_date`, ADD `updated_by` INT(11) NOT NULL DEFAULT '0' AFTER `created_by`;

  /*...... UPDATE OLD B ID IN CREATED_BY AND UPDATED BY......*/
 UPDATE `gallery_tag_tbl` SET `created_by` = '1',`updated_by` = '1';

 /*......... CREATE SHAREAD CATALOG MENU ............*/
 INSERT INTO `b_menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `created_by`, `create_date`) VALUES (NULL, 'shared catalog', 'shared catalog', 'shared-catalog', 'shared catalog', '51', '0', '1', '0', NULL, '1', '1', CURRENT_TIMESTAMP);
 INSERT INTO `b_menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `created_by`, `create_date`) VALUES (NULL, 'Catalog user', 'Catalog user', 'catalog-user', 'shared-catalog', '52', '148', '1', '0', NULL, '1', '1', CURRENT_TIMESTAMP);
 UPDATE `b_menusetup_tbl` SET `page_url` = '' WHERE `b_menusetup_tbl`.`id` = 148;
 UPDATE `b_menusetup_tbl` SET `module` = 'shared_catalog' WHERE `b_menusetup_tbl`.`id` = 148;
 UPDATE `b_menusetup_tbl` SET `module` = 'shared_catalog' WHERE `b_menusetup_tbl`.`id` = 149;
 INSERT INTO `b_menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `created_by`, `create_date`) VALUES (NULL, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', '53', '148', '1', '0', NULL, '1', '1', CURRENT_TIMESTAMP);
 INSERT INTO `b_menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `created_by`, `create_date`) VALUES (NULL, 'Order list', 'Order list', 'catalog-order-list', 'shared_catalog', '54', '148', '1', '0', NULL, '1', '1', CURRENT_TIMESTAMP);