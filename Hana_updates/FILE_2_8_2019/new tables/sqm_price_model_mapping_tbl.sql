-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 02, 2019 at 06:19 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_new_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `sqm_price_model_mapping_tbl`
--

CREATE TABLE `sqm_price_model_mapping_tbl` (
  `id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_id` int(11) NOT NULL,
  `pattern_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sqm_price_model_mapping_tbl`
--

INSERT INTO `sqm_price_model_mapping_tbl` (`id`, `price`, `product_id`, `pattern_id`) VALUES
(1, 10.00, 94, '333'),
(3, 20.00, 97, '340'),
(4, 140.00, 103, '340'),
(5, 10.00, 94, '333');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sqm_price_model_mapping_tbl`
--
ALTER TABLE `sqm_price_model_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sqm_price_model_mapping_tbl`
--
ALTER TABLE `sqm_price_model_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
