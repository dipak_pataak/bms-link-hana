
/***phone number format***/
$(".phone").keypress(function (e) {


    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
    var curchr = this.value.length;

    var curval =  $(this).val();

    if (curchr == 0 && curval.indexOf("(") <= -1) {
        $(this).val("+1" +"(" + curval );
    }else if (curchr == 4 && curval.indexOf("(") <= -1) {
        $(this).val("+1" + curval + + ")" +  "-");
    } else if (curchr == 6 && curval.indexOf("(") > -1) {
        $(this).val(curval + ")-");
    } else if (curchr == 9 && curval.indexOf(")") > -1) {
        $(this).val(curval + "");
    } else if (curchr == 11) {
        $(this).val(curval + "-");
        $(this).attr('maxlength', '16');
    }
});
