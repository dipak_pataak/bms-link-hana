<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// -------------------------------------------------------------
// Paypal IPN Class
//--------------------------------------------------------------
// PayPal Data
$ci = & get_instance();
$user_id = $ci->session->userdata('user_id');
$paypal = $ci->db->select('*')
        ->from('gateway_tbl')
        ->where('default_status', 1)
        ->where('created_by', $user_id)
        ->get()
        ->row();
//echo '<pre>';print_r($paypal);die();
$sandbox = TRUE;
if ($paypal->payment_gateway == "paypal") {
    $sandbox = FALSE;
} else {
    $sandbox = TRUE;
}

// Use PayPal on Sandbox or Live
$config['sandbox'] = $sandbox; // FALSE for live environment
// PayPal Business Email ID
$config['business'] = $paypal->payment_mail; //'fleet_business@example.com';
// What is the default currency?
$config['paypal_lib_currency_code'] = (!empty($paypal->currency) ? $paypal->currency : 'USD');


// If (and where) to log ipn to file
$config['paypal_lib_ipn_log_file'] = BASEPATH . 'logs/paypal_ipn.log';
$config['paypal_lib_ipn_log'] = TRUE;

// Where are the buttons located at 
$config['paypal_lib_button_path'] = 'buttons';

//echo '<pre>';print_r($config);die();





