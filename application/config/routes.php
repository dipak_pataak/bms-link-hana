<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'b_level/Auth_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//============ its for c-level ==================
$route['login'] = 'c_level/Auth_controller/index';
$route['c-level'] = 'c_level/Auth_controller';
$route['c-level-forgot-password-form'] = 'c_level/Auth_controller/c_level_forgot_password_form';
$route['c-level-forgot-password-send'] = 'c_level/Auth_controller/c_level_forgot_password_send';
$route['c-level-dashboard'] = 'c_level/Dashboard_controller/dashboard';
$route['c-level-logout'] = 'c_level/Auth_controller/c_level_logout';
//$route['d-customer-form'] = 'd_level/Customer_controller/customer_form';
//============= its for settings =============
$route['c-my-account'] = 'c_level/Setting_controller/my_account';
$route['my-account-update'] = 'c_level/Setting_controller/my_account_update';
$route['company-profile'] = 'c_level/Setting_controller/company_profile';
$route['company-profile-update'] = 'c_level/Setting_controller/company_profile_update';
$route['c-password-change'] = 'c_level/Setting_controller/change_password';
$route['c-update-password'] = 'c_level/Setting_controller/update_password';
$route['payment-setting'] = 'c_level/Setting_controller/payment_setting';
$route['c-save-gateway'] = 'c_level/Setting_controller/c_save_gateway';
$route['c-gateway-edit/(:any)'] = 'c_level/Setting_controller/c_gateway_edit/$1';
$route['c-update-payment-gateway/(:any)'] = 'c_level/Setting_controller/update_payment_gateway/$1';
$route['c-gateway-delete/(:any)'] = 'c_level/Setting_controller/gateway_delete/$1';
$route['payment-gateway'] = 'c_level/Setting_controller/payment_gateway';
$route['payment-gateway-update'] = 'c_level/Setting_controller/payment_gateway_update';
$route['c-cost-factor'] = 'c_level/Setting_controller/cost_factor';
$route['c-cost-factor-save'] = 'c_level/Setting_controller/cost_factor_save';
$route['iframe-code'] = 'c_level/Setting_controller/iframe_code';
$route['iframe-code-save'] = 'c_level/Setting_controller/iframe_code_save';
$route['d-customer-form'] = 'c_level/Setting_controller/d_customer_form';
$route['d-customer-ifrc-us-stateame-form'] = 'c_level/Setting_controller/d_customer_iframe_form';
$route['d-customer-iframe-form-custom/(:any)/(:any)'] = 'c_level/Setting_controller/d_customer_iframe_form_custom/$1/$2';
$route['d-customer-info-save'] = 'c_level/Setting_controller/d_customer_info_save';
//$route['profile-setting'] = 'c_level/Setting_controller/profile_setting';
$route['c-us-state'] = 'c_level/Setting_controller/us_state';
$route['c-us-state-save'] = 'c_level/Setting_controller/c_us_state_save';
$route['c-us-state-edit/(:any)'] = 'c_level/Setting_controller/c_us_state_edit/$1';
$route['c-us-state-update/(:any)'] = 'c_level/Setting_controller/c_us_state_update/$1';
$route['c-us-state-delete/(:any)'] = 'c_level/Setting_controller/us_state_delete/$1';
$route['import-c-us-state-save'] = 'c_level/Setting_controller/import_c_us_state_save';
$route['logs'] = 'c_level/Setting_controller/logs';

//================ its for users ===============
$route['add-user'] = 'c_level/User_controller/index';
$route['user-save'] = 'c_level/User_controller/user_save';
$route['user-email-check'] = 'c_level/User_controller/user_email_check';
$route['users'] = 'c_level/User_controller/users';
$route['user-edit/(:any)'] = 'c_level/User_controller/user_edit/$1';
$route['user-update/(:any)'] = 'c_level/User_controller/user_update/$1';

//================ its for Role ===============
$route['access-role'] = 'c_level/Role_controller/access_role';
$route['role-permission'] = 'c_level/Role_controller/role_permission';
$route['role-save'] = 'c_level/Role_controller/role_save';
$route['role-list'] = 'c_level/Role_controller/role_list';
$route['role-edit/(:any)'] = 'c_level/Role_controller/role_edit/$1';
$route['role-update'] = 'c_level/Role_controller/role_update';
$route['role-delete/(:any)'] = 'c_level/Role_controller/role_delete/$1';
$route['user-role'] = 'c_level/Role_controller/user_roles';
$route['assign-user-role-save'] = 'c_level/Role_controller/assign_user_role_save';
$route['edit-user-access-role/(:any)'] = 'c_level/Role_controller/edit_user_access_role/$1';
$route['delete-user-access-role/(:any)'] = 'c_level/Role_controller/delete_user_access_role/$1';
$route['assign-user-role-update/(:any)'] = 'c_level/Role_controller/assign_user_role_update/$1';
$route['c-level-check-user-role'] = 'c_level/Role_controller/c_level_check_user_role';



//========== its for menu setup =============
$route['menu-setup'] = 'c_level/Menusetup_controller/menu_setup';
$route['c-menu-setup'] = 'c_level/Menusetup_controller/c_menu_setup';
$route['c-menu-setup/(:any)'] = 'c_level/Menusetup_controller/c_menu_setup/$1';
$route['c-menusetup-edit/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_edit/$1';
$route['c-menusetup-update/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_update/$1';
$route['c-menusetup-delete/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_delete/$1';
$route['c-menusetup-inactive/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_inactive/$1';
$route['c-menusetup-active/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_active/$1';
$route['clevel-menu-search'] = 'c_level/Menusetup_controller/clevel_menu_search';
$route['c-menu-export-csv'] = 'c_level/Menusetup_controller/menu_export_csv';
$route['c-import-menu-save'] = 'c_level/Menusetup_controller/import_menu_update';

$route['menu-setup/(:any)'] = 'c_level/Menusetup_controller/menu_setup/$1';
$route['menusetup-save'] = 'c_level/Menusetup_controller/menusetup_save';
$route['menusetup-edit/(:any)'] = 'c_level/Menusetup_controller/menusetup_edit/$1';
$route['menusetup-update/(:any)'] = 'c_level/Menusetup_controller/menusetup_update/$1';
$route['menusetup-delete/(:any)'] = 'c_level/Menusetup_controller/menusetup_delete/$1';
$route['c-level-menu-search'] = 'c_level/Menusetup_controller/c_level_menu_search';
$route['menusetup-inactive/(:any)'] = 'c_level/Menusetup_controller/menusetup_inactive/$1';
$route['menusetup-active/(:any)'] = 'c_level/Menusetup_controller/menusetup_active/$1';

//============= its for account_chart ==============
$route['chart-of-account'] = 'c_level/Account_controller/chart_of_account';
$route['show-selected-form/(:any)'] = 'c_level/Account_controller/selectedform/$1';
$route['insert-coa'] = 'c_level/Account_controller/insert_coa';
$route['new-account-form/(:any)'] = 'c_level/Account_controller/newform/$1';
$route['account-chart'] = 'c_level/Account_controller/account_chart';
$route['voucher-debit'] = 'c_level/Account_controller/voucher_debit';
$route['debit-voucher-code/(:any)'] = 'c_level/Account_controller/debit_voucher_code/$1';
$route['create-debit-voucher'] = 'c_level/Account_controller/create_debit_voucher';
$route['voucher-credit'] = 'c_level/Account_controller/voucher_credit';
$route['create-credit-voucher'] = 'c_level/Account_controller/create_credit_voucher';
$route['voucher-journal'] = 'c_level/Account_controller/voucher_journal';
$route['create-journal-voucher'] = 'c_level/Account_controller/create_journal_voucher';
$route['update-journal-voucher'] = 'c_level/Account_controller/update_journal_voucher';
$route['contra-voucher'] = 'c_level/Account_controller/contra_voucher';
$route['create-contra-voucher'] = 'c_level/Account_controller/create_contra_voucher';
$route['update-contra-voucher'] = 'c_level/Account_controller/update_contra_voucher';
$route['voucher-approval'] = 'c_level/Account_controller/voucher_approval';
$route['voucher-edit/(:any)'] = 'c_level/Account_controller/voucher_edit/$1';
$route['update-debit-voucher'] = 'c_level/Account_controller/update_debit_voucher';
$route['update-credit-voucher'] = 'c_level/Account_controller/update_credit_voucher';
$route['isactive/(:any)/(:any)'] = 'c_level/Account_controller/isactive/$1/$2';

//============= its for account reports ==============
$route['cash-book'] = 'c_level/Account_controller/cash_book';
$route['bank-book'] = 'c_level/Account_controller/bank_book';
$route['cash-flow'] = 'c_level/Account_controller/cash_flow';
$route['c-cash-flow-report-search'] = 'c_level/Account_controller/cash_flow_report_search';
$route['voucher-reports'] = 'c_level/Account_controller/voucher_reports';
$route['c-voucher-report-serach'] = 'c_level/Account_controller/voucher_report_serach';
$route['c-vouchar-cash/(:any)'] = 'c_level/Account_controller/vouchar_cash/$1';
$route['general-ledger'] = 'c_level/Account_controller/general_ledger';
$route['c-general-led'] = 'c_level/Account_controller/general_led';
$route['c-accounts-report-search'] = 'c_level/Account_controller/accounts_report_search';
$route['profit-loss'] = 'c_level/Account_controller/profit_loss';
$route['c-profit-loss-report-search'] = 'c_level/Account_controller/profit_loss_report_search';
$route['trial-balance'] = 'c_level/Account_controller/trial_ballance';
$route['c-trial-balance-report'] = 'c_level/Account_controller/trial_balance_report';

//================== its for orders ================
$route['new-quotation'] = 'c_level/Order_controller/new_quotation';
$route['manage-order'] = 'c_level/Order_controller/manage_order';
$route['manage-order/(:num)'] = 'c_level/Order_controller/manage_order/$1';
$route['invoice-print'] = 'c_level/Order_controller/invoice_print';
$route['quotation-edit'] = 'c_level/Order_controller/quotation_edit';
$route['manage-invoice'] = 'c_level/Order_controller/manage_invoice';
$route['manage-invoice/(:num)'] = 'c_level/Order_controller/manage_invoice/$1';
$route['order-cancel'] = 'c_level/Order_controller/order_cancel';
$route['track-order'] = 'c_level/Order_controller/track_order';

$route['my-orders'] = 'c_level/Order_controller/my_orders';
$route['my-orders/(:num)'] = 'c_level/Order_controller/my_orders/$1';

$route['c-customer-order-return/(:any)'] = 'c_level/Return_controller/c_customer_order_return/$1';
$route['c-customer-order-return-save'] = 'c_level/Return_controller/c_customer_order_return_save';
$route['customer-return'] = 'c_level/Return_controller/customer_return';
$route['c-show-return-resend'] = 'c_level/Return_controller/c_show_return_resend';
$route['c-show-return-view'] = 'c_level/Return_controller/c_show_return_view';
$route['c-purchase-return'] = 'c_level/Return_controller/purchase_return';
$route['c-purchase-order-return/(:any)'] = 'c_level/Return_controller/c_purchase_order_return/$1';
$route['c-customer-purchase-order-return-save'] = 'c_level/Return_controller/c_customer_purchase_order_return_save';
$route['c-order-return-detials-show/(:any)'] = 'c_level/Return_controller/c_order_return_detials_show/$1';


//============== its for customers ================
$route['add-customer'] = 'c_level/Customer_controller/add_customer';
$route['customer-save'] = 'c_level/Customer_controller/customer_save';
$route['customer-list'] = 'c_level/Customer_controller/customer_list';
$route['customer-list/(:any)'] = 'c_level/Customer_controller/customer_list/$1';
$route['c-show-address-map/(:any)'] = 'c_level/Customer_controller/show_address_map/$1';
$route['customer-view/(:any)'] = 'c_level/Customer_controller/customer_view/$1';
$route['show-customer-record/(:any)'] = 'c_level/Customer_controller/show_customer_record/$1';
$route['customer-edit/(:any)'] = 'c_level/Customer_controller/customer_edit/$1';
$route['c-customer-file-delete/(:any)/(:any)'] = 'c_level/Customer_controller/c_customer_file_delete/$1/$2';
$route['customer-update/(:any)'] = 'c_level/Customer_controller/customer_update/$1';
$route['c-customer-delete/(:any)'] = 'c_level/Customer_controller/customer_delete/$1';
$route['state-wise-city/(:any)'] = 'c_level/Customer_controller/state_wise_city/$1';
$route['c-level-customer-filter'] = 'c_level/Customer_controller/c_level_customer_filter';
$route['get-check-c-customer-unique-email'] = 'c_level/Customer_controller/get_check_unique_email';
$route['customer-comment-view/(:any)'] = 'c_level/Customer_controller/customer_comment_view/$1';
$route['out-customer-comment'] = 'c_level/Customer_controller/out_customer_comment';
$route['c-customer-bulk-upload'] = 'c_level/Customer_controller/customer_bulk_upload';
$route['c-customer-csv-upload'] = 'c_level/Customer_controller/customer_csv_upload_save';
$route['c-level-customer-search'] = 'c_level/Customer_controller/c_level_customer_search';
$route['c-customer-export-csv'] = 'c_level/Customer_controller/c_customer_export_csv';
$route['c-customer-export-pdf'] = 'c_level/Customer_controller/c_customer_export_pdf';
$route['show-old-c-cstomer-comment'] = 'c_level/Customer_controller/show_old_c_cstomer_comment';

//============== its for appointment-form ====================
$route['appointment-setup'] = 'c_level/Appointment_controller/appointment_setup';
$route['appointment-form'] = 'c_level/Appointment_controller/appointment_form';

//$route['appointment-form'] = 'c_level/Setting_controller/appointment_form';


$route['faq'] = 'c_level/Setting_controller/faq';

//============= its for category module ===============
$route['category-wise-subcategory/(:any)'] = 'c_level/Order_controller/category_wise_subcategory/$1';
$route['category-wise-condition/(:any)'] = 'b_level/Condition_controller/category_wise_condition/$1';
$route['category-wise-pattern/(:any)'] = 'b_level/Pattern_controller/category_wise_pattern/$1';

$route['order-logo-print/(:any)'] = 'b_level/Invoice_receipt/order_logo_print/$1';


//================= its for start b-level ======================
$route['b-level-menu-search'] = 'b_level/Menusetup_controller/b_level_menu_search';
$route['b-level'] = 'b_level/Auth_controller';
$route['b-level-dashboard'] = 'b_level/Dashboard';
$route['b-level-logout'] = 'b_level/auth_controller/b_level_logout';
$route['b-level-forgot-password-form'] = 'b_level/Auth_controller/b_level_forgot_password_form';
$route['b-level-forgot-password-send'] = 'b_level/Auth_controller/b_level_forgot_password_send';

//=========== its for supplier module ==============
$route['add-supplier'] = 'b_level/Supplier_controller/add_supplier';
$route['supplier-list'] = 'b_level/Supplier_controller/supplier_list';
$route['supplier-invoice/(:any)'] = 'b_level/Supplier_controller/supplier_invoice/$1';
$route['supplier-edit/(:any)'] = 'b_level/Supplier_controller/supplier_edit/$1';
$route['supplier-delete/(:any)'] = 'b_level/Supplier_controller/supplier_delete/$1';
$route['supplier-filter'] = 'b_level/Supplier_controller/supplier_filter';
$route['get-check-supplier-unique-email'] = 'b_level/Supplier_controller/get_check_supplier_unique_email';
$route['b-level-supplier-search'] = 'b_level/Supplier_controller/b_level_supplier_search';

//============= its for customer module ==============
$route['add-b-customer'] = 'b_level/Customer_controller/add_customer';
$route['get-check-customer-unique-email'] = 'b_level/Customer_controller/get_check_unique_email';
$route['b-customer-list'] = 'b_level/Customer_controller/b_customer_list';
$route['b-level-customer-filter'] = 'b_level/Customer_controller/b_level_customer_filter';
$route['show-b-customer-record/(:any)'] = 'b_level/Customer_controller/show_b_customer_record/$1';
$route['b-customer-view/(:any)'] = 'b_level/Customer_controller/customer_view/$1';
$route['send-customer-comment'] = 'b_level/Customer_controller/send_customer_comment';
$route['b-customer-edit/(:any)'] = 'b_level/Customer_controller/customer_edit/$1';
$route['b-customer-file-delete/(:any)/(:any)'] = 'b_level/Customer_controller/b_customer_file_delete/$1/$2';
$route['customer-import'] = 'b_level/Customer_controller/customer_import';
$route['show-address-map/(:any)'] = 'b_level/Customer_controller/show_address_map/$1';
$route['customer-csv-upload'] = 'b_level/Customer_controller/customer_csv_upload';
$route['sales-today'] = 'b_level/Customer_controller/sales_today';
$route['sales-yesterday'] = 'b_level/Customer_controller/sales_yesterday';
$route['sales-lastweek'] = 'b_level/Customer_controller/sales_lastweek';
$route['show-customer-new-window-popup'] = 'b_level/Customer_controller/show_customer_new_window_popup';
$route['new-customer-modal'] = 'b_level/Customer_controller/new_customer_modal';
$route['b-level-customer-search'] = 'b_level/Customer_controller/b_level_customer_search';
$route['customer-export-csv'] = 'b_level/Customer_controller/customer_export_csv';
$route['customer-export-pdf'] = 'b_level/Customer_controller/customer_export_pdf';
$route['top-search-customer-order-info'] = 'b_level/Customer_controller/top_search_customer_order_info';
$route['c-top-search-customer-order-info'] = 'c_level/Customer_controller/top_search_customer_order_info';


//============== its for  new_order ===============
$route['new-order'] = 'b_level/Order_controller/new_order';
$route['order-kanban'] = 'b_level/Order_controller/order_kanban';
$route['b-order-list'] = 'b_level/Order_controller/order_list';
$route['b-order-list/(:any)'] = 'b_level/Order_controller/order_list/$1';
$route['delete-order/(:any)'] = 'b_level/Order_controller/delete_order/$1';

$route['order-view/(:any)'] = 'b_level/Order_controller/order_view/$1';

$route['customer-wise-sidemark/(:any)'] = 'b_level/Order_controller/customer_wise_sidemark/$1';


$route['order-id-generate'] = 'b_level/Order_controller/order_id_generate';
$route['b-single-order-test'] = 'b_level/Account_controller/b_single_order_test';

$route['order-customer-info-edit/(:any)'] = 'b_level/Invoice_receipt/order_customer_info_edit/$1';
$route['order-single-product-edit/(:any)'] = 'b_level/Invoice_receipt/order_single_product_edit/$1';

//=============== its for category module===============
$route['add-category'] = 'b_level/Category_controller/add_category';
$route['manage-category'] = 'b_level/Category_controller/manage_category';
$route['category-edit/(:any)'] = 'b_level/Category_controller/category_edit/$1';
$route['category-assign'] = 'b_level/Category_controller/category_assign';
$route['category-filter'] = 'b_level/Category_controller/category_filter';
$route['b-category-delete/(:any)'] = 'b_level/Category_controller/b_category_delete/$1';
$route['b-level-category-search'] = 'b_level/Category_controller/b_level_category_search';
$route['b-assinged-category-product-delete'] = 'b_level/Category_controller/assinged_category_product_delete';


// this is row material section
$route['add-row-material'] = 'b_level/Row_materials/add_row_material';
$route['test-mail'] = 'b_level/Row_materials/sendmail';
$route['row-material-list'] = 'b_level/Row_materials/row_material_list';
$route['raw-material-csv-upload'] = 'b_level/Row_materials/raw_material_csv_upload';
$route['edit-row-material/(:any)'] = 'b_level/Row_materials/edit_row_material/$1';
$route['delete-row-material/(:any)'] = 'b_level/Row_materials/delete_row_material/$1';
$route['b-level-rawmaterial-search'] = 'b_level/Row_materials/b_level_rawmaterial_search';
$route['raw-material-usage'] = 'b_level/Row_materials/raw_material_usage';
$route['raw-material-usage-save'] = 'b_level/Row_materials/raw_material_usage_save';


// this is color section
$route['color'] = 'b_level/Color_controller/add_color';
$route['import-color-save'] = 'b_level/Color_controller/import_color_save';
$route['get-color-check/(:any)'] = 'b_level/Color_controller/get_color_check/$1';
$route['b-level-color-filter'] = 'b_level/Color_controller/color_filter';
$route['b-level-color-search'] = 'b_level/Color_controller/b_level_color_search';

//================= its for product module ================
$route['add-product'] = 'b_level/Product_controller/add_product';
$route['product-manage'] = 'b_level/Product_controller/product_manage';
$route['product-manage/(:num)'] = 'b_level/Product_controller/product_manage/$1';
$route['product-edit/(:any)'] = 'b_level/Product_controller/product_edit/$1';
$route['delete-product/(:any)'] = 'b_level/Product_controller/delete_product/$1';
$route['product-filter'] = 'b_level/Product_controller/product_filter';
$route['product-bulk-csv-upload'] = 'b_level/Product_controller/product_bulk_csv_upload';
$route['b-level-product-search'] = 'b_level/Product_controller/b_level_product_search';
$route['price-model-wise-style'] = 'b_level/Product_controller/price_model_wise_style';

//========== its for Pattern_controller ==============
$route['add-pattern'] = 'b_level/Pattern_controller/add_pattern';
$route['manage-pattern'] = 'b_level/Pattern_controller/manage_pattern';
//$route['pattern-edit/(:any)'] = 'b_level/Pattern_controller/pattern_edit/$1';
$route['pattern-edit/(:any)'] = 'b_level/Pattern_controller/pattern_edit/$1';
$route['pattern-delete/(:any)'] = 'b_level/Pattern_controller/pattern_delete/$1';
$route['pattern-model-filter'] = 'b_level/Pattern_controller/pattern_model_filter';
$route['b-level-pattern-search'] = 'b_level/Pattern_controller/b_level_pattern_search';
$route['b-assinged-product-delete'] = 'b_level/Pattern_controller/b_assinged_product_delete';

//============ its for Attribute_controller ================
$route['add-attribute'] = 'b_level/Attribute_controller/add_attribute';
$route['manage-attribute'] = 'b_level/Attribute_controller/manage_attribute';
$route['attribute-filter'] = 'b_level/Attribute_controller/attribute_filter';

$route['attribute-edit/(:any)'] = 'b_level/Attribute_controller/attribute_edit/$1';
$route['attribute-delete/(:any)'] = 'b_level/Attribute_controller/attribute_delete/$1';
$route['attribute-type'] = 'b_level/Attribute_controller/attribute_type';
$route['b-level-attribute-search'] = 'b_level/Attribute_controller/b_level_attribute_search';

//============= its for Condition_controller===============
$route['add-condition'] = 'b_level/Condition_controller/add_condition';
$route['condition-save'] = 'b_level/Condition_controller/condition_save';
$route['condition-edit/(:any)'] = 'b_level/Condition_controller/condition_edit/$1';
$route['condition-update/(:any)'] = 'b_level/Condition_controller/condition_update/$1';
$route['condition-delete/(:any)'] = 'b_level/Condition_controller/condition_delete/$1';
$route['condition-filter'] = 'b_level/Condition_controller/condition_filter';

$route['b-cost-factor'] = 'b_level/Setting_controller/cost_factor';
$route['b-iframe-code'] = 'b_level/Setting_controller/iframe_code_generate';
$route['d-customer-iframe-generate/(:any)/(:any)'] = 'b_level/Setting_controller/d_customer_iframe_generate/$1/$2';
$route['b-d-customer-info-save'] = 'b_level/Setting_controller/b_d_customer_info_save';
$route['b-customer-iframe-view'] = 'b_level/Setting_controller/b_customer_iframe_view';
$route['b-cost-factor-save'] = 'b_level/Setting_controller/cost_factor_save';
$route['add-tax'] = 'b_level/Condition_controller/add_tax';

$route['shipping'] = 'b_level/Setting_controller/shipping';
$route['shipping-edit/(:any)'] = 'b_level/Setting_controller/shipping_edit/$1';
$route['shipping-method-delete/(:any)'] = 'b_level/Setting_controller/shipping_method_delete/$1';

//=========== its for Pricemodel_controller ==============
$route['add-price'] = 'b_level/Pricemodel_controller/add_price';
$route['save-price-style'] = 'b_level/Pricemodel_controller/save_price_style';
$route['manage-price'] = 'b_level/Pricemodel_controller/manage_price';
$route['add-group'] = 'b_level/Pricemodel_controller/add_group';
$route['group-manage'] = 'b_level/Pricemodel_controller/group_manage';
$route['edit-group/(:any)'] = 'b_level/Pricemodel_controller/edit_group/$1';
$route['price-manage-filter'] = 'b_level/Pricemodel_controller/price_manage_filter';
$route['row-column-search'] = 'b_level/Pricemodel_controller/row_column_search';

//=========== group price ==============
$route['add-group-price'] = 'b_level/Pricemodel_controller/add_group_price';
$route['manage-group-price'] = 'b_level/Pricemodel_controller/manage_group_price';
$route['group-manage-filter'] = 'b_level/Pricemodel_controller/group_manage_filter';
$route['group-price-search'] = 'b_level/Pricemodel_controller/group_price_search';

//=============== its for manufacturer module ===================
$route['add-manufacturer'] = 'b_level/Manufacturer_controller/add_manufacturer';
$route['manage-manufacturer'] = 'b_level/Manufacturer_controller/manage_manufacturer';
$route['manufacturer-edit/(:any)'] = 'b_level/Manufacturer_controller/manufacturer_edit/$1';


//=============== its for stock module ==============
$route['stock-availability'] = 'b_level/Stock_controller/stock_availability';
$route['stock-history'] = 'b_level/Stock_controller/stock_history';

//============== its for return =============
$route['show-return-resend'] = 'b_level/Return_controller/show_return_resend';
//$route['show-return-calculation'] = 'b_level/Return_controller/show_return_calculation';
$route['b-customer-return'] = 'b_level/Return_controller/customer_return';
$route['customer-order-return/(:any)'] = 'b_level/Return_controller/customer_order_return/$1';
$route['customer-order-return-save'] = 'b_level/Return_controller/customer_order_return_save';
$route['raw-material-return-purchase/(:any)'] = 'b_level/Return_controller/raw_material_return_purchase/$1';
$route['raw-material-supplier-return'] = 'b_level/Return_controller/raw_material_supplier_return';
$route['raw-material-return-purchase-save'] = 'b_level/Return_controller/raw_material_return_purchase_save';
$route['raw-material-return-purchase-approved'] = 'b_level/Return_controller/raw_material_return_purchase_approved';
$route['product-return'] = 'b_level/Return_controller/product_return';
$route['b-supplier-return-filter'] = 'b_level/Return_controller/supplier_return_filter';
//$route['raw-material-return-approved/(:any)'] = 'b_level/Return_controller/raw_material_return_approved/$1';
//$route['raw-material-return-rejected/(:any)'] = 'b_level/Return_controller/raw_material_return_rejected/$1';
//============= its for accounts module ============
$route['b-account-chart'] = 'b_level/Account_controller/b_account_chart';

$route['purchase-entry'] = 'b_level/Purchase_controller/purchase_entry';
$route['purchase-list'] = 'b_level/Purchase_controller/purchase_list';
$route['purchase-save'] = 'b_level/Purchase_controller/purchase_save';
$route['delete-purchase/(:any)'] = 'b_level/Purchase_controller/delete_purchase/$1';
$route['edit-purchase/(:any)'] = 'b_level/Purchase_controller/edit_purchase/$1';
$route['update-purchase'] = 'b_level/Purchase_controller/update_purchase';
$route['view-purchase/(:any)'] = 'b_level/Purchase_controller/view_purchase/$1';
$route['get-material-info/(:any)'] = 'b_level/Purchase_controller/get_material_wise_color_info/$1';
$route['get-material-wise-pattern-info/(:any)'] = 'b_level/Purchase_controller/get_material_wise_pattern_info/$1';
$route['get-material-color-pattern-wise-stock-qnt/(:any)/(:any)/(:any)'] = 'b_level/Purchase_controller/get_material_color_pattern_wise_stock_qnt/$1/$2/$3';

$route['b-debit-voucher'] = 'b_level/Account_controller/debit_voucher';
$route['create-b-debit-voucher'] = 'b_level/Account_controller/create_debit_voucher';
$route['b-debit-voucher-code/(:any)'] = 'b_level/Account_controller/debit_voucher_code/$1';
$route['b-show-selected-form/(:any)'] = 'b_level/Account_controller/selectedform/$1';
$route['b-insert-coa'] = 'b_level/Account_controller/insert_coa';
$route['b-new-account-form/(:any)'] = 'b_level/Account_controller/newform/$1';
$route['b-credit-voucher'] = 'b_level/Account_controller/credit_voucher';
$route['create-b-credit-voucher'] = 'b_level/Account_controller/create_credit_voucher';
$route['b-journal-voucher'] = 'b_level/Account_controller/journal_voucher';
$route['create-b-journal-voucher'] = 'b_level/Account_controller/create_journal_voucher';
$route['b-contra-voucher'] = 'b_level/Account_controller/contra_voucher';
$route['create-b-contra-voucher'] = 'b_level/Account_controller/create_contra_voucher';
$route['b-voucher-approval'] = 'b_level/Account_controller/voucher_approval';
$route['update-b-debit-voucher'] = 'b_level/Account_controller/update_debit_voucher';
$route['update-b-credit-voucher'] = 'b_level/Account_controller/update_credit_voucher';
$route['update-b-journal-voucher'] = 'b_level/Account_controller/update_journal_voucher';
$route['update-b-contra-voucher'] = 'b_level/Account_controller/update_contra_voucher';
$route['b-isactive/(:any)/(:any)'] = 'b_level/Account_controller/isactive/$1/$2';
$route['b-voucher-edit/(:any)'] = 'b_level/Account_controller/voucher_edit/$1';
$route['b-voucher-reports'] = 'b_level/Account_controller/voucher_reports';
$route['b-vouchar-cash/(:any)'] = 'b_level/Account_controller/vouchar_cash/$1';
$route['b-voucher-report-serach'] = 'b_level/Account_controller/voucher_report_serach';


$route['b-bank-book'] = 'b_level/Account_controller/bank_book';
$route['b-cash-book'] = 'b_level/Account_controller/cash_book';
$route['b-cash-flow'] = 'b_level/Account_controller/cash_flow';
$route['b-cash-flow-report-search'] = 'b_level/Account_controller/cash_flow_report_search';
$route['b-general-ledger'] = 'b_level/Account_controller/general_ledger';
$route['b-accounts-report-search'] = 'b_level/Account_controller/b_accounts_report_search';
$route['b-general-led'] = 'b_level/Account_controller/general_led';
$route['b-profit-loss'] = 'b_level/Account_controller/profit_loss';
$route['b-profit-loss-report-search'] = 'b_level/Account_controller/b_profit_loss_report_search';
$route['b-trial-ballance'] = 'b_level/Account_controller/trial_ballance';
$route['b-trial-balance-report'] = 'b_level/Account_controller/b_trial_ballance_report';


//============= its for settings module ============
$route['b-my-account'] = 'b_level/Setting_controller/my_account';
$route['profile-setting'] = 'b_level/Setting_controller/profile_setting';
$route['mail'] = 'b_level/Setting_controller/mail';
$route['update-mail-configure'] = 'b_level/Setting_controller/update_mail_configure';
$route['sms-configure'] = 'b_level/Setting_controller/sms_configure';
$route['sms-config-save'] = 'b_level/Setting_controller/sms_config_save';
$route['sms-config-edit/(:any)'] = 'b_level/Setting_controller/sms_config_edit/$1';
$route['sms-config-update/(:any)'] = 'b_level/Setting_controller/sms_config_update/$1';
$route['sms-config-delete/(:any)'] = 'b_level/Setting_controller/sms_config_delete/$1';
$route['sms'] = 'b_level/Setting_controller/sms';
$route['sms-send'] = 'b_level/Setting_controller/sms_send';
$route['sms-csv-upload'] = 'b_level/Setting_controller/sms_csv_upload';
$route['group-sms-send'] = 'b_level/Setting_controller/group_sms_send';
$route['gateway'] = 'b_level/Setting_controller/gateway';
$route['save-gateway'] = 'b_level/Setting_controller/save_gateway';
$route['gateway-edit/(:any)'] = 'b_level/Setting_controller/gateway_edit/$1';
$route['gateway-delete/(:any)'] = 'b_level/Setting_controller/gateway_delete/$1';
$route['update-gateway/(:any)'] = 'b_level/Setting_controller/update_gateway/$1';
$route['b-change-password'] = 'b_level/Setting_controller/change_password';
$route['b-update-password'] = 'b_level/Setting_controller/update_password';
$route['b-access-logs'] = 'b_level/Setting_controller/access_logs';
$route['b-level-users'] = 'b_level/Setting_controller/b_level_users';
$route['b-level-users'] = 'b_level/Setting_controller/b_level_users';
$route['b-level-users/(:any)'] = 'b_level/Setting_controller/b_level_users/$1';
$route['user-filter'] = 'b_level/Setting_controller/user_filter';

$route['uom-list'] = 'b_level/Setting_controller/uom_list';
$route['us-state'] = 'b_level/Setting_controller/us_state';
$route['us-state-save'] = 'b_level/Setting_controller/us_state_save';
$route['us-state-edit/(:any)'] = 'b_level/Setting_controller/us_state_edit/$1';
$route['us-state-update/(:any)'] = 'b_level/Setting_controller/us_state_update/$1';
$route['us-state-delete/(:any)'] = 'b_level/Setting_controller/us_state_delete/$1';
$route['import-us-state-save'] = 'b_level/Setting_controller/import_us_state_save';
$route['b-level-usstate-search/(:any)'] = 'b_level/Setting_controller/b_level_usstate_search/$1';

//================ its for b-level Role permission===============
$route['b-user-role-assign'] = 'b_level/role_controller/user_roles';
$route['b-level-check-user-role'] = 'b_level/role_controller/b_level_check_user_role';
$route['b-user-access-role-list'] = 'b_level/role_controller/access_role';
$route['b-role-list'] = 'b_level/role_controller/role_list';
$route['b-role-permission'] = 'b_level/role_controller/role_permission';
$route['b-menu-setup'] = 'b_level/menusetup_controller/menu_setup';
$route['menu-export-csv'] = 'b_level/menusetup_controller/menu_export_csv';
$route['import-menu-save'] = 'b_level/menusetup_controller/import_menu_update';

$route['create-user'] = 'b_level/Setting_controller/create_user';
$route['get-check-user-unique-email'] = 'b_level/Setting_controller/get_check_user_unique_email';


$route['catalog-user'] = 'b_level/Shared_catalog_controller';
$route['catalog-user/(:any)'] = 'b_level/Shared_catalog_controller/index/$1';
$route['b-user-request-for-catalog'] = 'b_level/Shared_catalog_controller/b_user_request_for_catalog';
$route['catalog-request'] = 'b_level/Shared_catalog_controller/catalog_request_list';
$route['approve-catalog-request'] = 'b_level/Shared_catalog_controller/approve_catalog_request';
$route['not-approve-catalog-request/(:any)'] = 'b_level/Shared_catalog_controller/not_approve_catalog_request/$1';
$route['b-user-catalog-product/(:any)'] = 'b_level/Shared_catalog_controller/b_user_catalog_product_list/$1';
$route['catalog-product-add-order/(:any)'] = 'b_level/Shared_catalog_controller/catalog_product_order_add/$1';
$route['b-user-wise-sidemark/(:any)'] = 'b_level/Shared_catalog_controller/b_user_wise_sidemark/$1';
$route['catalog-order-list'] = 'b_level/Shared_catalog_controller/catalog_order_list';
$route['b-user-re-order'] = 'b_level/Shared_catalog_controller/re_order_b_user_order';
$route['manufacturing-order-view/(:any)'] = 'b_level/Shared_catalog_controller/manufacturing_order_view/$1';

$route['manufacturing-catalog-order-list'] = 'b_level/Shared_catalog_controller/manufacturing_catalog_order_list';

$route['catalog-b-user-order-list'] = 'b_level/Shared_catalog_controller/catalog_b_user_order_list';

// this is tag section
$route['gallery_tag'] = 'b_level/Tag_controller/add_tag';
$route['b-level-tag-search'] = 'b_level/Tag_controller/b_level_tag_search';
$route['get-tag-check/(:any)'] = 'b_level/Tag_controller/get_tag_check/$1';
$route['import-tag-save'] = 'b_level/Tag_controller/import_tag_save';

$route['add_b_gallery_image'] = 'b_level/Image_controller/add_image';
$route['manage_b_gallery_image'] = 'b_level/Image_controller/manage_image';

//C-level
$route['manage_c_gallery_image'] = 'c_level/Image_controller/manage_image';
$route['b_commission_report'] = 'b_level/Commision_report_controller/search_b/All';
$route['b_commission_report/(:any)'] = 'b_level/Commision_report_controller/b_commission_report/$1';
$route['c_commission_report'] = 'b_level/Commision_report_controller/search_c/All';
$route['c_commission_report(:any)'] = 'b_level/Commision_report_controller/c_commission_report/$1';