<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* Name:  Twilio
	*
	* Author: Ben Edmunds
	*		  ben.edmunds@gmail.com
	*         @benedmunds
	*
	* Location:
	*
	* Created:  03.29.2011
	*
	* Description:  Twilio configuration settings.
	*
	*
	*/
                $CI = & get_instance();
                $active_sms_gateway = $CI->db->select('*')->from('sms_gateway')->where('default_status', 1)->get()->result();
//                echo $active_sms_gateway[0]->user;
//                echo '<pre>';                print_r($active_sms_gateway);die();
	/**
	 * Mode ("sandbox" or "prod")
	 **/
	$config['mode']   = 'sandbox';

	/**
	 * Account SID
	 **/
	$config['account_sid']   = $active_sms_gateway[0]->user; //'ACbf1bb71d0816cd1c88b81b8a240e792f';

	/**
	 * Auth Token
	 **/
	$config['auth_token']    = $active_sms_gateway[0]->password;//'c5b6264fb1bf8ed2c628c1c6af71b2d7';

	/**
	 * API Version
	 **/
	$config['api_version']   = '2010-04-01';

	/**
	 * Twilio Phone Number
	 **/
	$config['number']        = $active_sms_gateway[0]->phone; //'+12062024567';
//                echo '<pre>';                print_r($config);die();


/* End of file twilio.php */