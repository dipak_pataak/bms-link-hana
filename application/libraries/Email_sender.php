<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_sender {


    public function send_email($arrayData)
    {
        $data = (object)$arrayData;
    
        $ci =& get_instance();
        
        $ci->db->select('*');
        $ci->db->from('mail_config_tbl');
        $ci->db->limit('1');
        $get_mail_config =  $ci->db->get()->row();


        $config = Array(
            'protocol'  => $get_mail_config->protocol, //'smtp',
            'smtp_host' => $get_mail_config->smtp_host, //'ssl://smtp.gmail.com',
            'smtp_port' => $get_mail_config->smtp_port, //465,
            'smtp_user' => $get_mail_config->smtp_user, //'khs2010welfare@gmail.com', // change it to yours
            'smtp_pass' => $get_mail_config->smtp_pass, // 'bvrayygbwwmxnkdj', // change it to yours
            'mailtype' => $get_mail_config->mailtype, //'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        
        // customer information
        $customer_info = $ci->db->where('customer_id',$data->customer_id)->get('customer_info')->row();
        
        // Load Html template
        $mesg = $data->message;
        $subject = $data->subject;


        $ci->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $ci->email->set_header('Content-type', 'text/html');
        $ci->load->library('email', $config);
        $ci->email->initialize($config);
        $ci->email->set_newline("\r\n");

        $ci->email->from($get_mail_config->smtp_user, "Support Center");
        $ci->email->to($customer_info->email);

        $ci->email->subject($subject);
        $ci->email->message($mesg);
        $ci->email->send();
        return 1;
        

    }
    

}