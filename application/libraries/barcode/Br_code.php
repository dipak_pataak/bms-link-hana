<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include('src/BarcodeGenerator.php');
include('src/BarcodeGeneratorPNG.php');
include('src/BarcodeGeneratorSVG.php');
include('src/BarcodeGeneratorJPG.php');
include('src/BarcodeGeneratorHTML.php');

class Br_code {

	public function gcode($code=NULL)
	{
		$generatorHTML = new Picqer\Barcode\BarcodeGeneratorJPG();
		return $generatorHTML->getBarcode($code, $generatorHTML::TYPE_CODE_128);
	}



	public function gcode1($code=NULL)
	{

		$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
		$path =  '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128, 1.5,50)) . '">';

		return $path;
	}

 
}





// $generatorSVG = new Picqer\Barcode\BarcodeGeneratorSVG();
// file_put_contents('tests/verified-files/081231723897-ean13.svg', $generatorSVG->getBarcode('081231723897', $generatorSVG::TYPE_EAN_13));

// $generatorHTML = new Picqer\Barcode\BarcodeGeneratorJPG();
// echo file_put_contents('tests/verified-files/0812317238-ean13.JPG', $generatorHTML->getBarcode('0812454556317238', $generatorHTML::TYPE_CODE_128));

// $generatorSVG = new Picqer\Barcode\BarcodeGeneratorSVG();
// file_put_contents('tests/verified-files/0049000004632-ean13.svg', $generatorSVG->getBarcode('0049000004632', $generatorSVG::TYPE_EAN_13));
