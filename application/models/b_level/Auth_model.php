<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function check_user($data = array()) {
//        echo '<pre>';        print_r($data);die();
        $result = $this->db->select("*")
                        ->from('log_info')
                        ->join('user_info', 'user_info.id = log_info.user_id')
                        ->where('log_info.user_type', 'b')
                        ->where('log_info.email', $data['email'])
                        ->where('log_info.password', md5($data['password']))
//                        ->or_where('log_info.random_key', $data['password'])
                        ->get()->row();



        return $result;
    }

//    =========== its for permission =============
    public function userPermission($id = null) {

        $acc_tbl = $this->db->select('*')->from('b_user_access_tbl')->where('user_id', $id)->get()->result();

        if ($acc_tbl != NULL) {
            $role_id = [];
            foreach ($acc_tbl as $key => $value) {
                $role_id[] = $value->role_id;
            }

            return $result = $this->db->select("
				b_role_permission_tbl.role_id, 
				b_role_permission_tbl.menu_id, 
				IF(SUM(b_role_permission_tbl.can_create)>=1,1,0) AS 'create', 
				IF(SUM(b_role_permission_tbl.can_access)>=1,1,0) AS 'read', 
				IF(SUM(b_role_permission_tbl.can_edit)>=1,1,0) AS 'update', 
				IF(SUM(b_role_permission_tbl.can_delete)>=1,1,0) AS 'delete',
				b_menusetup_tbl.menu_title,
				b_menusetup_tbl.page_url,
				b_menusetup_tbl.module
				")
                    ->from('b_role_permission_tbl')
                    ->join('b_menusetup_tbl', 'b_menusetup_tbl.id = b_role_permission_tbl.menu_id', 'full')
                    ->where_in('b_role_permission_tbl.role_id', $role_id)
                    ->group_by('b_role_permission_tbl.menu_id')
                    ->group_start()
                    ->where('can_create', 1)
                    ->or_where('can_access', 1)
                    ->or_where('can_edit', 1)
                    ->or_where('can_delete', 1)
                    ->group_end()
                    ->get()
                    ->result();
        } else {
            return 0;
        }
    }

//    =========== permission close ============
    //    ============== its for checkEmail =============
    public function checkEmail($email) {
        return $query = $this->db->select('*')->from('log_info')->where('email', $email)->where('user_type', 'b')->get()->row();
    }

//    ========== its for sendLink ===============
    public function sendLink($log_id, $email, $data, $random_key) {
//        echo $data['get_mail_config'][0]->protocol;
//        echo $email. " logid ". $log_id. "random key ". $random_key;
//        echo '<pre>';        print_r($random_key);die();
        $data['baseurl'] = base_url();
        $config = Array(
            'protocol' => $data['get_mail_config'][0]->protocol, //'smtp',
            'smtp_host' => $data['get_mail_config'][0]->smtp_host, //'ssl://smtp.gmail.com',
            'smtp_port' => $data['get_mail_config'][0]->smtp_port, //465,
            'smtp_user' => $data['get_mail_config'][0]->smtp_user, //'khs2010welfare@gmail.com', // change it to yours
            'smtp_pass' => $data['get_mail_config'][0]->smtp_pass, // 'bvrayygbwwmxnkdj', // change it to yours
            'mailtype' => $data['get_mail_config'][0]->mailtype, //'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
//        echo '<pre>';        print_r($config);die();
        $data['author_info'] = $this->author_info($log_id);
        $data['random_key'] = $random_key;
//        echo $data['author_info']->doctor_name; 
//        echo $data['random_key'];die();
//        echo '<pre>';        print_r($data['author_info']);die();
        $mesg = $this->load->view('b_level/sendPasswordLink', $data, TRUE);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['get_mail_config'][0]->smtp_user, "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMSLINK");
// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
// . "\n\nThanks\nMetallica Gifts");
// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");
        $this->email->message($mesg);
        $this->email->send();
    }

    //    =========== its for author_info ==============
    public function author_info($log_id) {
        
        $query = $this->db->select('*')
                        ->from('log_info a')
                        ->join('user_info c', 'c.id = a.user_id', 'left')
                        ->where('a.row_id', $log_id)
                        ->get()->row();
        return $query;
    }

}
