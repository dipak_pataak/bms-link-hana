<?php

class Category_model extends CI_model {

//============= its for category list ==============
    public function category_list($offset, $limit) {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('category_tbl')
                        ->order_by('category_id', 'desc')
                        ->where('created_by',$created_by)
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//=========== its for get_category ================
    public function get_category() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('category_tbl')
                        ->where('parent_category', 0)
                        ->where('status', 1)
                        ->where('created_by',$created_by)
                        ->order_by('category_name', 'asc')
                        ->get()->result();
        return $query;
    }

//    =========== its for category_edit ==============
    public function category_edit($id) {
        $query = $this->db->select('*')
                ->from('category_tbl')
                ->where('category_id', $id)
                ->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

//    =========== its for category_assigned_product ==============
    public function category_assigned_product($id) {
        $query = $this->db->select('*')
                ->from('product_tbl')
                ->where('category_id', $id)
                ->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

//    =============its for get_filter_data =============
    public function get_filter_data($category_name, $parent_category, $status) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $check = 1;
        $this->db->select('*');
        $this->db->from('category_tbl a');
        if ($category_name && $parent_category && $status) {
            $this->db->like('a.category_name', $category_name, 'both');
            $this->db->like('a.parent_category', $parent_category, 'both');
            $this->db->like('a.status', $status, 'both');
        } elseif ($category_name) {
            $this->db->like('a.category_name', $category_name, 'both');
        } elseif ($parent_category) {
            $this->db->like('a.parent_category', $parent_category, 'both');
        } elseif ($status != '') {
            if ($status == 1) {
                $this->db->like('a.status', $status, 'both');
            } else {
                $this->db->like('a.status', 0, 'both');
            }
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.category_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_category_search_result($keyword) {
        $sql = "SELECT a.* FROM category_tbl a WHERE (a.category_name LIKE '%$keyword%') ORDER BY category_id DESC ";
//        echo $sql;        die();
        $query = $this->db->query($sql);
        return $query->result();
    }

}
