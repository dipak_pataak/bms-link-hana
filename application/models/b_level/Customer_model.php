<?php

class Customer_model extends CI_model {

    //    ========= its for doctor certificate save ==========
    public function save_customer_file($customerFileinfo = array()) {
        $this->db->insert_batch('customer_file_tbl', $customerFileinfo);
    }

//============= its for get_customer =================
    public function get_customer() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', $level_id)
                        ->order_by('customer_id', 'desc')
                        ->get()->result();
        return $query;
    }

//============ its for customer_list method ==============
    public function customer_list($offset = null, $limit = null) {

       // print_r($this->session->userdata('isAdmin') );die();
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name")
                        ->from('customer_info')
                        ->where('customer_info.level_id', $level_id)
                        ->order_by('customer_info.customer_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    //    =============== its for show_customer_record ================= 
    public function show_b_customer_record($id) {

        $query = $this->db->select("a.*, b.created_by c_level_parent_user, CONCAT_WS(' ', a.first_name, a.last_name) as full_name")
                ->from('customer_info a')
                ->join('user_info b', 'b.id = a.created_by')
                ->where('a.customer_id', $id)
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    =============== its for customer_view ================= 
    public function customer_view($id) {
        $query = $this->db->select("a.*, CONCAT_WS(' ', a.first_name, a.last_name) as full_name, b.logo")
                ->from('customer_info a')
                ->join('company_profile b', 'b.user_id = a.customer_user_id', 'left')
//                ->join('customer_phone_type_tbl c', 'c.customer_id = a.customer_id', 'left')
                ->where('a.customer_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ============= its for get_customer_phones =============
    public function get_customer_phones($id) {
        $this->db->select('*');
        $this->db->from('customer_phone_type_tbl a');
        $this->db->where('a.customer_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    //    =========== its for customer_edit ==============
    // public function customer_edit($id) {
    //     $query = $this->db->select('a.*, b.email as username, c.*')
    //             ->from('customer_info a')
    //             ->join('log_info b', 'b.user_id = a.customer_user_id', 'left')
    //             ->join('customer_phone_type_tbl c', 'c.customer_id = a.customer_id', 'left')
    //             ->where('a.customer_id', $id)
    //             ->get();
    //     if ($query->num_rows() > 0) {
    //         return $query->result_array();
    //     } else {
    //         return false;
    //     }
    // }


    //    =========== its for customer_edit ==============
    public function customer_edit($id) {

        $query =  $this->db->select('customer_info.*,log_info.email as username, 
            customer_phone_type_tbl.phone_type,
            customer_phone_type_tbl.phone')

        ->from('customer_info')
        ->join('log_info','log_info.user_id=customer_info.customer_id','left')
        ->join('customer_phone_type_tbl','customer_phone_type_tbl.customer_id=customer_info.customer_id','left')
        ->where('customer_info.customer_id',$id)
        ->get();

// echo "<pre>";
// print_r($query->result_array()); exit;
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
        
    }


//    ============ its for accounts table hit ============
    public function headcode() {
        $query = $this->db->query("SELECT MAX(HeadCode) as HeadCode FROM b_acc_coa WHERE HeadLevel='4' And HeadCode LIKE '1020301%'");
        return $query->row();
    }

//    ============ get_customer_filter =============
    public function get_customer_filter($company_name, $phone, $email, $address) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, CONCAT(" ", first_name,  last_name) as full_name');
        $this->db->from('customer_info a');
        if ($company_name && $phone && $email && $address) {
            $this->db->like('a.company', $company_name, 'both');
            if ($phone) {
                $this->db->like('a.phone', $phone, 'both');
                $this->db->or_like('a.phone_2', $phone, 'both');
                $this->db->or_like('a.phone_3', $phone, 'both');
            }
            $this->db->like('a.email', $email, 'both');
            $this->db->like('a.address', $address, 'both');
            $this->db->where('a.level_id', $level_id);
        } elseif ($company_name) {
            $this->db->like('a.company', $company_name, 'both');
            $this->db->where('a.level_id', $level_id);
        } elseif ($phone) {
            $this->db->like('a.phone', $phone, 'both');
//            $this->db->or_like('a.phone_2', $phone, 'both');
//            $this->db->or_like('a.phone_3', $phone, 'both');
            $this->db->where('a.level_id', $level_id);
        } elseif ($email) {
            $this->db->like('a.email', $email, 'both');
            $this->db->where('a.level_id', $level_id);
        } elseif ($address) {
            $this->db->like('a.address', $address, 'both');
            $this->db->where('a.level_id', $level_id);
        } else {
            $this->db->where('a.level_id', $level_id);
        }
        $this->db->order_by('a.customer_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    //--------------------------------------
    public function get_orderd_by_customer_id($customer_id) {
        //        $query = $this->db->select("quatation_tbl.*,
//            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
//            customer_info.address,
//            customer_info.customer_no,
//            customer_info.customer_id")
//                        ->from('quatation_tbl')
//                        ->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left')
//                        ->where('quatation_tbl.customer_id', $customer_id)
//                        ->get()->result();
//
//        return $query;
//        $query = $this->db->select('*')
//                ->from('b_level_quatation_tbl a')
//                ->where('a.customer_id', $customer_id)
//                ->get()->result();
//                
//
//        return $query;
         $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
            $this->db->where('b_level_quatation_tbl.customer_id', $customer_id);
       

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    //--------------------------------------
    //--------------------------------------
    public function get_remarks_by_customer_id($customer_id) {
        $result = $this->db->select('*')->where('customer_id', $customer_id)->get('appointment_calendar')->result();
        return $result;
    }

    //--------------------------------------
//    ========== its for sendLink ===============
    public function sendLink($user_insert_id, $email, $data, $username, $password) {
        $data['baseurl'] = base_url();
        $config = Array(
            'protocol' => $data['get_mail_config'][0]->protocol, //'smtp',
            'smtp_host' => $data['get_mail_config'][0]->smtp_host, //'ssl://smtp.gmail.com',
            'smtp_port' => $data['get_mail_config'][0]->smtp_port, //465,
            'smtp_user' => $data['get_mail_config'][0]->smtp_user, //'khs2010welfare@gmail22.com', // change it to yours
            'smtp_pass' => $data['get_mail_config'][0]->smtp_pass, // 'bvrayygbwwmxnkd111j', // change it to yours
            'mailtype' => $data['get_mail_config'][0]->mailtype, //'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
//        echo '<pre>';        print_r($config);die();
        $data['author_info'] = $this->customer_user_info($user_insert_id);
        $data['username'] = $username;
        $data['password'] = $password;
//        echo $data['author_info']->doctor_name; 
//        echo $data['random_key'];die();
//        echo '<pre>';        print_r($data['author_info']);die();
        $mesg = $this->load->view('b_level/customers/sendCustomer_user_info', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['get_mail_config'][0]->smtp_user, "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMSLINK");
// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
// . "\n\nThanks\nMetallica Gifts");
// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");

        $fp = @fsockopen(base_url(), $data['get_mail_config'][0]->smtp_port, $errno, $errstr);
        if ($fp) {

            $this->email->message($mesg);
            $this->email->send();

        }


    }

//    =========== its for customer_user_info ==============
    public function customer_user_info($user_insert_id) {
        $query = $this->db->select('*')
                        ->from('log_info a')
                        ->join('user_info b', 'b.id = a.user_id', 'left')
                        ->where('a.row_id', $user_insert_id)
                        ->get()->row();
        return $query;
    }

//    ========== its for get_customer_search_result =============
    public function get_customer_search_result($keyword) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $sql = "SELECT a.*
	FROM
	customer_info a
	WHERE (a.company LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                 OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id ORDER BY a.customer_id DESC ";

        //echo $sql;        die();
        $query = $this->db->query($sql);
//        $this->db->select('a.*, CONCAT(" ", first_name,  last_name) as full_name');
//        $this->db->from('customer_info a');
//        if ($keyword) {
//            $this->db->like('a.company', $keyword, 'both');
//            $this->db->or_like('a.first_name', $keyword, 'both');
//            $this->db->or_like('a.phone', $keyword, 'both');
//            $this->db->or_like('a.email', $keyword, 'both');
//            $this->db->where('a.level_id', $level_id);
//        } else {
//            $this->db->where('a.level_id', $level_id);
//        }
//        $query = $this->db->get();
        return $query->result();
    }

//    ========= its for customer_csv_data ================
    public function customer_csv_data($offset,$limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.customer_id, a.first_name, a.last_name, a.phone, a.email, a.company, a.address, a.city, a.state, a.zip_code, a.country_code');
        $this->db->from('customer_info a');
        $this->db->where('a.level_id', $level_id);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function customer_pdf_data($offset,$limit) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.customer_id, a.first_name, a.last_name, a.phone, a.email, a.company, a.address, a.city, a.state, a.zip_code, a.country_code');
        $this->db->from('customer_info a');
        $this->db->where('a.level_id', $level_id);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

//    =========== its for get_top_search_customer_info =============
    public function get_top_search_customer_info($keyword) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $sql = "SELECT a.*
	FROM
	customer_info a
	WHERE (a.company_customer_id LIKE '%$keyword%' OR a.company LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                 OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id";
        $query = $this->db->query($sql);
        return $query->result();
    }
//    ========== its for  get_top_search_order_info ===========
    public function get_top_search_order_info($keyword){
        $this->db->select("a.*, CONCAT(b.first_name,' ',b.last_name) as customer_name");
        $this->db->from('quatation_tbl a');
        $this->db->join('customer_info b', 'b.customer_id = a.customer_id');
        $this->db->like('a.order_id', $keyword);
        $query = $this->db->get();
        return $query->result();
    }

}
