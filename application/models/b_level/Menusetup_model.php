<?php

class Menusetup_model extends CI_model {

    public function checkMenuSetup($menu_name, $module) {

        $query = $this->db->select('*')->from('b_menusetup_tbl')
                        ->where('menu_title', $menu_name)
                        ->where('module', $module)
                        ->get()->result();
        return $query;
    }

    public function menusetup_save($allMenu) {
        $this->db->insert('b_menusetup_tbl', $allMenu);
        $this->session->set_flashdata('success', "<div class='alert alert-success'>Menu save successfully!");
    }

    public function menu_setuplist($offset, $limit) {
        $query = $this->db->select('*')
                        ->from('b_menusetup_tbl')
//                        ->where('status', 1)
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    public function single_menu_edit($id = null) {
        $query = $this->db->select('*')
                        ->from('b_menusetup_tbl')
                        ->where('id', $id)
                        ->get()->row();
        return $query;
    }

//    ========== its for b_level_menu_search ================
    public function b_level_menu_search($keyword) {
        $this->db->select('*');
        $this->db->from('b_menusetup_tbl');
        $this->db->like('menu_title', $keyword, 'both');
        $this->db->or_like('page_url', $keyword, 'both');
        $query = $this->db->get();
        return $query->result();
    }
//    ========= its for b_level_menu_csv_data ================
public function menu_csv_data($offset,$limit) {
    $this->db->select('bms.id, bms.menu_title, bms.korean_name, bms.page_url, bms.module, bms.parent_menu');
    $this->db->from('b_menusetup_tbl  AS bms');
    $this->db->order_by('id', 'desc');
    $this->db->limit($limit, $offset);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        $results = $query->result_array();
        foreach($results AS $key => $row) {
            $results[$key]['menu_title'] = str_replace("_", " ", ucfirst(@$row['menu_title']));
            $results[$key]['korean_name'] = str_replace("_", " ", ucfirst(@$row['korean_name']));
            $parent_menu = $this->db->select('*')->where('id', $row['parent_menu'])->get('b_menusetup_tbl')->row();
            $results[$key]['parent_menu'] = str_replace("_", " ", ucfirst(@$parent_menu->menu_title));
        }
        return $results;
    }
    return false;
}

}
