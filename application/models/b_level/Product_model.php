<?php

class Product_model extends CI_model {

//============ its for get_attribute_type method ==============
    public function get_product_list($per_page = NULL, $page = NULL) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id')
                        ->from('product_tbl a')
                        ->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left')
                        ->join('category_tbl c', 'c.category_id=a.category_id', 'left')
                        ->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left')
                        ->where('a.created_by',$level_id)
                        ->limit($per_page, $page)
                        ->order_by('a.product_id', 'desc')
                        ->get()->result();
        return $query;
    }

    public function get_product_by_id($product_id) {
        $query = $this->db->select('a.*,b.style_name, c.category_name,')
                        ->from('product_tbl a')
                        ->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left')
                        ->join('category_tbl c', 'c.category_id=a.category_id', 'left')
                        ->where('a.product_id', $product_id)
                        ->get()->row();
        return $query;
    }

    public function get_product_attribute_mapping_by_id($product_id) {
        $query = $this->db->select('product_attribute_mapping.*,attribute_tbl.attribute_name')
                        ->join('attribute_tbl', 'attribute_tbl.attribute_id=product_attribute_mapping.attribute_id')
                        ->where('product_attribute_mapping.product_id', $product_id)
                        ->get('product_attribute_mapping')->result();

        $table = '';

        $table .= '<table width="80%" class="border-0" border="0">';
        foreach ($query as $key => $value) {
            $table .= '<tr>
                        <td id="Prod_01" class="border-0"><input type="checkbox" class="checkboxes" name="attribute_id[]" value="' . $value->attribute_id . '" onChange="shuffle_pricebox(' . $value->attribute_id . ');" ' . (!empty($value->price) ? 'checked' : '') . '> </td>
                        <td class="border-0"><b>' . $value->attribute_name . '</b></td>

                        <td class="border-0"><input type="number" name="attribute_price_' . $value->attribute_id . '" id="' . $value->attribute_id . '" value="' . $value->price . '" disabled></td>

                        <td class="border-0">
                            <div><input type="radio" name="price_type_' . $value->attribute_id . '" ' . ($value->price_type == 1 ? 'checked' : '') . ' value="1" checked> $ &nbsp;
                            <input type="radio" name="price_type_' . $value->attribute_id . '" ' . ($value->price_type == 2 ? 'checked' : '') . ' value="2"> %</div>
                        </td>
                    </tr>';
        }
        $table .= '</table>';

        return $table;
    }

//    ============= its for get_product_filter =================
    public function get_product_filter($search) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }


        $this->db->select('a.*, b.style_name, c.category_name, d.pattern_name');
        $this->db->from('product_tbl a');
        $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
        
        if($search->category_name!=NULL){
            $this->db->where('a.category_id', $search->category_name);
        }        
        if($search->status!=NULL){
            $this->db->where('a.active_status', $search->status);
        }        
        if($search->price_style_type!=NULL){
            $this->db->where('a.price_style_type', $search->price_style_type);
        }
        
        // if ($product_name && $category_name && $pattern_model) {

        //     $this->db->like('a.product_name', $product_name, 'both');
        //     $this->db->like('a.category_id', $category_name, 'both');
        //     $this->db->where('a.created_by', $level_id);

        // } elseif ($product_name && $category_name) {

        //     $this->db->like('a.product_name', $product_name, 'both');
        //     $this->db->like('a.category_id', $category_name, 'both');
        //     $this->db->where('a.created_by', $level_id);

        // } elseif ($product_name && $pattern_model) {

        //     $this->db->like('a.product_name', $product_name, 'both');
        //     $this->db->like('a.pattern_models_ids', $pattern_model, 'both');
        //     $this->db->where('a.created_by', $level_id);

        // } elseif ($category_name && $pattern_model) {
        //     $this->db->like('a.category_id', $category_name, 'both');
        //     $this->db->like('a.pattern_models_ids', $pattern_model, 'both');
        //     $this->db->where('a.created_by', $level_id);
        // } elseif ($product_name) {
        //     $this->db->like('a.product_name', $product_name, 'both');
        //     $this->db->where('a.created_by', $level_id);
        // } elseif ($category_name) {
        //     $this->db->like('a.category_id', $category_name, 'both');
        //     $this->db->where('a.created_by', $level_id);
        // } elseif ($pattern_model) {
        //     $this->db->like('a.pattern_models_ids', $pattern_model);
        //     $this->db->where('a.created_by', $level_id);
        // }else{
        //     $this->db->where('a.created_by', $level_id);
        // }

        $this->db->order_by('a.product_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

//    ========= its for  get_only_product for b-level cost factor===========
    public function get_only_product_b_cost_factor() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.product_id, a.product_name, b.dealer_cost_factor, b.individual_cost_factor');
        $this->db->from('product_tbl a');
        $this->db->join('b_cost_factor_tbl b', 'b.product_id = a.product_id', 'left');
        $this->db->where('a.created_by',$level_id);
        $this->db->group_by('a.product_id');
        $query = $this->db->get();
        return $query->result();
    }

////    ========= its for  get_only_product for c-level cost factor===========
//    public function get_only_product_c_cost_factor() {
//        $this->db->select('a.product_id, a.product_name, b.individual_cost_factor, b.costfactor_discount');
//        $this->db->from('product_tbl a');
//        $this->db->join('c_cost_factor_tbl b', 'b.product_id = a.product_id', 'left');
//        $query = $this->db->get();
//        return $query->result();
//    }

    
    //============ its for get_attribute_type method ==============
    public function product_search($keyword) {
        $query = $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id')
                        ->from('product_tbl a')
                        ->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left')
                        ->join('category_tbl c', 'c.category_id=a.category_id', 'left')
                        ->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left')
                        ->like('a.product_name', $keyword, 'both')
                        ->order_by('a.product_id','DESC')
                        ->limit(50)
                        ->get()->result();
        return $query;
    }

    public function get_b_user_product_list($created_by)
    {
        $query = $this->db->select('a.*')
            ->from('product_tbl a')
            ->where('a.created_by',$created_by)
            ->where('a.active_status',1)
            ->order_by('a.product_id', 'desc')
            ->get()->result();


        return $query;
    }

}
