<?php

class Role_model extends CI_model {

//============= its for get_users =================
    public function get_users() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        $this->db->where('user_type','b');
        $this->db->where('created_by!=',0);
        $this->db->where('created_by=',$created_by);
        $this->db->order_by('id','DESC');
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->result();

        } else {

            return false;

        }

    }

    public function create($data = array()) {

        $this->db->where('role_id', $data[0]['role_id'])->delete('b_role_permission_tbl');

        return $this->db->insert_batch('b_role_permission_tbl', $data);

    }

//======== its for role_list =========== 
    public function role_list() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('b_role_tbl')
                        ->where('created_by',$created_by)
                        ->get()->result();
        return $query;
    }

//    ========= its for role_info =============
    public function role_info($id) {

        $query = $this->db->select('*')
                ->from('b_role_tbl r')
                ->join('b_role_permission_tbl rp', 'rp.role_id = r.id')
                ->where('r.id', $id)
                ->get()
                ->row();
        return $query;
    }

//    ============ its for user_access_role ===============
    public function user_access_role() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select('a.role_acc_id, a.user_id,  a.role_id, b.first_name, b.last_name, b.phone, b.email, c.role_name');
        $this->db->from('b_user_access_tbl a');
        $this->db->join('user_info b', 'b.id = a.user_id');
        $this->db->join('b_role_tbl c', 'c.id = a.role_id');
        $this->db->where('b.created_by',$created_by);
        $this->db->order_by('a.role_acc_id', 'desc');
        $this->db->group_by('a.user_id');
        $query = $this->db->get();
        return $query->result();
    }

//    ============ its for  edit_user_access_role ========= 
    public function edit_user_access_role($access_id) {
        $query = $this->db->select('*')
                ->from('b_user_access_tbl a')
                ->where('a.role_acc_id', $access_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

//    ========= its for service_list show ============
    public function user_list() {
        return $this->db->select("*")
                        ->from('c_level_user a')
//                        ->where('user_type', 2)
//                        ->order_by('a.created_b', 'desc')
                        ->get()->result();
    }

}
