<?php

class Stock_model extends CI_model {

//    =========== its for customer_user_info ==============
    public function get_stock_history_filter($material_id, $from_date, $to_date) {
        $date_range = "a.stock_date BETWEEN '$from_date' AND '$to_date'";
        $this->db->select('a.*, b.material_name, c.pattern_name, d.color_name');
        $this->db->from('row_material_stock_tbl a');
        $this->db->join('row_material_tbl b', 'b.id = a.row_material_id');
        $this->db->join('pattern_model_tbl c', 'c.pattern_model_id = a.pattern_model_id', 'left');
        $this->db->join('color_tbl d', 'd.id = a.color_id', 'left');
        $this->db->order_by("a.id", 'desc');
        if ($material_id && $from_date && $to_date) {
            $this->db->where('a.row_material_id', $material_id);
            $this->db->where($date_range);
        }
        if ($from_date && $to_date) {
            $this->db->where($date_range);
        }
        if($material_id){
            $this->db->where('a.row_material_id', $material_id);            
        }
        $query = $this->db->get()->result();
        return $query;
    }

}
