<?php

class Order_model extends CI_model {

    public function smsSend($dataArray){


        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }
        $info = (object)$dataArray;

        $customer_info = $this->db->where('customer_id',$info->customer_id)->get('customer_info')->row();

        if($customer_info->phone!=NULL){

            $this->load->library('twilio');

            $name = @$customer_info->first_name.' '. @$customer_info->last_name;

            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by',$level_id)->where('default_status', 1)->get()->row();
            
            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $customer_info->phone;
            $message = $info->message;
            $response = $this->twilio->sms($from, $to, $message);

            return 1;
        }

    }


    public function send_email($order_id,$order_stage) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }

        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by',$level_id);
        $this->db->limit('1');
        $data['get_mail_config'] =  $this->db->get()->result();

        if($order_stage==1){$stage = 'Quote';}
        if($order_stage==2){$stage = 'Paid';}
        if($order_stage==3){$stage = 'Partially Paid';}
        if($order_stage==4){$stage = 'Manufacturing';}
        if($order_stage==5){$stage = 'Shipping';}
        if($order_stage==6){$stage = 'Cancelled';}
        if($order_stage==7){$stage = 'Delivered';}


        $orderData = $this->db->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();

        $customer_id = $orderData->customer_id;
        

        $config = Array(
            'protocol'  => $data['get_mail_config'][0]->protocol, //'smtp',
            'smtp_host' => $data['get_mail_config'][0]->smtp_host, //'ssl://smtp.gmail.com',
            'smtp_port' => $data['get_mail_config'][0]->smtp_port, //465,
            'smtp_user' => $data['get_mail_config'][0]->smtp_user, //'khs2010welfare@gmail.com', // change it to yours
            'smtp_pass' => $data['get_mail_config'][0]->smtp_pass, // 'bvrayygbwwmxnkdj', // change it to yours
            'mailtype' => $data['get_mail_config'][0]->mailtype, //'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        
        // customer information
        $customer_info = $this->db->where('customer_id',$customer_id)->get('customer_info')->row();

        // Load Html template
        $mesg = 'Order Stage has been updated to <b>'.$stage.'</b> for '.$orderData->order_id;


        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['get_mail_config'][0]->smtp_user, "Support Center");
        $this->email->to($customer_info->email);
        $this->email->subject("Welcome to BMSLINK");

        $fp = @fsockopen(base_url(), $data['get_mail_config'][0]->smtp_port, $errno, $errstr);
        if ($fp) {

            $this->email->message($mesg);
            $this->email->send();

        }

        
        return 1;

        
    }




    //============ its for get_customer method ==============
    public function get_customer() {
        
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        
        $query = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', $level_id)
                        ->order_by('customer_id', 'desc')
                        ->get()->result();
        return $query;
    }
    

//========= its for get category =============
    public function get_category() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('category_tbl')
                        ->where('parent_category', 0)
                        ->where('status', 1)
                        ->where('created_by', $level_id)
                        ->order_by('category_name', 'asc')
                        ->get()->result();
        return $query;
    }

//========= its for get_patern_model =============
    public function get_patern_model() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('pattern_model_tbl')
//                        ->where('parent_category', 0)
                        ->where('status', 1)
                        ->where('created_by', $level_id)
                        ->order_by('pattern_name', 'asc')
                        ->get()->result();
        return $query;
    }

//========= its for get_patern_model =============
    public function get_product() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
                        ->from('product_tbl')
                        ->where('active_status', 1)
                        ->where('created_by', $level_id)
//                        ->where('parent_category', 0)
                        ->order_by('product_name', 'asc')
                        ->get()->result();
        return $query;
    }


    public function count_table_row($search=NULL){

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }

        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){
            
            $this->db->where('b_level_quatation_tbl.order_stage',$search->order_stage);
        }

        $this->db->where('b_level_quatation_tbl.status',1);



        $this->db->where('b_level_quatation_tbl.created_by',$level_id);
        $total = $this->db->count_all('b_level_quatation_tbl');

        return $total;

    }

    public function manufacturing_count_table_row($search=NULL){

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }

        if($search->customer_id!=NULL){
            $this->db->where('manufacturing_b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('manufacturing_b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){

            $this->db->where('manufacturing_b_level_quatation_tbl.order_stage',$search->order_stage);
        }

        $this->db->where('manufacturing_b_level_quatation_tbl.status',1);



        $this->db->where('manufacturing_b_level_quatation_tbl.created_by',$level_id);
        $total = $this->db->count_all('manufacturing_b_level_quatation_tbl');

        return $total;

    }



    public function manufacturing_get_all_orderd($search=NULL,$per_page=NULL,$page=NULL){
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }
        $this->db->select("manufacturing_b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('manufacturing_b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=manufacturing_b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('manufacturing_b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('manufacturing_b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){
            
            $this->db->where('manufacturing_b_level_quatation_tbl.order_stage',$search->order_stage);
        }
      //  $this->db->where('status',1);


        $this->db->where('manufacturing_b_level_quatation_tbl.level_id',$created_by);
        $this->db->limit($per_page, $page);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;

    }

    public function get_all_orderd($search=NULL,$per_page=NULL,$page=NULL){
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');

        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){

            $this->db->where('b_level_quatation_tbl.order_stage',$search->order_stage);
        }
      //  $this->db->where('status',1);


        $this->db->where('b_level_quatation_tbl.level_id',$created_by);
        $this->db->limit($per_page, $page);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;

    }

    public function get_all_orderd_count($search=NULL,$per_page=NULL,$page=NULL){
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');

        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){

            $this->db->where('b_level_quatation_tbl.order_stage',$search->order_stage);
        }
        $this->db->where('status',1);

        $this->db->where('b_level_quatation_tbl.created_by',$created_by);


        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return count($query);

    }


    public function get_all_manufactur_orderd($search=NULL,$per_page=NULL,$page=NULL){

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }
        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){
            
            $this->db->where('b_level_quatation_tbl.order_stage',$search->order_stage);
        }
        $this->db->where('order_stage',4);
        $this->db->where('b_level_quatation_tbl.created_by',$level_id);

        $this->db->limit($per_page, $page);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();



        return $query;

    }



    public function get_orderd_by_id($order_id){

        $query = $this->db->select("b_level_quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
                        ->from('b_level_quatation_tbl')
                        ->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left')
                        ->where('b_level_quatation_tbl.order_id', $order_id)
                        ->get()->row();
        return $query;

    }



    public function get_manufacturing_orderd_by_id($order_id){

        $query = $this->db->select("manufacturing_b_level_quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
            ->from('manufacturing_b_level_quatation_tbl')
            ->join('customer_info','customer_info.customer_id=manufacturing_b_level_quatation_tbl.customer_id','left')
            ->where('manufacturing_b_level_quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;

    }

    public function get_b_to_b_orderd_by_id($order_id){

        $query = $this->db->select("b_to_b_level_quatation_tbl.*,
            CONCAT(user_info.first_name,' ',user_info.last_name) as user_name,
            user_info.phone,
            user_info.address,
            user_info.city,
            user_info.state,
            user_info.zip_code,
            user_info.country_code,
            user_info.id")
            ->from('b_to_b_level_quatation_tbl')
            ->join('user_info','user_info.id=b_to_b_level_quatation_tbl.b_user_id','left')
            ->where('b_to_b_level_quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;

    }

    public function get_b_to_b_orderd_details_by_id($order_id){

        $query = $this->db->select("b_to_b_level_qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
            b_to_b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")

            ->from('b_to_b_level_qutation_details')
            ->join('product_tbl','product_tbl.product_id=b_to_b_level_qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=b_to_b_level_qutation_details.category_id','left')
            ->join('b_to_b_level_quatation_attributes','b_to_b_level_quatation_attributes.fk_od_id=b_to_b_level_qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=b_to_b_level_qutation_details.pattern_model_id','left')

            ->join('color_tbl','color_tbl.id=b_to_b_level_qutation_details.color_id','left')

            ->where('b_to_b_level_qutation_details.order_id', $order_id)

            ->get()->result();

        // echo "<pre>";
        // print_r($query); exit;

        return $query;

    }

    public function get_b_to_b_all_orderd($search=NULL,$per_page=NULL,$page=NULL,$status = ''){
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }
        $this->db->select("b_to_b_level_quatation_tbl.*,CONCAT(user_info.first_name,' ',user_info.last_name) as b_user_name");
        $this->db->from('b_to_b_level_quatation_tbl');
        $this->db->join('user_info','user_info.id=b_to_b_level_quatation_tbl.created_by','left');
/*
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }*/
        if($search->order_date!=NULL){
            $this->db->where('b_to_b_level_quatation_tbl.order_date',$search->order_date);
        }
        if($search->order_stage!=NULL){

            $this->db->where('b_to_b_level_quatation_tbl.order_stage',$search->order_stage);
        }
        $this->db->where('status',1);

        if(!empty($status) && $status == 'FROM_B_USER')
        {
            $this->db->where('b_to_b_level_quatation_tbl.b_user_id',$created_by);
        }
        else{
            $this->db->where('b_to_b_level_quatation_tbl.created_by',$created_by);
        }


        $this->db->limit($per_page, $page);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;

    }




    public function get_orderd_details_by_id($order_id){

        $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
            b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")

            ->from('b_level_qutation_details')
            ->join('product_tbl','product_tbl.product_id=b_level_qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=b_level_qutation_details.category_id','left')
            ->join('b_level_quatation_attributes','b_level_quatation_attributes.fk_od_id=b_level_qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id','left')

            ->join('color_tbl','color_tbl.id=b_level_qutation_details.color_id','left')

            ->where('b_level_qutation_details.order_id', $order_id)

            ->get()->result();

        // echo "<pre>";
        // print_r($query); exit;

        return $query;

    }

    public function get_manufacturing_orderd_details_by_id($order_id){

        $query = $this->db->select("manufacturing_b_level_qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
           manufacturing_b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")

            ->from('manufacturing_b_level_qutation_details')
            ->join('product_tbl','product_tbl.product_id=manufacturing_b_level_qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=manufacturing_b_level_qutation_details.category_id','left')
            ->join('manufacturing_b_level_quatation_attributes','manufacturing_b_level_quatation_attributes.fk_od_id=manufacturing_b_level_qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=manufacturing_b_level_qutation_details.pattern_model_id','left')

            ->join('color_tbl','color_tbl.id=manufacturing_b_level_qutation_details.color_id','left')

            ->where('manufacturing_b_level_qutation_details.order_id', $order_id)

            ->get()->result();

        // echo "<pre>";
        // print_r($query); exit;

        return $query;

    }


    public function get_quote_orderd($search=NULL)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        
        $this->db->where('b_level_quatation_tbl.created_by',$created_by);
        $this->db->where('b_level_quatation_tbl.order_stage',1);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
        
    }


    public function get_paid_orderd($search=NULL)
    {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        
        $this->db->where('b_level_quatation_tbl.created_by',$created_by);
        $this->db->where('b_level_quatation_tbl.order_stage',2);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
        
    }    




    public function get_partially_paid_orderd($search=NULL)
    {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        
        $this->db->where('b_level_quatation_tbl.order_stage',3);
        $this->db->where('b_level_quatation_tbl.created_by',$created_by);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
        
    } 



    public function get_manufactur_orderd($search=NULL){

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }
        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }

        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        
        $this->db->where('b_level_quatation_tbl.order_stage',4);
        $this->db->where('b_level_quatation_tbl.created_by',$created_by);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
        
    } 




    public function get_shipping_orderd($search=NULL){

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }
        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        
        $this->db->where('b_level_quatation_tbl.order_stage',5);
        $this->db->where('b_level_quatation_tbl.created_by',$created_by);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
        
    } 



    public function get_cancelled_orderd($search=NULL){

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        
        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        
        if($search->customer_id!=NULL){
            $this->db->where('b_level_quatation_tbl.customer_id',$search->customer_id);
        }

        if($search->order_date!=NULL){
            $this->db->where('b_level_quatation_tbl.order_date',$search->order_date);
        }
        
        $this->db->where('b_level_quatation_tbl.order_stage',6);
        $this->db->where('b_level_quatation_tbl.created_by',$created_by);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
        
    } 




}
