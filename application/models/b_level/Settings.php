<?php

class Settings extends CI_model {

    function get_userlist($limit, $start)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        $this->db->where('user_type', 'b');
        $this->db->where('created_by!=', '');
        $this->db->where('created_by',$created_by);
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->result();
        } else {

            return false;
        }
    }

    function get_user_by_id($user_id) {
        $this->db->select("*");
        $this->db->from('user_info a');
        $this->db->join('log_info b', 'b.user_id = a.id');
        $this->db->where('a.user_type', 'b');
        $this->db->where('a.created_by!=', '');
        $this->db->where('a.id', $user_id);

        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->row();
        } else {

            return false;
        }
    }

//    ============= its for my_account ==========
    public function my_account() {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('user_info a');
        $this->db->where('a.id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

//    ============= its for company_profile ================
    public function company_profile() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }



        $query = $this->db->select('*')
                ->from('company_profile')
                ->where('user_id', $level_id)
                ->get();


        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function single_company_profile($level_id) {

        $query = $this->db->select('*')
                ->from('company_profile')
                ->where('user_id', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

//    =========== its for uom_list ==============
    public function uom_list($offset = null, $limit = null) {
//        if ($this->session->userdata('isAdmin') == 1) {
//            $level_id = $this->session->userdata('user_id');
//        } else {
//            $level_id = $this->session->userdata('admin_created_by');
//        }
        $query = $this->db->select("*")
                        ->from('unit_of_measurement')
//                        ->where('level_id', $level_id)
                        ->order_by('uom_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    ============ its for get uom list =============
    public function get_uom_list() {
        $query = $this->db->select("*")
                        ->from('unit_of_measurement')
                        ->order_by('uom_name', 'Asc')
                        ->get()->result();
        return $query;
    }

//============ its for uom_edit =============
    public function uom_edit($id) {
        $query = $this->db->select('a.*')
                ->from('unit_of_measurement a')
                ->where('a.uom_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ================= its for get_mail_config ==============
    public function get_mail_config() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by',$created_by);
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

//=========== its for web_setting ===============
    public function gateway_list($offset, $limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('gateway_tbl')
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->where('created_by', $created_by)
                        ->get()->result();
        return $query;
    }

//==================== its for gateway_edit ==============
    public function gateway_edit($id) {
        $query = $this->db->select('*')
                ->from('gateway_tbl a')
                ->where('a.id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ============ its for get_sms_config ================
    public function get_sms_config() {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('created_by',$created_by);
        $this->db->order_by('a.gateway_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

//    =============== its for sms_config_edit ==============
    public function sms_config_edit($gateway_id) {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('a.gateway_id', $gateway_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//==================== its for us_state_edit ==============
    public function us_state_edit($id) {
        $query = $this->db->select('*')
                ->from('us_state_tbl a')
                ->where('a.state_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    =========== its for uom_list ==============
    public function us_state_list($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.created_by=" . $user_id;
        }
        $query = $this->db->select("*")
                        ->from('us_state_tbl a')
                        ->where($filter)
                        ->order_by('a.state_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    //    ========== its for get_us_state_search_result =============
    public function get_us_state_search_result($keyword) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $sql = "SELECT a.*
	FROM
	customer_info a
	WHERE (a.company LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                 OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id";
//        echo $sql;        die();
        $query = $this->db->query($sql);
//        $this->db->select('a.*, CONCAT(" ", first_name,  last_name) as full_name');
//        $this->db->from('customer_info a');
//        if ($keyword) {
//            $this->db->like('a.company', $keyword, 'both');
//            $this->db->or_like('a.first_name', $keyword, 'both');
//            $this->db->or_like('a.phone', $keyword, 'both');
//            $this->db->or_like('a.email', $keyword, 'both');
//            $this->db->where('a.level_id', $level_id);
//        } else {
//            $this->db->where('a.level_id', $level_id);
//        }
//        $query = $this->db->get();
        return $query->result();
    }

//    ============= its for user_filter ============
    public function user_filter($first_name, $email, $type) {
        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        if ($first_name && $email && $type) {
            $this->db->like('first_name', $first_name, 'both');
            $this->db->like('email', $email, 'both');
            $this->db->like('user_type', $type, 'both');
        }
        if ($first_name) {
            $this->db->like('first_name', $first_name, 'both');
            $this->db->where('created_by!=', '');
        }
        if ($email) {
            $this->db->like('email', $email, 'both');
            $this->db->where('created_by!=', '');
        }
        if ($type) {
            $this->db->like('user_type', $type, 'both');
            $this->db->where('created_by!=', '');
        }
        $this->db->where('created_by!=', '');
        $this->db->where('user_type', 'b');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

//=========== its for b level color_filter ===============
    public function color_filter($colorname, $colornumber) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('*');
        $this->db->from('color_tbl a');
        if ($colorname && $colornumber) {
            $this->db->like('a.color_name', $colorname, 'both');
            $this->db->like('a.color_number', $colornumber, 'both');
        } elseif ($colorname) {
            $this->db->like('a.color_name', $colorname, 'both');
        } elseif ($colornumber) {
            $this->db->like('a.color_number', $colornumber, 'both');
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function access_logs($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.user_name=" . $user_id;
        }
        $query = $this->db->select('a.*, CONCAT( b.first_name, " ", b.last_name) AS name')
                        ->from('accesslog a')
                        ->join('user_info b', 'b.id = user_name')
//                        ->where('user_name', $level_id)
                        ->where($filter)
                        ->order_by('entry_date', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

}
