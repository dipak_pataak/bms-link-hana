<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Return_model extends CI_model {

    //============= its for get_raw_material_supplier_return list ==============
    public function get_raw_material_supplier_return($offset, $limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('a.*, c.supplier_id, c.supplier_name, CONCAT(d.first_name , " " , d.last_name) as name, CONCAT(e.first_name , " " , e.last_name) as approve_name')
                        ->from('raw_material_return_tbl a')
                        ->join('purchase_tbl b', 'b.purchase_id = a.purchase_id')
                        ->join('supplier_tbl c', 'c.supplier_id = b.supplier_id')
                        ->join('user_info d', 'd.id = a.returned_by', 'left')
                        ->join('user_info e', 'e.id = a.approved_by', 'left')
                        ->where('a.created_by', $level_id)
                        ->order_by('a.return_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    ================== its for  supplier_return_filter ===============
    public function supplier_return_filter($invoice_no, $supplier_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, c.supplier_id, c.supplier_name, CONCAT(d.first_name , " " , d.last_name) as name, CONCAT(e.first_name , " " , e.last_name) as approve_name');
        $this->db->from('raw_material_return_tbl a');
        $this->db->join('purchase_tbl b', 'b.purchase_id = a.purchase_id');
        $this->db->join('supplier_tbl c', 'c.supplier_id = b.supplier_id');
        $this->db->join('user_info d', 'd.id = a.returned_by', 'left');
        $this->db->join('user_info e', 'e.id = a.approved_by', 'left');
        if ($invoice_no && $supplier_name) {
            $this->db->like('b.purchase_id', $invoice_no);
            $this->db->like('b.supplier_id', $supplier_name);
            $this->db->where('a.created_by', $level_id);
        }
        if ($invoice_no) {
            $this->db->like('b.purchase_id', $invoice_no);
            $this->db->where('a.created_by', $level_id);
        }
        if ($supplier_name) {
            $this->db->like('b.supplier_id', $supplier_name);
            $this->db->where('a.created_by', $level_id);
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.return_id', 'desc');
        $query = $this->db->get()->result();
        return $query;
    }

    //============ its for customer_list method ==============
    public function customer_return($offset = null, $limit = null) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name, b.created_by as c_u_id, b.side_mark, b.company')
                        ->from('order_return_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        //->join('company_profile c', 'c.user_id = b.c_u_id')
                        ->where('a.level_id', $level_id)
//                        ->where('a.order_stage', 7)
                        ->order_by('a.return_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();

        return $query;

    }


    
//    =========== its for get_order_info ==========
    public function get_order_info($order_id){
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name')
                        ->from('b_level_quatation_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        ->where('a.order_id', $order_id)
                        ->get()->row();
        return $query;
    }

//    =========== its for get_order_details ==========
    public function get_order_details($order_id){
        $query = $this->db->select('a.*, b.product_name')
                ->from('b_level_qutation_details a')
                ->join('product_tbl b', 'b.product_id = a.product_id')
                ->where('a.order_id', $order_id)
                ->get()->result();
        return $query;
    }

}
