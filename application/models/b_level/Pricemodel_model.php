<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricemodel_model extends CI_model {

//    ========== its for price_manage_filter ============
    public function price_manage_filter($price_sheet_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('*');
        $this->db->from('row_column a');
        $this->db->join('product_tbl b', 'b.price_rowcol_style_id =  a.style_id', 'left');
        if ($price_sheet_name) {
            $this->db->like('a.style_name', $price_sheet_name, 'both');
        } else {
            $this->db->where('a.create_by', $level_id);
        }
        $this->db->order_by('a.style_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

//    ========== its for group_manage_filter ============
    public function group_manage_filter($price_sheet_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, a.active_status as status, b.product_name, b.active_status');
        $this->db->from('row_column a');
        $this->db->join('product_tbl b', 'b.price_rowcol_style_id=a.style_id', 'left');
         if ($price_sheet_name) {
            $this->db->like('a.style_name', $price_sheet_name, 'both');
        } else {
            $this->db->where('a.create_by', $level_id);
        }
        $this->db->where('a.style_type', 4);
        $this->db->order_by('a.style_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

}
