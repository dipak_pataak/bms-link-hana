<?php

class Purchase_model extends CI_model {

//============ its for get_attribute_type method ==============
    public function get_row_material_list() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('row_material_tbl.*,color_tbl.color_name,pattern_model_tbl.pattern_name')
                        ->join('color_tbl', 'color_tbl.id=row_material_tbl.color_id', 'left')
                        ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=row_material_tbl.pattern_model_id', 'left')
                        ->where('row_material_tbl.created_by',$level_id)
                        ->get('row_material_tbl')->result();

        return $query;
    }

    public function get_purchase_list() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')->from('purchase_tbl a')
                ->join('supplier_tbl b', 'b.supplier_id = a.supplier_id', 'left')
                ->where('a.created_by',$level_id)
                ->order_by('a.id', 'desc')->get()->result();
        return $query;
    }

    public function get_purchase_by_id($purchase_id) {
        $query = $this->db->select('purchase_tbl.*,supplier_tbl.supplier_name')
                        ->from('purchase_tbl')
                        ->join('supplier_tbl', 'supplier_tbl.supplier_id=purchase_tbl.supplier_id')
                        ->where('purchase_id', $purchase_id)
                        ->get()->row();
        return $query;
    }

    public function get_purchase_details_by_id($purchase_id) {
        $query = $this->db->select('purchase_details_tbl.*,
            row_material_tbl.material_name,
            color_tbl.color_name,
            pattern_model_tbl.pattern_name')
                        ->from('purchase_details_tbl')
                        ->join('row_material_tbl', 'row_material_tbl.id=purchase_details_tbl.material_id', 'left')
                        ->join('color_tbl', 'color_tbl.id=purchase_details_tbl.color_id', 'left')
                        ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=purchase_details_tbl.pattern_model_id')
                        ->where('purchase_details_tbl.purchase_id', $purchase_id)
                        ->get()->result();
        return $query;
    }
// ========== its for get_material_wise_color_info =================
    public function get_material_wise_color_info($material_id) {
        $this->db->select('b.id, b.color_name, b.color_number');
        $this->db->from('raw_material_color_mapping_tbl a');
        $this->db->join('color_tbl b', 'b.id = a.color_id');
        $this->db->where('a.raw_material_id', $material_id);
        $get_material_wise_color_info = $this->db->get()->result();
        return $get_material_wise_color_info;

//        $result = array(
//            'get_material_wise_color_info' => $get_material_wise_color_info,
//            'get_material_wise_pattern_info' => $get_material_wise_pattern_info,
//        );
//        return $result;
    }

//    ================ its for get_material_wise_pattern_info ==============
    public function get_material_wise_pattern_info($material_id) {
        $this->db->select('a.pattern_model_id, a.pattern_type, pattern_name');
        $this->db->from('pattern_model_tbl a');
        $this->db->join('row_material_tbl b', 'b.pattern_model_id = a.pattern_model_id');
        $this->db->where('b.id', $material_id);
        $get_material_wise_pattern_info = $this->db->get()->result();
        return $get_material_wise_pattern_info;
    }
//    ============= its for get material color and pattern wise available qnt info =============
    public function get_material_color_pattern_wise_available_qnt($raw_material_id, $color_id, $pattern_id){
        $this->db->select('(SUM(a.in_qty)-sum(a.out_qty))  as available_quantity');
        $this->db->from('row_material_stock_tbl a');
        $this->db->where('a.row_material_id', $raw_material_id);
        $this->db->where('a.color_id', $color_id);
        $this->db->where('a.pattern_model_id', $pattern_id);
        $query = $this->db->get()->row();
        return $query;
    }

}
