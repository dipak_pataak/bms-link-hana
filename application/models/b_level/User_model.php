<?php

class User_model extends CI_model {

//============= its for get_users =================
    public function get_users() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        dd($level_id);
        $query = $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name")
                        ->from('user_info a')
                        ->join('log_info b', 'b.user_id = a.id')
                        ->where('a.created_by', $level_id)
//                        ->where('is_admin', 2)
                        ->order_by('a.id', 'desc')
                        ->get()->result();
        return $query;
    }

//    ========== its for sendLink ===============
    public function sendLink($user_id, $data, $email, $password) {
        $data['baseurl'] = base_url();
        $config = Array(
            'protocol' => $data['get_mail_config'][0]->protocol, //'smtp',
            'smtp_host' => $data['get_mail_config'][0]->smtp_host, //'ssl://smtp.gmail.com',
            'smtp_port' => $data['get_mail_config'][0]->smtp_port, //465,
            'smtp_user' => $data['get_mail_config'][0]->smtp_user, //'khs2010welfare@gmail.com', // change it to yours
            'smtp_pass' => $data['get_mail_config'][0]->smtp_pass, // 'bvrayygbwwmxnkdj', // change it to yours
            'mailtype' => $data['get_mail_config'][0]->mailtype, //'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
//        echo '<pre>';        print_r($config);die();
        $data['author_info'] = $this->b_user_info($user_id);
        $data['username'] = $email;
        $data['password'] = $password;
//        echo $data['author_info']->doctor_name; 
//        echo $data['random_key'];die();
//        echo '<pre>';        print_r($data['author_info']);die();
        $mesg = $this->load->view('b_level/settings/send_user_info', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['get_mail_config'][0]->smtp_user, "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMSLINK");
// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
// . "\n\nThanks\nMetallica Gifts");
// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");

        $fp = @fsockopen(base_url(), $data['get_mail_config'][0]->smtp_port, $errno, $errstr);
        if ($fp) {

            $this->email->message($mesg);
            $this->email->send();

        }
    }

//    =========== its for customer_user_info ==============
    public function b_user_info($user_id) {
        $query = $this->db->select('*')
                        ->from('log_info a')
                        ->join('user_info b', 'b.id = a.user_id', 'left')
                        ->where('a.row_id', $user_id)
                        ->get()->row();
        return $query;
    }

    public function b_user_list($offset = null, $limit = null) {

        // print_r($this->session->userdata('isAdmin') );die();
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name,b.status AS request_status,b.request_id")
            ->from('user_info')
            ->join('b_user_catalog_request b','b.b_user_id = user_info.id','left')
            ->where('user_info.id !=', $level_id)
           ->where('user_info.created_by', '')
            ->order_by('user_info.id', 'desc')
            ->limit($offset, $limit)
            ->get()->result();

        return $query;
    }

}
