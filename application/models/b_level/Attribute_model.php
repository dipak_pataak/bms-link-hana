<?php

class Attribute_model extends CI_model {

//============ its for get_attribute_type method ==============
    public function get_attribute_type() {
        $query = $this->db->select('*')
                        ->from('attribute_type_tbl')
                        ->get()->result();
        return $query;
    }

    //============ its for customer_list method ==============
    public function attribute_list($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select("attribute_tbl.*,category_tbl.category_name")
                        ->from('attribute_tbl')
                        ->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left')
                        ->where('attribute_tbl.created_by',$level_id)
                        ->order_by('attribute_tbl.position', 'asc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    //    =========== its for attribute_edit ==============
    public function attribute_edit($id) {
        $query = $this->db->select('*')
                ->from('attribute_tbl')
                ->where('attribute_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //============ its for attribute_type_list method ==============
    public function attribute_type_list($offset = null, $limit = null) {
//        if ($this->session->userdata('isAdmin') == 1) {
//            $level_id = $this->session->userdata('user_id');
//        } else {
//            $level_id = $this->session->userdata('admin_created_by');
//        }
        $query = $this->db->select("*")
                        ->from('attribute_type_tbl')
//                        ->where('level_id', $level_id)
                        ->order_by('attribute_type_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    =============its for attribute_type_edit ===============
    public function attribute_type_edit($id) {
        $query = $this->db->select('a.*')
                ->from('attribute_type_tbl a')
                ->where('a.attribute_type_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_attribute_types() {
        $query = $this->db->select("*")
                        ->from('attribute_type_tbl')
                        ->order_by('attribute_type_id', 'ASC')
                        ->get()->result();
        return $query;
    }

    public function get_attr_by_attr_types() {

        $query = $this->db->select("*")
                        ->from('attribute_type_tbl')
                        ->order_by('attribute_type_id', 'ASC')
                        ->get()->result();
        return $query;
    }

//    ============ its for attribute_filter ==============
    public function attribute_filter($attribute_name, $category_name) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('user_created_by');
        }

        $this->db->select("a.*,b.category_name");
        $this->db->from('attribute_tbl a');
        $this->db->join('category_tbl b', 'b.category_id=a.category_id', 'left');

        if ($attribute_name && $category_name) {
            $this->db->like('a.attribute_name', $attribute_name, 'both');
            $this->db->like('a.category_id', $category_name, 'both');
        } elseif ($attribute_name) {
            $this->db->like('a.attribute_name', $attribute_name, 'both');
        } elseif ($category_name) {
            $this->db->like('a.category_id', $category_name, 'both');
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.position', 'ASC');
        $query = $this->db->get()->result();
        return $query;
    }

//    ========= its for attribut onkeyup search ==========
    public function attribute_onkeysearch($keyword) {
        
        $query = $this->db->select("attribute_tbl.*,category_tbl.category_name")
                        ->from('attribute_tbl')
                        ->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left')
                        ->like('attribute_name', $keyword, 'both')
                        ->order_by('attribute_tbl.position', 'ASC')
                        ->get()->result();
        return $query;
    }

}
