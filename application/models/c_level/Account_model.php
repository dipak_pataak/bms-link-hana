<?php

class Account_model extends CI_model {

    function get_userlist() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }


        $this->db->select('*');
        $this->db->from('acc_coa');
        $this->db->where('level_id', $level_id);
        $this->db->where('IsActive', 1);
        $this->db->order_by('HeadName');

        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function dfs($HeadName, $HeadCode, $oResult, $visit, $d) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }


        // echo $HeadName; echo " "; echo $HeadCode; echo " "; echo $oResult; echo " "; echo $visit; echo " "; echo $d; echo '<hr>'; 
        if ($d == 0)
            echo "<li>$HeadName";
        else
            echo "<li><a href='javascript:' onclick=\"loadData('" . $HeadCode . "')\">$HeadName</a>";
        $p = 0;
        for ($i = 0; $i < count($oResult); $i++) {

            if (!$visit[$i]) {
                if ($HeadName == $oResult[$i]->PHeadName) {
                    $visit[$i] = true;
                    if ($p == 0)
                        echo "<ul>";
                    $p++;
                    $this->dfs($oResult[$i]->HeadName, $oResult[$i]->HeadCode, $oResult, $visit, $d + 1);
                }
            }
        }
        if ($p == 0)
            echo "</li>";
        else
            echo "</ul>";
    }

// Accounts list
    public function Transacc($level_id) {
        return $data = $this->db->select("*")
                ->from('acc_coa')
                ->where('IsTransaction', 1)
                ->where('IsActive', 1)
                ->where('level_id', $level_id)
                ->order_by('HeadName')
                ->get()
                ->result();
    }

// Credit Account Head
    public function Cracc($level_id) {
        return $data = $this->db->select("*")
                ->from('acc_coa')
                ->like('HeadCode', 1020102, 'after')
                ->where('IsTransaction', 1)
                ->where('level_id', $level_id)
                ->order_by('HeadName')
                ->get()
                ->result();
    }

    //Generate Voucher No
    public function voNO($level_id) {
        return $data = $this->db->select("Max(VNo) as voucher")
                ->from('acc_transaction')
                ->where('level_id', $level_id)
                ->like('VNo', 'DV-', 'after')
                ->get()
                ->row();
        // print_r($data);exit;
    }

    // Insert Debit voucher 
    public function insert_debitvoucher($level_id) {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "DV";
        $cAID = $this->input->post('cmbDebit');
        $dAID = $this->input->post('txtCode');
        $Debit = $this->input->post('txtAmount');
        $Credit = $this->input->post('grand_total');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');

        $cinsert = array(
            'VNo' => $voucher_no,
            'Vtype' => $Vtype,
            'VDate' => $VDate,
            'COAID' => $cAID,
            'Narration' => $Narration,
            'Debit' => 0,
            'Credit' => $Credit,
//            'StoreID' => $this->session->userdata('store_id'),
            'level_id' => $level_id,
            'IsPosted' => $IsPosted,
            'CreateBy' => $CreateBy,
            'CreateDate' => $createdate,
            'IsAppove' => 0
        );

        $this->db->insert('acc_transaction', $cinsert);
        for ($i = 0; $i < count($dAID); $i++) {
            $dbtid = $dAID[$i];
            $Damnt = $Debit[$i];

            $debitinsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $dbtid,
                'Narration' => $Narration,
                'Debit' => $Damnt,
                'Credit' => 0,
//                'StoreID' => $this->session->userdata('store_id'),
                'level_id' => $level_id,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 0
            );
            // print_r($debitinsert);exit;
            $this->db->insert('acc_transaction', $debitinsert);
        }
        return true;
    }

    // Credit voucher no
    public function crVno($level_id) {
        return $data = $this->db->select("Max(VNo) as voucher")
                ->from('acc_transaction')
                ->where('level_id', $level_id)
                ->like('VNo', 'CV-', 'after')
                ->get()
                ->row();
        // print_r($data);exit;
    }

    // Insert Credit voucher 
    public function insert_creditvoucher($level_id) {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "CV";
        $dAID = $this->input->post('cmbDebit');
        $cAID = $this->input->post('txtCode');
        $Credit = $this->input->post('txtAmount');
        $debit = $this->input->post('grand_total');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');

        $cinsert = array(
            'VNo' => $voucher_no,
            'Vtype' => $Vtype,
            'VDate' => $VDate,
            'COAID' => $dAID,
            'Narration' => $Narration,
            'Debit' => $debit,
            'Credit' => 0,
//      'StoreID'        => $this->session->userdata('store_id'),
            'level_id' => $level_id,
            'IsPosted' => $IsPosted,
            'CreateBy' => $CreateBy,
            'CreateDate' => $createdate,
            'IsAppove' => 0
        );

        $this->db->insert('acc_transaction', $cinsert);
        for ($i = 0; $i < count($cAID); $i++) {
            $crtid = $cAID[$i];
            $Cramnt = $Credit[$i];

            $debitinsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $crtid,
                'Narration' => $Narration,
                'Debit' => 0,
                'Credit' => $Cramnt,
//      'StoreID'        => $this->session->userdata('store_id'),
                'level_id' => $level_id,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 0
            );
            // print_r($debitinsert);exit;
            $this->db->insert('acc_transaction', $debitinsert);
        }
        return true;
    }

// journal voucher
    public function journal($level_id) {
        return $data = $this->db->select("Max(VNo) as voucher")
                ->from('acc_transaction')
                ->where('level_id', $level_id)
                ->like('VNo', 'Journal-', 'after')
                ->get()
                ->row();
        // print_r($data);exit;
    }

// Insert journal voucher 
    public function insert_journalvoucher($level_id) {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "JV";
        $dAID = $this->input->post('cmbDebit');
        $cAID = $this->input->post('txtCode');
        $debit = $this->input->post('txtAmount');
        $credit = $this->input->post('txtAmountcr');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');

//        print_r($debit);        echo '<br>';
//        print_r($credit);        echo '<br>';
        for ($i = 0; $i < count($cAID); $i++) {
//            $crtid = $cAID[$i];
//            $Cramnt = $credit[$i];
//            $debit = $debit[$i];

            $contrainsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $cAID[$i],
                'Narration' => $Narration,
                'Debit' => $debit[$i],
                'Credit' => $credit[$i],
//                'StoreID' => $this->session->userdata('store_id'),
                'level_id' => $level_id,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 0
            );
//            print_r($contrainsert);echo '<br>';
            $this->db->insert('acc_transaction', $contrainsert);
        }
//        exit();
        return true;
    }

    // Contra voucher 

    public function contra($level_id) {
        return $data = $this->db->select("Max(VNo) as voucher")
                ->from('acc_transaction')
                ->where('level_id', $level_id)
                ->like('VNo', 'Contra-', 'after')
                ->get()
                ->row();
        // print_r($data);exit;
    }

// Insert Countra voucher 
    public function insert_contravoucher($level_id) {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "Contra";
        $dAID = $this->input->post('cmbDebit');
        $cAID = $this->input->post('txtCode');
        $debit = $this->input->post('txtAmount');
//        $grand_debit = $this->input->post('grand_total');
        $credit = $this->input->post('txtAmountcr');
//        $grand_credit = $this->input->post('grand_total1');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');

//        print_r($debit);
//        echo "<br>";
//        print_r($credit);
//        echo "<br>";
        for ($i = 0; $i < count($cAID); $i++) {
            $contrainsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $cAID[$i],
                'Narration' => $Narration,
                'Debit' => $debit[$i],
                'Credit' => $credit[$i],
//                'StoreID' => $this->session->userdata('store_id'),
                'level_id' => $level_id,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 0
            );
//             print_r($contrainsert); echo '<br>';
            $this->db->insert('acc_transaction', $contrainsert);
        }
//        for ($i = 0; $i < count($cAID); $i++) {
//            echo $i;
//            $crtid = $cAID[$i];
//            $Cramnt = $credit[$i];
//            $debit = $debit[$i];
//            echo $Cramnt; echo '<br>'; echo $debit;
//            $contrainsert = array(
//                'VNo' => $voucher_no,
//                'Vtype' => $Vtype,
//                'VDate' => $VDate,
//                'COAID' => $crtid,
//                'Narration' => $Narration,
//                'Debit' => $debit,
//                'Credit' => $Cramnt,
////                'StoreID' => $this->session->userdata('store_id'),
//                'IsPosted' => $IsPosted,
//                'CreateBy' => $CreateBy,
//                'CreateDate' => $createdate,
//                'IsAppove' => 0
//            );
//            print_r($contrainsert);
////            $this->db->insert('acc_transaction', $contrainsert);
//        }
//        exit();
        return true;
    }

    // voucher Aprove 
    public function approve_voucher() {
        $values = array("DV", "CV", "JV", "Contra");

        return $approveinfo = $this->db->select('*')
                ->from('acc_transaction')
                ->where_in('Vtype', $values)
                ->where('IsAppove', 0)
                ->group_by('VNo')
                ->get()
                ->result();
        // print_r($approveinfo);exit;
    }

    //debit update voucher
    public function dbvoucher_updata($id) {
        return $vou_info = $this->db->select('*')
                ->from('acc_transaction')
                ->where('VNo', $id)
                ->where('Credit <', 1)
                ->get()
                ->result();
        // print_r($vou_info);exit;
    }

    // debit update voucher credit info
    public function crvoucher_updata($id) {
        return $v_info = $this->db->select('*')
                ->from('acc_transaction')
                ->where('VNo', $id)
                ->where('Debit<', 1)
                ->get()
                ->row();
        //print_r($v_info);exit;
    }

// Update debit voucher
    public function update_debitvoucher($level_id) {
        $voucher_no = $this->input->post('txtVNo');
        $Vtype = "DV";
        $cAID = $this->input->post('cmbDebit');
        $dAID = $this->input->post('txtCode');
        $Debit = $this->input->post('txtAmount');
        $Credit = $this->input->post('grand_total');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');

        $cinsert = array(
            'VNo' => $voucher_no,
            'Vtype' => $Vtype,
            'VDate' => $VDate,
            'COAID' => $cAID,
            'Narration' => $Narration,
            'Debit' => 0,
            'Credit' => $Credit,
//            'StoreID' => $this->session->userdata('store_id'),
            'level_id' => $level_id,
            'IsPosted' => $IsPosted,
            'CreateBy' => $CreateBy,
            'CreateDate' => $createdate,
            'IsAppove' => 0
        );
        $this->db->where('VNo', $voucher_no)
                ->delete('acc_transaction');

        $this->db->insert('acc_transaction', $cinsert);
        for ($i = 0; $i < count($dAID); $i++) {
            $dbtid = $dAID[$i];
            $Damnt = $Debit[$i];

            $debitinsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $dbtid,
                'Narration' => $Narration,
                'Debit' => $Damnt,
                'Credit' => 0,
//                'StoreID' => $this->session->userdata('store_id'),
                'level_id' => $level_id,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 0
            );
            // print_r($debitinsert);exit;
            $this->db->insert('acc_transaction', $debitinsert);
        }
        return true;
    }

    //credit voucher update 
    public function crdtvoucher_updata($id) {
        return $vou_info = $this->db->select('*')
                ->from('acc_transaction')
                ->where('VNo', $id)
                ->where('Debit <', 1)
                ->get()
                ->result();
        // print_r($vou_info);exit;
    }

    //Debit voucher inof
    //credit voucher update 
    public function debitvoucher_updata($id) {
        return $cr_info = $this->db->select('*')
                ->from('acc_transaction')
                ->where('VNo', $id)
                ->where('Credit<', 1)
                ->get()
                ->row();
        // print_r($vou_info);exit;
    }

    // update Credit voucher
    public function update_creditvoucher($level_id) {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "CV";
        $dAID = $this->input->post('cmbDebit');
        $cAID = $this->input->post('txtCode');
        $Credit = $this->input->post('txtAmount');
        $debit = $this->input->post('grand_total');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');

        $cinsert = array(
            'VNo' => $voucher_no,
            'Vtype' => $Vtype,
            'VDate' => $VDate,
            'COAID' => $dAID,
            'Narration' => $Narration,
            'Debit' => $debit,
            'Credit' => 0,
//            'StoreID' => $this->session->userdata('store_id'),
            'level_id' => $level_id,
            'IsPosted' => $IsPosted,
            'CreateBy' => $CreateBy,
            'CreateDate' => $createdate,
            'IsAppove' => 0
        );
        $this->db->where('VNo', $voucher_no)
                ->delete('acc_transaction');

        $this->db->insert('acc_transaction', $cinsert);
        for ($i = 0; $i < count($cAID); $i++) {
            $crtid = $cAID[$i];
            $Cramnt = $Credit[$i];

            $debitinsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $crtid,
                'Narration' => $Narration,
                'Debit' => 0,
                'Credit' => $Cramnt,
//                'StoreID' => $this->session->userdata('store_id'),
                'level_id' => $level_id,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 0
            );
            // print_r($debitinsert);exit;
            $this->db->insert('acc_transaction', $debitinsert);
        }
        return true;
    }

    //contra voucher update 
    public function contraCrebitVoucher_edit($id) {
        return $vou_info = $this->db->select('*')
                ->from('acc_transaction')
                ->where('VNo', $id)
//                ->where('Debit <', 1)
                ->get()
                ->result();
        // print_r($vou_info);exit;
    }

    //contra credit voucher update 
//    public function contraDebitVoucher_edit($id) {
//        return $cr_info = $this->db->select('*')
//                ->from('acc_transaction')
//                ->where('VNo', $id)
////                ->where('Credit<', 1)
//                ->get()
//                ->row();
//        // print_r($vou_info);exit;
//    }
    //journalCrebitVoucher_edit voucher update 
    public function journalCrebitVoucher_edit($id) {
        return $vou_info = $this->db->select('*')
                ->from('acc_transaction')
                ->where('VNo', $id)
//                ->where('Debit <', 1)
                ->get()
                ->result();
        // print_r($vou_info);exit;
    }

//approved
    public function approved($data = []) {
        return $this->db->where('VNo', $data['VNo'])
                        ->update('acc_transaction', $data);
    }

//    ============== its for  account reports ================
    public function get_cash() {
      /*  if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }*/
        $date = date('Y-m-d');
        $sql = "SELECT SUM(Debit) as Amount FROM acc_transaction WHERE VDate='$date' AND COAID ='1020101' AND VType NOT IN ('DV','JV','CV') AND IsAppove='1'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_vouchar() {
        $date = date('Y-m-d');
        $sql = "SELECT VNo, Vtype,VDate, SUM(Debit+Credit)/2 as Amount FROM acc_transaction  WHERE VDate='$date' AND VType IN ('DV','JV','CV') GROUP BY VNO, Vtype, VDate ORDER BY VDate";
        // $sql="SELECT VNo, Vtype,VDate, SUM(Debit+Credit)/2 as Amount FROM acc_transaction where Vdate='$date' AND VType IN('DV','JV','CV') GROUP BY VNo, Vtype, VDate ORDER BY VDate";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function voucher_report_serach($vouchar) {
        $sql = "SELECT SUM(Debit) as Amount FROM acc_transaction WHERE VDate='$vouchar' AND COAID ='1020101' AND VType NOT IN ('DV','JV','CV') AND IsAppove='1'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_vouchar_view($date) {
        $sql = "SELECT acc_income_expence.COAID,SUM(acc_income_expence.Amount) AS Amount, acc_coa.HeadName FROM acc_income_expence INNER JOIN acc_coa ON acc_coa.HeadCode=acc_income_expence.COAID WHERE Date='$date' AND acc_income_expence.IsApprove=1 AND acc_income_expence.Paymode='Cash' GROUP BY acc_income_expence.COAID, acc_coa.HeadName ORDER BY acc_coa.HeadName";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_general_ledger($level_id) {
        $this->db->select('*');
        $this->db->from('acc_coa');
        $this->db->where('IsGL', 1);
        $this->db->where('level_id', $level_id);
        $this->db->order_by('HeadName', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function general_led_get($Headid, $level_id) {
        $sql = "SELECT * FROM acc_coa WHERE HeadCode='$Headid' AND level_id = '$level_id' ";
        $query = $this->db->query($sql);
        $rs = $query->row();

        $sql = "SELECT * FROM acc_coa WHERE IsTransaction=1 AND level_id = '$level_id' AND PHeadName='" . $rs->HeadName . "' ORDER BY HeadName";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function general_led_report_headname($cmbGLCode, $level_id) {
        $this->db->select('*');
        $this->db->from('acc_coa');
        $this->db->where('HeadCode', $cmbGLCode);
        $this->db->where('level_id', $level_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function general_led_report_headname2($cmbGLCode, $cmbCode, $dtpFromDate, $dtpToDate, $chkIsTransction, $level_id) {
        if ($chkIsTransction) {
            //$cmbCode1=$cmbCode;
            $this->db->select('acc_transaction.VNo, acc_transaction.Vtype, acc_transaction.VDate, acc_transaction.Narration, acc_transaction.Debit, acc_transaction.Credit, acc_transaction.IsAppove, acc_transaction.COAID,acc_coa.HeadName, acc_coa.PHeadName, acc_coa.HeadType');
            $this->db->from('acc_transaction');
            $this->db->join('acc_coa', 'acc_transaction.COAID = acc_coa.HeadCode', 'left');
            $this->db->where('acc_transaction.IsAppove', 1);
            $this->db->where('VDate BETWEEN "' . $dtpFromDate . '" and "' . $dtpToDate . '"');
            $this->db->where('acc_transaction.COAID', $cmbCode);
            $this->db->where('acc_transaction.level_id', $level_id);
            //$this->db->like('acc_transaction.COAID',$cmbGLCode,'after');

            $query = $this->db->get();
            return $query->result();
        } else {
            // $cmbCode1=$cmbCode;
            $this->db->select('acc_transaction.COAID,acc_transaction.Debit, acc_transaction.Credit,acc_coa.HeadName,acc_transaction.IsAppove, acc_coa.PHeadName, acc_coa.HeadType');
            $this->db->from('acc_transaction');
            $this->db->join('acc_coa', 'acc_transaction.COAID = acc_coa.HeadCode', 'left');
            $this->db->where('acc_transaction.IsAppove', 1);
            $this->db->where('VDate BETWEEN "' . $dtpFromDate . '" and "' . $dtpToDate . '"');
            $this->db->where('acc_transaction.COAID', $cmbCode);
            //$this->db->like('acc_transaction.COAID',$cmbGLCode,'after');
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function general_led_report_prebalance($cmbCode, $dtpFromDate, $level_id) {
        //$cmbCode1=$cmbCode;
        $this->db->select('sum(acc_transaction.Debit) as predebit, sum(acc_transaction.Credit) as precredit');
        $this->db->from('acc_transaction');
        $this->db->where('acc_transaction.IsAppove', 1);
        $this->db->where('VDate < ', $dtpFromDate);
        $this->db->where('acc_transaction.COAID', $cmbCode);
        $this->db->where('acc_transaction.level_id', $level_id);
        //$this->db->like('acc_transaction.COAID',$cmbGLCode,'after');

        $query = $this->db->get()->row();
        // print_r($query);exit;
        return $balance = $query->predebit - $query->precredit;
    }

    public function profit_loss_serach($level_id) {
        $sql = "SELECT * FROM acc_coa WHERE level_id = $level_id AND acc_coa.HeadType='I'";
        $sql1 = $this->db->query($sql);

        $sql = "SELECT * FROM acc_coa WHERE level_id = $level_id AND acc_coa.HeadType='E'";
        $sql2 = $this->db->query($sql);

        $data = array(
            'oResultAsset' => $sql1->result(),
            'oResultLiability' => $sql2->result(),
        );
        return $data;
    }

    public function trial_balance_report($FromDate, $ToDate, $WithOpening, $level_id) {

        if ($WithOpening)
            $WithOpening = true;
        else
            $WithOpening = false;

        $sql = "SELECT * FROM acc_coa WHERE level_id = $level_id AND IsGL=1 AND IsActive=1 AND HeadType IN ('A','L') ORDER BY HeadCode";
        $oResultTr = $this->db->query($sql);

        $sql = "SELECT * FROM acc_coa WHERE level_id = $level_id AND IsGL=1 AND IsActive=1 AND HeadType IN ('I','E') ORDER BY HeadCode";
        $oResultInEx = $this->db->query($sql);

        $data = array(
            'oResultTr' => $oResultTr->result_array(),
            'oResultInEx' => $oResultInEx->result_array(),
            'WithOpening' => $WithOpening
        );

        return $data;
    }

}
