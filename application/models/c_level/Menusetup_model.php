<?php

class Menusetup_model extends CI_model {

    //    ========= its for checkMenuSetup=======
    public function checkMenuSetup($menu_name, $module) {
        $query = $this->db->select('*')->from('menusetup_tbl')
                        ->where('menu_title', $menu_name)
                        ->where('module', $module)
                        ->get()->result();
        return $query;
    }

//    ========= its for menusetup_save =============
    public function menusetup_save($allMenu) {
        $this->db->insert('menusetup_tbl', $allMenu);
        $this->session->set_flashdata('success', "<div class='alert alert-success'>Menu save successfully!");
    }

    //    ========= its for menu_setuplist ==========
    public function menu_setuplist($offset, $limit) {
        $query = $this->db->select('*')
                        ->from('menusetup_tbl')
//                        ->where('status', 1)
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    public function c_menu_setuplist($offset, $limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
                        ->from('c_menusetup_tbl')
                        ->where('level_id', $level_id)
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
//        echo $this->db->last_query();
        return $query;
    }

    //    =========== its for single_menu_edit ============
    public function single_menu_edit($id = null) {
        $query = $this->db->select('*')
                        ->from('menusetup_tbl')
                        ->where('id', $id)
                        ->get()->row();
        return $query;
    }

    public function c_single_menu_edit($id = null) {
        $query = $this->db->select('*')
                        ->from('c_menusetup_tbl')
                        ->where('id', $id)
                        ->get()->row();
        return $query;
    }

//    ========== its for b_level_menu_search ================
    public function clevel_menu_search($keyword) {
        $likequery = "(menu_title LIKE '%$keyword%' OR page_url LIKE '%$keyword%')";
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        dd($level_id);
        $this->db->select('*');
        $this->db->from('c_menusetup_tbl');
        $this->db->where($likequery);
        $this->db->where('level_id', $level_id);
        $query = $this->db->get();
        return $query->result();
    }

//    ========== its for b_level_menu_search ================
    public function c_level_menu_search($keyword) {
        $this->db->select('*');
        $this->db->from('menusetup_tbl');
        $this->db->like('menu_title', $keyword, 'both');
        $this->db->or_like('page_url', $keyword, 'both');
        $query = $this->db->get();
        return $query->result();
    }

//    ========= its for c_level_menu_csv_data ================
    public function menu_csv_data($offset,$limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('bms.id, bms.menu_title, bms.korean_name, bms.page_url, bms.module, bms.parent_menu');
        $this->db->from('c_menusetup_tbl  AS bms');
        $this->db->where('bms.level_id', $level_id);
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results AS $key => $row) {
                $results[$key]['menu_title'] = str_replace("_", " ", ucfirst(@$row['menu_title']));
                $results[$key]['korean_name'] = str_replace("_", " ", ucfirst(@$row['korean_name']));
                $parent_menu = $this->db->select('*')->where('id', $row['parent_menu'])->get('b_menusetup_tbl')->row();
                $results[$key]['parent_menu'] = str_replace("_", " ", ucfirst(@$parent_menu->menu_title));
            }
            return $results;
        }
        return false;
    }
}
