<?php

class Order_model extends CI_model
{

    public function smsSend($dataArray)
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $info = (object)$dataArray;

        $customer_info = $this->db->where('customer_id', $info->customer_id)->get('customer_info')->row();

        if ($customer_info->phone != NULL)
        {

            $this->load->library('twilio');

            $name = @$customer_info->first_name . ' ' . @$customer_info->last_name;

            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $main_b_id)->where('default_status', 1)->get()->row();

            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $customer_info->phone;
            $message = $info->message;
            $response = $this->twilio->sms($from, $to, $message);

            return 1;
        }
    }

    public function send_link($orderData)
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by', $main_b_id);
        $this->db->limit('1');
        $data['get_mail_config'] = $this->db->get()->result();


        $data['orderData'] = (object)$orderData;
        $data['baseurl'] = base_url();

        $config = Array(
            'protocol' => $data['get_mail_config'][0]->protocol, //'smtp',
            'smtp_host' => $data['get_mail_config'][0]->smtp_host, //'ssl://smtp.gmail.com',
            'smtp_port' => $data['get_mail_config'][0]->smtp_port, //465,
            'smtp_user' => $data['get_mail_config'][0]->smtp_user, //'khs2010welfare@gmail.com', // change it to yours
            'smtp_pass' => $data['get_mail_config'][0]->smtp_pass, // 'bvrayygbwwmxnkdj', // change it to yours
            'mailtype' => $data['get_mail_config'][0]->mailtype, //'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        // customer information
        $data['customer_info'] = $this->db->where('customer_id', $data['orderData']->customer_id)->get('customer_info')->row();

        // company profile
        $data['company_profile'] = $this->db->select('*')
            ->where('user_id', $data['customer_info']->level_id)
            ->get('company_profile')->row();
        // order data
        $data['orderd'] = $this->get_orderd_by_id($data['orderData']->order_id);
        // order details
        $data['order_details'] = $this->get_orderd_details_by_id($data['orderData']->order_id);

        // Load Html template
        $mesg = $this->load->view('c_level/invo', $data, TRUE);


        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['get_mail_config'][0]->smtp_user, "Support Center");
        $this->email->to($data['customer_info']->email);
        $this->email->subject("Welcome to BMSLINK");

        $fp = @fsockopen(base_url(), 465, $errno, $errstr);

        if ($fp)
        {

            $this->email->message($mesg);
            $this->email->send();

        }


        if ($data['customer_info']->phone != NULL)
        {

            $this->load->library('twilio');

            $name = @$data['customer_info']->first_name . ' ' . @$data['customer_info']->last_name;

            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('default_status', 1)->get()->row();
            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $data['customer_info']->phone;
            $message = 'Hi! ' . $name . ' Order Successfully. OrderId ' . @$data['orderData']->order_id;
            $response = $this->twilio->sms($from, $to, $message);
        }

        return 1;
    }

    public function customer_info($log_id)
    {

        $query = $this->db->select('*')
            ->from('customer_info')
            ->where('customer_id', $log_id)
            ->get()->row();
        return $query;
    }

    //============ its for get_customer method ==============
    public function get_customer()
    {


        if ($this->session->userdata('isAdmin') == 1)
        {
            $level_id = $this->session->userdata('user_id');
        } else
        {
            $level_id = $this->session->userdata('admin_created_by');
        }


        $query = $this->db->select('*')
            ->from('customer_info')
            ->where('level_id', $level_id)
            ->order_by('customer_id', 'desc')
            ->get()->result();
        return $query;
    }

//========= its for get category =============
    public function get_category()
    {

        $main_b_id = $this->session->userdata('main_b_id');

        $query = $this->db->select('*')
            ->from('category_tbl')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $main_b_id)
            ->order_by('category_name', 'asc')
            ->get()->result();
        return $query;
    }

    function get_customer_category()
    {

        $main_b_id = $this->session->userdata('main_b_id');

        $query = $this->db->select('a.*')
            ->from('category_tbl a')
            ->where('a.parent_category', 0)
            ->where('a.status', 1)
            ->where('a.created_by', $main_b_id)
            ->get()->result();

        /*$query1 = $this->db->last_query();

        $this->db->select('c.*')
            ->from('b_user_catalog_products a')
            ->join('b_user_catalog_request', 'b_user_catalog_request.request_id=a.request_id')
            ->join('product_tbl b', 'b.product_id=a.product_id')
            ->join('category_tbl c', 'c.category_id=b.category_id')
            ->where('b_user_catalog_request.requested_by', $main_b_id)
            ->where('a.approve_status', 1)
            ->group_by('c.category_id')
            ->get()->result();
        $query2 = $this->db->last_query();
        $query = $this->db->query($query1 . " UNION " . $query2);*/

        return $query;


    }

    function get_customer_product_by_category($category_id)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $query = $this->db->select('product_id,category_id,product_name')
            ->where('category_id', $category_id)
            ->where('active_status', 1)
            ->where('created_by', $main_b_id)
            ->get('product_tbl')->result();


      /*  $query1 = $this->db->last_query();

        $this->db->select('a.product_id,a.category_id,a.product_name')
            ->from('product_tbl a')
            ->join('b_user_catalog_products b', 'b.product_id=a.product_id')
            ->join('b_user_catalog_request', 'b_user_catalog_request.request_id=b.request_id')
            ->where('b_user_catalog_request.requested_by', $main_b_id)
            ->where('a.category_id', $category_id)
            ->where('b.approve_status', 1)
            ->group_by('a.product_id')
            ->get()->result();
        $query2 = $this->db->last_query();
        $query = $this->db->query($query1 . " UNION " . $query2);*/

        return $query;
    }

//========= its for get_patern_model =============
    public function get_patern_model()
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $query = $this->db->select('*')
            ->from('pattern_model_tbl')
            ->where('status', 1)
            ->where('created_by', $main_b_id)
//                        ->where('parent_category', 0)
            ->order_by('pattern_name', 'asc')
            ->get()->result();
        return $query;
    }

//========= its for get_patern_model =============

    public function get_product()
    {

        $query = $this->db->select('*')
            ->from('product_tbl')
            ->where('parent_category', 0)
            ->where('active_status', 1)
            ->order_by('product_name', 'asc')
            ->get()->result();
        return $query;
    }

    public function get_all_cancel_orderd($search = NULL)
    {
        if ($this->session->userdata('isAdmin') == 1)
        {
            $level_id = $this->session->userdata('user_id');
        } else
        {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }


        if ($search->order_id != NULL)
        {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }
        $this->db->where('quatation_tbl.level_id', $level_id);

        $this->db->where('quatation_tbl.order_stage', 6);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_all_invoice_orderd($search = NULL, $offset = NULL, $limit = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }
        if ($search->order_stage != NULL)
        {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_id != NULL)
        {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }

        $this->db->where('quatation_tbl.created_by', $this->session->userdata('user_id'));

        $this->db->where('quatation_tbl.order_stage', 2);

        $this->db->limit($offset, $limit);


        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }


    public function get_all_orderd($search = NULL, $offset = NULL, $limit = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }
        if ($search->order_stage != NULL)
        {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_date != NULL)
        {

            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        if (@$search->user_id != NULL)
        {

            $this->db->where('quatation_tbl.created_by', $search->user_id);
        }

        if (@$search->level_id != NULL)
        {

            $this->db->where('quatation_tbl.level_id', $search->level_id);
        }

        if (@$search->order_id != NULL)
        {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }

        if (@$search->synk_status != NULL)
        {

            $this->db->where('quatation_tbl.synk_status', $search->synk_status);
        }

        $this->db->where('quatation_tbl.order_stage!=', 6);

        $this->db->limit($offset, $limit);


        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }


    public function order_row_count($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');
        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }
        if ($search->order_stage != NULL)
        {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_date != NULL)
        {

            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        if (@$search->user_id != NULL)
        {

            $this->db->where('quatation_tbl.created_by', $search->user_id);
        }
        if (@$search->level_id != NULL)
        {

            $this->db->where('quatation_tbl.level_id', $search->level_id);
        }
        if (@$search->order_id != NULL)
        {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }
        if (@$search->synk_status != NULL)
        {

            $this->db->where('quatation_tbl.synk_status', $search->synk_status);
        }
        $this->db->where('quatation_tbl.order_stage!=', 6);
        $total = $this->db->get()->num_rows();
        return $total;
    }


    public function get_orderd_by_id($order_id)
    {

        $query = $this->db->select("quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.email,
            customer_info.customer_id")
            ->from('quatation_tbl')
            ->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left')
            ->where('quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;

    }


    public function get_orderd_details_by_id($order_id)
    {

        $query = $this->db->select("qutation_details.*,
            product_tbl.product_name,
            quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
            ->from('qutation_details')
            ->join('product_tbl', 'product_tbl.product_id=qutation_details.product_id', 'left')
            ->join('quatation_attributes', 'quatation_attributes.fk_od_id=qutation_details.row_id', 'left')
            ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id', 'left')
            ->join('color_tbl', 'color_tbl.id=qutation_details.color_id', 'left')
            ->where('qutation_details.order_id', $order_id)
            ->get()->result();

        // echo "<pre>";
        // print_r($query); exit;

        return $query;
    }

    public function get_quote_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 1);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_paid_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 2);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_partially_paid_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 3);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_shipping_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 4);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_cancelled_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL)
        {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }

        if ($search->order_date != NULL)
        {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 5);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

}
