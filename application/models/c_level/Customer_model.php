<?php

class Customer_model extends CI_model {

    //    ========= its for doctor certificate save ==========
    public function save_customer_file($customerFileinfo = array()) {
        $this->db->insert_batch('customer_file_tbl', $customerFileinfo);
    }

//============= its for get_customer =================
//    public function get_customer() {
//        $query = $this->db->select('*')
//                        ->from('customer_info')
//                        ->where('created_by', $this->session->userdata('user_id'))
////                        ->where('is_admin', 2)
//                        ->order_by('customer_id', 'desc')
//                        ->get()->result();
//        return $query;
//    }
//============ its for get_customer method ==============
    public function get_customer($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', $level_id)
                        ->order_by('customer_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    =========== its for customer_edit ==============
    public function customer_edit($id) {
        $query = $this->db->select('a.*')
                ->from('customer_info a')
//                ->join('customer_phone_type_tbl b', 'b.customer_id = a.customer_id', 'left')
                ->where('a.customer_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ========== its for get_states =================
    public function get_states() {
        $query = $this->db->select('*')->from('city_state_tbl')->group_by('state_name')->get()->result();
        return $query;
    }

//    =============== its for get_states_wise_city ===============
    public function get_states_wise_city($state_id) {
        $query = $this->db->select('*')->from('city_state_tbl')->where('state_id', $state_id)->get()->result();
        return $query;
    }

//    =============== its for customer_view ================= 
    public function customer_view($id) {
        $query = $this->db->select('a.*')
                ->from('customer_info a')
//                ->join('customer_phone_type_tbl b', 'b.customer_id = a.customer_id', 'left')
                ->where('a.customer_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ============= its for get_customer_phones =============
    public function get_customer_phones($id) {
        $this->db->select('*');
        $this->db->from('customer_phone_type_tbl a');
        $this->db->where('a.customer_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

//    =============== its for show_customer_record ================= 
    public function show_customer_record($id) {
        $query = $this->db->select('a.*, b.created_by AS c_level_parent_user, CONCAT( b.first_name, "&nbsp;", b.last_name) AS comment_by')
                ->from('customer_info a')
                ->join('user_info b', 'b.id = a.created_by')
//                ->join('customer_phone_type_tbl c', 'c.customer_id = a.customer_id')
                ->where('a.customer_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ============= its for show_customer_appointment_record ============
    public function show_customer_appointment_record($id) {
        $this->db->select('*');
        $this->db->from('appointment_calendar a');
        $this->db->where('a.customer_id', $id);
        $this->db->order_by('a.appointment_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

//    ============== its for get_customer_filter_info =============
    public function get_customer_filter_info($first_name, $sidemark, $phone, $address) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, CONCAT(" ", first_name,  last_name) as full_name');
        $this->db->from('customer_info a');
        if ($first_name && $sidemark && $phone && $address) {
            $this->db->like('a.first_name', $first_name, 'both');
            $this->db->like('a.side_mark', $sidemark, 'both');
            if ($phone) {
                $this->db->like('a.phone', $phone, 'both');
                $this->db->or_like('a.phone_2', $phone, 'both');
                $this->db->or_like('a.phone_3', $phone, 'both');
            }
            $this->db->like('a.address', $address, 'both');
            $this->db->where('level_id', $level_id);
        } elseif ($first_name) {
            $this->db->like('a.first_name', $first_name, 'both');
            $this->db->where('level_id', $level_id);
        } elseif ($sidemark) {
            $this->db->like('a.side_mark', $sidemark, 'both');
            $this->db->where('level_id', $level_id);
        } elseif ($phone) {
            $this->db->like('a.phone', $phone, 'both');
            $this->db->or_like('a.phone_2', $phone, 'both');
            $this->db->or_like('a.phone_3', $phone, 'both');
            $this->db->where('a.level_id', $level_id);
        } elseif ($address) {
            $this->db->like('a.address', $address, 'both');
            $this->db->where('a.level_id', $level_id);
        } else {
            $this->db->where('a.level_id', $level_id);
        }
        $query = $this->db->get();
        return $query->result();
    }

//    ============ its for accounts table hit ============
    public function headcode($level_id) {
        $query = $this->db->query("SELECT HeadCode FROM acc_coa WHERE level_id='".$level_id."' AND HeadLevel='4' AND HeadCode LIKE '1020301-%' ORDER BY row_id DESC LIMIT 1");
        return $query->row();
    }

    //    ========== its for get_customer_search_result =============
    public function get_customer_search_result($keyword) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $sql = "SELECT a.*
            	FROM
            	customer_info a
            	WHERE (a.address LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id ORDER BY a.customer_id DESC LIMIT 50";
//        echo $sql;        die();
        $query = $this->db->query($sql);
        return $query->result();
    }

//    ========= its for customer_csv_data ================
    public function customer_csv_data($offset, $limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.customer_id, a.first_name, a.last_name, a.side_mark, a.phone, a.email, a.company, a.address, a.city, a.state, a.zip_code, a.country_code');
        $this->db->from('customer_info a');
        $this->db->where('a.level_id', $level_id);
        $this->db->limit($offset, $limit);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function customer_pdf_data($offset, $limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.customer_id, a.first_name, a.last_name, a.side_mark, a.phone, a.email, a.company, a.address, a.city, a.state, a.zip_code, a.country_code');
        $this->db->from('customer_info a');
        $this->db->where('a.level_id', $level_id);
        $this->db->limit($offset, $limit);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

//    =========== its for get_top_search_customer_info =============
    public function get_top_search_customer_info($keyword) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $sql = "SELECT a.*
	FROM
	customer_info a
	WHERE (a.company_customer_id LIKE '%$keyword%' OR a.company LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                 OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

//    ========== its for  get_top_search_order_info ===========
    public function get_top_search_order_info($keyword) {
        $this->db->select("a.*, CONCAT(b.first_name,' ',b.last_name) as customer_name");
        $this->db->from('quatation_tbl a');
        $this->db->join('customer_info b', 'b.customer_id = a.customer_id');
        $this->db->like('a.order_id', $keyword);
        $query = $this->db->get();
        return $query->result();
    }

//    ============ its for get_customer_order ===========
    public function get_customer_order($customer_id) {
        $this->db->select("a.*,CONCAT(b.first_name,' ',b.last_name) as customer_name");
        $this->db->from('quatation_tbl a');
        $this->db->join('customer_info b', 'b.customer_id = a.customer_id', 'left');
        $this->db->where('a.customer_id', $customer_id);
        $this->db->order_by('a.order_date', 'DESC');
        $query = $this->db->get()->result();
        return $query;
    }

}
