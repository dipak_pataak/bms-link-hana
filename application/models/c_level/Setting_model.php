<?php

class Setting_model extends CI_model {

//    ============= its for my_account ==========
    public function my_account() {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('user_info a');
        $this->db->where('a.id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

//    =========== its for get_product ============
    public function get_product() {
        $query = $this->db->select('a.product_id, a.product_name')
                        ->from('product_tbl a')
                        ->where('active_status', 1)
                        ->order_by('a.product_name', 'asc')
                        ->get()->result();
        return $query;
    }

//    ========= its for  get_only_product for c-level cost factor===========
    public function get_only_product_c_cost_factor($level_id) {
        $this->db->select('a.product_id, a.product_name, b.individual_cost_factor, b.costfactor_discount');
        $this->db->from('product_tbl a');
        $this->db->join('c_cost_factor_tbl b', 'b.product_id = a.product_id', 'left');
        $this->db->where('b.level_id', $level_id);
        $query = $this->db->get();
//        echo $this->db->last_query();
        return $query->result();
    }

//    ============= its for company_profile ================
    public function company_profile() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        echo $level_id;die();
        $query = $this->db->select('company_profile.*')
                ->from('company_profile')
//                ->join('customer_info','customer_info.customer_user_id=company_profile.user_id')
                ->where('company_profile.user_id', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

//    ============ its for check check_iframe_code exists =============
    public function check_iframe_code() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('user_created_by');
        }
        $query = $this->db->select('*')
                ->from('iframe_code_tbl a')
                ->where('created_by', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

//=========== its for web_setting ===============
    public function gateway_list($offset, $limit) {
        $query = $this->db->select('*')
                        ->from('gateway_tbl')
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->where('created_by', $this->session->userdata('user_id'))
                        ->get()->result();
        return $query;
    }

    //==================== its for gateway_edit ==============
    public function gateway_edit($id) {
        $query = $this->db->select('*')
                ->from('gateway_tbl a')
                ->where('a.id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    =========== its for uom_list ==============
    public function us_state_list($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.created_by=" . $user_id;
        }
        $query = $this->db->select("*")
                        ->from('c_us_state_tbl a')
                        ->where($filter)
                        ->order_by('a.state_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//==================== its for us_state_edit ==============
    public function us_state_edit($id) {
        $query = $this->db->select('*')
                ->from('c_us_state_tbl a')
                ->where('a.state_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function access_logs($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.user_name=" . $user_id;
        }
        $query = $this->db->select('a.*, CONCAT( b.first_name, " ", b.last_name) AS name')
                        ->from('accesslog a')
                        ->join('user_info b', 'b.id = user_name')
//                        ->where('a.user_name', $level_id)
                        ->where($filter)
                        ->order_by('a.entry_date', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    public function sendStaffAppointmentMail($c_level_staff_id, $email, $staff_name, $customer_name, $customer_phone, $data) {
        $data['name'] = $staff_name;
        $data['customer_name'] = $customer_name;
        $data['customer_phone'] = $customer_phone;
//        $data['author_info'] = $this->user_information($c_level_staff_id);
//        echo $data['get_form_data']['appointment_time'];
        $setting_detail = $this->db->select('*')->from('mail_config_tbl')->get()->row();
        $subject = 'Appointment Information';
        $message = $data['name'];
        $config = Array(
            'protocol' => $setting_detail->protocol,
            'smtp_host' => $setting_detail->smtp_host,
            'smtp_port' => $setting_detail->smtp_port,
            'smtp_user' => $setting_detail->smtp_user,
            'smtp_pass' => $setting_detail->smtp_pass,
            'mailtype' => $setting_detail->mailtype,
            'charset' => 'utf-8',
        );
//        echo "<pre>";        print_r($config);die();

        $mesg = $this->load->view('c_level/settings/send_appointment', $data, TRUE);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($setting_detail->smtp_user, "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMS Decor");
// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
// . "\n\nThanks\nMetallica Gifts");
// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");
        $this->email->message($mesg);
        $this->email->send();
    }

    //    =========== its for customer_user_info ==============
    public function user_information($c_level_staff_id) {
        $query = $this->db->select('*')
                        ->from('user_info a')
//                        ->join('user_info b', 'b.id = a.user_id', 'left')
                        ->where('a.id', $c_level_staff_id)
                        ->get()->row();
        return $query;
    }

//    ============ its for staff appointment sms send =============
    public function sendStaffAppointmentSms($c_level_staff_id, $staff_name, $customer_name, $customer_phone, $data) {
        $user_info = $this->db->where('id', $c_level_staff_id)->get('user_info')->row();
        $author_info = $this->user_information($c_level_staff_id);
        $appointment_date = $data['get_form_data']['appointment_date'];
        $appointment_time = $data['get_form_data']['appointment_time'];
        $appointment_remarks = $data['get_form_data']['remarks'];
//        echo $appointment_remarks;        die();
        if ($user_info->phone != NULL) {
            $this->load->library('twilio');
//            $name = @$user_info->first_name . ' ' . @$user_info->last_name;
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('default_status', 1)->get()->row();
            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = "+88" . $user_info->phone;
            $message = 'Hi! ' . $staff_name . ', Your Appointment scheduled with ' . @$customer_name . " ph : " . @$customer_phone . " on " . $appointment_date . " " . $appointment_time . ". " . $appointment_remarks . ".";
            if ($sms_gateway_info->provider_name == 'Twilio') {
                $response = $this->twilio->sms($from, $to, $message);
                if ($response->IsError) {
                    echo 'Error: ' . $response->ErrorMessage;
                } else {
//                    $sms_data = array(
//                        'from' => $from,
//                        'to' => $to,
//                        'message' => $message,
//                        'created_by' => $this->user_id,
//                    );
//                    $this->db->insert('custom_sms_tbl', $sms_data);
                    $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                }
            }
            
            return 1;
        }
//    }
    }

}
