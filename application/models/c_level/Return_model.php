<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Return_model extends CI_model {

  

    //============ its for customer_list method ==============
    public function customer_return($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name, b.side_mark')
                        ->from('order_return_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        ->where('a.level_id', $level_id)
//                        ->where('a.order_stage', 7)
                        ->order_by('a.return_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }
    
//    =========== its for get_order_info ==========
    public function get_order_info($order_id){
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name')
                        ->from('quatation_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        ->where('a.order_id', $order_id)
                        ->get()->row();
        return $query;
    }
//    =========== its for get_order_details ==========
    public function get_order_details($order_id){
        $query = $this->db->select('a.*, b.product_name')
                ->from('qutation_details a')
                ->join('product_tbl b', 'b.product_id = a.product_id')
                ->where('a.order_id', $order_id)
                ->get()->result();
        return $query;
    }

//    ============= its for purchase_order =========
        public function purchase_order($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name')
                        ->from('b_level_quatation_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        ->where('a.level_id', $level_id)
                        ->where('a.status', 1)
                        ->order_by('a.id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }
    
    //    =========== its for get_order_info ==========
    public function get_purchase_order_info($order_id){
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name')
                        ->from('b_level_quatation_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        ->where('a.order_id', $order_id)
                        ->get()->row();
        return $query;
    }
//    ========= its for get_purchase_order_details ===========
    public function get_purchase_order_details($order_id){
           $query = $this->db->select('a.*, b.product_name')
                ->from('b_level_qutation_details a')
                ->join('product_tbl b', 'b.product_id = a.product_id')
                ->where('a.order_id', $order_id)
                ->get()->result();
        return $query;
    }
}
