<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function check_user($data = array()) {

        $result = $this->db->select("*")
                        ->from('log_info a')
                        ->join('user_info b', 'b.id = a.user_id', 'left')
                        ->where('a.email', $data['email'])
                        ->where('a.password', md5($data['password']))
                        ->where('a.user_type', 'c')
                        ->get()->row();

        return $result;
    }

//    =========== its for permission =============
    public function userPermission2($id = null) {
//        echo $id;die();
        $acc_tbl = $this->db->select('*')->from('user_access_tbl')->where('user_id', $id)->get()->result();
//        echo '<pre>';        print_r($acc_tbl);die();
        if ($acc_tbl != NULL) {
            $role_id = [];
            foreach ($acc_tbl as $key => $value) {
                $role_id[] = $value->role_id;
            }

            return $result = $this->db->select("
				role_permission_tbl.role_id, 
				role_permission_tbl.menu_id, 
				IF(SUM(role_permission_tbl.can_create)>=1,1,0) AS 'create', 
				IF(SUM(role_permission_tbl.can_access)>=1,1,0) AS 'read', 
				IF(SUM(role_permission_tbl.can_edit)>=1,1,0) AS 'update', 
				IF(SUM(role_permission_tbl.can_delete)>=1,1,0) AS 'delete',
				c_menusetup_tbl.menu_title,
				c_menusetup_tbl.page_url,
				c_menusetup_tbl.module
				")
                    ->from('role_permission_tbl')
                    ->join('c_menusetup_tbl', 'c_menusetup_tbl.menu_id = role_permission_tbl.menu_id', 'full')
                    ->where_in('role_permission_tbl.role_id', $role_id)
                    ->group_by('role_permission_tbl.menu_id')
                    ->group_start()
                    ->where('can_create', 1)
                    ->or_where('can_access', 1)
                    ->or_where('can_edit', 1)
                    ->or_where('can_delete', 1)
                    ->group_end()
                    ->get()
                    ->result();
///echo $this->db->last_query();
                   // print_r($result);exit;
        } else {
            return 0;
        }
    }

//    =========== permission close ============
}
