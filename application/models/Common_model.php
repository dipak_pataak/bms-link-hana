<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_model {

	public function payment_type(){
		return $data = array('1' => 'Cash','2'=>'Check' );
    }	
    
    // START For Multiple delete : Insys
	public function DeleteSelected($table,$fields='id'){
		$this->db->where_in($fields, $this->input->post('Id_List'));
		$query = $this->db->delete($table);
		return $query;
	}
	// END For Multiple delete : Insys
}
