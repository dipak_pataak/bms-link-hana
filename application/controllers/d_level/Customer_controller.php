<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_controller extends CI_Controller {

    public function index() {
//        $this->load->view('welcome_message');
    }

//    =============== its for customer_form ===================
    public function customer_form() {
        $data['d_level_form'] = $this->db->select('*')->from('iframe_code_tbl')->where('frame_id', 1)->get()->result();

//        $this->load->view('c_level/header');
//        $this->load->view('c_level/sidebar');
        $this->load->view('d_level/customer_form', $data);
//        $this->load->view('c_level/footer');
    }

}
