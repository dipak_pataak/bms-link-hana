<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($session_id == '' || $user_type != 'c') {
            redirect('c-level-logout');
        }

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }


        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Appointment_model');
        $this->load->model('b_level/settings');
        $this->load->model('c_level/Setting_model');
    }

    public function index() {
        
    }

//  ============ its for appointment-form =============
    public function appointment_form() {

        $data['get_users'] = $this->User_model->get_users();
        $data['get_customer'] = $this->Customer_model->get_customer();
        $data['get_appointment_info'] = $this->Appointment_model->get_appointment_info($this->level_id);


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/form_appointment', $data);
        $this->load->view('c_level/footer');
    }

//    ============ its for appointment_setup method ================
    public function appointment_setup() {
        $status = $this->input->post('status');
        $acc_remarks = "Appointment information save";
        $created_date = date('Y-m-d');

        $customer_id = $this->input->post('customer_id');

//        dd($c_level_id);
        $appointment_date = $this->input->post('appointment_date');
        $appointment_time = $this->input->post('appointment_time');
        $c_level_staff_id = $this->input->post('c_level_staff_id');
        $remarks = $this->input->post('remarks');
        $check_customer_info = $this->db->select('*')->from('customer_info a')->where('a.customer_id', $customer_id)->get()->row();
        $check_user = $this->db->select('*')->from('user_info a')->where('a.id', $c_level_staff_id)->get()->row();
        $staff_name = $check_user->first_name." ".@$check_user->last_name;
        $staff_mail = $check_user->email;
        $customer_name = $check_customer_info->first_name." ".@$check_customer_info->last_name;
        $customer_phone =$check_customer_info->phone;
        if ($check_user) {
//            $data['get_mail_config'] = $this->settings->get_mail_config();
            $data['get_form_data'] = array(
                'appointment_date' => $appointment_date,
                'appointment_time' => $appointment_time,
                'customer_id' => $customer_id,
                'remarks' => $remarks,
            );
            $this->Setting_model->sendStaffAppointmentMail($c_level_staff_id, $staff_mail, $staff_name, $customer_name, $customer_phone, $data);
            $this->Setting_model->sendStaffAppointmentSms($c_level_staff_id, $staff_name, $customer_name, $customer_phone, $data);
        }

        $appointment_data = array(
            'customer_id' => $customer_id,
            'c_level_id' => $this->level_id,
            'appointment_date' => $appointment_date,
            'appointment_time' => $appointment_time,
            'c_level_staff_id' => $c_level_staff_id,
            'remarks' => $remarks,
            'create_by' => $this->user_id,
            'create_date' => $created_date,
        );

//        echo '<pre>';        print_r($appointment_data);die();
        $this->db->insert('appointment_calendar', $appointment_data);
        $this->db->set('now_status', $status)->where('customer_id', $customer_id)->update('customer_info');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "appointment information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
//        echo '<pre>';print_r($appointment_data); die();
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Appointment setup successfully!</div>");
        redirect('customer-list');
    }

    public function get_appointment_info() {

        $data = $this->Appointment_model->get_appointment_info($this->level_id);



        $this->db->select("*,COUNT(appointment_date)as title")
                ->where('c_level_id', $this->level_id)
                ->group_by('appointment_date')
                ->get('appointment_calendar')->result();

        $data_events = array();
        $i = 1;
        foreach ($data as $r) {

            $color = "#37a000";
            $data_events[] = array(
                "title" => $r->title,
                "start" => $r->appointment_date,
                "end" => $r->create_date,
                "mydate" => $r->appointment_date,
                "color" => $color
            );
            $i++;
        }

        echo json_encode(array("events" => $data_events));
    }

    public function get_appointment_info_by_date($date) {


        $data = $this->db->select("appointment_calendar.*,
            CONCAT_WS(' ', customer_info.first_name, customer_info.last_name) AS customer_name,
            CONCAT_WS(' ', user_info.first_name, user_info.last_name) AS staffname
            ")
                ->join('customer_info', 'customer_info.customer_id=appointment_calendar.customer_id')
                ->join('user_info', 'user_info.id=appointment_calendar.c_level_staff_id')
                ->where('appointment_date', $date)
                ->order_by('appointment_time', 'ASC')
                ->get('appointment_calendar')
                ->result();



        $table = '';
        if (!empty($data)) {
            foreach ($data as $r) {
                $table .= '<tr>
                            <td>' . $r->customer_name . '</td>
                            <td>' . $r->staffname . '</td>
                            <td>' . $r->remarks . '</td>
                            <td>' . $r->appointment_date . '</td>
                            <td>' . $r->appointment_time . '</td>
                          </tr>';
            }
        } else {
            $table .= '<tr><h4>Data not found</h4></tr>';
        }
        echo $table;
    }

    public function staff_appointment() {


        $this->db->select('*');
        $this->db->from('appointment_calendar');
        $this->db->where('appointment_date', date('Y-m-d'));
        $this->db->where('c_level_staff_id', $this->session->userdata('user_id'));
        $data['list'] = $this->db->get()->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/users/staff_appointment_list', $data);
        $this->load->view('c_level/footer');
    }

}
