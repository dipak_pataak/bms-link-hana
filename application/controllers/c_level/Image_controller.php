<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Image_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        $session_id = $this->session->userdata('session_id');
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('email_sender');
        $this->load->model('b_level/settings');
        $this->load->model('c_level/setting_model', 'c_settings');
        $this->load->library('paypal_lib');
        $this->load->model('c_level/Order_model');
    }

    public function manage_image() {
        $config["base_url"] = base_url('c_level/Image_controller/manage_image');
        $config["total_rows"] = $this->db->count_all('gallery_image_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $tags = $this->db->order_by('id')->get('tag_tbl')->result_array();

        $filter_arr = array();
        $a = 0 ;
        $final_img_ids = array();
        $is_filter = $is_valid = 0;
        foreach($tags as $key => $tag) {
            $filter_val = $this->input->post('tag_'.$tag['id']);
            if($filter_val != '') {
                $is_filter = 1;
                $image_ids = $this->db->select('image_id')
                        ->where('tag_id', $tag['id'])
                        ->where('tag_value', $filter_val)
                        ->get('gallery_tag_tbl')
                        ->result_array();
                if(count($image_ids) > 0) {
                    $ids = array_column($image_ids, 'image_id');
                    if(count($final_img_ids) > 0 || $is_valid == 1) {
                        $final_img_ids = array_intersect ($final_img_ids, $ids);
                        $is_valid = 1;
                    } else {
                        $final_img_ids = $ids;
                    }
                }
            }
            $tag_value = $this->db->select('DISTINCT(tag_value)')
                            ->where('tag_id', $tag['id'])
                            ->get('gallery_tag_tbl')
                            ->result_array();
            if(count($tag_value) > 0) {
                $filter_arr[$a]['value']= $tag_value;
                $filter_arr[$a]['tag_name'] = $tag['tag_name'];
                $filter_arr[$a]['tag_id'] = $tag['id'];
                $filter_arr[$a]['selected_val'] = $filter_val;
                $a++;
            }
            
        }
        $data['filter_arr'] = $filter_arr;
        if(count($final_img_ids) > 0) {
            $this->db->where_in('id', $final_img_ids);
        } else if($is_filter == 1){
            $this->db->where('1', '0');
        }
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["images"] = $this->db->from('gallery_image_tbl')
                            ->order_by('id', 'desc')
                            ->limit($config["per_page"], $page)
                            ->get()
                            ->result();
        $data["links"] = $this->pagination->create_links();
        $data["patterns"] = $this->db->get('pattern_model_tbl')->result();
        $data['pagenum'] = $page;


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/image/manage_image', $data);
        $this->load->view('c_level/footer');
    }

    public function view_image($id) {
        $tag_data = $this->db->select('gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value,tag_tbl.tag_name')
            ->from('gallery_tag_tbl')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->where('image_id',$id)
            ->get()->result_array();

        $data['tag_data'] = array();
        if(count($tag_data) > 0) {
            foreach($tag_data as $row) {
                $data['tag_ids'][] = $row['tag_id'];
                $data['tag_data'][$row['tag_id']] = $row['tag_value'];
            }
        }
        $data['images_tag_data'] = $this->db->from('gallery_tag_tbl')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->join('gallery_image_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
            ->where('image_id',$id)
            ->get()
            ->row();

        echo json_encode(array($data['images_tag_data'],$data['tag_data'],$tag_data));
    }
}