<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        // if ($session_id == '' || $user_type != 'c') {
        //    redirect('c-level-logout');
        // }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('b_level/settings');
    }

    public function index() {
        
    }

    public function delete_quotation($quote_id) {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "quatation information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('quote_id', $quote_id)->delete('quote_comparison_tbl');
        $this->db->where('quote_id', $quote_id)->delete('quote_comparison_details');

        $this->session->set_flashdata('success', "<div class='alert alert-success msg'>Data updated successfully!</div>");
        redirect('c_level/quotation_controller/manage_quotation');
    }

    public function quotation_receipt($quote_id) {
        $this->permission_c->check_label('quotation_receipt')->create()->redirect();

        $data['company_profile'] = $this->settings->company_profile();

        $data['quotation'] = $this->db->select("quote_comparison_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
                        ->join('customer_info', 'customer_info.customer_id=quote_comparison_tbl.customer_id', 'left')
                        ->where('quote_id', $quote_id)
                        ->get('quote_comparison_tbl')->row();

        $data['room'] = $this->db->where('quote_id', $quote_id)->group_by('room_name')->get('quote_comparison_details')->result();

        $data['cat'] = $this->db->select('category_tbl.category_name,category_tbl.category_id,quote_comparison_details.quote_id')
                        ->join('category_tbl', 'category_tbl.category_id=quote_comparison_details.category_id')
                        ->where('quote_id', $quote_id)->group_by('quote_comparison_details.category_id')->get('quote_comparison_details')->result();


        $data['quotation_details'] = $this->db->select("quote_comparison_details.*,category_tbl.*")
                        ->join('category_tbl', 'category_tbl.category_id=quote_comparison_details.category_id', 'left')
                        ->where('quote_id', $quote_id)
                        ->get('quote_comparison_details')->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/quotation_receipt', $data);
        $this->load->view('c_level/footer');
    }

    public function manage_quotation() {
        $this->permission_c->check_label('manage_quotation')->create()->redirect();

        $data['get_customer'] = $this->Order_model->get_customer();
        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();

        $data['quotation'] = $this->db->select("quote_comparison_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
                        ->join('customer_info', 'customer_info.customer_id=quote_comparison_tbl.customer_id', 'left')
                        ->get('quote_comparison_tbl')->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/manage_quotation', $data);
        $this->load->view('c_level/footer');
    }

    // public function edit_quotation($quote_id) {
    //     $data['get_customer'] = $this->Order_model->get_customer();
    //     $data['categories'] = $this->Order_model->get_category();
    //     $data['company_profile'] = $this->settings->company_profile();
    //     $data['qtjs'] = "c_level/quotation/qt.php";
    //     $data['quotation'] = $this->db->select("quote_comparison_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
    //     ->join('customer_info','customer_info.customer_id=quote_comparison_tbl.customer_id','left')
    //     ->where('quote_id',$quote_id)
    //     ->get('quote_comparison_tbl')->row();
    //     $this->load->view('c_level/header');
    //     $this->load->view('c_level/sidebar');
    //     $this->load->view('c_level/quotation/edit_quotation',$data);
    //     $this->load->view('c_level/footer');
    // }




    public function add_new_quotation() {
        $this->permission_c->check_label('add_new_quotation')->create()->redirect();

        $data['get_customer'] = $this->Order_model->get_customer();

        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();
        $data['qtjs'] = "c_level/quotation/qt.php";

        $data['rooms'] = $this->db->get('rooms')->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/add_new_quotation', $data);
        $this->load->view('c_level/footer');
    }

    public function get_color_partan_model($product_id) {

        $pp = $this->db->select('*')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();


        $patter_ids = explode(',', @$pp->pattern_models_ids);

        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->get('pattern_model_tbl')->result();




        $q = '<option value="">-- select pattern --</option>';
        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }

        if ($pp->price_style_type == 3) {
            $main_price = $pp->fixed_price;
        } elseif ($pp->price_style_type == 2) {
            $main_price = $pp->sqft_price;
        } else {
            $main_price = 0;
        }

        $arr = array('price' => $main_price, 'pattern' => $q);
        echo json_encode($arr);
    }

    public function get_product_by_category($category_id) {

        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)->where('subcategory_id', 0)
                        ->get('product_tbl')->result();

        $q = '';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }

    public function customer_wise_sidemark($customer_id = null) {

        $customer_wise_sidemark = $this->db->select('customer_info.*,c_us_state_tbl.tax_rate')
                        ->from('customer_info')
                        ->join('c_us_state_tbl', 'c_us_state_tbl.state_name=customer_info.state', 'left')
                        ->where('customer_info.customer_id', $customer_id)
                        ->get()->row();
        echo json_encode($customer_wise_sidemark);
    }

    public function price_call($width = NULL, $height = NULL, $product_id = NULL, $patter_id = NULL) {


        if (isset($product_id) && !empty($height) && !empty($width)) {

            $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();

            $q = "";
            $price = "";
            if ($p->price_style_type == 1) {

                $prices = $this->db->where('style_id', $p->price_rowcol_style_id)
                                ->where('row =', $width)
                                ->where('col =', $height)
                                ->get('price_style')->row();


                if (isset($prices) && $prices->price != 0) {

                    $price = $pc = $prices->price;
                } else {

                    $prices = $this->db->where('style_id', $p->price_rowcol_style_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();


                    $pc = ($prices != NULL ? $prices->price : 0);
                    $price = $pc;
                };
            } elseif ($p->price_style_type == 2) {

                $price = $p->sqft_price;
            } elseif ($p->price_style_type == 3) {

                $price = $p->fixed_price;
            } elseif ($p->price_style_type == 4) {

                if ($patter_id != NULL) {

                    $pg = $this->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $patter_id)->get()->row();

                    $price = $this->db->where('style_id', $pg->group_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();

                    $pc = ($price != NULL ? $price->price : 0);
                    $price = $pc;
                } else {
                    $price = 0;
                }
            } else {

                $price = 0;
            }


            $arr = array('price' => $price);

            echo json_encode($arr);
        }
    }

    public function upcharge($category) {

        $q = '';

        if ($category == 'Shutters') {

            $q .= '<tr>
                <td>
                    <p class="text-danger">Specialty shapes</p>
                    No of Panel 
                </td>
                <td colspan="2"><input type="number" name="no_panel" id="no_panel" class=" text-right"> X $150 each panel = $<b class="plan_cost"></b>
                    <input type="hidden" name="plan_price" value="0" id="plan_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Chair Rail Cut</p>
                    No of window
                </td>
                <td colspan="2"><input type="number" name="no_window" id="no_window" class="text-right"> X $30 each window = $<b class="window_cost"></b> 
                    <input type="hidden" name="window_price" value="0" id="window_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Door handle Cut</p>
                    No of Door
                </td>
                <td colspan="2"><input type="number" name="no_door" id="no_door" class="text-right"> X $150 per Door = $<b class="door_cost"></b>
                    <input type="hidden" name="door_price" value="0" id="door_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Build out</p>
                    No of Shutters
                </td>
                <td colspan="2"><input type="number" name="no_shutters" id="no_shutters" class="text-right"> X $30 each = $<b class="shutters_cost"></b>
                    <input type="hidden" name="shutters_price" value="0" id="shutters_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">T- Post</p>
                    No of window 
                </td>
                <td colspan="2"><input type="number" name="no_t_window" id="no_t_window" class="text-right"> X $40 each = $<b class="t_window_cost"></b>
                    <input type="hidden" name="t_window_price" value="0" id="t_window_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Blinds') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Upgraded Valance</p>
                    No of Blinds
                </td>
                <td colspan="2"><input type="number" name="no_blinds" id="no_blinds" class=" text-right"> X $4 each = $<b class="blind_cost"></b> 
                <input type="hidden" name="blinds_price" value="0" id="blinds_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Shades') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Fabric Wraped Cassette</p>
                    Macro
                </td>
                <td colspan="2"><input type="number" name="macro" id="Macro" class=" text-right"> X 10% = $<b class="Macro_cost"></b> 
                    <input type="hidden" name="macro_price" value="0" id="macro_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Arches') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Custom Paint</p>
                    Macro
                </td>
                <td colspan="2"><input type="number" name="custom_paint" id="custom_paint" class=" text-right"> X 20% = $<b class="custom_paint_cost"></b> 
                <input type="hidden" name="custom_paint_price" value="0" id="custom_paint_price" class="up_price"/>
                </td>
            </tr>';
        }

        echo $q;
    }

    public function add_to_cart() {

        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $product_id = $this->input->post('product_id');
        $patter_id = $this->input->post('patter_id');
        $price = $this->input->post('price');

        $upcharge = $this->input->post('upcharge');
        $room_sub_price = $this->input->post('room_sub_price');
        $room_name = $this->input->post('room_name');
        $category_id = $this->input->post('category_id');
        $category_name = $this->input->post('category_name');


        if ($category_name == 'Shutters') {

            $upchargeData = array(
                'no_panel' => $this->input->post('no_panel'),
                'panel_price' => $this->input->post('plan_price'),
                'no_window' => $this->input->post('no_window'),
                'window_price' => $this->input->post('window_price'),
                'no_door' => $this->input->post('no_door'),
                'door_price' => $this->input->post('door_price'),
                'no_shutters' => $this->input->post('no_shutters'),
                'shutters_price' => $this->input->post('shutters_price'),
                'no_t_window' => $this->input->post('no_t_window'),
                't_window_price' => $this->input->post('t_window_price')
            );
        } elseif ($category_name == 'Blinds') {

            $upchargeData = array(
                'no_blinds' => $this->input->post('no_blinds'),
                'blinds_price' => $this->input->post('blinds_price')
            );
        } elseif ($category_name == 'Shades') {

            $upchargeData = array(
                'macro' => $this->input->post('macro'),
                'macro_price' => $this->input->post('macro_price')
            );
        } elseif ($category_name == 'Arches') {

            $upchargeData = array(
                'custom_paint' => $this->input->post('custom_paint'),
                'custom_paint_price' => $this->input->post('custom_paint_price')
            );
        }




        $qty = count($product_id);
        $id = str_replace(' ', '_', $room_name) . $qty . $category_id;

        $product_data = [];
        foreach ($product_id as $key => $pid) {
            $product_data[] = array(
                'width' => $width[$key],
                'height' => $height[$key],
                'product_id' => $pid,
                'patter_id' => $patter_id[$key],
                'price' => $price[$key]
            );
        }


        $cartData = array(
            'id' => $id,
            'product_id' => $id,
            'name' => $room_name,
            'category_name' => $category_name,
            'category_id' => $category_id,
            'price' => $room_sub_price + $upcharge,
            'qty' => $qty,
            'product_data' => json_encode($product_data),
            'upchargeData' => json_encode($upchargeData),
            'upcharge' => $upcharge,
            'room_price' => $room_sub_price
        );

        $this->cart->insert($cartData);
        echo 1;
    }

    //-------------------------------------
    // Clear Cart 
    // ------------------------------------
    public function clear_cart() {
        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Cleare successfully! </div>");
        redirect('c_level/quotation_controller/add_new_quotation');
    }

    // END--------------------------------
    //---------------------------------------
    // Delete Cart item
    // --------------------------------------
    public function delete_cart_item($rowid) {
        $this->cart->remove($rowid);
        echo 1;
    }

    public function save_quotation() {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "inserted";
        $remarks = "quatation information saved";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $quotData = array(
            'customer_id' => $this->input->post('customer_id'),
            'grand_total' => $this->input->post('grand_total'),
            'sub_total' => $this->input->post('subtotal'),
            'sales_tax' => $this->input->post('tax'),
            'created_date' => $this->input->post('order_date'),
            'create_by' => $this->session->userdata('user_id')
        );


        if ($this->db->insert('quote_comparison_tbl', $quotData)) {

            $quote_id = $this->db->insert_id();

            $room_name = $this->input->post('room_name');
            $category_id = $this->input->post('category_id');

            $product_data = $this->input->post('product_data');
            $upchargeData = $this->input->post('upchargeData');

            $product_price = $this->input->post('room_price');
            $subtotal = $this->input->post('list_price');
            $quote_price = $this->input->post('utprice');
            $discount = $this->input->post('discount');
            $upcharge = $this->input->post('upcharge');


            $quotDetails = [];
            for ($i = 0; $i < count($room_name); $i++) {

                $pp = json_decode($product_data[$i]);


                $quotDetails[] = array(
                    'quote_id' => $quote_id,
                    'room_name' => $room_name[$i],
                    'width' => $pp[0]->width,
                    'height' => $pp[0]->height,
                    'category_id' => $category_id[$i],
                    'product_data' => $product_data[$i],
                    'upcharge_data' => $upchargeData[$i],
                    'product_price' => $product_price[$i],
                    'subtotal' => $subtotal[$i],
                    'quote_price' => $quote_price[$i],
                    'upcharge' => $upcharge[$i],
                    'discount' => $discount[$i]
                );
            }

            $this->db->insert_batch('quote_comparison_details', $quotDetails);
        }

        $this->cart->destroy();

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order add successfully</div>");
        redirect("c_level/quotation_controller/add_new_quotation");
    }

}
