<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_payment extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->model('c_level/Order_model');
        $this->load->library('paypal_lib');

    }

    public function payment() {

        $order_id = $this->input->get('order_id');
        $customer_id = $this->input->get('customer_id');

        $orderd = $this->Order_model->get_orderd_by_id($order_id);
        $order_details = $this->Order_model->get_orderd_details_by_id($order_id);
        

        $orderData = array(
            'order_id'                   => $order_id,
            'customer_id'                => $customer_id,
            'created_by'                 => $orderd->level_id,
            'paid_amount'                => $orderd->grand_total,
            'due'                         => $orderd->due
        );

         $this->session->set_userdata($orderData);

        $pyament = $this->payment_by_paypal($orderData);

    }



    public function payment_by_paypal($orderData){


        $customer_id = $orderData['customer_id'];
        $order_id = $orderData['order_id'];
        $paid_amount = $orderData['paid_amount'];

        //Set variables for paypal form
        $returnURL = base_url("c_level/Customer_payment/success/$order_id/$customer_id"); //payment success url
        $cancelURL = base_url("c_level/Customer_payment/cancel/$order_id/$customer_id"); //payment cancel url
        $notifyURL = base_url('c_level/Customer_payment/ipn'); //ipn url


        //set session token
        $this->session->unset_userdata('_tran_token');
        $this->session->set_userdata(array('_tran_token' => $order_id));
        // set form auto fill data
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);


        $paypal = $this->db->select('*')
                ->from('gateway_tbl')
                ->where('default_status', 1)
                ->where('created_by', $orderData['created_by'])
                ->where('level_type','c' )
                ->get()
                ->row();

        $paypal_lib_currency_code = (!empty($paypal->currency) ? $paypal->currency : 'USD');
        $paypal_lib_ipn_log_file = BASEPATH . 'logs/paypal_ipn.log';
        $paypal_lib_ipn_log = TRUE;
        $paypal_lib_button_path = 'buttons';

        // my customization
        $this->paypal_lib->add_field('mode', $paypal->status);
        $this->paypal_lib->add_field('business', $paypal->payment_mail);
        $this->paypal_lib->add_field('paypal_lib_currency_code', $paypal_lib_currency_code);
        $this->paypal_lib->add_field('paypal_lib_ipn_log_file', $paypal_lib_ipn_log_file);
        $this->paypal_lib->add_field('paypal_lib_ipn_log', $paypal_lib_ipn_log);
        $this->paypal_lib->add_field('paypal_lib_button_path', $paypal_lib_button_path);
        //-----------------------
        
        // item information
        $this->paypal_lib->add_field('item_number', $order_id);
        //$this->paypal_lib->add_field('item_name', $item_name);
        $this->paypal_lib->add_field('amount', $paid_amount);
        // $this->paypal_lib->add_field('quantity', $quantity);
        // $this->paypal_lib->add_field('discount_amount', $discount);

        // additional information 
        $this->paypal_lib->add_field('custom', $order_id);
        $this->paypal_lib->image('');
        // generates auto form
        $this->paypal_lib->paypal_auto_form();
  
    }



    public function success($order_id = null,$customer_id = null) {

        $paid_amount = $this->session->userdata('paid_amount');


        if (!empty($order_id)) {

            $order_stage = 2;
           
            $rowData = $this->db->select('due,paid_amount')->where('order_id', $order_id)->get('quatation_tbl')->row();
            if(!empty($rowData)){

                // quatation table update
                $orderData = array(
                    'paid_amount'   => @$rowData->paid_amount + $paid_amount,
                    'due'           => 0,
                    'order_stage'   => $order_stage
                );

                //update to quatation table with pyament due 
                $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);

                // quatation table update
                $payment_tbl = array(
                    'quotation_id'      => $order_id,
                    'payment_method'    => 'Paypal',
                    'paid_amount'       => $paid_amount,
                    'payment_date'      => date('Y-m-d'),
                    'created_by'        => $customer_id,
                    'create_date'       => date('Y-m-d')
                );
                $this->db->insert('payment_tbl', $payment_tbl);


                $cNotificationData = array(
                    'notification_text'     => 'Payment has been submited',
                    'go_to_url'             => 'c_level/invoice_receipt/receipt/' . $order_id,
                    'created_by'            => $customer_id,
                    'date'                  => date('Y-m-d')
                );

                $this->db->insert('c_notification_tbl', $cNotificationData);
            }
        }


        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('customer_id');
        $this->session->unset_userdata('created_by');
        $this->session->unset_userdata('paid_amount');
        $this->session->unset_userdata('due');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
            Payment Successfull</div>");

        redirect("c_level/Customer_payment/message/");


    }


    public function cancel($order_id = null, $customer_id = null) {

        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('customer_id');
        $this->session->unset_userdata('created_by');
        $this->session->unset_userdata('paid_amount');
        $this->session->unset_userdata('due');

        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
            Payment Cancel</div>");

        redirect("c_level/Customer_payment/message/".$order_id.'/'.$customer_id);

    }


    public function ipn() {

        //paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["mc_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;

        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (preg_match("/VERIFIED/i", $result)) {
            //insert the transaction data into the database
            $this->load->model('Paypal_model');
            $this->Paypal_model->insertTransaction($data);
        }

        return true;
    }



    public function message($order_id=NULL,$customer_id=NULL){

        $this->load->model('c_level/Order_model');
        $data['customer_info'] = $this->db->where('customer_id',$customer_id)->get('customer_info')->row();
        $data['company_profile'] = $this->db->select('*')
        ->where('user_id',$data['customer_info']->level_id)
        ->get('company_profile')->row();

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        $this->load->view('c_level/Payment_msg',$data);
        $this->load->view('c_level/footer');
    }


}
