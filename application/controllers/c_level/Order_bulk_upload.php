<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class order_bulk_upload extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'c') {
            redirect('c-level-logout');
        }
    }

    public function index() {
        $this->permission_c->check_label('index')->create()->redirect();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/order_bulk_upload');
        $this->load->view('c_level/footer');
    }

//========== its for c level customer bulk upload save =============
    public function csv_upload_save() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }


        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            $productData = [];

            while ($csv_line = fgetcsv($fp, 1024)) {

                if ($count != 0) {

                    if ($csv_line[10] == 2 || $csv_line[10] == 3 || $csv_line[10] == 7) {
                        
                        $order_stage_value = $csv_line[10];
                    
                    } else {

                        $order_stage_value = 1;

                    }

                    $customer_email = (!empty($csv_line[0]) ? $csv_line[0] : null);

                    $customer_sidemark = (!empty($csv_line[1]) ? $csv_line[1] : null);

                    $invoice_number = (!empty($csv_line[2]) ? $csv_line[2] : null);

                    $date = (!empty($csv_line[3]) ? $csv_line[3] : null);

                    $product_name = (!empty($csv_line[4]) ? $csv_line[4] : null);

                    $pattern_name = (!empty($csv_line[5]) ? $csv_line[5] : null);

                    $color_name = (!empty($csv_line[6]) ? $csv_line[6] : null);

                    $qty = (!empty($csv_line[7]) ? $csv_line[7] : null);

                    $dscription = (!empty($csv_line[8]) ? $csv_line[8] : null);

                    $amount = (!empty($csv_line[9]) ? $csv_line[9] : null);

//                  $order_stage = (!empty($csv_line[10]=='2') ? $csv_line[10] : 3);
                    $order_stage = $order_stage_value;

                    // duplicat id delete this table
                    $this->db->where('order_id', $invoice_number)->delete('quatation_tbl');
                    $this->db->where('order_id', $invoice_number)->delete('qutation_details');
                    $this->db->where('order_id', $invoice_number)->delete('quatation_attributes');
                    $this->db->where('order_id', $invoice_number)->delete('shipment_data');
                    //-------------------------

                    $customer_info = $this->db->select('*')->where('email', $customer_email)->get('customer_info')->row();

                    $product_info = $this->db->select('*')->where('product_name', $product_name)->get('product_tbl')->row();

                    $color = $this->db->select('id')->where('color_name', $color_name)->get('color_tbl')->row();

                    $pattern = $this->db->select('pattern_model_id')->where('pattern_name', $pattern_name)->get('pattern_model_tbl')->row();
                    
                    $new_date = date_create($date);

                    $order_data = date_format($new_date, 'Y-m-d');

                    if(@$customer_info->customer_id!=NULL && @$product_info->product_id!=NULL ){

                        $productData[$invoice_number][] = array(
                            'order_id' => $invoice_number,
                            'product_id' => @$product_info->product_id,
                            'room' => '',
                            'product_qty' => @$qty,
                            'list_price' => @$amount,
                            'discount' => 0,
                            'unit_total_price' => @$amount * $qty,
                            'category_id' => @$product_info->category_id,
                            'pattern_model_id' => @$pattern->pattern_model_id,
                            'color_id' => @$color->id,
                            'width' => 0,
                            'height' => 0,
                            'height_fraction_id' => 0,
                            'width_fraction_id' => 0,
                            'notes' => $dscription,
                            'customer_id' => @$customer_info->customer_id,
                            'side_mark' => $customer_sidemark,
                            'order_date' => $order_data,
                            'order_stage' => $order_stage
                        );
                    }
                }

                $count++;
            }



            foreach ($productData as $key => $value) {

                $producDetails = [];

                $grand_total = 0;
                $subtotal = 0;
                $order_id = '';
                $customer_id = '';
                $side_mark = '';
                $order_date = '';

                foreach ($value as $key => $product) {

                    $product = (object) ($product);

                    $producDetails[] = array(
                        'order_id' => $product->order_id,
                        'product_id' => $product->product_id,
                        'room' => $product->room,
                        'product_qty' => $product->product_qty,
                        'list_price' => $product->list_price,
                        'discount' => $product->discount,
                        'unit_total_price' => $product->unit_total_price,
                        'category_id' => $product->category_id,
                        'pattern_model_id' => $product->pattern_model_id,
                        'color_id' => $product->color_id,
                        'width' => $product->width,
                        'height' => $product->height,
                        'height_fraction_id' => $product->height_fraction_id,
                        'width_fraction_id' => $product->width_fraction_id,
                        'notes' => $product->notes
                    );


                    $order_id = $product->order_id;
                    $customer_id = $product->customer_id;
                    $side_mark = $product->side_mark;
                    $grand_total += $product->unit_total_price;
                    $subtotal += $product->unit_total_price;
                    $order_date = $product->order_date;
                    $order_stage = $product->order_stage;

                    if($order_stage==2 || $order_stage==7){

                        $paid_amount = $grand_total;
                        $due = 0;

                    }else{

                        $paid_amount = 0;
                        $due = $grand_total;

                    }
                }


                $orderdata = array(
                    'order_id' => $order_id,
                    'customer_id' => $customer_id,
                    'side_mark' => $side_mark,
                    'is_different_shipping' => 0,
                    'state_tax' => 0,
                    'shipping_charges' => 0,
                    'installation_charge' => 0,
                    'invoice_discount' => 0,
                    'other_charge' => 0,
                    'misc' => 0,
                    'grand_total' => $grand_total,
                    'subtotal' => $subtotal,
                    'paid_amount' => $paid_amount,
                    'due' => $due,
                    'order_status' => 1,
                    'order_stage' => $order_stage,
                    'level_id' => $this->level_id,
                    'created_by' => $this->level_id,
                    'order_date' => $order_data,
                    'created_date' => date('Y-m-d')
                );
                
                if ($this->db->insert('quatation_tbl', $orderdata)) {

                    $this->db->insert_batch('qutation_details', $producDetails);
                }
            }

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order imported successfully!</div>");
            redirect('c_level/order_bulk_upload');

            
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Invalid!</div>");
            redirect('c_level/order_bulk_upload');
        }
    }

}
