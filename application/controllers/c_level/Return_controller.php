<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Return_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
//        if ($session_id == '' || $user_type != 'c') {
//            redirect('c-level-logout');
//        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Setting_model');
        $this->load->model('b_level/Product_model');
        $this->load->model('c_level/Return_model');
        $this->load->model('b_level/settings');
    }

//=========== its for c_customer_order_return ===========
    public function c_customer_order_return($order_id) {
        $data['orders'] = $this->Return_model->get_order_info($order_id);
        $data['order_details'] = $this->Return_model->get_order_details($order_id);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/returns/customer_order_return_form', $data);
        $this->load->view('c_level/footer');
    }

//    ========= its for c_customer_order_return_save ==========
    public function c_customer_order_return_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "customer order return information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $notifications_data = array(
            'notification_text' => 'Product returned Successfully done',
            'go_to_url' => 'b-customer-return',
            'created_by' => $this->user_id,
            'date' => date('Y-m-d'),
        );
        $this->db->insert('b_notification_tbl', $notifications_data);

        $product_id = $this->input->post('product_id');
        $return_qty = $this->input->post('return_qty');
        $return_comments = $this->input->post('return_comments');
        $customer_id = $this->input->post('customer_id');
        $order_id = $this->input->post('order_id');
        $is_approved = 0;
        $approved_by = 0;

        $order_return_data = array(
            'order_id' => $order_id,
            'customer_id' => $customer_id,
            'return_date' => date('Y-m-d'),
            'return_comments' => $return_comments,
            'returned_by' => $this->user_id,
            'is_approved' => $is_approved,
            'approved_by' => $approved_by,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
            'create_date' => date('Y-m-d h:i:s'),
        );
        $check_order_id = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $order_id)->get()->row();
        if ($check_order_id->order_id) {
            $this->db->where('order_id', $order_id)->update('order_return_tbl', $order_return_data);
            $return_id = $check_order_id->return_id;
        } else {
            $this->db->insert('order_return_tbl', $order_return_data);
            $return_id = $this->db->insert_id();
        }
//        echo '<pre>';        print_r($check_order_id);        echo '<pre>';
//        print_r($order_return_data);        die();
        for ($i = 0; $i < count($product_id); $i++) {
            $order_return_details = array(
                'return_id' => $return_id,
                'product_id' => $product_id[$i],
                'return_qty' => $return_qty[$i],
            );
            if ($return_qty[$i] != '') {
                $this->db->insert('order_return_details', $order_return_details);
            }
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Returned successfully!</div>");
        redirect("manage-order");
    }

    //    ==========its for own user customer count =========
    public function own_level_returned_order_count($level_id) {
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('order_return_tbl');
        return $num_rows;
    }

    //    =========== its for customer_return ===============
    public function customer_return() {
        $this->permission_c->check_label('customer_return')->create()->redirect();
        $config["base_url"] = base_url('c_level/Return_controller/customer_return');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_level_returned_order_count($this->level_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["customer_return"] = $this->Return_model->customer_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/customer_return');
        $this->load->view('c_level/footer');
    }

//    ============= its for c_show_return_view ===========
    public function c_show_return_view() {
        $data['return_id'] = $this->input->post('return_id');
        $data['order_details'] = $this->db->select('*')
                        ->from('order_return_details a')
                        ->join('product_tbl b', 'b.product_id = a.product_id')
                        ->where('a.return_id', $data['return_id'])
                        ->where('a.replace_qty', 0)
                        ->get()->result();
//        echo $this->db->last_query();
//        echo '<pre>';        print_r($data['order_details']);
        $this->load->view('c_level/returns/show_return_view', $data);
    }

//    ============= its for c_show_return_resend ===========
    public function c_show_return_resend() {
        $data['return_id'] = $this->input->post('return_id');
        $data['order_id'] = $this->input->post('order_id');
        $data['order_details'] = $this->db->select('*')
                        ->from('order_return_details a')
                        ->join('product_tbl b', 'b.product_id = a.product_id')
                        ->where('a.return_id', $data['return_id'])
                        ->where('a.replace_qty', 0)
                        ->get()->result();


        $this->load->view('c_level/returns/return_resend_form', $data);
    }

//    ============ its for return_resend_comment =============
    public function return_resend_comment() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "return resend comment information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $product_id = $this->input->post('product_id');
        $replace_qty = $this->input->post('replace_qty');
        $return_id = $this->input->post('return_id');
        $resend_comment = $this->input->post('resend_comment');
        $resend_date = date('Y-m-d');
        $resend_data = array(
            'approved_by' => $this->user_id,
            'resend_comment' => $resend_comment,
            'resend_date' => $resend_date,
            'is_approved' => 1,
        );
        $this->db->where('return_id', $return_id);
        $this->db->update('order_return_tbl', $resend_data);

        for ($i = 0; $i < count($product_id); $i++) {
            $order_return_details = array(
                'return_id' => $return_id,
                'product_id' => $product_id[$i],
                'replace_qty' => $replace_qty[$i],
                'replace_date' => date('Y-m-d'),
            );
            if ($replace_qty[$i] != '') {
                $this->db->insert('order_return_details', $order_return_details);
            }
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Resend successfully!</div>");
        redirect("customer-return");
    }

    //    ==========its for own user customer count =========
    public function own_level_returned_purchase_count($level_id) {
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('b_level_quatation_tbl');
        return $num_rows;
    }

    //    =========== its for customer_return ===============
    public function purchase_return() {
        $this->permission_c->check_label('purchase_return')->create()->redirect();
//        $this->permission_c->check_label('customer_return')->create()->redirect();
        $config["base_url"] = base_url('c_level/Return_controller/purchase_return');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_level_returned_purchase_count($this->level_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["orderd"] = $this->Return_model->purchase_order($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/purchase_return');
        $this->load->view('c_level/footer');
    }

//    ============= its for c_order_return_detials_show =============
    public function c_order_return_detials_show($order_id) {
        $data['company_profile'] = $this->settings->company_profile();
        $data['orders'] = $this->Return_model->get_purchase_order_info($order_id);
//        sum(b.return_qty) as return_quantity, sum(b.replace_qty) as replace_quantity
        $data['return_detais_info'] = $this->db->select('b.*, c.product_name, max(b.replace_date) as replace_date, sum(b.return_qty) as return_quantity, sum(b.replace_qty) as replace_quantity')
                        ->from('order_return_tbl a')
                        ->join('order_return_details b', 'b.return_id = a.return_id', 'left')
                        ->join('product_tbl c', 'c.product_id = b.product_id', 'left')
                        ->where('a.order_id', $order_id)
                        ->group_by('b.product_id')
                        ->order_by('b.id', 'desc')
                        ->get()->result();
//        echo $this->db->last_query();
        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/c_purchase_return_detials_show');
        $this->load->view('c_level/footer');
    }

    //=========== its for c_purchase_order_return ===========
    public function c_purchase_order_return($order_id) {
        $data['orders'] = $this->Return_model->get_purchase_order_info($order_id);
        $data['order_details'] = $this->Return_model->get_purchase_order_details($order_id);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/returns/c_purchase_order_return', $data);
        $this->load->view('c_level/footer');
    }

    //    ========= its for c_customer_order_return_save ==========
    public function c_customer_purchase_order_return_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "purchase order return information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $product_id = $this->input->post('product_id');
        $return_qty = $this->input->post('return_qty');
        $return_comments = $this->input->post('return_comments');
        $customer_id = $this->input->post('customer_id');
        $order_id = $this->input->post('order_id');
        $is_approved = 0;
        $approved_by = 0;

        $order_return_data = array(
            'order_id' => $order_id,
            'customer_id' => $customer_id,
            'return_date' => date('Y-m-d'),
            'return_comments' => $return_comments,
            'returned_by' => $this->user_id,
            'is_approved' => $is_approved,
            'approved_by' => $approved_by,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
            'create_date' => date('Y-m-d h:i:s'),
        );
        $check_order_id = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $order_id)->get()->row();
        if ($check_order_id->order_id) {
            $this->db->where('order_id', $order_id)->update('order_return_tbl', $order_return_data);
            $return_id = $check_order_id->return_id;
        } else {
            $this->db->insert('order_return_tbl', $order_return_data);
            $return_id = $this->db->insert_id();
        }
        for ($i = 0; $i < count($product_id); $i++) {
            $order_return_details = array(
                'return_id' => $return_id,
                'product_id' => $product_id[$i],
                'return_qty' => $return_qty[$i],
            );
            $this->db->insert('order_return_details', $order_return_details);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Returned successfully!</div>");
        redirect("c-purchase-return");
    }

}
