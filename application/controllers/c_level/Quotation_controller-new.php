<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        // if ($session_id == '' || $user_type != 'c') {
        //    redirect('c-level-logout');
        // }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('b_level/settings');
    }

    public function index() {
        
    }

    public function delete_quotation($quote_id) {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "quatation information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('qt_id', $quote_id)->delete('c_quatation_table');
        $this->db->where('fk_qt_id', $quote_id)->delete('c_quatation_details');

        $this->session->set_flashdata('success', "<div class='alert alert-success msg'>Data updated successfully!</div>");
        redirect('c_level/quotation_controller/manage_quotation');
    }

    public function quotation_receipt($quote_id) {

        $data['company_profile'] = $this->settings->company_profile();

        $data['quotation'] = $this->db->select("c_quatation_table.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->where('qt_id', $quote_id)
                        ->get('c_quatation_table')->row();

        $data['quotation_details'] = $this->db->select("c_quatation_details.*")
                        ->where('fk_qt_id', $quote_id)
                        ->get('c_quatation_details')->result();


        $data['categories'] = [];

        for ($i = 0; $i < sizeof($data['quotation_details']); $i++) {
            $categories = json_decode($data['quotation_details'][$i]->win_categories);
            for ($j = 0; $j < sizeof($categories); $j++) {
                if (!in_array($categories[$j]->win_category_name, $data['categories'])) {
                    array_push($data['categories'], $categories[$j]->win_category_name);
                }
            }
        }

        $roomWiseArray = [];
        $newRoomArray = [];
        $currentRoom = null;
        $totalWindow = sizeOf($data['quotation_details']);

        for ($i = 0; $i < sizeOf($data['quotation_details']); $i++) {
            if (($currentRoom !== null && $data['quotation_details'][$i]->room_no !== $currentRoom)) {
                array_push($roomWiseArray, $newRoomArray);
                $newRoomArray = [];
            }
            array_push($newRoomArray, $data['quotation_details'][$i]);
            $currentRoom = $data['quotation_details'][$i]->room_no;
        }

        if (sizeOf($newRoomArray) > 0) {
            array_push($roomWiseArray, $newRoomArray);
            $newRoomArray = [];
        }

        $data['roomWiseArray'] = $roomWiseArray;

        // echo "<pre>";
        // print_r($data['roomWiseArray']);
        // print_r($data['quotation_details']);
        // exit; 

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/quotation_receipt', $data);
        $this->load->view('c_level/footer');

        // $data['company_profile'] = $this->settings->company_profile();
        // $data['quotation'] = $this->db->select("quote_comparison_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
        //                 ->join('customer_info', 'customer_info.customer_id=quote_comparison_tbl.customer_id', 'left')
        //                 ->where('quote_id', $quote_id)
        //                 ->get('quote_comparison_tbl')->row();
        // $data['room'] = $this->db->where('quote_id', $quote_id)->group_by('room_name')->get('quote_comparison_details')->result();
        // $data['cat'] = $this->db->select('category_tbl.category_name,category_tbl.category_id,quote_comparison_details.quote_id')
        //                 ->join('category_tbl', 'category_tbl.category_id=quote_comparison_details.category_id')
        //                 ->where('quote_id', $quote_id)->group_by('quote_comparison_details.category_id')->get('quote_comparison_details')->result();
        // $data['quotation_details'] = $this->db->select("quote_comparison_details.*,category_tbl.*")
        //                 ->join('category_tbl', 'category_tbl.category_id=quote_comparison_details.category_id', 'left')
        //                 ->where('quote_id', $quote_id)
        //                 ->get('quote_comparison_details')->result();
        // $this->load->view('c_level/header');
        // $this->load->view('c_level/sidebar');
        // $this->load->view('c_level/quotation/quotation_receipt', $data);
        // $this->load->view('c_level/footer');
    }

    public function manage_quotation() {
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();

        $data['quotation'] = $this->db->select("c_quatation_table.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->get('c_quatation_table')->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/manage_quotation', $data);
        $this->load->view('c_level/footer');


        // $data['get_customer'] = $this->Order_model->get_customer();
        // $data['categories'] = $this->Order_model->get_category();
        // $data['company_profile'] = $this->settings->company_profile();
        // $data['quotation'] = $this->db->select("quote_comparison_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
        //                 ->join('customer_info', 'customer_info.customer_id=quote_comparison_tbl.customer_id', 'left')
        //                 ->get('quote_comparison_tbl')->result();
        // $this->load->view('c_level/header');
        // $this->load->view('c_level/sidebar');
        // $this->load->view('c_level/quotation/manage_quotation', $data);
        // $this->load->view('c_level/footer');
    }

    // public function edit_quotation($quote_id) {
    //     $data['get_customer'] = $this->Order_model->get_customer();
    //     $data['categories'] = $this->Order_model->get_category();
    //     $data['company_profile'] = $this->settings->company_profile();
    //     $data['qtjs'] = "c_level/quotation/qt.php";
    //     $data['quotation'] = $this->db->select("quote_comparison_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
    //     ->join('customer_info','customer_info.customer_id=quote_comparison_tbl.customer_id','left')
    //     ->where('quote_id',$quote_id)
    //     ->get('quote_comparison_tbl')->row();
    //     $this->load->view('c_level/header');
    //     $this->load->view('c_level/sidebar');
    //     $this->load->view('c_level/quotation/edit_quotation',$data);
    //     $this->load->view('c_level/footer');
    // }




    public function add_new_quotation() {

        $data['get_customer'] = $this->Order_model->get_customer();

        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();
        $data['qtjs'] = "c_level/quotation/qt.php";

        $data['rooms'] = $this->db->get('rooms')->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/add_new_quotation', $data);
        $this->load->view('c_level/footer');
    }

    public function get_color_partan_model($product_id) {

        $pp = $this->db->select('*')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();


        $patter_ids = explode(',', @$pp->pattern_models_ids);

        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->get('pattern_model_tbl')->result();




        $q = '<option value="">-- select pattern --</option>';
        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }

        if ($pp->price_style_type == 3) {
            $main_price = $pp->fixed_price;
        } elseif ($pp->price_style_type == 2) {
            $main_price = $pp->sqft_price;
        } else {
            $main_price = 0;
        }

        $arr = array('price' => $main_price, 'pattern' => $q);
        echo json_encode($arr);
    }

    public function get_product_by_category($category_id) {

        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)->where('subcategory_id', 0)
                        ->get('product_tbl')->result();

        $q = '';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }

    public function customer_wise_sidemark($customer_id = null) {

        $customer_wise_sidemark = $this->db->select('customer_info.*,c_us_state_tbl.tax_rate')
                        ->from('customer_info')
                        ->join('c_us_state_tbl', 'c_us_state_tbl.state_name=customer_info.state', 'left')
                        ->where('customer_info.customer_id', $customer_id)
                        ->get()->row();
        echo json_encode($customer_wise_sidemark);
    }

    public function price_call($width = NULL, $height = NULL, $product_id = NULL, $patter_id = NULL) {


        if (isset($product_id) && !empty($height) && !empty($width)) {

            $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();

            $q = "";
            $price = "";
            if ($p->price_style_type == 1) {

                $prices = $this->db->where('style_id', $p->price_rowcol_style_id)
                                ->where('row =', $width)
                                ->where('col =', $height)
                                ->get('price_style')->row();


                if (isset($prices) && $prices->price != 0) {

                    $price = $pc = $prices->price;
                } else {

                    $prices = $this->db->where('style_id', $p->price_rowcol_style_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();


                    $pc = ($prices != NULL ? $prices->price : 0);
                    $price = $pc;
                };
            } elseif ($p->price_style_type == 2) {

                $price = $p->sqft_price;
            } elseif ($p->price_style_type == 3) {

                $price = $p->fixed_price;
            } elseif ($p->price_style_type == 4) {

                if ($patter_id != NULL) {

                    $pg = $this->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $patter_id)->get()->row();

                    $price = $this->db->where('style_id', $pg->group_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();

                    $pc = ($price != NULL ? $price->price : 0);
                    $price = $pc;
                } else {
                    $price = 0;
                }
            } else {

                $price = 0;
            }


            $arr = array('price' => $price);

            echo json_encode($arr);
        }
    }

    public function upcharge($category) {

        $q = '';

        if ($category == 'Shutters') {

            $q .= '<tr>
                <td>
                    <p class="text-danger">Specialty shapes</p>
                    No of Panel 
                </td>
                <td colspan="2"><input type="number" name="no_panel" id="no_panel" class=" text-right"> X $150 each panel = $<b class="plan_cost"></b>
                    <input type="hidden" name="plan_price" value="0" id="plan_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Chair Rail Cut</p>
                    No of window
                </td>
                <td colspan="2"><input type="number" name="no_window" id="no_window" class="text-right"> X $30 each window = $<b class="window_cost"></b> 
                    <input type="hidden" name="window_price" value="0" id="window_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Door handle Cut</p>
                    No of Door
                </td>
                <td colspan="2"><input type="number" name="no_door" id="no_door" class="text-right"> X $150 per Door = $<b class="door_cost"></b>
                    <input type="hidden" name="door_price" value="0" id="door_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Build out</p>
                    No of Shutters
                </td>
                <td colspan="2"><input type="number" name="no_shutters" id="no_shutters" class="text-right"> X $30 each = $<b class="shutters_cost"></b>
                    <input type="hidden" name="shutters_price" value="0" id="shutters_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">T- Post</p>
                    No of window 
                </td>
                <td colspan="2"><input type="number" name="no_t_window" id="no_t_window" class="text-right"> X $40 each = $<b class="t_window_cost"></b>
                    <input type="hidden" name="t_window_price" value="0" id="t_window_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Blinds') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Upgraded Valance</p>
                    No of Blinds
                </td>
                <td colspan="2"><input type="number" name="no_blinds" id="no_blinds" class=" text-right"> X $4 each = $<b class="blind_cost"></b> 
                <input type="hidden" name="blinds_price" value="0" id="blinds_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Shades') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Fabric Wraped Cassette</p>
                    Macro
                </td>
                <td colspan="2"><input type="number" name="macro" id="Macro" class=" text-right"> X 10% = $<b class="Macro_cost"></b> 
                    <input type="hidden" name="macro_price" value="0" id="macro_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Arches') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Custom Paint</p>
                    Macro
                </td>
                <td colspan="2"><input type="number" name="custom_paint" id="custom_paint" class=" text-right"> X 20% = $<b class="custom_paint_cost"></b> 
                <input type="hidden" name="custom_paint_price" value="0" id="custom_paint_price" class="up_price"/>
                </td>
            </tr>';
        }

        echo $q;
    }

    public function add_to_cart1() {

        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $product_id = $this->input->post('product_id');
        $patter_id = $this->input->post('patter_id');
        $price = $this->input->post('price');

        $upcharge = $this->input->post('upcharge');
        $room_sub_price = $this->input->post('room_sub_price');
        $room_name = $this->input->post('room_name');
        $category_id = $this->input->post('category_id');
        $category_name = $this->input->post('category_name');


        if ($category_name == 'Shutters') {

            $upchargeData = array(
                'no_panel' => $this->input->post('no_panel'),
                'panel_price' => $this->input->post('plan_price'),
                'no_window' => $this->input->post('no_window'),
                'window_price' => $this->input->post('window_price'),
                'no_door' => $this->input->post('no_door'),
                'door_price' => $this->input->post('door_price'),
                'no_shutters' => $this->input->post('no_shutters'),
                'shutters_price' => $this->input->post('shutters_price'),
                'no_t_window' => $this->input->post('no_t_window'),
                't_window_price' => $this->input->post('t_window_price')
            );
        } elseif ($category_name == 'Blinds') {

            $upchargeData = array(
                'no_blinds' => $this->input->post('no_blinds'),
                'blinds_price' => $this->input->post('blinds_price')
            );
        } elseif ($category_name == 'Shades') {

            $upchargeData = array(
                'macro' => $this->input->post('macro'),
                'macro_price' => $this->input->post('macro_price')
            );
        } elseif ($category_name == 'Arches') {

            $upchargeData = array(
                'custom_paint' => $this->input->post('custom_paint'),
                'custom_paint_price' => $this->input->post('custom_paint_price')
            );
        }




        $qty = count($product_id);
        $id = str_replace(' ', '_', $room_name) . $qty . $category_id;

        $product_data = [];
        foreach ($product_id as $key => $pid) {
            $product_data[] = array(
                'width' => $width[$key],
                'height' => $height[$key],
                'product_id' => $pid,
                'patter_id' => $patter_id[$key],
                'price' => $price[$key]
            );
        }


        $cartData = array(
            'id' => $id,
            'product_id' => $id,
            'name' => $room_name,
            'category_name' => $category_name,
            'category_id' => $category_id,
            'price' => $room_sub_price + $upcharge,
            'qty' => $qty,
            'product_data' => json_encode($product_data),
            'upchargeData' => json_encode($upchargeData),
            'upcharge' => $upcharge,
            'room_price' => $room_sub_price
        );

        $this->cart->insert($cartData);
        echo 1;
    }

    //-------------------------------------
    // Clear Cart 
    // ------------------------------------
    public function clear_cart() {
        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Cleare successfully! </div>");
        redirect('c_level/quotation_controller/add_new_quotation');
    }

    // END--------------------------------
    //---------------------------------------
    // Delete Cart item
    // --------------------------------------
    public function delete_cart_item($rowid) {
        $this->cart->remove($rowid);
        echo 1;
    }

    public function save_quotation() {
        $data = $this->input->post();
        $cartItems = json_decode($this->session->userdata('cartItems'));
        foreach ($data['win_discount_amt'] as $key => $value) {
            $cartItems[$key]->win_discount_amt = $value;
        }
        $newCartItems = [];
        $newCartItemsByWindow = [];
        $newCartItemsCategoryArray = [];
        $RoomCount = NULL;
        $WinCount = NULL;

        for ($i = 0; $i < sizeof($cartItems); $i++) {
            if ($RoomCount !== NULL && $RoomCount !== $cartItems[$i]->room_no || $WinCount !== NULL && $RoomCount === $cartItems[$i]->room_no && $WinCount !== $cartItems[$i]->win_no) {
                $newCartItemsByWindow['win_categories'] = json_encode($newCartItemsCategoryArray);
                array_push($newCartItems, $newCartItemsByWindow);
                $newCartItemsByWindow = [];
                $newCartItemsCategoryArray = [];
            }
            if ($RoomCount === $cartItems[$i]->room_no && $WinCount === $cartItems[$i]->win_no) {
                array_push($newCartItemsCategoryArray, array(
                    'win_category_name' => $cartItems[$i]->win_category_name,
                    'win_product' => $cartItems[$i]->win_product,
                    'win_product_name' => $cartItems[$i]->win_product_name,
                    'win_pattern' => $cartItems[$i]->win_pattern,
                    'win_pattern_name' => $cartItems[$i]->win_pattern_name,
                    'win_price' => $cartItems[$i]->win_price,
                    'win_upcharges_array' => json_encode($cartItems[$i]->win_upcharges_array),
                    'win_upcharges' => $cartItems[$i]->win_upcharges,
                    'win_discount_amt' => $cartItems[$i]->win_discount_amt
                ));
            } else {
                $newCartItemsCategoryArray = [];
                $newCartItemsByWindow['room_no'] = $cartItems[$i]->room_no;
                $newCartItemsByWindow['room_name'] = $cartItems[$i]->room_name;
                $newCartItemsByWindow['win_no'] = $cartItems[$i]->win_no;
                $newCartItemsByWindow['win_width'] = $cartItems[$i]->win_width;
                $newCartItemsByWindow['win_height'] = $cartItems[$i]->win_height;
                array_push($newCartItemsCategoryArray, array(
                    'win_category_name' => $cartItems[$i]->win_category_name,
                    'win_product' => $cartItems[$i]->win_product,
                    'win_product_name' => $cartItems[$i]->win_product_name,
                    'win_pattern' => $cartItems[$i]->win_pattern,
                    'win_pattern_name' => $cartItems[$i]->win_pattern_name,
                    'win_price' => $cartItems[$i]->win_price,
                    'win_upcharges_array' => json_encode($cartItems[$i]->win_upcharges_array),
                    'win_upcharges' => $cartItems[$i]->win_upcharges,
                    'win_discount_amt' => $cartItems[$i]->win_discount_amt
                ));
            }
            $RoomCount = $cartItems[$i]->room_no;
            $WinCount = $cartItems[$i]->win_no;
        }

        if (sizeof($newCartItemsByWindow) > 0) {
            $newCartItemsByWindow['win_categories'] = json_encode($newCartItemsCategoryArray);
            array_push($newCartItems, $newCartItemsByWindow);
            $newCartItemsByWindow = [];
            $newCartItemsCategoryArray = [];
        }


        $quotTblData = array(
            'qt_cust_id' => $data['customer_id'],
            'qt_cust_type' => $data['customertype'],
            'qt_subtotal' => $data['subtotal'],
            'qt_tax' => $data['tax'],
            'qt_grand_total' => $data['grand_total'],
            'qt_order_date' => date("Y-m-d", strtotime($data['order_date'])),
            'qt_created_on' => date("Y-m-d H:i:s"),
            'qt_modified_on' => date("Y-m-d H:i:s"),
            'qt_created_by' => $this->session->userdata('user_id')
        );
        $this->db->insert('c_quatation_table', $quotTblData);
        $quote_id = $this->db->insert_id();
        for ($i = 0; $i < sizeof($newCartItems); $i++) {
            $newCartItems[$i]['fk_qt_id'] = $quote_id;
            $newCartItems[$i]['win_created_on'] = date("Y-m-d H:i:s");
            $newCartItems[$i]['win_modified_on'] = date("Y-m-d H:i:s");
            $newCartItems[$i]['win_created_by'] = $this->session->userdata('user_id');
        }
        $this->db->insert_batch('c_quatation_details', $newCartItems);
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order add successfully</div>");
        redirect("c_level/quotation_controller/add_new_quotation");
        die();
        // // ============ its for access log info collection ===============
        // $action_page = $this->uri->segment(3);
        // $action_done = "inserted";
        // $remarks = "quatation information saved";
        // $accesslog_info = array(
        //     'action_page' => $action_page,
        //     'action_done' => $action_done,
        //     'remarks' => $remarks,
        //     'user_name' => $this->user_id,
        //     'level_id' => $this->level_id,
        //     'ip_address' => $_SERVER['REMOTE_ADDR'],
        //     'entry_date' => date("Y-m-d H:i:s"),
        // );
        // $this->db->insert('accesslog', $accesslog_info);
        // // ============== close access log info =================
        // $quotData = array(
        //     'customer_id' => $this->input->post('customer_id'),
        //     'grand_total' => $this->input->post('grand_total'),
        //     'sub_total' => $this->input->post('subtotal'),
        //     'sales_tax' => $this->input->post('tax'),
        //     'created_date' => $this->input->post('order_date'),
        //     'create_by' => $this->session->userdata('user_id')
        // );
        // if ($this->db->insert('quote_comparison_tbl', $quotData)) {
        //     $quote_id = $this->db->insert_id();
        //     $room_name = $this->input->post('room_name');
        //     $category_id = $this->input->post('category_id');
        //     $product_data = $this->input->post('product_data');
        //     $upchargeData = $this->input->post('upchargeData');
        //     $product_price = $this->input->post('room_price');
        //     $subtotal = $this->input->post('list_price');
        //     $quote_price = $this->input->post('utprice');
        //     $discount = $this->input->post('discount');
        //     $upcharge = $this->input->post('upcharge');
        //     $quotDetails = [];
        //     for ($i = 0; $i < count($room_name); $i++) {
        //         $pp = json_decode($product_data[$i]);
        //         $quotDetails[] = array(
        //             'quote_id' => $quote_id,
        //             'room_name' => $room_name[$i],
        //             'width' => $pp[0]->width,
        //             'height' => $pp[0]->height,
        //             'category_id' => $category_id[$i],
        //             'product_data' => $product_data[$i],
        //             'upcharge_data' => $upchargeData[$i],
        //             'product_price' => $product_price[$i],
        //             'subtotal' => $subtotal[$i],
        //             'quote_price' => $quote_price[$i],
        //             'upcharge' => $upcharge[$i],
        //             'discount' => $discount[$i]
        //         );
        //     }
        //     $this->db->insert_batch('quote_comparison_details', $quotDetails);
        // }
        // $this->cart->destroy();
    }

    /** start added by sm */
    public function getModelsByCategory($category_id) {
        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)->where('subcategory_id', 0)
                        ->get('product_tbl')->result();
        $q = '';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        return $q;
    }

    public function getPatternByModel($model_id) {

        $pp = $this->db->select('*')
                        ->where('product_id', $model_id)
                        ->get('product_tbl')->row();
        $patter_ids = explode(',', @$pp->pattern_models_ids);
        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->get('pattern_model_tbl')->result();
        $q = '<option value="">-- select pattern --</option>';
        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }
        if ($pp->price_style_type == 3) {
            $main_price = $pp->fixed_price;
        } elseif ($pp->price_style_type == 2) {
            $main_price = $pp->sqft_price;
        } else {
            $main_price = 0;
        }

        $arr = array('price' => $main_price, 'pattern' => $q);
        echo json_encode($arr);
    }

    public function getRoomTypeDP() {
        $rooms = $this->db->get('rooms')->result();
        $html = '';
        foreach ($rooms as $room) {
            $html .= "<option value='$room->room_name'>$room->room_name</option>";
        }
        return $html;
    }

    public function getCategoryDP() {
        $categories = $this->Order_model->get_category();
        $html = '<option>--Select Categry--</option>';
        foreach ($categories as $category) {
            $html .= "<option value='$category->category_id'>$category->category_name</option>";
        }

        echo $html;
        exit();
    }

    public function getRoomRow() {
        $room_cnt = $this->input->post('room_cnt');
        $room_cnt_ro = $room_cnt + 1;
        $html = '<div class="row room_' . $room_cnt . ' col-md-12">
                                <div class="row col-md-12">
                                    <div class="form-group col-md-6"><h2>Room ' . $room_cnt_ro . ' </h2>';
        $html .= '<select class="form-control select2" name="room_name[' . $room_cnt . ']" id="room_name_' . $room_cnt . '" onchange="reload()" required="" data-placeholder="-- select one --"><option value="">--Select room type--</option>';
        $html .= $this->getRoomTypeDP();
        $html .= '</select>';
        $html .= '</div>
                                    <div class="form-group col-md-6"> <label>&nbsp;</label>
                                        <button type="button" class="btn btn-xs btn-success pull-right right add_window_btn" data-room_cnt="' . $room_cnt . '" >+ Add Window</button>
                                    </div>
                                </div>
                                <div class="no_of_window_div row col-md-12">

                                </div>
                            </div>';
        echo json_encode(array('html' => $html));
        exit();
    }

    public function getWindowRow() {
        $room_cnt = $this->input->post('room_cnt');
        $window_cnt = $this->input->post('window_cnt');
        $window_cnt_ro = $window_cnt + 1;
        $html = '<div class="row detail_row_' . $window_cnt . ' col-md-12">
                            <div class="col-md-12"><h3>Window ' . $window_cnt_ro . '</h3></div>
                            <div class="form-group col-md-4">
                                <label for="room_name" class="col-form-label">Category<span class="text-danger">*</span></label>';
        $html .= '<select class="form-control select2 category_dp" name="category_id[' . $room_cnt . '][' . $window_cnt . ']" onchange="reload()" data-room_no = ' . $room_cnt . ' data-window_no=' . $window_cnt . ' required="" data-placeholder="-- select one --"><option value="">--Select category-</option>';
        $html .= $this->getCategoryDP();
        $html .= '</select>';
        $html .= '</div>
                            <div class="form-group col-md-8 window_details_' . $room_cnt . '_' . $window_cnt . '">

                            </div>
                        </div><div class="col-md-1"><span class="delete_room" data-room_no="' . $room_cnt . '">X</span></div>';
        echo json_encode(array('html' => $html));
        exit();
    }

    public function getMoreRoom() {
        $room_no = $this->input->post('room_no');
        $room_cnt_ro = $room_no + 1;
        $html = '<fieldset class="rooms_section room_' . $room_no . '"> <legend>Room ' . $room_cnt_ro . '</legend><div class="row room_details_section room_section_' . $room_no . '"><div class="form-group col-md-2">';
        $html .= '<select class="form-control select2 twok" name="room_name[' . $room_no . ']"  required="" data-placeholder="-- select Room Type --"><option value="">--Select room type--</option>';
        $html .= $this->getRoomTypeDP();
        $html .= '</select>';

        $html .= '</div><div class="form-group col-md-2"><input type="number"  name="no_of_windows[' . $room_no . ']" class="form-control twok no_of_windows" value="0" data-room_no="' . $room_no . '" placeholder="No of Windows" min="0" max="100"></div><div class="height_width_sec_' . $room_no . ' sixk col-md-2"></div><div class="form-group col-md-1 add_category_section_' . $room_no . '"> <label>&nbsp;</label><button type="button" class="btn btn-xs btn-success pull-right right add_category_btn add_category_btn" data-room_no="' . $room_no . '" >+ Add Cat.</button></div></div> <span class="delete_room" data-room_no="' . $room_no . '">X</span></fieldset>';
        echo json_encode(array('html' => $html, 'room_cnt_ro' => $room_cnt_ro));
        exit();
    }

    function manageUpcharges() {

        
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $this->global['pageTitle'] = 'Upcharges :  Settings';
            $user_id = $this->session->userdata('user_id');
            $where['created_by'] = $user_id;
            $data['upcharges'] = $this->user_model->getData('c_level_upcharges', $where);

            $table = 'category_tbl';
            $categories = array();
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $categories[$ro->category_id] = $ro->category_name;
                }
            }

            $data['categories'] = $categories;
            $data['upcharge_table'] = 'c_level_upcharges';

            $data['redirect_key'] = '';
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/quotation/manage-upcharges', $data);
            $this->load->view('c_level/footer');
        }
    }

    public function getUpcharge() {
        $this->load->model('user_model');
        $user_id = $this->session->userdata('user_id');
        $category_id = $this->input->post('category_id');
        $category = $this->input->post('category_name');
        $room_no = $this->input->post('room_no');
        $window_no = $this->input->post('window_no');
        $cat_sec_no = $this->input->post('cat_sec_no');

        $html = '';

        $table = 'c_level_upcharges';
        $categories = array();
        $where['category_id'] = $category_id;
        $where['created_by'] = $user_id;

        $res = $this->user_model->getData($table, $where);
        if (!empty($res) && $res->num_rows() > 0) {
            foreach ($res->result() as $ro) {
                $html .= '<div class="form-group"><label class="label">' . $ro->name . '</label> &nbsp;&nbsp;';
                if ($ro->type == 'Amount') {
                    $index = $ro->id . '_' . $ro->type;
                    $html .= '<input type="checkbox" class="amount_calc" name="upcharges[' . $room_no . '][' . $window_no . '][' . $category_id . '][' . $index . ']" value="' . $ro->value . '">  $' . $ro->value;
                } else {
                    $index = $ro->id . '_Percantage';
                    $html .= '<input type="checkbox" class="amount_calc" name="upcharges[' . $room_no . '][' . $window_no . '][' . $category_id . '][' . $index . ']" value="' . $ro->value . '">  ' . $ro->value . '%';
                }
                $html .= '</div>';
            }
        }
        echo json_encode(array('html' => $html));
        exit();
    }

    function addUpcharges() {
        $this->load->model('user_model');
        $insertInfo['created_by'] = $this->session->userdata('user_id');
        $insertInfo['name'] = $this->input->post('name');
        $insertInfo['type'] = $this->input->post('type');
        $insertInfo['value'] = $this->input->post('value');
        $insertInfo['category_id'] = $this->input->post('category_id');
        $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
        $result = $this->user_model->addData('c_level_upcharges', $insertInfo);
        if ($result > 0) {
            $this->session->set_flashdata('success', 'Upcharge created successfully');
        } else {
            $this->session->set_flashdata('error', 'Upcharge creation failed');
        }
        redirect('c_level/quotation_controller/manageUpcharges');
    }

    function updateUpcharges() {
        $this->load->model('user_model');
        $whereInfo['created_by'] = $this->session->userdata('user_id');
        $updateInfo['name'] = $this->input->post('name');
        $updateInfo['type'] = $this->input->post('type');
        $updateInfo['value'] = $this->input->post('value');
        $updateInfo['category_id'] = $this->input->post('category_id');
        $whereInfo['id'] = $this->input->post('upcharge_id');
        // $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
        $table = 'c_level_upcharges';
        $result = $this->user_model->updateTable($table, $whereInfo, $updateInfo);
        if ($result > 0) {
            $this->session->set_flashdata('success', 'Upcharge updated successfully');
        } else {
            $this->session->set_flashdata('error', 'Upcharge updatation failed ');
        }
        redirect('c_level/quotation_controller/manageUpcharges');
    }

    function deleteRow() {
        if (0) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $this->load->model('user_model');
            $del_id = $this->input->post('del_id');
            $del_tbl = $this->input->post('del_tbl');

            $result = $this->user_model->deleteRow($del_id, $del_tbl);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    public function addToList() {
        print_r($this->input->post());
        exit;
        die('here');
        $room_name = $this->input->post('room_name');
        $i = 0;
        foreach ($room_name as $room) {
            // room $i

            $i++;
        }
    }

    public function add_to_cart() {
        $cartItems = [];
        $total_rooms = count($this->input->post('room_name'));
        $room = 0;
        for ($room = 0; $room < $total_rooms; $room++) {
            $room_name = $this->input->post('room_name[' . $room . ']');
            $total_windows = count($this->input->post('width[' . $room . ']'));
            //echo 'room'.$room.'has'.$total_windows.'no of windows<br>';
            for ($window = 0; $window < $total_windows; $window++) {
                $categories_no = count($this->input->post('category_id[' . $room . ']'));
                $cat_id = $this->input->post('category_id[' . $room . ']');
                $width = $this->input->post('width[' . $room . '][' . $window . ']');
                $height = $this->input->post('height[' . $room . '][' . $window . ']');
                //echo 'room ' . $room . ' & window ' . $window . ' has ' . $categories_no . ' no of cat<br>';
                if ($categories_no < 1) {
                    array_push($cartItems, array(
                        'room_no' => $room,
                        'room_name' => $room_name,
                        'win_no' => $window,
                        'win_width' => $width,
                        'win_height' => $height,
                        'win_cat' => "",
                        'win_category_name' => "",
                        'win_product' => "",
                        'win_product_name' => "",
                        'win_pattern' => "",
                        'win_pattern_name' => "",
                        'win_price' => "",
                        'win_upcharges_array' => null,
                        'win_upcharges' => "",
                        'list_amount' => $price
                    ));
                } else {
                    for ($category = 0; $category < $categories_no; $category++) {
                        $cat_id = $this->input->post('category_id[' . $room . '][' . $category . ']');
                        $product_id = $this->input->post('product_id[' . $room . '][' . $window . '][' . $cat_id . ']');
                        $pattern_id = $this->input->post('pattern_id[' . $room . '][' . $window . '][' . $cat_id . ']');
                        $price = $this->input->post('price_id[' . $room . '][' . $window . '][' . $cat_id . ']');
                        $upcharges = $this->input->post('upcharges[' . $room . '][' . $window . '][' . $cat_id . ']');
                        //echo ' Room ' . $room . ', Window ' . $window . ', Height ' . $height . ', Width ' . $width . ', Category ID ' . $cat_id . ' Prod ID ' . $product_id . ', Pattern ID ' . $pattern_id . ', Price ' . $price . '<br>';

                        if ($cat_id != NULL && $cat_id > 0) {
                            $selCategory = $this->db->select('*')->where('category_id', $cat_id)->get('category_tbl')->row();
                            $win_category_name = $selCategory->category_name;
                        } else {
                            $win_category_name = "";
                        }

                        if ($product_id != NULL && $product_id > 0) {
                            $selProduct = $this->db->select('*')->where('product_id', $product_id)->get('product_tbl')->row();
                            $win_product_name = $selProduct->product_name;
                        } else {
                            $win_product_name = "";
                        }

                        if ($pattern_id != NULL && $pattern_id > 0) {
                            $selPattern = $this->db->select('*')->where('pattern_model_id', $pattern_id)->get('pattern_model_tbl')->row();
                            $win_pattern_name = $selPattern->pattern_name;
                        } else {
                            $win_pattern_name = "";
                        }

                        if ($upcharges != NULL && $upcharges > 0) {
                            $win_upcharges_array = [];
                            $win_upcharges = 0;
                            foreach ($upcharges as $key => $value) {
                                $upchargesArray = explode('_', $key);
                                array_push($win_upcharges_array, array(
                                    'id' => $upchargesArray[0],
                                    'type' => $upchargesArray[1],
                                    'value' => $value
                                ));
                                if ($upchargesArray[1] == 'Amount') {
                                    $this_win_upcharges = $value;
                                } else {
                                    $this_win_upcharges = $price * ($value / 100);
                                }
                                $win_upcharges = $win_upcharges + $this_win_upcharges;
                            }
                        } else {
                            $win_upcharges = 0;
                            $win_upcharges_array = null;
                        }

                        array_push($cartItems, array(
                            'room_no' => $room,
                            'room_name' => $room_name,
                            'win_no' => $window,
                            'win_width' => $width,
                            'win_height' => $height,
                            'win_cat' => $cat_id,
                            'win_category_name' => $win_category_name,
                            'win_product' => $product_id,
                            'win_product_name' => $win_product_name,
                            'win_pattern' => $pattern_id,
                            'win_pattern_name' => $win_pattern_name,
                            'win_price' => $price,
                            'win_upcharges_array' => $win_upcharges_array,
                            'win_upcharges' => $win_upcharges,
                            'list_amount' => $price + $win_upcharges
                        ));
                    }
                }
            }
        }
        $this->session->set_userdata('cartItems', json_encode($cartItems));
        echo $this->generateCartItemsHtml($cartItems);
        die();
    }

    public function deleteCartItem() {
        $cart_item_index = $this->input->post('cart_item_index');
        $cartItems = json_decode($this->session->userdata('cartItems'));
        $newCartItems = [];
        for ($i = 0; $i < sizeof($cartItems); $i++) {
            if ($cart_item_index != $i) {
                array_push($newCartItems, json_decode(json_encode($cartItems[$i]), True));
            }
        }
        $this->session->set_userdata('cartItems', json_encode($newCartItems));
        echo $this->generateCartItemsHtml($newCartItems);
        die();
    }

    public function generateCartItemsHtml($cartItems) {
        $subtotal = 0;
        $cartItemsHtml = "";
        for ($i = 0; $i < sizeof($cartItems); $i++) {
            $cartItemsPrice = $cartItems[$i]['win_price'] + $cartItems[$i]['win_upcharges'];
            $cartItemsHtml .= "<tr>
                <td>" . ($i + 1) . "</td>
                <td>" . $cartItems[$i]['room_name'] . "</td>
                <td>" . $cartItems[$i]['win_width'] . " X " . $cartItems[$i]['win_height'] . "</td>
                <td>" . $cartItems[$i]['win_category_name'] . "</td>";


            if ($cartItems[$i]['win_category_name'] == 'Misc') {
                $cartItemsHtml .= "<td>" . $cartItems[$i]['win_product'] . "</td> ";
            } else {
                $cartItemsHtml .= "<td>" . $cartItems[$i]['win_product_name'] . "</td>";
            }

            $cartItemsHtml .= " <td>$" . $cartItems[$i]['win_price'] . "</td>
              
                <td>$" . $cartItems[$i]['win_upcharges'] . "</td>
                <td class='list-amount'>$" . $cartItems[$i]['list_amount'] . "</td>
                <td><input type='number' min='0' max='100' name='win_discount_amt[]' onchange='updatePrice(this)'/></td>
                <td class='price'>$" . $cartItemsPrice . "</td>
                <td><span class='btn btn-danger btn-sm' id='deleteCartItem' onclick='deleteCartItem(" . $i . ")'>Remove</span></td>
            </tr>";
            $subtotal = $subtotal + $cartItemsPrice;
        }
        return json_encode(array(
            'cartItemsHtml' => $cartItemsHtml,
            'subtotal' => $subtotal
        ));
    }

    public function getProductByCategory() {
        $room_no = $this->input->post('room_no');
        $category_id = $this->input->post('category_id');
        $category_name = $this->input->post('category_name');
        $window_no = $this->input->post('window_no');

        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)->where('subcategory_id', 0)
                        ->get('product_tbl')->result();

        $q = '<select name="product_id[ ' + $room_no + '][' + $window_no + '][' . $category_id . ']" class="form-control select2 threek product_id" id="product_id_' + $room_no + '_' + $window_no + '_' + $category_id + '" onchange="getPattern(' + $room_no + ',' + $window_no + ',' + $category_id + ')" required>';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        $q .= '</select>';
        echo json_encode(array('html' => $q));
        exit();
    }

    /** end added by itsea */
}
