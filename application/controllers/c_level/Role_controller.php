<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'c') {
            redirect('c-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Role_model');
    }

    public function index() {
        
    }

//    ============= its for role_permission =============== 
    public function role_permission() {
        $data['modules'] = $this->db->select('*')->from('c_menusetup_tbl')->where('status', 1)->group_by('module')->get()->result();
        $data['level_id'] = $this->level_id;
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/role/role_permission', $data);
        $this->load->view('c_level/footer');
    }

//    ========= its for module_save ==========
    public function role_save() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "role permission information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $role_name = $this->input->post('role_name');
        $description = $this->input->post('role_description');
        $roleData = array(
            'role_name' => $role_name,
            'description' => $description,
            'created_by' => $this->user_id,
            'level_id' => $level_id,
        );
        $this->db->insert('role_tbl', $roleData);
        $role_id = $this->db->insert_id();
        $module = $this->input->post('module');
        $menu_id = $this->input->post('menu_id');
        $create = $this->input->post('create');
        $read = $this->input->post('read');
        $edit = $this->input->post('edit');
        $delete = $this->input->post('delete');


        $new_array = array();
        for ($m = 0; $m < sizeof($module); $m++) {
            for ($i = 0; $i < sizeof($menu_id[$m]); $i++) {
                for ($j = 0; $j < sizeof($menu_id[$m][$i]); $j++) {
                    $dataStore = array(
                        'role_id' => $role_id,
                        'menu_id' => $menu_id[$m][$i][$j],
                        'can_create' => (!empty($create[$m][$i][$j]) ? $create[$m][$i][$j] : 0),
                        'can_edit' => (!empty($edit[$m][$i][$j]) ? $edit[$m][$i][$j] : 0),
                        'can_access' => (!empty($read[$m][$i][$j]) ? $read[$m][$i][$j] : 0),
                        'can_delete' => (!empty($delete[$m][$i][$j]) ? $delete[$m][$i][$j] : 0),
                        'created_by' => $this->user_id,
                        'level_id' => $level_id,
                    );
                    array_push($new_array, $dataStore);
                }
            }
        }

        if ($this->Role_model->create($new_array)) {
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Data save successfully!</div>");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
        }
        redirect('role-permission');
    }

    //    =========== its for role_list ==========
    public function role_list() {
//        $this->permission->check_label('role_list')->read()->redirect();
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['role_list'] = $this->Role_model->role_list($level_id);


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/role/role_list', $data);
        $this->load->view('c_level/footer');
    }

    //    =========== its for edit_role ===========
    public function role_edit($id) {

        $data['modules'] = $this->db->select('*')->from('c_menusetup_tbl')
        ->where('status', 1)
        ->where('level_id', $this->level_id)
        ->group_by('module')->get()->result();

        $data['roleInfo'] = $this->db->select("*")
                        ->from('role_tbl')
                        ->where('id', $id)
                        ->get()->row();


        $data['permissionInfo'] = $this->db->select('role_permission_tbl.*,c_menusetup_tbl.menu_title')
                        ->from('role_permission_tbl')
                        ->join('c_menusetup_tbl', 'c_menusetup_tbl.menu_id = role_permission_tbl.menu_id','left')
                        ->where('role_permission_tbl.role_id', $id)
                        ->where('role_permission_tbl.level_id', $this->level_id)
                        ->get()->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/role/role_edit', $data);
        $this->load->view('c_level/footer');
    }

//    ============= its for role update ================
    public function role_update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "role permission updated done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $role_id = $this->input->post('role_id');
        $rolData = array(
            'role_name' => $this->input->post('role_name'),
            'description' => $this->input->post('role_description'),
            'created_by' => $this->user_id,
        );
        $this->db->where('id', $role_id)->update('role_tbl', $rolData);

        //======= ==========
        $module = $this->input->post('module');
        $menu_id = $this->input->post('menu_id');
        $create = $this->input->post('create');
        $read = $this->input->post('read');
        $update = $this->input->post('edit');
        $delete = $this->input->post('delete');

        $new_array = array();
        for ($m = 0; $m < sizeof($module); $m++) {
            for ($i = 0; $i < sizeof($menu_id[$m]); $i++) {
                for ($j = 0; $j < sizeof($menu_id[$m][$i]); $j++) {
                    $dataStore = array(
                        'role_id' => $role_id,
                        'menu_id' => $menu_id[$m][$i][$j],
                        'can_create' => (!empty($create[$m][$i][$j]) ? $create[$m][$i][$j] : 0),
                        'can_edit' => (!empty($update[$m][$i][$j]) ? $update[$m][$i][$j] : 0),
                        'can_access' => (!empty($read[$m][$i][$j]) ? $read[$m][$i][$j] : 0),
                        'can_delete' => (!empty($delete[$m][$i][$j]) ? $delete[$m][$i][$j] : 0),
                        'created_by' => $this->user_id,
                    );
                    array_push($new_array, $dataStore);
                }
            }
        }

        if ($this->Role_model->create($new_array)) {
            $this->session->set_flashdata('success', "<div class='alert alert-success msg'>Data updated successfully!</div>");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger msg'>Please try again</div>");
        }
        redirect('role-list');
    }

//    ================its for c_level_check_user_role =========
    public function c_level_check_user_role() {
        $user_id = $this->input->post('user_id');
        $check_user_role = $this->db->select('*')->from('user_access_tbl a')
                        ->join('role_tbl b', 'b.id = a.role_id', 'left')
                        ->where('a.user_id', $user_id)->get()->result();
        if (empty($check_user_role)) {
            $notFound = array(array('role_name' => 'Not Found'));
            echo json_encode($notFound);
        } else {
            echo json_encode($check_user_role);
        }
    }

//    ============= its for user_roles =============== 
    public function user_roles() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['get_users'] = $this->Role_model->get_users();
        $data['role_list'] = $this->Role_model->role_list($level_id);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/role/user_roles', $data);
        $this->load->view('c_level/footer');
    }

    //    ========== its for assign_user_role_save ===============
    public function assign_user_role_save() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "assign user role information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $user_id = $this->input->post('user_id');
        $role_id = $this->input->post('role_id');

        $this->db->where('user_id', $user_id)->delete('user_access_tbl');
        for ($i = 0; $i < count($role_id); $i++) {
            $user_role = array(
                'user_id' => $user_id,
                'role_id' => $role_id[$i],
                'created_by' => $this->user_id,
                'level_id' => $level_id,
            );
            $this->db->insert('user_access_tbl', $user_role);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User role assign successfully!</div>");
        redirect('user-role');
    }

//    ============= its for access_role =============== 
    public function access_role() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['user_access_role'] = $this->Role_model->user_access_role($level_id);
        $data['role_list'] = $this->Role_model->role_list($level_id);


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/role/access_role', $data);
        $this->load->view('c_level/footer');
    }

//    ========= its for edit_user_access_role ================
    public function edit_user_access_role($access_id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['user_list'] = $this->Role_model->get_users();
        $data['role_list'] = $this->Role_model->role_list($level_id);
        $data['edit_user_access_role'] = $this->Role_model->edit_user_access_role($access_id);
        $data['assign_role'] = $this->db->select('role_id')
                        ->where('user_id', $data['edit_user_access_role']->user_id)
                        ->get('user_access_tbl')->result();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/role/edit_user_access_role', $data);
        $this->load->view('c_level/footer');
    }

//    ============== its for delete_user_access_role ==============
    public function delete_user_access_role($id) {
         //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "assign user role information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('role_acc_id', $id)->delete('user_access_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Role assign deleted successfully!</div>");
        redirect('access-role');
    }

//=========== its for assign_user_role_update ===========
    public function assign_user_role_update($role_acc_id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "update";
        $remarks = "assign user role information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $user_id = $this->input->post('user_id');
        $role_id = $this->input->post('role_id');

        $this->db->where('user_id', $user_id)->delete('user_access_tbl');
        for ($i = 0; $i < count($role_id); $i++) {
            $user_role = array(
                'user_id' => $user_id,
                'role_id' => $role_id[$i],
                'created_by' => $this->user_id,
                'level_id' => $level_id,
            );
            $this->db->insert('user_access_tbl', $user_role);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User role assign updated successfully!</div>");
        redirect('access-role');
    }

//    ============= its for role_delete -=================
    public function role_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "update";
        $remarks = "assign user role information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

        $this->db->where('id', $id)->delete('role_tbl');
        $this->db->where('role_id', $id)->delete('role_permission_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Role permission deleted successfully!</div>");
        redirect('role-list');
    }

}
