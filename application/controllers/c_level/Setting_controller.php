<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
//        if ($session_id == '' || $user_type != 'c') {
//            redirect('c-level-logout');
//        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Setting_model');
        $this->load->model('b_level/Product_model');
    }

    public function index() {
        
    }

    public function get_card_info($id) {

        $info = $this->db->where('id', $id)->where('created_by', $this->user_id)->get('c_card_info')->row();

        echo json_encode($info);
    }

    //============= its for card info =============== 
    public function card_info() {

        $this->permission_c->check_label('card info')->create()->redirect();

        $data['info'] = $this->db->where('created_by', $this->user_id)->get('c_card_info')->result();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/my_card_info', $data);
        $this->load->view('c_level/footer');
    }

    public function getCard() {

        $data = $this->db->where('created_by', $this->user_id)->where('is_active', 1)->get('c_card_info')->row();
        echo json_encode($data);
    }

    public function card_info_update() {
        $id = $this->input->post('id');

        if (isset($id) && !empty($id)) {

            $info = $this->db->where('id', $id)->where('created_by', $this->user_id)->get('c_card_info')->row();

            if ($info != NULL) {

                if ($this->input->post('is_active') == '1') {
                    $this->db->set('is_active', 0)->where('created_by', $this->user_id)->update('c_card_info');
                }
                //        ============ its for access log info collection ===============
                $action_page = $this->uri->segment(1);
                $action_done = "updated";
                $remarks = "my card information updated";
                $accesslog_info = array(
                    'action_page' => $action_page,
                    'action_done' => $action_done,
                    'remarks' => $remarks,
                    'user_name' => $this->user_id,
                    'level_id' => $this->level_id,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'entry_date' => date("Y-m-d H:i:s"),
                );
                $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
                $arrayName = array(
                    'card_number' => $this->input->post('card_number'),
                    'expiry_month' => $this->input->post('expiry_month'),
                    'expiry_year' => $this->input->post('expiry_year'),
                    'card_holder' => $this->input->post('card_holder'),
                    'created_by' => $this->user_id,
                    'is_active' => $this->input->post('is_active'),
                    'update_by' => $this->user_id,
                    'update_date' => date('Y-m-d')
                );
                $this->db->where('id', $id)->where('created_by', $this->user_id)->update('c_card_info', $arrayName);

                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Card information update successfully!</div>");
            }
        } else {
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "insert";
            $remarks = "my card information inserted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
            if ($this->input->post('is_active') == '1') {
                $this->db->set('is_active', 0)->where('created_by', $this->user_id)->update('c_card_info');
            }


            $arrayName = array(
                'card_number' => $this->input->post('card_number'),
                'expiry_month' => $this->input->post('expiry_month'),
                'expiry_year' => $this->input->post('expiry_year'),
                'card_holder' => $this->input->post('card_holder'),
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->user_id,
                'create_date' => date('Y-m-d')
            );

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Card information setup successfully!</div>");

            $this->db->insert('c_card_info', $arrayName);
        }

        redirect("c_level/setting_controller/card_info");
    }

    public function card_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "my card information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->db->where('id', $id)->where('created_by', $this->user_id)->delete('c_card_info');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Card delete successfully!</div>");

        redirect("c_level/setting_controller/card_info");
    }

//    ============= its for my_account =============== 
    public function my_account() {
        $this->permission_c->check_label('my_account')->create()->redirect();
        $data['my_account'] = $this->Setting_model->my_account();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/my_account', $data);
        $this->load->view('c_level/footer');
    }

//    ========== its for my_account_update ===========
    public function my_account_update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "my account information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $updatedate = date('Y-m-d');
        $user_id = $this->input->post('user_id');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $language = $this->input->post('language');

        // configure for upload 
        $config = array(
            'upload_path' => "./assets/c_level/uploads/appsettings/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
//            'file_name' => "SPF" . time(),
            'max_size' => '0',
            'encrypt_name' => TRUE,
        );
//
        $image_data = array();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('image')) {
            $myaccount_picture_unlink = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
            if ($myaccount_picture_unlink->user_image) {
                $img_path = FCPATH . 'assets/c_level/uploads/appsettings/' . $myaccount_picture_unlink->user_image;
                unlink($img_path);
            }

            $image_data = $this->upload->data();
//                print_r($image_data); die();
            $image_name = $image_data['file_name'];
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //get original image
            $config['maintain_ratio'] = TRUE;
            $config['height'] = '*';
            $config['width'] = '*';
//                $config['quality'] = 50;
            $this->load->library('image_lib', $config);
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
        } else {
            $image_name = $this->input->post('image_hdn');
        }

        $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'company' => $company,
            'address' => $address,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'phone' => $phone,
            'email' => $email,
            'language' => $language,
            'user_image' => $image_name,
            'updated_by' => $this->user_id,
            'update_date' => $updatedate,
        );
//        echo '<pre>';        print_r($user_data);die();
        $this->db->where('id', $user_id);
        $this->db->update('user_info', $user_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>My account updated successfully!</div>");
        redirect("c-my-account");
    }

//    ============= its for company-profile =============== 
    public function company_profile() {

        $this->permission_c->check_label('company_profile')->create()->redirect();

        $data['company_profile'] = $this->Setting_model->company_profile();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/company_profile', $data);
        $this->load->view('c_level/footer');
    }

//    ================ its for company_profile_update ===================
    public function company_profile_update() {
        $user_id = $this->input->post('user_id');
        $company_name = $this->input->post('company_name');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
//        $password = $this->input->post('password');
//        $image = $this->input->post('image');
        $email_notification = $this->input->post('email_notification');
        $sms_notification = $this->input->post('sms_notification');
        $currency = $this->input->post('currency');
//        $logo = $this->input->post('logo');
//        if ($password) {
//            $password_change = array(
//                'password' => md5($password)
//            );
//            $this->db->where('user_id', $user_id);
//            $this->db->update('log_info', $password_change);
//        }
//        ========== its for logo =============
// configure for upload 
        $config = array(
            'upload_path' => "./assets/c_level/uploads/appsettings/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
//            'file_name' => "SPF" . time(),
            'max_size' => '0',
            'encrypt_name' => TRUE,
        );
//
        $image_data = array();

        $this->load->library('upload', $config);

        $this->upload->initialize($config);
//
//        if ($this->upload->do_upload('image')) {
//            $image_data = $this->upload->data();
////                print_r($image_data); die();
//            $image_name = $image_data['file_name'];
//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $image_data['full_path']; //get original image
//            $config['maintain_ratio'] = TRUE;
//            $config['height'] = '*';
//            $config['width'] = '*';
////                $config['quality'] = 50;
//            $this->load->library('image_lib', $config);
//            $this->image_lib->clear();
//            $this->image_lib->initialize($config);
//            if (!$this->image_lib->resize()) {
//                echo $this->image_lib->display_errors();
//            }
//        } else {
//            $image_name = $this->input->post('image_hdn');
//        }
//            ========= its for logo upload =========

        if ($this->upload->do_upload('logo')) {
            $company_picture_unlink = $this->db->select('*')->from('company_profile')->where('user_id', $user_id)->get()->row();
            if ($company_picture_unlink->logo) {
                $img_path = FCPATH . 'assets/c_level/uploads/appsettings/' . $company_picture_unlink->logo;
                unlink($img_path);
            }
            $apps_logo = $this->upload->data();
//          echo '<pre>';  print_r($image_dataTwo); die();
            $logo = $apps_logo['file_name'];
        } else {
            $logo = $this->input->post('logo_hdn');
        }
//            ============ close logo upload ===========

        $web_setting = array(
            'company_name' => $company_name,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'email' => $email,
            'phone' => $phone,
            'currency' => $currency,
            'email_notification' => $email_notification,
            'sms_notification' => $sms_notification,
//            'picture' => $image_name,
            'logo' => $logo,
            'updated_by' => $this->user_id,
            'setting_status'=>1
        );
//        dd($web_setting);
        $this->db->where('user_id', $user_id);
        $this->db->update('company_profile', $web_setting);
//        =========== its for company info update by customer table ============
        $customer_data = array(
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'email' => $email,
            'phone' => $phone,
        );
        $this->db->where('customer_user_id ', $user_id);
        $this->db->update('customer_info', $customer_data);
//        ============= its for user info update ============
        $customer_data = array(
            'address' => $address,
            'company' => $company_name,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'email' => $email,
            'phone' => $phone,
        );
        $this->db->where('id ', $user_id);
        $this->db->update('user_info', $customer_data);


        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "company profile information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Company profile updated successfully!</div>");
        redirect('company-profile');
    }

//     =========== its for change_password =============
    public function change_password() {
        $this->permission_c->check_label('change_password')->create()->redirect();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/change_password');
        $this->load->view('c_level/footer');
    }

//    ============== its for update_password ============
    public function update_password() {
        
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "update";
        $remarks = "user password updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $user_id = $this->input->post('user_id');
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $old_password_check = $this->db->select('password')->from('log_info')
                        ->where('password', md5($old_password))
                        ->where('user_id', $user_id)->get()->result();
//        echo '<pre>'; print_r($old_password_check);die();
        if (!empty($old_password_check)) {
            $change_details = array(
                'password' => md5($new_password),
            );
            $this->db->where('user_id', $user_id);
            $this->db->update('log_info', $change_details);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Password changed successfully!</div>");
            redirect("c-password-change");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Old password does not match!</div>");
            redirect("c-password-change");
        }
    }

//==============its for payment_gateway ============
    public function payment_gateway() {



$this->permission_c->check_label('paypal_setting')->create()->redirect();

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['gateway_edit'] = $this->db->where('level_id', $level_id)->get('tms_payment_setting')->row();

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/payment_gateway');
        $this->load->view('c_level/footer');
    }

//    ========== its for payment_gateway_update =============
    public function payment_gateway_update() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "payment gateway information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $gatewaydata = array(
            'url' => $this->input->post('url'),
            'user_name' => $this->input->post('user_name'),
            'password' => $this->input->post('password'),
            'mode' => 0,
            'level_id' => $level_id,
        );
        $gateway_check = $this->db->where('level_id', $level_id)->get('tms_payment_setting')->row();
        if ($gateway_check) {
            $this->db->where('level_id', $level_id)->update('tms_payment_setting', $gatewaydata);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway updated successfully!</div>");
        } else {
            $this->db->insert('tms_payment_setting', $gatewaydata);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway save successfully!</div>");
        }
        redirect("payment-gateway");
    }

//    ============= its for payment_setting =============== 
    public function payment_setting() {


        $this->permission_c->check_label('paypal_setting')->create()->redirect();

        $config["base_url"] = base_url('b_level/Setting_controller/gateway');
        $config["total_rows"] = $this->db->count_all('gateway_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["gateway_list"] = $this->Setting_model->gateway_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/payment_setting');
        $this->load->view('c_level/footer');
    }

//    ============= its for c_save_gateway =====
    public function c_save_gateway() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "payment setting information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $createdate = date('Y-m-d');
        $payment_gateway = $this->input->post('payment_gateway');
        $payment_mail = $this->input->post('payment_mail');
        $currency = $this->input->post('currency');
        $mode = $this->input->post('mode');
        $gateway_data = array(
            'payment_gateway' => $payment_gateway,
            'payment_mail' => $payment_mail,
            'currency' => $currency,
            'level_type' => 'c',
            'created_by' => $level_id,
            'default_status' => '1',
            'status' => $mode,
            'created_date' => $createdate
        );

        $check_gateway = $this->db->where('created_by', $level_id)->where('level_type', 'c')->get('gateway_tbl')->result();
        if ($check_gateway) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway configuration already done!</div>");
            redirect("payment-setting");
        } else {
            $this->db->insert('gateway_tbl', $gateway_data);
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway save successfully!</div>");
        redirect("payment-setting");
    }

    public function c_gateway_edit($id) {
        $data['gateway_edit'] = $this->Setting_model->gateway_edit($id);
//        dd($data['gateway_edit']);

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/payment_setting_edit');
        $this->load->view('c_level/footer');
    }

//    ============= its for update_payment_gateway =============
    public function update_payment_gateway($id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $updatedate = date('Y-m-d');
        $payment_gateway = $this->input->post('payment_gateway');
        $payment_mail = $this->input->post('payment_mail');
        $currency = $this->input->post('currency');
        $is_active = $this->input->post('is_active');
        $mode = $this->input->post('mode');
        $gateway_data = array(
            'payment_gateway' => $payment_gateway,
            'payment_mail' => $payment_mail,
            'currency' => $currency,
            'level_type' => 'c',
            'default_status' => $is_active,
            'updated_by' => $this->user_id,
            'status' => $mode,
            'updated_date' => $updatedate
        );
//        echo '<pre>';        print_r($category_data);die();
        $this->db->where('id', $id);
        $this->db->update('gateway_tbl', $gateway_data);

        $default_status = array(
            'default_status' => 0,
        );
        $this->db->where('id !=', $id);
        $this->db->where('created_by', $level_id);
        $this->db->update('gateway_tbl', $default_status);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "payment setting information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway updated successfully!</div>");
        redirect("payment-setting");
    }

//    ============== its for gateway_delete==============
    public function gateway_delete($id) {

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "gateway information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->db->where('id', $id)->delete('gateway_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway deleted successfully!</div>");
        redirect("payment-setting");
    }

//    ============= its for cost_factor =============== 
    public function cost_factor() {
        $this->permission_c->check_label('cost_factor')->create()->redirect();
//        echo $this->level_id;die();
        $data['get_product'] = $this->Setting_model->get_product();
        $data['get_only_product_c_cost_factor'] = $this->Setting_model->get_only_product_c_cost_factor($this->level_id);

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/cost_factor');
        $this->load->view('c_level/footer');
    }

//    ============== its for c-level cost_factor_save =============
    public function cost_factor_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "cost factor information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        if($this->input->post('submit_type') != '' && $this->input->post('submit_type') == 'bulk') {
            $product_id = $this->input->post('Id_List');
            $txt_individual_cost_factor = $this->input->post('txt_individual_cost_factor');
            foreach ($product_id as $key => $p_id) {
                $data = [
                    'individual_cost_factor' => $txt_individual_cost_factor,
                    'costfactor_discount' => (100 - $txt_individual_cost_factor),
                ];
                $this->db->where('product_id', $p_id);
                $this->db->where('level_id', $this->level_id);
                $this->db->where('created_by', $this->user_id);
                $this->db->update('c_cost_factor_tbl', $data);
            }
        } else {
            $product_id = $this->input->post('product_id');
            $individual_cost_factor = $this->input->post('individual_cost_factor');
            $costfactor_discount = $this->input->post('costfactor_discount');

            $this->db->where('level_id', $this->level_id)->delete('c_cost_factor_tbl');

            //=========== its for product condition mapping ==============
            for ($i = 0; $i < count($product_id); $i++) {
                $cost_factor_data = array(
                    'product_id' => $product_id[$i],
                    'individual_cost_factor' => $individual_cost_factor[$i],
                    'costfactor_discount' => $costfactor_discount[$i],
                    'level_id' => $this->level_id,
                    'created_by' => $this->user_id,
                    'create_date' => date('Y-m-d'),
                );
                $this->db->insert('c_cost_factor_tbl', $cost_factor_data);
            }
            //=========== close product condition mapping ============
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cost factor updated successfully</div>");
        redirect("c-cost-factor");
    }

//    ============= its for iframe_code =============== 
    public function iframe_code() {

        $this->permission_c->check_label('web_iframe_code')->create()->redirect();
        $data['check_iframe_code'] = $this->Setting_model->check_iframe_code();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/iframe_code', $data);
        $this->load->view('c_level/footer');
    }

//    =============== its for iframe_code_save ==============
    public function iframe_code_save() {
        $data['check_iframe_code'] = $this->Setting_model->check_iframe_code();
//        dd($data['check_iframe_code']);
        $iframe_code = $this->input->post('iframe_code');

        if ($data['check_iframe_code']) {
            $iframe_data = array(
                'iframe_code' => $iframe_code,
                'updated_by' => $this->user_id,
                'level_type' => 'c',
            );
            $this->db->where('created_by', $this->user_id);
            $this->db->update('iframe_code_tbl', $iframe_data);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Iframe code updated successfully!</div>");
        } else {
            $iframe_data = array(
                'iframe_code' => $iframe_code,
                'created_by' => $this->user_id,
                'level_type' => 'c',
            );
            $this->db->insert('iframe_code_tbl', $iframe_data);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Iframe code inserted successfully!</div>");
        }
        redirect('iframe-code');
    }

//    ============ its for d_customer_form ============
    public function d_customer_form() {
//         $data['d_level_form'] = $this->db->select('*')->from('iframe_code_tbl')->where('frame_id', 1)->get()->result();
// //        $this->load->view('c_level/header');
// //        $this->load->view('c_level/sidebar');
//         $this->load->view('d_level/customer_form', $data);
//        $this->load->view('c_level/footer');
    }

//    ========= its for d_customer_iframe_form ============ 
    public function d_customer_iframe_form() {

//        $user_id = $this->session->userdata('user_id');
//          $data['d_level_form'] = $this->db->select('*')->from('iframe_code_tbl')->where('created_by', $user_id)->get()->result();

        $this->load->view('d_level/d_customer_iframe_form');
    }

    public function d_customer_iframe_form_custom($user_id, $user_type) {

        $this->load->view('d_level/customer_form');
    }

//================ its for d_customer_info_save =============
    public function d_customer_info_save() {
//        $action_page = $this->uri->segment(1);
//        $action_done = "insert";
//        $remarks = "Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');
        $reference = $this->input->post('reference');
        $user_id = $this->input->post('user_id');
        $level_type = $this->input->post('level_type');
        $level_id = $user_id;
//        if ($this->session->userdata('isAdmin') == 1) {
//            $level_id = $this->session->userdata('user_id');
//        } else {
//            $level_id = $this->session->userdata('admin_created_by');
//        }
//        dd($level_id);
        $email_check = $this->db->select('email')->from('customer_info')->where('email', $email)->where('level_id', $level_id)->get()->result();
        if ($email_check) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This email already exists!</div>");
            redirect($_SERVER['HTTP_REFERER']);
        } else {

//        ============ its for accounts coa table ===============
            $coa = $this->Customer_model->headcode($level_id);
                 // echo '<pre>';                print_r($coa); 
                    if ($coa->HeadCode != NULL) {
                        $hc= explode("-", $coa->HeadCode);
                        $nxt = $hc[1]+1;
                        $headcode = $hc[0]."-".  $nxt;                      
                    } else {
                        $headcode = "1020301-1";
                    } 
//                    dd($headcode);

            $lastid = $this->db->select("*")->from('customer_info')
                    ->order_by('customer_id', 'desc')
                    ->get()
                    ->row();
            //dd($lastid);
            $sl = $lastid->customer_no;
            if (empty($sl)) {
                $sl = "CUS-0001";
            } else {
                $sl = $sl;
            }
            $supno = explode('-', $sl);
            $nextno = $supno[1] + 1;
            $si_length = strlen((int) $nextno);

            $str = '0000';
            $cutstr = substr($str, $si_length);
            $sino = "CUS" . "-" . $cutstr . $nextno;

//        $customer_name = $this->input->post('customer_name');
            $customer_no = $sino . '-' . $first_name . " " . $last_name;
//        ================= close =======================
            //        =============== its for company name with customer id start =============
            $last_c_id = $lastid->customer_id;
//            $cn = strtoupper(substr($company, 0, 3)) . "-";
            $cn = $first_name . "-";
            if (empty($last_c_id)) {
                $last_c_id = $cn . "1";
            } else {
                $last_c_id = $last_c_id;
            }
            $cust_nextid = $last_c_id + 1;
            $company_custid = $cn . $cust_nextid;
//            dd($company_custid);
//        =============== its for company name with customer id close=============
//        ======== its for customer COA data array ============
            $customer_coa = array(
                'HeadCode' => $headcode,
                'HeadName' => $customer_no,
                'PHeadName' => 'Customer Receivable',
                'HeadLevel' => '4',
                'IsActive' => '1',
                'IsTransaction' => '1',
                'IsGL' => '0',
                'HeadType' => 'A',
                'IsBudget' => '0',
                'IsDepreciation' => '0',
                'DepreciationRate' => '0',
                'CreateBy' => $user_id,
                'CreateDate' => $created_date,
            );
//        dd($customer_coa);
            $this->db->insert('acc_coa', $customer_coa);
//        ======= close ==============

            $customer_data = array(
                'company_customer_id' => $company_custid,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'phone' => $phone,
                'company' => $company,
                'customer_no' => $customer_no,
                'customer_type' => 'personal',
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'street_no' => $street_no,
                'side_mark' => $side_mark,
                'reference' => $reference,
                'level_id' => $level_id,
                'created_by' => $user_id,
                'create_date' => $created_date,
            );
            $this->db->insert('customer_info', $customer_data);
            $customer_id = $this->db->insert_id();

            $remarks = $this->input->post('comment');
            $appointmen_status = 'enquiry';
            $appointment_data = array(
                'customer_id' => $customer_id,
                'c_level_id' => $level_id,
                'appointment_date' => $created_date, //$appointment_date,
                'appointment_time' => '', //$appointment_time,
                'c_level_staff_id' => '', //$c_level_staff_id,
                'remarks' => $remarks,
                'create_by' => $customer_id, //$user_id,
                'level_from' => 'd',
                'create_date' => $created_date,
            );
//        echo '<pre>';        print_r($appointment_data);die();
            $this->db->insert('appointment_calendar', $appointment_data);
            $this->db->set('now_status', $appointmen_status)->where('customer_id', $customer_id)->update('customer_info');

            $phone_types_number = array(
                'phone' => $phone,
                'phone_type' => 'Phone',
                'customer_id' => $customer_id,
                'customer_user_id' => $user_id,
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
//        ============ its for access log info collection ===============
//        $accesslog_info = array(
//            'action_page' => $action_page,
//            'action_done' => $action_done,
//            'remarks' => $remarks,
//            'user_name' => $this->user_id,
//            'entry_date' => date("Y-m-d H:i:s"),
//        );
//        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info save successfully!</div>");
        redirect($_SERVER['HTTP_REFERER']);
    }

//============= its for faq =============
    public function faq() {

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/faq');
        $this->load->view('c_level/footer');
    }

//============= its for profile_setting =============
//    public function profile_setting(){
//        
//         $this->load->view('c_level/header');
//        $this->load->view('c_level/sidebar');
//        $this->load->view('c_level/settings/profile_setting');
//        $this->load->view('c_level/footer');
//    }
    public function own_user_usstate_count($level_id, $user_id) {
        if ($user_id == $level_id) {
            $filter = "level_id=" . $level_id;
        } else {
            $filter = "created_by=" . $user_id;
        }
        $this->db->where($filter);
        $num_rows = $this->db->count_all_results('c_us_state_tbl');
        return $num_rows;
    }

//============= its for us_state =============
    public function us_state() {
        $this->permission_c->check_label('us_state')->create()->redirect();
        $data['get_city_state'] = $this->db->select('*')->from('city_state_tbl')->group_by('state_name')->get()->result();
        $config["base_url"] = base_url('c_level/Setting_controller/us_state');
        $config["total_rows"] = $this->own_user_usstate_count($this->level_id, $this->user_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["us_state_list"] = $this->Setting_model->us_state_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/us_state');
        $this->load->view('c_level/footer');
    }

//    ============= its for us_state_save ==========
    public function c_us_state_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "us state information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $short_code = $this->input->post('short_code');
        $state_name = $this->input->post('state_name');
        $tax_rate = $this->input->post('tax_rate');
        $us_state_data = array(
            'shortcode' => $short_code,
            'state_name' => $state_name,
            'tax_rate' => $tax_rate,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
        );
        $this->db->insert('c_us_state_tbl', $us_state_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State save successfully!</div>");
        redirect('c-us-state');
    }

//    ============= its for us_state_edit ============
    public function c_us_state_edit($id) {
        $this->permission_c->check_label('c_us_state_edit')->read()->redirect();
        $data['get_city_state'] = $this->db->select('*')->from('city_state_tbl')->group_by('state_id')->get()->result();
        $data['us_state_edit'] = $this->Setting_model->us_state_edit($id);
        $shortcode = $data['us_state_edit'][0]['shortcode'];
        $data['get_statename'] = $this->db->where('state_id', $shortcode)->group_by('state_name')->get('city_state_tbl')->result();

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/us_state_edit');
        $this->load->view('c_level/footer');
    }

//    =========== its for c_us_state_update ===========
    public function c_us_state_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "us state information update";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $short_code = $this->input->post('short_code');
        $state_name = $this->input->post('state_name');
        $tax_rate = $this->input->post('tax_rate');

        $us_state_data = array(
            'shortcode' => $short_code,
            'state_name' => $state_name,
            'tax_rate' => $tax_rate,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
        );
        $this->db->where('state_id', $id);
        $this->db->update('c_us_state_tbl', $us_state_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State updated successfully!</div>");
        redirect('c-us-state');
    }

    //    ============ its for us_state_delete =============
    public function us_state_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "us state information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('state_id', $id);
        $this->db->delete('c_us_state_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State deleted successfully!</div>");
        redirect('c-us-state');
    }

//    =========== its for import_c_us_state_save =========
    public function import_c_us_state_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "us state information import done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['shortcode'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['state_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['tax_rate'] = (!empty($csv_line[2]) ? $csv_line[2] : 0);
//                    echo $insert_csv['tax_rate'];
                }
                $data = array(
                    'shortcode' => $insert_csv['shortcode'],
                    'state_name' => $insert_csv['state_name'],
                    'tax_rate' => $insert_csv['tax_rate'],
                    'level_id' => $this->level_id,
//                    'created_by' => $this->user_id,
//                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
//                echo $data['email'];
                    $result = $this->db->select('*')
                            ->from('c_us_state_tbl')
                            ->where('shortcode', $data['shortcode'])
                            ->or_where('state_name', $data['state_name'])
                            ->get()
                            ->num_rows();
//                echo '<pre>';                print_r($result);die();
                    if ($result == 0 && !empty($data['shortcode'])) {
                        $this->db->insert('c_us_state_tbl', $data);
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State imported successfully!</div>");
                    } else {
                        $data = array(
//                            'customer_id' => $insert_csv['customer_id'],
                            'shortcode' => $insert_csv['shortcode'],
                            'state_name' => $insert_csv['state_name'],
                            'tax_rate' => $insert_csv['tax_rate'],
                            'level_id' => $this->level_id,
//                            'updated_by' => $this->user_id,
//                            'updated_date' => date('Y-m-d'),
                        ); 
//                echo '<pre>';                print_r($data);die();
                        $this->db->where('shortcode', $data['shortcode']);
                        $this->db->update('c_us_state_tbl', $data);
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State import updated successfully!</div>");
//                        redirect('customer-import');
                    }
                }
                $count++;
            }
        }
//        echo "DUke Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
//        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State imported successfully!</div>");
        redirect('c-us-state');
    }

//    ============== its for city_state_wise_statename ===========
    public function city_statename_wise_stateid() {
        $state_name = $this->input->post('state_name');
        $get_stateid = $this->db->where('state_name', $state_name)->group_by('state_name')->get('city_state_tbl')->result();
        echo json_encode($get_stateid);
    }

//    ==========its for own user customer count =========
    public function own_access_log_count($level_id) {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "level_id=" . $level_id;
        } else {
            $filter = "user_name=" . $user_id;
        }
        $this->db->where($filter);
        $num_rows = $this->db->count_all_results('accesslog');
        return $num_rows;
    }

//============= its for logs =============
    public function logs() {
        $this->permission_c->check_label('logs')->create()->redirect();
        $config["base_url"] = base_url('c_level/Setting_controller/logs');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_access_log_count($this->level_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["access_logs"] = $this->Setting_model->access_logs($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;


        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/logs');
        $this->load->view('c_level/footer');
    }

}
