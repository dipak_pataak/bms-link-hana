<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
//        echo $_SERVER['REMOTE_ADDR'];die();
        parent::__construct();
       $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
            if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        
      if($session_id == '' || $user_type != 'c'){
            redirect('c-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/User_model');
        $this->load->model('b_level/settings');
    }

    public function index() {

        $data['get_users'] = $this->User_model->get_users();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/users/add_user', $data);
        $this->load->view('c_level/footer');
    }

//    =========== its for user_email_check ================
    public function user_email_check() {
        $email = $this->input->post('email');
        $query = $this->db->select('*')->from('user_info')->where('email', $email)->get()->row();
//        echo '<pre>';        print_r($query);die();
        if (!empty($query)) {
            echo $query->email;
        } else {
            echo 0;
        }
    }

//    =============== its for user-save ===============
    public function user_save() {
//        $last_record = $this->db->select('*')->from('c_level_user')->order_by('id', "DESC")->limit(1)->get()->row();
////        echo '<pre>';        print_r($last_record);        die();
//        if (!empty($last_record->c_level_id)) {
//            $substr = substr($last_record->c_level_id, 2);
////            echo $substr;
//            $c_level_id = "c-" . ($substr + 1);
//        } else {
//            $c_level_id = "c-1001";
//        }
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $add_user = $this->input->post('add_user');
        $add_another_user = $this->input->post('add_another_user');
        $fixed_commission = $this->input->post('fixed_commission');
        $percentage_commission = $this->input->post('percentage_commission');

//        =========== its for save user info ============
        $user_data = array(
//            'c_level_id' => $c_level_id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'language' => 'English',
            'created_by' => $this->user_id,
            'user_type' => 'c',
            'create_date' => $created_date,
            'fixed_commission' => $fixed_commission,
            'percentage_commission' => $percentage_commission,
        );
        $this->db->insert('user_info', $user_data);
        $user_id = $this->db->insert_id();
//        =========== its for save log info ============
        $loginfo = array(
            'user_id' => $user_id,
            'email' => $email,
            'password' => md5($password),
            'user_type' => 'c',
            'is_admin' => 2,
        );
        $this->db->insert('log_info', $loginfo);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Color information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        //        =========== its for customer user info send by email ============     
        if ($email) {
            $data['get_mail_config'] = $this->settings->get_mail_config();
            $this->User_model->sendUserInfo($user_id, $data, $email, $password);
        }
        if ($add_user) {
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User save successfully!</div>");
            redirect('users');
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User save successfully!</div>");
            redirect('add-user');
        }
    }

//    ============= its for company-profile =============== 
    public function users() {
        $data['get_users'] = $this->User_model->get_users();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/users/users', $data);
        $this->load->view('c_level/footer');
    }

    public function delete_user($id){

        $this->db->where('id',$id)->delete('user_info');
        $this->db->where('user_id',$id)->delete('log_info');
        $this->db->where('user_id',$id)->delete('user_access_tbl');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User delete successfully!</div>");
            redirect('users');

    }

//    =========== its for user_edit ===============
    public function user_edit($id) {
        $data['user_edit'] = $this->User_model->user_edit($id);


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/users/user_edit', $data);
        $this->load->view('c_level/footer');
    }

//    ================= its for user_update ===============
    public function user_update($id) {
        $updated_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $fixed_commission = $this->input->post('fixed_commission');
        $percentage_commission = $this->input->post('percentage_commission');
//        echo $password; die();
        if (!empty($password)) {
            $user_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'updated_by' => $this->user_id,
                'user_type' => 'c',
                'fixed_commission' => $fixed_commission,
                'percentage_commission' => $percentage_commission,
                'update_date' => $updated_date,
            );
//        echo '<pre>';        print_r($user_data);die();
            $this->db->where('id', $id);
            $this->db->update('user_info', $user_data);
            //        =========== its for update log info ============
            $loginfo = array(
                'email' => $email,
                'password' => md5($password),
                'user_type' => 'c',
                'is_admin' => 2,
            );
            $this->db->where('user_id', $id);
            $this->db->update('log_info', $loginfo);
        } else {
            $user_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'updated_by' => $this->user_id,
                'user_type' => 'c',
                'fixed_commission' => $fixed_commission,
                'percentage_commission' => $percentage_commission,
                'update_date' => $updated_date,
            );
//        echo '<pre>';        print_r($user_data);die();
            $this->db->where('id', $id);
            $this->db->update('user_info', $user_data);
            //        =========== its for update log info ============
            $loginfo = array(
                'email' => $email,
                'user_type' => 'c',
                'is_admin' => 2,
            );
            $this->db->where('user_id', $id);
            $this->db->update('log_info', $loginfo);
        }
       
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "user information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
         if ($email) {
            $data['get_mail_config'] = $this->settings->get_mail_config();
            $this->User_model->sendUserInfo($id, $data, $email, $password);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User updated successfully!</div>");
        redirect('users');
    }

}
