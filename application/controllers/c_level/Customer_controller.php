<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($session_id == '' || $user_type != 'c') {
            redirect('c-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('b_level/settings');
    }

    public function index() {
//        $this->load->view('welcome_message');
    }

    //    =========== its for add_customer ===============
    public function add_customer() {
        $this->permission_c->check_label('add_customer')->create()->redirect();
        $data['get_states'] = $this->Customer_model->get_states();
        //      dd($data['get_states']);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/add_customer', $data);
        $this->load->view('c_level/footer');
    }

//======== its for exists get_check_unique_email  ===========
    public function get_check_unique_email() {
        $email = $this->input->post('email');
//        echo $email;die();
        $query = $this->db->select('*')->from('customer_info')->where('email', $email)
                        ->get()->row();
//        echo '<pre>';        print_r($query);die();
        if (!empty($query)) {
            echo $query->email;
        } else {
            echo 0;
        }
    }

//    ============= its for state_wise_city =============
    public function state_wise_city($state_id = null) {
        $state_wise_city = $this->db->select('*')->from('city_state_tbl')->where('state_id', $state_id)->get()->result();
        echo json_encode($state_wise_city);
    }

//================ its for customer save method ===================
    public function customer_save() {

        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $phone_type = $this->input->post('phone_type');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');
        $reference = $this->input->post('reference');

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        dd($phone);
//        ============ its for accounts coa table ===============
        $coa = $this->Customer_model->headcode($this->level_id);
                 // echo '<pre>';                print_r($coa); 
                    if ($coa->HeadCode != NULL) {
                        $hc= explode("-", $coa->HeadCode);
                        $nxt = $hc[1]+1;
                        $headcode = $hc[0]."-".  $nxt;                      
                    } else {
                        $headcode = "1020301-1";
                    } 
//                    dd($headcode);

        $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                ->order_by('customer_id', 'desc')
                ->get()
                ->row();
//        echo $headcode;
        $sl = $lastid->customer_no;
//        echo $sl;
        if (empty($sl)) {
            $sl = "CUS-0001";
        } else {
            $sl = $sl;
        }
        $supno = explode('-', $sl);
        $nextno = $supno[1] + 1;
        $si_length = strlen((int) $nextno);
        $str = '0000';
        $cutstr = substr($str, $si_length);
        $sino = "CUS" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;
//        dd($sino);
//        $customer_name = $this->input->post('customer_name');
        $customer_no = $sino . '-' . $first_name . " " . $last_name;
//        ================= close =======================
        //        =============== its for company name with customer id start =============
        $last_c_id = $lastid->customer_id;
        $cn = strtoupper(substr($company, 0, 3)) . "-";
        if (empty($last_c_id)) {
            $last_c_id = $cn . "1";
        } else {
            $last_c_id = $last_c_id;
        }
        $cust_nextid = $last_c_id + 1;
        $company_custid = $cn . $cust_nextid;
//        =============== its for company name with customer id close=============
//        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadCode' => $headcode,
            'HeadName' => $customer_no,
            'PHeadName' => 'Customer Receivable',
            'HeadLevel' => '4',
            'IsActive' => '1',
            'IsTransaction' => '1',
            'IsGL' => '0',
            'HeadType' => 'A',
            'IsBudget' => '0',
            'IsDepreciation' => '0',
            'DepreciationRate' => '0',
            'level_id' => $level_id,
            'CreateBy' => $this->user_id,
            'CreateDate' => $created_date
        );
//        dd($customer_coa);
        $this->db->insert('acc_coa', $customer_coa);
//        ======= close ==============

        $customer_data = array(
            'company_customer_id' => $company_custid,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone[0],
//            'phone_2' => $phone_2,
//            'phone_3' => $phone_3,
            'company' => $company,
            'customer_no' => $customer_no,
            'customer_type' => 'personal',
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip_code,
            'country_code' => $country_code,
            'street_no' => $street_no,
            'side_mark' => $side_mark,
            'reference' => $reference,
            'level_id' => $level_id,
            'created_by' => $this->user_id,
            'create_date' => $created_date,
        );
        $this->db->insert('customer_info', $customer_data);
        $customer_inserted_id = $this->db->insert_id();


//            ================ its for multiple certificate info save ===========
        if ($_FILES["file_upload"]['name'][0] != '') {
            $file_uploadCount = count($_FILES['file_upload']['name']);
            for ($i = 0; $i < $file_uploadCount; $i++) {
                $_FILES['file_uploader']['name'] = $_FILES['file_upload']['name'][$i];
                $_FILES['file_uploader']['type'] = $_FILES['file_upload']['type'][$i];
                $_FILES['file_uploader']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
                $_FILES['file_uploader']['error'] = $_FILES['file_upload']['error'][$i];
                $_FILES['file_uploader']['size'] = $_FILES['file_upload']['size'][$i];

// configure for upload 
                $config = array(
                    'upload_path' => "./assets/c_level/uploads/customers/",
                    'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
                    'overwrite' => TRUE,
//            'file_name' => "BMSLINK" . time(),
                    'file_size' => '2048',
                );
                $image_data = array();

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file_uploader')) {
                    $image_data = $this->upload->data();
//                print_r($image_data); die();
                    $customer_file_name = $image_data['file_name'];

                    $customerFileinfo[$i]['customer_id'] = $customer_inserted_id;
                    $customerFileinfo[$i]['customer_user_id'] = '';
                    $customerFileinfo[$i]['file_upload'] = $customer_file_name;
//                    $customerFileinfo[$i]['degree_name'] = $degrees_name[$i];
                }
            }
            $this->Customer_model->save_customer_file($customerFileinfo);
        }
//        =============== its for customer phone type info ==============
        for ($i = 0; $i < count($phone); $i++) {
            $phone_types_number = array(
                'phone' => $phone[$i],
                'phone_type' => $phone_type[$i],
                'customer_id' => $customer_inserted_id,
                'customer_user_id' => '',
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
        }

//        ============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info save successfully!</div>");
        redirect('add-customer');
    }

//    ==========its for own user customer count =========
    public function own_user_customer_count() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('customer_info');
        return $num_rows;
    }

    //    =========== its for customer_list ===============
    public function customer_list() {
        $this->permission_c->check_label('customer_list')->create()->redirect();
//        $data['get_customer'] = $this->Customer_model->get_customer();
//        dd($data['get_customer']);
        #pagination starts
        #
        $config["base_url"] = base_url('customer-list');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_user_customer_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["get_customer"] = $this->Customer_model->get_customer($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
//        ========= its for pagination close===========

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/customer_list', $data);
        $this->load->view('c_level/footer');
    }

    //    =========== its for customer_view ===============
    public function customer_view($id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['customer_view'] = $this->Customer_model->customer_view($id);
        $data['get_customer_phones'] = $this->Customer_model->get_customer_phones($id);
        $data['orderd'] = $this->Customer_model->get_customer_order($id);
        $data['company_profile'] = $this->settings->company_profile();

//        ========= its for chart results strat ===========
        $months = '';
        $salesamount = '';
        $paid_amount = '';
        $manufacturing_amount = '';
        $year = date('Y');
        $numbery = date('y');
        $prevyear = $numbery - 1;
        $prevyearformat = $year - 1;
        $syear = '';
        $syearformat = '';
        for ($k = 1; $k < 13; $k++) {
            $month = date('m', strtotime("+$k month"));
            $gety = date('y', strtotime("+$k month"));
            if ($gety == $numbery) {
                $syear = $prevyear;
                $syearformat = $prevyearformat;
            } else {
                $syear = $numbery;
                $syearformat = $year;
            }
            $monthly_amount = $this->monthlysaleamount($syearformat, $month, $id);
            $paid_amounts = $this->monthlySalePaidAmount($syearformat, $month, $id);
            $manufacturing_amounts = $this->monthlySaleManufacturingAmount($syearformat, $month, $id);
            $salesamount .= $monthly_amount . ', ';
            $paid_amount .= $paid_amounts . ', ';
            $manufacturing_amount .= $manufacturing_amounts . ', ';
            $months .= "'" . date('M-' . $syear, strtotime("+$k month")) . "',";
        }
        $data['monthly_sales_amount'] = trim($salesamount, ',');
        $data['paid_amount'] = trim($paid_amount, ',');
        $data['manufacturing_amount'] = trim($manufacturing_amount, ',');
        $data['monthly_sales_month'] = trim($months, ',');
//        dd($data['paid_amount']);
//        ========= its for chart results close ===========

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/customer_view', $data);
        $this->load->view('c_level/footer');
    }

    public function monthlysaleamount($year, $month, $id) {
        $groupby = "GROUP BY YEAR(order_date), MONTH(order_date)";
        $amount = '';
        $wherequery = "YEAR(order_date)='$year' AND month(order_date)='$month' AND customer_id = '$id' $groupby";
        $this->db->select('round(SUM(grand_total),2) as amount');
        $this->db->from('quatation_tbl');
        $this->db->where($wherequery, NULL, FALSE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $row) {
                $amount .= $row->amount . ", ";
            }
            return trim($amount, ', ');
        }
        return 0;
    }

    public function monthlySalePaidAmount($year, $month, $id) {
        $groupby = "GROUP BY YEAR(order_date), MONTH(order_date)";
        $amount = '';
        $wherequery = "YEAR(order_date)='$year' AND month(order_date)='$month' AND customer_id = '$id' AND order_stage = '2' $groupby";
        $this->db->select('round(SUM(grand_total),2) as amount');
        $this->db->from('quatation_tbl');
        $this->db->where($wherequery, NULL, FALSE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $row) {
                $amount .= $row->amount . ", ";
            }
            return trim($amount, ', ');
        }
        return 0;
    }

    public function monthlySaleManufacturingAmount($year, $month, $id) {
        $groupby = "GROUP BY YEAR(order_date), MONTH(order_date)";
        $amount = '';
        $wherequery = "YEAR(order_date)='$year' AND month(order_date)='$month' AND customer_id = '$id' AND order_stage = '4' $groupby";
        $this->db->select('round(SUM(grand_total),2) as amount');
        $this->db->from('quatation_tbl');
        $this->db->where($wherequery, NULL, FALSE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $row) {
                $amount .= $row->amount . ", ";
            }
            return trim($amount, ', ');
        }
        return 0;
    }

//    ============== its for show_customer_record =============
    public function show_customer_record($id) {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['show_customer_record'] = $this->Customer_model->show_customer_record($id);
        $data['get_customer_phones'] = $this->Customer_model->get_customer_phones($id);
        $data['show_customer_appointment_record'] = $this->Customer_model->show_customer_appointment_record($id);
        $data['scheduled'] = $this->input->post('status');

//        dd($data['show_customer_record']);
        $data['get_users'] = $this->db->select("*, CONCAT_WS( first_name,' ', last_name) as full_name")->where('user_type', 'c')
                        ->where('created_by', $level_id)
                        ->where('created_by!=', 0)->get('user_info')->result();
        $data['remarks'] = $this->db->select("appointment_calendar.*,CONCAT_WS(' ', user_info.first_name, user_info.last_name) as creater")
                        ->join('user_info', 'user_info.id=appointment_calendar.create_by', 'left')
                        ->where('appointment_calendar.customer_id', $id)
                        ->order_by('appointment_calendar.appointment_id', 'desc')->limit(2)
                        ->get('appointment_calendar')->result();

//        $data['customer_remarks'] = '';
//        if ($data['show_customer_record'][0]['level_from'] == 'd') {
//            $data['customer_remarks'] = $this->db->select("a.*,CONCAT_WS(' ', b.first_name, b.last_name) as initial_creator")
//                            ->join('customer_info b', 'b.customer_id = a.customer_id', 'left')
//                            ->where('a.customer_id', $id)
//                            ->order_by('a.appointment_id', 'desc')->limit(2)
//                            ->get('appointment_calendar a')->result();
//        }


        $this->load->view('c_level/customers/show_customer_record', $data);
    }

    //    =========== its for customer_edit ===============
    public function customer_edit($id) {
        $data['customer_edit'] = $this->Customer_model->customer_edit($id);
        $state_id = $data['customer_edit'][0]['state'];
//        echo $state_id;die();
        $data['get_states'] = $this->Customer_model->get_states();
        $data['get_states_wise_city'] = $this->Customer_model->get_states_wise_city($state_id);
//        dd($data['get_states_wise_city']);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/customer_edit', $data);
        $this->load->view('c_level/footer');
    }

//================ its for  customer_update method ===================
    public function customer_update($customer_id) {
        $get_customer_info = $this->db->select('customer_no')->where('customer_id', $customer_id)->get('customer_info')->result();
        $get_customer_no = $get_customer_info[0]->customer_no;
        $get_customer_coa_info = $this->db->select('HeadCode')->where('HeadName', $get_customer_no)->get('acc_coa')->result();
        $get_customer_headCode = $get_customer_coa_info[0]->HeadCode;
        $action_page = $this->uri->segment(1);
        $action_done = "update";
        $remarks = "Customer information update";
        $update_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $cust_no = $this->input->post('customer_no');
        $customer_no = $cust_no . "-" . $first_name . " " . $last_name;
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $phone_type = $this->input->post('phone_type');
//         echo "1st ".$phone." 2nd ".$phone_2. " 3rd ". $phone_3. "1st ".$phone_type_1." 2nd ".$phone_type_2. " 3rd ". $phone_type_3; die();
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');
        $reference = $this->input->post('reference');
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        // configure for upload 
        $config = array(
            'upload_path' => "./assets/c_level/uploads/customers/",
            'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
            'overwrite' => TRUE,
//            'file_name' => "BMSLINK" . time(),
            'file_size' => '2048',
        );
        $cn = strtoupper(substr($company, 0, 3)) . "-";
        $company_custid = $cn . $customer_id;
        $customer_data = array(
            'company_customer_id' => $company_custid,
            'customer_no' => $customer_no,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone[0],
            'company' => $company,
            'customer_type' => 'personal',
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip_code,
            'country_code' => $country_code,
            'street_no' => $street_no,
            'side_mark' => $side_mark,
            'reference' => $reference,
            'level_id' => $level_id,
            'update_by' => $this->user_id,
            'update_date' => $update_date,
        );
//        echo '<pre>';        print_r($customer_data);die();
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customer_info', $customer_data);
        //        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadName' => $customer_no,
            'UpdateBy' => $this->user_id,
            'UpdateDate' => $update_date,
        );
//        dd($customer_coa);
        $this->db->where('HeadCode', $get_customer_headCode);
        $this->db->update('acc_coa', $customer_coa);
//        ======= close ==============
        //            ================ its for multiple certificate info save ===========
        if ($_FILES["file_upload"]['name'][0] != '') {
            $file_uploadCount = count($_FILES['file_upload']['name']);
            for ($i = 0; $i < $file_uploadCount; $i++) {
                $_FILES['file_uploader']['name'] = $_FILES['file_upload']['name'][$i];
                $_FILES['file_uploader']['type'] = $_FILES['file_upload']['type'][$i];
                $_FILES['file_uploader']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
                $_FILES['file_uploader']['error'] = $_FILES['file_upload']['error'][$i];
                $_FILES['file_uploader']['size'] = $_FILES['file_upload']['size'][$i];

                // configure for upload 
//                $config = array(
//                    'upload_path' => "./assets/uploads/doctor/",
//                    'allowed_types' => "gif|jpg|png|jpeg|pdf",
//                    'overwrite' => TRUE,
////                    'file_name' => "BDTASK" . time(),
//                    'max_size' => '0',
//                );
                $image_data = array();

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file_uploader')) {
                    $image_data = $this->upload->data();
//                print_r($image_data); die();
                    $customer_file_name = $image_data['file_name'];

                    $customerFileinfo[$i]['customer_id'] = $customer_id;
                    $customerFileinfo[$i]['customer_user_id'] = '';
                    $customerFileinfo[$i]['file_upload'] = $customer_file_name;
//                    $customerFileinfo[$i]['degree_name'] = $degrees_name[$i];
                }
            }
            $this->Customer_model->save_customer_file($customerFileinfo);
        }

//        =============== its for customer phone type info ==============
        $customer_id_check_phone_types = $this->db->where('customer_id', $customer_id)->from('customer_phone_type_tbl')->get()->result();
        if ($customer_id_check_phone_types) {
            $customer_phone_types_deleted_sql = $this->db->where('customer_id', $customer_id)->delete('customer_phone_type_tbl');
//        =============== its for customer phone type info ==============
            for ($i = 0; $i < count($phone); $i++) {
                $phone_types_number = array(
                    'phone' => $phone[$i],
                    'phone_type' => $phone_type[$i],
                    'customer_id' => $customer_id,
                    'customer_user_id' => '',
                );
                $this->db->insert('customer_phone_type_tbl', $phone_types_number);
            }
        }
//        ============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info updated successfully!</div>");
        redirect('customer-list');
    }

    //    ============== its for b_customer_file_delete ============
    public function c_customer_file_delete($file_upload_id, $customer_id) {
        $customer_upload_unlink = $this->db->select('*')->from('customer_file_tbl')->where('id', $file_upload_id)->get()->row();
        if ($customer_upload_unlink->id == $file_upload_id) {
            if ($customer_upload_unlink->file_upload) {
                $img_path = FCPATH . 'assets/c_level/uploads/customers/' . $customer_upload_unlink->file_upload;
                unlink($img_path);
            }
            $this->db->where('id', $file_upload_id)->delete('customer_file_tbl');
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>File deleted successfully!</div>");
        redirect('customer-edit/' . $customer_id);
    }

//============ its for customer_delete ============
    public function customer_delete($customer_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "customer deleted information";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
//        $customer_details = $this->db->where('customer_id', $customer_id)->get('customer_info')->row();
////        echo $customer_details->customer_no;
////        dd($customer_details);
        $this->db->where('customer_id', $customer_id)->delete('customer_file_tbl');
        $this->db->where('customer_id', $customer_id)->delete('customer_phone_type_tbl');
        $this->db->where('customer_id', $customer_id)->delete('customer_info');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info deleted successfully!</div>");
        redirect('customer-list');
    }

//    ========== its for customer bulk upload =============
    public function customer_bulk_upload() {
        $this->permission_c->check_label('bulk_upload')->create()->redirect();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/customer_bulk_upload_form');
        $this->load->view('c_level/footer');
    }

//    ========== its for c level customer bulk upload save =============
    public function customer_csv_upload_save() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {

                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();

//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['first_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['last_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['email'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                    $insert_csv['phone'] = (!empty($csv_line[3]) ? $csv_line[3] : null);
                    $insert_csv['fax'] = (!empty($csv_line[4]) ? $csv_line[4] : null);
                    $insert_csv['mobile'] = (!empty($csv_line[5]) ? $csv_line[5] : null);
                    $insert_csv['company'] = (!empty($csv_line[6]) ? $csv_line[6] : null);
                    $insert_csv['customer_type'] = 'personal'; //(!empty($csv_line[5]) ? $csv_line[5] : null);
                    $insert_csv['address'] = (!empty($csv_line[7]) ? $csv_line[7] : null);
                    $insert_csv['city'] = (!empty($csv_line[8]) ? $csv_line[8] : null);
                    $insert_csv['state'] = (!empty($csv_line[9]) ? $csv_line[9] : null);
                    $insert_csv['zip_code'] = (!empty($csv_line[10]) ? $csv_line[10] : null);
                    $insert_csv['country_code'] = (!empty($csv_line[11]) ? $csv_line[11] : null);
                    $insert_csv['street_no'] = (!empty($csv_line[12]) ? $csv_line[12] : null);
                }

                if ($count > 0) {
//                    echo $insert_csv['address'];

                    $address_explode = explode(",", $insert_csv['address']);
                    $address = $address_explode[0];
                    $street_no = explode(' ', $insert_csv['address']);
                    $street_no = $street_no[0];
                    $side_mark = $insert_csv['first_name'] . "-" . $street_no;
//                    echo '<pre>';   print_r($address_explode);   echo $address; echo "<br>";   echo $street_no;    die();

                    $phone = $this->phone_fax_mobile_no_generate($insert_csv['phone']);
                    $phone_2 = $this->phone_fax_mobile_no_generate($insert_csv['fax']);
                    $phone_3 = $this->phone_fax_mobile_no_generate($insert_csv['mobile']);
//                    echo $phone . " 1st " . $phone_2 . " 2st " . $phone_3;                    die();
                    $data = array(
                        'email' => $insert_csv['email'],
                    );
//                echo $data['email'];
                    $result = $this->db->select('*')
                            ->from('customer_info')
                            ->where('email', $data['email'])
                            ->get()
                            ->num_rows();
//                
//        ============ its for accounts coa table ===============
                    $coa = $this->Customer_model->headcode($this->level_id);
                 // echo '<pre>';                print_r($coa); 
                    if ($coa->HeadCode != NULL) {
                        $hc= explode("-", $coa->HeadCode);
                        $nxt = $hc[1]+1;
                        $headcode = $hc[0]."-".  $nxt;                      
                    } else {
                        $headcode = "1020301-1";
                    } 
//                    dd($headcode);
                    $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                            ->order_by('customer_id', 'desc')
                            ->get()
                            ->row();
                    $sl = @$lastid->customer_no;
                    if (empty($sl)) {
                        $sl = "CUS-0001";
                    } else {
                        $sl = $sl;
                    }
//                        dd($sl);
                    $supno = explode('-', $sl);
                    $nextno = $supno[1] + 1;
                    $si_length = strlen((int) $nextno);

                    $str = '0000';
                    $cutstr = substr($str, $si_length);
                    $sino = "CUS" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;
//        $customer_name = $this->input->post('customer_name');
                    $customer_no = $sino . '-' . $insert_csv['first_name'] . " " . $insert_csv['last_name'];
//        ================= close =======================
//        =============== its for company name with customer id start =============
                    $last_c_id = $lastid->customer_id;
                    $cn = strtoupper(substr($insert_csv['company'], 0, 3)) . "-";
                    if (empty($last_c_id)) {
                        $last_c_id = $cn . "1";
                    } else {
                        $last_c_id = $last_c_id;
                    }
                    $cust_nextid = $last_c_id + 1;
                    $company_custid = $cn . $cust_nextid;

                    if ($result == 0 && !empty($data['email'])) {

//                        if ($insert_csv['customer_type'] == 'business') {
////            =========== its for save customer data in the users table ===============
//                            $customer_userinfo_data = array(
//                                'created_by' => $this->user_id,
//                                'first_name' => $insert_csv['first_name'],
//                                'last_name' => $insert_csv['last_name'],
//                                'company' => $insert_csv['company'],
//                                'address' => $address, //$insert_csv['address'],
//                                'city' => $insert_csv['city'],
//                                'state' => $insert_csv['state'],
//                                'zip_code' => $insert_csv['zip_code'],
//                                'country_code' => $insert_csv['country_code'],
//                                'phone' => $phone,
//                                'email' => $insert_csv['email'],
//                                'user_type' => 'c',
//                                'create_date' => date('Y-m-d'),
//                            );
//                            $this->db->insert('user_info', $customer_userinfo_data);
//                            $user_insert_id = $this->db->insert_id();
////            =========== its for save customer data in the users table ===============
//                            $customer_loginfo_data = array(
//                                'user_id' => $user_insert_id,
//                                'email' => $username,
//                                'password' => md5($insert_csv['password']),
//                                'user_type' => 'c',
//                                'is_admin' => '1',
//                            );
//                            $this->db->insert('log_info', $customer_loginfo_data);
//
////            ============== its for company profile ===========
//                            $company_profile = array(
//                                'user_id' => $user_insert_id,
//                                'company_name' => $insert_csv['company'],
//                                'email' => $insert_csv['email'],
//                                'phone' => $phone,
//                                'address' => $address,
//                                'city' => $insert_csv['city'],
//                                'state' => $insert_csv['state'],
//                                'zip_code' => $insert_csv['zip_code'],
//                                'country_code' => $insert_csv['country_code'],
//                                'created_by' => $this->user_id,
//                                'created_at' => date('Y-m-d'),
//                            );
//                            $this->db->insert('company_profile', $company_profile);
//                        } else {
//                            $user_insert_id = '';
//                        }
//        =============== its for company name with customer id close=============
                        $data = array(
                            'customer_user_id' => '', //$user_insert_id,
                            'customer_no' => $customer_no,
                            'company_customer_id' => $company_custid,
                            'first_name' => $insert_csv['first_name'],
                            'last_name' => $insert_csv['last_name'],
                            'email' => $insert_csv['email'],
                            'phone' => $phone,
//                            'phone_2' => $phone_2,
//                            'phone_3' => $phone_3,
                            'company' => $insert_csv['company'],
                            'customer_type' => $insert_csv['customer_type'],
                            'address' => $address, //$insert_csv['address'],
                            'city' => $insert_csv['city'],
                            'state' => $insert_csv['state'],
                            'zip_code' => $insert_csv['zip_code'],
                            'country_code' => $insert_csv['country_code'],
                            'street_no' => $insert_csv['street_no'],
                            'side_mark' => $side_mark,
//                            'file_upload' => '', //$insert_csv['file_upload'],
                            'level_id' => $level_id, //$insert_csv['level_id'],
                            'created_by' => $this->user_id,
                            'create_date' => date('Y-m-d')
                        );

                        //        ======== its for customer COA data array ============
                        $customer_coa = array(
                            'HeadCode' => $headcode,
                            'HeadName' => $customer_no,
                            'PHeadName' => 'Customer Receivable',
                            'HeadLevel' => '4',
                            'IsActive' => '1',
                            'IsTransaction' => '1',
                            'IsGL' => '0',
                            'HeadType' => 'A',
                            'IsBudget' => '0',
                            'IsDepreciation' => '0',
                            'DepreciationRate' => '0',
                            'level_id' => $level_id,
                            'CreateBy' => $this->user_id,
                            'CreateDate' => date('Y-m-d')
                        );
//        dd($customer_coa);
                        $this->db->insert('acc_coa', $customer_coa);
//        ======= close ==============                        
                        $this->db->insert('customer_info', $data);
                        $csv_customer_id = $this->db->insert_id();

//                        =========== its for multiple phone insert ==========
                        if ($insert_csv['phone'] || $insert_csv['fax'] || $insert_csv['mobile']) {
//                            $phone_type_1 = $phone_type_2 = $phone_type_3 = '';
//                            if ($insert_csv['phone']) {
//                                $phone_type_1 = 'Phone';
//                            }
//                            if ($insert_csv['fax']) {
//                                $phone_type_2 = 'Fax';
//                            }
//                            if ($insert_csv['mobile']) {
//                                $phone_type_3 = 'Mobile';
//                            }
                            //        =============== its for customer phone type info ==============
                            if (!empty($phone)) {
                                $customer_phone_info = array(
                                    'customer_id' => $csv_customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Phone',
                                    'phone' => $phone,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_phone_info);
                            }
                            if (!empty($phone_2)) {
                                $customer_fax_info = array(
                                    'customer_id' => $csv_customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Fax',
                                    'phone' => $phone_2,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_fax_info);
                            }
                            if (!empty($phone_3)) {
                                $customer_mobile_info = array(
                                    'customer_id' => $csv_customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Mobile',
                                    'phone' => $phone_3,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_mobile_info);
                            }
                        }
//                    ============= close multiple phone type =============
                    } else {
                        $get_cid_by_email = $this->db->from('customer_info')->where('email', $insert_csv['email'])->get()->result();
                        $check_exist_headcode = $this->db->from('acc_coa')->where('HeadName', $get_cid_by_email[0]->customer_no)->get()->row();
                        if (empty($check_exist_headcode)) {
                            //        ======== its for customer COA data array ============
                            $customer_coa = array(
                                'HeadCode' => $headcode,
                                'HeadName' => $customer_no,
                                'PHeadName' => 'Customer Receivable',
                                'HeadLevel' => '4',
                                'IsActive' => '1',
                                'IsTransaction' => '1',
                                'IsGL' => '0',
                                'HeadType' => 'A',
                                'IsBudget' => '0',
                                'IsDepreciation' => '0',
                                'DepreciationRate' => '0',
                                'level_id' => $level_id,
                                'CreateBy' => $this->user_id,
                                'CreateDate' => date('Y-m-d')
                            );
//                            dd($customer_coa);
                            $this->db->insert('acc_coa', $customer_coa);
                        }

                        $cn = strtoupper(substr($insert_csv['company'], 0, 3)) . "-";
                        $company_custid = $cn . @$get_cid_by_email[0]->customer_id;

//                      echo $get_cid_by_email[0]->customer_id;   echo '<pre>';                        print_r($company_custid);die();

                        $data = array(
                            'customer_user_id' => '', //$user_insert_id,
                            'customer_no' => $customer_no,
                            'company_customer_id' => $company_custid,
                            'first_name' => $insert_csv['first_name'],
                            'last_name' => $insert_csv['last_name'],
                            'email' => $insert_csv['email'],
                            'phone' => $phone,
                            'company' => $insert_csv['company'],
                            'customer_type' => $insert_csv['customer_type'],
                            'address' => $address, //$insert_csv['address'],
                            'city' => $insert_csv['city'],
                            'state' => $insert_csv['state'],
                            'zip_code' => $insert_csv['zip_code'],
                            'country_code' => $insert_csv['country_code'],
                            'street_no' => $street_no, //$insert_csv['street_no'],
                            'side_mark' => $side_mark,
//                            'file_upload' => '', //$insert_csv['file_upload'],
                            'level_id' => $level_id, //$insert_csv['level_id'],
                            'created_by' => $this->user_id,
                            'update_date' => date('Y-m-d')
                        );
                        $this->db->where('email', $data['email']);
                        $this->db->update('customer_info', $data);

                        $customer_id_check_phone_types = $this->db->where('customer_id', @$get_cid_by_email[0]->customer_id)->from('customer_phone_type_tbl')->get()->result();

                        if ($customer_id_check_phone_types) {
                            $customer_phone_types_deleted_sql = $this->db->where('customer_id', $get_cid_by_email[0]->customer_id)->delete('customer_phone_type_tbl');
                            if (!empty($phone)) {
                                $customer_phone_info = array(
                                    'customer_id' => $get_cid_by_email[0]->customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Phone',
                                    'phone' => $phone,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_phone_info);
                            }
                            if (!empty($phone_2)) {
                                $customer_fax_info = array(
                                    'customer_id' => $get_cid_by_email[0]->customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Fax',
                                    'phone' => $phone_2,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_fax_info);
                            }
                            if (!empty($phone_3)) {
                                $customer_mobile_info = array(
                                    'customer_id' => $get_cid_by_email[0]->customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Mobile',
                                    'phone' => $phone_3,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_mobile_info);
                            }
                        }

//                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Already exists!</div>");
//                        redirect('customer-import');
                    }
                }
                $count++;
            }
        }
//        echo "Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer imported successfully!</div>");
        redirect('c-customer-bulk-upload');
    }

    //    ======== its for phone, fax and mobile no us format generate =============
    public function phone_fax_mobile_no_generate($number) {
        $first = substr($number, 0, 3);
        $second = substr($number, 3, 3);
        $third = substr($number, 6, 10);
        if ($number) {
            return "+1 (" . $first . ") " . $second . "-" . $third;
        } else {
            return '';
        }
    }

//    =============== its for c_level_customer_filter ==============
    public function c_level_customer_filter() {
        $data['first_name'] = $this->input->post('first_name');
        $data['sidemark'] = $this->input->post('sidemark');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['get_customer_filter_info'] = $this->Customer_model->get_customer_filter_info($data['first_name'], $data['sidemark'], $data['phone'], $data['address']);

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/get_customer_filter_info');
        $this->load->view('c_level/footer');
    }

//    ================== its for customer comment notification count -===============
    public function customer_comment_notification_count() {

        $this->db->select('count(a.id) total_id');
        $this->db->from('customer_commet_tbl a');
        $this->db->where('a.is_visited', 0);
        $this->db->where('a.comment_to', $this->session->userdata('user_id'));
//                    $this->db->group_by('a.comment_to');
        $customer_commet_count = $this->db->get()->result();
        echo $customer_commet_count[0]->total_id;
    }

//    ============== its for customer_comment_notification_results ============
//    public function customer_comment_notification_results() {
//        $this->db->select('a.*, b.first_name, b.last_name');
//        $this->db->from('customer_commet_tbl a');
//        $this->db->join('customer_info b', 'b.customer_user_id = a.comment_to');
//        $this->db->where('a.is_visited', 0);
//        $this->db->where('a.comment_to', $this->session->userdata('user_id'));
////                    $this->db->group_by('a.comment_to');
//        $customer_commet_query = $this->db->get()->result();
//        return $customer_commet_query;
//    }
//    ============= its for customer_comment_view =============
    public function customer_comment_view($comment_to) {
        $from = $this->db->select('*')->from('customer_commet_tbl')->where('comment_to', $comment_to)->get()->result();
        $comment_from = $from[0]->comment_from;
        $comments_data = array(
            'is_visited' => 1,
        );
        $this->db->where('comment_to', $comment_to)->update('customer_commet_tbl', $comments_data);

        $sql = "SELECT comment_from, comment_to, comments, status, created_at FROM customer_commet_tbl 
	WHERE (comment_from = $comment_from AND comment_to = $comment_to) 
                    or (comment_from = $comment_to AND comment_to = $comment_from)  ORDER BY id DESC";
        $data['customer_commet_results'] = $this->db->query($sql)->result();

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/customer_comment_view');
        $this->load->view('c_level/footer');
    }

//    ============ its for out_customer_comment ==========
    public function out_customer_comment() {
//        $comment_from = $this->input->post('comment_from');
        $customer_to = $this->input->post('customer_to');
        $comment = $this->input->post('comment');
        $comments_data = array(
            'comment_from' => $this->user_id,
            'comment_to' => $customer_to,
            'comments' => $comment,
            'status' => 'OUT',
        );
        $this->db->insert('customer_commet_tbl', $comments_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Comment send successfully!</div>");
        redirect('customer-comment-view/' . $this->user_id);
    }

    //    ============ its for show_address_map ============ 
    public function show_address_map($id) {
        $customer_info = $this->db->select('*')->from('customer_info a')->where('customer_id', $id)->get()->result();
        $address = $customer_info[0]->address . " " . @$customer_info[0]->city . " " . @$customer_info[0]->state . " " . @$customer_info[0]->zip_code . " " . @$customer_info[0]->country_code;
        echo "<iframe width='600' height='450' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q=" . urlencode($address) . "&amp;output=embed'></iframe>";
    }

    //    ==============it for b_level_customer_search ==============
    public function c_level_customer_search() {
        $keyword = $this->input->post('keyword');
        $data['get_customer'] = $this->Customer_model->get_customer_search_result($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('c_level/customers/customer_search', $data);
    }

//    ========= its for customer_export_csv ===========
    public function c_customer_export_csv() {
        $offset = $this->input->post('ofset');
        $limit = $this->input->post('limit');

        if (($offset > 0) && ($limit >= 1)) {
            // file name 
            $filename = 'customer_' . date('Y-m-d') . '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");

            // get data 
            $usersData = $this->Customer_model->customer_csv_data($offset, $limit);

            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('customer_id', 'first_name', 'last_name', 'side_mark', 'phone', 'email', 'company', 'address', 'city', 'state', 'zip_code', 'country_code');
            fputcsv($file, $header);
            foreach ($usersData as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            exit;
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>'Please check valid start and limit data!</div>");
            redirect('customer-list');
        }
    }

//    ======== its for customer_export_pdf ===========
    public function c_customer_export_pdf() {
        $offset = $this->input->post('ofset');
        $limit = $this->input->post('limit');

        if (($offset > 0) && ($limit >= 1)) {
            $data['customer_list'] = $this->Customer_model->customer_pdf_data($offset, $limit);
            //=========== its for create pdf ==============
            $times = "BMSLINK Customer List_" . date('mdy');
            $this->load->library('pdfgenerator');
            $dompdf = new DOMPDF();
            $page = $this->load->view('c_level/customers/customer_export_pdf', $data, true);
            $dompdf->load_html($page);
            $dompdf->render();
            $output = $dompdf->output();
            file_put_contents("assets/c_level/pdf/customers/$times.pdf", $output);
            $filename = $times . '.pdf';
            $file_path = base_url() . 'assets/c_level/pdf/customers/' . $filename;

            $this->load->helper('download');
            force_download(FCPATH . 'assets/c_level/pdf/customers/' . $filename, null);
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>'Please check valid start and limit data!</div>");
            redirect('customer-list');
        }
    }

    //    ============= its for top_search_customer_order_info ====================
    public function top_search_customer_order_info() {

        $keyword = $this->input->post('keyword');
        $data['get_top_search_customer_info'] = $this->Customer_model->get_top_search_customer_info($keyword);
        $data['get_top_search_order_info'] = $this->Customer_model->get_top_search_order_info($keyword);
        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/customers/top_search_customer_order_info');
        $this->load->view('c_level/footer');
    }

//    =========== its for show_old_c_cstomer_comment ==============
    public function show_old_c_cstomer_comment() {
        $customer_id = $this->input->post('customer_id');
        $data['old_comments'] = $this->db->select('*')->from('appointment_calendar a')->where('a.customer_id', $customer_id)
                        ->get()->result();
        $this->load->view('c_level/customers/old_comment_list', $data);
    }

    /** Start added by insys */
//    =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('customer_info','customer_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Customer has been Deleted successfully.</div>");
        }
        redirect("customer-list");
    } 
    /** End added by insys */
}
