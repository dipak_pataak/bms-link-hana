<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->library('UpsTrack');
        $this->load->model('b_level/settings');
    }

	/*
	| -------------------------------------------------------------------
	| Track
	| -------------------------------------------------------------------
	*/

	public function track(){


		$shiperInfo = $this->get_shiper_info(1);
		$trackNumber = $this->input->post('trackNumber');
		$this->upstrack->addField('trackNumber',$trackNumber);
		list($response, $status) = $this->upstrack->processTrack($shiperInfo);
		$ups_response = json_decode($response);


		if($ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode!=NULL) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors code : ".$ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code."</p>
                <p>Description : ".$ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description."</p>
            </div>");
            redirect("track-order");

        } else{

        		$data['response'] = $ups_response;

				$this->load->view('c_level/header');
		        $this->load->view('c_level/sidebar');
		        $this->load->view('c_level/orders/traking', $data);
		        $this->load->view('c_level/footer');
        	
        }

	}


    public function get_shiper_info($method_id){

        $method = $this->db->where('id',$method_id)->get('shipping_method')->row();

        $company_profile = $this->settings->company_profile();

        $arrayName = array(

            'username'      => $method->username,
            'password'      => $method->password,
            'account_id'    => $method->account_id,
            'access_token'  => $method->access_token,
            'name'          => $company_profile[0]->first_name.' '.$company_profile[0]->last_name,
            'attentionname' => 'Attention Name',
            'description'   => 'This is test deiscription',
            'address'       => $company_profile[0]->address,
            'city'          => $company_profile[0]->city,
            'state'         => $company_profile[0]->state,
            'zip_code'      => $company_profile[0]->zip_code,
            'country_code'  => $company_profile[0]->country_code,
            'phone'         => $company_profile[0]->phone,
            'method_id'     => $method->id

        );

        return $arrayName;
    }





}