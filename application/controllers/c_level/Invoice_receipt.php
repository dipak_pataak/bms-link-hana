<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_receipt extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        $session_id = $this->session->userdata('session_id');
        $this->user_id = $this->session->userdata('user_id');

        $this->load->model('b_level/settings');
        $this->load->model('c_level/Order_model');
        $this->load->library('paypal_lib');
        $this->load->model('email_sender');
    }

    public function order_payment_update() {

        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "updated";
        $remarks = "order payment information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );

        $this->db->insert('accesslog', $accesslog_info);


        //============== close access log info =================
        $customer_id = $this->input->post('customer_id');
        $customer_no = $this->input->post('customer_no');
        $payment_method = $this->input->post('payment_method');
        $order_id = $this->input->post('order_id');

        $cHead = $this->db->select('HeadCode,level_id')->where('HeadName', $customer_no)->get('acc_coa')->row();


        if (@$cHead->HeadCode != NULL) {


            if ($payment_method == 'paypal') {

                $orderData = array(
                    'order_id' => $order_id,
                    'customer_id' => $customer_id,
                    'paid_amount' => $this->input->post('paid_amount'),
                );

                $orderDataPaypal = array(
                    'order_id' => $order_id,
                    'headcode' => $cHead->HeadCode,
                    'level_id' => $cHead->level_id,
                    'grand_total' => $this->input->post('grand_total'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'payment_method' => $this->input->post('payment_method'),
                    'card_number' => $this->input->post('card_number'),
                    'issuer' => $this->input->post('issuer'),
                    'customer_id' => $customer_id
                );

                $this->session->set_userdata($orderDataPaypal);

                $pyament = $this->payment_by_paypal($orderData);

            } else {


                $due = $this->input->post('due');

                if ($due > 0) {
                    $order_stage = 3;
                } else {
                    $order_stage = 2;
                }


                $rowData = $this->db->select('due,paid_amount,synk_status,created_by,commission_amt')->where('order_id', $order_id)->get('quatation_tbl')->row();

                // Get C Level User info for get commission : START
                $created_by = @$rowData->created_by;
                $commission_amt = @$rowData->commission_amt;
                if($created_by != ''){
                    $user_data = $this->db->select('fixed_commission,percentage_commission')->where('id', @$rowData->created_by)->get('user_info')->row();

                    if(@$user_data->fixed_commission != '' && $commission_amt == 0){
                        $commission_amt_fixed = @$user_data->fixed_commission;
                    }else{
                        $commission_amt_fixed = 0;
                    } 

                    if(@$user_data->percentage_commission != '' && @$user_data->percentage_commission > 0){
                        $commission_amt_percentage = round((($this->input->post('paid_amount') * @$user_data->percentage_commission)/100),2);
                    }else{
                        $commission_amt_percentage = 0;
                    } 

                    $commission_amt +=  ($commission_amt_fixed + $commission_amt_percentage); 
                }    
                // Get C Level User info for get commission : END


                // quatation table update
                $orderData = array(
                    'paid_amount' => @$rowData->paid_amount + $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'order_stage' => $order_stage,
                    'commission_amt'   => $commission_amt
                );

                //update to quatation table with pyament due 
                $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);
                //-----------------------------------------
                // quatation table update
                $payment_tbl = array(
                    'quotation_id' => $order_id,
                    'payment_method' => $this->input->post('payment_method'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'payment_date' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('user_id'),
                    'create_date' => date('Y-m-d')
                );


                if ($this->db->insert('payment_tbl', $payment_tbl)) {

                    $payment_id = $this->db->insert_id();

                    if ($payment_method == 'check') {


                        if (@$_FILES['check_image']['name']) {

                            $config['upload_path'] = './assets/c_level/uploads/check_img/';
                            $config['allowed_types'] = 'jpeg|jpg';
                            $config['overwrite'] = false;
                            $config['max_size'] = 3000;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('check_image')) {
                                $error = $this->upload->display_errors();
                                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                                redirect("new-order");
                            } else {

                                $data = $this->upload->data();
                                $check_image = $config['upload_path'] . $data['file_name'];
                            }
                        } else {
                            @$check_image = '';
                        }

                        $payment_check_tbl = array(
                            'payment_id' => $payment_id,
                            'check_number' => $this->input->post('check_number'),
                            'check_image' => $check_image
                        );

                        $this->db->insert('payment_check_tbl', $payment_check_tbl);
                    }
                }


                $voucher_no = $order_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $this->input->post('paid_amount');
                $cAID = $cHead->HeadCode;
                $IsPosted = 1;
                $CreateBy = $this->session->userdata('user_id');
                $createdate = date('Y-m-d H:i:s');

                //customer credit insert acc_transaction
                $customerCredit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $cAID,
                    'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'level_id'=>$cHead->level_id,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);
                //------------------------------------
                //b_level debit insert b_acc_transaction
                $payment_method = $this->input->post('payment_method');

                if ($payment_method == 'cash') {
                    $COAID = '1020101';
                }if ($payment_method == 'check') {
                    $COAID = '102010202';
                }if ($payment_method == 'card') {
                    $COAID = '1020103';
                }


                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $COAID,
                    'level_id'=>$cHead->level_id,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $b_levelDebit);
                //------------------------------------

                $cNotificationData = array(
                    'notification_text' => 'Payment has been received for ' . $order_id,
                    'go_to_url' => 'c_level/invoice_receipt/receipt/' . $order_id,
                    'created_by' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d')
                );

                $this->db->insert('c_notification_tbl', $cNotificationData);


                $this->email_sender->send_email(
                        $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject' => 'Order payment'
                        )
                );


                $this->Order_model->smsSend(
                    $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject' => 'Order payment'
                    )
                );


                if ($rowData->synk_status == 1) {

                    //$this->payment_to_b($order_id);
                    $b_order = $this->db->where('clevel_order_id',$order_id)->get('b_level_quatation_tbl')->row();
                    
                    $bs_url = base_url().'c_level/invoice_receipt/money_receipt/'.$order_id;
                    $bs_url1 = base_url().'c_level/make_payment/payment_to_b/'.$order_id;
                    $bs_url3 = base_url().'c_level/order_controller/order_view/'.$order_id;
                    
                    if($b_order->paid_amount<1){

                        echo "<script>
                        popupWindow = window.open('".$bs_url."','popUpWindow');
                        window.location.href='".$bs_url1."'; 
                        </script>";

                    } else {

                        echo "<script> popupWindow = window.open('".$bs_url."','popUpWindow'); </script>";
                        echo "<script>
                        window.location.href='".$bs_url3."'; 
                        </script>";
                    }

                    //sleep(2);
                    //redirect('c_level/make_payment/payment_to_b/' . $order_id);
                
                    $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                
                } else {

                    redirect('c_level/invoice_receipt/money_receipt/' . $order_id);
                    //redirect('c_level/invoice_receipt/receipt/' . $order_id);
                    //redirect('manage-order');
                    //$this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                }
            }

        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer account code not found! </div>");
            redirect('c_level/order_controller/order_view/' . $order_id);
        }
    }





    public function money_receipt($order_id = NULL) {


        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);

        $data['payment'] = $this->db->where('quotation_id', $order_id)->order_by('payment_id', 'DESC')->get('payment_tbl')->row();

        $data['company_profile'] = $this->settings->company_profile();
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();


        //$this->load->view('b_level/orders/order_receipt', $data);
        $this->load->view('c_level/orders/money_receipt', $data);
    }



    public function receipt($order_id = NULL) {
        $this->permission_c->check_label('receipt')->create()->redirect();

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        //dd($data['order_details']);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();

        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        //$this->load->view('c_level/orders/order_receipt',$data);
        $this->load->view('c_level/orders/invoice_print', $data);
        $this->load->view('c_level/footer');
    }



    public function payment_by_paypal($orderData) {

        $customer_id = $orderData['customer_id'];
        $order_id = $orderData['order_id'];


        //$discount = 15; //number_format((!empty($history->discount) ? $history->discount : 0), 2);
        //$item_name = "Order :: Test";
        // ---------------------
        //Set variables for paypal form
        $returnURL = base_url("c_level/invoice_receipt/success/$order_id/$customer_id"); //payment success url
        $cancelURL = base_url("c_level/invoice_receipt/cancel/$order_id/$customer_id"); //payment cancel url
        $notifyURL = base_url('c_level/invoice_receipt/ipn'); //ipn url
        //set session token
        $this->session->unset_userdata('_tran_token');
        $this->session->set_userdata(array('_tran_token' => $order_id));
        // set form auto fill data
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);


        $user_id = $this->session->userdata('user_id');

        $paypal = $this->db->select('*')
                ->from('gateway_tbl')
                ->where('default_status', 1)
                ->where('created_by', $this->user_id)
                ->where('level_type', 'c')
                ->get()
                ->row();


        if (empty($paypal)) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please Setup your paypal business account</div>");
            redirect('c_level/order_controller/order_view/' . $order_id);
        }


        $paypal_lib_currency_code = (!empty($paypal->currency) ? $paypal->currency : 'USD');
        $paypal_lib_ipn_log_file = BASEPATH . 'logs/paypal_ipn.log';
        $paypal_lib_ipn_log = TRUE;
        $paypal_lib_button_path = 'buttons';


        // my customization
        $this->paypal_lib->add_field('mode', $paypal->status);
        $this->paypal_lib->add_field('business', $paypal->payment_mail);
        $this->paypal_lib->add_field('paypal_lib_currency_code', $paypal_lib_currency_code);
        $this->paypal_lib->add_field('paypal_lib_ipn_log_file', $paypal_lib_ipn_log_file);
        $this->paypal_lib->add_field('paypal_lib_ipn_log', $paypal_lib_ipn_log);
        $this->paypal_lib->add_field('paypal_lib_button_path', $paypal_lib_button_path);
        //-----------------------
        // item information
        $this->paypal_lib->add_field('item_number', $order_id);
        //$this->paypal_lib->add_field('item_name', $item_name);
        $this->paypal_lib->add_field('amount', $orderData['paid_amount']);
        // $this->paypal_lib->add_field('quantity', $quantity);
        // $this->paypal_lib->add_field('discount_amount', $discount);
        // additional information 
        $this->paypal_lib->add_field('custom', $order_id);
        $this->paypal_lib->image('');
        // generates auto form
        $this->paypal_lib->paypal_auto_form();
    }

    public function success($order_id = null, $customer_id = null) {

        $this->order_save_update();
    }

    public function cancel($order_id = null, $customer_id = null) {


        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('headcode');
        $this->session->unset_userdata('grand_total');
        $this->session->unset_userdata('paid_amount');
        $this->session->unset_userdata('due');
        $this->session->unset_userdata('payment_method');
        $this->session->unset_userdata('card_number');
        $this->session->unset_userdata('issuer');
    }

    /*
     * Add this ipn url to your paypal account
     * Profile and Settings > My selling tools > 
     * Instant Payment Notification (IPN) > update 
     * Notification URL: (eg:- http://domain.com/website/paypal/ipn/)
     * Receive IPN messages (Enabled) 
     */

    public function ipn() {

        //paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["mc_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;

        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (preg_match("/VERIFIED/i", $result)) {
            //insert the transaction data into the database
            $this->load->model('Paypal_model');
            $this->Paypal_model->insertTransaction($data);
        }

        return true;
    }

    //Send Customer Email with invoice
    public function setmail($email, $file_path, $id = null, $name = null) {

        $mail_config_detail = $this->db->select('*')->from('mail_config_tbl')->get()->row();
        $subject = 'ticket Information';
        $message = "Congratulation Mr. " . ' ' . $name . "Your Purchase Order No-  " . '-' . $id;

        $config = Array(
            'protocol' => $mail_config_detail->protocol,
            'smtp_host' => $mail_config_detail->smtp_host,
            'smtp_port' => $mail_config_detail->smtp_port,
            'smtp_user' => $mail_config_detail->smtp_user,
            'smtp_pass' => $mail_config_detail->smtp_pass,
            'mailtype' => $mail_config_detail->mailtype,
            'charset' => 'utf-8'
        );


        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($mail_config_detail->smtp_user);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach($file_path);

        $check_email = $this->test_input($email);

        if (filter_var($check_email, FILTER_VALIDATE_EMAIL)) {

            if ($this->email->send()) {
                $this->session->set_flashdata(array('message' => "Email Sent Sucessfully"));
                return true;
            } else {
                $this->session->set_flashdata(array('exception' => "Please configure your mail."));
                return false;
            }
        } else {
            $this->session->set_userdata(array('message' => "Your Data Successfully Saved"));
            //redirect("website/Paypal/local_success/" . $id);
            // redirect("b-single-order-test");
        }
    }

    //Email testing for email
    public function test_input($data) {

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function order_save_update() {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "updated";
        $remarks = "order information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $order_id = $this->session->userdata('order_id');
        $customer_id = $this->session->userdata('customer_id');
        $payment_method = $this->session->userdata('payment_method');

        if (!empty($order_id)) {


            $due = $this->session->userdata('due');
            if ($due > 0) {
                $order_stage = 3;
            } else {
                $order_stage = 2;
            }

            $rowData = $this->db->select('due,paid_amount,synk_status')->where('order_id', $order_id)->get('quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'paid_amount' => @$rowData->paid_amount + $this->session->userdata('paid_amount'),
                'due' => $this->session->userdata('due'),
                'order_stage' => $order_stage
            );

            //update to quatation table with pyament due 
            $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);
            // quatation table update
            $payment_tbl = array(
                'quotation_id' => $order_id,
                'payment_method' => $this->session->userdata('payment_method'),
                'paid_amount' => $this->session->userdata('paid_amount'),
                'payment_date' => date('Y-m-d'),
                'created_by' => $this->session->userdata('user_id'),
                'create_date' => date('Y-m-d')
            );
            $this->db->insert('payment_tbl', $payment_tbl);

            $voucher_no = $this->session->userdata('order_id');
            $Vtype = "INV";
            $VDate = date('Y-m-d');
            $paid_amount = $this->session->userdata('paid_amount');
            $cAID = $this->session->userdata('headcode');
            $level_id = $this->session->userdata('level_id');
            $IsPosted = 1;
            $CreateBy = $this->session->userdata('user_id');
            $createdate = date('Y-m-d H:i:s');

            //customer credit insert b_acc_transaction
            $customerCredit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => 0,
                'Credit' => $paid_amount,
                'COAID' => $cAID,
                'level_id'=>$level_id,
                'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('acc_transaction', $customerCredit);

            //------------------------------------

            $COAID = '1020103';
            $c_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'Credit' => 0,
                'COAID' => $COAID,
                'level_id'=>$level_id,
                'Narration' => "Amount received for invoice #" . $voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('acc_transaction', $c_levelDebit);

            $this->session->unset_userdata('order_id');
            $this->session->unset_userdata('headcode');
            $this->session->unset_userdata('grand_total');
            $this->session->unset_userdata('paid_amount');
            $this->session->unset_userdata('due');
            $this->session->unset_userdata('payment_method');
            $this->session->unset_userdata('card_number');
            $this->session->unset_userdata('issuer');
            $this->session->unset_userdata('customer_id');
            $this->session->unset_userdata('level_id');


            $bNotificationData = array(
                'notification_text' => 'Payment has been received for ' . $order_id,
                'go_to_url' => 'c_level/invoice_receipt/receipt/' . $order_id,
                'created_by' => $this->session->userdata('user_id'),
                'date' => date('Y-m-d')
            );

            $this->db->insert('c_notification_tbl', $bNotificationData);


            $this->email_sender->send_email(
                    $data = array(
                'customer_id' => $customer_id,
                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                'subject' => 'Order payment'
                    )
            );


            $this->Order_model->smsSend(
                    $data = array(
                'customer_id' => $customer_id,
                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                'subject' => 'Order payment'
                    )
            );


            

            if ($rowData->synk_status == 1) {


                    //$this->payment_to_b($order_id);
                    $b_order = $this->db->where('clevel_order_id',$order_id)->get('b_level_quatation_tbl')->row();
                    
                    $bs_url = base_url().'c_level/invoice_receipt/money_receipt/'.$order_id;
                    $bs_url1 = base_url().'c_level/make_payment/payment_to_b/'.$order_id;
                    $bs_url3 = base_url().'c_level/order_controller/order_view/'.$order_id;
                    
                    if($b_order->paid_amount<1){

                        echo "<script>
                        popupWindow = window.open('".$bs_url."','popUpWindow');
                        window.location.href='".$bs_url1."'; 
                        </script>";

                    } else {

                        echo "<script> popupWindow = window.open('".$bs_url."','popUpWindow'); </script>";
                        echo "<script>
                        window.location.href='".$bs_url3."'; 
                        </script>";
                    }


                //redirect('c_level/make_payment/payment_to_b/' . $order_id);
                //$this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
            } else {

                redirect('c_level/invoice_receipt/money_receipt/' . $order_id);

                // redirect('c_level/invoice_receipt/receipt/' . $order_id);
               // redirect('manage-order');
                //$this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
            }
        }
    }

//    ============= its for random key generator ============
    public function random_keygenerator($mode = null, $len = null) {

        $result = "";
        if ($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4):
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

    public function order_id_generate() {

        $maxid = 0;
        $row = $this->db->query('SELECT MAX(order_id) AS `maxid` FROM `b_level_quatation_tbl`')->row();
        if ($row) {
            $maxid = $row->maxid;
        }

        if ($maxid == 0) {
            $custom_id = "154762855914ZJ-001";
        } else {
            $custom_id = $maxid;
        }

        $order_id = explode('-', $custom_id);

        $order_id = $order_id[1] + 1;
        $si_length = strlen((int) $order_id);
        $str = '000';
        $order_str = substr($str, $si_length);
        $passenger_id = time() . $this->random_keygenerator(1, 4) . "b-" . $order_str . $order_id;
        return $passenger_id;
    }

    public function synk_to_b($order_id = NULL) {

        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "synchronize";
        $remarks = "synchronize to BMS Decor order";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->session->userdata('main_b_id'),
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        // get customer information
        $query = $this->db->select('*')
                        ->from('customer_info')
                     /*   ->where('level_id', 1)*/
                        ->where('customer_user_id', $this->user_id)
                        ->order_by('customer_id', 'desc')
                        ->get()->row();




        // get sales tax
        $state_tax = $this->db->select('tax_rate')
                        ->from('us_state_tbl')
                        ->where('shortcode', $query->state)
                        ->get()->row();


        if (empty($query->customer_id)) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Error! information not found</div>");
            redirect('manage-order');
        }

        if (isset($order_id) && !empty($order_id)) {

            $orderd = $this->Order_model->get_orderd_by_id($order_id);
            $order_details = $this->Order_model->get_orderd_details_by_id($order_id);

            $g_order_id = $this->order_id_generate();
            $productData = [];
            $subtotal = 0;

            $barcode_img_path = '';

            if (!empty($g_order_id)) {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $g_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($g_order_id));
            }



            $attrData = [];
            $productData = [];

            foreach ($order_details as $value) {

                // get dealer_cost_factor
                $product_cost_f = $this->db->select('dealer_cost_factor')->where('product_id', $value->product_id)->where('customer_id', $query->customer_id)->get('b_cost_factor_tbl')->row();

                $product = $this->db->select('dealer_price')->where('product_id', $value->product_id)->get('product_tbl')->row();

                $proAtt = $this->db->where('fk_od_id', $value->row_id)->where('order_id', $value->order_id)->get('quatation_attributes')->row();

                if (isset($product_cost_f) && !empty($product_cost_f)) {

                    $cost_f = $product_cost_f->dealer_cost_factor;

                    // if ($product->dealer_price <= $product_cost_f->dealer_cost_factor) {
                    //     $cost_f = $product_cost_f->dealer_cost_factor;
                    // } else {
                    //     $cost_f = $product->dealer_price;
                    // }
                } else {

                    $cost_f = @$product->dealer_price;
                }

                $discount = ($value->list_price / 100) * @$cost_f;
                $unit_total_price = $value->list_price - (@$discount * $value->product_qty);

                $subtotal += $unit_total_price;

                $productData = array(
                    'order_id' => $g_order_id,
                    'room' => $value->room,
                    'product_id' => $value->product_id,
                    'product_qty' => $value->product_qty,
                    'list_price' => $value->list_price,
                    'discount' => @$cost_f,
                    'unit_total_price' => $unit_total_price,
                    'category_id' => $value->category_id,
                    'pattern_model_id' => $value->pattern_model_id,
                    'color_id' => $value->color_id,
                    'width' => $value->width,
                    'height' => $value->height,
                    'height_fraction_id' => $value->height_fraction_id,
                    'width_fraction_id' => $value->width_fraction_id,
                    'notes' => $value->notes
                );

                $this->db->insert('b_level_qutation_details', $productData);


                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_attribute' => @$proAtt->product_attribute
                );

                $this->db->insert('b_level_quatation_attributes', $attrData);
            }

            $stax = ($state_tax->tax_rate != NULL ? $state_tax->tax_rate : 0);
            $tax_val = ($subtotal / 100) * $stax;

            $orderData = array(
                'order_id' => $g_order_id,
                'clevel_order_id' => $order_id,
                'order_date' => $orderd->order_date,
                'customer_id' => $query->customer_id,
                'is_different_shipping' => $orderd->is_different_shipping,
                'different_shipping_address' => $orderd->different_shipping_address,
                'level_id' => $this->session->userdata('main_b_id'),
                'side_mark' => $orderd->side_mark,
                'upload_file' => $orderd->upload_file,
                'barcode' => @$barcode_img_path,
                'state_tax' => $stax,
                'shipping_charges' => 0,
                'installation_charge' => 0,
                'other_charge' => 0,
                'misc' => 0,
                'invoice_discount' => 0,
                'grand_total' => $subtotal + $tax_val,
                'subtotal' => $subtotal,
                'paid_amount' => 0,
                'due' => $subtotal + $tax_val,
                'order_status' => $orderd->order_status,
                'created_by' => $orderd->created_by,
                'updated_by' => '',
                'created_date' => $orderd->created_date,
                'updated_date' => '',
                'status' => 0
            );


            $this->db->insert('b_level_quatation_tbl', $orderData);

            $this->db->set('synk_status', 1)->where('order_id', $order_id)->update('quatation_tbl');

            redirect('c_level/make_payment/payment_to_b/' . $order_id);
        } else {

            redirect('manage-order');
        }
    }

    public function synk_data_to_b($order_id = NULL) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "synchronize";
        $remarks = "synchronize to BMS Decor order";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->session->userdata('main_b_id'),
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================

        $query = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', 1)
                        ->where('customer_user_id', $this->user_id)
                        ->order_by('customer_id', 'desc')
                        ->get()->row();



        // get sales tax
        $state_tax = $this->db->select('tax_rate')
                        ->from('us_state_tbl')
                        ->where('shortcode', $query->state)
                        ->get()->row();


        if (empty($query->customer_id)) {

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
            redirect('c_level/order_controller/order_view/' . $order_id);
        }

        if (isset($order_id) && !empty($order_id)) {

            $orderd = $this->Order_model->get_orderd_by_id($order_id);
            $order_details = $this->Order_model->get_orderd_details_by_id($order_id);

            $g_order_id = $this->order_id_generate();
            $productData = [];
            $subtotal = 0;

            $barcode_img_path = '';

            if (!empty($g_order_id)) {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $g_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($g_order_id));
            }



            $attrData = [];
            $productData = [];

            foreach ($order_details as $value) {

                // get dealer_cost_factor
                $product_cost_f = $this->db->select('dealer_cost_factor')->where('product_id', $product_id)->where('customer_id', $query->customer_id)->get('b_cost_factor_tbl')->row();

                $product = $this->db->select('dealer_price')->where('product_id', $value->product_id)->get('product_tbl')->row();
                $proAtt = $this->db->where('fk_od_id', $value->row_id)->where('order_id', $value->order_id)->get('quatation_attributes')->row();

                if (isset($product_cost_f) && !empty($product_cost_f)) {

                    // if ($product->dealer_price <= $product_cost_f->dealer_cost_factor) {
                    //     $cost_f = $product_cost_f->dealer_cost_factor;
                    // } else {
                    //     $cost_f = $product->dealer_price;
                    // }
                    $cost_f = $product->dealer_price;
                } else {
                    $cost_f = @$product->dealer_price;
                }

                $discount = ($value->list_price / 100) * @$cost_f;
                $unit_total_price = $value->list_price - (@$discount * $value->product_qty);
                $subtotal += $unit_total_price;

                $productData = array(
                    'order_id' => $g_order_id,
                    'room' => @$value->room,
                    'product_id' => $value->product_id,
                    'product_qty' => $value->product_qty,
                    'list_price' => $value->list_price,
                    'discount' => @$cost_f,
                    'unit_total_price' => $unit_total_price,
                    'category_id' => $value->category_id,
                    'pattern_model_id' => $value->pattern_model_id,
                    'color_id' => $value->color_id,
                    'width' => $value->width,
                    'height' => $value->height,
                    'height_fraction_id' => $value->height_fraction_id,
                    'width_fraction_id' => $value->width_fraction_id,
                    'notes' => $value->notes
                );

                $this->db->insert('b_level_qutation_details', $productData);


                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_attribute' => $proAtt->product_attribute
                );

                $this->db->insert('b_level_quatation_attributes', $attrData);
            }

            $stax = ($state_tax->tax_rate != NULL ? $state_tax->tax_rate : 0);
            $tax_val = ($subtotal / 100) * $stax;


            $orderData = array(
                'order_id' => $g_order_id,
                'clevel_order_id' => $order_id,
                'order_date' => $orderd->order_date,
                'customer_id' => $query->customer_id,
                'is_different_shipping' => $orderd->is_different_shipping,
                'different_shipping_address' => $orderd->different_shipping_address,
                'level_id' => $this->session->userdata('main_b_id'),
                'side_mark' => $orderd->side_mark,
                'upload_file' => $orderd->upload_file,
                'barcode' => @$barcode_img_path,
                'state_tax' => $stax,
                'shipping_charges' => 0,
                'installation_charge' => 0,
                'other_charge' => 0,
                'misc' => 0,
                'invoice_discount' => 0,
                'grand_total' => $subtotal + $tax_val,
                'subtotal' => $subtotal,
                'paid_amount' => 0,
                'due' => $subtotal + $tax_val,
                'order_status' => $orderd->order_status,
                'created_by' => $orderd->created_by,
                'updated_by' => '',
                'created_date' => $orderd->created_date,
                'updated_date' => '',
                'status' => 0
            );


            $this->db->insert('b_level_quatation_tbl', $orderData);

            $this->db->set('synk_status', 1)->where('order_id', $order_id)->update('quatation_tbl');

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order sync successfully! </div>");
        }

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");

        redirect('c_level/order_controller/order_view/' . $order_id);
    }

}
