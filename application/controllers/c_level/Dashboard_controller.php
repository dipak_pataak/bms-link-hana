<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($session_id == '' || $user_type != 'c') {
            redirect('c-level-logout');
        }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/settings');
        $this->load->model('c_level/User_model');
    }

    public function index() {
        
    }

//    =========== its for c-level dashboard =============
    public function dashboard() {

        
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $data['pending_invoice'] = $this->pending_invoice($level_id);
        $data['pending_order'] = $this->pending_order($level_id);
        $data['pending_appointment'] = $this->pending_appointment($level_id);
        $data['payment_done'] = $this->payment_done($level_id);
//        echo $data['pending_appointment'][0]->appointment_count;
//        dd($data['pending_appointment']);
        $data['total_new_order_count'] = $this->total_new_order_count($level_id);
        $data['day_wise_sale'] = $this->day_wise_sale($level_id);
        $data['week_wise_sale'] = $this->week_wise_sale($level_id);
        $data['month_wise_sale'] = $this->month_wise_sale($level_id);
        $data['year_wise_sale'] = $this->year_wise_sale($level_id);
        $data['total_customer_count'] = $this->total_customer_count($level_id);
        $data['total_commission'] = $this->total_commission_count($this->session->userdata('user_id'));
//        dd($data['year_wise_sale']);

        $months = '';
        $salesamount = '';
        $salesorder = '';
        $year = date('Y');
        $numbery = date('y');
        $prevyear = $numbery - 1;
        $prevyearformat = $year - 1;
        $syear = '';
        $syearformat = '';
        for ($k = 1; $k < 13; $k++) {
            $month = date('m', strtotime("+$k month"));
            $gety = date('y', strtotime("+$k month"));
            if ($gety == $numbery) {
                $syear = $prevyear;
                $syearformat = $prevyearformat;
            } else {
                $syear = $numbery;
                $syearformat = $year;
            }
//            echo $month; echo '<br>'; echo $syearformat;
            $monthly = $this->monthlysaleamount($syearformat, $month, $level_id);
            $monthlysaleorders = $this->monthlysaleorder($syearformat, $month, $level_id);
            $salesamount .= $monthly . ', ';
            $salesorder .= $monthlysaleorders . ', ';
            $months .= "'" . date('M-' . $syear, strtotime("+$k month")) . "',";
        }
        $data['monthly_sales_amount'] = trim($salesamount, ',');
        $data['monthly_sales_month'] = trim($months, ',');
        $data['monthlysaleorders'] = trim($salesorder, ',');


        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/dashboard');
        $this->load->view('c_level/footer');
    }

//    =========== its for pending_invoice ==============
    public function pending_invoice($level_id) {

        $this->db->where('due>', 0);
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('quatation_tbl');
        return ($num_rows); //  / 100;
    }

//    =========== its for pending_order ==============
    public function pending_order($level_id) {
        $this->db->where('due !=', 0);
        $this->db->where('order_stage!=', 7);
        $this->db->where('order_stage!=', 1);
        $this->db->where('order_stage!=', 6);
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('quatation_tbl');
        return ($num_rows); //  / 100;
    }

//    =========== its for pending_order ==============
    public function payment_done($level_id) {

        $this->db->where('synk_status!=', 0);
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('quatation_tbl');
        return ($num_rows); //  / 100;
        
    }

//    =========== its for pending appointment ============
    public function pending_appointment($level_id) {

        $today = date('Y-m-d');
        $sql = "SELECT COUNT(a.customer_id) as appointment_count FROM customer_info a
                    JOIN appointment_calendar b ON b.customer_id = a.customer_id
                    WHERE (a.now_status = 'scheduled' OR a.now_status = 're-schedule') 
                    AND a.level_id = $level_id AND b.appointment_date >= '$today'";
        $query = $this->db->query($sql)->result();
        return ($query);
    }

    //    =========== its for total new order count==============
    public function total_new_order_count($level_id) {
        $this->db->where('order_date', date('Y-m-d'));
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('quatation_tbl');
        return ($num_rows); //   / 100;
    }

//    ========== its for day wise sale =============
    public function day_wise_sale($level_id) {
        $this->db->where('order_date', date('Y-m-d'));
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('quatation_tbl');
        return ($num_rows); // / 100;
    }

//    ========== its for week wise sale =============
    public function week_wise_sale($level_id) {
        $last_week = date("Y-m-d", strtotime("last week"));
        $this->db->where('order_date >= ', $last_week);
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('quatation_tbl');
        return ($num_rows); // / 100;
    }

//    ============ its for month wise sale ============
    public function month_wise_sale($level_id) {
        $last_month = date("Y-m-d", strtotime("last month"));
        $this->db->where('order_date >=', $last_month);
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results("quatation_tbl");
        return ($num_rows); // / 100;
    }

//    ============== its for year wise sale ============
    public function year_wise_sale($level_id) {
        $last_year = date("Y-m-d", strtotime("last year"));
        $this->db->where('order_date >=', $last_year);
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results("quatation_tbl");
        return ($num_rows); // / 100;
    }

    //    =========== its for total customer count==============
    public function total_customer_count($level_id) {
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('customer_info');
        return ($num_rows); // / 100;
    }

    //    =========== its for total commission count==============
    public function total_commission_count($user_id) {
        $this->db->select('sum(commission_amt) AS total_commission');
        $this->db->where('created_by', $user_id);
        $res = $this->db->get('quatation_tbl');
        $data = $res->row_array();
        return (isset($data['total_commission']) ? round($data['total_commission'],2) : '0');
    }

    public function monthlysaleamount($year, $month, $level_id) {
        $groupby = "GROUP BY YEAR(order_date), MONTH(order_date)";
        $amount = '';
        $wherequery = "YEAR(order_date)='$year' AND month(order_date)='$month' AND level_id = '$level_id' GROUP BY YEAR(order_date), MONTH(order_date)";
        $this->db->select('round(SUM(grand_total),2) as amount');
        $this->db->from('quatation_tbl');
        $this->db->where($wherequery, NULL, FALSE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $row) {
                $amount .= $row->amount . ", ";
            }
            return trim($amount, ', ');
        }
        return 0;
    }

    public function monthlysaleorder($year, $month, $level_id) {
        $groupby = "GROUP BY YEAR(order_date), MONTH(order_date)";
        $order_number = '';
        $wherequery = "YEAR(order_date)='$year' AND month(order_date)='$month' AND level_id = '$level_id' GROUP BY YEAR(order_date), MONTH(order_date)";
        $this->db->select('COUNT(order_id) as order_count');
        $this->db->from('quatation_tbl');
        $this->db->where($wherequery, NULL, FALSE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $row) {
                $order_number .= $row->order_count . ", ";
            }
            return trim($order_number, ', ');
        }
        return 0;
    }

//    =========== its for b-level dashboard =============
    public function b_level_dashboard() {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/dashboard');
        $this->load->view('b_level/footer');
    }

}
