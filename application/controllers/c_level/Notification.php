<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {


    public function __construct() {
        parent::__construct();
    }



    public function notification_list(){

        $date = date('Y-m-d');
        $user_id = $this->session->userdata('user_id');

        $fromdate = $this->input->post('fromdate');
         $todate = $this->input->post('todate'); 

        $this->db->select("c_notification_tbl.*,CONCAT(user_info.first_name, '.', user_info.last_name) as fullname, user_info.user_image");
        $this->db->join('user_info', 'user_info.id=c_notification_tbl.created_by', 'left');
        $this->db->where('c_notification_tbl.created_by', $user_id);
        
        if($fromdate!=NULL && $todate!=NULL){

            $this->db->where('c_notification_tbl.date >= ', $fromdate);
            $this->db->where('c_notification_tbl.date <=', $todate);


        } elseif ($fromdate!=NULL) {
            $this->db->where('c_notification_tbl.date', $fromdate);
            
        }
        $this->db->order_by('id','DESC');
        

        $data['notification'] = $this->db->get('c_notification_tbl')->result();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/notification', $data);
        $this->load->view('c_level/footer');
                    

    }



}