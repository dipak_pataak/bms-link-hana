<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Tmspayment_gateway extends CI_Controller {


    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }



        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        // if ($session_id == '' || $user_type != 'c') {
        //    redirect('c-level-logout');
        // }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('b_level/settings');


        $this->load->library('gwapi');
    }





    public function index(){

        $cusstomer_info     = $this->db->where('customer_user_id',$this->user_id)->get('customer_info')->row();

        $customer_id        = $cusstomer_info->customer_id;

        $payment_method                 = $this->input->post('payment_method');
        $order_id                       = $this->input->post('order_id');
        $clevel_order_id                = $this->input->post('clevel_order_id');

        $paid_ammount                   = $this->input->post('paid_amount');

        $ccnumber                       = str_replace('-','',$this->input->post('card_number'));
        $card_holder_name               = $this->input->post('card_holder_name');
        $ccexp                          = $this->input->post('expiry_month').$this->input->post('expiry_year');
        $cvv                            = $this->input->post('cvv');
        $ip                             = $this->input->ip_address();


        $orderd = $this->db->select("*")->from('b_level_quatation_tbl')->where('order_id', $order_id)->get()->row();

        $cHead      = $this->db->select('HeadCode')->where('HeadName',$cusstomer_info->customer_no)->get('acc_coa')->row();
        $cmp_info   = $this->settings->company_profile();

        $machentinfo   = $this->db->where('level_id',$this->level_id)->get('tms_payment_setting')->row();



#----------------------------
#   payment gatway
#----------------------------       

        $this->gwapi->setLogin($machentinfo);
        $this->gwapi->setBilling($cusstomer_info);
        $this->gwapi->setShipping($cusstomer_info);
        $this->gwapi->setOrder($orderd->order_id, $orderdescription='', $tax=0,$Shipping=0,$ip);
        
        //$r = (object)$this->gwapi->doAuth($paid_ammount, $ccnumber, $ccexp, $cvv);
       
        //$r = (object)$this->gwapi->doCredit($paid_ammount, $ccnumber, $ccexp);
    
        $r = (object)$this->gwapi->doSale($paid_ammount, $ccnumber, $ccexp, $cvv);

#----------------------------



#----------------------------
#   payment set database
#----------------------------


        if($r->response=='1'){


                    $due = $this->input->post('due');

                    if($due>0){
                        $order_stage = 3;
                    }else{
                        $order_stage = 2;
                    }


                    $rowData = $this->db->select('due,paid_amount,synk_status')->where('order_id',$order_id)->get('b_level_quatation_tbl')->row();
                    // quatation table update
                    $orderData = array(
                        'paid_amount'                   => @$rowData->paid_amount+$this->input->post('paid_amount'),
                        'due'                           => $this->input->post('due'),
                        'order_stage'                   => $order_stage,
                        'status'                     => 1
                    );

                    //update to quatation table with pyament due 
                    $this->db->where('order_id',$order_id)->update('b_level_quatation_tbl',$orderData);
                    //-----------------------------------------


                    // quatation table update
                    $payment_tbl = array(
                        'quotation_id'                  => $order_id,
                        'payment_method'                => $this->input->post('payment_method'),
                        'paid_amount'                   => $this->input->post('paid_amount'),
                        'payment_date'                  => date('Y-m-d'),
                        'created_by'                    => $this->session->userdata('user_id'),
                        'create_date'                   => date('Y-m-d')
                    );

                    $this->db->insert('payment_tbl',$payment_tbl);



                    if(!empty($cHead->HeadCode)){
                        
                        $voucher_no = $order_id;
                        $Vtype = "INV";
                        $VDate = date('Y-m-d');
                        $paid_amount = $this->input->post('paid_amount');
                        $cAID = $cHead->HeadCode;
                        $IsPosted = 1;
                        $CreateBy = $this->session->userdata('user_id');
                        $createdate = date('Y-m-d H:i:s');

                        //customer credit insert b_acc_transaction
                        $customerCredit = array(

                            'VNo'           => $voucher_no,
                            'Vtype'         => $Vtype,
                            'VDate'         => $VDate,
                            'Debit'         => 0,
                            'Credit'        => $paid_amount,
                            'COAID'         => $cAID,
                            'level_id'=>$this->level_id,
                            'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                            'IsPosted'      => $IsPosted,
                            'CreateBy'      => $CreateBy,
                            'CreateDate'    => $createdate,
                            'IsAppove'      => 1
                        );

                        $this->db->insert('acc_transaction', $customerCredit);
                        //------------------------------------

                        //b_level debit insert b_acc_transaction
                        $payment_method = $this->input->post('payment_method');

                        $COAID = '1020103';

                        $b_levelDebit = array(
                            'VNo' => $voucher_no,
                            'Vtype' => $Vtype,
                            'VDate' => $VDate,
                            'Debit' => $paid_amount,
                            'Credit' => 0,
                            'COAID' => $COAID,
                            'level_id'=>$this->level_id,
                            'Narration' => "Amount received for invoice #".$voucher_no,
                            'IsPosted' => $IsPosted,
                            'CreateBy' => $CreateBy,
                            'CreateDate' => $createdate,
                            'IsAppove' => 1
                        );

                        $this->db->insert('acc_transaction', $b_levelDebit);
                        //------------------------------------

                    }

                    // C level notification

                    $cNotificationData = array(
                        'notification_text' => 'Payment has been done for'.$order_id,
                        'go_to_url'         => 'c_level/invoice_receipt/receipt/'.$clevel_order_id,
                        'created_by'        => $this->session->userdata('user_id'),
                        'date'              => date('Y-m-d')
                    );

                    $this->db->insert('c_notification_tbl',$cNotificationData);

                    //-------------------------

                    
                    // Blevel notification
                    $bNotificationData = array(

                        'notification_text'     =>  'Payment has been received for'.$order_id,
                        'go_to_url'             =>  'b_level/invoice_receipt/c_receipt/'.$order_id,
                        'created_by'            =>  $this->session->userdata('user_id'),
                        'date'                  =>  date('Y-m-d')
                        
                    );
                    $this->db->insert('b_notification_tbl',$bNotificationData);  
                    //--------------------------
                    

                    $data = array(
                        'customer_id' => $customer_id,
                        'message'     => 'Payment has been done for OrdreId '.$order_id,
                        'subject'     => 'Order payment'
                    );

                    

            if ($rowData->synk_status == 1) {


                    //$this->payment_to_b($order_id);
                    $b_order = $this->db->where('clevel_order_id',$clevel_order_id)->get('b_level_quatation_tbl')->row();
                    
                    $bs_url = base_url().'c_level/invoice_receipt/money_receipt/'.$clevel_order_id;
                    $bs_url1 = base_url().'c_level/make_payment/payment_to_b/'.$clevel_order_id;
                    $bs_url3 = base_url().'c_level/order_controller/order_view/'.$clevel_order_id;
                    
                    if($b_order->paid_amount<1){

                        echo "<script>
                        popupWindow = window.open('".$bs_url."','popUpWindow');
                        window.location.href='".$bs_url1."'; 
                        </script>";

                    } else {

                        echo "<script> popupWindow = window.open('".$bs_url."','popUpWindow'); </script>";
                        echo "<script>
                        window.location.href='".$bs_url3."'; 
                        </script>";
                    }


                    //  $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                    // redirect('c_level/make_payment/payment_to_b/'.$clevel_order_id);


  
            } else {

                redirect('c_level/invoice_receipt/money_receipt/' . $clevel_order_id);

                // redirect('c_level/invoice_receipt/receipt/' . $order_id);
               // redirect('manage-order');
                //$this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
            }


                    
                   


        } else{

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><p>".$r->response."</p><p>".$r->responsetext."</p> </div>");
            redirect('c_level/make_payment/payment_to_b/'.$clevel_order_id);

        }
#--------------------------
        
    }



    public function receive_payment(){


        $customer_id                    = $this->input->post('customer_id');
        $customer_no                    = $this->input->post('customer_no');
        $payment_method                 = $this->input->post('payment_method');
        $order_id                       = $this->input->post('order_id');
        $paid_ammount                   = $this->input->post('paid_amount');

        $ccnumber                       = str_replace('-','',$this->input->post('card_number'));

        $card_holder_name               = $this->input->post('card_holder_name');
        $ccexp                          = $this->input->post('expiry_month').$this->input->post('expiry_year');
        $cvv                            = $this->input->post('cvv');
        $ip                             = $this->input->ip_address();

        $cusstomer_info = $this->db->where('customer_id',$customer_id)->get('customer_info')->row();

        $orderd = $this->db->select("*")->from('quatation_tbl')->where('order_id', $order_id)->get()->row();
        
        $cHead      = $this->db->select('HeadCode,level_id')->where('HeadName',$customer_no)->get('acc_coa')->row();
        $cmp_info   = $this->settings->company_profile();

        $machentinfo   = $this->db->where('level_id',$this->level_id)->get('tms_payment_setting')->row();



#----------------------------
#   payment gatway
#----------------------------       

        $this->gwapi->setLogin($machentinfo);
        $this->gwapi->setBilling($cusstomer_info);
        $this->gwapi->setShipping($cusstomer_info);
        $this->gwapi->setOrder($orderd->order_id, $orderdescription='', $tax=0,$Shipping=0,$ip);
        
        //$r = (object)$this->gwapi->doAuth($paid_ammount, $ccnumber, $ccexp, $cvv);
       
        //$r = (object)$this->gwapi->doCredit($paid_ammount, $ccnumber, $ccexp);
    
        $r = (object)$this->gwapi->doSale($paid_ammount, $ccnumber, $ccexp, $cvv);

#----------------------------



#----------------------------
#   payment set database
#----------------------------


        if($r->response=='1'){


                    $due = $this->input->post('due');

                    if($due>0){
                        $order_stage = 3;
                    }else{
                        $order_stage = 2;
                    }


                    $rowData = $this->db->select('due,paid_amount')->where('order_id',$order_id)->get('quatation_tbl')->row();
                    // quatation table update
                    $orderData = array(
                        'paid_amount'                   => @$rowData->paid_amount+$this->input->post('paid_amount'),
                        'due'                           => $this->input->post('due'),
                        'order_stage'                   => $order_stage
                    );

                    //update to quatation table with pyament due 
                    $this->db->where('order_id',$order_id)->update('quatation_tbl',$orderData);
                    //-----------------------------------------


                    // quatation table update
                    $payment_tbl = array(
                        'quotation_id'                  => $order_id,
                        'payment_method'                => $this->input->post('payment_method'),
                        'paid_amount'                   => $this->input->post('paid_amount'),
                        'payment_date'                  => date('Y-m-d'),
                        'created_by'                    => $this->session->userdata('user_id'),
                        'create_date'                   => date('Y-m-d')
                    );

                    $this->db->insert('payment_tbl',$payment_tbl);



                    if(!empty($cHead->HeadCode)){

                        $voucher_no = $order_id;
                        $Vtype = "INV";
                        $VDate = date('Y-m-d');
                        $paid_amount = $this->input->post('paid_amount');
                        $cAID = $cHead->HeadCode;
                        $IsPosted = 1;
                        $CreateBy = $this->session->userdata('user_id');
                        $createdate = date('Y-m-d H:i:s');

                        //customer credit insert acc_transaction
                        $customerCredit = array(

                            'VNo'           => $voucher_no,
                            'Vtype'         => $Vtype,
                            'VDate'         => $VDate,
                            'Debit'         => 0,
                            'Credit'        => $paid_amount,
                            'COAID'         => $cAID,
                            'level_id'      => $cHead->cHead,
                            'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                            'IsPosted'      => $IsPosted,
                            'CreateBy'      => $CreateBy,
                            'CreateDate'    => $createdate,
                            'IsAppove'      => 1
                        );

                        $this->db->insert('acc_transaction', $customerCredit);
                        //------------------------------------

                        // debit acc_transaction
                        $payment_method = $this->input->post('payment_method');
                        
                        $COAID = '102010202';
                        

                        $b_levelDebit = array(
                            'VNo' => $voucher_no,
                            'Vtype' => $Vtype,
                            'VDate' => $VDate,
                            'Debit' => $paid_amount,
                            'Credit' => 0,
                            'COAID' => $COAID,
                            'level_id'      => $cHead->cHead,
                            'Narration' => "Amount received for invoice #".$voucher_no,
                            'IsPosted' => $IsPosted,
                            'CreateBy' => $CreateBy,
                            'CreateDate' => $createdate,
                            'IsAppove' => 1
                        );

                        $this->db->insert('acc_transaction', $b_levelDebit);
                        //------------------------------------

                    }

                    // C level notification

                    $cNotificationData = array(
                        'notification_text' => 'Payment has been received for '.$order_id,
                        'go_to_url'         => 'c_level/invoice_receipt/receipt/'.$order_id,
                        'created_by'        => $this->session->userdata('user_id'),
                        'date'              => date('Y-m-d')
                    );

                    $this->db->insert('c_notification_tbl',$cNotificationData);

                    //-------------------------


                    $this->Order_model->smsSend(

                        $data = array(
                            'customer_id' => $customer_id,
                            'message'     => 'Payment has been done for Order id '.$order_id,
                            'subject'     => 'Order payment'
                        )

                    );


                    if($rowData->synk_status==1){
                        
                        redirect('c_level/make_payment/payment_to_b/' . $order_id);
                        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                    
                    } else{

                        //redirect('c_level/invoice_receipt/receipt/' . $order_id);
                        redirect('manage-order');
                        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                    
                    }


                    
                    $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                    redirect('c_level/order_controller/order_view/'.$order_id);


        } else{

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><p>".$r->response."</p><p>".$r->responsetext."</p> </div>");
            redirect('c_level/order_controller/order_view/'.$order_id);

        }
#--------------------------
        
    }





}