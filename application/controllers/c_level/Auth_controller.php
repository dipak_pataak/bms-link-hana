<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

//        $session_id = $this->session->userdata('session_id');
//        $user_type = $this->session->userdata('user_type');
//        if ($user_type != 'c') {
//            redirect('c-level-logout');
//        }
        

        $this->baseurl = $this->config->item('base_url');
        $this->load->model('b_level/settings');
        $this->load->model('Auth_model');
        $this->load->model('c_level/User_model');
    
    }


    public function index() {

        $data['baseurl'] = $this->baseurl;
        $this->load->view('c_level/c_level_login', $data);

    }


    public function c_level_logout() {
        //update database status
//        $this->auth_model->last_logout();
        //destroy session
        //            ========= its for last logout info collect start ============
        $last_logout_info = array(
            'logout_datetime' => date("Y-m-d H:i:s"),
        );
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('log_info', $last_logout_info);
//            ========= its for last logout info collect close============ 
        $this->session->sess_destroy();

        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You are not valid user!</div>");
        redirect('c-level');
    }

    //    =========== its for c_level_login_process ==============
    public function authentication() {

        $password = $this->input->post('password');
        $email = $this->input->post('email');

        $data['user'] = (object) $userdata = array(
            'email' => $email,
            'password' => $password,
        );

        $user = $this->Auth_model->check_user($userdata);
        if($user==NULL){
             $this->session->set_flashdata('exception', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Unauthorized user</div>");
                redirect('c-level');
        }

//        echo '<pre>'; print_r($user); die();
//     =========== its for permission start ==================
       
        $checkPermission = $this->Auth_model->userPermission2($user->user_id);


        if ($checkPermission != NULL) {

            $permission = array();
            $permission1 = array();
            if (!empty($checkPermission)) {
                foreach ($checkPermission as $value) {
                    $permission[$value->module] = array(
                        'create' => $value->create,
                        'read' => $value->read,
                        'update' => $value->update,
                        'delete' => $value->delete
                    );

                    $permission1[$value->menu_title] = array(
                        'create' => $value->create,
                        'read' => $value->read,
                        'update' => $value->update,
                        'delete' => $value->delete
                    );
                }
            }
        }

        $main_b_id = "";

        if(empty($user->created_by))
            $main_b_id = $user->id;
        else{
            $b_user = $this->db->where('id',$user->created_by)->get('user_info')->row();
            $main_b_id = $b_user->id;
        }

//        =========== its for permission close ==================
//            echo "DDD";die();
        if ($user->user_type === 'c') {

            if ($user->is_admin == 1) {

                $session_data = array(
                    'isLogIn' => true,
                    'isAdmin' => (($user->is_admin == 1) ? true : false),
                    'user_type' => $user->user_type,
                    'user_id' => $user->user_id,
                    'admin_created_by' => $user->created_by,
                    'name' => $user->first_name . " " . $user->last_name,
//                'username' => $user->username,
                    'email' => $user->email,
                    'picture' => $user->user_image,
                    'permission' => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in' => TRUE,
                    'session_id' => session_id(),
                    'main_b_id' => $main_b_id,
                );

                
                //echo 'Admin -><pre>';                   print_r($session_data); die();
                $this->session->set_userdata($session_data);
                //            ========= its for last login info collect start ============
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );
                //echo '<pre>';            print_r($last_login_info); die();
                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);
//            ========= its for last login info collect close============   
                $this->session->set_flashdata('message', "Welcome Back !" . ' ' . $user->first_name . " " . $user->last_name);

                redirect('c-level-dashboard');


            } else if ($user->is_admin == 2 || $user->is_admin == 3) {

                $session_data = array(
                    'isLogIn'       => true,
                    'isAdmin'       => '', //(($user->is_admin == 2) ? true : false),
                    'user_type'         => $user->user_type,
                    'user_id'           => $user->user_id,
//                    'user_created_by' => $user->created_by,
                    'admin_created_by' => $user->created_by,
                    'name'              => $user->first_name . " " . $user->last_name,
//                'username' => $user->username,
                    'email'             => $user->email,
                    'picture'           => $user->user_image,
                    'permission'        => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in'         => TRUE,
                    'session_id'        => session_id(),
                    'main_b_id' => $main_b_id,
                );
                //  echo 'User -><pre>';                   print_r($session_data); die();
                $this->session->set_userdata($session_data);
                //            ========= its for last login info collect start ============
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );
//            echo '<pre>';            print_r($last_login_info); die();
                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);
//            ========= its for last login info collect close============ 
                $this->session->set_flashdata('message', "Welcome Back !" . ' ' . $user->first_name . " " . $user->last_name);
                redirect('c-level-dashboard');
                
                
            } else {

                $this->session->set_flashdata('exception', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Email or password is invalid</div>");

                redirect('c-level');
            }

        } else {

            $this->session->set_flashdata('exception', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> You are not a c-level user</div>");

            redirect('c-level');
        }

        // redirect('b-level-dashboard');
    }

//    ============ its for b-level-forgot-password-form ==============
    public function c_level_forgot_password_form() {

        $this->load->view('c_level/forgot_password_form');
    }

//    ============ its for b_level_forgot_password_send ==============
    public function c_level_forgot_password_send() {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "password forgot";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $data['get_mail_config'] = $this->settings->get_mail_config();
//        echo $data['get_mail_config'][0]->protocol;die();
        $email = $this->input->post('email');
        $checkEmail = $this->User_model->checkEmail($email);
//        echo '<pre>';        print_r($checkEmail);        die();
        if (!empty($checkEmail)) {
            $log_id = $checkEmail->row_id;
            $random_key = ("RK" . date('y') . strtoupper($this->randstrGen(2, 4)));
            $random_array = array(
                'password' => md5($random_key),
            );
            $this->db->where('row_id', $log_id);
            $this->db->update('log_info', $random_array);
            $this->User_model->sendLink($log_id, $email, $data, $random_key);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please check your mail!</div>");
            redirect('c-level');
        } else {
            $this->session->set_flashdata('exception', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Invalid your mail!</div>");
            redirect('c-level-forgot-password-form');
        }
    }

    //    ========= its for randomly key generate ==========
    function randstrGen($mode = null, $len = null) {
        $result = "";
        if ($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4):
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

}
