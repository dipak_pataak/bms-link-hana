<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }


        // if ($session_id == '' || $user_type != 'c') {
        //    redirect('c-level-logout');
        // }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('b_level/settings');
    }

    public function index() {
        
    }



    public function get_color_partan_model($product_id) {


        $pp = $this->db->select('colors,pattern_models_ids')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();

        $color_ids = explode(',', $pp->colors);
        $pattern_model_id = explode(',', $pp->pattern_models_ids);
        $colors = $this->db->where_in('id', $color_ids)->get('color_tbl')->result();
        $pattern_model = $this->db->where_in('pattern_model_id', @$pattern_model_id)->get('pattern_model_tbl')->result();
        $q = '';


        $q .= '<div class="row">
                    <label for="" class="col-sm-3">Select Pattern/Model</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="pattern_model_id" id="pattern_id" data-placeholder="-- select pattern/model --">
                            <option value="">-- select pattern/model --</option>';

                    foreach ($pattern_model as $pattern) {
                        $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
                    }

                    $q .= '</select>
                                </div>
                            </div><br/>';

        $q .= '
            <div class="row">
                <label for="" class="col-sm-3">Color</label>
                <div class="col-sm-6">
                    <select class="form-control select2" name="color_id" id="color_id" onchange="getColorCode(this.value)"  required="" data-placeholder="-- select one --">
                        <option value="">-- select one --</option>';
        foreach ($colors as $color) {
            $q .= '<option value="' . $color->id . '">' . $color->color_name . '</option>';
        }

        $q .= '</select>
                </div>
                <div  class="col-sm-1">Color code :</div>
                <div  class="col-sm-2"><input type="text" id="colorcode" onkeyup="getColorCode_select(this.value)" class="form-control"></div>
            </div>';

        echo $q;
    }


    public function get_color_code($id) {

        $colors = $this->db->where('id', $id)->get('color_tbl')->row();

        echo $colors->color_number;
    }

    public function get_color_code_select($keyword) {

        $colors = $this->db->where('color_number', $keyword)->get('color_tbl')->row();

        echo @$colors->id;
    }

    //------------------------------------------------------
    // Category waise subcategory
    //------------------------------------------------------
    public function category_wise_subcategory($category_id = null) {

        if ($category_id == 'none') {
            $category_id = ' ';
            $where = "parent_category < 0";
        } else {
            $where = "parent_category = $category_id";
        }

        $category_wise_subcategory = $this->db->select('*')
                        ->where($where)
                        ->get('category_tbl')->result();
//        echo '<pre>';        print_r($category_wise_subcategory);        die();
//        $category_wise_subcategory = $this->db->select('*')
//        ->where('parent_category', $category_id)
//        ->get('category_tbl')->result();

        $q = "";

        if (!empty($category_wise_subcategory)) {

            $q .= '<div class="row">
                    <label for="" class="col-sm-3">Select Sub Category</label>
                    <div class="col-sm-6">
                        <select class="form-control select2-single" name="subcategory_id" id="sub_category_id" data-placeholder="-- select one --">
                            <option value="">--Select--</option>';

            foreach ($category_wise_subcategory as $value) {
                $q .= "<option value='$value->category_id'>$value->category_name</option>";
            }

            $q .= '</select>
                    </div>
                </div>';
        }

        echo $q;
    }






    //------------------------------------------------
    //------------------------------------------------
    //      its for new_quotation
    //------------------------------------------------
    public function new_quotation() {



        $this->permission->check_label('new_order')->create()->redirect();

        $data['get_customer'] = $this->Order_model->get_customer();


        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['colors'] = $this->db->get('color_tbl')->result();
        $data['orderjs'] = "c_level/orders/order_js.php";
        $data['currencys'] = $this->settings->company_profile();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();

        $data['rooms'] = $this->db->get('rooms')->result();


        $data['binfo'] = $this->db->where('user_id', 1)->get('company_profile')->row();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/new_quotation', $data);
        $this->load->view('c_level/footer');


    }

    public function contri_price($price_type, $option_price, $main_price) {

        $q = '';
        if ($price_type == 1) {
            $price_total = $main_price + @$option_price;
            $contribution_price = (!empty($option_price) ? $option_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {
            $price_total = ($main_price * $option_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }
        return $q;
    }

    public function get_product_to_attribute($product_id) {

        $attributes = $this->db->select('product_attribute.*,attribute_tbl.attribute_name,attribute_tbl.attribute_type')
                        ->join('attribute_tbl', 'attribute_tbl.attribute_id=product_attribute.attribute_id')
                        ->where('product_attribute.product_id', $product_id)
                        ->order_by('attribute_tbl.position', 'ASC')
                        ->get('product_attribute')->result();

        // for price style
        $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();

        $main_price = 0;
        $q = '';
        $q .= '<input type="hidden" name="pricestyle"  value="' . $p->price_style_type . '" id="pricestyle" >';

        if ($p->price_style_type == 3) {

            // Fix price 

            $q .= '<div class="row hidden" style="display:none;">
                    <label class="col-sm-3">Fixed Price</label>
                    <div class="col-sm-6">
                        <input type="text" name="main_price" class="form-control" id="main_price" readonly value="' . $p->fixed_price . '" >
                    </div>
                </div><br>';

            $main_price = $p->fixed_price;


        } elseif ($p->price_style_type == 2) {

            // sqr fit price 
            $q .= '<input type="hidden" name="sqr_price"  value="' . $p->sqft_price . '" id="sqr_price" >';
            $q .= '<div class="row hidden" style="display:none;">
                        <label class="col-sm-3">Sqft Price Price</label>
                        <div class="col-sm-6">
                            <input type="text" name="main_price" class="form-control" id="main_price"  readonly value="' . $p->sqft_price . '" >
                        </div>
                    </div><br>';

            $main_price = $p->sqft_price;

        }


        foreach ($attributes as $attribute) {


            if ($attribute->attribute_type == 3) {

                $options = $this->db->select('attr_options.*,product_attr_option.id')
                                ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                                ->where('product_attr_option.pro_attr_id', $attribute->id)
                                ->order_by('attr_options.att_op_id', 'ASC')
                                ->get('product_attr_option')->result();


                $q .= '<div class="row">
                            
                        <label class="col-sm-3">' . $attribute->attribute_name . '</label>
                        <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                        <input type="hidden" name="attribute_value[]" value="">';

                foreach ($options as $op) {

                    $q .= '<div class="col-sm-3">
                                <label>' . $op->option_name . '</label>
                                <input type="hidden" name="op_id_' . $attribute->attribute_id . '[]" value="' . $op->id . '_' . $op->att_op_id . '">';
                    $q .= '<input type="text" name="op_value_' . $attribute->attribute_id . '[]" min="0"  class="form-control">';
                    $q .= '</div>';

                    $q .= $this->contri_price($op->price_type, $op->price, $main_price);
                }

                $q .= '<div class="col-sm-6" style="display:none;"></div>';
                $q .= '<div class="col-sm-4" style="margin-top:-7px;"></div>';

                $q .= '</div><br>';
            } elseif ($attribute->attribute_type == 2) {

                $options = $this->db->select('attr_options.*,product_attr_option.id')
                                ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                                ->where('product_attr_option.pro_attr_id', $attribute->id)
                                ->order_by('attr_options.att_op_id', 'ASC')
                                ->get('product_attr_option')->result();

                $q .= '<div class="row">
                            
                            <label class="col-sm-3">' . $attribute->attribute_name . '</label>

                            <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                            <input type="hidden" name="attribute_value[]" value="">';

                $q .= '<input type="hidden" name="op_value_' . $attribute->attribute_id . '[]" min="0"  class="form-control">';

                $q .= '<div class="col-sm-3">
                        
                        <select class="form-control select2 op_op_load options_' . $attribute->attribute_id . '" name="op_id_' . $attribute->attribute_id . '[]"  onChange="OptionOptions(this.value,' . $attribute->attribute_id . ')" data-placeholder="-- select pattern/model --">
                            
                            <option value="0_0" >--Select one--</option>';
                $so1 = 1;
                foreach ($options as $op) {
                    $q .= '<option value="' . $op->id . '_' . $op->att_op_id . '" ' . ($so1 == 1 ? 'selected' : '') . ' >' . $op->option_name . '</option>';
                    $so1++;
                }

                $q .= '</select></div>';

                $q .= '<div class="col-sm-3" style="display:none;"></div>
                                <div class="col-sm-12" style="/*margin-top:-7px;/*"></div>
                        </div><br>';
            } elseif ($attribute->attribute_type == 1) {

                $height = $attribute->attribute_name;
                $q .= '<div class="row">
                                <lable class="col-sm-3">' . $attribute->attribute_name . '</lable>
                                <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">

                                <div class="col-sm-3">
                                    <input type="text" name="attribute_value[]" id="' . $attribute->attribute_name . '" class="form-control">
                                </div>
                                
                        </div><br>';

            } else {

                $q .= '';

            }
        }

        echo $q;
    }

    //--------------------------------------------------
    // get product attributes options price
    public function get_product_attr_option_option_price($att_op_id, $main_price) {

        $option = $this->db->where('att_op_id', $att_op_id)->get('attr_options')->row();

        //$products = $this->db->where('product_id',$product_id)->get('product_tbl')->row();
        $main_price = (int) $main_price;
        $contribution_price = 0;

        if (!empty($option)) {

            if ($option->price_type == 1) {

                $price_total = $main_price + @$option->price;
                $prices = '<p>' . $main_price . '+' . @$option->price . '=' . $price_total . '</p>';
                $contribution_price = (!empty($option->price) ? $option->price : 0);
            } else {

                $price_total = ($main_price * $option->price) / 100;
                $prices = '<p>(' . $main_price . '*' . @$option->price . ')/100 = ' . ($main_price + $price_total) . '</p>';
                $contribution_price = (!empty($price_total) ? $price_total : 0);
            }

            $q = '';
            //option price
            $q .= '<input type="text" readonly name="option_price_' . $option->attribute_id . '" value="' . @$option->price . '" class="form-control">' . @$prices;
            //------
            $q .= '<input type="hidden" name="option_id_' . $option->attribute_id . '" value="' . @$option->att_op_id . '" class="form-control">';
            $q .= '<input type="hidden" name="option_value_' . $option->attribute_id . '" value="" class="form-control">';
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            echo $q;
        } else {
            $q = '';
        }
    }

    public function get_product_attr_option_option($pro_att_op_id, $attribute_id, $main_price) {

        $options = $this->db->select('attr_options.*')
                        ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                        ->where('product_attr_option.id', $pro_att_op_id)
                        ->get('product_attr_option')->row();

        $q = '';

        if (!empty($options)) {

            if (@$options->price_type == 1) {

                $price_total = $main_price + @$options->price;
                $contribution_price = (!empty($options->price) ? $options->price : 0);

                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            } else {

                $price_total = ($main_price * $options->price) / 100;
                $contribution_price = (!empty($price_total) ? $price_total : 0);

                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            }



            if (@$options->option_type == 4) {

                $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                                ->get('product_attr_option_option')->result();

                foreach ($opops as $op_op) {

                    $opopops = $this->db->where('attribute_id', $attribute_id)->where('att_op_op_id', $op_op->op_op_id)->get('attr_options_option_option_tbl')->result();

                    $q .= '<input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">';
                    $q .= '<div class="row"><label class="col-sm-3"><br>' . $op_op->op_op_name . '</label>
                            <div class="col-sm-3"><br><select class="form-control select2 op_op_op_load " id="mul_op_op_id' . $op_op->op_op_id . '" onChange="multiOptionPriceValue(this.value)" data-placeholder="-- select pattern/model --">
                                <option value="" >--Select one--</option>';
                    foreach ($opopops as $opopop) {
                        $q .= '<option value="' . $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id . '">' . $opopop->att_op_op_op_name . '</option>';
                    }
                    $q .= '</select></div></div>';
                    $q .= '<div class="col-sm-12" style="display:none;" >
                                <div class="col-sm-12" style="margin-top:-7px;"></div>
                            </div><br>';

                    $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price);
                }
            } elseif (@$options->option_type == 3) {

                $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                                ->get('product_attr_option_option')->result();


                foreach ($opops as $op_op) {

                    $q .= '<br><div class="row">
                            <label class="col-sm-3">' . $op_op->op_op_name . '</label>
                            <input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">
                            <div class="col-sm-3"><input type="text" name="op_op_value_' . $attribute_id . '[]"  class="form-control"></div>
                        </div>';

                    $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price);
                }
            } elseif (@$options->option_type == 2) {

                $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                                ->get('product_attr_option_option')->result();

                //$q .= '<input type="hidden" name="op_id_'.$attribute_id.'[]" value="' . $options->att_op_id. '" class="form-control">';
                //$q .= '<input type="hidden" name="op_price_'.$attribute_id.'" value="' . $contribution_price . '" class="form-control">';

                $q .= '<input type="hidden" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';
                $q .= '<div class="row"><label class="col-sm-3"><br></label>
                            <div class="col-sm-3"><br><select class="form-control select2 op_op_op_load" id="op_' . $options->att_op_id . '" name="op_op_id_' . $attribute_id . '[]"  onChange="OptionOptionsOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                                    <option value="0">--Select one--</option>';

                foreach ($opops as $op_op) {
                    $q .= '<option value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">' . $op_op->op_op_name . '</option>';
                }

                $q .= '</select></div></div><br>';

                $q .= '<div class="col-sm-12">
                                <div class="col-sm-12" style="/*margin-top:-7px;*/"></div>
                            </div><br>';
            } elseif (@$options->option_type == 1) {

                $q .= '<br>
                        <div class="row">
                            <label class="col-sm-3"><br></label>
                            <div class="col-sm-3"><br>
                                <input type="text" name="op_value_' . $attribute_id . '" class="form-control">
                            </div>
                        </div>
                    <br>';
            } else {

                $q .= '';
            }
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function get_product_attr_op_op_op($op_op_id, $pro_att_op_op_id, $attribute_id, $main_price) {

        $opop = $this->db->where('op_op_id', $op_op_id)->get('attr_options_option_tbl')->row();

        //$opopop = $this->db->where('att_op_op_op_id',$op_op_op_id)->get('attr_options_option_option_tbl')->row();

        $q = '';

        if ($opop->att_op_op_price_type == 1) {

            $price_total = $main_price + @$opop->att_op_op_price;
            $contribution_price = (!empty($opop->att_op_op_price) ? $opop->att_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opop->att_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        // $q .= '<input type="hidden" name="op_op_price_'.$attribute_id.'[]" value="' . $contribution_price . '" class="form-control">';

        if ($opop->type == 3) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*')
                            ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                            ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                            ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                            ->order_by('attr_options_option_option_tbl.att_op_op_op_id', 'ASC')
                            ->get('product_attr_option_option_option')->result();

            foreach ($opopop as $op_op_op) {

                $q .= '<div class="row">
                            <label class="col-sm-3"><br>' . $op_op_op->att_op_op_op_name . '</label>
                            <input type="hidden" name="op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">
                            <div class="col-sm-3"><br><input type="text" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control"></div>
                        </div>';

                $q .= $this->contri_price($op_op_op->att_op_op_op_price_type, $op_op_op->att_op_op_op_price, $main_price);
            }
        } elseif ($opop->type == 2) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*')
                            ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                            ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                            ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                            ->order_by('product_attr_option_option_option.op_op_op_id', 'ASC')
                            ->get('product_attr_option_option_option')->result();

            $q .= '<input type="hidden" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<div class="col-sm-3"><br>
                    <select class="form-control select2 op_op_op_op_load" id="op_op_' . $op_op_id . '" name="op_op_op_id_' . $attribute_id . '[]" onChange="OptionOptionsOptionOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                            <option value="0">--Select one--</option>';

            foreach ($opopop as $op_op_op) {
                $q .= '<option value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">' . $op_op_op->att_op_op_op_name . '</option>';
            }

            $q .= '</select></div><br>';

            $q .= '<div>
                         <div class="col-sm-12" style="/*margin-top:-7px;*/"></div>
                    </div><br>';
        } elseif ($opop->type == 1) {

            $q .= '<div class="col-sm-3"><br>
                        <input type="text" name="op_op_value_' . $attribute_id . '" class="form-control">
                    </div>
                <br>';
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function get_product_attr_op_op_op_op($op_op_op_id, $attribute_id, $main_price) {

        $opopop = $this->db->where('att_op_op_op_id', $op_op_op_id)->where('attribute_id', $attribute_id)->get('attr_options_option_option_tbl')->row();

        $q = '';

        if ($opopop->att_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopop->att_op_op_op_price;
            $contribution_price = (!empty($opopop->att_op_op_op_price) ? $opopop->att_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opopop->att_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        //$q .= '<input type="hidden" name="op_op_price_'.$attribute_id.'[]" value="' . $contribution_price . '" class="form-control">';

        if ($opopop->att_op_op_op_type == 3) {

            $opopopop = $this->db->select('*')
                            ->where('attribute_id', $attribute_id)
                            ->where('op_op_op_id', $op_op_op_id)
                            ->order_by('op_op_op_id', 'ASC')
                            ->get('attr_op_op_op_op_tbl')->result();

            foreach ($opopopop as $op_op_op_op) {

                $q .= '<br><div class="col-sm-12">
                            <label>' . $op_op_op_op->att_op_op_op_op_name . '</label>
                            <input type="hidden" name="op_op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">
                            <input type="text" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control">
                        </div>';

                $q .= $this->contri_price($op_op_op_op->att_op_op_op_op_price_type, $op_op_op_op->att_op_op_op_op_price, $main_price);
            }
        } elseif ($opopop->att_op_op_op_type == 2) {

            $opopopop = $this->db->select('*')
                            ->where('attribute_id', $attribute_id)
                            ->where('op_op_op_id', $op_op_op_id)
                            ->order_by('attr_op_op_op_op_tbl.att_op_op_op_op_id', 'ASC')
                            ->get('attr_op_op_op_op_tbl')->result();

            $q .= '<input type="hidden" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<div class="col-sm-3"><br><select class="form-control select2 op_op_op_op_op_load " id="op_op_op_' . $op_op_op_id . '"  name="op_op_op_op_id_' . $attribute_id . '[]" onChange="OptionFive(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                    <option value="0">--Select one--</option>';
            foreach ($opopopop as $op_op_op_op) {
                $q .= '<option value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">' . $op_op_op_op->att_op_op_op_op_name . '</option>';
            }
            $q .= '</select></div><br>';

            $q .= '<div>
                        <div class="col-sm-12"></div>
                    </div><br>';
        } elseif ($opopop->att_op_op_op_type == 1) {

            $q .= '<div class="col-sm-12">
                        <input type="text" name="op_op_op_value_' . $attribute_id . '" class="form-control">
                    </div>
                <br>';
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function multioption_price_value($op_op_op_id, $attribute_id, $main_price) {

        $opopopop = $this->db->select('*')
                        ->where('attribute_id', $attribute_id)
                        ->where('att_op_op_op_id', $op_op_op_id)
                        ->get('attr_options_option_option_tbl')->row();


        $q = '';

        if ($opopopop->att_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopopop->att_op_op_op_price;
            $contribution_price = (!empty($opopopop->att_op_op_op_price) ? $opopopop->att_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opopopop->att_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        $q .= '<div class="col-sm-3"><br><input type="text" name="op_op_value_' . $attribute_id . '[]" value="' . $opopopop->att_op_op_op_name . '" class="form-control"></div>';


        echo $q;
    }

    public function get_product_attr_op_five($att_op_op_op_op_id, $attribute_id, $main_price) {

        $opopopop = $this->db->select('*')
                        ->where('attribute_id', $attribute_id)
                        ->where('att_op_op_op_op_id', $att_op_op_op_op_id)
                        ->get('attr_op_op_op_op_tbl')->row();



        $q = '';

        if ($opopopop->att_op_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopopop->att_op_op_op_op_price;
            $contribution_price = (!empty($opopopop->att_op_op_op_op_price) ? $opopopop->att_op_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opopopop->att_op_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        if ($opopopop->att_op_op_op_op_type == 1) {

            $q .= '<div class="col-sm-3"><br><input type="text" name="op_op_op_op_value_' . $attribute_id . '" class="form-control"></div>';
        } else {

            $q .= '';
        }

        echo $q;
    }

    //-----------------------------------------------
    //-----------------------------------------------
    //      Get product price
    //-----------------------------------------------

    public function get_product_row_col_price($height = NULL, $width = NULL, $product_id = NULL, $pattern_id = NULL) {

        
            $q = "";
            $st = "";
            $row = "";
            $col = "";
            $prince = "";


        if ($height!=NULL && $width!=NULL && !empty($product_id)) {

            $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();



            if (!empty($p->price_style_type) && $p->price_style_type == 1) {

                $prince = $this->db->where('style_id', $p->price_rowcol_style_id)
                                ->where('row', $width)
                                ->where('col', $height)
                                ->get('price_style')->row();

                if (!empty($prince)) {

                    $q .= '<div class="row hidden" style="display:none;">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $prince->price . '" >
                            </div>
                        </div>';
                    $st = 1;

                    $row = $prince->row;
                    $col = $prince->col;
                    $prince = $prince->price;

                } else {

                    $prince = $this->db->where('style_id', $p->price_rowcol_style_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();

                    

                    $q .= '<div class="row hidden" style="display:none;">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . ($prince->price!=NULL?$prince->price:0) . '" >
                            </div>
                        </div>';

                    $row = @$prince->row;
                    $col = @$prince->col;
                    $prince = ($prince->price!=NULL?$prince->price:0);
                    $st = 2;
                }


            } elseif (!empty($p->price_style_type) && $p->price_style_type == 4) {

                $pg = $this->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();

                $price = $this->db->where('style_id', $pg->group_id)
                                ->where('row <=', $width)
                                ->where('col <=', $height)
                                ->order_by('row_id', 'asc')
                                ->limit(1)
                                ->get('price_style')->row();


                $pc = ($price != NULL ? $price->price : 0);

                $q .= '<div class="row hidden" style="display:none;">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';

                $row = $price->row;
                $col = $price->col;
                $prince = $pc;
                $st = 2;

            }

            $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
            echo json_encode($arr);

        }else{

            $q .= '<div class="row hidden" style="display:none;">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="0" >
                            </div>
                        </div>';
                    $st = 1;

                    $row = @$prince->row;
                    $col = @$prince->col;
                    $prince = 0;

            $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
            echo json_encode($arr);
        }

    }



    //-----------------------------------------------
    //------------------------------------------
    //  Product add to cart
    //-------------------------------------------
    public function add_to_cart() {

        $attributes = $this->input->post('attribute_id');
        $attribute_value = $this->input->post('attribute_value');

        $attrib = [];

        foreach ($attributes as $key => $att) {


            $options = [];
            $op_op_s = [];
            $op_op_op_s = [];
            $op_op_op_op_s = [];


            $ops = $this->input->post('op_id_' . $att);
            $option_value = $this->input->post('op_value_' . $att);

            if (isset($ops) && is_array($ops)) {

                foreach ($ops as $key => $op) {

                    $options[] = array(
                        'option_id' => explode('_', $op)[1],
                        'option_value' => $option_value[$key]
                    );
                }
            }

            $opopid = $this->input->post('op_op_id_' . $att);
            $op_op_value = $this->input->post('op_op_value_' . $att);

            if (isset($opopid) && is_array($opopid)) {

                foreach ($opopid as $key => $opop) {
                    $op_op_s[] = array(
                        'op_op_id' => explode('_', $opop)[0],
                        'op_op_value' => $op_op_value[$key]
                    );
                }
            }

            $opopopid = $this->input->post('op_op_op_id_' . $att);
            $op_op_op_value = $this->input->post('op_op_op_value_' . $att);

            if (isset($opopopid) && is_array($opopopid)) {

                foreach ($opopopid as $key => $opopop) {

                    $op_op_op_s[] = array(
                        'op_op_op_id' => explode('_', $opopop)[0],
                        'op_op_op_value' => $op_op_op_value[$key]
                    );
                }
            }


            $opopopopid = $this->input->post('op_op_op_op_id_' . $att);
            $op_op_op_op_value = $this->input->post('op_op_op_op_value_' . $att);


            if (isset($opopopopid) && is_array($opopopopid)) {

                foreach ($opopopopid as $key => $opopopop) {

                    $op_op_op_op_s[] = array(
                        'op_op_op_op_id' => explode('_', $opopopop)[0],
                        'op_op_op_op_value' => $op_op_op_op_value[$key]
                    );
                }
            }


            $attrib[] = array(
                'attribute_id' => $att,
                'attribute_value' => $attribute_value[@$key],
                'options' => @$options,
                'opop' => @$op_op_s,
                'opopop' => @$op_op_op_s,
                'opopopop' => @$op_op_op_op_s
            );
        }



        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $price = $this->input->post('price');
        $total_price = $this->input->post('total_price');
        $notes = $this->input->post('notes');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $room = $this->input->post('room');


        $product = $this->db->select('product_id,product_name,category_id,subcategory_id,dealer_price,individual_price')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();


        $id = $product->product_id . $width . $height . $total_price;

        $product_name = str_replace('&quot;', '"', $product->product_name);

        $cartData = array(
            'id' => $id,
            'product_id' => $product->product_id,
            'room' => $room,
            'name' => $product_name,
            'price' => $total_price,
            'qty' => 1,
            'dealer_price' => $product->dealer_price,
            'individual_price' => $product->individual_price,
            'category_id' => $category_id,
            'pattern_model_id' => $pattern_model_id,
            'color_id' => $color_id,
            'width' => $width,
            'height' => $height,
            'width_fraction_id' => $width_fraction_id,
            'height_fraction_id' => $height_fraction_id,
            'notes' => $notes,
            'row_status' => 1,
            'att_options' => json_encode($attrib)
        );



        $this->cart->insert($cartData);
        echo 1;
    }

    // END
    // ------------------------------------
    //-------------------------------------
    // Clear Cart 
    // ------------------------------------
    public function clear_cart() {
        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Cleare successfully! </div>");
        redirect('new-quotation');
    }

    // END--------------------------------
    //---------------------------------------
    // Delete Cart item
    // --------------------------------------
    public function delete_cart_item($rowid) {
        $this->cart->remove($rowid);
        echo 1;
    }

    //--------------------------------------
    //-------------------------------------

    public function getproductcomission($product_id, $customer_id) {

        $product = $this->db->select('individual_cost_factor')
                        ->where('product_id', $product_id)
                        ->get('c_cost_factor_tbl')->row();

        $commission = [];

        if (!empty($product)) {

            $commission = array(
                'individual_price' => $product->individual_cost_factor
            );
        } else {

            $product = $this->db->select('individual_price')
                            ->where('product_id', $product_id)
                            ->get('product_tbl')->row();

            $commission = array(
                'individual_price' => $product->individual_price
            );
        }
        echo json_encode($commission);
    }

#-----------------------------------
#   order save
#----------------------------------

    public function save_order() {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "inserted";
        $remarks = "order information inserted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $products = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $orderid = $this->input->post('orderid');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');


        if (!empty($this->input->post('synk_status'))) {
            $synk_status = 1;
        } else {
            $synk_status = 0;
        }

        $barcode_img_path = '';

        if (!empty($orderid)) {
            $this->load->library('barcode/br_code');
            $barcode_img_path = 'assets/barcode/c/' . $orderid . '.jpg';
            file_put_contents($barcode_img_path, $this->br_code->gcode($orderid));
        }


        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/file/';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['overwrite'] = false;
            $config['max_size'] = 3000;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("new-order");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            @$upload_file = '';
        }


        $list_price = $this->input->post('list_price');

        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');

        $orderData = array(
            'order_id' => $orderid,
            'order_date' => $this->input->post('order_date'),
            'customer_id' => $this->input->post('customer_id'),
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $this->level_id,
            'side_mark' => $this->input->post('side_mark'),
            'upload_file' => $upload_file,
            'barcode' => @$barcode_img_path,
            'state_tax' => $this->input->post('tax'),
            'shipping_charges' => 0,
            'installation_charge' => $this->input->post('install_charge'),
            'other_charge' => $this->input->post('other_charge'),
            'misc' => $this->input->post('misc'),
            'invoice_discount' => $this->input->post('invoice_discount'),
            'grand_total' => $this->input->post('grand_total'),
            'subtotal' => $this->input->post('subtotal'),
            'paid_amount' => 0,
            'due' => $this->input->post('grand_total'),
            'order_status' => $this->input->post('order_status'),
            'created_by' => $this->user_id,
            'updated_by' => '',
            'created_date' => date('Y-m-d'),
            'updated_date' => '',
            'synk_status' => $synk_status
        );

        $order_status = $this->input->post('order_status');

        if ($this->db->insert('quatation_tbl', $orderData)) {

            foreach ($products as $key => $product_id) {


                $productData = array(
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'room' => $room[$key],
                    'product_qty' => $qty[$key],
                    'list_price' => $list_price[$key],
                    'discount' => $discount[$key],
                    'unit_total_price' => $utprice[$key],
                    'category_id' => $category[$key],
                    'pattern_model_id' => $pattern_model[$key],
                    'color_id' => $color[$key],
                    'width' => $width[$key],
                    'height' => $height[$key],
                    'height_fraction_id' => $height_fraction_id[$key],
                    'width_fraction_id' => $width_fraction_id[$key],
                    'notes' => $notes[$key]
                );


                //order details
                $this->db->insert('qutation_details', $productData);

                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'product_attribute' => $attributes[$key]
                );

                //order attributes
                $this->db->insert('quatation_attributes', $attrData);
            }


            // send email for customer
            $this->Order_model->send_link($orderData);
            //-----------------------
            // clear the cart session
            $this->cart->destroy();

            if ($synk_status == 1) {

                //$this->synk_data_to_b($order_id);
                redirect('c_level/invoice_receipt/synk_data_to_b/' . $orderid);

            } 

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
            redirect('c_level/order_controller/order_view/' . $orderid);
        
        //}
        
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
            redirect("new-quotation");
        }
    }


    public function synk_data_to_b($order_id = NULL) {
   
        $action_page = $this->uri->segment(2);
        $action_done = "synchronize";
        $remarks = "synchronize to BMS Decor order";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

//        ============== close access log info =================

        $query = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', 1)
                        ->where('customer_user_id', $this->user_id)
                        ->order_by('customer_id', 'desc')
                        ->get()->row();



        if (empty($query->customer_id)) {

            return 1;
        }

        if (isset($order_id) && !empty($order_id)) {

            $orderd = $this->Order_model->get_orderd_by_id($order_id);
            $order_details = $this->Order_model->get_orderd_details_by_id($order_id);

            $g_order_id = $this->order_id_generate();
            $productData = [];
            $grand_total = 0;

            $barcode_img_path = '';

            if (!empty($g_order_id)) {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $g_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($g_order_id));
            }



            $attrData = [];
            $productData = [];

            foreach ($order_details as $value) {

                // get dealer_cost_factor
                $product_cost_f = $this->db->select('dealer_cost_factor')->where('product_id', $value->product_id)->where('customer_id', $query->customer_id)->get('b_cost_factor_tbl')->row();
                //
                $product = $this->db->select('dealer_price')->where('product_id', $value->product_id)->get('product_tbl')->row();
                
                //
                $proAtt = $this->db->where('fk_od_id', $value->row_id)->where('order_id', $value->order_id)->get('quatation_attributes')->row();

                if (isset($product_cost_f) && !empty($product_cost_f)) {

                    $cost_f = $product_cost_f->dealer_cost_factor;

                    // if ($product->dealer_price <= $product_cost_f->dealer_cost_factor) {
                    //     $cost_f = $product_cost_f->dealer_cost_factor;
                    // } else {
                    //     $cost_f = $product->dealer_price;
                    // }

                } else {
                    $cost_f = @$product->dealer_price;
                }

                

                $discount = ($value->list_price / 100) * @$cost_f;
                $unit_total_price = $value->list_price - @$discount;
                $grand_total += $unit_total_price;

                $productData = array(
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_qty' => $value->product_qty,
                    'list_price' => $value->list_price,
                    'discount' => @$cost_f,
                    'unit_total_price' => $unit_total_price,
                    'category_id' => $value->category_id,
                    'pattern_model_id' => $value->pattern_model_id,
                    'color_id' => $value->color_id,
                    'width' => $value->width,
                    'height' => $value->height,
                    'height_fraction_id' => $value->height_fraction_id,
                    'width_fraction_id' => $value->width_fraction_id,
                    'notes' => $value->notes
                );

                $this->db->insert('b_level_qutation_details', $productData);


                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_attribute' => $proAtt->product_attribute
                );

                $this->db->insert('b_level_quatation_attributes', $attrData);
            }



            $orderData = array(
                'order_id' => $g_order_id,
                'clevel_order_id' => $order_id,
                'order_date' => $orderd->order_date,
                'customer_id' => $query->customer_id,
                'is_different_shipping' => $orderd->is_different_shipping,
                'different_shipping_address' => $orderd->different_shipping_address,
                'level_id' => $this->user_id,
                'side_mark' => $orderd->side_mark,
                'upload_file' => $orderd->upload_file,
                'barcode' => @$barcode_img_path,
                'state_tax' => 0,
                'shipping_charges' => 0,
                'installation_charge' => 0,
                'other_charge' => 0,
                'misc' => 0,
                'invoice_discount' => 0,
                'grand_total' => $grand_total,
                'subtotal' => $grand_total,
                'paid_amount' => 0,
                'due' => $grand_total,
                'order_status' => $orderd->order_status,
                'created_by' => $orderd->created_by,
                'updated_by' => '',
                'created_date' => $orderd->created_date,
                'updated_date' => '',
                'status' => 0
            );


            $this->db->insert('b_level_quatation_tbl', $orderData);

            $this->db->set('synk_status', 1)->where('order_id', $order_id)->update('quatation_tbl');

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order synk successfully! </div>");
            return 1;

        } else {

            return 1;
        }
    }







#-------------------------------------------
#   get side mark and sales tax
#-------------------------------------------

    public function customer_wise_sidemark($customer_id = null) {


        $customer_sidemark = $this->db->select('*')
                        ->from('customer_info')
                        ->where('customer_id', $customer_id)
                        ->get()->row();

        $sales_tax = $this->db->select('tax_rate')
                        ->from('c_us_state_tbl')
                        ->where('shortcode', $customer_sidemark->state)
                        ->where('level_id', $this->level_id)
                        ->get()->row();

                        $n = (object)$new_arr = array_merge((array)$customer_sidemark,(array)$sales_tax);

        echo json_encode($n);

    }

#--------------------------------------




    #---------------------------------
    #   get sales tax by c state
    #---------------------------------
    public function different_shipping_tax() {

        $val = $this->input->post('vs');
        $state = explode(',', $val);
        $different_state_tax = $this->db->select('tax_rate')
                        ->from('c_us_state_tbl')
                        ->where('state_name', $state[1])
                        ->where('level_id', $this->level_id)
                        ->get()->row();
        echo json_encode($different_state_tax);

    }

    public function order_id_generate() {

        $last_order_id = $this->db->select('order_id')->from('quatation_tbl')->order_by('id', 'desc')->get()->row();
        $custom_id = @$last_order_id->order_id;

        if (empty($custom_id)) {
            $custom_id = "154762855914ZJ-001";
        } else {
            $custom_id = $custom_id;
        }
        $order_id = explode('-', $custom_id);
        $order_id = $order_id[1] + 1;
        $si_length = strlen((int) $order_id);
        $str = '000';
        $order_str = substr($str, $si_length);

        $passenger_id = time() . $this->random_keygenerator(1, 4) . "c-" . $order_str . $order_id;
        echo strtoupper($passenger_id);
    }

//    ============= its for random key generator ============
    public function random_keygenerator($mode = null, $len = null) {
        $result = "";
        if ($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4):
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

    //    =========== its for quotation_edit ===============
    public function quotation_edit() {

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/quotation_edit');
        $this->load->view('c_level/footer');
    }

    //    =========== its for manage_order ===============
    public function manage_order() {

        $this->permission->check_label('manage_order')->create()->redirect();

        $search = (object) array(
                    'order_id' => $this->input->post('order_id'),
                    'customer_id' => $this->input->post('customer_id'),
                    'order_date' => $this->input->post('order_date'),
                    'order_stage' => $this->input->post('order_stage'),
                    'level_id' =>$this->level_id,
                    'user_id' =>''
        );


        $data['customers'] = $this->Order_model->get_customer();

        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['orderd'] = $this->Order_model->get_all_orderd($search);
        $data['search'] = $search;
        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/manage_order', $data);
        $this->load->view('c_level/footer');
    }

    //    =========== its for invoice_print ===============
    // public function invoice_print() {
    //     $search = (object) array(
    //         'order_id' =>$this->input->post('order_id'),
    //         'product_id' =>$this->input->post('product_id'),
    //         'customer_id' =>$this->input->post('customer_id'),
    //         'order_date' =>$this->input->post('order_date'),
    //         'order_stage' =>$this->input->post('order_stage')
    //     );
    //     $data['customers'] = $this->Order_model->get_customer();
    //     $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
    //     $data['orderd'] = $this->Order_model->get_all_paid_orderd($search);
    //     $data['search'] = $search;
    //     $this->load->view('c_level/header');
    //     $this->load->view('c_level/sidebar');
    //     $this->load->view('c_level/orders/invoice_print');
    //     $this->load->view('c_level/footer');
    // }
    //    =========== its for manage_invoice ===============
    public function manage_invoice() {

        //$this->permission->check_label('manage_invoice')->create()->redirect();

        $search = (object) array(
                    'order_id' => $this->input->post('order_id'),
                    'customer_id' => $this->input->post('customer_id'),
                    'order_date' => $this->input->post('order_date'),
                    'order_stage' => $this->input->post('order_stage')
        );

        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['orderd'] = $this->Order_model->get_all_invoice_orderd($search);
        $data['search'] = $search;
        $data['cmp_info'] = $this->settings->company_profile();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/manage_invoice', $data);
        $this->load->view('c_level/footer');
    }




    //    =========== its for order_cancel ===============
    public function order_cancel() {

//        $this->permission->check_label('order_cancel')->create()->redirect();

        $search = (object) array(
                    'order_id' => $this->input->post('order_id'),
                    'customer_id' => $this->input->post('customer_id'),
                    'order_date' => $this->input->post('order_date'),
                    'order_stage' => 6
        );


        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['orderd'] = $this->Order_model->get_all_cancel_orderd($search);
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/order_cancel', $data);
        $this->load->view('c_level/footer');
    }

    //    =========== its for track_order ===============
    public function track_order() {
//        $this->permission->check_label('track_order')->create()->redirect();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/track_order');
        $this->load->view('c_level/footer');
    }



    //    =========== its for my_orders ===============
    public function my_orders() {


        $search = (object) array(
            'order_id' => $this->input->post('order_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_date' => $this->input->post('order_date'),
            'order_stage' => $this->input->post('order_stage'),
            'level_id' => $this->level_id,
            'user_id' => $this->user_id
        );


        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();

        $data['orderd']  = $this->Order_model->get_all_orderd($search);
        $data['search'] = $search;
        $data['company_profile'] = $this->settings->company_profile();



        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/my_orders',$data);
        $this->load->view('c_level/footer');

    }

    // 
    public function shipment($order_id = NULL) {

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['methods'] = $this->db->get('shipping_method')->result();
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/shipment_form', $data);
        $this->load->view('c_level/footer');
    }

    public function shipping() {

        $order_id = $this->input->post('orderid');
        $length = $this->input->post('length');
        $weight = $this->input->post('weight');
        $method_id = $this->input->post('ship_method');
        $service_type = $this->input->post('service_type');
        $shipping_address = $this->input->post('shippin_address');

        $shipDataCheck = $this->db->where('order_id', $order_id)->get('shipment_data')->num_rows();

        if ($shipDataCheck > 0) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment alredy done, can't back process</div>");
            redirect("c_level/order_controller/order_view/" . $order_id);
        }


        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();

        $order = $this->db->select('quatation_tbl.*,customer_info.*')
                        ->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id')
                        ->where('order_id', $order_id)
                        ->get('quatation_tbl')->row();

        // if($order->is_different_shipping==1){
        //     $add = explode(',', $order->different_shipping_address);
        //     if($order->different_shipping_address!=NULL){
        //         $ShipTo = (object)[
        //             'Name'                  => $order->first_name.' '.$order->last_name,
        //             'AttentionName'         => $order->first_name.' '.$order->last_name,
        //             'Phone'                 =>  $order->phone,
        //             'AddressLine'           =>  trim($add[0]),
        //             'City'                  =>  trim($add[1]),
        //             'StateProvinceCode'     =>  trim($add[2]),
        //             'PostalCode'            =>  trim($add[3]),
        //             'CountryCode'           =>  trim($add[4]),
        //         ];
        //     } else {
        //        $this->session->set_flashdata('message', "<div class='alert alert-danger'>
        //         <p>Errors!</p>
        //         <p>Description : Different shipping address missing!</p>
        //         </div>");
        //         redirect("c_level/order_controller/shipment/".$order_id);
        //     }
        // } else {
        //$st = $this->db->select('shortcode')->where('state_name',@$order->state)->get('us_state_tbl')->row();


        $addd = explode(',', $shipping_address);

        if ($addd[0] != NULL && $addd[1] != NULL && $addd[2] != NULL && $addd[3] != NULL && $addd[4] != NULL) {
            //if($order->address!=NULL && $order->city!=NULL && $order->state!=NULL && $order->zip_code!=NULL ){
            // $ShipTo = (object)[
            //         'Name'                  =>  $order->first_name.' '.$order->last_name,
            //         'AttentionName'         =>  $order->first_name.' '.$order->last_name,
            //         'Phone'                 =>  $order->phone,
            //         'AddressLine'           =>  $order->address,
            //         'City'                  =>  $order->city,
            //         'StateProvinceCode'     =>  $order->state,
            //         'PostalCode'            =>  $order->zip_code,
            //         'CountryCode'           =>  $order->country_code,
            //     ];

            $phone = str_replace('+1', '', $order->phone);

            $ShipTo = (object) [
                        'Name' => $order->first_name . ' ' . $order->last_name,
                        'AttentionName' => $order->first_name . ' ' . $order->last_name,
                        'Phone' => $phone,
                        'AddressLine' => $addd[0],
                        'City' => $addd[1],
                        'StateProvinceCode' => $addd[2],
                        'PostalCode' => $addd[3],
                        'CountryCode' => $addd[4]
            ];
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : Wrong address format!</p>
                </div>");
            redirect("c_level/order_controller/shipment/" . $order_id);
        }

        //}


        if (strtolower($method->method_name) == 'ups') {

            $shiperInfo = $this->get_shiper_info($method_id, $service_type);

            $this->ups_shipping($ShipTo, $length, $weight, $shiperInfo, $order_id, $method_id);
        }

        if (strtolower($method->method_name) == 'delivery in person' || strtolower($method->method_name) == 'customer pickup') {

            $this->delivery_in_person($ShipTo, $order_id, $method_id);
        }
    }

    public function ups_shipping($ShipTo = null, $length = NULL, $weight = NULL, $shiperInfo = NULL, $order_id = NULL, $method_id = NULL) {


        $this->load->library('Upsshipping');

        /* Ship To Address */
        $this->upsshipping->addField('ShipTo_Name', $ShipTo->Name);

        $this->upsshipping->addField('ShipTo_AddressLine', array($ShipTo->AddressLine));

        $this->upsshipping->addField('ShipTo_City', $ShipTo->City);

        $this->upsshipping->addField('ShipTo_StateProvinceCode', $ShipTo->StateProvinceCode);

        $this->upsshipping->addField('ShipTo_PostalCode', $ShipTo->PostalCode);

        $this->upsshipping->addField('ShipTo_CountryCode', $ShipTo->CountryCode);

        $this->upsshipping->addField('ShipTo_Number', $ShipTo->Phone);

        $this->upsshipping->addField('length', $length);

        $this->upsshipping->addField('weight', $weight);

        list($response, $status) = $this->upsshipping->processShipAccept($shiperInfo);

        $ups_response = json_decode($response);

        if (isset($ups_response->ShipmentResponse) && $ups_response->ShipmentResponse->Response->ResponseStatus->Code == 1) {

            $track_number = $ups_response->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber;

            $total_charges = $ups_response->ShipmentResponse->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue;

            $graphic_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage;

            $html_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->HTMLImage;

            //graphic_image_path
            $data1g = "data:image/jpeg;base64," . $graphic_image;

            $img_name_g = $track_number . '_graphic_image.jpeg';

            $source_g = fopen($data1g, 'r');

            $graphic_image = fopen("assets/b_level/shipping_img/" . $img_name_g, "w");

            stream_copy_to_stream($source_g, $graphic_image);

            fclose($source_g);

            fclose($graphic_image);

            $graphic_image_path = 'assets/b_level/shipping_img/' . $img_name_g;

            // html image
            $data1 = "data:image/jpeg;base64," . $html_image;

            $img_name = uniqid() . $track_number . '_html_image.jpeg';

            $source = fopen($data1, 'r');

            $html_image = fopen("assets/b_level/shipping_img/" . $img_name, "w");

            stream_copy_to_stream($source, $html_image);

            fclose($source);

            fclose($html_image);

            $html_image_path = 'assets/b_level/shipping_img/' . $img_name;

            $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'grand_total' => @$rowData->grand_total + $total_charges,
                'due' => @$rowData->due + $total_charges,
                'shipping_charges' => $total_charges,
            );
            //update to quatation table with pyament due 
            $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);

            $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

            $upsResponseData = array(
                'order_id' => $order_id,
                'track_number' => $track_number,
                'shipping_charges' => $total_charges,
                'graphic_image' => $graphic_image_path,
                'html_image' => $html_image_path,
                'method_id' => $method_id,
                'service_type' => @$shiperInfo['service_type'],
                'shipping_addres' => @$shipping_addres
            );

            $this->db->insert('shipment_data', $upsResponseData);
            redirect('c_level/order_controller/order_view/' . $order_id);
        } elseif ($ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode != NULL) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors code : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code . "</p>
                <p>Description : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description . "</p>
            </div>");
            redirect("c_level/order_controller/shipment/" . $order_id);
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : This is internal error!</p>
            </div>");
            redirect("c_level/order_controller/shipment/" . $order_id);
        }
    }

    public function order_view($order_id) {

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();
        $data['cmp_info'] = $this->settings->company_profile();

        $data['card_info'] = $this->db->where('created_by', $this->user_id)->where('is_active', 1)->get('c_card_info')->row();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/order_view', $data);
        $this->load->view('c_level/footer');
    }

    public function delete_order($order_id) {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "order information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('order_id', $order_id)->delete('quatation_tbl');
        $this->db->where('order_id', $order_id)->delete('qutation_details');
        $this->db->where('order_id', $order_id)->delete('quatation_attributes');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Delete successfully!</div>");
        redirect("manage-order");
    }

    public function get_shiper_info($method_id, $service_type) {

        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();

        $company_profile = $this->settings->company_profile();


        $arrayName = array(
            'username' => $method->username,
            'password' => $method->password,
            'account_id' => $method->account_id,
            'access_token' => $method->access_token,
            'name' => $company_profile[0]->company_name,
            'attentionname' => 'Attention Name',
            'description' => 'This is test deiscription',
            'address' => $company_profile[0]->address,
            'city' => $company_profile[0]->city,
            'state' => $company_profile[0]->state,
            'zip_code' => $company_profile[0]->zip_code,
            'country_code' => $company_profile[0]->country_code,
            'phone' => $company_profile[0]->phone,
            'method_id' => $method->id,
            'mode' => $method->mode,
            'service_type' => $service_type,
            'pickup_method' => $method->pickup_method
        );

        return $arrayName;
    }

    public function delivery_in_person($ShipTo, $order_id, $method_id) {

        $delivery_date = $this->input->post('delivery_date');
        $delivery_charge = $this->input->post('delivery_charge');
        $comment = $this->input->post('comment');

        $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('quatation_tbl')->row();
        // quatation table update
        $orderData = array(
            'grand_total' => @$rowData->grand_total + @$delivery_charge,
            'due' => @$rowData->due + @$delivery_charge,
            'shipping_charges' => @$delivery_charge,
        );

        //update to quatation table with pyament due 
        $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);

        $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

        $deliveryData = array(
            'order_id' => $order_id,
            'track_number' => '',
            'shipping_charges' => $delivery_charge,
            'graphic_image' => '',
            'html_image' => '',
            'method_id' => $method_id,
            'comment' => $comment,
            'delivery_date' => $delivery_date,
            'service_type' => '',
            'shipping_addres' => $shipping_addres
        );

        $this->db->insert('shipment_data', $deliveryData);
        redirect('c_level/order_controller/order_view/' . $order_id);
    }

//    =========== its for set_order_stage ==============
    public function set_order_stage($stage, $order_id) {
        if (!empty($stage) && !empty($order_id)) {
            $this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('quatation_tbl');
            echo 1;
        } else {
            echo 2;
        }
    }

// ============== its for cancel_comment ==============
    public function cancel_comment() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "cancel";
        $remarks = "order cancel comment";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $order_id = $this->input->post('order_id');
        $stage_id = $this->input->post('stage_id');
        $cancel_comment = $this->input->post('cancel_comment');
        $cancel_data = array(
            'cancel_comment' => $cancel_comment,
            'order_stage' => $stage_id,
        );
        $this->db->where('order_id', $order_id);
        $this->db->update('quatation_tbl', $cancel_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order cancel successfully!</div>");
        redirect("manage-order");
    }

}
