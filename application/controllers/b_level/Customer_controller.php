<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->load->model('b_level/Customer_model');
        $this->load->model('b_level/User_model');
        $this->load->model('b_level/settings');
        $this->load->model('b_level/order_model');
    }

    public function index() {
        
    }

//     =========== its for add_customer =============
    public function add_customer() {
        $this->permission->check_label('add')->create()->redirect();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/customer_new');
        $this->load->view('b_level/footer');
    }

//======== its for exists get_check_unique_email  ===========
    public function get_check_unique_email() {
        $email = $this->input->post('email');
        $query = $this->db->select('*')->from('customer_info')->where('email', $email)
                        ->get()->row();
//        echo '<pre>';        print_r($query);die();
        if (!empty($query)) {
            echo $query->email;
        } else {
            echo 0;
        }
    }

//============ its for when a c level customer create then auto save all menu c menusetup table by this customer id =============
    public function c_level_menu_transfer($user_insert_id) {

        $this->db->where('level_id', $user_insert_id)->delete('c_menusetup_tbl');

        $get_all_c_default_menu = $this->db->get('menusetup_tbl')->result();

//        $menu_title = $get_all_c_default_menu
        foreach ($get_all_c_default_menu as $single) {
            $transfer_menu = array(
                'menu_id' => $single->id,
                'menu_title' => $single->menu_title,
                'korean_name' => $single->korean_name,
                'page_url' => $single->page_url,
                'module' => $single->module,
                'ordering' => $single->ordering,
                'parent_menu' => $single->parent_menu,
                'menu_type' => $single->menu_type,
                'is_report' => $single->is_report,
                'icon' => $single->icon,
                'status' => $single->status,
                'level_id' => $user_insert_id,
                'created_by' => $single->created_by,
                'create_date' => $single->create_date,
            );
//            echo '<pre>';            print_r($transfer_menu);echo '</pre>';
            $this->db->insert('c_menusetup_tbl', $transfer_menu);
        }
//        dd($get_all_c_default_menu);
        return true;
    }

    public function transfar() {

        $customer = $this->order_model->get_customer();

        foreach ($customer as $key => $value) {
            //$this->c_level_acc_coa($value->customer_user_id);
            $this->c_level_menu_transfer($value->customer_user_id);
        }
    }

//============ its for when a c level customer create then auto save all menu c menusetup table by this customer id =============
    public function c_level_acc_coa($user_insert_id) {

        $get_all_acc_coa = $this->db->get('acc_coa_default')->result();
//        $menu_title = $get_all_acc_coa
        foreach ($get_all_acc_coa as $single) {

            $transfer_acc_coa = array(
                'HeadCode' => $single->HeadCode,
                'HeadName' => $single->HeadName,
                'PHeadName' => $single->PHeadName,
                'HeadLevel' => $single->HeadLevel,
                'IsActive' => $single->IsActive,
                'IsTransaction' => $single->IsTransaction,
                'IsGL' => $single->IsGL,
                'HeadType' => $single->HeadType,
                'IsBudget' => $single->IsBudget,
                'IsDepreciation' => $single->IsDepreciation,
                'DepreciationRate' => $single->DepreciationRate,
                'level_id' => $user_insert_id,
                'CreateBy' => $single->CreateBy,
                'CreateDate' => $single->CreateDate,
                'UpdateBy' => $single->UpdateBy,
                'UpdateDate' => $single->UpdateDate
            );
//            echo '<pre>';            print_r($transfer_acc_coa);echo '</pre>';
            $this->db->insert('acc_coa', $transfer_acc_coa);
        }
//        dd($get_all_c_default_menu);
        return true;
    }

//=============== its for customer_save =============
    public function customer_save() {
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $phone_type = $this->input->post('phone_type');
        $company = $this->input->post('company');
        $customer_type = $this->input->post('customer_type');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $file_upload = $this->input->post('file_upload');
//        print_r($_FILES['file_upload']);        die();
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        dd($phone);
        if ($customer_type == 'business') {
//            =========== its for save customer data in the users table ===============
            $customer_userinfo_data = array(
                'created_by' => $this->user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $company,
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'phone' => $phone[0],
                'email' => $email,
                'language' => 'English',
                'user_type' => 'c',
                'create_date' => $created_date,
            );
            $this->db->insert('user_info', $customer_userinfo_data);
            $user_insert_id = $this->db->insert_id();
//            =========== its for save customer data in the users table and login info ===============
            $customer_loginfo_data = array(
                'user_id' => $user_insert_id,
                'email' => $username,
                'password' => md5($password),
                'user_type' => 'c',
                'is_admin' => '1',
            );
            $this->db->insert('log_info', $customer_loginfo_data);
//            ============== its for company profile ===========
            $company_profile = array(
                'user_id' => $user_insert_id,
                'company_name' => $company,
                'email' => $email,
                'phone' => $phone[0],
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'created_by' => $this->user_id,
                'created_at' => $created_date,
            );
            $this->db->insert('company_profile', $company_profile);
        }
//        ============ its for accounts coa table ===============
        $coa = $this->Customer_model->headcode();
        if ($coa->HeadCode != NULL) {
            $headcode = $coa->HeadCode + 1;
        } else {
            $headcode = "102030101";
        }

        $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                ->order_by('customer_id', 'desc')
                ->get()
                ->row();
//        dd($lastid);
        $sl = $lastid->customer_no;
        if (empty($sl)) {
            $sl = "CUS-0001";
        } else {
            $sl = $sl;
        }
        $supno = explode('-', $sl);
        $nextno = $supno[1] + 1;
        $si_length = strlen((int) $nextno);

        $str = '0000';
        $cutstr = substr($str, $si_length);
        $sino = "CUS" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;
//        $customer_name = $this->input->post('customer_name');
        $customer_no = $sino . '-' . $first_name . " " . $last_name;
//        ================= close =======================
//        =============== its for company name with customer id start =============
        $last_c_id = $lastid->customer_id;
        $cn = strtoupper(substr($company, 0, 3)) . "-";
        if (empty($last_c_id)) {
            $last_c_id = $cn . "1";
        } else {
            $last_c_id = $last_c_id;
        }
        $cust_nextid = $last_c_id + 1;
        $company_custid = $cn . $cust_nextid;
//        =============== its for company name with customer id close=============
//        ========== its for logo =============
// configure for upload 
        $config = array(
            'upload_path' => "./assets/b_level/uploads/customers/",
            'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
            'overwrite' => TRUE,
//            'file_name' => "BMSLINK" . time(),
            'file_size' => '2048',
            'encrypt_name' => TRUE,
        );
//        $image_name = '';
//        $image_data = array();
//        $this->load->library('upload', $config);
//        $this->upload->initialize($config);
//        echo '<pre>';        print_r($config);die();
//        if ($this->upload->do_upload('file_upload_1')) {
////            echo "DS";die();
//            $image_data = $this->upload->data();
////                dd($image_data);
//            $image_name = $image_data['file_name'];
//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $image_data['full_path']; //get original image
//            $config['maintain_ratio'] = TRUE;
//            $config['height'] = '*';
//            $config['width'] = '*';
////                $config['quality'] = 50;
//            $this->load->library('image_lib', $config);
//            $this->image_lib->clear();
//            $this->image_lib->initialize($config);
//            if (!$this->image_lib->resize()) {
//                echo $this->image_lib->display_errors();
//            }
//        } else {
//            $image_name = '';
//        }
//        //            ========= its for file upload 2 =========
//        if ($this->upload->do_upload('file_upload_2')) {
//            $image_dataTwo = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_2 = $image_dataTwo['file_name'];
//        } else {
//            $file_upload_2 = '';
//        }
//        //            ========= its for file upload 3 =========
//        if ($this->upload->do_upload('file_upload_3')) {
//            $image_dataThree = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_3 = $image_dataThree['file_name'];
//        } else {
//            $file_upload_3 = '';
//        }
//        //            ========= its for file upload 2 =========
//        if ($this->upload->do_upload('file_upload_4')) {
//            $image_dataFour = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_4 = $image_dataFour['file_name'];
//        } else {
//            $file_upload_4 = '';
//        }
//        dd($config);
        $customer_data = array(
            'customer_user_id' => $user_insert_id,
            'customer_no' => $customer_no,
            'company_customer_id' => $company_custid,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone[0],
//            'phone_2' => $phone_2,
//            'phone_3' => $phone_3,
            'company' => $company,
            'customer_type' => $customer_type,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip_code,
            'country_code' => $country_code,
            'street_no' => $street_no,
            'side_mark' => $side_mark,
            'level_id' => $level_id,
            'created_by' => $this->user_id,
            'create_date' => $created_date,
        );
//        dd($customer_data);
        $this->db->insert('customer_info', $customer_data);
        $customer_inserted_id = $this->db->insert_id();
        //============ its for when a c level customer create then auto save all menu c menusetup table by this customer id =============
        $this->c_level_menu_transfer($user_insert_id);

        //transfar acc_coa_table data
        $this->c_level_acc_coa($user_insert_id);
//        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadCode' => $headcode,
            'HeadName' => $customer_no,
            'PHeadName' => 'Customer Receivable',
            'HeadLevel' => '4',
            'IsActive' => '1',
            'IsTransaction' => '1',
            'IsGL' => '0',
            'HeadType' => 'A',
            'IsBudget' => '0',
            'IsDepreciation' => '0',
            'DepreciationRate' => '0',
            'CreateBy' => $this->user_id,
            'CreateDate' => $created_date,
        );
//        dd($customer_coa);
        $this->db->insert('b_acc_coa', $customer_coa);
//        ======= close ==============
//            ================ its for multiple certificate info save ===========


        if ($_FILES["file_upload"]['name'][0] != '') {

            $file_uploadCount = count($_FILES['file_upload']['name']);

            for ($i = 0; $i < $file_uploadCount; $i++) {

                $_FILES['file_uploader']['name'] = $_FILES['file_upload']['name'][$i];
                $_FILES['file_uploader']['type'] = $_FILES['file_upload']['type'][$i];
                $_FILES['file_uploader']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
                $_FILES['file_uploader']['error'] = $_FILES['file_upload']['error'][$i];
                $_FILES['file_uploader']['size'] = $_FILES['file_upload']['size'][$i];


                $image_data = array();

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file_uploader')) {

                    $image_data = $this->upload->data();

                    $customer_file_name = $image_data['file_name'];

                    $customerFileinfo[$i]['customer_id'] = $customer_inserted_id;
                    $customerFileinfo[$i]['customer_user_id'] = $user_insert_id;
                    $customerFileinfo[$i]['file_upload'] = $customer_file_name;
//                    $customerFileinfo[$i]['degree_name'] = $degrees_name[$i];
                }
            }

            $this->Customer_model->save_customer_file($customerFileinfo);
        }


//        =============== its for customer phone type info ==============
        for ($i = 0; $i < count($phone); $i++) {
            $phone_types_number = array(
                'phone' => $phone[$i],
                'phone_type' => $phone_type[$i],
                'customer_id' => $customer_inserted_id,
                'customer_user_id' => $user_insert_id,
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
        }
        //        =========== its for customer user info send by email ============     
        if ($email) {
            $data['get_mail_config'] = $this->settings->get_mail_config();
            $this->Customer_model->sendLink($user_insert_id, $email, $data, $username, $password);
        }
//        ============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );

        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info save successfully!</div>");
        redirect('b-customer-list');
    }

//    ==========its for own user customer count =========
    public function own_user_customer_count() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('customer_info');
        return $num_rows;
    }

//=========== its for b_customer_list =============
    public function b_customer_list()
    {
        $this->permission->check_label('manage')->create()->redirect();

        $config["base_url"] = base_url('b_level/Customer_controller/b_customer_list');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_user_customer_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["customer_list"] = $this->Customer_model->customer_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

       /* print_r($data["customer_list"]);die();*/

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/customer_list', $data);
        $this->load->view('b_level/footer');
    }

//============== its for show_customer_record =============

    public function show_b_customer_record($id) {
        $data['show_b_customer_record'] = $this->Customer_model->show_b_customer_record($id);
        $data['get_users'] = $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name")->where('user_type', 'b')->where('created_by!=', 0)->get('user_info')->result();

        $data['remarks'] = $this->db->select("appointment_calendar.*, CONCAT_WS(' ', user_info.first_name, user_info.last_name) as creater")
                        ->join('user_info', 'user_info.id=appointment_calendar.create_by', 'left')
                        ->where('appointment_calendar.customer_id', $id)
                        ->order_by('appointment_calendar.appointment_id', 'desc')->limit(2)
                        ->get('appointment_calendar')->result();

        $this->load->view('b_level/customers/show_b_customer_record', $data);
    }

//============ its for appointment_setup method ================
    public function appointment_setup() {
        $action_page = $this->uri->segment(3);
        $status = $this->input->post('status');
        $action_done = "insert";
        $acc_remarks = "Appointment information save";
        $created_date = date('Y-m-d');

        $customer_id = $this->input->post('customer_id');

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $appointment_date = $this->input->post('appointment_date');
        $appointment_time = $this->input->post('appointment_time');
        $b_level_staff_id = $this->input->post('b_level_staff_id');
        $remarks = $this->input->post('remarks');

        $appointment_data = array(
            'customer_id' => $customer_id,
            'c_level_id' => $level_id,
            'appointment_date' => $appointment_date,
            'appointment_time' => $appointment_time,
            'c_level_staff_id' => $b_level_staff_id,
            'remarks' => $remarks,
            'create_by' => $this->user_id,
            'create_date' => $created_date,
        );

        $this->db->insert('appointment_calendar', $appointment_data);
        $this->db->set('now_status', $status)->where('customer_id', $customer_id)->update('customer_info');

        //============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

//        ============== close access log info =================
//        echo '<pre>';print_r($appointment_data); die();
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Appointment setup successfully!</div>");
        redirect('b-customer-list');
    }

    // get customer details
    public function customer_view($id) {

        $data['customer_view'] = $this->Customer_model->customer_view($id);

        $data['get_customer_phones'] = $this->Customer_model->get_customer_phones($id);

        $data['orderd'] = $this->Customer_model->get_orderd_by_customer_id($id);


        $data['remarks'] = $this->Customer_model->get_remarks_by_customer_id($id);
        $data['company_profile'] = $this->settings->company_profile();
        $data['currency'] = $data['company_profile'][0]->currency;
//        echo '<pre>';        print_r($data['company_profile']);die();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/customer_view', $data);
        $this->load->view('b_level/footer');
    }

//    ============== its for send_customer_comment ============
    public function send_customer_comment() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "send customer comment";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $customer_id = $this->input->post('customer_id');
        $customer_user_id = $this->input->post('customer_user_id');
        $comment = $this->input->post('comment');
        $comments_data = array(
            'comment_from' => $this->user_id,
            'comment_to' => $customer_user_id,
            'comments' => $comment,
            'status' => 'IN',
        );
        $this->db->insert('customer_commet_tbl', $comments_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Comment send successfully!</div>");
        redirect('b-customer-view/' . $customer_id);
    }

    //-----------------------------
//     =========== its for customer_view =============
    public function customer_edit($id) {
        $data['customer_edit'] = $this->Customer_model->customer_edit($id);


//        dd($data['customer_edit']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/customer_edit', $data);
        $this->load->view('b_level/footer');
    }

//    ============ its for appointment_setup method ================
    public function customer_update($id) {


        $get_customer_info = $this->db->select('customer_no')->where('customer_id', $id)->get('customer_info')->result();
        $get_customer_no = $get_customer_info[0]->customer_no;
        $get_customer_coa_info = $this->db->select('HeadCode')->where('HeadName', $get_customer_no)->get('b_acc_coa')->result();
        //$get_customer_headCode = $get_customer_coa_info[0]->HeadCode;
        $action_page = $this->uri->segment(3);

        $action_done = "update";
        $remarks = "Customer information update";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $cust_no = $this->input->post('customer_no');
        $customer_no = $cust_no . "-" . $first_name . " " . $last_name;
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $phone_type = $this->input->post('phone_type');
//        echo '<pre>';        print_r($phone);        echo '</pre>';        dd($phone_type);
//        echo "1st ".$phone." 2nd ".$phone_2. " 3rd ". $phone_3. "4th ".$phone_type_1." 5th ".$phone_type_2. " 6th ". $phone_type_3; die();
        $company = $this->input->post('company');
        $customer_type = $this->input->post('customer_type');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $file_upload = $this->input->post('file_upload');
//        print_r($_FILES['file_upload']);        die();
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        if ($customer_type == 'business') {
//            =========== its for save customer data in the users table ===============
            $user_wise_customer_info = $this->db->select('customer_user_id')->where('customer_id', $id)->get('customer_info')->result();

            //print_r($user_wise_customer_info); exit;

            $customer_user_id = $user_wise_customer_info[0]->customer_user_id;

            $user_wise_log_info = $this->db->select('password')->where('user_id', $customer_user_id)->get('log_info')->result();
            $customer_user_password = @$user_wise_log_info[0]->password;
//            dd($user_wise_log_info);
            $customer_userinfo_data = array(
                'updated_by' => $this->user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $company,
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'phone' => $phone[0],
                'email' => $email,
                'user_type' => 'c',
                'update_date' => $created_date,
            );
            $this->db->where('id', $customer_user_id);
            $this->db->update('user_info', $customer_userinfo_data);
//            =========== its for save customer data in the users table ===============
            if (!empty($password)) {
                $password = md5($password);
            } else {
                $password = @$customer_user_password;
            }
//            echo $password;die();
            $customer_loginfo_data = array(
                'email' => $username,
                'password' => @$password,
                'user_type' => 'c',
                'is_admin' => '1',
            );
            $this->db->where('user_id', $customer_user_id);
            $this->db->update('log_info', $customer_loginfo_data);
            //            ============== its for company profile ===========
            $company_profile = array(
//                'user_id' => $user_insert_id,
                'company_name' => $company,
                'email' => $email,
                'phone' => $phone[0],
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'created_by' => $this->user_id,
                'created_at' => $created_date,
            );
            $this->db->where('user_id', $customer_user_id);
            $this->db->update('company_profile', $company_profile);
        }
//        ========== its for file upload =============
// configure for upload 
        $config = array(
            'upload_path' => "./assets/b_level/uploads/customers/",
            'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
            'overwrite' => TRUE,
//            'file_name' => "BMSLINK" . time(),
            'file_size' => '2048',
        );
//        $image_data = array();
//        $this->load->library('upload', $config);
//        $this->upload->initialize($config);
////        echo '<pre>';        print_r($config);die();
//        if ($this->upload->do_upload('file_upload')) {
////============= its for file unlink from folder =============
//            $customer_picture_unlink = $this->db->select('*')->from('customer_info')->where('customer_id', $id)->get()->row();
////        dd($customer_picture_unlink);
//            if ($customer_picture_unlink->file_upload) {
//                $img_path = FCPATH . 'assets/b_level/uploads/customers/' . $customer_picture_unlink->file_upload;
//                unlink($img_path);
//            }
////        ===== close ===========
//            $image_data = $this->upload->data();
////                dd($image_data);
//            $image_name = $image_data['file_name'];
//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $image_data['full_path']; //get original image
//            $config['maintain_ratio'] = TRUE;
//            $config['height'] = '*';
//            $config['width'] = '*';
////                $config['quality'] = 50;
//            $this->load->library('image_lib', $config);
//            $this->image_lib->clear();
//            $this->image_lib->initialize($config);
//            if (!$this->image_lib->resize()) {
//                echo $this->image_lib->display_errors();
//            }
//        } else {
//            $image_name = $this->input->post('file_upload_hdn');
//        }
//
//        //            ========= its for file upload 2 =========
//        if ($this->upload->do_upload('file_upload_2')) {
//            $customer_upload_2_unlink = $this->db->select('*')->from('customer_info')->where('customer_id', $id)->get()->row();
//            if ($customer_upload_2_unlink->file_upload_2) {
//                $img_path = FCPATH . 'assets/b_level/uploads/customers/' . $customer_upload_2_unlink->file_upload_2;
//                unlink($img_path);
//            }
//            $image_dataTwo = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_2 = $image_dataTwo['file_name'];
//        } else {
//            $file_upload_2 = $this->input->post('file_upload_2_hdn');
//        }
//        //            ========= its for file upload 3 =========
//        if ($this->upload->do_upload('file_upload_3')) {
//            $customer_upload_3_unlink = $this->db->select('*')->from('customer_info')->where('customer_id', $id)->get()->row();
//            if ($customer_upload_3_unlink->file_upload_3) {
//                $img_path = FCPATH . 'assets/b_level/uploads/customers/' . $customer_upload_3_unlink->file_upload_3;
//                unlink($img_path);
//            }
//            $image_dataThree = $this->upload->data();
//            $file_upload_3 = $image_dataThree['file_name'];
//        } else {
//            $file_upload_3 = $this->input->post('file_upload_3_hdn');
//        }
//        //            ========= its for file upload 2 =========
//        if ($this->upload->do_upload('file_upload_4')) {
//            $customer_upload_4_unlink = $this->db->select('*')->from('customer_info')->where('customer_id', $id)->get()->row();
//            if ($customer_upload_4_unlink->file_upload_4) {
//                $img_path = FCPATH . 'assets/b_level/uploads/customers/' . $customer_upload_4_unlink->file_upload_4;
//                unlink($img_path);
//            }
//            $image_dataFour = $this->upload->data();
//            $file_upload_4 = $image_dataFour['file_name'];
//        } else {
//            $file_upload_4 = $this->input->post('file_upload_4_hdn');
//        }
//        dd($config);

        $cn = strtoupper(substr($company, 0, 3)) . "-";
        $company_custid = $cn . $id;
        $customer_data = array(
            'customer_id' => $id,
            'customer_no' => $customer_no,
            'company_customer_id' => $company_custid,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone[0],
            'company' => $company,
            'customer_type' => $customer_type,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip_code,
            'country_code' => $country_code,
            'street_no' => $street_no,
            'side_mark' => $side_mark,
            'level_id' => $level_id,
            'created_by' => $this->user_id,
            'create_date' => $created_date,
        );
//        dd($customer_data);
        $this->db->where('customer_id', $id);
        $this->db->update('customer_info', $customer_data);
        //        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadName' => $customer_no,
            'UpdateBy' => $this->user_id,
            'UpdateDate' => $created_date,
        );
//        dd($customer_coa);
        //$this->db->where('HeadCode', $get_customer_headCode);
        //$this->db->update('b_acc_coa', $customer_coa);
//        ======= close ==============
//            ================ its for multiple certificate info save ===========
        if ($_FILES["file_upload"]['name'][0] != '') {
            $file_uploadCount = count($_FILES['file_upload']['name']);
            for ($i = 0; $i < $file_uploadCount; $i++) {
                $_FILES['file_uploader']['name'] = $_FILES['file_upload']['name'][$i];
                $_FILES['file_uploader']['type'] = $_FILES['file_upload']['type'][$i];
                $_FILES['file_uploader']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
                $_FILES['file_uploader']['error'] = $_FILES['file_upload']['error'][$i];
                $_FILES['file_uploader']['size'] = $_FILES['file_upload']['size'][$i];

                // configure for upload 
//                $config = array(
//                    'upload_path' => "./assets/uploads/doctor/",
//                    'allowed_types' => "gif|jpg|png|jpeg|pdf",
//                    'overwrite' => TRUE,
////                    'file_name' => "BDTASK" . time(),
//                    'max_size' => '0',
//                );
                $image_data = array();

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file_uploader')) {
                    $image_data = $this->upload->data();
//                print_r($image_data); die();
                    $customer_file_name = $image_data['file_name'];



                    $customerFileinfo[$i]['customer_id'] = $id;
                    $customerFileinfo[$i]['customer_user_id'] = $customer_user_id;
                    $customerFileinfo[$i]['file_upload'] = $customer_file_name;
//                    $customerFileinfo[$i]['degree_name'] = $degrees_name[$i];
                }
            }
            $this->Customer_model->save_customer_file($customerFileinfo);
        }
//        =============== its for customer phone type info ==============
        $customer_id_check_phone_types = $this->db->where('customer_id', $id)->from('customer_phone_type_tbl')->get()->result();

        $customer_phone_types_deleted_sql = $this->db->where('customer_id', $id)->delete('customer_phone_type_tbl');

//        =============== its for customer phone type info ==============
        for ($i = 0; $i < count($phone); $i++) {
            $phone_types_number = array(
                'phone' => $phone[$i],
                'phone_type' => $phone_type[$i],
                'customer_id' => $id,
                'customer_user_id' => $customer_user_id,
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
        }

//        ============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => @$action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        //        =========== its for customer user info send by email ============     
        if ($email) {
            $password = $this->input->post('password');
            $data['get_mail_config'] = $this->settings->get_mail_config();
            $this->Customer_model->sendLink($customer_user_id, $email, $data, $username, $password);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info updated successfully!</div>");
        redirect('b-customer-list');
    }

//    ============== its for b_customer_file_delete ============
    public function b_customer_file_delete($file_upload_id, $customer_id) {
        $customer_upload_unlink = $this->db->select('*')->from('customer_file_tbl')->where('id', $file_upload_id)->get()->row();
        if ($customer_upload_unlink->id == $file_upload_id) {
            if ($customer_upload_unlink->file_upload) {
                $img_path = FCPATH . 'assets/b_level/uploads/customers/' . $customer_upload_unlink->file_upload;
                unlink($img_path);
            }
            $this->db->where('id', $file_upload_id)->delete('customer_file_tbl');
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "customer file deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>File deleted successfully!</div>");
        redirect('b-customer-edit/' . $customer_id);
    }

//     =========== its for customer_import =============
    public function customer_import() {
        $this->permission->check_label('bulk_upload')->create()->redirect();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/customer_import');
        $this->load->view('b_level/footer');
    }

//================= its for customer_csv_upload =============
    public function customer_csv_upload() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "customer csv information imported";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();

//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['first_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['last_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['email'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                    $insert_csv['phone'] = (!empty($csv_line[3]) ? $csv_line[3] : null);
                    $insert_csv['fax'] = (!empty($csv_line[4]) ? $csv_line[4] : null);
                    $insert_csv['mobile'] = (!empty($csv_line[5]) ? $csv_line[5] : null);
                    $insert_csv['company'] = (!empty($csv_line[6]) ? $csv_line[6] : null);
                    $insert_csv['customer_type'] = 'business'; //(!empty($csv_line[5]) ? $csv_line[5] : null);
                    $insert_csv['address'] = (!empty($csv_line[7]) ? $csv_line[7] : null);
                    $insert_csv['city'] = (!empty($csv_line[8]) ? $csv_line[8] : null);
                    $insert_csv['state'] = (!empty($csv_line[9]) ? $csv_line[9] : null);
                    $insert_csv['zip_code'] = (!empty($csv_line[10]) ? $csv_line[10] : null);
                    $insert_csv['country_code'] = (!empty($csv_line[11]) ? $csv_line[11] : null);
                    $insert_csv['street_no'] = (!empty($csv_line[12]) ? $csv_line[12] : null);
//                    $insert_csv['side_mark'] = (!empty($csv_line[11]) ? $csv_line[11] : null);
                    $insert_csv['username'] = $insert_csv['email']; //(!empty($csv_line[13]) ? $csv_line[13] : null);
                    $insert_csv['password'] = (!empty($csv_line[13]) ? $csv_line[13] : null);
//                    $insert_csv['created_by'] = (!empty($csv_line[14]) ? $csv_line[14] : null);
                }

                if ($count > 0) {
//                    echo $insert_csv['address'];

                    $address_explode = explode(",", $insert_csv['address']);
                    $address = $address_explode[0];
                    $street_no = explode(' ', $insert_csv['address']);
                    $street_no = $street_no[0];
//                    echo '<pre>';   print_r($address_explode);   echo $address; echo "<br>";   echo $street_no;    die();

                    $phone = $this->phone_fax_mobile_no_generate($insert_csv['phone']);
                    $phone_2 = $this->phone_fax_mobile_no_generate($insert_csv['fax']);
                    $phone_3 = $this->phone_fax_mobile_no_generate($insert_csv['mobile']);

                    if ($insert_csv['username']) {
                        $username = $insert_csv['username'];
                    } else {
                        $username = $insert_csv['email'];
                    }
                    $data = array(
                        'email' => $insert_csv['email'],
                    );
//                echo $data['email'];
                    $result = $this->db->select('*')
                            ->from('customer_info')
                            ->where('email', $data['email'])
                            ->get()
                            ->num_rows();
//                echo '<pre>';                print_r($result);die();
//        ============ its for accounts coa table ===============
                    $coa = $this->Customer_model->headcode();
                    if ($coa->HeadCode != NULL) {
                        $headcode = $coa->HeadCode + 1;
                    } else {
                        $headcode = "102030101";
                    }
                    $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                            ->order_by('customer_id', 'desc')
                            ->get()
                            ->row();
//        dd($lastid);
                    $sl = $lastid->customer_no;
                    if (empty($sl)) {
                        $sl = "CUS-0001";
                    } else {
                        $sl = $sl;
                    }
                    $supno = explode('-', $sl);
                    $nextno = $supno[1] + 1;
                    $si_length = strlen((int) $nextno);

                    $str = '0000';
                    $cutstr = substr($str, $si_length);
                    $sino = "CUS" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;

                    $customer_no = $sino . '-' . $insert_csv['first_name'] . " " . $insert_csv['last_name'];
//        ================= close =======================
//        =============== its for company name with customer id start =============
                    $last_c_id = $lastid->customer_id;
                    $cn = strtoupper(substr($insert_csv['company'], 0, 3)) . "-";
                    if (empty($last_c_id)) {
                        $last_c_id = $cn . "1";
                    } else {
                        $last_c_id = $last_c_id;
                    }
                    $cust_nextid = $last_c_id + 1;
                    $company_custid = $cn . $cust_nextid;
//        =============== its for company name with customer id close=============

                    if ($result == 0 && !empty($data['email'])) {

                        if ($insert_csv['customer_type'] == 'business') {
//            =========== its for save customer data in the users table ===============
                            $customer_userinfo_data = array(
                                'created_by' => $this->user_id,
                                'first_name' => $insert_csv['first_name'],
                                'last_name' => $insert_csv['last_name'],
                                'company' => $insert_csv['company'],
                                'address' => $address, //$insert_csv['address'],
                                'city' => $insert_csv['city'],
                                'state' => $insert_csv['state'],
                                'zip_code' => $insert_csv['zip_code'],
                                'country_code' => $insert_csv['country_code'],
                                'phone' => $phone,
                                'email' => $insert_csv['email'],
                                'language' => 'English',
                                'user_type' => 'c',
                                'create_date' => date('Y-m-d'),
                            );
                            $this->db->insert('user_info', $customer_userinfo_data);
                            $user_insert_id = $this->db->insert_id();

                            $this->c_level_menu_transfer($user_insert_id);
                            //transfar acc_coa_table data
                            $this->c_level_acc_coa($user_insert_id);



//            =========== its for save customer data in the users table ===============
                            $customer_loginfo_data = array(
                                'user_id' => $user_insert_id,
                                'email' => $username,
                                'password' => md5($insert_csv['password']),
                                'user_type' => 'c',
                                'is_admin' => '1',
                            );
                            $this->db->insert('log_info', $customer_loginfo_data);

//            ============== its for company profile ===========
                            $company_profile = array(
                                'user_id' => @$user_insert_id,
                                'company_name' => $insert_csv['company'],
                                'email' => $insert_csv['email'],
                                'phone' => $phone,
                                'address' => $address,
                                'city' => $insert_csv['city'],
                                'state' => $insert_csv['state'],
                                'zip_code' => $insert_csv['zip_code'],
                                'country_code' => $insert_csv['country_code'],
                                'created_by' => $this->user_id,
                                'created_at' => date('Y-m-d'),
                            );
                            $this->db->insert('company_profile', $company_profile);
                        } else {
                            $user_insert_id = '';
                        }

                        $data = array(
                            'customer_user_id' => @$user_insert_id,
                            'customer_no' => $customer_no,
                            'company_customer_id' => $company_custid,
                            'first_name' => $insert_csv['first_name'],
                            'last_name' => $insert_csv['last_name'],
                            'email' => $insert_csv['email'],
                            'phone' => $phone,
//                            'phone_2' => $phone_2,
//                            'phone_3' => $phone_3,
                            'company' => $insert_csv['company'],
                            'customer_type' => $insert_csv['customer_type'],
                            'address' => $address, //$insert_csv['address'],
                            'city' => $insert_csv['city'],
                            'state' => $insert_csv['state'],
                            'zip_code' => $insert_csv['zip_code'],
                            'country_code' => $insert_csv['country_code'],
                            'street_no' => $insert_csv['street_no'],
//                            'side_mark' => $insert_csv['side_mark'],
//                            'file_upload' => '', //$insert_csv['file_upload'],
                            'level_id' => $level_id, //$insert_csv['level_id'],
                            'created_by' => $this->user_id,
                            'create_date' => date('Y-m-d'),
                        );

                        //        ======== its for customer COA data array ============
                        $customer_coa = array(
                            'HeadCode' => $headcode,
                            'HeadName' => $customer_no,
                            'PHeadName' => 'Customer Receivable',
                            'HeadLevel' => '4',
                            'IsActive' => '1',
                            'IsTransaction' => '1',
                            'IsGL' => '0',
                            'HeadType' => 'A',
                            'IsBudget' => '0',
                            'IsDepreciation' => '0',
                            'DepreciationRate' => '0',
                            'CreateBy' => $this->user_id,
                            'CreateDate' => date('Y-m-d'),
                        );
//        dd($customer_coa);
                        $this->db->insert('b_acc_coa', $customer_coa);
//        ======= close ==============                        
                        $this->db->insert('customer_info', $data);
                        $csv_customer_id = $this->db->insert_id();

//                        =========== its for multiple phone insert ==========
                        if ($insert_csv['phone'] || $insert_csv['fax'] || $insert_csv['mobile']) {
                            $phone_type_1 = $phone_type_2 = $phone_type_3 = '';
//                            if ($insert_csv['phone']) {
//                                $phone_type_1 = 'Phone';
//                            }
//                            if ($insert_csv['fax']) {
//                                $phone_type_2 = 'Fax';
//                            }
//                            if ($insert_csv['mobile']) {
//                                $phone_type_3 = 'Mobile';
//                            }
                            //        =============== its for customer phone type info ==============
                            if (!empty($phone)) {
                                $customer_phone_info = array(
                                    'customer_id' => $csv_customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Phone',
                                    'phone' => $phone,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_phone_info);
                            }
                            if (!empty($phone_2)) {
                                $customer_fax_info = array(
                                    'customer_id' => $csv_customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Fax',
                                    'phone' => $phone_2,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_fax_info);
                            }
                            if (!empty($phone_3)) {
                                $customer_mobile_info = array(
                                    'customer_id' => $csv_customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Mobile',
                                    'phone' => $phone_3,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_mobile_info);
                            }
                        }
//                    ============= close multiple phone type =============
                    } else {
                        $get_cid_by_email = $this->db->from('customer_info')->where('email', $insert_csv['email'])->get()->result();
                        $cn = strtoupper(substr($insert_csv['company'], 0, 3)) . "-";
                        $company_custid = $cn . $get_cid_by_email[0]->customer_id;
//                        echo '<pre>';                        print_r($company_custid);die();
                        $data = array(
                            'customer_user_id' => @$user_insert_id,
                            'customer_no' => $customer_no,
                            'company_customer_id' => $company_custid,
                            'first_name' => $insert_csv['first_name'],
                            'last_name' => $insert_csv['last_name'],
                            'email' => $insert_csv['email'],
                            'phone' => $phone,
//                            'phone_2' => $phone_2,
//                            'phone_3' => $phone_3,
                            'company' => $insert_csv['company'],
                            'customer_type' => $insert_csv['customer_type'],
                            'address' => $address, //$insert_csv['address'],
                            'city' => $insert_csv['city'],
                            'state' => $insert_csv['state'],
                            'zip_code' => $insert_csv['zip_code'],
                            'country_code' => $insert_csv['country_code'],
                            'street_no' => $street_no, //$insert_csv['street_no'],
//                            'side_mark' => $insert_csv['side_mark'],
//                            'file_upload' => '', //$insert_csv['file_upload'],
                            'level_id' => $level_id, //$insert_csv['level_id'],
                            'created_by' => $this->user_id,
                            'update_date' => date('Y-m-d'),
                        );
                        $this->db->where('email', $data['email']);
                        $this->db->update('customer_info', $data);

                        $customer_id_check_phone_types = $this->db->where('customer_id', $get_cid_by_email[0]->customer_id)->from('customer_phone_type_tbl')->get()->result();
                        if ($customer_id_check_phone_types) {
                            $customer_phone_types_deleted_sql = $this->db->where('customer_id', $get_cid_by_email[0]->customer_id)->delete('customer_phone_type_tbl');
                            if (!empty($phone)) {
                                $customer_phone_info = array(
                                    'customer_id' => $get_cid_by_email[0]->customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Phone',
                                    'phone' => $phone,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_phone_info);
                            }
                            if (!empty($phone_2)) {
                                $customer_fax_info = array(
                                    'customer_id' => $get_cid_by_email[0]->customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Fax',
                                    'phone' => $phone_2,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_fax_info);
                            }
                            if (!empty($phone_3)) {
                                $customer_mobile_info = array(
                                    'customer_id' => $get_cid_by_email[0]->customer_id,
                                    'customer_user_id' => '', //$user_insert_id,
                                    'phone_type' => 'Mobile',
                                    'phone' => $phone_3,
                                );
                                $this->db->insert('customer_phone_type_tbl', $customer_mobile_info);
                            }
                        }

//                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Already exists!</div>");
//                        redirect('customer-import');
                    }
                }
                $count++;
            }
        }
//        echo "Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer imported successfully!</div>");
        redirect('b-customer-list');
    }

//    ======== its for phone, fax and mobile no us format generate =============
    public function phone_fax_mobile_no_generate($number) {
        $first = substr($number, 0, 3);
        $second = substr($number, 3, 3);
        $third = substr($number, 6, 10);
        if ($number) {
            return "+1 (" . $first . ") " . $second . "-" . $third;
        } else {
            return '';
        }
    }

//     =========== its for sales_today =============
    public function sales_today() {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/sales_today');
        $this->load->view('b_level/footer');
    }

//     =========== its for sales_yesterday =============
    public function sales_yesterday() {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/sales_yesterday');
        $this->load->view('b_level/footer');
    }

//     =========== its for sales-lastweek =============
    public function sales_lastweek() {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/sales_lastweek');
        $this->load->view('b_level/footer');
    }

//    =========== its for b_level_customer_filter ===============
    public function b_level_customer_filter() {
        $data['company_name'] = $this->input->post('company_name');
//        $first_name = $this->input->post('first_name');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['get_customer_filter'] = $this->Customer_model->get_customer_filter($data['company_name'], $data['phone'], $data['email'], $data['address']);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/get_customer_filter');
        $this->load->view('b_level/footer');
    }

//    =========== its for show_customer_new_window_popup =========
    public function show_customer_new_window_popup() {

        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/customer_new_window_popup');
        $this->load->view('b_level/footer');
    }

//    =========== its for new_customer_modal =========
    public function new_customer_modal() {

//        $this->load->view('b_level/header');
////        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/new_customer_modal');
//        $this->load->view('b_level/footer');
    }

    public function delete_customer($customer_id = NULL, $customer_user_id = NULL) {
        $check_customer_order = $this->db->select('*')->from('b_level_quatation_tbl a')->where('a.customer_id', $customer_id)->get()->result();
        if ($check_customer_order) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Can't delete customer. Order already exists!!</div>");
            redirect('b-customer-list');
        } else {
            $this->db->where('customer_id', $customer_id)->delete('customer_info');
            $this->db->where('user_id', $customer_user_id)->delete('company_profile');
            $this->db->where('customer_id', $customer_id)->delete('customer_file_tbl');
            $this->db->where('customer_id', $customer_id)->delete('customer_phone_type_tbl');
            if ($customer_user_id != NULL) {
                $this->db->where('user_id', $customer_user_id)->delete('log_info');
                $this->db->where('id', $customer_user_id)->delete('user_info');
                $this->db->where('level_id', $customer_user_id)->delete('c_menusetup_tbl');
            }
        }

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "Customer information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer delete successfully!</div>");
        redirect('b-customer-list');
    }

//    ============ its for show_address_map ============ 
    public function show_address_map($id) {
        $customer_info = $this->db->select('*')->from('customer_info a')->where('customer_id', $id)->get()->result();
        $address = $customer_info[0]->address . " " . @$customer_info[0]->city . " " . @$customer_info[0]->state . " " . @$customer_info[0]->zip_code . " " . @$customer_info[0]->country_code;
        echo "<iframe width='600' height='450' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q=" . urlencode($address) . "&amp;output=embed'></iframe>";
    }

//    ==============it for b_level_customer_search ==============
    public function b_level_customer_search() {
        $keyword = $this->input->post('keyword');
        $data['customer_list'] = $this->Customer_model->get_customer_search_result($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/customers/customer_search', $data);
    }

//    ========= its for customer_export_csv ===========
    public function customer_export_csv() {

        $offset = $this->input->post('ofset');
        $limit = $this->input->post('limit');


        if(($offset>0) && ($limit>=1)){


            // file name 
            $filename = 'customer_' . date('Y-m-d') . '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");

            // get data 
            $usersData = $this->Customer_model->customer_csv_data($offset,$limit);

            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('customer_id', 'first_name', 'last_name', 'phone', 'email', 'company', 'address', 'city', 'state', 'zip_code', 'country_code');
            fputcsv($file, $header);
            foreach ($usersData as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            exit;


        }else {

            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>'Please check valid start and limit data!</div>");

            redirect('b-customer-list');

        }



    }

//    ======== its for customer_export_pdf ===========
    public function customer_export_pdf() {

         $offset = $this->input->post('ofset');
         $limit = $this->input->post('limit');


        if(($offset>0) && ($limit>=1)){

            $data['customer_list'] = $this->Customer_model->customer_pdf_data($offset,$limit);
            //=========== its for create pdf ==============
            $times = "BMSLINK Customer List_" . date('mdy');
            $this->load->library('pdfgenerator');

            $dompdf = new DOMPDF();

            $page = $this->load->view('b_level/customers/customer_export_pdf', $data, true);
            $dompdf->load_html($page);
            $dompdf->render();
            $output = $dompdf->output();
            file_put_contents("assets/b_level/pdf/customers/$times.pdf", $output);
            $filename = $times . '.pdf';
            $file_path = base_url() . 'assets/b_level/pdf/customers/' . $filename;

            $this->load->helper('download');
            force_download(FCPATH . 'assets/b_level/pdf/customers/' . $filename, null);

        } else {

            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>'Please check valid start and limit data!</div>");

            redirect('b-customer-list');

        }


    }



    public function top_search_customer_order_info() {

        $keyword = $this->input->post('keyword');
        $data['get_top_search_customer_info'] = $this->Customer_model->get_top_search_customer_info($keyword);
        $data['get_top_search_order_info'] = $this->Customer_model->get_top_search_order_info($keyword);
        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/customers/top_search_customer_order_info');
        $this->load->view('b_level/footer');
    }

/** Start added by insys */
//    =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('customer_info','customer_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Customer has been Deleted successfully.</div>");
        }
        redirect("b-customer-list");
    } 
/** End added by insys */

}
