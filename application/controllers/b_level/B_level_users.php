<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class B_level_users extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('session_id'); 
        $user_type = $this->session->userdata('user_type'); 
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }

        $this->load->model(array(
            'b_level/settings' 
        ));
       
    }


    public function index() {


    }


    //=========== its for new_user =============
    public function edit_user($user_id) {
        
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/edit_user');
        $this->load->view('b_level/footer');

    }


    public function update_user(){


        $this->form_validation->set_rules('first_name', 'Firstname','required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Lastname','required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[log_info.email]|max_length[100]');
        $this->form_validation->set_rules('password', 'Password','required|max_length[32]');
        $this->form_validation->set_rules('address', 'Address','max_length[1000]');

            $created_date = date('Y-m-d');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $address = $this->input->post('address');

            $user_id = $this->input->post('user_id');

            //=========== its for save user info ============
            $user = (object) $user_data = array(
                'first_name'    =>  $first_name,
                'last_name'     =>  $last_name,
                'email'         =>  $email,
                'address'       =>  $address,
                'phone'       =>  $phone,
                'created_by'    =>  $this->user_id,
                'user_type'     =>  'b',
                'create_date'   =>  $created_date
            );

        if ($this->form_validation->run()) {

            $this->db->insert('user_info', $user_data);
            $user_id = $this->db->insert_id();

            // =========== its for save log info ============
            $loginfo = array(
                'user_id'   => $user_id,
                'email'     => $email,
                'password'  => md5($password),
                'user_type' => 'b',
                'is_admin'  => 2,
            );
            $this->db->insert('log_info', $loginfo);

            //============ its for access log info collection ===============
            $accesslog_info = array(
                'action_page' =>$this->uri->segment(1),
                'action_done' => 'insert',
                'remarks' => "User information save",
                'user_name' => $this->user_id,
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
     
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User save successfully!</div>");
            redirect('create-user');
        }else{

            $data['user'] = $user;
            $this->load->view('b_level/header',$data);
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/settings/new_user');
            $this->load->view('b_level/footer');            
        }

    } 
    
}
