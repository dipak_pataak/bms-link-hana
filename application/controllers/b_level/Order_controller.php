<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('session_id');

        if ($session_id == NULL) {
            redirect('b-level-logout');
        }

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Order_model');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Customer_model');
        $this->load->model('b_level/settings');

        $this->load->model('email_sender');
    }


    public function index() {
        
    }


    public function get_color_partan_model($product_id) {

        $pp = $this->db->select('colors,pattern_models_ids')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();

        $colors = $pattern_model = array();

        if ($pp->colors) {
            $color_ids = explode(',', $pp->colors);
            $colors = $this->db->where_in('id', $color_ids)->get('color_tbl')->result();
        }

        $patter_ids = explode(',', @$pp->pattern_models_ids);

        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->get('pattern_model_tbl')->result();

        $q = '';

        $q .= '<div class="row">
                    <label for="" class="col-sm-3">Pattern</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="pattern_model_id" id="pattern_id" data-placeholder="-- select pattern --">
                            <option value="">-- select pattern --</option>';

        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }

        $q .= '</select>
                    </div>
                </div><br/>';

        /* $q .= '
            <div class="row">
                <label for="" class="col-sm-3">Color</label>
                <div class="col-sm-3">
                    <select class="form-control select2" name="color_id" id="color_id" onchange="getColorCode(this.value)"  data-placeholder="-- select one --">
                        <option value="">-- select one --</option>';
        foreach ($colors as $color) {
            $q .= '<option value="' . $color->id . '">' . $color->color_name . '</option>';
        }
        $q .= '</select>
                </div>
                <div  class="col-sm-2">Color code :</div>
                <div  class="col-sm-2"><input type="text" id="colorcode" onkeyup="getColorCode_select(this.value)" class="form-control"></div>
            </div>'; */

        echo $q;
    }
    
    public function get_color_model($pattern_id) {

        $colors = array();
        $colors = $this->db->where('pattern_id', $pattern_id)->get('color_tbl')->result();

        $q = '';

        $q .= '
            <div class="row">
                <label for="" class="col-sm-3">Color</label>
                <div class="col-sm-3">
                    <select class="form-control select2" name="color_id" id="color_id" onchange="getColorCode(this.value)"  data-placeholder="-- select one --">
                        <option value="">-- select one --</option>';
        foreach ($colors as $color) {
            $q .= '<option value="' . $color->id . '">' . $color->color_name . '</option>';
        }
        $q .= '</select>
                </div>
                <div  class="col-sm-2">Color code :</div>
                <div  class="col-sm-2"><input type="text" id="colorcode" onkeyup="getColorCode_select(this.value)" class="form-control"></div>
            </div>';

        echo $q;
    }


    public function get_color_code($id) {

        $colors = $this->db->where('id', $id)->get('color_tbl')->row();

        echo $colors->color_number;
    }


    public function get_color_code_select($keyword) {

        $colors = $this->db->where('color_number', $keyword)->get('color_tbl')->row();

        echo @$colors->id;
    }



    //------------------------------------------------------
    // Category waise subcategory
    //------------------------------------------------------
    public function category_wise_subcategory($category_id = null) {

        $category_wise_subcategory = $this->db->select('*')
                        ->where('parent_category', $category_id)
                        ->get('category_tbl')->result();

        $q = "";
        if (!empty($category_wise_subcategory)) {

            $q .= '<div class="row">
                        <label for="" class="col-sm-3">Select Sub Category</label>

                        <div class="col-sm-6">
                            <select class="form-control select2" name="category_id" id="sub_category_id" data-placeholder="-- select one --">
                                <option value="0">--Select--</option>';

            foreach ($category_wise_subcategory as $value) {
                $q .= "<option value='$value->category_id'>$value->category_name</option>";
            }

            $q .= '</select>
                        </div>
                    </div>';

            // echo '<label for="subcategory_id" class="mb-2">Select Sub-Category </label>';
            // echo '<select class="form-control select2" name="subcategory_id" data-placeholder="-- select firstly category --"">';
            // echo '<option value=""></option>';
            // echo'<option value="">-- select one -- </option>';
            // foreach ($category_wise_subcategory as  $value) {
            //    echo'<option value="'.$value->category_id.'">'.$value->category_name.'</option>';
            // }
            // echo '</select>';
        }
        echo $q;
    }

    //------------------------------------------------
    //=============== its for customer_save =============
    public function customer_save() {
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "b level Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $phone_type = $this->input->post('phone_type');
        $company = $this->input->post('company');
        $customer_type = $this->input->post('customer_type');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $file_upload = $this->input->post('file_upload');
        //print_r($_FILES['file_upload']);        die();
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        dd($phone);
        if ($customer_type == 'business') {
//            =========== its for save customer data in the users table ===============
            $customer_userinfo_data = array(
                'created_by' => $this->user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $company,
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'phone' => $phone[0],
                'email' => $email,
                'user_type' => 'c',
                'create_date' => $created_date,
            );
            $this->db->insert('user_info', $customer_userinfo_data);
            $user_insert_id = $this->db->insert_id();
//            =========== its for save customer data in the users table and login info ===============
            $customer_loginfo_data = array(
                'user_id' => $user_insert_id,
                'email' => $username,
                'password' => md5($password),
                'user_type' => 'c',
                'is_admin' => '1',
            );
            $this->db->insert('log_info', $customer_loginfo_data);
//            ============== its for company profile ===========
            $company_profile = array(
                'user_id' => $user_insert_id,
                'company_name' => $company,
                'email' => $email,
                'phone' => $phone[0],
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'created_by' => $this->user_id,
                'created_at' => $created_date,
            );
            $this->db->insert('company_profile', $company_profile);
        }

//  ============ its for accounts coa table ===============
        $coa = $this->Customer_model->headcode();
        if ($coa->HeadCode != NULL) {
            $headcode = $coa->HeadCode + 1;
        } else {
            $headcode = "102030101";
        }

        $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                ->order_by('customer_id', 'desc')
                ->get()
                ->row();
//        dd($lastid);
        $sl = $lastid->customer_no;
        if (empty($sl)) {
            $sl = "CUS-0001";
        } else {
            $sl = $sl;
        }
        $supno = explode('-', $sl);
        $nextno = $supno[1] + 1;
        $si_length = strlen((int) $nextno);

        $str = '0000';
        $cutstr = substr($str, $si_length);
        $sino = "CUS" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;
//        $customer_name = $this->input->post('customer_name');
        $customer_no = $sino . '-' . $first_name . " " . $last_name;
//        ================= close =======================
//        =============== its for company name with customer id start =============
        $last_c_id = $lastid->customer_id;
        $cn = strtoupper(substr($company, 0, 3)) . "-";
        if (empty($last_c_id)) {
            $last_c_id = $cn . "1";
        } else {
            $last_c_id = $last_c_id;
        }
        $cust_nextid = $last_c_id + 1;
        $company_custid = $cn . $cust_nextid;
//        =============== its for company name with customer id close=============
//        ========== its for logo =============
// configure for upload 
        $config = array(
            'upload_path' => "./assets/b_level/uploads/customers/",
            'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
            'overwrite' => TRUE,
//            'file_name' => "BMSLINK" . time(),
            'file_size' => '2048',
        );

//        $image_name = '';
//        $image_data = array();
//        $this->load->library('upload', $config);
//        $this->upload->initialize($config);
//        echo '<pre>';        print_r($config);die();
//        if ($this->upload->do_upload('file_upload_1')) {
////            echo "DS";die();
//            $image_data = $this->upload->data();
////                dd($image_data);
//            $image_name = $image_data['file_name'];
//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $image_data['full_path']; //get original image
//            $config['maintain_ratio'] = TRUE;
//            $config['height'] = '*';
//            $config['width'] = '*';
////                $config['quality'] = 50;
//            $this->load->library('image_lib', $config);
//            $this->image_lib->clear();
//            $this->image_lib->initialize($config);
//            if (!$this->image_lib->resize()) {
//                echo $this->image_lib->display_errors();
//            }
//        } else {
//            $image_name = '';
//        }
//        //            ========= its for file upload 2 =========
//        if ($this->upload->do_upload('file_upload_2')) {
//            $image_dataTwo = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_2 = $image_dataTwo['file_name'];
//        } else {
//            $file_upload_2 = '';
//        }
//        //            ========= its for file upload 3 =========
//        if ($this->upload->do_upload('file_upload_3')) {
//            $image_dataThree = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_3 = $image_dataThree['file_name'];
//        } else {
//            $file_upload_3 = '';
//        }
//        //            ========= its for file upload 2 =========
//        if ($this->upload->do_upload('file_upload_4')) {
//            $image_dataFour = $this->upload->data();
////              echo '<pre>';  print_r($image_dataTwo); die();
//            $file_upload_4 = $image_dataFour['file_name'];
//        } else {
//            $file_upload_4 = '';
//        }
//        dd($config);
        $customer_data = array(
            'customer_user_id' => $user_insert_id,
            'customer_no' => $customer_no,
            'company_customer_id' => $company_custid,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone[0],
//            'phone_2' => $phone_2,
//            'phone_3' => $phone_3,
            'company' => $company,
            'customer_type' => $customer_type,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip_code,
            'country_code' => $country_code,
            'street_no' => $street_no,
            'side_mark' => $side_mark,
            'level_id' => $level_id,
            'created_by' => $this->user_id,
            'create_date' => $created_date,
        );
//        dd($customer_data);
        $this->db->insert('customer_info', $customer_data);
        $customer_inserted_id = $this->db->insert_id();
//        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadCode' => $headcode,
            'HeadName' => $customer_no,
            'PHeadName' => 'Customer Receivable',
            'HeadLevel' => '4',
            'IsActive' => '1',
            'IsTransaction' => '1',
            'IsGL' => '0',
            'HeadType' => 'A',
            'IsBudget' => '0',
            'IsDepreciation' => '0',
            'DepreciationRate' => '0',
            'CreateBy' => $this->user_id,
            'CreateDate' => $created_date,
        );
//        dd($customer_coa);
        $this->db->insert('b_acc_coa', $customer_coa);
//        ======= close ==============
//            ================ its for multiple certificate info save ===========
//        if ($_FILES["file_upload"]['name'][0] != '') {
//            $file_uploadCount = count($_FILES['file_upload']['name']);
//            for ($i = 0; $i < $file_uploadCount; $i++) {
//                $_FILES['file_uploader']['name'] = $_FILES['file_upload']['name'][$i];
//                $_FILES['file_uploader']['type'] = $_FILES['file_upload']['type'][$i];
//                $_FILES['file_uploader']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
//                $_FILES['file_uploader']['error'] = $_FILES['file_upload']['error'][$i];
//                $_FILES['file_uploader']['size'] = $_FILES['file_upload']['size'][$i];
//
//                // configure for upload 
////                $config = array(
////                    'upload_path' => "./assets/uploads/doctor/",
////                    'allowed_types' => "gif|jpg|png|jpeg|pdf",
////                    'overwrite' => TRUE,
//////                    'file_name' => "BDTASK" . time(),
////                    'max_size' => '0',
////                );
//                $image_data = array();
//
//                $this->load->library('upload', $config);
//                $this->upload->initialize($config);
//
//                if ($this->upload->do_upload('file_uploader')) {
//                    $image_data = $this->upload->data();
////                print_r($image_data); die();
//                    $customer_file_name = $image_data['file_name'];
//
//                    $customerFileinfo[$i]['customer_id'] = $customer_inserted_id;
//                    $customerFileinfo[$i]['customer_user_id'] = $user_insert_id;
//                    $customerFileinfo[$i]['file_upload'] = $customer_file_name;
////                    $customerFileinfo[$i]['degree_name'] = $degrees_name[$i];
//                }
//            }
//            $this->Customer_model->save_customer_file($customerFileinfo);
//        }
//        =============== its for customer phone type info ==============
        for ($i = 0; $i < count($phone); $i++) {
            $phone_types_number = array(
                'phone' => $phone[$i],
                'phone_type' => $phone_type[$i],
                'customer_id' => $customer_inserted_id,
                'customer_user_id' => $user_insert_id,
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
        }
        //        =========== its for customer user info send by email ============     
        if ($email) {
            $data['get_mail_config'] = $this->settings->get_mail_config();
            $this->Customer_model->sendLink($user_insert_id, $email, $data, $username, $password);
        }
//        ============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

//        ============== close access log info =================

        $query_customer_info = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', $level_id)
                        ->where('customer_id', $customer_inserted_id)
                        ->get()->row();

        echo json_encode($query_customer_info);
    }


#---------------------------------------
//its for new_order  form
#---------------------------------------
    public function new_order() {

        $this->permission->check_label('order')->create()->redirect();

        $data['get_customer'] = $this->Order_model->get_customer();

        $data['get_category'] = $this->Order_model->get_category();

        $data['get_patern_model'] = $this->Order_model->get_patern_model();

        $data['get_product'] = $this->Order_model->get_product();


        $data['colors'] = $this->db->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();

        $data['rooms'] = $this->db->get('rooms')->result();

        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "b_level/orders/customer_js.php";

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_new', $data);
        $this->load->view('b_level/footer');
    }

#------------------------------------
# END
# --------------------------------------


//    ============ its for customer_wise_sidemark ==========
    public function customer_wise_sidemark($customer_id = null) {

        $customer_wise_sidemark = $this->db->select('customer_info.*,us_state_tbl.tax_rate')
                        ->from('customer_info')
                        ->join('us_state_tbl', 'us_state_tbl.shortcode=customer_info.state', 'left')
                        ->where('customer_info.customer_id', $customer_id)
                        ->get()->row();

        echo json_encode($customer_wise_sidemark);
    }


//    ============= its for passenger_id_generate ===========
    public function order_id_generate() {

        $last_order_id = $this->db->select('order_id')->from('b_level_quatation_tbl')->order_by('id', 'desc')->get()->row();
        $custom_id = @$last_order_id->order_id;

        if (empty($custom_id)) {
            $custom_id = "154762855914ZJ-001";
        } else {
            $custom_id = $custom_id;
        }

        $order_id = explode('-', $custom_id);
        $order_id = $order_id[1] + 1;
        $si_length = strlen((int) $order_id);
        $str = '000';
        $order_str = substr($str, $si_length);
//        echo $cutstr.$order_id;        die();

        $passenger_id = time() . $this->random_keygenerator(1, 4) . "b-" . $order_str . $order_id;
        echo strtoupper($passenger_id);
    }

//    ============= its for random key generator ============
    public function random_keygenerator($mode = null, $len = null) {
        $result = "";
        if ($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4):
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

// ============= its for order_kanban ===============
    public function order_kanban() {

        $this->permission->check_label('order')->create()->redirect();

        $search = (object) array(
            'product_id'    => $this->input->post('product_id'),
            'customer_id'   => $this->input->post('customer_id'),
            'order_date'    => $this->input->post('order_date'),
            'order_stage'   => $this->input->post('order_stage')
        );

        $data['customers'] = $this->Order_model->get_customer();

        $data['quote_orderd'] = $this->Order_model->get_quote_orderd($search);
        $data['paid_orderd'] = $this->Order_model->get_paid_orderd($search);
        $data['partially_paid_orderd'] = $this->Order_model->get_partially_paid_orderd($search);
        
        $data['shipping_orderd'] = $this->Order_model->get_shipping_orderd($search);
        $data['cancelled_orderd'] = $this->Order_model->get_cancelled_orderd($search);
        $data['manufactur_orderd'] = $this->Order_model->get_manufactur_orderd($search);


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_kanban', $data);
        $this->load->view('b_level/footer');
    }


#--------------------------------------
# its for order_list 
#--------------------------------------
    public function order_list() {

        $this->permission->check_label('order')->read()->redirect();

        $search = (object) array(
                    'product_id' => $this->input->post('product_id'),
                    'customer_id' => $this->input->post('customer_id'),
                    'order_date' => $this->input->post('order_date'),
                    'order_stage' => $this->input->post('order_stage')
        );



        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->customer_id;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;

        //total row count for pasination

        $per_page = 15;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        //-----------
        $total_rows = $this->Order_model->get_all_orderd_count($search, $per_page, $page);

        $data['orderd'] = $this->Order_model->get_all_orderd($search, $per_page, $page);


        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_list', $data);
        $this->load->view('b_level/footer');
    }

#-----------------------------------
# END
#-----------------------------------


    function pagination($total_rows, $per_page, $page) {

        $config["base_url"] = base_url('b-order-list');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $page;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        return $config;
    }


#-----------------------------------
# order_view
#-----------------------------------

    public function order_view($order_id) {

        $this->permission->check_label('order')->read()->redirect();

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_view', $data);
        $this->load->view('b_level/footer');

    }


#-----------------------------------
# delete_order
#-----------------------------------
    public function delete_order($order_id) {

        $this->permission->check_label('order')->delete()->redirect();

        $this->db->where('order_id', $order_id)->delete('b_level_quatation_tbl');
        $this->db->where('order_id', $order_id)->delete('b_level_qutation_details');
        $this->db->where('order_id', $order_id)->delete('b_level_quatation_attributes');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Delete successfully!</div>");
        redirect("b-order-list");
    }




#-----------------------------------
# get_product_by_category
#-----------------------------------
    public function get_product_by_category($category_id) {

        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)
                        ->where('active_status', 1)
                        ->get('product_tbl')->result();





        $q = '';
        $q .= '<option value=""></option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }




//========== its for get_product_by_subcategory ===========
    public function get_product_by_subcategory($subcategory_id) {

        $result = $this->db->select('product_id,category_id,product_name')->where('subcategory_id', $subcategory_id)->where('active_status', 1)->get('product_tbl')->result();
        $q = '';
        $q .= '<option value=""></option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }

    //-----------------------------
    //--------------------------------------------------
    // get product attributes options price
    public function get_product_attr_option_option_price($pro_att_op_id, $main_price) {

        //$option = $this->db->where('att_op_id', $att_op_id)->get('attr_options')->row();

        $option = $this->db->select('attr_options.*')
                        ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                        ->where('product_attr_option.id', $pro_att_op_id)
                        ->get('product_attr_option')->row();


        //$products = $this->db->where('product_id',$product_id)->get('product_tbl')->row();
        $main_price = (int) $main_price;
        $contribution_price = 0;

        $q = '';

        if (!empty($option)) {

            if ($option->price_type == 1) {

                $price_total = $main_price + @$option->price;
                $prices = '<p>' . $main_price . '+' . @$option->price . '=' . $price_total . '</p>';
                $contribution_price = (!empty($option->price) ? $option->price : 0);
            } else {

                $price_total = ($main_price * $option->price) / 100;
                $prices = '<p>(' . $main_price . '*' . @$option->price . ')/100 = ' . ($main_price + $price_total) . '</p>';
                $contribution_price = (!empty($price_total) ? $price_total : 0);
            }


            // $q.='<div class="form-group col-md-12">
            //             <div class="row">
            //                 <lable class="col-sm-12">'.$option->option_name.' :</lable>
            //                 <div class="col-sm-12">
            //                     <input type="text" value="' . $contribution_price . '" readonly class="form-control contri_price">
            //                 </div>
            //             </div>
            //         </div>';



            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->get('product_attr_option_option')->result();

            if (!empty($opops)) {

                foreach ($opops as $key => $opop) {

                    if ($opop->att_op_op_price_type == 1) {

                        $price_total = $main_price + @$opop->att_op_op_price;
                        $contribution_price1 = (!empty($opop->att_op_op_price) ? $opop->att_op_op_price : 0);
                    } else {

                        $price_total = ($main_price * $opop->att_op_op_price) / 100;
                        $contribution_price1 = (!empty($price_total) ? $price_total : 0);
                    }


                    // $q.='<div class="form-group col-md-12">
                    //     <div class="row">
                    //         <lable class="col-sm-12">'.$opop->op_op_name.' :</lable>
                    //         <div class="col-sm-12">
                    //             <input type="text" value="' . $contribution_price1 . '" readonly class="form-control contri_price">
                    //         </div>
                    //     </div>
                    // </div>';

                    $q .= '<input type="text" value="' . $contribution_price1 . '" class="form-control contri_price">';
                }
            }


            //option price
            //$q .= '<input type="text" readonly name="option_price_' . $option->attribute_id . '" value="' . @$option->price . '" class="form-control">' . @$prices;
            //------
            // $q .= '<input type="text" name="option_id_' . $option->attribute_id . '" value="' . @$option->att_op_id . '" class="form-control">';
            //$q .= '<input type="text" name="option_value_' . $option->attribute_id . '" value="" class="form-control">';
            $q .= '<input type="text" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $q .= '';
        }

        echo $q;
    }



    //-----------------------------
    //--------------------------------------------------
    // get product attributes options price
    public function get_product_attr_option_option_option_price($op_op_op_id, $attribute_id, $main_price) {


        $opopop = $this->db->where('att_op_op_op_id', $op_op_op_id)->get('attr_options_option_option_tbl')->row();

        $q = '';

        if ($opopop) {

            if ($opopop->att_op_op_op_price_type == 1) {

                $price_total = $main_price + @$opopop->att_op_op_op_price;
                $contribution_price = (!empty($opopop->att_op_op_op_price) ? $opopop->att_op_op_op_price : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            } else {

                $price_total = ($main_price * @$opopop->att_op_op_op_price) / 100;
                $contribution_price = (!empty($price_total) ? $price_total : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            }

            $q .= '<div class="form-group col-md-12">
                        <div class="row">
                            <lable class="col-sm-12">' . $opopop->att_op_op_op_name . ' :</lable>
                            <div class="col-sm-12">
                                <input type="text" value="' . $contribution_price . '" readonly class="form-control contri_price">
                            </div>
                        </div>
                    </div>';


            $opopopop = $this->db->where('op_op_op_id', $op_op_op_id)->where('attribute_id', $attribute_id)->get('attr_op_op_op_op_tbl')->result();

            foreach ($opopopop as $key => $value) {


                if ($value->att_op_op_op_op_price_type == 1) {

                    $price_total = $main_price + @$value->att_op_op_op_op_price;
                    $contribution_price1 = (!empty($value->att_op_op_op_op_price) ? $value->att_op_op_op_op_price : 0);
                    $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
                } else {

                    $price_total = ($main_price * @$value->att_op_op_op_op_price) / 100;
                    $contribution_price1 = (!empty($price_total) ? $price_total : 0);
                    $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
                }


                $q .= '<div class="form-group col-md-12">
                        <div class="row">
                            <lable class="col-sm-12">' . $value->att_op_op_op_op_name . ' :</lable>
                            <div class="col-sm-12">
                                <input type="text" value="' . $contribution_price1 . '" readonly class="form-control contri_price">
                            </div>
                        </div>
                    </div>';
            }
        } else {

            $q .= '';
        }

        echo $q;
    }

    //---------------------------------------
    // get product attributs

    public function get_product_to_attribute($product_id) {

        $attributes = $this->db->select('product_attribute.*,attribute_tbl.attribute_name,attribute_tbl.attribute_type')
                        ->join('attribute_tbl', 'attribute_tbl.attribute_id=product_attribute.attribute_id')
                        ->where('product_attribute.product_id', $product_id)
                        ->order_by('attribute_tbl.position', 'ASC')
                        ->get('product_attribute')->result();

        // for price style
        $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();


        $q = '';
        $main_price = 0;
        if ($p->price_style_type == 3) {

            $q .= '<input type="hidden" name="pricestyle"  value="' . $p->price_style_type . '" id="pricestyle" >';
            // Fix price 
            $q .= '<div class="row hidden">
                    <label class="col-sm-3">Fixed Price</label>
                    <div class="col-sm-6">
                        <input type="text" name="main_price" class="form-control" id="main_price" readonly value="' . $p->fixed_price . '" >
                    </div>
                </div><br>';

            $main_price = $p->fixed_price;
        } elseif ($p->price_style_type == 2) {

            // sqr fit price 
            $q .= '<input type="hidden" name="pricestyle"  value="' . $p->price_style_type . '" id="pricestyle" >';
            $q .= '<input type="hidden" name="sqr_price"  value="' . $p->sqft_price . '" id="sqr_price" >';
            $q .= '<div class="row hidden">
                        <label class="col-sm-3">Sqft Price Price</label>
                        <div class="col-sm-6">
                            <input type="text" name="main_price" class="form-control" id="main_price"  readonly value="' . $p->sqft_price . '" >
                        </div>
                    </div><br>';

            $main_price = $p->sqft_price;
        }


        foreach ($attributes as $attribute) {

            if ($attribute->attribute_type == 3) {

                $options = $this->db->select('attr_options.*,product_attr_option.id')
                                ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                                ->where('product_attr_option.pro_attr_id', $attribute->id)
                                ->order_by('attr_options.att_op_id', 'ASC')
                                ->get('product_attr_option')->result();


                $q .= '<div class="row">
                            
                        <label class="col-sm-3">' . $attribute->attribute_name . '</label>
                        <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                        <input type="hidden" name="attribute_value[]" value="">';

                foreach ($options as $op) {

                    $q .= '<div class="col-sm-3">
                                <label>' . $op->option_name . '</label>
                                <input type="hidden" name="op_id_' . $attribute->attribute_id . '[]" value="' . $op->id . '_' . $op->att_op_id . '">';
                    $q .= '<input type="text" name="op_value_' . $attribute->attribute_id . '[]" min="0"  class="form-control">';
                    $q .= '</div>';
                    $q .= $this->contri_price($op->price_type, $op->price, $main_price);
                }

                $q .= '<div class="col-sm-6" style="display:none;"></div>';
                $q .= '<div class="col-sm-4" style="margin-top:-7px;"></div>';

                $q .= '</div><br>';
            } elseif ($attribute->attribute_type == 2) {

                $options = $this->db->select('attr_options.*,product_attr_option.id')
                                ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                                ->where('product_attr_option.pro_attr_id', $attribute->id)
                                ->order_by('attr_options.att_op_id', 'ASC')
                                ->get('product_attr_option')->result();

                $q .= '<div class="row">
                            <label class="col-sm-3">' . $attribute->attribute_name . '</label>
                            <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                            <input type="hidden" name="attribute_value[]" value="">';

                $q .= '<input type="hidden" name="op_value_' . $attribute->attribute_id . '[]" min="0"  class="form-control">';

                $q .= '<div class="col-sm-3">
                                
                                <select class="form-control op_op_load select2 options_' . $attribute->attribute_id . '" name="op_id_' . $attribute->attribute_id . '[]"  onChange="OptionOptions(this.value,' . $attribute->attribute_id . ')" data-placeholder="-- select pattern/model --">
                                    
                                    <option value="0_0" >--Select one--</option>';
                $sl1 = 1;
                foreach ($options as $op) {
                    $q .= '<option value="' . $op->id . '_' . $op->att_op_id . '" ' . ($sl1 == 1 ? 'selected' : '') . ' >' . $op->option_name . '</option>';
                    $sl1++;
                }
                $q .= '</select></div>';

                $q .= '<div class="col-sm-6" style="display:none;""></div>';
                $q .= '<div class="col-sm-4" style="margin-top:-7px;"></div>';

                $q .= '</div><br>';
            } elseif ($attribute->attribute_type == 1) {

                $height = $attribute->attribute_name;
                $q .= '<div class="row">
                                <lable class="col-sm-3">' . $attribute->attribute_name . '</lable>
                                <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">

                                <div class="col-sm-6">
                                    <input type="text" name="attribute_value[]" id="' . $attribute->attribute_name . '" class="form-control">
                                </div>
                                
                        </div><br>';
            } else {

                $q .= '';
            }
        }

        echo $q;
    }



    public function contri_price($price_type, $option_price, $main_price) {


        $q = '';

        if ($price_type == 1) {
            $price_total = $main_price + @$option_price;
            $contribution_price = (!empty($option_price) ? $option_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {
            $price_total = ($main_price * $option_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        return $q;
    }



    public function get_product_attr_option_option($pro_att_op_id, $attribute_id, $main_price) {

        $options = $this->db->select('attr_options.*')
                        ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                        ->where('product_attr_option.id', $pro_att_op_id)
                        ->order_by('attr_options.att_op_id', 'ASC')
                        ->get('product_attr_option')->row();

        $q = '';

        if ($options->price_type == 1) {
            $price_total = $main_price + @$options->price;
            $contribution_price = (!empty($options->price) ? $options->price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {
            $price_total = ($main_price * $options->price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }




        if (@$options->option_type == 5) {


            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                            ->get('product_attr_option_option')->result();

            $fractions = $this->db->get('width_height_fractions')->result();

            foreach ($opops as $op_op) {

                $q .= '<br><div class="col-sm-12">

                            <label>' . $op_op->op_op_name . '</label>
                            <input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">
                            <input type="text" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';

                $q .='<div class="col-sm-6" style="float:right; margin-left:-15px;">
                                <select class="form-control" name="fraction_' . $attribute_id . '[]" id=""  data-placeholder="-- select one --">
                                    <option value="">-- select one --</option>';
                                    
                                    foreach ($fractions as $f) {
                                        $q .= '<option value='.$f->fraction_value.'>'.$f->fraction_value.'</option>';
                                    }
                                $q .='</select></div>';        
                        $q .='</div>';

                $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price);
            }



        }elseif ($options->option_type == 4) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                            ->get('product_attr_option_option')->result();

            foreach ($opops as $op_op) {

                $opopops = $this->db->where('attribute_id', $attribute_id)->where('att_op_op_id', $op_op->op_op_id)->get('attr_options_option_option_tbl')->result();

                $q .= '<input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">';
                
                if($op_op->type==2){

                    $q .= '<div class="col-sm-12"><label>' . $op_op->op_op_name . '</label>
                                <select class="form-control select2" id="mul_op_op_id' . $op_op->op_op_id . '" onChange="multiOptionPriceValue(this.value)" data-placeholder="-- select pattern/model --">
                                    <option value="" >--Select one--</option>';
                                foreach ($opopops as $opopop) {
                                    $q .= '<option value="' . $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id . '">' . $opopop->att_op_op_op_name . '</option>';
                                }
                    $q .= '</select></div>';

                }elseif($op_op->type==1){

                    $q .= '<div class="col-sm-12"><label>' . $op_op->op_op_name . '</label>';
                    $q .='<input type="text" name="op_op_value_' . $attribute_id . '[]" value="" class="form-control">';          
                               
                    $q .= '</div>';
                }


                // $q .= '<div class="col-sm-12"><label>' . $op_op->op_op_name . '</label>
                //             <select class="form-control select2" id="mul_op_op_id' . $op_op->op_op_id . '" onChange="multiOptionPriceValue(this.value)" data-placeholder="-- select pattern/model --">
                //                 <option value="" >--Select one--</option>';
                // foreach ($opopops as $opopop) {
                //     $q .= '<option value="' . $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id . '">' . $opopop->att_op_op_op_name . '</option>';
                // }
                // $q .= '</select></div>';

                $q .= '<div class="col-sm-12" style="display:none;" >
                                <div class="col-sm-12" style="margin-top:-7px;"></div>
                            </div><br>';

                $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price);
            



            }


        } elseif (@$options->option_type == 3) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                            ->get('product_attr_option_option')->result();


            foreach ($opops as $op_op) {

                $q .= '<br><div class="col-sm-12">
                            <label>' . $op_op->op_op_name . '</label>
                            <input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">
                            <input type="text" name="op_op_value_' . $attribute_id . '[]"  class="form-control">
                        </div>';

                $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price);
            }

        } elseif (@$options->option_type == 2) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                            ->get('product_attr_option_option')->result();

            //$q .= '<input type="hidden" name="op_id_'.$attribute_id.'[]" value="' . $options->att_op_id. '" class="form-control">';
            $q .= '<input type="hidden" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<br><div class="col-sm-12">
                            <select class="form-control select2 " id="op_' . $options->att_op_id . '" name="op_op_id_' . $attribute_id . '[]"  onChange="OptionOptionsOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                                    <option value="0">--Select one--</option>';

            foreach ($opops as $op_op) {
                $q .= '<option value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">' . $op_op->op_op_name . '</option>';
            }

            $q .= '</select></div><br>';

            $q .= '<div class="col-sm-12">
                                <div class="col-sm-12" style="margin-top:-7px;"></div>
                            </div><br>';
        } elseif (@$options->option_type == 1) {

            $q .= '<br>
                        <div class="col-sm-12">
                            <input type="text" name="op_value_' . $attribute_id . '[]" class="form-control">
                        </div>
                    <br>';
        } else {

            $q .= '';
        }

        echo $q;
    }



    public function get_product_attr_op_op_op($op_op_id, $pro_att_op_op_id, $attribute_id, $main_price) {

        $opop = $this->db->where('op_op_id', $op_op_id)->get('attr_options_option_tbl')->row();

        //$opopop = $this->db->where('att_op_op_op_id',$op_op_op_id)->get('attr_options_option_option_tbl')->row();


        $q = '';
        if ($opop->att_op_op_price_type == 1) {

            $price_total = $main_price + @$opop->att_op_op_price;
            $contribution_price = (!empty($opop->att_op_op_price) ? $opop->att_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opop->att_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        // $q .= '<input type="hidden" name="op_op_price_'.$attribute_id.'[]" value="' . $contribution_price . '" class="form-control">';

        if ($opop->type == 3) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*')
                            ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                            ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                            ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                            ->order_by('attr_options_option_option_tbl.att_op_op_op_id', 'ASC')
                            ->get('product_attr_option_option_option')->result();

            foreach ($opopop as $op_op_op) {

                $q .= '<br><div class="col-sm-12">
                            <label>' . $op_op_op->att_op_op_op_name . '</label>
                            <input type="hidden" name="op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">
                            <input type="text" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control">
                        </div>';

                $q .= $this->contri_price($op_op_op->att_op_op_op_price_type, $op_op_op->att_op_op_op_price, $main_price);
            }
        } elseif ($opop->type == 2) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*')
                            ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                            ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                            ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                            ->order_by('attr_options_option_option_tbl.att_op_op_op_id', 'ASC')
                            ->get('product_attr_option_option_option')->result();

            $q .= '<br><input type="hidden" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control">
                        <select class="form-control select2 " id="op_op_' . $op_op_id . '" name="op_op_op_id_' . $attribute_id . '[]" onChange="OptionOptionsOptionOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                                <option value="0">--Select one--</option>';

            foreach ($opopop as $op_op_op) {
                $q .= '<option value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">' . $op_op_op->att_op_op_op_name . '</option>';
            }

            $q .= '</select><br>';

            $q .= '<div>
                         <div class="col-sm-12" style="margin-top:-7px;"></div>
                    </div><br>';
        } elseif ($opop->type == 1) {

            $q .= '<br>
                        <input type="text" name="op_op_value_' . $attribute_id . '[]" class="form-control">
                    
                <br>';
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function get_product_attr_op_op_op_op($op_op_op_id, $attribute_id, $main_price) {

        $opopop = $this->db->where('att_op_op_op_id', $op_op_op_id)->where('attribute_id', $attribute_id)->get('attr_options_option_option_tbl')->row();

        $q = '';

        // if ($opopop->att_op_op_op_price_type == 1) {
        //     $price_total = $main_price + @$opopop->att_op_op_op_price;
        //     $contribution_price = (!empty($opopop->att_op_op_op_price) ? $opopop->att_op_op_op_price : 0);
        //     $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        // } else {
        //     $price_total = ($main_price * @$opopop->att_op_op_op_price) / 100;
        //     $contribution_price = (!empty($price_total) ? $price_total : 0);
        //     $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        // }

        $q .= $this->contri_price($opopop->att_op_op_op_price_type, $opopop->att_op_op_op_price, $main_price);

        //$q .= '<input type="hidden" name="op_op_price_'.$attribute_id.'[]" value="' . $contribution_price . '" class="form-control">';
        if ($opopop->att_op_op_op_type == 3) {

            $opopopop = $this->db->select('*')
                            ->where('attribute_id', $attribute_id)
                            ->where('op_op_op_id', $op_op_op_id)
                            ->order_by('op_op_op_id', 'ASC')
                            ->get('attr_op_op_op_op_tbl')->result();

            foreach ($opopopop as $op_op_op_op) {

                $q .= '<br><div class="col-sm-12">
                            <label>' . $op_op_op_op->att_op_op_op_op_name . '</label>
                            <input type="hidden" name="op_op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">
                            <input type="text" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control">
                        </div>';

                $q .= $this->contri_price($op_op_op_op->att_op_op_op_op_price_type, $op_op_op_op->att_op_op_op_op_price, $main_price);
            }
        } elseif ($opopop->att_op_op_op_type == 2) {

            $opopopop = $this->db->select('*')
                            ->where('attribute_id', $attribute_id)
                            ->where('op_op_op_id', $op_op_op_id)
                            ->order_by('op_op_op_id', 'ASC')
                            ->get('attr_op_op_op_op_tbl')->result();

            $q .= '<input type="hidden" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<select class="form-control select2 " id="op_op_op_' . $op_op_op_id . '"  name="op_op_op_op_id_' . $attribute_id . '[]" onChange="OptionFive(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                    <option value="0">--Select one--</option>';
            foreach ($opopopop as $op_op_op_op) {
                $q .= '<option value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">' . $op_op_op_op->att_op_op_op_op_name . '</option>';
            }
            $q .= '</select><br>';

            $q .= '<div>
                        <div class="col-sm-12"></div>
                    </div><br>';
        } elseif ($opopop->att_op_op_op_type == 1) {

            $q .= '<div class="col-sm-12">
                        <input type="text" name="op_op_op_value_' . $attribute_id . '[]" class="form-control">
                    </div>
                <br>';
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function multioption_price_value($op_op_op_id, $attribute_id, $main_price) {

        $opopopop = $this->db->select('*')
                        ->where('attribute_id', $attribute_id)
                        ->where('att_op_op_op_id', $op_op_op_id)
                        ->get('attr_options_option_option_tbl')->row();


        $q = '';

        if ($opopopop->att_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopopop->att_op_op_op_price;
            $contribution_price = (!empty($opopopop->att_op_op_op_price) ? $opopopop->att_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opopopop->att_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        $q .= '<br><input type="text" name="op_op_value_' . $attribute_id . '[]" value="' . $opopopop->att_op_op_op_name . '" class="form-control">';


        echo $q;
    }

    public function get_product_attr_op_five($att_op_op_op_op_id, $attribute_id, $main_price) {

        $opopopop = $this->db->select('*')
                        ->where('attribute_id', $attribute_id)
                        ->where('att_op_op_op_op_id', $att_op_op_op_op_id)
                        ->get('attr_op_op_op_op_tbl')->row();

        $q = '';

        if ($opopopop->att_op_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopopop->att_op_op_op_op_price;
            $contribution_price = (!empty($opopopop->att_op_op_op_op_price) ? $opopopop->att_op_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $price_total = ($main_price * @$opopopop->att_op_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        if ($opopopop->att_op_op_op_op_type == 1) {

            $q .= '<br><input type="text" name="op_op_op_op_value_' . $attribute_id . '" class="form-control">';
        } else {

            $q .= '';
        }

        echo $q;
    }


#-----------------------------------------
#  get_product_row_col_price
#-----------------------------------------

    public function get_product_row_col_price($height = NULL, $width = NULL, $product_id = NULL, $pattern_id = NULL) {

        $q = "";
        $st = "";
        $row = "";
        $col = "";
        $prince = "";

        if (!empty($height) && !empty($width)) {

            $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();
        

            if (!empty($p->price_style_type) && $p->price_style_type == 1) {

                $prince = $this->db->where('style_id', $p->price_rowcol_style_id)
                                ->where('row', $width)
                                ->where('col', $height)
                                ->get('price_style')->row();

                $pc = ($prince != NULL ? $prince->price : 0);

                if (!empty($prince)) {

                    $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';
                    $st = 1;

                    $row = $prince->row;
                    $col = $prince->col;
                    $prince = $pc;

                } else {

                    $prince = $this->db->where('style_id', $p->price_rowcol_style_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();


                    $pc = ($prince != NULL ? $prince->price : 0);

                    $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';

                    $row = $prince->row;
                    $col = $prince->col;
                    $prince = $pc;
                    $st = 2;
                }
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 2) {

                $prince = $p->sqft_price;
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 3) {

                $prince = $p->fixed_price;
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 4) {

                $pg = $this->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();


                $price = $this->db->where('style_id', $pg->group_id)
                                ->where('row >=', $width)
                                ->where('col >=', $height)
                                ->order_by('row_id', 'asc')
                                ->limit(1)
                                ->get('price_style')->row();


                $pc = ($price != NULL ? $price->price : 0);

                $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';

                $row = $price->row;
                $col = $price->col;
                $prince = $pc;
                $st = 2;
            }
            elseif (!empty($p->price_style_type) && $p->price_style_type == 5) {

                $pg = $this->db->select('*')->from('sqm_price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();




                $pc = ($pg->price != NULL ? $pg->price : 0);

                $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';

                $row = $price->row;
                $col = $price->col;
                $prince = $pc;
                $st = 2;
            }

            $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
            echo json_encode($arr);
        }

        else{
            $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="0" >
                            </div>
                        </div>';
                    $st = 1;

                    $row = $prince->row;
                    $col = $prince->col;
                    $prince = 0;

            $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
            echo json_encode($arr);
        }
    }




#---------------------------------------------
#   Add to cart products 
#---------------------------------------------
    public function add_to_cart() {

        $attributes = $this->input->post('attribute_id');
        $attribute_value = $this->input->post('attribute_value');

        $attrib = [];

        foreach ($attributes as $key => $att) {

            $options = [];
            $op_op_s = [];
            $op_op_op_s = [];
            $op_op_op_op_s = [];

            $ops = $this->input->post('op_id_' . $att);
            $option_value = $this->input->post('op_value_' . $att);

            if (isset($ops) && is_array($ops)) {

                foreach ($ops as $key => $op) {

                    $options[] = array(
                        'option_id' => explode('_', $op)[1],
                        'option_value' => $option_value[$key]
                    );
                }
            }

            //$op_op_id = explode('_', $this->input->post('op_op_id_' . $att))[0];
            $opopid = $this->input->post('op_op_id_' . $att);
            $op_op_value = $this->input->post('op_op_value_' . $att);
            $fraction = $this->input->post('fraction_' . $att);

            if (isset($opopid) && is_array($opopid)) {

                foreach ($opopid as $key => $opop) {
                    $op_op_s[] = array(
                        'op_op_id' => explode('_', $opop)[0],
                        'op_op_value' => $op_op_value[$key].' '.$fraction[$key]
                    );
                }
            }

            $opopopid = $this->input->post('op_op_op_id_' . $att);
            //$op_op_op_id = explode('_', $this->input->post('op_op_op_id_' . $att))[0];
            $op_op_op_value = $this->input->post('op_op_op_value_' . $att);

            if (isset($opopopid) && is_array($opopopid)) {

                foreach ($opopopid as $key => $opopop) {

                    $op_op_op_s[] = array(
                        'op_op_op_id' => explode('_', $opopop)[0],
                        'op_op_op_value' => $op_op_op_value[$key]
                    );
                }
            }


            //$op_op_op_op_id = explode('_', $this->input->post('op_op_op_op_id_' . $att))[0];
            $opopopopid = $this->input->post('op_op_op_op_id_' . $att);
            $op_op_op_op_value = $this->input->post('op_op_op_op_value_' . $att);


            if (isset($opopopopid) && is_array($opopopopid)) {

                foreach ($opopopopid as $key => $opopopop) {

                    $op_op_op_op_s[] = array(
                        'op_op_op_op_id' => explode('_', $opopopop)[0],
                        'op_op_op_op_value' => $op_op_op_op_value[$key]
                    );
                }
            }


            $attrib[] = array(
                'attribute_id' => $att,
                'attribute_value' => $attribute_value[@$key],
                'options' => @$options,
                'opop' => @$op_op_s,
                'opopop' => @$op_op_op_s,
                'opopopop' => @$op_op_op_op_s
            );
        }


        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $price = $this->input->post('price');
        $total_price = $this->input->post('total_price');
        $notes = $this->input->post('notes');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $room = $this->input->post('room');

        $product = $this->db->select('product_id,product_name,category_id,subcategory_id,dealer_price,individual_price')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();

        $id = $product->product_id . $width . $height . $total_price . $pattern_model_id . $color_id;

        $product_name = str_replace('&quot;', '"', $product->product_name);

        $data = array(
            'id' => $id,
            'product_id' => $product->product_id,
            'room' => $room,
            'name' => $product_name,
            'price' => $total_price,
            'qty' => 1,
            'dealer_price' => $product->dealer_price,
            'individual_price' => $product->individual_price,
            'category_id' => $category_id,
            'pattern_model_id' => $pattern_model_id,
            'color_id' => $color_id,
            'width' => $width,
            'height' => $height,
            'notes' => $notes,
            'width_fraction_id' => $width_fraction_id,
            'height_fraction_id' => $height_fraction_id,
            'row_status' => 1,
            'att_options' => json_encode($attrib)
        );

        $pro = $this->cart->insert($data);
        echo 1;
    }



#---------------------------------------------
#   cart clear
#---------------------------------------------


    public function clear_cart() {

        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Clear successfully! </div>");
        redirect('new-order');
    }


#---------------------------------------------
#   cart item clear
#---------------------------------------------


    public function delete_cart_item($rowid) {

        $this->cart->remove($rowid);
        echo 1;
    }



#---------------------------------------------
#   get product comission
#---------------------------------------------
    public function getproductcomission($product_id, $customer_id) {

        $product = $this->db->select('dealer_cost_factor,individual_cost_factor')
                        ->where('product_id', $product_id)
                        ->where('customer_id', $customer_id)
                        ->get('b_cost_factor_tbl')->row();
        $commission = [];
        
        if (!empty($product)) {
            $discount_dealer_price = 100-($product->dealer_cost_factor);
            $commission = array('dealer_price' => $discount_dealer_price, 'individual_price' => $product->individual_cost_factor);
        } else {

            $product = $this->db->select('dealer_price,individual_price')
                            ->where('product_id', $product_id)
                            ->get('product_tbl')->row();

            $commission = array('dealer_price' => $product->dealer_price, 'individual_price' => $product->individual_price);
        }

        echo json_encode($commission);

    }




#---------------------------------------------
#   save order
#---------------------------------------------
    public function save_order() {

        $this->permission->check_label('order')->create()->redirect();

        $products = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $orderid = $this->input->post('orderid');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');

        $barcode_img_path = '';
        if (!empty($orderid)) {
            $this->load->library('barcode/br_code');
            $barcode_img_path = 'assets/barcode/b/' . $orderid . '.jpg';
            file_put_contents($barcode_img_path, $this->br_code->gcode($orderid));
        }



        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/file/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx|xls|xlsx';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("new-order");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {

            @$upload_file = '';
        }


        $list_price = $this->input->post('list_price');

        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $orderData = array(
            'order_id' => $orderid,
            'order_date' => $this->input->post('order_date'),
            'customer_id' => $this->input->post('customer_id'),
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $level_id,
            'side_mark' => $this->input->post('side_mark'),
            'upload_file' => $upload_file,
            'barcode' => @$barcode_img_path,
            'state_tax' => $this->input->post('tax'),
            'shipping_charges' => 0,
            'installation_charge' => $this->input->post('install_charge'),
            'other_charge' => $this->input->post('other_charge'),
            'misc' => $this->input->post('misc'),
            'invoice_discount' => $this->input->post('invoice_discount'),
            'grand_total' => $this->input->post('grand_total'),
            'subtotal' => $this->input->post('subtotal'),
            'paid_amount' => 0,
            'due' => $this->input->post('grand_total'),
            'order_status' => $this->input->post('order_status'),
            'created_by' => $this->session->userdata('user_id'),
            'updated_by' => '',
            'created_date' => date('Y-m-d'),
            'updated_date' => ''
        );





        if ($this->db->insert('b_level_quatation_tbl', $orderData)) {

            foreach ($products as $key => $product_id) {

                $productData = array(
                    'order_id' => $orderid,
                    'room' => $room[$key],
                    'product_id' => $product_id,
                    'product_qty' => $qty[$key],
                    'list_price' => $list_price[$key],
                    'discount' => $discount[$key],
                    'unit_total_price' => $utprice[$key],
                    'category_id' => $category[$key],
                    'pattern_model_id' => $pattern_model[$key],
                    'color_id' => $color[$key],
                    'width' => $width[$key],
                    'height' => $height[$key],
                    'height_fraction_id' => $height_fraction_id[$key],
                    'width_fraction_id' => $width_fraction_id[$key],
                    'notes' => $notes[$key],
                );

                $this->db->insert('b_level_qutation_details', $productData);
                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'product_attribute' => $attributes[$key]
                );

                $this->db->insert('b_level_quatation_attributes', $attrData);
            }

            // Send sms
            $this->smsSend($this->input->post('customer_id'), $orderid);


            $this->cart->destroy();

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
            redirect('b_level/invoice_receipt/receipt/' . $orderid);
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
            redirect("new-order");
        }
    }


    function smsSend($customer_id, $order_id) {

        $customer_info = $this->db->where('customer_id', $customer_id)->get('customer_info')->row();

        if ($customer_info->phone != NULL) {

            $this->load->library('twilio');

            $name = @$customer_info->first_name . ' ' . @$customer_info->last_name;

            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by',$this->level_id)->where('default_status', 1)->get()->row();

            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $customer_info->phone;
            $message = 'Hi! ' . $name . ' Order Successfully. OrderId ' . @$order_id;
            $response = $this->twilio->sms($from, $to, $message);

            return 1;
        }
    }

    // public function order_edit($order_id){
    //     $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
    //     $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);
    //     $this->load->view('b_level/header');
    //     $this->load->view('b_level/sidebar');
    //     $this->load->view('b_level/orders/order_edit',$data);
    //     $this->load->view('b_level/footer');     
    // }
    //--------------------------------------------------
    //
    //--------------------------------------------------
    
    public function set_order_stage($stage, $order_id) {

        if (!empty($stage) && !empty($order_id)) {

            $order = $this->db->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();

           
            //$this->shipping($order_id);
            $this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('b_level_quatation_tbl');
                
                $this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('quatation_tbl');
            
                    if($stage==1){$order_stage = 'Quote';}
                    if($stage==2){$order_stage = 'Paid';}
                    if($stage==3){$order_stage = 'Partially Paid';}
                    if($stage==4){$order_stage = 'Manufacturing';}
                    if($stage==5){$order_stage = 'Shipping';}
                    if($stage==6){$order_stage = 'Cancelled';}
                    if($stage==7){$order_stage = 'Delivered';}

                    // C level notification
                    $cNotificationData = array(
                        'notification_text' => 'Order Stage has been updated to '.$order_stage.' for '.$order_id,
                        'go_to_url'         => '#',
                        'created_by'        => $order->level_id,
                        'date'              => date('Y-m-d')

                    );

                    $this->db->insert('c_notification_tbl',$cNotificationData);

                    // Send sms
                    $this->Order_model->smsSend( $data = array(

                            'customer_id' => $order->customer_id,
                            'message'     => 'Order Stage has been updated to  '.$order_stage.' for '.$order_id,
                            'subject'     => 'Order payment'
                    ));
        
            $this->Order_model->send_email($order_id, $stage);

            echo 1;

        } else {
            echo 2;
        }

    }

    

    //--------------------------------------------------
    //  View SHIpment form
    //--------------------------------------------------
    public function shipment($order_id = NULL) {

        $this->permission->check_label('order')->read()->redirect();


        $shipDataCheck = $this->db->where('order_id', $order_id)->get('shipment_data')->num_rows();

        if ($shipDataCheck > 0) {

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect("b_level/order_controller/order_view/" . $order_id);

        }


        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);

        $data['methods'] = $this->db->where('status',1)->get('shipping_method')->result();
 

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/shipment_form', $data);
        $this->load->view('b_level/footer');

        
    }

    //--------------------------------------------------
    //  Shipping
    //--------------------------------------------------
    public function shipping() {


        $order_id = $this->input->post('orderid');
        $length = $this->input->post('length');
        $weight = $this->input->post('weight');
        $method_id = $this->input->post('ship_method');
        $service_type = $this->input->post('service_type');
        $customer_name = $this->input->post('customer_name');
        $shipping_address = $this->input->post('shipping_address');
        $phone = $this->input->post('phone');

        $shipDataCheck = $this->db->where('order_id', $order_id)->get('shipment_data')->num_rows();

        if ($shipDataCheck > 0) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect("b_level/order_controller/order_view/" . $order_id);
        }

        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();

        $order = $this->db->select('b_level_quatation_tbl.*,customer_info.*')
                        ->join('customer_info', 'customer_info.customer_id=b_level_quatation_tbl.customer_id')
                        ->where('order_id', $order_id)
                        ->get('b_level_quatation_tbl')->row();

        //if ($order->is_different_shipping == 1) {
        //$add = explode(',', $order->different_shipping_address);
        // if ($order->different_shipping_address != NULL) {
        //     $ShipTo = (object) [
        //         'Name'          => $order->first_name . ' ' . $order->last_name,
        //         'AttentionName' => $order->first_name . ' ' . $order->last_name,
        //         'Phone'         => $order->phone,
        //         'AddressLine'   => trim($add[0]),
        //         'City'          => trim($add[1]),
        //         'StateProvinceCode' => trim($add[2]),
        //         'PostalCode'    => trim($add[3]),
        //         'CountryCode'   => trim($add[4]),
        //     ];
        // } else {
        //     $this->session->set_flashdata('message', "<div class='alert alert-danger'>
        //     <p>Errors!</p>
        //     <p>Description : Different shipping address missing!</p>
        //     </div>");
        //     redirect("b_level/order_controller/shipment/" . $order_id);
        // }
        //} else {
        //$st = $this->db->select('shortcode')->where('state_name',@$order->state)->get('us_state_tbl')->row();
        //if ($order->address != NULL && $order->city != NULL && $order->state != NULL && $order->zip_code != NULL) {

        $addd = explode(',', $shipping_address);


        if ($addd[0] != NULL && $addd[1] != NULL && $addd[2] != NULL && $addd[3] != NULL && $addd[4] != NULL) {

            $phone = str_replace('+1', '', $phone);

            $ShipTo = (object) [
                        'Name' => $customer_name,
                        'AttentionName' => $customer_name,
                        'Phone' => $phone,
                        'AddressLine' => $addd[0],
                        'City' => $addd[1],
                        'StateProvinceCode' => $addd[2],
                        'PostalCode' => $addd[3],
                        'CountryCode' => $addd[4]
            ];
            
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : Wrong address format!</p>
                </div>");
            redirect("b_level/order_controller/shipment/" . $order_id);
        }
        //}

        if (strtolower($method->method_name) == 'ups') {

            $shiperInfo = $this->get_shiper_info($method_id, $service_type);

            $this->ups_shipping($ShipTo, $length, $weight, $shiperInfo, $order_id, $method_id);
        }


        if (strtolower($method->method_name) == 'delivery in person' || strtolower($method->method_name) == 'customer pickup') {

            $this->delivery_in_person($ShipTo, $order_id, $method_id);
        }
    }

    //--------------------------------------------------
    //  SHIper information get
    //--------------------------------------------------
    public function get_shiper_info($method_id, $service_type) {

        $company_profile = $this->settings->company_profile();
        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();


        $arrayName = array(
            'username' => $method->username,
            'password' => $method->password,
            'account_id' => $method->account_id,
            'access_token' => $method->access_token,
            'name' => $company_profile[0]->company_name,
            'attentionname' => 'Attention Name',
            'description' => 'This is test deiscription',
            'address' => $company_profile[0]->address,
            'city' => $company_profile[0]->city,
            'state' => $company_profile[0]->state,
            'zip_code' => $company_profile[0]->zip_code,
            'country_code' => $company_profile[0]->country_code,
            'phone' => $company_profile[0]->phone,
            'method_id' => $method->id,
            'mode' => $method->mode,
            'service_type' => $service_type,
            'pickup_method' => $method->pickup_method
        );

        return $arrayName;
    }

    //--------------------------------------------------
    //  UPS SHIPPIng 
    //--------------------------------------------------
    public function ups_shipping($ShipTo = null, $length = NULL, $weight = NULL, $shiperInfo = NULL, $order_id = NULL, $method_id = NULL) {


        $phone = ltrim($ShipTo->Phone,'+1 ');


        $this->load->library('Upsshipping');

        /* Ship To Address */
        $this->upsshipping->addField('ShipTo_Name', $ShipTo->Name);

        $this->upsshipping->addField('ShipTo_AddressLine', array($ShipTo->AddressLine));

        $this->upsshipping->addField('ShipTo_City', $ShipTo->City);

        $this->upsshipping->addField('ShipTo_StateProvinceCode', $ShipTo->StateProvinceCode);

        $this->upsshipping->addField('ShipTo_PostalCode', $ShipTo->PostalCode);

        $this->upsshipping->addField('ShipTo_CountryCode', $ShipTo->CountryCode);

        $this->upsshipping->addField('ShipTo_Number', $phone);

        $this->upsshipping->addField('length', $length);

        $this->upsshipping->addField('weight', $weight);

        list($response, $status) = $this->upsshipping->processShipAccept($shiperInfo);

        $ups_response = json_decode($response);

        if (isset($ups_response->ShipmentResponse) && $ups_response->ShipmentResponse->Response->ResponseStatus->Code == 1) {

            $track_number = $ups_response->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber;

            $total_charges = $ups_response->ShipmentResponse->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue;

            $graphic_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage;

            $html_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->HTMLImage;

            //graphic_image_path
            $data1g = "data:image/jpeg;base64," . $graphic_image;

            $img_name_g = $track_number . '_graphic_image.jpeg';

            $source_g = fopen($data1g, 'r');

            $graphic_image = fopen("assets/b_level/shipping_img/" . $img_name_g, "w");

            stream_copy_to_stream($source_g, $graphic_image);

            fclose($source_g);

            fclose($graphic_image);

            $graphic_image_path = 'assets/b_level/shipping_img/' . $img_name_g;

            // html image
            $data1 = "data:image/jpeg;base64," . $html_image;

            $img_name = uniqid() . $track_number . '_html_image.jpeg';

            $source = fopen($data1, 'r');

            $html_image = fopen("assets/b_level/shipping_img/" . $img_name, "w");

            stream_copy_to_stream($source, $html_image);

            fclose($source);

            fclose($html_image);

            $html_image_path = 'assets/b_level/shipping_img/' . $img_name;


            $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'grand_total'       => @$rowData->grand_total + $total_charges,
                'due'               => @$rowData->due + $total_charges,
                'shipping_charges'  => $total_charges,
                'order_stage'       => 5
            );

            //update to quatation table with pyament due 
            $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);
            $orderDATA = $this->db->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();

            $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

            $upsResponseData = array(
                'customer_name' => $ShipTo->Name,
                'customer_phone' => $ShipTo->Phone,
                'order_id' => $order_id,
                'track_number' => $track_number,
                'shipping_charges' => $total_charges,
                'graphic_image' => $graphic_image_path,
                'html_image' => $html_image_path,
                'method_id' => $method_id,
                'service_type' => @$shiperInfo['service_type'],
                'shipping_addres' => $shipping_addres
            );


            $this->db->insert('shipment_data', $upsResponseData);

            // C level notification
            $cNotificationData = array(
                'notification_text' => 'Order stage updated to shipping for ' . $order_id,
                'go_to_url' => '' ,
                'created_by' => $orderDATA->level_id,
                'date' => date('Y-m-d')
            );

            $this->db->insert('c_notification_tbl', $cNotificationData);
            //-------------------------
            //


            redirect('order-view/' . $order_id);


        } elseif ($ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode != NULL) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors code : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code . "</p>
                <p>Description : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description . "</p>
            </div>");
            redirect("b_level/order_controller/shipment/" . $order_id);
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : This is internal error!</p>
            </div>");
            redirect("b_level/order_controller/shipment/" . $order_id);
        }
    }

    //--------------------------------------------------
    //   SHIPPIng  in PERson 
    //--------------------------------------------------
    public function delivery_in_person($ShipTo, $order_id, $method_id) {

        $delivery_date = $this->input->post('delivery_date');
        $delivery_charge = $this->input->post('delivery_charge');
        $comment = $this->input->post('comment');

        $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
        // quatation table update
        $orderData = array(
            'grand_total'       => @$rowData->grand_total + @$delivery_charge,
            'due'               => @$rowData->due + @$delivery_charge,
            'shipping_charges'  => @$delivery_charge,
            'order_stage'       => 5
        );

        //update to quatation table with pyament due 
        $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);
        $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;


        $deliveryData = array(
            'customer_name' => $ShipTo->Name,
            'customer_phone' => $ShipTo->Phone,
            'order_id' => $order_id,
            'track_number' => '',
            'shipping_charges' => $delivery_charge,
            'graphic_image' => '',
            'html_image' => '',
            'method_id' => $method_id,
            'comment' => $comment,
            'delivery_date' => $delivery_date,
            'service_type' => '',
            'shipping_addres' => $shipping_addres
        );

        $this->db->insert('shipment_data', $deliveryData);

                    $orderDATA = $this->db->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();


            // C level notification
            $cNotificationData = array(
                'notification_text' => 'Order stage updated to shipping for ' . $order_id,
                'go_to_url' => '' ,
                'created_by' => $orderDATA->level_id,
                'date' => date('Y-m-d')
            );

            $this->db->insert('c_notification_tbl', $cNotificationData);
            //-------------------------
            //



        redirect('order-view/' . $order_id);
    }

    //--------------------------------------------------
    //  GET HEight WIdth fraction 
    //--------------------------------------------------
    public function get_height_width_fraction($g_val) {

        if (isset($g_val) && !empty($g_val)) {

            $hw = $this->db->select('*')
                            ->where('decimal_value >=', "0." . $g_val)
                            ->order_by('decimal_value', 'asc')
                            ->limit(1)
                            ->get('width_height_fractions')->row();

            echo $hw->id;
        } else {

            echo 0;
        }
    }
/** Start added by insys */
//=========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('b_level_quatation_tbl','id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Order has been Deleted successfully.</div>");
        }
        redirect("b-order-list");
    } 
/** End added by insys */
}
