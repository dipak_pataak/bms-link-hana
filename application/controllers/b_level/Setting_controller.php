<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
//          if ($session_id == '' || $user_type != 'b') {
//            redirect('b-level-logout');
//        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');

        $this->load->model(array(
            'b_level/settings',
            'b_level/Product_model',
            'b_level/Customer_model',
            'b_level/Order_model',
            'b_level/User_model'
        ));
        $this->load->library('twilio');
    }

    public function index() {
        
    }

    public function getCardInfo($customer_id) {

        $q = $this->db->where('customer_id', $customer_id)->get('customer_info')->row();

        $info = $this->db->where('created_by', $q->customer_user_id)->where('is_active', 1)->get('c_card_info')->row();
        echo json_encode($info);
    }

//     =========== its for profile_setting =============
    public function profile_setting() {

        $this->permission->check_label('company_profile')->create()->redirect();
        //$data['user'] = $this->settings->get_user_by_id($this->user_id);        
        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/profile_setting');
        $this->load->view('b_level/footer');
    }

//     =========== its for my_account =============
    public function my_account() {

        //$this->permission->check_label('company_profile_setting')->create()->redirect();
        //$data['user'] = $this->settings->get_user_by_id($this->user_id);        
        $data['my_account'] = $this->settings->my_account();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/my_account');
        $this->load->view('b_level/footer');
    }

//    ========== its for my_account_update ===========
    public function my_account_update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "my account information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $updatedate = date('Y-m-d');
        $user_id = $this->input->post('user_id');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $language = $this->input->post('language');

// configure for upload 
        $config = array(
            'upload_path' => "./assets/b_level/uploads/users/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'file_name' => "BMSLINK" . time(),
            'max_size' => '0',
        );
        $image_data = array();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('image')) {
            //============= its for file unlink from folder =============
            $image_unlink = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
            if ($image_unlink->user_image) {
                $img_path = FCPATH . 'assets/b_level/uploads/users/' . $image_unlink->user_image;
                unlink($img_path);
            }
            $image_data = $this->upload->data();
//                print_r($image_data); die();
            $image_name = $image_data['file_name'];
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //get original image
            $config['maintain_ratio'] = TRUE;
            $config['height'] = '*';
            $config['width'] = '*';
//                $config['quality'] = 50;
            $this->load->library('image_lib', $config);
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
        } else {
            $image_name = $this->input->post('image_hdn');
        }

        $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'user_image' => $image_name,
            'company' => $company,
            'address' => $address,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'phone' => $phone,
            'email' => $email,
            'language' => $language,
            'updated_by' => $this->user_id,
            'update_date' => $updatedate,
        );
//        echo '<pre>';        print_r($user_data);die();
        $this->db->where('id', $user_id);
        $this->db->update('user_info', $user_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>My account updated successfully!</div>");
        redirect("b-my-account");
    }

//     =========== its for mail =============
    public function mail() {
        $this->permission->check_label('email')->create()->redirect();
        $data['get_mail_config'] = $this->settings->get_mail_config();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/mail');
        $this->load->view('b_level/footer');
    }

//    ==================== its for update_mail_configure ============
    public function update_mail_configure() {
        $updated_date = date('Y-m-d');
        $protocol = $this->input->post('protocol');
        $smtp_host = $this->input->post('smtp_host');
        $smtp_port = $this->input->post('smtp_port');
        $smtp_user = $this->input->post('smtp_user');
        $smtp_pass = $this->input->post('smtp_pass');
        $mailtype = $this->input->post('mailtype');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Email configuration save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================


        $mail_config_data = array(
            'protocol' => $protocol,
            'smtp_host' => $smtp_host,
            'smtp_port' => $smtp_port,
            'smtp_user' => $smtp_user,
            'smtp_pass' => $smtp_pass,
            'mailtype' => $mailtype,
            'created_by' =>  $this->level_id,
            'updated_by' => $this->user_id,
            'updated_date' => $updated_date,
        );
        $check_exist = $this->db->where('created_by',$this->level_id)->get('mail_config_tbl')->row();
        if(!isset($check_exist->id) || empty($check_exist->id))
        {
            $this->db->insert('mail_config_tbl', $mail_config_data);
        }
        else
        {
            $this->db->where('created_by', $this->level_id);
            $this->db->update('mail_config_tbl', $mail_config_data);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mail configuration updated successfully!</div>");
        redirect("mail");
    }

//     =========== its for sms_configure =============
    public function sms_configure() {
        $this->permission->check_label('sms_configuration')->create()->redirect();
        $data['get_sms_config'] = $this->settings->get_sms_config();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/sms_configure');
        $this->load->view('b_level/footer');
    }

//    ============= its for sms_config_save =============
    public function sms_config_save() {

        $provider_name = $this->input->post('provider_name');
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $sender_name = $this->input->post('sender_name');

        $config_data = array(
            'provider_name' => $provider_name,
            'user' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'authentication' => $sender_name,
            'default_status' => 0,
            'status' => 1,
            'created_by' => $this->level_id,
            'updated_by' => $this->user_id
        );
        $this->db->insert('sms_gateway', $config_data);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "sms configuration insert";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway save successfully!</div>");
        redirect("sms-configure");
    }

//     =========== its for sms_config_edit =============
    public function sms_config_edit($gateway_id) {
        $data['sms_config_edit'] = $this->settings->sms_config_edit($gateway_id);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/sms_configure_edit');
        $this->load->view('b_level/footer');
    }

//    ================ its for sms_config_update ==============
    public function sms_config_update($gateway_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "sms configuration updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $provider_name = $this->input->post('provider_name');
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $sender_name = $this->input->post('sender_name');
        $is_active = $this->input->post('is_active');

        $config_data = array(
            'provider_name' => $provider_name,
            'user' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'authentication' => $sender_name,
            'default_status' => $is_active,
            'status' => 1,
            'updated_by' => $this->user_id
        );
        $this->db->where('gateway_id', $gateway_id);
        $this->db->update('sms_gateway', $config_data);

        $default_status = array(
            'default_status' => 0,
        );
        $this->db->where('gateway_id !=', $gateway_id);
        $this->db->where('created_by',$this->level_id);
        $this->db->update('sms_gateway', $default_status);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway updated successfully!</div>");
        redirect("sms-configure");
    }

//    ============== its for sms_config_delete ============
    public function sms_config_delete($gateway_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "sms configuration deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('gateway_id', $gateway_id);
        $this->db->delete('sms_gateway');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway deleted successfully!</div>");
        redirect("sms-configure");
    }

//     =========== its for sms =============
    public function sms() {
        $this->permission->check_label('custom_sms')->create()->redirect();
        $data['customers'] = $this->Customer_model->get_customer();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/sms');
        $this->load->view('b_level/footer');
    }

//    =========== its for sms_send ==============
    public function sms_send() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "custom sms send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $receiver = $this->input->post('receiver_id');
        $message = $this->input->post('message');
//        echo "to ". $receiver. " sms ". $message;die();

        $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by',$this->level_id)->where('default_status', 1)->get()->row();

//        echo '<pre>';        print_r($sms_gateway_info);die();
        $from = $sms_gateway_info->phone; //'+12062024567';
        $to = $receiver;
        $message = $message;

        if ($sms_gateway_info->provider_name == 'Twilio') {

            $response = $this->twilio->sms($from, $to, $message);

            if ($response->IsError) {
                echo 'Error: ' . $response->ErrorMessage;
            } else {
                $sms_data = array(
                    'from' => $from,
                    'to' => $to,
                    'message' => $message,
                    'created_by' => $this->user_id,
                );
                $this->db->insert('custom_sms_tbl', $sms_data);
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
            }
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS not send successfully!</div>");
        }
        redirect("sms");


//        if ($response->IsError)
//            echo 'Error: ' . $response->ErrorMessage;
//        else
////            echo 'Sent message to ' . $to;
//            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
//        redirect("sms");
    }

//    =========== its for sms_csv_upload ==============
    public function sms_csv_upload() {
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['to'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['message'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                }

                if ($count > 0) {

                    $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('default_status', 1)->get()->row();
//        echo '<pre>';        print_r($sms_gateway_info);die();
                    $from = $sms_gateway_info->phone; //'+12062024567';
                    $to = $this->phone_fax_mobile_no_generate($insert_csv['to']); //$insert_csv['to'];
                    $message = $insert_csv['message'];

                    if ($sms_gateway_info->provider_name == 'Twilio') {
                        $response = $this->twilio->sms($from, $to, $message);
                        if ($response->IsError) {
                            echo 'Error: ' . $response->ErrorMessage;
                        } else {
                            $sms_data = array(
                                'from' => $from,
                                'to' => $to,
                                'message' => $message,
                                'created_by' => $this->user_id,
                            );
                            $this->db->insert('custom_sms_tbl', $sms_data);
//                            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms send successfully!</div>");
                        }
                    } else {
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms not send successfully!</div>");
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms send successfully!</div>");
        redirect('sms');
    }

    //    ======== its for phone, fax and mobile no us format generate =============
    public function phone_fax_mobile_no_generate($number) {
        $first = substr($number, 0, 3);
        $second = substr($number, 3, 3);
        $third = substr($number, 6, 10);
        if ($number) {
            return "+1(" . $first . ")-" . $second . "-" . $third;
        } else {
            return 0;
        }
    }

//    =========== its for group_sms_send ============
    public function group_sms_send() {
        $customer_phone = $this->input->post('customer_phone');
        $message = $this->input->post('message');
        $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('default_status', 1)->get()->row();
//        echo '<pre>';        print_r($sms_gateway_info);die();
        $from = $sms_gateway_info->phone; //'+12062024567';
        for ($i = 0; $i < count($customer_phone); $i++) {
            $to = $customer_phone[$i];
            $message = $message;

            if ($sms_gateway_info->provider_name == 'Twilio') {
                $response = $this->twilio->sms($from, $to, $message);
                if ($response->IsError) {
                    echo 'Error: ' . $response->ErrorMessage;
                } else {
                    $sms_data = array(
                        'from' => $from,
                        'to' => $to,
                        'message' => $message,
                        'created_by' => $this->user_id,
                    );
                    $this->db->insert('custom_sms_tbl', $sms_data);
                    $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                }
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS not send successfully!</div>");
            }
        }
        redirect('sms');
    }

//     =========== its for gateway =============
    public function gateway() {


        $this->permission->check_label('paypal_settings')->create()->redirect();

        $total_row = $this->db->where('created_by',$this->level_id)->count_all_results('gateway_tbl');

        $config["base_url"] = base_url('b_level/Setting_controller/gateway');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["gateway_list"] = $this->settings->gateway_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/gateway', $data);
        $this->load->view('b_level/footer');
    }

//    ============== its for save_gateway ==================
    public function save_gateway() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $createdate = date('Y-m-d');
        $payment_gateway = $this->input->post('payment_gateway');
        $payment_mail = $this->input->post('payment_mail');
        $currency = $this->input->post('currency');
        $mode = $this->input->post('mode');

        $gateway_data = array(
            'payment_gateway' => $payment_gateway,
            'payment_mail' => $payment_mail,
            'currency' => $currency,
            'level_type' => 'b',
            'created_by' => $level_id,
            'status' => $mode,
            'created_date' => $createdate,
        );
        $check_gateway = $this->db->where('created_by', $level_id)->where('level_type', 'b')->get('gateway_tbl')->result();
        if ($check_gateway) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway configuration already done!</div>");
            redirect("gateway");
        } else {
            $this->db->insert('gateway_tbl', $gateway_data);
        }

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "payment gateway setting save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway save successfully!</div>");
        redirect("gateway");
    }

//    ============= its for gateway_edit =============
    public function gateway_edit($id) {

        $data['gateway_edit'] = $this->settings->gateway_edit($id);
//        dd($data['gateway_edit']);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/gateway_edit');
        $this->load->view('b_level/footer');
    }

    public function tms_gateway_edit() {

        $data['gateway_edit'] = $this->db->where('level_id', $this->level_id)->get('tms_payment_setting')->row();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/tms_gateway_edit');
        $this->load->view('b_level/footer');
    }

    public function update_tms_payment_setting() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $gatewaydata = array(
            'url' => $this->input->post('url'),
            'user_name' => $this->input->post('user_name'),
            'password' => $this->input->post('password'),
            'mode' => 0,
            'level_id' => $level_id,
        );
        $gateway_check = $this->db->where('level_id', $level_id)->get('tms_payment_setting')->row();
        if ($gateway_check) {
            $this->db->where('level_id', $level_id)->update('tms_payment_setting', $gatewaydata);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway updated successfully!</div>");
        } else {
            $this->db->insert('tms_payment_setting', $gatewaydata);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway save successfully!</div>");
        }
        redirect("b_level/Setting_controller/tms_gateway_edit");
    }

//    ============== its for update-gateway =============
    public function update_gateway($id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $updatedate = date('Y-m-d');
        $payment_gateway = $this->input->post('payment_gateway');
        $payment_mail = $this->input->post('payment_mail');
        $currency = $this->input->post('currency');
        $is_active = $this->input->post('is_active');
        $mode = $this->input->post('mode');

        $gateway_data = array(
            'payment_gateway' => $payment_gateway,
            'payment_mail' => $payment_mail,
            'currency' => $currency,
            'level_type' => 'b',
            'default_status' => $is_active,
            'status' => $mode,
            'updated_by' => $level_id,
            'updated_date' => $updatedate
        );

//        echo '<pre>';        print_r($category_data);die();
        $this->db->where('id', $id);
        $this->db->update('gateway_tbl', $gateway_data);

        $default_status = array(
            'default_status' => 0,
        );
        $this->db->where('id !=', $id);
        $this->db->where('created_by', $level_id);
        $this->db->update('gateway_tbl', $default_status);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "payment gateway updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway updated successfully!</div>");
        redirect("gateway");
    }

//    ============== its for gateway_delete==============
    public function gateway_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "payment gateway deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('id', $id)->delete('gateway_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway deleted successfully!</div>");
        redirect("gateway");
    }

//     =========== its for change_password =============
    public function change_password() {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/change_password');
        $this->load->view('b_level/footer');
    }

//    ============== its for update_password ============
    public function update_password() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "password change successfully";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $user_id = $this->input->post('user_id');
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $old_password_check = $this->db->select('password')->from('log_info')
                        ->where('password', md5($old_password))
                        ->where('user_id', $user_id)->get()->result();
//        echo '<pre>'; print_r($old_password_check);
        if (!empty($old_password_check)) {
            $change_details = array(
                'password' => md5($new_password),
            );
            $this->db->where('user_id', $user_id);
            $this->db->update('log_info', $change_details);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Password changed successfully!</div>");
            redirect("b-change-password");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Old password does not match!</div>");
            redirect("b-change-password");
        }
    }

//     =========== its for new_user =============
    public function create_user() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/new_user');
//        $this->load->view('b_level/footer');
    }

//======== its for exists get_check_user_unique_email  ===========
    public function get_check_user_unique_email() {
        $email = $this->input->post('email');
        $query = $this->db->select('*')->from('user_info')->where('email', $email)
                        ->get()->row();
//        echo '<pre>';        print_r($query);die();
        if (!empty($query)) {
            echo $query->email;
        } else {
            echo 0;
        }
    }

    /*
      |--------------------------------------------------------
      |       save new user in database
      |--------------------------------------------------------
     */

    public function save_user() {
        $this->form_validation->set_rules('first_name', 'Firstname', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Lastname', 'required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        $this->form_validation->set_rules('address', 'Address', 'max_length[1000]');

        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');
        $fixed_commission = $this->input->post('fixed_commission');
        $percentage_commission = $this->input->post('percentage_commission');

        // if (@$_FILES['user_img']['name']) {
        //     $config['upload_path']   = 'assets/b_level/img/';
        //     $config['allowed_types'] = 'jpg|jpeg|png';
        //     $config['overwrite']     = false;
        //     $config['max_size']      = 6200;
        //     $config['remove_spaces'] = true;
        //     $config['max_filename']   = 10;
        //     $config['file_ext_tolower'] = true;
        //     $this->upload->initialize($config);
        //     $this->load->library('upload', $config);
        //     if (!$this->upload->do_upload('user_img'))
        //     {
        //        $error = $this->upload->display_errors();
        //        $this->session->set_flashdata('exception',$error);
        //        redirect("profile-setting");
        //     } else {
        //      $data = $this->upload->data();
        //      $image = $config['upload_path'].$data['file_name'];
        //     }
        //     } else {
        //         $image = "";
        //     }
        //=========== its for save user info ============
        $user = (object) $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'address' => $address,
            'language' => 'English',
            'phone' => $phone,
            'created_by' => $this->user_id,
            'user_type' => 'b',
            'fixed_commission' => $fixed_commission,
            'percentage_commission' => $percentage_commission,
            'create_date' => $created_date
        );

        if ($this->form_validation->run()) {

            $this->db->insert('user_info', $user_data);
            $user_id = $this->db->insert_id();

            // =========== its for save log info ============
            $loginfo = array(
                'user_id' => $user_id,
                'email' => $email,
                'password' => md5($password),
                'user_type' => 'b',
                'is_admin' => 2,
            );
            $this->db->insert('log_info', $loginfo);
//        =========== its for customer user info send by email ============     
            if ($email) {
                $data['get_mail_config'] = $this->settings->get_mail_config();
                $this->User_model->sendLink($user_id, $data, $email, $password);
            }


            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "insert";
            $remarks = "user information save";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User save successfully!</div>");
            redirect('b-level-users');
        } else {

            $data['user'] = $user;
            $this->load->view('b_level/header', $data);
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/settings/new_user');
            $this->load->view('b_level/footer');
        }
    }

    /*
      |-------------------------------------------------------
      |   VIEW EDIT USER
      |-------------------------------------------------------
     */

    public function edit_user($user_id) {

        $data['user'] = $this->settings->get_user_by_id($user_id);
        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/edit_user');
        $this->load->view('b_level/footer');
    }

    /*
      |------------------------------------------------------
      |   UPDATE USER
      |------------------------------------------------------
     */

    public function update_user() {
        // $this->form_validation->set_rules('first_name', 'Firstname','required|max_length[50]');
        // $this->form_validation->set_rules('last_name', 'Lastname','required|max_length[50]');
        // $this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[log_info.email]|max_length[100]');
        // $this->form_validation->set_rules('password', 'Password','required|max_length[32]');
        // $this->form_validation->set_rules('address', 'Address','max_length[1000]');

        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
//        if($password == NULL){
//            echo "disi";
//        }else{
//            echo "nai";
//        }die();
         $oldpass = $this->input->post('oldpassword');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');

        $user_id = $this->input->post('user_id');
        $redirect_url = $this->input->post('redirect_url');

        $fixed_commission = $this->input->post('fixed_commission');
        $percentage_commission = $this->input->post('percentage_commission');


        // $config['upload_path']          = './assets/';
        // $config['allowed_types']        = 'gif|jpg|png';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        // $this->load->library('upload', $config);
        // if ( ! $this->upload->do_upload('userfile'))
        // {
        //        $this->session->set_flashdata('exception', "<div class='alert alert-danger'>
        //     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
        //     ". $this->upload->display_errors()."!</div>");
        //     redirect($redirect_url);
        // }
        // else
        // {
        //         $data = array('upload_data' => $this->upload->data());
        //         print_r($data); exit;
        // }
        //=========== its for save user info ============
        $user = (object) $user_data = array(
            'id' => $user_id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'user_image' => '', //@$image,
            'email' => $email,
            'address' => $address,
            'phone' => $phone,
            'created_by' => $this->user_id,
            'user_type' => 'b',
            'fixed_commission' => $fixed_commission,
            'percentage_commission' => $percentage_commission,
            'create_date' => $created_date
        );


        $this->db->where('id', $user_id)->update('user_info', $user_data);
//        (!empty($this->input->post('password'))?$npassword:$oldpass),
        // =========== its for save log info ============(!empty($npassword)?$npassword:$oldpass),
        $loginfo = array(
            'user_id' => $user_id,
            'email' => $email,
//            'password' => md5($password),
            'password' => (!empty($this->input->post('password'))?$password:$oldpass),
            'user_type' => 'b',
            'is_admin' => 2,
        );
        $this->db->where('user_id', $user_id)->update('log_info', $loginfo);


        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "user information updated ";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        if ($email) {
            $data['get_mail_config'] = $this->settings->get_mail_config();
            $this->User_model->sendLink($user_id, $data, $email, $password);
        }

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
            User update successfully!</div>");

        redirect('b-level-users');
    }

    /*
      |---------------------------------------------------------
      |   USER LIST
      |---------------------------------------------------------
     */

    public function b_level_users() {
        $this->permission->check_label('employees')->create()->redirect();
        $limit = 25;
        @$start = ($this->uri->segment(2) ? $this->uri->segment(2) : 0);

        $config = $this->pasination($limit, 'user_info', 'b-level-users');

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['users'] = $this->settings->get_userlist($limit, $start);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/b_level_users');
        $this->load->view('b_level/footer');
    }

    public function pasination($limit, $tbl, $url) {

        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        $this->db->where('user_type', 'b');
        $this->db->where('created_by!=', NULL);
        $query = $this->db->get();

        $total_rows = $query->num_rows();


        $config["base_url"] = base_url($url);
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $limit;
        // $config['suffix'] = '?'.http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'];
        // integrate bootstrap pagination
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        return $config;
    }

//    ================ its for company_profile_update ===================
    public function company_profile_update() {
        $company_name = $this->input->post('company_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
//        $password = $this->input->post('password');
//        $image = $this->input->post('image');
        $currency = $this->input->post('currency');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "Company profile information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
//        if ($password) {
//            $password_change = array(
//                'password' => md5($password)
//            );
//            $this->db->where('user_id', $this->user_id);
//            $this->db->update('log_info', $password_change);
//        }
//        ========== its for logo =============
// configure for upload 
        $config = array(
            'upload_path' => "./assets/b_level/uploads/appsettings/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
//            'file_name' => "SPF" . time(),
            'max_size' => '0',
        );
        $image_data = array();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('image')) {
            $image_data = $this->upload->data();
//                print_r($image_data); die();
            $image_name = $image_data['file_name'];
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //get original image
            $config['maintain_ratio'] = TRUE;
            $config['height'] = '*';
            $config['width'] = '*';
//                $config['quality'] = 50;
            $this->load->library('image_lib', $config);
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
        } else {
            $image_name = $this->input->post('image_hdn');
        }
//            ========= its for logo upload =========

        if ($this->upload->do_upload('logo')) {
            $apps_logo = $this->upload->data();
//              echo '<pre>';  print_r($image_dataTwo); die();
            $logo = $apps_logo['file_name'];
        } else {
            $logo = $this->input->post('logo_hdn');
        }
//            ============ close logo upload ===========

        $web_setting = array(
            'company_name' => $company_name,
            'email' => $email,
            'phone' => $phone,
            'currency' => $currency,
            'picture' => $image_name,
            'logo' => $logo,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,

            'updated_by' => $this->user_id,
        );
        $check = $this->db->where('user_id',$this->user_id)->get('company_profile')->row();
        if(empty($check))
        {
            $web_setting['created_by'] = $this->user_id;
            $web_setting['user_id'] = $this->user_id;
            $this->db->insert('company_profile', $web_setting);
        }
        else
        {

//        dd($web_setting);
            $this->db->where('user_id', $this->user_id);
            $this->db->update('company_profile', $web_setting);
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Company profile updated successfully!</div>");
        redirect('profile-setting');
    }

//============ its for unit of measurement ============
    public function uom_list() {

        $this->permission->check_label('unit_of_measurement')->create()->redirect();

        $config["base_url"] = base_url('b_level/Setting_controller/uom_list');
        $config["total_rows"] = $this->db->count_all('unit_of_measurement');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["uom_list"] = $this->settings->uom_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/uom', $data);
        $this->load->view('b_level/footer');
    }

//    =========== its for uom_save ===============
    public function uom_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "unit of measurement insert done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $uom_name = $this->input->post('uom_name');
        $status = $this->input->post('status');
        $uom_data = array(
            'uom_name' => $uom_name,
            'status' => $status
        );
        $this->db->insert('unit_of_measurement', $uom_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Unit of measurement save successfully!</div>");
        redirect('uom-list');
    }

//    ============ its for uom_edit ==============
    public function uom_edit($id) {
        $data['uom_edit'] = $this->settings->uom_edit($id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/uom_edit', $data);
        $this->load->view('b_level/footer');
    }

//    =========== its for uom_update ===============
    public function uom_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "update";
        $remarks = "unit of measurement updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $uom_name = $this->input->post('uom_name');
        $status = $this->input->post('status');
        $uom_data = array(
            'uom_name' => $uom_name,
            'status' => $status,
        );
        $this->db->where('uom_id', $id);
        $this->db->update('unit_of_measurement', $uom_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Unit of measurement updated successfully!</div>");
        redirect('uom-list');
    }

//    ============ its for uom_delete =============
    public function uom_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "unit of measurement deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('uom_id', $id)->delete('unit_of_measurement');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Unit of measurement deleted successfully!</div>");
        redirect('uom-list');
    }

    public function own_user_usstate_count($level_id, $user_id) {
        if ($user_id == $level_id) {
            $filter = "level_id=" . $level_id;
        } else {
            $filter = "created_by=" . $user_id;
        }
        $this->db->where($filter);
        $num_rows = $this->db->count_all_results('customer_info');
        return $num_rows;
    }

//    ============ its for us_state ============
    public function us_state() {
        $this->permission->check_label('state_tax')->create()->redirect();

        $data['get_city_state'] = $this->db->select('*')->from('city_state_tbl')->group_by('state_name')->get()->result();
        $config["base_url"] = base_url('b_level/Setting_controller/us_state');
        $config["total_rows"] = $this->own_user_usstate_count($this->level_id, $this->user_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["us_state_list"] = $this->settings->us_state_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
//        echo '<pre>';        print_r($data["pagenum"]);die();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/us_state');
        $this->load->view('b_level/footer');
    }

//    ============= its for us_state_save ==========
    public function us_state_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "us state information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $short_code = $this->input->post('short_code');
        $state_name = $this->input->post('state_name');
        $tax_rate = $this->input->post('tax_rate');

        $us_state_data = array(
            'shortcode' => $short_code,
            'state_name' => $state_name,
            'tax_rate' => $tax_rate,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
        );
        $this->db->insert('us_state_tbl', $us_state_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State save successfully!</div>");
        redirect('us-state');
    }

//    ============= its for us_state_edit ============
    public function us_state_edit($id) {
        $data['get_city_state'] = $this->db->select('*')->from('city_state_tbl')->group_by('state_name')->get()->result();
        $data['us_state_edit'] = $this->settings->us_state_edit($id);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/us_state_edit');
        $this->load->view('b_level/footer');
    }

//    ========== its for us_state_update ===========
    public function us_state_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "us state updated done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $short_code = $this->input->post('short_code');
        $state_name = $this->input->post('state_name');
        $tax_rate = $this->input->post('tax_rate');

        $us_state_data = array(
            'shortcode' => $short_code,
            'state_name' => $state_name,
            'tax_rate' => $tax_rate,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
        );
        $this->db->where('state_id', $id);
        $this->db->update('us_state_tbl', $us_state_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State updated successfully!</div>");
        redirect('us-state');
    }

//    ============ its for us_state_delete =============
    public function us_state_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "us state information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('state_id', $id);
        $this->db->delete('us_state_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State deleted successfully!</div>");
        redirect('us-state');
    }

//    ============== its for =============/
    public function import_us_state_save() {
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['shortcode'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['state_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['tax_rate'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                }
                $data = array(
                    'shortcode' => $insert_csv['shortcode'],
                    'state_name' => $insert_csv['state_name'],
                    'tax_rate' => $insert_csv['tax_rate'],
//                    'created_by' => $this->user_id,
//                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
//                echo $data['email'];
                    $result = $this->db->select('*')
                            ->from('us_state_tbl')
                            ->where('shortcode', $data['shortcode'])
                            ->or_where('state_name', $data['color_number'])
                            ->get()
                            ->num_rows();
//                echo '<pre>';                print_r($result);die();
                    if ($result == 0 && !empty($data['shortcode'])) {
                        $this->db->insert('us_state_tbl', $data);
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State imported successfully!</div>");
                    } else {
                        $data = array(
//                            'customer_id' => $insert_csv['customer_id'],
                            'shortcode' => $insert_csv['shortcode'],
                            'state_name' => $insert_csv['state_name'],
                            'tax_rate' => $insert_csv['tax_rate'],
//                            'updated_by' => $this->user_id,
//                            'updated_date' => date('Y-m-d'),
                        );
                        $this->db->where('shortcode', $data['shortcode']);
                        $this->db->update('us_state_tbl', $data);
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State import updated successfully!</div>");
//                        redirect('customer-import');
                    }
                }
                $count++;
            }
        }
//        echo "DUke Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
//        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State imported successfully!</div>");
        redirect('us-state');
    }

//    ============== its for city_state_wise_statename ===========
    public function city_statename_wise_stateid() {
        $state_name = $this->input->post('state_name');
        $get_stateid = $this->db->where('state_name', $state_name)->group_by('state_name')->get('city_state_tbl')->result();
        echo json_encode($get_stateid);
    }

//    ============ its for b_level_usstate_search ==========
    public function b_level_usstate_search($keyword) {
        $data['us_state_list'] = $this->Settings->get_us_state_search_result($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/customers/customer_search', $data);
    }

//====== its for delete_user =============
    public function delete_user($id) {
        $this->db->where('user_id', $id)->delete('log_info');
        $this->db->where('id', $id)->delete('user_info');
        $this->db->where('user_id', $id)->delete('b_user_access_tbl');
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "user information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================       
        
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User deleted successfully!</div>");
        redirect('b-level-users');
    }

//    ============ its for user_filter ===========
    public function user_filter() {
        $data['first_name'] = $this->input->post('first_name');
        $data['email'] = $this->input->post('email');
        $data['type'] = $this->input->post('type');
        $data['get_user_filter_data'] = $this->settings->user_filter($data['first_name'], $data['email'], $data['type']);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/user_filter');
        $this->load->view('b_level/footer');
    }

//    ===============its for shipping ===============
    public function shipping() {

        $this->permission->check_label('shipping')->create()->redirect();

        $data['lists'] = $this->db->select('*')->where('created_by',$this->level_id)->get('shipping_method')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/condition/shipping', $data);
        $this->load->view('b_level/footer');
    }

    public function save_shipping_method() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Shipping method insert";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $methodData = array(
            'method_name' => $this->input->post('method_name'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'account_id' => $this->input->post('account_id'),
            'access_token' => $this->input->post('access_token'),
            'created_by' => $this->level_id,
            'updated_by' => $this->user_id,
        );

        $check = $this->db->where('method_name', $this->input->post('method_name'))->get('shipping_method')->row();

        if (!empty($check)) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> This shipping method already exists</div>");
            redirect("shipping");
        } else {

            $this->db->insert('shipping_method', $methodData);

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Shipping method save successfully</div>");
            redirect("shipping");
        }
    }

//    ===============its for shipping_edit ===============
    public function shipping_edit($id) {

        $data['method'] = $this->db->where('id', $id)->get('shipping_method')->row();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/condition/shipping_edit', $data);
        $this->load->view('b_level/footer');
    }

    public function update_shipping_method() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "Updated";
        $remarks = "Updated shipping method";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $methodData = array(
            'method_name' => $this->input->post('method_name'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'account_id' => $this->input->post('account_id'),
            'access_token' => $this->input->post('access_token'),
            'service_type' => $this->input->post('service_type'),
            'pickup_method' => $this->input->post('pickup_method'),
            'mode' => $this->input->post('mode'),
            'status' => $this->input->post('status')
        );
        $id = $this->input->post('id');



        $this->db->where('id', $id)->update('shipping_method', $methodData);

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Shipping method update successfully</div>");
//        redirect("shipping-edit/" . $id);
        redirect("shipping/");
    }

//    ===============its for shipping_edit ===============
    public function shipping_method_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "shipping method deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->db->where('id', $id)->delete('shipping_method');
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Shipping method delete successfully</div>");
        redirect("shipping");
    }

//     =========== its for cost_factor =============
    public function cost_factor() {

        $data['get_only_product'] = $this->Product_model->get_only_product_b_cost_factor();
        $data['get_customer'] = $this->Order_model->get_customer();

        $data['costfactor'] = $this->db->select('b_cost_factor_tbl.*,
            product_tbl.product_name,
            customer_info.first_name,
            customer_info.last_name
            ')
                        ->join('product_tbl', 'product_tbl.product_id=b_cost_factor_tbl.product_id')
                        ->join('customer_info', 'customer_info.customer_id=b_cost_factor_tbl.customer_id')
                        ->where('b_cost_factor_tbl.created_by',$this->level_id)
                        ->get('b_cost_factor_tbl')->result();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/cost_factor');
        $this->load->view('b_level/footer');
    }

//    ============== its for b-level cost_factor_save =============
    public function cost_factor_save() {

        $customer_id = $this->input->post('customer_id');


        $product_id = $this->input->post('product_id');

        $dealer_cost_factor = $this->input->post('dealer_cost_factor');

        $individual_cost_factor = $this->input->post('individual_cost_factor');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);

        $action_done = "insert";

        $remarks = "cost factor information save";

        //checking
        //--------------
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        //$this->db->empty_table('b_cost_factor_tbl');

        foreach ($customer_id as $key => $c_id) {

            $this->db->where('customer_id', $c_id)->delete('b_cost_factor_tbl');
            $cost_factor_data = [];

            foreach ($product_id as $key => $pid) {

                $cost_factor_data[] = array(
                    'customer_id' => $c_id,
                    'product_id' => $pid,
                    'dealer_cost_factor' => $dealer_cost_factor,
                    'created_by' => $this->user_id,
                    'create_date' => date('Y-m-d')
                );
            }

            $this->db->insert_batch('b_cost_factor_tbl', $cost_factor_data);
        }


//      =========== its for product condition mapping ==============
        // for ($i = 0; $i < count($product_id); $i++) {
        //     $cost_factor_data = array(
        //         'product_id' => $product_id[$i],
        //         'dealer_cost_factor' => $dealer_cost_factor[$i],
        //         'individual_cost_factor' => $individual_cost_factor[$i],
        //         'created_by' => $this->user_id,
        //         'create_date' => date('Y-m-d'),
        //     );
        //     $this->db->insert('b_cost_factor_tbl', $cost_factor_data);
        // }
//            =========== close product condition mapping ============
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cost factor updated successfully</div>");
        redirect("b-cost-factor");
    }

    public function delete_cust_factor($id) {

        $this->db->where('id', $id)->delete('b_cost_factor_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cost factor delete successfully</div>");
        redirect("b-cost-factor");
    }

    public function edit_cust_factor($id) {

        $data['get_only_product'] = $this->Product_model->get_only_product_b_cost_factor();
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['data'] = $this->db->where('id', $id)->get('b_cost_factor_tbl')->row();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/edit_cost_factor');
        $this->load->view('b_level/footer');
    }

    public function update_cost_factor() {

        $cost_factor_data = array(
            'customer_id' => $this->input->post('customer_id'),
            'product_id' => $this->input->post('product_id'),
            'dealer_cost_factor' => $this->input->post('dealer_cost_factor'),
            //'individual_cost_factor' => $individual_cost_factor,
            'created_by' => $this->user_id,
            'create_date' => date('Y-m-d'),
        );

        $this->db->where('id', $this->input->post('id'))->update('b_cost_factor_tbl', $cost_factor_data);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cost factor update successfully</div>");
        redirect("b-cost-factor");
    }

//    ============ its for iframe_code_generate ==========
    public function iframe_code_generate() {
//        $data['iframe_code_generate'] = $this->Setting_model->iframe_code_generate();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/iframe_code');
        $this->load->view('b_level/footer');
    }

    public function b_customer_iframe_view() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/b_customer_iframe_view');
//        $this->load->view('b_level/footer');
    }

    public function d_customer_iframe_generate($user_id, $user_type) {

        $this->load->view('d_level/b_d_customer_form');
    }

//    ================== its for b_d_customer_info_save ============
    public function b_d_customer_info_save() {
//        $action_page = $this->uri->segment(1);
//        $action_done = "insert";
//        $remarks = "Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');
        $reference = $this->input->post('reference');
        $user_id = $this->input->post('user_id');
        $level_type = $this->input->post('level_type');
        $level_id = $user_id;
//        if ($this->session->userdata('isAdmin') == 1) {
//            $level_id = $this->session->userdata('user_id');
//        } else {
//            $level_id = $this->session->userdata('user_created_by');
//        }
//        dd($level_id);
        $email_check = $this->db->select('email')->from('customer_info')->where('email', $email)->get()->result();
        if ($email_check) {
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This email already exists!</div>");
            redirect($_SERVER['HTTP_REFERER']);
        } else {

//        ============ its for accounts coa table ===============
            $coa = $this->Customer_model->headcode();
            if ($coa->HeadCode != NULL) {
                $headcode = $coa->HeadCode + 1;
            } else {
                $headcode = "102030101";
            }

            $lastid = $this->db->select("*")->from('customer_info')
                    ->order_by('customer_no', 'desc')
                    ->get()
                    ->row();
//        dd($lastid);
            $sl = $lastid->customer_no;
            if (empty($sl)) {
                $sl = "cus-0001";
            } else {
                $sl = $sl;
            }
//            echo $sl;die();
            $supno = explode('-', $sl);
            $nextno = $supno[1] + 1;
            $si_length = strlen((int) $nextno);

            $str = '0000';
            $cutstr = substr($str, $si_length);
            $sino = $supno[0] . "-" . $cutstr . $nextno;

//        $customer_name = $this->input->post('customer_name');
            $customer_no = $sino . '-' . $first_name . " " . $last_name;
//        ================= close =======================
//        ======== its for customer COA data array ============
            $customer_coa = array(
                'HeadCode' => $headcode,
                'HeadName' => $customer_no,
                'PHeadName' => 'Customer Receivable',
                'HeadLevel' => '4',
                'IsActive' => '1',
                'IsTransaction' => '1',
                'IsGL' => '0',
                'HeadType' => 'A',
                'IsBudget' => '0',
                'IsDepreciation' => '0',
                'DepreciationRate' => '0',
                'CreateBy' => $user_id,
                'CreateDate' => $created_date,
            );
//        dd($customer_coa);
            $this->db->insert('acc_coa', $customer_coa);
//        ======= close ==============

            $customer_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'phone' => $phone,
                'company' => $company,
                'customer_no' => $customer_no,
                'customer_type' => 'personal',
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'street_no' => $street_no,
                'side_mark' => $side_mark,
                'reference' => $reference,
                'level_id' => $level_id,
                'created_by' => $user_id,
                'create_date' => $created_date,
            );
            $this->db->insert('customer_info', $customer_data);
//        ============ its for access log info collection ===============
//        $accesslog_info = array(
//            'action_page' => $action_page,
//            'action_done' => $action_done,
//            'remarks' => $remarks,
//            'user_name' => $this->user_id,
//            'entry_date' => date("Y-m-d H:i:s"),
//        );
//        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info save successfully!</div>");
        redirect($_SERVER['HTTP_REFERER']);
    }

//    ==========its for own user customer count =========
    public function own_access_log_count() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->where('user_name', $level_id);
        $num_rows = $this->db->count_all_results('accesslog');
        return $num_rows;
    }

//============= its for logs =============
    public function access_logs() {
        $config["base_url"] = base_url('b_level/Setting_controller/access_logs');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_access_log_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["access_logs"] = $this->settings->access_logs($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/logs');
        $this->load->view('b_level/footer');
    }
//    ============== its for b-level cost_factor_save =============
    public function save_cost_factor() {
        $customer_id = $this->input->post('Id_List');
        $dealer_cost_factor = $this->input->post('txt_dealer_cost_factor');

        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "cost factor information save";
        //--------------
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================

        foreach ($customer_id as $key => $c_id) {
            $data = [
                'dealer_cost_factor' => $dealer_cost_factor,
            ];
            $this->db->where('id', $c_id);
            $this->db->update('b_cost_factor_tbl', $data);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cost factor updated successfully</div>");
        redirect("b-cost-factor");
    }
}
