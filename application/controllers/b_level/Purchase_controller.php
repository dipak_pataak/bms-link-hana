<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Supplier_model');
        $this->load->model('b_level/User_model');
        $this->load->model('b_level/purchase_model');
        $this->load->model('b_level/pattern_model');
        $this->load->model('common_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    =============== its for purchase_entry ==============
    public function purchase_entry() {
        $this->permission->check_label('raw_material_purchase')->create()->redirect();

        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $data['rmtts'] = $this->purchase_model->get_row_material_list();
        $data['colors'] = $this->db->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['patern_model'] = $this->pattern_model->get_patern_model();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/purchase/purchase_entry', $data);
        $this->load->view('b_level/footer');
    }

    public function purchase_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "purchase information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $purchase_id = time();
        $supplier_id = $this->input->post('supplier_id');
        $date = $this->input->post('date');
        $material = $this->input->post('rmtt_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');

        $quantity = $this->input->post('product_quantity');

        $purchase_price = $this->input->post('product_rate');
        $unit_total_price = $this->input->post('unit_total_price');
        $discount = $this->input->post('product_discount');

        $total_discount = $this->input->post('total_discount');
        $invoice_discount = $this->input->post('invoice_discount');

        $total_discount = $this->input->post('total_discount');
        $grand_total_price = $this->input->post('grand_total_price');
        $paid_amount = $this->input->post('paid_amount');
        $paid_amount = $this->input->post('paid_amount');
        $due_amount = $this->input->post('due_amount');
        $payment_type = $this->input->post('payment_type');
        $check_no = $this->input->post('check_no');
        $bank_branch_name = $this->input->post('branch_name');
        $account_no = $this->input->post('ac_no');
        $purchase_description = $this->input->post('purchase_description');


        $purchaseData = array(
            'purchase_id' => $purchase_id,
            'supplier_id' => $supplier_id,
            'date' => $date,
            'invoice_discount' => $invoice_discount,
            'total_discount' => $total_discount,
            'grand_total' => $grand_total_price,
            'paid_amount' => $paid_amount,
            'due_amount' => $due_amount,
            'payment_type' => $payment_type,
            'check_no' => @$check_no,
            'bank_branch_name' => @$bank_branch_name,
            'account_no' => @$account_no,
            'purchase_description' => @$purchase_description,
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d'),
        );


        if ($this->db->insert('purchase_tbl', $purchaseData)) {

            $i = 0;
            $purchaseDetails = [];

            foreach ($material as $key => $material_id) {

                $purchaseDetails[] = $stock = array(
                    'purchase_id' => $purchase_id,
                    'material_id' => $material_id,
                    'pattern_model_id' => $pattern_model_id[$i],
                    'color_id' => $color_id[$i],
                    'purchase_price' => $purchase_price[$i],
                    'quantity' => $quantity[$i],
                    'discount' => $discount[$i],
                    'created_by' => $this->session->userdata('user_id'),
                    'created_date' => date('Y-m-d'),
                );

                $i++;
                $this->row_material_stock_save($stock);
            }

            $this->db->insert_batch('purchase_details_tbl', $purchaseDetails);
        }


        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
             Purchase save successfull </div>");

        redirect('purchase-entry');
    }

    public function row_material_stock_save($data) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "raw material stock information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $stockData = array(
            'row_material_id' => $data['material_id'],
            'pattern_model_id' => $data['pattern_model_id'],
            'color_id' => $data['color_id'],
            'in_qty' => $data['quantity'],
            'from_module' => 'purchase in',
            'measurment' => '',
            'stock_date' => date('Y-m-d')
        );
        $this->db->insert('row_material_stock_tbl', $stockData);
        return 1;
    }

    public function purchase_list() {
        $this->permission->check_label('raw_material')->create()->redirect();
        $data['purchases'] = $this->purchase_model->get_purchase_list();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/purchase/purchase_list', $data);
        $this->load->view('b_level/footer');
    }

    public function delete_purchase($purchase_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "purchase information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('purchase_id', $purchase_id)->delete('purchase_tbl');
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Purchase delete successfull </div>");

        redirect('purchase-list');
    }

    public function edit_purchase($purchase_id) {
        $data['purchase'] = $this->purchase_model->get_purchase_by_id($purchase_id);
        $data['purchase_details'] = $this->purchase_model->get_purchase_details_by_id($purchase_id);

        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $data['rmtts'] = $this->purchase_model->get_row_material_list();
        $data['colors'] = $this->db->get('color_tbl')->result();
        $data['patern_model'] = $this->pattern_model->get_patern_model();

        $data['payment_types'] = $this->common_model->payment_type();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/purchase/edit_purchase', $data);
        $this->load->view('b_level/footer');
    }

    public function update_purchase() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "raw material information update";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $purchase_id = $this->input->post('purchase_id');
        $supplier_id = $this->input->post('supplier_id');
        $date = $this->input->post('date');
        $material = $this->input->post('rmtt_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');

        $quantity = $this->input->post('product_quantity');

        $purchase_price = $this->input->post('product_rate');
        $unit_total_price = $this->input->post('unit_total_price');
        $discount = $this->input->post('product_discount');

        $total_discount = $this->input->post('total_discount');
        $invoice_discount = $this->input->post('invoice_discount');

        $total_discount = $this->input->post('total_discount');
        $grand_total_price = $this->input->post('grand_total_price');
        $paid_amount = $this->input->post('paid_amount');
        $due_amount = $this->input->post('due_amount');
        $due_amount = $this->input->post('due_amount');
        $payment_type = $this->input->post('payment_type');
        $check_no = $this->input->post('check_no');
        $bank_branch_name = $this->input->post('branch_name');
        $account_no = $this->input->post('ac_no');
        $purchase_description = $this->input->post('purchase_description');


        $purchaseData = array(
            'purchase_id' => $purchase_id,
            'supplier_id' => $supplier_id,
            'date' => $date,
            'invoice_discount' => $invoice_discount,
            'total_discount' => $total_discount,
            'grand_total' => $grand_total_price,
            'paid_amount' => $paid_amount,
            'due_amount' => $due_amount,
            'payment_type' => $payment_type,
            'check_no' => @$check_no,
            'bank_branch_name' => @$bank_branch_name,
            'account_no' => @$account_no,
            'purchase_description' => @$purchase_description,
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d'),
        );


        if ($this->db->where('purchase_id', $purchase_id)->update('purchase_tbl', $purchaseData)) {

            $this->db->where('purchase_id', $purchase_id)->delete('purchase_details_tbl');

            $i = 0;
            $purchaseDetails = [];

            foreach ($material as $key => $material_id) {

                $purchaseDetails[] = array(
                    'purchase_id' => $purchase_id,
                    'material_id' => $material_id,
                    'pattern_model_id' => $pattern_model_id[$i],
                    'color_id' => $color_id[$i],
                    'purchase_price' => $purchase_price[$i],
                    'quantity' => $quantity[$i],
                    'discount' => $discount[$i],
                    'created_by' => $this->session->userdata('user_id'),
                    'created_date' => date('Y-m-d'),
                );

                $i++;
            }


            $this->db->insert_batch('purchase_details_tbl', $purchaseDetails);
        }

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Purchase update successfull </div>");

        redirect('purchase-list');
    }

    public function view_purchase($purchase_id) {
        $data['purchase'] = $this->purchase_model->get_purchase_by_id($purchase_id);
        $data['purchase_details'] = $this->purchase_model->get_purchase_details_by_id($purchase_id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/purchase/view_purchase', $data);
        $this->load->view('b_level/footer');
    }

//    ============= its for get-material-info ===========
    public function get_material_wise_color_info($material_id) {
        $get_material_wise_color_info = $this->purchase_model->get_material_wise_color_info($material_id);
        echo json_encode($get_material_wise_color_info);
    }

//    ============= its for get-material-info ===========
    public function get_material_wise_pattern_info($material_id) {
        $get_material_wise_pattern_info = $this->purchase_model->get_material_wise_pattern_info($material_id);
        echo json_encode($get_material_wise_pattern_info);
    }

//============= its for material, color and pattern wise stock summation when change color_cals function =============
    public function get_material_color_pattern_wise_stock_qnt($raw_material_id, $color_id, $pattern_id) {
        $available_qnt = $this->purchase_model->get_material_color_pattern_wise_available_qnt($raw_material_id, $color_id, $pattern_id);
        echo json_encode($available_qnt);
    }

}
