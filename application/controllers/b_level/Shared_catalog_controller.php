<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shared_catalog_controller extends CI_Controller
{


    private $user_id = '';
    private $level_id = '';

    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1)
        {
            $this->level_id = $this->session->userdata('user_id');
        } else
        {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b')
        {
            redirect('b-level-logout');
        }

        $this->load->model('b_level/User_model');
        $this->load->model('b_level/Catalog_request_model');
        $this->load->model('b_level/product_model');
        $this->load->model('b_level/Order_model');
        $this->load->model('b_level/settings');
        $this->load->library('paypal_lib');

    }


    public function index()
    {
        $this->permission->check_label('manage')->create()->redirect();
        $total_row = $this->db->where('user_type', 'b')->where('created_by', '')->count_all_results('user_info');
        $config["base_url"] = base_url('catalog-user/');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $data["b_user_list"] = $this->User_model->b_user_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        /* print_r($data["customer_list"]);die();*/

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/b_user_list', $data);
        $this->load->view('b_level/footer');
    }

    function b_user_request_for_catalog()
    {
        $check = $this->db->where('b_user_id', $_POST['b_user_id'])->where('requested_by', $this->level_id)->get('b_user_catalog_request')->row();
        if (!empty($check))
        {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You are already requested for this user</div>");
            redirect("catalog-user");
        }

        $data = array(
            'b_user_id' => $_POST['b_user_id'],
            'requested_by' => $this->level_id,
            'remark' => $_POST['catalog_remark'],
            'create_date' => date('Y-m-d'),
        );

        $this->db->insert('b_user_catalog_request', $data);

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Your catalog request has been sent successfully</div>");
        redirect("catalog-user");
    }

    function catalog_request_list()
    {
        $this->permission->check_label('manage')->create()->redirect();
        $total_row = $this->db->where('b_user_id', $this->level_id)->count_all_results('b_user_catalog_request');
        $config["base_url"] = base_url('/catalog-request/');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $data["catalog_request_list"] = $this->Catalog_request_model->get_catalog_request($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $data["product_list"] = $this->product_model->get_b_user_product_list($this->level_id);


        /* print_r($data["customer_list"]);die();*/

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/catalog_request_list', $data);
        $this->load->view('b_level/footer');
    }

    function approve_catalog_request()
    {
        if (isset($_POST['approve_products']) && !empty($_POST['approve_products']))
        {
            $this->db->delete('b_user_catalog_products', array('request_id' => $_POST['request_id']));
            foreach ($_POST['approve_products'] as $value)
            {
                $data = array(
                    'request_id' => $_POST['request_id'],
                    'product_id' => $value,
                    'create_date' => date('Y-m-d'),
                );

                $this->db->insert('b_user_catalog_products', $data);
            }

            $update_data = ['status' => 1];
            $this->db->where('request_id', $_POST['request_id']);
            $this->db->update('b_user_catalog_request', $update_data);
        }
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Catalog request has been approved successfully</div>");
        redirect("catalog-request");
    }

    function not_approve_catalog_request($request_id)
    {
        $this->db->delete('b_user_catalog_products', array('request_id' => $request_id));
        $update_data = ['status' => 0];
        $this->db->where('request_id', $request_id);
        $this->db->update('b_user_catalog_request', $update_data);

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Catalog request has not been approved successfully</div>");
        redirect("catalog-request");
    }

    function b_user_catalog_product_list($request_id)
    {
        $requestData = $this->db->where('request_id', $request_id)->where('requested_by', $this->level_id)->get('b_user_catalog_request')->row();

        if (!isset($requestData->status) || $requestData->status != 1)
        {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Your request not approved</div>");
            redirect("catalog-user");
        }

        $data['product_data'] = $this->db->select('*')
            ->from('product_tbl')
            ->where('created_by', $this->level_id)
            ->where('active_status', 1)
            ->order_by('product_name', 'asc')
            ->get()->result();



        $data['b_user_product_data'] = $this->Catalog_request_model->get_approve_product_list($request_id);

        $data['request_id'] = $request_id;
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/requested_product_list', $data);
        $this->load->view('b_level/footer');

    }

    function catalog_product_order_add($request_id)
    {
        /*$result =  $this->Catalog_request_model->get_catalog_product_by_category(29,1);
        print_r($result);die();*/
        $requestData = $this->db->where('request_id', $request_id)->where('requested_by', $this->level_id)->get('b_user_catalog_request')->row();

        if (!isset($requestData->status) || $requestData->status != 1)
        {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Your request not approved</div>");
            redirect("catalog-user");
        }

        $bUserData = $this->db->where('id', $requestData->b_user_id)->get('user_info')->row();

        $this->permission->check_label('order')->create()->redirect();
      //  $data['get_category'] = $this->Catalog_request_model->get_catalog_category($request_id);
        $data['get_category'] = $this->Order_model->get_category($request_id);
        $data['b_user_data'] = $bUserData;


        // $data['get_patern_model'] = $this->Order_model->get_patern_model();

        // $data['get_product'] = $this->Order_model->get_product();


        //  $data['colors'] = $this->db->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();

        $data['rooms'] = $this->db->get('rooms')->result();

        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "b_level/orders/customer_js.php";
        $data['request_id'] = $request_id;
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/catalog_order_new', $data);
        $this->load->view('b_level/footer');
    }

    function get_catalog_product_by_category($category_id, $request_id)
    {
        $result = $this->db->select('product_id,category_id,product_name')
            ->where('category_id', $category_id)
            ->where('active_status', 1)
            ->where('created_by', $this->level_id)
            ->get('product_tbl')->result();
       /* $result = $this->Catalog_request_model->get_catalog_product_by_category($category_id, $request_id);*/

        $q = '';
        $q .= '<option value=""></option>';
        foreach ($result as $key => $product)
        {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }


    function b_user_wise_sidemark($b_user_id)
    {
        $customer_wise_sidemark = $this->db->select('user_info.*,us_state_tbl.tax_rate')
            ->from('user_info')
            ->join('us_state_tbl', 'us_state_tbl.shortcode=user_info.state', 'left')
            ->where('user_info.id', $b_user_id)
            ->get()->row();

        echo json_encode($customer_wise_sidemark);
    }

    public function save_catalog_product_order()
    {

        $this->permission->check_label('order')->create()->redirect();

        $products = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $orderid = $this->input->post('orderid');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');

        $barcode_img_path = '';
        if (!empty($orderid))
        {
            $this->load->library('barcode/br_code');
            $barcode_img_path = 'assets/barcode/b/' . $orderid . '.jpg';
            file_put_contents($barcode_img_path, $this->br_code->gcode($orderid));
        }


        if (@$_FILES['file_upload']['name'])
        {

            $config['upload_path'] = './assets/b_level/uploads/file/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx|xls|xlsx';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload'))
            {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("catalog-user");
            } else
            {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else
        {

            @$upload_file = '';
        }


        $list_price = $this->input->post('list_price');

        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');

        if ($this->session->userdata('isAdmin') == 1)
        {
            $level_id = $this->session->userdata('user_id');
        } else
        {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $orderData = array(
            'order_id' => $orderid,
            'order_date' => $this->input->post('order_date'),
            'b_user_id' => $this->input->post('customer_id'),
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $level_id,
            'side_mark' => $this->input->post('side_mark'),
            'upload_file' => $upload_file,
            'barcode' => @$barcode_img_path,
            'state_tax' => $this->input->post('tax'),
            'shipping_charges' => 0,
            'installation_charge' => $this->input->post('install_charge'),
            'other_charge' => $this->input->post('other_charge'),
            'misc' => $this->input->post('misc'),
            'invoice_discount' => $this->input->post('invoice_discount'),
            'grand_total' => $this->input->post('grand_total'),
            'subtotal' => $this->input->post('subtotal'),
            'paid_amount' => 0,
            'due' => $this->input->post('grand_total'),
            'order_status' => $this->input->post('order_status'),
            'created_by' => $this->session->userdata('user_id'),
            'updated_by' => '',
            'created_date' => date('Y-m-d'),
            'updated_date' => ''
        );


        if ($this->db->insert('b_to_b_level_quatation_tbl', $orderData))
        {

            foreach ($products as $key => $product_id)
            {

                $productData = array(
                    'order_id' => $orderid,
                    'room' => $room[$key],
                    'product_id' => $product_id,
                    'product_qty' => $qty[$key],
                    'list_price' => $list_price[$key],
                    'discount' => $discount[$key],
                    'unit_total_price' => $utprice[$key],
                    'category_id' => $category[$key],
                    'pattern_model_id' => $pattern_model[$key],
                    'color_id' => $color[$key],
                    'width' => $width[$key],
                    'height' => $height[$key],
                    'height_fraction_id' => $height_fraction_id[$key],
                    'width_fraction_id' => $width_fraction_id[$key],
                    'notes' => $notes[$key],
                );

                $this->db->insert('b_to_b_level_qutation_details', $productData);
                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'product_attribute' => $attributes[$key]
                );

                $this->db->insert('b_to_b_level_quatation_attributes', $attrData);
            }

            // Send sms
            $this->smsSend($this->input->post('customer_id'), $orderid);


            $this->cart->destroy();

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
            redirect('b_level/Shared_catalog_controller/receipt/' . $orderid);
        } else
        {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
            redirect(base_url('catalog-user'));
        }
    }

    function smsSend($b_user_id, $order_id)
    {

        $b_user_info = $this->db->where('id', $b_user_id)->get('user_info')->row();

        if ($b_user_info->phone != NULL)
        {

            $this->load->library('twilio');

            $name = @$b_user_info->first_name . ' ' . @$b_user_info->last_name;

            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->level_id)->where('default_status', 1)->get()->row();

            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $b_user_info->phone;
            $message = 'Hi! ' . $name . ' Order Successfully. OrderId ' . @$order_id;
            $response = $this->twilio->sms($from, $to, $message);

            return 1;
        }
    }

    public function receipt($order_id = NULL)
    {

        $this->permission->check_label('order')->read()->redirect();

        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);

        $data['order_details'] = $this->Order_model->get_b_to_b_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('b_to_b_shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=b_to_b_shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('b_to_b_shipment_data')->row();


        $data['company_profile'] = $query = $this->db->select('*')
            ->from('company_profile')
            ->where('user_id', $data['orderd']->b_user_id)
            ->get()->result();

        $data['created_by_data'] = $this->db->where('id', $data['orderd']->created_by)->get('user_info')->row();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/order_receipt', $data);
        $this->load->view('b_level/footer');

    }

    public function shipment($order_id = NULL)
    {

        $this->permission->check_label('order')->read()->redirect();


        $shipDataCheck = $this->db->where('order_id', $order_id)->get('b_to_b_shipment_data')->num_rows();

        if ($shipDataCheck > 0)
        {

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect("b_level/Shared_catalog_controller/order_view/" . $order_id);

        }


        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
        $data['methods'] = $this->db->where('status', 1)->get('shipping_method')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/shipment_form', $data);
        $this->load->view('b_level/footer');


    }

    public function order_view($order_id)
    {

        $this->permission->check_label('order')->read()->redirect();

        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_b_to_b_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('b_to_b_shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=b_to_b_shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('b_to_b_shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/order_view', $data);
        $this->load->view('b_level/footer');

    }

    public function shipping()
    {


        $order_id = $this->input->post('orderid');
        $length = $this->input->post('length');
        $weight = $this->input->post('weight');
        $method_id = $this->input->post('ship_method');
        $service_type = $this->input->post('service_type');
        $b_user_name = $this->input->post('b_user_name');
        $shipping_address = $this->input->post('shipping_address');
        $phone = $this->input->post('phone');

        $shipDataCheck = $this->db->where('order_id', $order_id)->get('b_to_b_shipment_data')->num_rows();

        if ($shipDataCheck > 0)
        {
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect("b_level/Shared_catalog_controller/order_view/" . $order_id);
        }

        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();

        $order = $this->db->select('b_to_b_level_quatation_tbl.*,user_info.*')
            ->join('user_info', 'user_info.id=b_to_b_level_quatation_tbl.b_user_id')
            ->where('order_id', $order_id)
            ->get('b_to_b_level_quatation_tbl')->row();

        $addd = explode(',', $shipping_address);


        if ($addd[0] != NULL && $addd[1] != NULL && $addd[2] != NULL && $addd[3] != NULL && $addd[4] != NULL)
        {

            $phone = str_replace('+1', '', $phone);

            $ShipTo = (object)[
                'Name' => $b_user_name,
                'AttentionName' => $b_user_name,
                'Phone' => $b_user_name,
                'AddressLine' => $addd[0],
                'City' => $addd[1],
                'StateProvinceCode' => $addd[2],
                'PostalCode' => $addd[3],
                'CountryCode' => $addd[4]
            ];

        } else
        {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : Wrong address format!</p>
                </div>");
            redirect("b_level/Shared_catalog_controller/shipment/" . $order_id);
        }
        //}

        if (strtolower($method->method_name) == 'ups')
        {

            $shiperInfo = $this->get_shiper_info($method_id, $service_type);

            $this->ups_shipping($ShipTo, $length, $weight, $shiperInfo, $order_id, $method_id);
        }


        if (strtolower($method->method_name) == 'delivery in person' || strtolower($method->method_name) == 'customer pickup')
        {

            $this->delivery_in_person($ShipTo, $order_id, $method_id);
        }
    }

    //--------------------------------------------------
    //  SHIper information get
    //--------------------------------------------------
    public function get_shiper_info($method_id, $service_type)
    {

        $company_profile = $this->settings->company_profile();
        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();


        $arrayName = array(
            'username' => $method->username,
            'password' => $method->password,
            'account_id' => $method->account_id,
            'access_token' => $method->access_token,
            'name' => $company_profile[0]->company_name,
            'attentionname' => 'Attention Name',
            'description' => 'This is test deiscription',
            'address' => $company_profile[0]->address,
            'city' => $company_profile[0]->city,
            'state' => $company_profile[0]->state,
            'zip_code' => $company_profile[0]->zip_code,
            'country_code' => $company_profile[0]->country_code,
            'phone' => $company_profile[0]->phone,
            'method_id' => $method->id,
            'mode' => $method->mode,
            'service_type' => $service_type,
            'pickup_method' => $method->pickup_method
        );

        return $arrayName;
    }

    //--------------------------------------------------
    //  UPS SHIPPIng
    //--------------------------------------------------
    public function ups_shipping($ShipTo = null, $length = NULL, $weight = NULL, $shiperInfo = NULL, $order_id = NULL, $method_id = NULL)
    {


        $phone = ltrim($ShipTo->Phone, '+1 ');


        $this->load->library('Upsshipping');

        /* Ship To Address */
        $this->upsshipping->addField('ShipTo_Name', $ShipTo->Name);

        $this->upsshipping->addField('ShipTo_AddressLine', array($ShipTo->AddressLine));

        $this->upsshipping->addField('ShipTo_City', $ShipTo->City);

        $this->upsshipping->addField('ShipTo_StateProvinceCode', $ShipTo->StateProvinceCode);

        $this->upsshipping->addField('ShipTo_PostalCode', $ShipTo->PostalCode);

        $this->upsshipping->addField('ShipTo_CountryCode', $ShipTo->CountryCode);

        $this->upsshipping->addField('ShipTo_Number', $phone);

        $this->upsshipping->addField('length', $length);

        $this->upsshipping->addField('weight', $weight);

        list($response, $status) = $this->upsshipping->processShipAccept($shiperInfo);

        $ups_response = json_decode($response);

        if (isset($ups_response->ShipmentResponse) && $ups_response->ShipmentResponse->Response->ResponseStatus->Code == 1)
        {

            $track_number = $ups_response->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber;

            $total_charges = $ups_response->ShipmentResponse->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue;

            $graphic_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage;

            $html_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->HTMLImage;

            //graphic_image_path
            $data1g = "data:image/jpeg;base64," . $graphic_image;

            $img_name_g = $track_number . '_graphic_image.jpeg';

            $source_g = fopen($data1g, 'r');

            $graphic_image = fopen("assets/b_level/shipping_img/" . $img_name_g, "w");

            stream_copy_to_stream($source_g, $graphic_image);

            fclose($source_g);

            fclose($graphic_image);

            $graphic_image_path = 'assets/b_level/shipping_img/' . $img_name_g;

            // html image
            $data1 = "data:image/jpeg;base64," . $html_image;

            $img_name = uniqid() . $track_number . '_html_image.jpeg';

            $source = fopen($data1, 'r');

            $html_image = fopen("assets/b_level/shipping_img/" . $img_name, "w");

            stream_copy_to_stream($source, $html_image);

            fclose($source);

            fclose($html_image);

            $html_image_path = 'assets/b_level/shipping_img/' . $img_name;


            $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'grand_total' => @$rowData->grand_total + $total_charges,
                'due' => @$rowData->due + $total_charges,
                'shipping_charges' => $total_charges,
                'order_stage' => 5
            );

            //update to quatation table with pyament due
            $this->db->where('order_id', $order_id)->update('b_to_b_level_quatation_tbl', $orderData);
            $orderDATA = $this->db->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();

            $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

            $upsResponseData = array(
                'b_user_name' => $ShipTo->Name,
                'b_user_phone' => $ShipTo->Phone,
                'order_id' => $order_id,
                'track_number' => $track_number,
                'shipping_charges' => $total_charges,
                'graphic_image' => $graphic_image_path,
                'html_image' => $html_image_path,
                'method_id' => $method_id,
                'service_type' => @$shiperInfo['service_type'],
                'shipping_addres' => $shipping_addres
            );


            $this->db->insert('b_to_b_shipment_data', $upsResponseData);

            // C level notification
            $cNotificationData = array(
                'notification_text' => 'Order stage updated to shipping for ' . $order_id,
                'go_to_url' => '',
                'created_by' => $orderDATA->level_id,
                'date' => date('Y-m-d')
            );

            //   $this->db->insert('c_notification_tbl', $cNotificationData);
            //-------------------------
            //


            redirect('b_level/Shared_catalog_controller/order_view/' . $order_id);


        } elseif ($ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode != NULL)
        {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors code : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code . "</p>
                <p>Description : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description . "</p>
            </div>");
            redirect("b_level/Shared_catalog_controller/shipment/" . $order_id);
        } else
        {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : This is internal error!</p>
            </div>");
            redirect("b_level/Shared_catalog_controller/shipment/" . $order_id);
        }
    }

    //--------------------------------------------------
    //   SHIPPIng  in PERson
    //--------------------------------------------------
    public function delivery_in_person($ShipTo, $order_id, $method_id)
    {

        $delivery_date = $this->input->post('delivery_date');
        $delivery_charge = $this->input->post('delivery_charge');
        $comment = $this->input->post('comment');

        $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
        // quatation table update
        $orderData = array(
            'grand_total' => @$rowData->grand_total + @$delivery_charge,
            'due' => @$rowData->due + @$delivery_charge,
            'shipping_charges' => @$delivery_charge,
            'order_stage' => 5
        );

        //update to quatation table with pyament due
        $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);
        $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;


        $deliveryData = array(
            'b_user_name' => $ShipTo->Name,
            'b_user_phone' => $ShipTo->Phone,
            'order_id' => $order_id,
            'track_number' => '',
            'shipping_charges' => $delivery_charge,
            'graphic_image' => '',
            'html_image' => '',
            'method_id' => $method_id,
            'comment' => $comment,
            'delivery_date' => $delivery_date,
            'service_type' => '',
            'shipping_addres' => $shipping_addres
        );

        $this->db->insert('b_to_b_shipment_data', $deliveryData);

        $orderDATA = $this->db->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();


        // C level notification
        $cNotificationData = array(
            'notification_text' => 'Order stage updated to shipping for ' . $order_id,
            'go_to_url' => '',
            'created_by' => $orderDATA->level_id,
            'date' => date('Y-m-d')
        );

        //  $this->db->insert('c_notification_tbl', $cNotificationData);
        //-------------------------
        //


        redirect('b_level/Shared_catalog_controller/order_view/' . $order_id);
    }

    function catalog_order_list()
    {

        $this->permission->check_label('order')->read()->redirect();

        $search = (object)array(
            'product_id' => $this->input->post('product_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_date' => $this->input->post('order_date'),
            'order_stage' => $this->input->post('order_stage')
        );


        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->customer_id;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;

        //total row count for pasination
        $total_rows = $this->db->where('level_id', $this->level_id)->count_all('b_to_b_level_quatation_tbl');

        $per_page = 15;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        //-----------

        $data['orderd'] = $this->Order_model->get_b_to_b_all_orderd($search, $per_page, $page);
        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/order_list', $data);
        $this->load->view('b_level/footer');

    }


    function pagination($total_rows, $per_page, $page)
    {

        $config["base_url"] = base_url('b-order-list');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $page;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        return $config;
    }

    public function delete_order($order_id)
    {

        $this->permission->check_label('order')->delete()->redirect();

        $this->db->where('order_id', $order_id)->delete('b_to_b_level_quatation_tbl');
        $this->db->where('order_id', $order_id)->delete('b_to_b_level_qutation_details');
        $this->db->where('order_id', $order_id)->delete('b_to_b_level_quatation_attributes');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Delete successfully!</div>");
        redirect("catalog-order-list");
    }

    public function b_user_delete_order($order_id)
    {

        $this->permission->check_label('order')->delete()->redirect();

        $this->db->where('order_id', $order_id)->delete('b_to_b_level_quatation_tbl');
        $this->db->where('order_id', $order_id)->delete('b_to_b_level_qutation_details');
        $this->db->where('order_id', $order_id)->delete('b_to_b_level_quatation_attributes');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Delete successfully!</div>");
        redirect("catalog-b-user-order-list");
    }





    public function set_order_stage($stage, $order_id)
    {

        if (!empty($stage) && !empty($order_id))
        {

            $order = $this->db->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();


            //$this->shipping($order_id);
            $this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('b_to_b_level_quatation_tbl');

            //$this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('quatation_tbl');

            if ($stage == 1)
            {
                $order_stage = 'Quote';
            }
            if ($stage == 2)
            {
                $order_stage = 'Paid';
            }
            if ($stage == 3)
            {
                $order_stage = 'Partially Paid';
            }
            if ($stage == 4)
            {
                $order_stage = 'Manufacturing';
            }
            if ($stage == 5)
            {
                $order_stage = 'Shipping';
            }
            if ($stage == 6)
            {
                $order_stage = 'Cancelled';
            }
            if ($stage == 7)
            {
                $order_stage = 'Delivered';
            }

            // C level notification
            /*  $cNotificationData = array(
                  'notification_text' => 'Order Stage has been updated to '.$order_stage.' for '.$order_id,
                  'go_to_url'         => '#',
                  'created_by'        => $order->level_id,
                  'date'              => date('Y-m-d')

              );

              $this->db->insert('c_notification_tbl',$cNotificationData);

              // Send sms
              $this->Order_model->smsSend( $data = array(

                  'customer_id' => $order->customer_id,
                  'message'     => 'Order Stage has been updated to  '.$order_stage.' for '.$order_id,
                  'subject'     => 'Order payment'
              ));

              $this->Order_model->send_email($order_id, $stage);*/

            echo 1;

        } else
        {
            echo 2;
        }

    }


    public function order_payment_update()
    {

        $b_user_id = $this->input->post('b_user_id');
        //$customer_no    = $this->input->post('customer_no');
        $payment_method = $this->input->post('payment_method');
        $order_id = $this->input->post('order_id');


        $cHead = $this->db->select('HeadCode')->where('HeadName', 'CUS-4672-first customer')->get('b_acc_coa')->row();

        if ($cHead->HeadCode != NULL)
        {

            $user_info = $this->db->where('id', $b_user_id)->get('user_info')->row();

            if ($payment_method == 'paypal')
            {


                $orderData = array(
                    'order_id' => $order_id,
                    'b_user_id' => $b_user_id,
                    'paid_amount' => $this->input->post('paid_amount'),
                );

                $orderDataPaypal = array(

                    'order_id' => $order_id,
                    'headcode' => $cHead->HeadCode,
                    'grand_total' => $this->input->post('grand_total'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'payment_method' => $this->input->post('payment_method'),
                    'card_number' => $this->input->post('card_number'),
                    'issuer' => $this->input->post('issuer'),
                 /*   'cusstomer_user_id' => $cusstomer_info->customer_user_id,*/
                    'b_user_id' => $b_user_id,

                );

                $this->session->set_userdata($orderDataPaypal);

                $pyament = $this->payment_by_paypal($orderData,'RECEIVE_PAYMENT');


            } else
            {


                $due = $this->input->post('due');

                if ($due > 0)
                {
                    $order_stage = 3;
                } else
                {
                    $order_stage = 2;
                }

                $rowData = $this->db->select('due,paid_amount,clevel_order_id,created_by,commission_amt')->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();


                // Get B Level User info for get commission : START
                $created_by = @$rowData->created_by;
                $commission_amt = @$rowData->commission_amt;
                if ($created_by != '')
                {
                    $user_data = $this->db->select('fixed_commission,percentage_commission')->where('id', @$rowData->created_by)->get('user_info')->row();

                    if (@$user_data->fixed_commission != '' && $commission_amt == 0)
                    {
                        $commission_amt_fixed = @$user_data->fixed_commission;
                    } else
                    {
                        $commission_amt_fixed = 0;
                    }

                    if (@$user_data->percentage_commission != '' && @$user_data->percentage_commission > 0)
                    {
                        $commission_amt_percentage = round((($this->input->post('paid_amount') * @$user_data->percentage_commission) / 100), 2);
                    } else
                    {
                        $commission_amt_percentage = 0;
                    }

                    $commission_amt += ($commission_amt_fixed + $commission_amt_percentage);
                }
                // Get B Level User info for get commission : END

                // quatation table update
                $orderData = array(
                    'paid_amount' => @$rowData->paid_amount + $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'order_stage' => $order_stage,
                    'commission_amt' => $commission_amt
                );

                if ($rowData->order_id != NULL)
                {
                    $this->db->set('order_stage', $order_stage)->where('order_id', $rowData->order_id)->update('b_to_b_level_quatation_tbl');
                }

                //update to quatation table with pyament due
                $this->db->where('order_id', $order_id)->update('b_to_b_level_quatation_tbl', $orderData);
                //-----------------------------------------
                // quatation table update
                $payment_tbl = array(
                    'quotation_id' => $order_id,
                    'payment_method' => $this->input->post('payment_method'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'payment_date' => date('Y-m-d'),
                    'created_by' => $this->user_id,
                    'create_date' => date('Y-m-d')
                );


                if ($this->db->insert('payment_tbl', $payment_tbl))
                {

                    $payment_id = $this->db->insert_id();

                    if ($payment_method == 'check')
                    {

                        if (@$_FILES['check_image']['name'])
                        {

                            $config['upload_path'] = './assets/b_level/uploads/check_img/';
                            $config['allowed_types'] = 'jpeg|jpg';
                            $config['overwrite'] = false;
                            $config['max_size'] = 3000;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('check_image'))
                            {
                                $error = $this->upload->display_errors();
                                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                                redirect("new-order");
                            } else
                            {

                                $data = $this->upload->data();
                                $check_image = $config['upload_path'] . $data['file_name'];
                            }
                        } else
                        {
                            @$check_image = '';
                        }

                        $payment_check_tbl = array(
                            'payment_id' => $payment_id,
                            'check_number' => $this->input->post('check_number'),
                            'check_image' => $check_image,
                        );

                        $this->db->insert('payment_check_tbl', $payment_check_tbl);

                    }

                }


                $voucher_no = $order_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $this->input->post('paid_amount');
                $cAID = $cHead->HeadCode;
                $IsPosted = 1;
                $CreateBy = $this->user_id;
                $createdate = date('Y-m-d H:i:s');


                $payment_method = $this->input->post('payment_method');

                if ($payment_method == 'cash')
                {
                    $COAID = '1020101';
                }
                if ($payment_method == 'check')
                {
                    $COAID = '102010202';
                }
                if ($payment_method == 'card')
                {
                    $COAID = '1020103';
                }

                // C level transection start
                //C level credit insert b_acc_transaction
                $customerCredit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $COAID,
                    'Narration' => "Paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);
                //------------------------------------

                //B level debit insert b_acc_transaction

                $bCOA = '502020101';
              /*  $b_levelDebit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $bCOA,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );*///b_acc_transaction

            /*    $this->db->insert('acc_transaction', $b_levelDebit);*/
                // C level transection END

                //. B level transection start
                //customer credit insert b_acc_transaction
                $customerCredit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $cAID,
                    'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('b_acc_transaction', $customerCredit);
                //------------------------------------

                //b_level debit insert b_acc_transaction

                $b_levelDebit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $COAID,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );//b_acc_transaction

                $this->db->insert('b_acc_transaction', $b_levelDebit);
                //- B level transection END----------------------

                // C level notification

             /*   $cNotificationData = array(

                    'notification_text' => 'Payment has been done for ' . $order_id,
                    'go_to_url' => '#',
                    'created_by' => $cusstomer_info->customer_user_id,
                    'date' => date('Y-m-d')

                );

                $this->db->insert('c_notification_tbl', $cNotificationData);*/

                //-------------------------
                //

               /* $bNotificationData = array(

                    'notification_text' => 'Payment has been received for ' . $order_id,
                    'go_to_url' => 'b_level/invoice_receipt/receipt/' . $order_id,
                    'created_by' => $cusstomer_info->customer_user_id,
                    'date' => date('Y-m-d')
                );
                $this->db->insert('b_notification_tbl', $bNotificationData);*/

                //--------------------------


               /* $this->email_sender->send_email(
                    $data = array(
                        'customer_id' => $customer_id,
                        'message' => 'Payment has been done for Order id' . $order_id . ', Paid amount:' . $paid_amount . ', Payment method:' . $payment_method,
                        'subject' => 'Order payment'
                    )
                );*/


                // Send sms
             /*   $this->Order_model->smsSend($data = array(
                    'customer_id' => $customer_id,
                    'message' => 'Payment has been done for Order id' . $order_id . ', Paid amount: ' . $paid_amount . ', Payment method:' . $payment_method,
                    'subject' => 'Order payment'
                ));*/


                //--------------------------
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                redirect('b_level/Shared_catalog_controller/money_receipt/' . $order_id);

            }

        } else
        {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer account code not found! </div>");
            redirect(base_url('b_level/Shared_catalog_controller/order_view/' . $order_id));

        }
    }


    public function payment_by_paypal($orderData,$user_status) {


        $b_user_id = $orderData['b_user_id'];
        $order_id = $orderData['order_id'];

        //$item_name = "Order :: Test";
        // ---------------------
        //Set variables for paypal form
        $returnURL = base_url("b_level/Shared_catalog_controller/paypal_b_to_b_success/$order_id/$b_user_id"); //payment success url
        $cancelURL = base_url("b_level/Shared_catalog_controller/paypal_b_to_b_cancel/$order_id/$b_user_id"); //payment cancel url
        $notifyURL = base_url('b_level/Shared_catalog_controller/paypal_b_to_b_ipn'); //ipn url
        //set session token

        $this->session->unset_userdata('_tran_token');
        $this->session->set_userdata(array('_tran_token' => $order_id));
        $this->session->set_userdata(array('user_status' => $user_status));
        // set form auto fill data
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);



        $paypal = $this->db->select('*')
            ->from('gateway_tbl')
            ->where('default_status', 1)
         /*   ->where('created_by',$this->user_id)*/
            ->where('created_by',141)
            ->get()
            ->row();



        $paypal_lib_currency_code = (!empty($paypal->currency) ? $paypal->currency : 'USD');
        $paypal_lib_ipn_log_file = BASEPATH . 'logs/paypal_ipn.log';
        $paypal_lib_ipn_log = TRUE;
        $paypal_lib_button_path = 'buttons';


        // my customization
        $this->paypal_lib->add_field('mode', $paypal->status);
        $this->paypal_lib->add_field('business', $paypal->payment_mail);
        $this->paypal_lib->add_field('paypal_lib_currency_code', $paypal_lib_currency_code);
        $this->paypal_lib->add_field('paypal_lib_ipn_log_file', $paypal_lib_ipn_log_file);
        $this->paypal_lib->add_field('paypal_lib_ipn_log', $paypal_lib_ipn_log);
        $this->paypal_lib->add_field('paypal_lib_button_path', $paypal_lib_button_path);
        //-----------------------


        // item information
        $this->paypal_lib->add_field('item_number', $order_id);
        //$this->paypal_lib->add_field('item_name', $item_name);
        $this->paypal_lib->add_field('amount', $orderData['paid_amount']);
        // $this->paypal_lib->add_field('quantity', $quantity);
        // $this->paypal_lib->add_field('discount_amount', $discount);
        // additional information
        $this->paypal_lib->add_field('custom', $order_id);
        $this->paypal_lib->image('');
        // generates auto form
        $this->paypal_lib->paypal_auto_form();


    }

    public function paypal_b_to_b_success($order_id = null, $b_user_id = null) {


        //$customer_info = $this->db->select('*')->from('customer_info')->where('customer_id', $customer_id)->get()->row();

        $this->order_payment_update_save();

    }

    public function paypal_b_to_b_cancel($order_id = null, $customer_id = null) {
        $user_status = $this->session->userdata('user_status');
        $this->session->unset_userdata('user_status');
        //--------------------------
        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cancel this payment </div>");
        if($user_status == 'MAKE_PAYMENT')
        {
            redirect('b_level/Shared_catalog_controller/b_user_receipt/' . $order_id);
        }
        else{
            redirect('b_level/Shared_catalog_controller/receipt/' . $order_id);
        }


    }

    public function order_payment_update_save() {

        $user_status = $this->session->userdata('user_status');


        $order_id = $this->session->userdata('order_id');
        $b_user_id = $this->session->userdata('b_user_id');

        $payment_method = $this->session->userdata('payment_method');

        if (!empty($order_id)) {


            $due = $this->session->userdata('due');

            if ($due > 0) {
                $order_stage = 3;
            } else {
                $order_stage = 2;
            }

            $rowData = $this->db->select('due,paid_amount')->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();

            // quatation table update
            $orderData = array(
                'paid_amount'    => @$rowData->paid_amount + $this->session->userdata('paid_amount'),
                'due'            => $this->session->userdata('due'),
                'order_stage'     => $order_stage
            );

            //update to quatation table with pyament due
            $this->db->where('order_id', $order_id)->update('b_to_b_level_quatation_tbl', $orderData);



            $this->db->where('order_id',$order_id)->update('b_to_b_level_quatation_tbl',$orderData);
            // quatation table update
            $payment_tbl = array(
                'quotation_id'                  => $order_id,
                'payment_method'                => $payment_method,
                'paid_amount'                   => $this->session->userdata('paid_amount'),
                'description'                   => "Payment given for Order#".$order_id,
                'payment_date'                  => date('Y-m-d'),
                'created_by'                    => $this->user_id,
                'create_date'                   => date('Y-m-d')
            );

            $this->db->insert('payment_tbl',$payment_tbl);


            $voucher_no = $order_id;
            $Vtype = "INV";
            $VDate = date('Y-m-d');
            $paid_amount = $this->session->userdata('paid_amount');
            $cAID = $this->session->userdata('headcode');
            $IsPosted = 1;
            $CreateBy = $this->user_id;
            $createdate = date('Y-m-d H:i:s');
            $Paypal_COAID = '1020104';
            //C level transaction Start
            $customerCredit = array(

                'VNo'           => $voucher_no,
                'Vtype'         => $Vtype,
                'VDate'         => $VDate,
                'Debit'         => 0,
                'Credit'        => $paid_amount,
                'COAID'         => $Paypal_COAID,
                'Narration'     => "Paid for invoice #".$voucher_no,
                'IsPosted'      => $IsPosted,
                'CreateBy'      => $CreateBy,
                'CreateDate'    => $createdate,
                'IsAppove'      => 1
            );

            $this->db->insert('acc_transaction', $customerCredit);

            //------------------------------------

            $bCOA = '502020101';

            $b_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'Credit' => 0,
                'COAID' => $bCOA,
                'Narration' => "Amount received for invoice #".$voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('acc_transaction', $b_levelDebit);
            // C level transaction END
            // B level transaction start
            //
            $customerCredit = array(

                'VNo'           => $voucher_no,
                'Vtype'         => $Vtype,
                'VDate'         => $VDate,
                'Debit'         => 0,
                'Credit'        => $paid_amount,
                'COAID'         => $cAID,
                'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                'IsPosted'      => $IsPosted,
                'CreateBy'      => $CreateBy,
                'CreateDate'    => $createdate,
                'IsAppove'      => 1
            );

            $this->db->insert('b_acc_transaction', $customerCredit);

            //------------------------------------

            $bCOA = '502020101';

            $b_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'Credit' => 0,
                'COAID' => $Paypal_COAID,
                'Narration' => "Amount received for invoice #".$voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('b_acc_transaction', $b_levelDebit);

            // B level transaction End


            $this->session->unset_userdata('order_id');
            $this->session->unset_userdata('headcode');
            $this->session->unset_userdata('grand_total');
            $this->session->unset_userdata('paid_amount');
            $this->session->unset_userdata('due');
            $this->session->unset_userdata('payment_method');
            $this->session->unset_userdata('card_number');
            $this->session->unset_userdata('issuer');
            $this->session->unset_userdata('cusstomer_user_id');
            $this->session->unset_userdata('customer_id');
            $this->session->unset_userdata('user_status');


        /*    $bNotificationData = array(
                'notification_text' => 'Payment has been submited for '. $order_id,
                'go_to_url'         => 'b_level/invoice_receipt/receipt/' . $order_id,
                'created_by'        =>  $cusstomer_user_id,
                'date'              =>  date('Y-m-d')
            );

            $this->db->insert('b_notification_tbl', $bNotificationData);



            // C level notification
            $cNotificationData = array(

                'notification_text' => 'Payment has been done for '.$order_id,
                'go_to_url'         => '#',
                'created_by'        => $cusstomer_user_id,
                'date'              => date('Y-m-d')

            );

            $this->db->insert('c_notification_tbl',$cNotificationData);


            $this->email_sender->send_email(
                $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject'     => 'Order payment'
                )
            );


            // Send sms
            $this->Order_model->smsSend( $data = array(
                'customer_id' => $customer_id,
                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                'subject'     => 'Order payment'
            ));*/



            //--------------------------
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
            if($user_status == 'MAKE_PAYMENT')
            {
                redirect('b_level/Shared_catalog_controller/b_user_money_receipt/' . $order_id);
            }
            else{
                redirect('b_level/Shared_catalog_controller/money_receipt/' . $order_id);
            }


        }
    }



    public function money_receipt($order_id = NULL) {

        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
        $data['payment'] = $this->db->where('quotation_id',$order_id)->order_by('payment_id','DESC')->get('payment_tbl')->row();

        $data['company_profile'] = $this->settings->company_profile();
        $data['b_user_info'] = $this->db->where('id', $data['orderd']->b_user_id)->get('user_info')->row();

        $this->load->view('b_level/shared_catalog/money_receipt', $data);

    }

    public function b_user_money_receipt($order_id = NULL) {

        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
        $data['payment'] = $this->db->where('quotation_id',$order_id)->order_by('payment_id','DESC')->get('payment_tbl')->row();

        $data['company_profile'] = $this->settings->company_profile();
        $data['b_user_info'] = $this->db->where('id', $data['orderd']->b_user_id)->get('user_info')->row();

        $this->load->view('b_level/shared_catalog/money_receipt', $data);

    }


    function approved_catalog_requested_product()
    {

        if ($this->input->post('action') == 'action_update')
        {
            $this->db->where('request_id', $this->input->post('request_id'))->update('b_user_catalog_products', ['approve_status' => 0,'product_link' => 0]);
            $listData = $this->input->post('Id_List');
            if(isset($listData) && count($listData) != 0)
            {
                foreach ($listData as $key => $value)
                {
                    $product_link = 0;
                    if(isset($_POST['product_arr'][$value]) && !empty($_POST['product_arr'][$value]))
                        $product_link = $_POST['product_arr'][$value];

                    $this->db->where('id', $value)->update('b_user_catalog_products', ['approve_status' => 1,'product_link' => $product_link]);
                }
            }


            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected product status has been Updated successfully.</div>");
        }

        redirect(base_url('b-user-catalog-product') . '/' . $_POST['request_id']);
    }

    public function random_keygenerator($mode = null, $len = null)
    {
        $result = "";
        if ($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4):
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++)
        {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

    function re_order_b_user_order()
    {

        $last_order_id = $this->db->select('order_id')->from('b_to_b_level_quatation_tbl')->order_by('id', 'desc')->get()->row();

        $custom_id = @$last_order_id->order_id;

        if (empty($custom_id))
        {
            $custom_id = "154762855914ZJ-001";
        } else
        {
            $custom_id = $custom_id;
        }

        $order_id = explode('-', $custom_id);
        $order_id = $order_id[1] + 1;
        $si_length = strlen((int)$order_id);
        $str = '000';
        $order_str = substr($str, $si_length);
//        echo $cutstr.$order_id;        die();

        $passenger_id = time() . $this->random_keygenerator(1, 4) . "b-" . $order_str . $order_id;
        $new_order_id = strtoupper($passenger_id);

        $mainOrderData = $this->db->where('clevel_order_id', $_POST['clevel_order_id'])->get('b_level_quatation_tbl')->row();
        $cOrderData = $this->db->where('order_id', $_POST['clevel_order_id'])->get('quatation_tbl')->row();

        if (!empty($cOrderData))
        {
            $customerData = $this->db->where('customer_id',$cOrderData->customer_id)->get('customer_info')->row();
            $sidemark = "";
            if(isset($customerData->side_mark) && !empty($customerData->side_mark))
                $sidemark = $customerData->side_mark;

            $barcode_img_path = '';
            if (!empty($new_order_id))
            {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $new_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($new_order_id));
            }

            $orderData = array(
                'order_id' => $new_order_id,
                'clevel_order_id' => $cOrderData->order_id,
                'order_date' => $cOrderData->order_date,
                'b_user_id' => $_POST['b_user_id'],
                'is_different_shipping' => $cOrderData->is_different_shipping,
                'different_shipping_address' => $cOrderData->different_shipping_address,
                'level_id' => $cOrderData->level_id,
                'side_mark' => $sidemark,
                'upload_file' => $cOrderData->upload_file,
                'barcode' => @$barcode_img_path,
                'state_tax' => $cOrderData->state_tax,
                'shipping_charges' => 0,
                'installation_charge' => $cOrderData->installation_charge,
                'other_charge' => $cOrderData->other_charge,
                'misc' => $cOrderData->misc,
                'invoice_discount' => $cOrderData->invoice_discount,
                'grand_total' => 0,
                'subtotal' => 0,
                'paid_amount' => 0,
                'due' => 0,
                'order_status' => 1,
                'created_by' => $this->session->userdata('user_id'),
                'updated_by' => '',
                'created_date' => date('Y-m-d'),
                'updated_date' => ''
            );
            $check = $this->db->insert('b_to_b_level_quatation_tbl', $orderData);

            if ($check)
            {
                $cOrderProductData = $this->db->where('order_id', $_POST['clevel_order_id'])->where_in('product_id', $_POST['re_order_product_data'])->get('qutation_details')->result();

                $total = 0;
                foreach ($cOrderProductData as $key => $order_product_data)
                {
                    $total += $order_product_data->list_price * $order_product_data->product_qty;
                    $productData = array(
                        'order_id' => $new_order_id,
                        'room' => $order_product_data->room,
                        'product_id' => $order_product_data->product_id,
                        'product_qty' => $order_product_data->product_qty,
                        'list_price' => $order_product_data->list_price,
                        'discount' => 0,
                        'unit_total_price' => $order_product_data->unit_total_price,
                        'category_id' => $order_product_data->category_id,
                        'pattern_model_id' => $order_product_data->pattern_model_id,
                        'color_id' => $order_product_data->color_id,
                        'width' => $order_product_data->width,
                        'height' => $order_product_data->height,
                        'height_fraction_id' => $order_product_data->height_fraction_id,
                        'width_fraction_id' => $order_product_data->width_fraction_id,
                        'notes' => $order_product_data->notes,
                    );

                    $this->db->insert('b_to_b_level_qutation_details', $productData);
                    $fk_od_id = $this->db->insert_id();

                    $attributeData = $this->db->where('order_id', $cOrderProductData->order_id)->get('quatation_attributes')->row();
                    $attrData = array(
                        'fk_od_id' => $fk_od_id,
                        'order_id' => $new_order_id,
                        'product_id' => $attributeData->product_id,
                        'product_attribute' => $attributeData->product_attribute
                    );

                    $this->db->insert('b_to_b_level_quatation_attributes', $attrData);
                }

                $this->db->where('order_id', $new_order_id)->update('b_to_b_level_quatation_tbl', ['subtotal' => $total, 'due' => $total, 'grand_total' => $total]);
                // Send sms
                $this->smsSend($this->input->post('customer_id'), $_POST['clevel_order_id']);

                $check_count =  $this->db->where('order_id', $_POST['clevel_order_id'])->get('qutation_details')->result();
                if(count($_POST['re_order_product_data']) != count($check_count))
                $this->manufacturing_re_order($_POST);

                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
                redirect('b_level/Shared_catalog_controller/receipt/' . $new_order_id);
            }
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Error! </div>");
            redirect('b_level/invoice_receipt/receipt/' . $mainOrderData->order_id);


        }

        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Error! </div>");
        redirect('b_level/invoice_receipt/receipt/' . $mainOrderData->order_id);
    }

    function manufacturing_re_order($request)
    {
        $last_order_id = $this->db->select('order_id')->from('manufacturing_b_level_quatation_tbl')->order_by('id', 'desc')->get()->row();

        $custom_id = @$last_order_id->order_id;

        if (empty($custom_id))
        {
            $custom_id = "154762855914ZJ-001";
        } else
        {
            $custom_id = $custom_id;
        }

        $order_id = explode('-', $custom_id);
        $order_id = $order_id[1] + 1;
        $si_length = strlen((int)$order_id);
        $str = '000';
        $order_str = substr($str, $si_length);
//        echo $cutstr.$order_id;        die();

        $passenger_id = time() . $this->random_keygenerator(1, 4) . "b-" . $order_str . $order_id;
        $new_order_id = strtoupper($passenger_id);

        $mainOrderData = $this->db->where('clevel_order_id', $request['clevel_order_id'])->get('b_level_quatation_tbl')->row();
       // $cOrderData = $this->db->where('order_id', $request['clevel_order_id'])->get('quatation_tbl')->row();

        if (!empty($mainOrderData))
        {
            $customerData = $this->db->where('customer_id',$mainOrderData->customer_id)->get('customer_info')->row();
            $sidemark = "";
            if(isset($customerData->side_mark) && !empty($customerData->side_mark))
                $sidemark = $customerData->side_mark;
            $barcode_img_path = '';
            if (!empty($new_order_id))
            {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $new_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($new_order_id));
            }

            $orderData = array(
                'order_id' => $new_order_id,
                'clevel_order_id' => $mainOrderData->clevel_order_id,
                'order_date' => $mainOrderData->order_date,
                'customer_id' => $mainOrderData->customer_id,
                'side_mark' => $sidemark,
                'upload_file' => $mainOrderData->upload_file,
                'barcode' => @$barcode_img_path,
                'is_different_shipping' => $mainOrderData->is_different_shipping,
                'different_shipping_address' => $mainOrderData->different_shipping_address,
                'level_id' => $mainOrderData->level_id,
                'state_tax' => $mainOrderData->state_tax,
                'shipping_charges' => 0,
                'installation_charge' => $mainOrderData->installation_charge,
                'other_charge' => $mainOrderData->other_charge,
                'misc' => $mainOrderData->misc,
                'invoice_discount' => $mainOrderData->invoice_discount,
                'grand_total' => 0,
                'subtotal' => 0,
                'paid_amount' => 0,
                'due' => 0,
                'order_status' => 1,
                'created_by' => $this->user_id,
                'updated_by' => $this->user_id,
                'created_date' => date('Y-m-d'),
                'updated_date' => ''
            );
            $check = $this->db->insert('manufacturing_b_level_quatation_tbl', $orderData);

            if ($check)
            {
                $cOrderProductData = $this->db->where('order_id', $_POST['clevel_order_id'])->where_not_in('product_id', $_POST['re_order_product_data'])->get('qutation_details')->result();

                $total = 0;
                foreach ($cOrderProductData as $key => $order_product_data)
                {
                    $total += $order_product_data->list_price * $order_product_data->product_qty;
                    $productData = array(
                        'order_id' => $new_order_id,
                        'room' => $order_product_data->room,
                        'product_id' => $order_product_data->product_id,
                        'product_qty' => $order_product_data->product_qty,
                        'list_price' => $order_product_data->list_price,
                        'discount' => 0,
                        'unit_total_price' => $order_product_data->unit_total_price,
                        'category_id' => $order_product_data->category_id,
                        'pattern_model_id' => $order_product_data->pattern_model_id,
                        'color_id' => $order_product_data->color_id,
                        'width' => $order_product_data->width,
                        'height' => $order_product_data->height,
                        'height_fraction_id' => $order_product_data->height_fraction_id,
                        'width_fraction_id' => $order_product_data->width_fraction_id,
                        'notes' => $order_product_data->notes,
                    );

                    $this->db->insert('manufacturing_b_level_qutation_details', $productData);
                    $fk_od_id = $this->db->insert_id();

                    $attributeData = $this->db->where('order_id', $cOrderProductData->order_id)->get('quatation_attributes')->row();
                    $attrData = array(
                        'fk_od_id' => $fk_od_id,
                        'order_id' => $new_order_id,
                        'product_id' => $attributeData->product_id,
                        'product_attribute' => $attributeData->product_attribute
                    );

                    $this->db->insert('manufacturing_b_level_quatation_attributes', $attrData);
                }

                $this->db->where('order_id', $new_order_id)->update('manufacturing_b_level_quatation_tbl', ['subtotal' => $total, 'due' => $total, 'paid_amount' => $total]);
                // Send sms
                return true;
            }
        }

    }

    function manufacturing_catalog_order_list()
    {
        $this->permission->check_label('order')->read()->redirect();

        $search = (object) array(
            'product_id' => $this->input->post('product_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_date' => $this->input->post('order_date'),
            'order_stage' => $this->input->post('order_stage')
        );


        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->customer_id;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;

        $total_rows = $this->Order_model->manufacturing_count_table_row($search);

        $per_page = 15;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        /* ends of bootstrap */
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['orderd'] = $this->Order_model->manufacturing_get_all_orderd($search, $per_page, $page);

        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/manufacturing_b_user_order_list', $data);
        $this->load->view('b_level/footer');
    }

    function manufacturing_receipt($order_id)
    {
        $orderData = $this->Order_model->get_manufacturing_orderd_by_id($order_id);
        $data['orderd'] = $orderData;

        $data['order_details'] = $this->Order_model->get_manufacturing_orderd_details_by_id($order_id);
        //dd($data['order_details']);
        $data['shipping'] = $this->db->select('manufacturing_shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=manufacturing_shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('manufacturing_shipment_data')->row();



        $data['company_profile'] = $this->settings->company_profile();
        $data['cinfo'] = $this->settings->single_company_profile($data['orderd']->level_id);

        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();


        $data['clevel_order_id'] = $orderData->clevel_order_id;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/manufacturing_order_receipt', $data);
        $this->load->view('b_level/footer');
    }

    function manufacturing_shipment($order_id = NULL)
    {
        $this->permission->check_label('order')->read()->redirect();


        $shipDataCheck = $this->db->where('order_id', $order_id)->get('shipment_data')->num_rows();

        if ($shipDataCheck > 0) {

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect(base_url('manufacturing-order-view/'.$order_id));

        }


        $data['orderd'] = $this->Order_model->get_manufacturing_orderd_by_id($order_id);

        $data['methods'] = $this->db->where('status',1)->get('shipping_method')->result();


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/manufacturing_shipment_form', $data);
        $this->load->view('b_level/footer');
    }

    function manufacturing_shipping()
    {
        $order_id = $this->input->post('orderid');
        $length = $this->input->post('length');
        $weight = $this->input->post('weight');
        $method_id = $this->input->post('ship_method');
        $service_type = $this->input->post('service_type');
        $customer_name = $this->input->post('customer_name');
        $shipping_address = $this->input->post('shipping_address');
        $phone = $this->input->post('phone');
        $shipDataCheck = $this->db->where('order_id', $order_id)->get('manufacturing_shipment_data')->num_rows();


        if ($shipDataCheck > 0) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect(base_url('manufacturing-order-view/'.$order_id));
        }

        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();

        $order = $this->db->select('manufacturing_b_level_quatation_tbl.*,customer_info.*')
            ->join('customer_info', 'customer_info.customer_id=manufacturing_b_level_quatation_tbl.customer_id')
            ->where('order_id', $order_id)
            ->get('manufacturing_b_level_quatation_tbl')->row();

       $addd = explode(',', $shipping_address);


        if ($addd[0] != NULL && $addd[1] != NULL && $addd[2] != NULL && $addd[3] != NULL && $addd[4] != NULL) {

            $phone = str_replace('+1', '', $phone);

            $ShipTo = (object) [
                'Name' => $customer_name,
                'AttentionName' => $customer_name,
                'Phone' => $phone,
                'AddressLine' => $addd[0],
                'City' => $addd[1],
                'StateProvinceCode' => $addd[2],
                'PostalCode' => $addd[3],
                'CountryCode' => $addd[4]
            ];

        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : Wrong address format!</p>
                </div>");
            redirect("b_level/Shared_catalog_controller/manufacturing_shipment/" . $order_id);
        }
        //}

        if (strtolower($method->method_name) == 'ups') {

            $shiperInfo = $this->manufacturing_get_shiper_info($method_id, $service_type);

            $this->manufacturing_ups_shipping($ShipTo, $length, $weight, $shiperInfo, $order_id, $method_id);
        }


        if (strtolower($method->method_name) == 'delivery in person' || strtolower($method->method_name) == 'customer pickup') {

            $this->manufacturing_delivery_in_person($ShipTo, $order_id, $method_id);
        }
    }

    public function manufacturing_get_shiper_info($method_id, $service_type) {

        $company_profile = $this->settings->company_profile();
        $method = $this->db->where('id', $method_id)->get('shipping_method')->row();


        $arrayName = array(
            'username' => $method->username,
            'password' => $method->password,
            'account_id' => $method->account_id,
            'access_token' => $method->access_token,
            'name' => $company_profile[0]->company_name,
            'attentionname' => 'Attention Name',
            'description' => 'This is test deiscription',
            'address' => $company_profile[0]->address,
            'city' => $company_profile[0]->city,
            'state' => $company_profile[0]->state,
            'zip_code' => $company_profile[0]->zip_code,
            'country_code' => $company_profile[0]->country_code,
            'phone' => $company_profile[0]->phone,
            'method_id' => $method->id,
            'mode' => $method->mode,
            'service_type' => $service_type,
            'pickup_method' => $method->pickup_method
        );

        return $arrayName;
    }


    public function manufacturing_ups_shipping($ShipTo = null, $length = NULL, $weight = NULL, $shiperInfo = NULL, $order_id = NULL, $method_id = NULL) {


        $phone = ltrim($ShipTo->Phone,'+1 ');


        $this->load->library('Upsshipping');

        /* Ship To Address */
        $this->upsshipping->addField('ShipTo_Name', $ShipTo->Name);

        $this->upsshipping->addField('ShipTo_AddressLine', array($ShipTo->AddressLine));

        $this->upsshipping->addField('ShipTo_City', $ShipTo->City);

        $this->upsshipping->addField('ShipTo_StateProvinceCode', $ShipTo->StateProvinceCode);

        $this->upsshipping->addField('ShipTo_PostalCode', $ShipTo->PostalCode);

        $this->upsshipping->addField('ShipTo_CountryCode', $ShipTo->CountryCode);

        $this->upsshipping->addField('ShipTo_Number', $phone);

        $this->upsshipping->addField('length', $length);

        $this->upsshipping->addField('weight', $weight);

        list($response, $status) = $this->upsshipping->processShipAccept($shiperInfo);

        $ups_response = json_decode($response);

        if (isset($ups_response->ShipmentResponse) && $ups_response->ShipmentResponse->Response->ResponseStatus->Code == 1) {

            $track_number = $ups_response->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber;

            $total_charges = $ups_response->ShipmentResponse->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue;

            $graphic_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage;

            $html_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->HTMLImage;

            //graphic_image_path
            $data1g = "data:image/jpeg;base64," . $graphic_image;

            $img_name_g = $track_number . '_graphic_image.jpeg';

            $source_g = fopen($data1g, 'r');

            $graphic_image = fopen("assets/b_level/shipping_img/" . $img_name_g, "w");

            stream_copy_to_stream($source_g, $graphic_image);

            fclose($source_g);

            fclose($graphic_image);

            $graphic_image_path = 'assets/b_level/shipping_img/' . $img_name_g;

            // html image
            $data1 = "data:image/jpeg;base64," . $html_image;

            $img_name = uniqid() . $track_number . '_html_image.jpeg';

            $source = fopen($data1, 'r');

            $html_image = fopen("assets/b_level/shipping_img/" . $img_name, "w");

            stream_copy_to_stream($source, $html_image);

            fclose($source);

            fclose($html_image);

            $html_image_path = 'assets/b_level/shipping_img/' . $img_name;


            $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('manufacturing_b_level_quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'grand_total'       => @$rowData->grand_total + $total_charges,
                'due'               => @$rowData->due + $total_charges,
                'shipping_charges'  => $total_charges,
                'order_stage'       => 5
            );

            //update to quatation table with pyament due
            $this->db->where('order_id', $order_id)->update('manufacturing_b_level_quatation_tbl', $orderData);
            $orderDATA = $this->db->where('order_id', $order_id)->get('manufacturing_b_level_quatation_tbl')->row();

            $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

            $upsResponseData = array(
                'customer_name' => $ShipTo->Name,
                'customer_phone' => $ShipTo->Phone,
                'order_id' => $order_id,
                'track_number' => $track_number,
                'shipping_charges' => $total_charges,
                'graphic_image' => $graphic_image_path,
                'html_image' => $html_image_path,
                'method_id' => $method_id,
                'service_type' => @$shiperInfo['service_type'],
                'shipping_addres' => $shipping_addres
            );


            $this->db->insert('manufacturing_shipment_data', $upsResponseData);



            redirect(base_url('manufacturing-order-view/'.$order_id));


        } elseif ($ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode != NULL) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors code : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code . "</p>
                <p>Description : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description . "</p>
            </div>");
            redirect("b_level/Shared_catalog_controller/manufacturing_shipment/" . $order_id);
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : This is internal error!</p>
            </div>");
            redirect("b_level/Shared_catalog_controller/manufacturing_shipment/" . $order_id);
        }
    }

    public function manufacturing_delivery_in_person($ShipTo, $order_id, $method_id) {

        $delivery_date = $this->input->post('delivery_date');
        $delivery_charge = $this->input->post('delivery_charge');
        $comment = $this->input->post('comment');

        $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('manufacturing_b_level_quatation_tbl')->row();
        // quatation table update
        $orderData = array(
            'grand_total'       => @$rowData->grand_total + @$delivery_charge,
            'due'               => @$rowData->due + @$delivery_charge,
            'shipping_charges'  => @$delivery_charge,
            'order_stage'       => 5
        );

        //update to quatation table with pyament due
        $this->db->where('order_id', $order_id)->update('manufacturing_b_level_quatation_tbl', $orderData);
        $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;


        $deliveryData = array(
            'customer_name' => $ShipTo->Name,
            'customer_phone' => $ShipTo->Phone,
            'order_id' => $order_id,
            'track_number' => '',
            'shipping_charges' => $delivery_charge,
            'graphic_image' => '',
            'html_image' => '',
            'method_id' => $method_id,
            'comment' => $comment,
            'delivery_date' => $delivery_date,
            'service_type' => '',
            'shipping_addres' => $shipping_addres
        );

        $this->db->insert('manufacturing_shipment_data', $deliveryData);

        $orderDATA = $this->db->where('order_id', $order_id)->get('manufacturing_b_level_quatation_tbl')->row();
        redirect(base_url('manufacturing-order-view/'.$order_id));
    }

    function manufacturing_order_view($order_id)
    {
        $data['orderd'] = $this->Order_model->get_manufacturing_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_manufacturing_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('manufacturing_shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=manufacturing_shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('manufacturing_shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/manufacturing_order_view', $data);
        $this->load->view('b_level/footer');
    }

    function delete_manufacturing_order($order_id)
    {
        $this->permission->check_label('order')->delete()->redirect();

        $this->db->where('order_id', $order_id)->delete('manufacturing_b_level_quatation_tbl');
        $this->db->where('order_id', $order_id)->delete('manufacturing_b_level_qutation_details');
        $this->db->where('order_id', $order_id)->delete('manufacturing_b_level_quatation_attributes');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Delete successfully!</div>");
        redirect("manufacturing-catalog-order-list");
    }

    function set_manufacturing_order_stage($stage, $order_id)
    {
        if (!empty($stage) && !empty($order_id))
        {

            $order = $this->db->where('order_id', $order_id)->get('manufacturing_b_level_quatation_tbl')->row();


            //$this->shipping($order_id);
            $this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('manufacturing_funb_level_quatation_tbl');

            //$this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('quatation_tbl');

            if ($stage == 1)
            {
                $order_stage = 'Quote';
            }
            if ($stage == 2)
            {
                $order_stage = 'Paid';
            }
            if ($stage == 3)
            {
                $order_stage = 'Partially Paid';
            }
            if ($stage == 4)
            {
                $order_stage = 'Manufacturing';
            }
            if ($stage == 5)
            {
                $order_stage = 'Shipping';
            }
            if ($stage == 6)
            {
                $order_stage = 'Cancelled';
            }
            if ($stage == 7)
            {
                $order_stage = 'Delivered';
            }

            echo 1;

        } else
        {
            echo 2;
        }
    }

    function catalog_b_user_order_list()
    {
        $this->permission->check_label('order')->read()->redirect();

        $search = (object)array(
            'product_id' => $this->input->post('product_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_date' => $this->input->post('order_date'),
            'order_stage' => $this->input->post('order_stage')
        );


        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->customer_id;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;

        //total row count for pasination
        $total_rows = $this->db->where('level_id', $this->level_id)->count_all('b_to_b_level_quatation_tbl');

        $per_page = 15;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        //-----------

        $data['orderd'] = $this->Order_model->get_b_to_b_all_orderd($search, $per_page, $page,$status = 'FROM_B_USER');
        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/b_user_order_list', $data);
        $this->load->view('b_level/footer');
    }

    function b_user_receipt($order_id)
    {
        $this->permission->check_label('order')->read()->redirect();

        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);

        $data['order_details'] = $this->Order_model->get_b_to_b_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('b_to_b_shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=b_to_b_shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('b_to_b_shipment_data')->row();


        $data['company_profile'] = $query = $this->db->select('*')
            ->from('company_profile')
            ->where('user_id', $data['orderd']->b_user_id)
            ->get()->result();

        $data['created_by_data'] = $this->db->where('id', $data['orderd']->created_by)->get('user_info')->row();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/b_user_order_receipt', $data);
        $this->load->view('b_level/footer');
    }


    function payment_b_to_b($order_id)
    {
        $this->permission->check_label('order')->read()->redirect();

        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);

        $data['order_details'] = $this->Order_model->get_b_to_b_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('b_to_b_shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=b_to_b_shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('b_to_b_shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/make_payment_b_to_b', $data);
        $this->load->view('b_level/footer');
    }

    function make_order_payment_update()
    {
        $b_user_id = $this->input->post('b_user_id');
        //$customer_no    = $this->input->post('customer_no');
        $payment_method = $this->input->post('payment_method');
        $order_id = $this->input->post('order_id');


        $cHead = $this->db->select('HeadCode')->where('HeadName', 'CUS-4672-first customer')->get('b_acc_coa')->row();

        if ($cHead->HeadCode != NULL)
        {

            $user_info = $this->db->where('id', $b_user_id)->get('user_info')->row();

            if ($payment_method == 'paypal')
            {


                $orderData = array(
                    'order_id' => $order_id,
                    'b_user_id' => $b_user_id,
                    'paid_amount' => $this->input->post('paid_amount'),
                );

                $orderDataPaypal = array(

                    'order_id' => $order_id,
                    'headcode' => $cHead->HeadCode,
                    'grand_total' => $this->input->post('grand_total'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'payment_method' => $this->input->post('payment_method'),
                    'card_number' => $this->input->post('card_number'),
                    'issuer' => $this->input->post('issuer'),
                    /*   'cusstomer_user_id' => $cusstomer_info->customer_user_id,*/
                    'b_user_id' => $b_user_id,

                );

                $this->session->set_userdata($orderDataPaypal);

                $pyament = $this->payment_by_paypal($orderData,'MAKE_PAYMENT');


            } else
            {


                $due = $this->input->post('due');

                if ($due > 0)
                {
                    $order_stage = 3;
                } else
                {
                    $order_stage = 2;
                }

                $rowData = $this->db->select('due,paid_amount,clevel_order_id,created_by,commission_amt')->where('order_id', $order_id)->get('b_to_b_level_quatation_tbl')->row();


                // Get B Level User info for get commission : START
                $created_by = @$rowData->created_by;
                $commission_amt = @$rowData->commission_amt;
                if ($created_by != '')
                {
                    $user_data = $this->db->select('fixed_commission,percentage_commission')->where('id', @$rowData->created_by)->get('user_info')->row();

                    if (@$user_data->fixed_commission != '' && $commission_amt == 0)
                    {
                        $commission_amt_fixed = @$user_data->fixed_commission;
                    } else
                    {
                        $commission_amt_fixed = 0;
                    }

                    if (@$user_data->percentage_commission != '' && @$user_data->percentage_commission > 0)
                    {
                        $commission_amt_percentage = round((($this->input->post('paid_amount') * @$user_data->percentage_commission) / 100), 2);
                    } else
                    {
                        $commission_amt_percentage = 0;
                    }

                    $commission_amt += ($commission_amt_fixed + $commission_amt_percentage);
                }
                // Get B Level User info for get commission : END

                // quatation table update
                $orderData = array(
                    'paid_amount' => @$rowData->paid_amount + $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'order_stage' => $order_stage,
                    'commission_amt' => $commission_amt
                );

                if ($rowData->order_id != NULL)
                {
                    $this->db->set('order_stage', $order_stage)->where('order_id', $rowData->order_id)->update('b_to_b_level_quatation_tbl');
                }

                //update to quatation table with pyament due
                $this->db->where('order_id', $order_id)->update('b_to_b_level_quatation_tbl', $orderData);
                //-----------------------------------------
                // quatation table update
                $payment_tbl = array(
                    'quotation_id' => $order_id,
                    'payment_method' => $this->input->post('payment_method'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'payment_date' => date('Y-m-d'),
                    'created_by' => $this->user_id,
                    'create_date' => date('Y-m-d')
                );


                if ($this->db->insert('payment_tbl', $payment_tbl))
                {

                    $payment_id = $this->db->insert_id();

                    if ($payment_method == 'check')
                    {

                        if (@$_FILES['check_image']['name'])
                        {

                            $config['upload_path'] = './assets/b_level/uploads/check_img/';
                            $config['allowed_types'] = 'jpeg|jpg';
                            $config['overwrite'] = false;
                            $config['max_size'] = 3000;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('check_image'))
                            {
                                $error = $this->upload->display_errors();
                                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                                redirect("new-order");
                            } else
                            {

                                $data = $this->upload->data();
                                $check_image = $config['upload_path'] . $data['file_name'];
                            }
                        } else
                        {
                            @$check_image = '';
                        }

                        $payment_check_tbl = array(
                            'payment_id' => $payment_id,
                            'check_number' => $this->input->post('check_number'),
                            'check_image' => $check_image,
                        );

                        $this->db->insert('payment_check_tbl', $payment_check_tbl);

                    }

                }


                $voucher_no = $order_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $this->input->post('paid_amount');
                $cAID = $cHead->HeadCode;
                $IsPosted = 1;
                $CreateBy = $this->user_id;
                $createdate = date('Y-m-d H:i:s');


                $payment_method = $this->input->post('payment_method');

                if ($payment_method == 'cash')
                {
                    $COAID = '1020101';
                }
                if ($payment_method == 'check')
                {
                    $COAID = '102010202';
                }
                if ($payment_method == 'card')
                {
                    $COAID = '1020103';
                }

                // C level transection start
                //C level credit insert b_acc_transaction
                $customerCredit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $COAID,
                    'Narration' => "Paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);
                //------------------------------------

                //B level debit insert b_acc_transaction

                $bCOA = '502020101';
                /*  $b_levelDebit = array(

                      'VNo' => $voucher_no,
                      'Vtype' => $Vtype,
                      'VDate' => $VDate,
                      'Debit' => $paid_amount,
                      'Credit' => 0,
                      'COAID' => $bCOA,
                      'Narration' => "Amount received for invoice #" . $voucher_no,
                      'IsPosted' => $IsPosted,
                      'CreateBy' => $CreateBy,
                      'CreateDate' => $createdate,
                      'IsAppove' => 1
                  );*///b_acc_transaction

                /*    $this->db->insert('acc_transaction', $b_levelDebit);*/
                // C level transection END

                //. B level transection start
                //customer credit insert b_acc_transaction
                $customerCredit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $cAID,
                    'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('b_acc_transaction', $customerCredit);
                //------------------------------------

                //b_level debit insert b_acc_transaction

                $b_levelDebit = array(

                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $COAID,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );//b_acc_transaction

                $this->db->insert('b_acc_transaction', $b_levelDebit);
                //- B level transection END----------------------

                // C level notification

                /*   $cNotificationData = array(

                       'notification_text' => 'Payment has been done for ' . $order_id,
                       'go_to_url' => '#',
                       'created_by' => $cusstomer_info->customer_user_id,
                       'date' => date('Y-m-d')

                   );

                   $this->db->insert('c_notification_tbl', $cNotificationData);*/

                //-------------------------
                //

                /* $bNotificationData = array(

                     'notification_text' => 'Payment has been received for ' . $order_id,
                     'go_to_url' => 'b_level/invoice_receipt/receipt/' . $order_id,
                     'created_by' => $cusstomer_info->customer_user_id,
                     'date' => date('Y-m-d')
                 );
                 $this->db->insert('b_notification_tbl', $bNotificationData);*/

                //--------------------------


                /* $this->email_sender->send_email(
                     $data = array(
                         'customer_id' => $customer_id,
                         'message' => 'Payment has been done for Order id' . $order_id . ', Paid amount:' . $paid_amount . ', Payment method:' . $payment_method,
                         'subject' => 'Order payment'
                     )
                 );*/


                // Send sms
                /*   $this->Order_model->smsSend($data = array(
                       'customer_id' => $customer_id,
                       'message' => 'Payment has been done for Order id' . $order_id . ', Paid amount: ' . $paid_amount . ', Payment method:' . $payment_method,
                       'subject' => 'Order payment'
                   ));*/


                //--------------------------
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                redirect('b_level/Shared_catalog_controller/b_user_money_receipt/' . $order_id);

            }

        } else
        {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer account code not found! </div>");
            redirect(base_url('b_level/Shared_catalog_controller/payment_b_to_b/' . $order_id));

        }
    }


    public function b_user_shipment($order_id = NULL)
    {

        $this->permission->check_label('order')->read()->redirect();


        $shipDataCheck = $this->db->where('order_id', $order_id)->get('b_to_b_shipment_data')->num_rows();

        if ($shipDataCheck > 0)
        {

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment already done, can't back process</div>");
            redirect("b_level/Shared_catalog_controller/payment_b_to_b/" . $order_id);

        }


        $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
        $data['methods'] = $this->db->where('status', 1)->get('shipping_method')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/shared_catalog/shipment_form', $data);
        $this->load->view('b_level/footer');


    }

}
