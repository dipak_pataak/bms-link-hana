<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Condition_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Condition_model');
    }


    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//     =========== its for add_condition =============
    public function add_condition() {
        $this->permission->check_label('add_condition')->create()->redirect();
        $data['get_category'] = $this->Category_model->get_category();
        $data['get_width_height'] = $this->db->get('width_height_fractions')->result();
        $config["base_url"] = base_url('b_level/Condition_controller/add_condition');
        $config["total_rows"] = $this->db->count_all('product_conditions_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["condition_list"] = $this->Condition_model->condition_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/condition/add_condition');
        $this->load->view('b_level/footer');
    }

//    ============= its for condition_save ========
    public function condition_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Condition information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $createdate = date('Y-m-d');
        $category_id = $this->input->post('category_id');
        $width_condition = $this->input->post('width_condition');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_condition = $this->input->post('height_condition');
        $height_fraction_id = $this->input->post('height_fraction_id');

        $condition_data = array(
            'category_id' => $category_id,
            'width_condition' => $width_condition,
            'width_fraction_id' => $width_fraction_id,
            'height_condition' => $height_condition,
            'height_fraction_id' => $height_fraction_id,
            'created_by' => $this->user_id,
            'create_date' => $createdate,
            'is_active' => 1
        );
//        echo '<pre>';        print_r($condition_data); die();
        $this->db->insert('product_conditions_tbl', $condition_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Condition save successfully!</div>");
        redirect("add-condition");
    }

//    =============== its for condition_edit ===================
    public function condition_edit($id) {
        $data['get_category'] = $this->Category_model->get_category();
        $data['get_width_height'] = $this->db->get('width_height_fractions')->result();
        $data['condition_edit'] = $this->Condition_model->condition_edit($id);
//        ======== its for condition list in edit page ======
        $config["base_url"] = base_url('b_level/Condition_controller/add_condition');
        $config["total_rows"] = $this->db->count_all('product_conditions_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["condition_list"] = $this->Condition_model->condition_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/condition/condition_edit');
        $this->load->view('b_level/footer');
    }

//    ============= its for condition_update=================
    public function condition_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "condition information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $updated_date = date('Y-m-d');
        $category_id = $this->input->post('category_id');
        $width_condition = $this->input->post('width_condition');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_condition = $this->input->post('height_condition');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $status = $this->input->post('status');


        $condition_data = array(
            'category_id' => $category_id,
            'width_condition' => $width_condition,
            'width_fraction_id' => $width_fraction_id,
            'height_condition' => $height_condition,
            'height_fraction_id' => $height_fraction_id,
            'updated_by' => $this->user_id,
            'update_date' => $updated_date,
            'is_active' => $status,
        );
//        echo '<pre>';        print_r($condition_data); die();
        $this->db->where('condition_id', $id);
        $this->db->update('product_conditions_tbl', $condition_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Condition updated successfully!</div>");
        redirect("add-condition");
    }

//    ================ its for condition_delete ===========
    public function condition_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "condition information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $condition_check_from_product = $this->Condition_model->condition_check_from_product($id);
        if (!empty($condition_check_from_product[0]->condition_id)) {
            $this->session->set_flashdata('error', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Its has child record !</div>");
            redirect("add-condition");
        } else {
            $this->db->where('condition_id', $id);
            $this->db->delete('product_conditions_tbl');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Data deleted successfully !</div>");
            redirect("add-condition");
        }
    }

//    ===============its for add_tax ===============
    public function add_tax() {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/condition/tax_add');
        $this->load->view('b_level/footer');
    }

//    ============== its for category_wise_condition ===========
    public function category_wise_condition($category_id) {

        $data['category_wise_condition'] = $this->Condition_model->category_wise_condition($category_id);

        $this->load->view('b_level/products/category_wise_condition', $data);
    }

//    ========= its for condition_filter =============
    public function condition_filter() {
        $data['get_width_height'] = $this->db->get('width_height_fractions')->result();
        $data['get_category'] = $this->Category_model->get_category();
        $data['category_name'] = $this->input->post('category_name');
        $data['get_condition_filter'] = $this->Condition_model->get_condition_filter($data['category_name']);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/condition/condition_filter');
        $this->load->view('b_level/footer');
    }

}
