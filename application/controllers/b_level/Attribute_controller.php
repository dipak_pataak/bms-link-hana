<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        
        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Attribute_model');
        $this->load->model('b_level/Category_model');
    }



    public function index() {

        //        $this->load->view('b_level/header');
        //        $this->load->view('b_level/sidebar');
        //        $this->load->view('b_level/customers/customer_edit');
        //        $this->load->view('b_level/footer');
    }



    //===============its for add_attribute ===============
    public function add_attribute() {

        $this->permission->check_label('add_attribute')->create()->redirect();
        //$data['get_attribute_type'] = $this->Attribute_model->get_attribute_type();
        $data['categorys'] = $this->db->select('category_name,category_id')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $this->level_id)
            ->get('category_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_add', $data);
        $this->load->view('b_level/footer');
    }

    //========== its for save_attribute ===========
    public function save_attribute() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "attribute information save";
        
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks'       => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $createdate = date('Y-m-d');
        $attribute = $this->input->post('attribute');
        $attribute_type = $this->input->post('attribute_type');
        $category = $this->input->post('category');
        $position = $this->input->post('position');

        $attributeData = array(
            'attribute_name' => $attribute,
            'attribute_type' => $attribute_type,
            'category_id' => $category,
            'position' => $position,
            'created_by' => $this->user_id,
            'created_date' => $createdate,
        );

        $this->db->insert('attribute_tbl', $attributeData);
        $attribute_id = $this->db->insert_id();

        $option_name = $this->input->post('option_name');
        $option_type = $this->input->post('option_type');
        $price_type = $this->input->post('price_type');
        $price = $this->input->post('price');
        $condition = $this->input->post('condition');

        $option_optin_name = $this->input->post('option_option_name');
        $option_option_type = $this->input->post('option_option_type');
        $op_op_price = $this->input->post('op_op_price');
        $op_op_price_type = $this->input->post('op_op_price_type');
        $op_op_condition = $this->input->post('op_op_condition');

        $op_op_op_name = $this->input->post('op_op_op_name');
        $op_op_op_type = $this->input->post('op_op_op_type');
        $op_op_op_price_type = $this->input->post('op_op_op_price_type');
        $op_op_op_price = $this->input->post('op_op_op_price');
        $op_op_op_condition = $this->input->post('op_op_op_condition');

        //print_r($op_op_op_name);
        //echo "<pre>";

        if (!empty($option_name)) {

            $k = 0;
            foreach ($option_name as $option) {

                $optionData = array(
                    'option_name' => $option,
                    'option_type' => $option_type[$k],
                    'attribute_id' => $attribute_id,
                    'price' => $price[$k],
                    'price_type' => $price_type[$k],
                    'op_condition' => $condition[$k]
                );

                //print_r($optionData); 

                $this->db->insert('attr_options', $optionData);
                $att_op_id = $this->db->insert_id();

                if (!empty($option_optin_name[$k])) {

                    $o = 0;
                    $oi = 0;

                    foreach ($option_optin_name[$k] as $option_option) {

                        $optionOptionData = array(
                            'op_op_name' => $option_option,
                            'type' => @$option_option_type[$k][$oi],
                            'option_id' => @$att_op_id,
                            'attribute_id' => @$attribute_id,
                            'att_op_op_price_type' => @$op_op_price_type[$k][$oi],
                            'att_op_op_price' => @$op_op_price[$k][$oi],
                            'att_op_op_condition' => @$op_op_condition[$k][$oi],
                        );

                        //print_r($optionOptionData);

                        if ($this->db->insert('attr_options_option_tbl', $optionOptionData)) {

                            $op_op_id = $this->db->insert_id();

                            //echo $k."_".$o;
                            //print_r($op_op_op_name[1][3]);

                            if (!empty($op_op_op_name[$k][$o])) {

                                //print_r($op_op_op_name[$k][$o]);

                                $j = 0;

                                foreach ($op_op_op_name[$k][$o] as $op_op_op) {

                                    $OpOpOpData = array(
                                        'att_op_op_op_name' => $op_op_op,
                                        'att_op_op_op_type' => @$op_op_op_type[@$k][@$o][@$j],
                                        'att_op_op_op_price_type' => $op_op_op_price_type[$k][$o][$j],
                                        'att_op_op_op_price' => $op_op_op_price[$k][$o][$j],
                                        'att_op_op_op_condition' => $op_op_op_condition[$k][$o][$j],
                                        'op_id' => @$att_op_id,
                                        'att_op_op_id' => @$op_op_id,
                                        'attribute_id' => @$attribute_id
                                    );

                                    $j++;

                                    //print_r($OpOpOpData);

                                    $this->db->insert('attr_options_option_option_tbl', $OpOpOpData);
                                }
                            }
                        }

                        $oi++;
                        $o++;
                    }
                }

                $k++;
            }

            //exit;
        }


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute save successfully!</div>");
        redirect("add-attribute");
    }

//  ===============its for manage_attribute ===============
    public function manage_attribute() {

        $this->permission->check_label('manage_attribute')->read()->redirect();

        $total_row = $this->db->from("attribute_tbl")->where('created_by',$this->level_id)->count_all_results();

        $data['get_category'] = $this->Category_model->get_category();
        $config["base_url"] = base_url('b_level/Attribute_controller/manage_attribute');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["attribute_list"] = $this->Attribute_model->attribute_list($config["per_page"], $page);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $data['js'] = "b_level/attributes/attribute_js";

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_manage', $data);
        $this->load->view('b_level/footer');
    }

    public function option_option_option_option_save() {

        $option_name = $this->input->post('option_name');
        $att_op_op_op_type = $this->input->post('op_op_op_op_type');
        $att_op_op_op_price_type = $this->input->post('op_op_op_op_price_type');
        $att_op_op_op_price = $this->input->post('op_op_op_op_price');
        $att_op_op_op_condition = $this->input->post('op_op_op_op_condition');

        $op_op_op_id = $this->input->post('op_op_op_id');

        $data = $this->db->where('att_op_op_op_id', $op_op_op_id)->get('attr_options_option_option_tbl')->row();


        $saveData = [];
        foreach ($option_name as $key => $value) {

            $saveData[] = array(
                'att_op_op_op_op_name' => $value,
                'att_op_op_op_op_type' => $att_op_op_op_type[$key],
                'att_op_op_op_op_price_type' => $att_op_op_op_price_type[$key],
                'att_op_op_op_op_price' => $att_op_op_op_price[$key],
                'att_op_op_op_op_condition' => $att_op_op_op_condition[$key],
                'op_id' => $data->op_id,
                'op_op_id' => $data->att_op_op_id,
                'op_op_op_id' => $op_op_op_id,
                'attribute_id' => $data->attribute_id
            );
        }

        $this->db->insert_batch('attr_op_op_op_op_tbl', $saveData);

        echo 1;
    }

    public function option_option_option_option_delete($att_op_op_op_op_id) {

        $this->db->where('att_op_op_op_op_id', $att_op_op_op_op_id)->delete('attr_op_op_op_op_tbl');
        echo 1;
    }

    public function attribute_edit($id) {

        $this->permission->check_label('attribute')->update()->redirect();

        $data['attribute'] = $this->db->where('attribute_id', $id)->where('created_by',$this->level_id)->get('attribute_tbl')->row();
        $data['option'] = $this->db->where('attribute_id', $id)->get('attr_options')->result();
        $data['categorys'] = $this->db->select('category_name,category_id')->where('created_by',$this->level_id)->get('category_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_edit', $data);
        $this->load->view('b_level/footer');
    }

//    ========== its for attribute_update ===========
    public function attribute_update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "attribute information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'levle_id' => $this->levle_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $updated_date = date('Y-m-d');
        $attribute = $this->input->post('attribute');
        $attribute_type = $this->input->post('attribute_type');
        $category = $this->input->post('category');
        $position = $this->input->post('position');

        $attribute_id = $this->input->post('attribute_id');

        $attributeData = array(
            'attribute_name' => $attribute,
            'attribute_type' => $attribute_type,
            'category_id' => $category,
            'position' => $position,
            'updated_by' => $this->user_id,
            'updated_date' => $updated_date,
        );

        $this->db->where('attribute_id', $attribute_id)->update('attribute_tbl', $attributeData);


        $option_ids = $this->input->post('option_id');
        $option_name = $this->input->post('option_name');
        $option_type = $this->input->post('option_type');
        $price_type = $this->input->post('price_type');
        $price = $this->input->post('price');
        $condition = $this->input->post('condition');

        $op_op_ids = $this->input->post('op_op_id');
        $option_optin_name = $this->input->post('option_option_name');
        $option_option_type = $this->input->post('option_option_type');
        $att_op_op_price_type = $this->input->post('att_op_op_price_type');
        $att_op_op_price = $this->input->post('att_op_op_price');
        $att_op_op_condition = $this->input->post('att_op_op_condition');


        $op_op_op_ids = $this->input->post('op_op_op_id');
        $op_op_op_name = $this->input->post('op_op_op_name');
        $op_op_op_type = $this->input->post('op_op_op_type');
        $op_op_op_price_type = $this->input->post('op_op_op_price_type');
        $op_op_op_price = $this->input->post('op_op_op_price');
        $op_op_op_condition = $this->input->post('op_op_op_condition');

        //echo "<pre>";

        $k = 0;
        for ($k = 0; $k <= count($option_ids); $k++) {


            if (array_key_exists($k, $option_ids)) {

                $optionData = array(
                    'option_name' => $option_name[$k],
                    'option_type' => $option_type[$k],
                    'attribute_id' => $attribute_id,
                    'price' => $price[$k],
                    'price_type' => $price_type[$k],
                    'op_condition' => $condition[$k]
                );

                //print_r($optionData); 

                $this->db->where('att_op_id', $option_ids[$k])->update('attr_options', $optionData);

                if (!empty($op_op_ids[@$k])) {

                    $o = 0;

                    for ($o = 0; $o <= count($op_op_ids[$k]); $o++) {

                        if (array_key_exists($o, $op_op_ids[$k])) {

                            $optionOptionData = array(
                                'op_op_name' => $option_optin_name[@$k][@$o],
                                'type' => $option_option_type[$k][$o],
                                'option_id' => $option_ids[$k],
                                'attribute_id' => $attribute_id,
                                'att_op_op_price_type' => $att_op_op_price_type[$k][$o],
                                'att_op_op_price' => $att_op_op_price[$k][$o],
                                'att_op_op_condition' => $att_op_op_condition[$k][$o]
                            );
                            //print_r($optionOptionData); 

                            $this->db->where('op_op_id', $op_op_ids[$k][$o])->update('attr_options_option_tbl', $optionOptionData);

                            if (!empty($op_op_op_ids[@$k][$o])) {

                                for ($j = 0; $j <= count($op_op_op_ids[$k][$o]); $j++) {

                                    if (!empty($op_op_op_ids[$k][$o][$j])) {

                                        $optionOptionOptionData = array(
                                            'att_op_op_op_name' => $op_op_op_name[@$k][@$o][$j],
                                            'att_op_op_op_type' => $op_op_op_type[$k][$o][$j],
                                            'attribute_id' => $attribute_id,
                                            'op_id' => $option_ids[$k],
                                            'att_op_op_id' => $op_op_ids[$k][$o],
                                            'att_op_op_op_price_type' => $op_op_op_price_type[$k][$o][$j],
                                            'att_op_op_op_price' => $op_op_op_price[$k][$o][$j],
                                            'att_op_op_op_condition' => $op_op_op_condition[$k][$o][$j]
                                        );

                                        // print_r($optionOptionOptionData);
                                        $this->db->where('att_op_op_op_id', $op_op_op_ids[$k][$o][$j])->update('attr_options_option_option_tbl', $optionOptionOptionData);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute updated successfully!</div>");
        redirect("attribute-edit/" . $attribute_id);
    }

//    ============== its for attribute_type ===============
    public function attribute_type() {

        $config["base_url"] = base_url('b_level/Attribute_controller/attribute_type');
        $config["total_rows"] = $this->db->count_all('attribute_type_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["attribute_type_list"] = $this->Attribute_model->attribute_type_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_type_list', $data);
        $this->load->view('b_level/footer');
    }

//    =========== its for attribute_type_save ===============
    public function attribute_type_save() {

        $attribute_type_save = $this->input->post('types_name');
        $attribute_type_data = array(
            'attribute_type_name' => $attribute_type_save
        );
        $this->db->insert('attribute_type_tbl', $attribute_type_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute types save successfully!</div>");
        redirect('attribute-type');
    }

//    ============ its for uom_edit ==============
    public function attribute_type_edit($id) {
        $data['attribute_type_edit'] = $this->Attribute_model->attribute_type_edit($id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_type_edit', $data);
        $this->load->view('b_level/footer');
    }

//    =========== its for uom_update ===============
    public function attribute_type_update($id) {
        $types_name = $this->input->post('types_name');
        $attribute_type_data = array(
            'attribute_type_name' => $types_name
        );
        $this->db->where('attribute_type_id', $id);
        $this->db->update('attribute_type_tbl', $attribute_type_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute types updated successfully!</div>");
        redirect('attribute-type');
    }



    public function get_attr_by_attr_types($type_id) {

        $query = $this->db->select("*")
                        ->from('attribute_tbl')
                        ->where('attribute_type_id', $type_id)
                        ->get()->result();

        echo '<table width="80%" class="border-0" border="0">';

        foreach ($query as $key => $value) {

            echo '<tr>
                        <td id="Prod_01" class="border-0"><input type="checkbox" class="checkboxes" name="attribute_id[]" value="' . $value->attribute_id . '" onChange="shuffle_pricebox(' . $value->attribute_id . ');"> </td>
                        <td class="border-0"><b>' . $value->attribute_name . '</b></td>
                        <td class="border-0"><input type="number" name="attribute_price_' . $value->attribute_id . '" id="' . $value->attribute_id . '" placeholder="$0.00" disabled></td>
                        <td class="border-0">
                            <div><input type="radio" name="price_type_' . $value->attribute_id . '" value="1" checked> $ &nbsp;
                            <input type="radio" name="price_type_' . $value->attribute_id . '" value="2"> %</div>
                        </td>
                    </tr>';
        }
        echo '</table>';
    }




    public function attribute_delete($attr_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "attribute information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => @$this->user_id,
            'level_id' => @$this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $this->db->where('attribute_id', $attr_id)->delete('attribute_tbl');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options_option_tbl');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options_option_option_tbl');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute delete successfully!</div>");
        redirect('manage-attribute');
    }




//============= its for attribute_filter ============
    public function attribute_filter() {

        $data['get_category'] = $this->Category_model->get_category();
        $data['attribute_name'] = $this->input->post('attribute_name');
        $data['category_name'] = $this->input->post('category_name');

        $data['get_attribute_filter'] = $this->Attribute_model->attribute_filter($data['attribute_name'], $data['category_name']);

        $data['js'] = "b_level/attributes/attribute_js";

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/get_attribute_filter', $data);
        $this->load->view('b_level/footer');
    }

//    ========== its for b_level_attribute_search =============
    public function b_level_attribute_search() {
        $keyword = $this->input->post('keyword');
        $data['attribute_list'] = $this->Attribute_model->attribute_onkeysearch($keyword);

        $this->load->view('b_level/attributes/attribute_search', $data);
    }
/** Start added by insys */
//    =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('attribute_tbl','attribute_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Attribute has been Deleted successfully.</div>");
        }
        redirect("manage-attribute");
    } 
/** End added by insys */
}
