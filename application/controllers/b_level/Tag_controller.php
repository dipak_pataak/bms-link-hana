<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_Controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');


        $this->load->model(array(
            'b_level/pattern_model', 'b_level/Settings'
        ));
    }

    public function index() {
        
    }

    public function add_tag() {

        $total_row = $this->db->where('created_by',$this->level_id)->count_all_results('tag_tbl');

        $this->permission->check_label('tag')->create()->redirect();
        $config["base_url"] = base_url('b_level/Tag_controller/add_tag');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["tags"] = $this->db->from('tag_tbl')->where('created_by',$this->level_id)->order_by('tag_tbl.id', 'desc')->limit($config["per_page"], $page)->get()->result();
        $data["links"] = $this->pagination->create_links();
        $data["patterns"] = $this->db->get('pattern_model_tbl')->result();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/tag/add_tag', $data);
        $this->load->view('b_level/footer');
    }

    //======== its for exists get_tag check ===========
    public function get_tag_check($tag) {
        $query = $this->db->where('tag_name', $tag)
                ->get('tag_tbl')
                ->row();
        if (!empty($query)) {
            echo $query->tag_name;
        } else {
            echo 0;
        }
    }

    public function save_tag() {
        $tagData = array(
            'tag_name' => $this->input->post('tag_name'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d')
        );
        $this->db->insert('tag_tbl', $tagData);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Tag information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Tag save successfull </div>");
        redirect('gallery_tag');
    }

    public function delete_tag($id) {
        $this->db->where('id', $id)->delete('tag_tbl');
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "Tag information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Tag deleted successfully! </div>");
        redirect('gallery_tag');
    }

    public function get_tag($id) {
        $result = $this->db->where('id', $id)->get('tag_tbl')->row();
        echo json_encode($result);
    }

    public function update_tag() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "Tag information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $tagData = array(
            'tag_name' => $this->input->post('tag_name'),
            'updated_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d')
        );

        $id = $this->input->post('id');

        $this->db->where('id', $id)->update('tag_tbl', $tagData);

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Tag update successfull </div>");
        redirect('gallery_tag');
    }

//    =========== its for import_tag_save ===========
    public function import_tag_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "tag csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['tag_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                }
                $data = array(
                    'tag_name' => $insert_csv['tag_name'],
                    'created_by' => $this->user_id,
                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
//                echo $data['email'];
                    $result = $this->db->select('*')
                            ->from('tag_tbl')
                            ->where('tag_name', $data['tag_name'])
//                            ->or_where('color_number', $data['color_number'])
                            ->get()
                            ->num_rows();
//                echo '<pre>';                print_r($result);die();
                    if ($result == 0 && !empty($data['tag_name'])) {
                        $this->db->insert('tag_tbl', $data);
                    } else {
                        $data = array(
//                            'customer_id' => $insert_csv['customer_id'],
                            'tag_name' => $insert_csv['tag_name'],
                            'updated_by' => $this->user_id,
                            'updated_date' => date('Y-m-d'),
                        );
                        $this->db->where('tag_name', $data['tag_name']);
                        $this->db->update('tag_tbl', $data);
//                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Already exists!</div>");
//                        redirect('customer-import');
                    }
                }
                $count++;
            }
        }
//        echo "DUke Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Tag imported successfully!</div>");
        redirect('gallery_tag');
    }

//    =============== b_level_pattern_search ============
    public function b_level_tag_search() {
        $keyword = $this->input->post('keyword');
        $data['tags'] = $this->db->select('*')->from('tag_tbl')->like('tag_name', $keyword, 'both')
                        ->order_by('id', 'desc')->limit(50)->get()->result();
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/tag/tag_search', $data);
    }

}
