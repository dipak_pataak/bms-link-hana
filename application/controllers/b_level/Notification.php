<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->model('b_level/Order_model');
    }

    public function notifications()
    {


        $customer_id = $this->input->post('customer_id');

        $from_date = ($this->input->post('from_date')?$this->input->post('from_date'):date('Y-m-d'));

        $to_date = $this->input->post('to_date');


        $this->db->select("b_notification_tbl.*,CONCAT(user_info.first_name, '.', user_info.last_name) as fullname, company");
        $this->db->join('user_info','user_info.id=b_notification_tbl.created_by','left');
        
        if($customer_id!=NULL){
            $this->db->where('b_notification_tbl.created_by',$customer_id);
        }

        if($from_date!=NULL && $to_date!=NULL) {
            $this->db->where('b_notification_tbl.date>=',$from_date);
            $this->db->where('b_notification_tbl.date<=',$to_date);
        } else {
            $this->db->where('b_notification_tbl.date',$from_date);
        }
        $this->db->order_by('id','DESC');

        $data['notification'] = $this->db->get('b_notification_tbl')->result();

        $data['get_customer']       = $this->Order_model->get_customer();



        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/notification', $data);
        $this->load->view('b_level/footer');
    }



}