<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_controller extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
//        $this->load->model('c_level/Customer_model');
//        $this->load->model('c_level/User_model');
        $this->load->model('b_level/BAccount_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    =============== its for b_account_chart ==============
    public function b_account_chart() {
        $this->permission->check_label('product_return')->create()->redirect();
        $data['userList'] = $this->BAccount_model->get_userlist();
     //  print_r($data['userList']);die();



        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/account_chart', $data);
        $this->load->view('b_level/footer');
    }

    public function selectedform($id) {
        $isactive = '';
        $role_reult = $this->db->select('*')
                ->from('b_acc_coa')
                ->where('HeadCode', $id)
                ->get()
                ->row();
        $baseurl = base_url() . 'b-insert-coa';
//        echo json_encode($role_reult);        exit();

        if ($role_reult) {
            $html = "";
            $html .= "
        <form name=\"form\" id=\"form\" action=\"" . $baseurl . "\" method=\"post\" enctype=\"multipart/form-data\">
                <div id=\"newData\">
   <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
    
      <tr>
        <td>Head Code</td>
        <td><input type=\"text\" name=\"txtHeadCode\" id=\"txtHeadCode\" class=\"form_input\"  value=\"" . $role_reult->HeadCode . "\" readonly=\"readonly\"/></td>
      </tr>
      <tr>
        <td>Head Name</td>
        <td><input type=\"text\" name=\"txtHeadName\" id=\"txtHeadName\" class=\"form_input\" value=\"" . $role_reult->HeadName . "\"/>
<input type=\"hidden\" name=\"HeadName\" id=\"HeadName\" class=\"form_input\" value=\"" . $role_reult->HeadName . "\"/>
        </td>
      </tr>
      <tr>
        <td>Parent Head</td>
        <td><input type=\"text\" name=\"txtPHead\" id=\"txtPHead\" class=\"form_input\" readonly=\"readonly\" value=\"" . $role_reult->PHeadName . "\"/></td>
      </tr>
      <tr>

        <td>Head Level</td>
        <td><input type=\"text\" name=\"txtHeadLevel\" id=\"txtHeadLevel\" class=\"form_input\" readonly=\"readonly\" value=\"" . $role_reult->HeadLevel . "\"/></td>
      </tr>
       <tr>
        <td>Head Type</td>
        <td><input type=\"text\" name=\"txtHeadType\" id=\"txtHeadType\" class=\"form_input\" readonly=\"readonly\" value=\"" . $role_reult->HeadType . "\"/></td>
      </tr>

       <tr>
        <td>&nbsp;</td>
        <td><input type=\"checkbox\" name=\"IsTransaction\" value=\"1\" id=\"IsTransaction\" size=\"28\"  onchange=\"IsTransaction_change();\"";
            if ($role_reult->IsTransaction == 1) {
                $html .= "checked";
            }

            $html .= "/><label for=\"IsTransaction\"> IsTransaction</label>
        <input type=\"checkbox\" value=\"1\" name=\"IsActive\" id=\"IsActive\"";
            if ($role_reult->IsActive == 1) {
                $html .= "checked";
            }
            $html .= " size=\"28\" checked=\"" . $isactive . "\" /><label for=\"IsActive\"> IsActive</label>
        <input type=\"checkbox\" value=\"1\" name=\"IsGL\" id=\"IsGL\" size=\"28\"";
            if ($role_reult->IsGL == 1) {
                $html .= "checked";
            }
            $html .= " onchange=\"IsGL_change();\"/><label for=\"IsGL\"> IsGL</label>

        </td>
      </tr>
       <tr>
                    <td>&nbsp;</td>
                    <td>";
//            if ($this->permission->method('accounts', 'create')->access()):
            $html .= "<input type=\"button\" name=\"btnNew\" id=\"btnNew\" value=\"New\" onClick=\"newdata(" . $role_reult->HeadCode . ")\" />
                     <input type=\"submit\" name=\"btnSave\" id=\"btnSave\" value=\"Save\" disabled=\"disabled\"/>";
//            endif;
//            if ($this->permission->method('accounts', 'update')->access()):
            $html .= " <input type=\"submit\" name=\"btnUpdate\" id=\"btnUpdate\" value=\"Update\" />";
//            endif;
            $html .= " </td>
                  </tr>
      
    </table>
 </form>
			";
        }
        echo json_encode($html);
    }

    public function newform($id) {

        $newdata = $this->db->select('*')
                ->from('b_acc_coa')
                ->where('HeadCode', $id)
                ->get()
                ->row();

        $newidsinfo = $this->db->select('*,count(HeadCode) as hc')
                ->from('b_acc_coa')
                ->where('PHeadName', $newdata->HeadName)
                ->get()
                ->row();

        $nid = $newidsinfo->hc;
//        echo json_encode($nid);exit();
        $n = $nid + 1;
        if ($n / 10 < 1)
            $HeadCode = $id . "0" . $n;
        else
            $HeadCode = $id . $n;

        $info['headcode'] = $HeadCode;
        $info['rowdata'] = $newdata;
        $info['headlabel'] = $newdata->HeadLevel + 1;
        echo json_encode($info);
    }

//    ============ its for insert coa ============
    public function insert_coa() {
        $headcode = $this->input->post('txtHeadCode');
        $HeadName = $this->input->post('txtHeadName');
        $PHeadName = $this->input->post('txtPHead');
        $HeadLevel = $this->input->post('txtHeadLevel');
        $txtHeadType = $this->input->post('txtHeadType');
        $isact = $this->input->post('IsActive');
        $IsActive = (!empty($isact) ? $isact : 0);
        $trns = $this->input->post('IsTransaction');
        $IsTransaction = (!empty($trns) ? $trns : 0);
        $isgl = $this->input->post('IsGL');
        $IsGL = (!empty($isgl) ? $isgl : 0);
//        $createby = $this->session->userdata('id');
        //$updateby=$this->session->userdata('id');
        $createdate = date('Y-m-d H:i:s');
        $postData = array(
            'HeadCode' => $headcode,
            'HeadName' => $HeadName,
            'PHeadName' => $PHeadName,
            'HeadLevel' => $HeadLevel,
            'IsActive' => $IsActive,
            'IsTransaction' => $IsTransaction,
            'IsGL' => $IsGL,
            'HeadType' => $txtHeadType,
            'IsBudget' => 0,
            'CreateBy' => $this->user_id,
            'CreateDate' => $createdate,
        );
        $upinfo = $this->db->select('*')
                ->from('b_acc_coa')
                ->where('HeadCode', $headcode)
                ->get()
                ->row();
        if (empty($upinfo)) {
            $this->db->insert('b_acc_coa', $postData);
        } else {

            $hname = $this->input->post('HeadName');
            $updata = array(
                'PHeadName' => $HeadName,
            );


            $this->db->where('HeadCode', $headcode)
                    ->update('b_acc_coa', $postData);
            $this->db->where('PHeadName', $hname)
                    ->update('b_acc_coa', $updata);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

//    =============== its for debit_voucher ==============
    public function debit_voucher() {
        $this->permission->check_label('debit_voucher')->create()->redirect();
        $data['acc'] = $this->BAccount_model->Transacc();
        $data['voucher_no'] = $this->BAccount_model->voNO();
        $data['crcc'] = $this->BAccount_model->Cracc();
//        dd($data['voucher_no']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/debit_voucher', $data);
        $this->load->view('b_level/footer');
    }

    // Debit voucher code select onchange
    public function debit_voucher_code($id) {
        $debitvcode = $this->db->select('*')
                ->from('b_acc_coa')
                ->where('HeadCode', $id)
                ->get()
                ->row();
        $code = $debitvcode->HeadCode;
        echo json_encode($code);
    }

    //Create Debit Voucher
    public function create_debit_voucher() {
//        $this->permission->method('accounts', 'create')->redirect();
        $this->form_validation->set_rules('cmbDebit', 'Debit Account', 'max_length[100]');
        if ($this->form_validation->run()) {
            if ($this->BAccount_model->insert_debitvoucher()) {

                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Debit voucher save successfully!</div>");
                redirect('b-debit-voucher');
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            }
            redirect("b-debit-voucher");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            redirect("b-debit-voucher");
        }
    }

//    =============== its for credit_voucher ==============
    public function credit_voucher() {
        $this->permission->check_label('credit_voucher')->create()->redirect();
        $data['acc'] = $this->BAccount_model->Transacc();
        $data['voucher_no'] = $this->BAccount_model->crVno();
        $data['crcc'] = $this->BAccount_model->Cracc();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/credit_voucher', $data);
        $this->load->view('b_level/footer');
    }

    //Create Credit Voucher
    public function create_credit_voucher() {
//        $this->permission->method('accounts', 'create')->redirect();
        $this->form_validation->set_rules('cmbDebit', 'Credit Account', 'max_length[100]');
        if ($this->form_validation->run()) {
            if ($this->BAccount_model->insert_creditvoucher()) {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Credit voucher save successfully!</div>");
                redirect("b-credit-voucher");
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            }
            redirect("b-credit-voucher");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            redirect("b-credit-voucher");
        }
    }

//    =============== its for journal_voucher ==============
    public function journal_voucher() {
        $this->permission->check_label('journal_voucher')->create()->redirect();
        $data['acc'] = $this->BAccount_model->Transacc();
        $data['voucher_no'] = $this->BAccount_model->journal();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/journal_voucher', $data);
        $this->load->view('b_level/footer');
    }

//    =============== its for contra_voucher ==============
    public function contra_voucher() {
        $this->permission->check_label('contra_voucher')->create()->redirect();
        $data['acc'] = $this->BAccount_model->Transacc();
        $data['voucher_no'] = $this->BAccount_model->contra();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/contra_voucher', $data);
        $this->load->view('b_level/footer');
    }

    //    =========== its for voucher_approval ===============
    public function voucher_approval() {
        $this->permission->check_label('voucher_approval')->create()->redirect();
        $data['aprrove'] = $this->BAccount_model->approve_voucher();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/voucher_approval', $data);
        $this->load->view('b_level/footer');
    }

    //Update voucher 
    public function voucher_edit($id = null) {
//        $this->permission->method('accounts', 'Update')->redirect();
        $vtype = $this->db->select('*')
                ->from('b_acc_transaction')
                ->where('VNo', $id)
                ->get()
                ->row();
//        dd($vtype);

        $data['crcc'] = $this->BAccount_model->Cracc();
        $data['acc'] = $this->BAccount_model->Transacc();

        if ($vtype->Vtype == "DV") {
//            $data['title'] = display('update_debit_voucher');
            $data['dbvoucher_info'] = $this->BAccount_model->dbvoucher_updata($id);
            $data['credit_info'] = $this->BAccount_model->crvoucher_updata($id);

//            $data['page'] = "update_dbt_crtvoucher";
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/accounts/voucher/update_dbt_crtvoucher', $data);
            $this->load->view('b_level/footer');
        }
        if ($vtype->Vtype == "CV") {
//            print_r($vtype);exit;
//            $data['title'] = display('update_credit_voucher');
            $data['crvoucher_info'] = $this->BAccount_model->crdtvoucher_updata($id);
            $data['debit_info'] = $this->BAccount_model->debitvoucher_updata($id);
//            dd($data['debit_info']);
//            $data['page'] = "update_credit_bdtvoucher";
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/accounts/voucher/update_credit_bdtvoucher', $data);
            $this->load->view('b_level/footer');
        }

        if ($vtype->Vtype == 'Contra') {
            $data['contraCrebitVoucher_edit'] = $this->BAccount_model->contraCrebitVoucher_edit($id);
//            $data['contraDebitVoucher_edit'] = $this->Account_model->contraDebitVoucher_edit($id);
//            dd($data['contraCrebitVoucher_edit']);
//            $data['page'] = "update_credit_bdtvoucher";
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/accounts/voucher/contravoucher_edit_info', $data);
            $this->load->view('b_level/footer');
        }
        if ($vtype->Vtype == 'JV') {
            $data['journalCrebitVoucher_edit'] = $this->BAccount_model->journalCrebitVoucher_edit($id);
//            $data['contraDebitVoucher_edit'] = $this->Account_model->contraDebitVoucher_edit($id);
//            dd($data['journalCrebitVoucher_edit']);
//            
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/accounts/voucher/journalVoucher_edit', $data);
            $this->load->view('b_level/footer');
        }
//        $data['module'] = "accounts";
//
//        echo Modules::run('template/layout', $data);
    }

    //Create Journal Voucher
    public function create_journal_voucher() {
//   $this->permission->method('accounts','create')->redirect();
        $this->form_validation->set_rules('cmbDebit', 'Journal Voucher', 'max_length[100]');
        if ($this->form_validation->run()) {
            if ($this->BAccount_model->insert_journalvoucher()) {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Journal voucher save successfully!</div>");
                redirect('b-journal-voucher');
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            }
            redirect('b-journal-voucher');
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            redirect('b-journal-voucher');
        }
    }

    //Create Contra Voucher
    public function create_contra_voucher() {
//        $this->permission->method('accounts', 'create')->redirect();
        $this->form_validation->set_rules('cmbDebit', 'voucher', 'max_length[100]');
        if ($this->form_validation->run()) {
            if ($this->BAccount_model->insert_contravoucher()) {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Contra voucher save successfully!</div>");
                redirect('b-contra-voucher');
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            }
            redirect('b-contra-voucher');
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            redirect('b-contra-voucher');
        }
    }

    // isApprove
    public function isactive($id = null, $action = null) {
//        echo $id ." "; echo $action;exit();
        $action = ($action == 'active' ? 1 : 0);

        $postData = array(
            'VNo' => $id,
            'IsAppove' => $action
        );

        if ($this->BAccount_model->approved($postData)) {
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Approved successfully!</div>");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    // Update Debit voucher 
    public function update_debit_voucher() {
//        $this->permission->method('accounts', 'create')->redirect();
        $this->form_validation->set_rules('cmbDebit', 'cmbDebit', 'max_length[100]');
        if ($this->form_validation->run()) {
            if ($this->BAccount_model->update_debitvoucher()) {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Updated successfully!</div>");
                redirect('b-voucher-approval');
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            }
            redirect('b-voucher-approval');
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            redirect('b-voucher-approval');
        }
    }

    // update credit voucher 
    public function update_credit_voucher() {
//        $this->permission->method('accounts', 'create')->redirect();
        $this->form_validation->set_rules('cmbDebit', 'cmbDebit', 'max_length[100]');
        if ($this->form_validation->run()) {
            if ($this->BAccount_model->update_creditvoucher()) {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Updated successfully!</div>");
                redirect('b-voucher-approval');
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            }
            redirect('b-voucher-approval');
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please try again!</div>");
            redirect('b-voucher-approval');
        }
    }

//    ============== its for update_journal_voucher ==============
    public function update_journal_voucher() {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "JV";
        $dAID = $this->input->post('cmbDebit');
        $cAID = $this->input->post('txtCode');
        $debit = $this->input->post('txtAmount');
        $credit = $this->input->post('txtAmountcr');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');
        if ($voucher_no) {
//            echo $voucher_no;die();
            $this->db->where('VNo', $voucher_no);
            $this->db->delete('b_acc_transaction');
        }
//        print_r($debit);        echo '<br>';
//        print_r($credit);        echo '<br>';
        for ($i = 0; $i < count($cAID); $i++) {
//            $crtid = $cAID[$i];
//            $Cramnt = $credit[$i];
//            $debit = $debit[$i];

            $contrainsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $cAID[$i],
                'Narration' => $Narration,
                'Debit' => $debit[$i],
                'Credit' => $credit[$i],
//                'StoreID' => $this->session->userdata('store_id'),
                'IsPosted' => $IsPosted,
                'UpdateBy' => $CreateBy,
                'UpdateDate' => $createdate,
                'IsAppove' => 0
            );
//            print_r($contrainsert);echo '<br>';
            $this->db->insert('b_acc_transaction', $contrainsert);
        }
//        exit();
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Journal voucher updated successfully!</div>");
        redirect("b-voucher-approval");
    }

//    ============= its for update_contra_voucher ==============
    public function update_contra_voucher() {
        $voucher_no = addslashes(trim($this->input->post('txtVNo')));
        $Vtype = "Contra";
        $dAID = $this->input->post('cmbDebit');
        $cAID = $this->input->post('txtCode');
        $debit = $this->input->post('txtAmount');
//        $grand_debit = $this->input->post('grand_total');
        $credit = $this->input->post('txtAmountcr');
//        $grand_credit = $this->input->post('grand_total1');
        $VDate = $this->input->post('dtpDate');
        $Narration = addslashes(trim($this->input->post('txtRemarks')));
        $IsPosted = 1;
        $IsAppove = 0;
        $CreateBy = $this->session->userdata('user_id');
        $createdate = date('Y-m-d H:i:s');
        if ($voucher_no) {
//            echo $voucher_no;die();
            $this->db->where('VNo', $voucher_no);
            $this->db->delete('b_acc_transaction');
        }
//        dd($cAID);
        for ($i = 0; $i < count($cAID); $i++) {
//            $crtid = $cAID[$i];
//            $Cramnt = $credit[$i];
//            $debit = $debit[$i];

            $contrainsert = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'COAID' => $cAID[$i],
                'Narration' => $Narration,
                'Debit' => $debit[$i],
                'Credit' => $credit[$i],
//                'StoreID' => $this->session->userdata('store_id'),
                'IsPosted' => $IsPosted,
                'UpdateBy' => $CreateBy,
                'UpdateDate' => $createdate,
                'IsAppove' => 0
            );
//            print_r($contrainsert);exit;
            $this->db->insert('b_acc_transaction', $contrainsert);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Contra voucher updated successfully!</div>");
        redirect("b-voucher-approval");
    }

    //    =========== its for voucher_reports ===============
    public function voucher_reports() {
        $this->permission->check_label('voucher_reports')->create()->redirect();
        $get_cash = $this->BAccount_model->get_cash();
        $get_vouchar = $this->BAccount_model->get_vouchar();


        $data = array(
            'get_cash' => $get_cash,
            'get_vouchar' => $get_vouchar,
        );



        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/voucher_reports');
        $this->load->view('b_level/footer');
    }

    public function vouchar_cash($date) {
        $vouchar_view = $this->BAccount_model->get_vouchar_view($date);
//        echo '<pre>';        print_r($vouchar_view);die();
        $data = array(
            'vouchar_view' => $vouchar_view,
        );

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/voucher/vouchar_cash');
        $this->load->view('b_level/footer');
    }

    public function voucher_report_serach($vouchar = NULL) {
        echo $vouchar = $this->input->post('vouchar');
        $voucher_report_serach = $this->BAccount_model->voucher_report_serach($vouchar);
//        echo '<pre>';        print_r($voucher_report_serach);die();
        if ($voucher_report_serach->Amount == '') {
            $pay = '0.00';
        } else {
            $pay = $voucher_report_serach->Amount;
        }
        $baseurl = base_url() . 'b-vouchar-cash/' . $vouchar;
        $html = "";
        $html .= "<td>
                   <a href=\"$baseurl\">CV-BAC-$vouchar</a>
                 </td>
                 <td>Aggregated Cash Credit Voucher of $vouchar</td>
                 <td>$pay</td>
                 <td align=\"center\">$vouchar</td>";
        echo $html;
    }

    //    =========== its for bank_book ===============
    public function bank_book() {
        $this->permission->check_label('bank_book')->create()->redirect();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/bank_book');
        $this->load->view('b_level/footer');
    }

    //    =========== its for cash_book ===============
    public function cash_book() {
        $this->permission->check_label('cash_book')->create()->redirect();
        
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/cash_book');
        $this->load->view('b_level/footer');
    }

    //    =========== its for cash_flow ===============
    public function cash_flow() {
        $this->permission->check_label('cash_flow')->create()->redirect();
        
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/cash_flow');
        $this->load->view('b_level/footer');
    }

//    =============== its for cash_flow_report_search ===============
    public function cash_flow_report_search() {
        $dtpFromDate = $this->input->post('dtpFromDate');
        $dtpToDate = $this->input->post('dtpToDate');

        $data['dtpFromDate'] = $dtpFromDate;
        $data['dtpToDate'] = $dtpToDate;
        // PDF Generator 
        $this->load->library('Pdfgenerator');
        $dompdf = new DOMPDF();
//        echo '<pre>';        print_r($dompdf);die();
        $page = $this->load->view('b_level/accounts/reports/cash_flow_report_search_pdf', $data, true);
        $dompdf->load_html($page);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('assets/b_level/pdf/Cash Flow Statement ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf', $output);

        $data['pdf'] = 'assets/b_level/pdf/Cash Flow Statement ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf';

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/cash_flow_report_search');
        $this->load->view('b_level/footer');
    }

    //    =========== its for general_ledger ===============
    public function general_ledger() {
        $this->permission->check_label('general_ledger')->create()->redirect();
        $general_ledger = $this->BAccount_model->get_general_ledger();
        $data = array(
            'general_ledger' => $general_ledger,
        );
        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/general_ledger');
        $this->load->view('b_level/footer');
    }

    public function b_accounts_report_search() {
        $cmbGLCode = $this->input->post('cmbGLCode');
        $cmbCode = $this->input->post('cmbCode');

        $dtpFromDate = $this->input->post('dtpFromDate');
        $dtpToDate = $this->input->post('dtpToDate');
        $chkIsTransction = $this->input->post('chkIsTransction');


        $HeadName = $this->BAccount_model->general_led_report_headname($cmbGLCode);
        $HeadName2 = $this->BAccount_model->general_led_report_headname2($cmbGLCode, $cmbCode, $dtpFromDate, $dtpToDate, $chkIsTransction);
        $pre_balance = $this->BAccount_model->general_led_report_prebalance($cmbCode, $dtpFromDate);

        $data = array(
            'dtpFromDate' => $dtpFromDate,
            'dtpToDate' => $dtpToDate,
            'HeadName' => $HeadName,
            'HeadName2' => $HeadName2,
            'prebalance' => $pre_balance,
            'chkIsTransction' => $chkIsTransction,
        );
        $data['ledger'] = $this->db->select('*')->from('b_acc_coa')->where('HeadCode', $cmbCode)->get()->row();
        $data['title'] = 'general_ledger_report';


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/general_ledger_report');
        $this->load->view('b_level/footer');

//        $data['module'] = "accounts";
//        $data['page']   = "general_ledger_report";
//        echo Modules::run('template/layout', $data);
    }

    public function general_led($Headid = NULL) {
        $Headid = $this->input->post('Headid');
        $HeadName = $this->BAccount_model->general_led_get($Headid);
        echo "<option>Transaction Head</option>";
        $html = "";
        foreach ($HeadName as $data) {
            $html .= "<option value='$data->HeadCode'>$data->HeadName</option>";
        }
        echo $html;
    }

    //    =========== its for profit_loss ===============
    public function profit_loss() {
        $this->permission->check_label('profit_loss')->create()->redirect();
        
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/profit_loss');
        $this->load->view('b_level/footer');
    }

    public function b_profit_loss_report_search() {
        $dtpFromDate = $this->input->post('dtpFromDate');
        $dtpToDate = $this->input->post('dtpToDate');
        $get_profit = $this->BAccount_model->profit_loss_serach();
//        echo '<pre>'; print_r($get_profit);

        $data['oResultAsset'] = $get_profit['oResultAsset'];
        $data['oResultLiability'] = $get_profit['oResultLiability'];
        $data['dtpFromDate'] = $dtpFromDate;
        $data['dtpToDate'] = $dtpToDate;

        // // PDF Generator 
        // $this->load->library('pdfgenerator');
        // $dompdf = new DOMPDF();
        // $page = $this->load->view('accounts/profit_loss_report_search_pdf',$data,true);
        // $dompdf->load_html($page);
        // $dompdf->render();
        // $output = $dompdf->output();
        // file_put_contents('assets/data/pdf/Statement of Comprehensive Income From '.$dtpFromDate.' To '.$dtpToDate.'.pdf', $output);


        $data['pdf'] = 'assets/b_level/pdf/Statement of Comprehensive Income From ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf';
        $data['title'] = "Profit Loss Report";

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/profit_loss_report_search');
        $this->load->view('b_level/footer');
    }

    //    =========== its for trial_ballance ===============
    public function trial_ballance() {
        $this->permission->check_label('trial_balance')->create()->redirect();
        
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/accounts/reports/trial_ballance');
        $this->load->view('b_level/footer');
    }

    public function b_trial_ballance_report() {
        $dtpFromDate = $this->input->post('dtpFromDate');
        $dtpToDate = $this->input->post('dtpToDate');
        $chkWithOpening = $this->input->post('chkWithOpening');

        $results = $this->BAccount_model->trial_balance_report($dtpFromDate, $dtpToDate, $chkWithOpening);

        if ($results['WithOpening']) {
            $data['oResultTr'] = $results['oResultTr'];
            $data['oResultInEx'] = $results['oResultInEx'];
            $data['dtpFromDate'] = $dtpFromDate;
            $data['dtpToDate'] = $dtpToDate;
//        echo '<pre>';        print_r($data);die();
            // PDF Generator 
            $this->load->library('pdfgenerator');
            $dompdf = new DOMPDF();
            $page = $this->load->view('b_level/accounts/reports/trial_balance_with_opening_pdf', $data, true);
            $dompdf->load_html($page);
            $dompdf->render();
            $output = $dompdf->output();
            file_put_contents('assets/b_level/pdf/Trial Balance With Opening As On ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf', $output);


            $data['pdf'] = 'assets/b_level/pdf/Trial Balance With Opening As On ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf';
            $data['title'] = "Trial Blance Report";

            $this->load->view('b_level/header', $data);
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/accounts/reports/trial_balance_with_opening');
            $this->load->view('b_level/footer');
        } else {

            $data['oResultTr'] = $results['oResultTr'];
            $data['oResultInEx'] = $results['oResultInEx'];
            $data['dtpFromDate'] = $dtpFromDate;
            $data['dtpToDate'] = $dtpToDate;

            // PDF Generator 
            $this->load->library('pdfgenerator');
            $dompdf = new DOMPDF();
            $page = $this->load->view('b_level/accounts/reports/trial_balance_without_opening_pdf', $data, true);
            $dompdf->load_html($page);
            $dompdf->render();
            $output = $dompdf->output();
            file_put_contents('assets/b_level/pdf/Trial Balance As On ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf', $output);
            $data['pdf'] = 'assets/b_level/pdf/Trial Balance As On ' . $dtpFromDate . ' To ' . $dtpToDate . '.pdf';

            $data['title'] = "Trial Blance Report";

            $this->load->view('b_level/header', $data);
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/accounts/reports/trial_balance_without_opening');
            $this->load->view('b_level/footer');
        }
    }

//    ============= its for b_single_order_test ================
    public function b_single_order_test($id = 1) {
//        $data['b_single_order'] = $this->BAccount_model->b_single_order($id);
//        $data['get_b_customer_info'] = $this->BAccount_model->get_b_customer_info($id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/b_single_order_test');
        $this->load->view('b_level/footer');
    }

}
