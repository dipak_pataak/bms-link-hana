<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->load->model('b_level/Stock_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    ============== its for stock_availability ==================
    public function stock_availability() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $this->permission->check_label('stock_availability')->create()->redirect();
        $data['get_allrecord'] = $this->db->select('a.*, b.material_name, c.pattern_name, d.color_name, sum(a.in_qty) as inqty, sum(a.out_qty) as outqty')
                        ->from('row_material_stock_tbl a')
                        ->join('row_material_tbl b', 'b.id = a.row_material_id')
                        ->join('pattern_model_tbl c', 'c.pattern_model_id = a.pattern_model_id', 'left')
                        ->join('color_tbl d', 'd.id = a.color_id', 'left')
                        ->where('c.created_by',$level_id)
                        ->group_by('a.row_material_id')
                        ->group_by('a.pattern_model_id')
                        ->group_by('a.color_id')
                        ->order_by("a.id", 'desc')
                        ->get()->result();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/stock/stock_availability');
        $this->load->view('b_level/footer');
    }

//    ============== its for stock_history ==================
    public function stock_history() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->permission->check_label('stock_history')->create()->redirect();
        $data['get_raw_materials'] = $this->db->where('created_by',$level_id)->get('row_material_tbl')->result();

//        $query = 'SELECT a.*, b.material_name, c.pattern_name, d.color_name FROM row_material_stock_tbl a 
//                                        JOIN row_material_tbl b ON b.id = a.row_material_id
//                                        LEFT JOIN pattern_model_tbl c ON c.pattern_model_id = a.pattern_model_id
//                                        LEFT JOIN color_tbl d ON d.id = a.color_id order by a.id desc';
//        $data['raw_material_stock_info'] = $this->db->query($query)->result();
        $data['raw_material_stock_info'] = $this->db->select('a.*, b.material_name, c.pattern_name, d.color_name')
                        ->from('row_material_stock_tbl a')
                        ->join('row_material_tbl b', 'b.id = a.row_material_id')
                        ->join('pattern_model_tbl c', 'c.pattern_model_id = a.pattern_model_id', 'left')
                        ->join('color_tbl d', 'd.id = a.color_id', 'left')
                        ->where('b.created_by',$level_id)
                        ->order_by("a.id", 'desc')
                        ->get()->result();



                        // $data['raw_material_stock_info'] = $this->db->select('row_material_tbl.*,
                        //     pattern_model_tbl.pattern_name,
                        //     color_tbl.color_name
                        // ')
                        // ->from('row_material_tbl')
                        // ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = row_material_tbl.pattern_model_id', 'left')
                        // ->join('color_tbl', 'color_tbl.id = row_material_tbl.color_id', 'left')
                        // ->get()->result();




        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/stock/stock_history');
        $this->load->view('b_level/footer');
    }

//    ========== its for filter_result =============
    public function filter_result() {
        $material_id = $this->input->post('material_id');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $data['get_stock_history'] = $this->Stock_model->get_stock_history_filter($material_id, $from_date, $to_date);
//        echo '<pre>';        print_r($data['get_stock_history']);
        $this->load->view('b_level/stock/filter_result', $data);
    }

}
