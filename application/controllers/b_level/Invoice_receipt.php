<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_receipt extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Order_model');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Customer_model');
        $this->load->model('b_level/settings');
        $this->load->library('paypal_lib');
        $this->load->model('email_sender');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

    }


    public function money_receipt($order_id = NULL) {
        
        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['payment'] = $this->db->where('quotation_id',$order_id)->order_by('payment_id','DESC')->get('payment_tbl')->row();
        $data['company_profile'] = $this->settings->company_profile();
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();
        $this->load->view('b_level/orders/money_receipt', $data);

    }


    public function receipt($order_id = NULL) {

        $this->permission->check_label('order')->read()->redirect();

        $orderData =$this->Order_model->get_orderd_by_id($order_id);
        $data['orderd'] = $orderData;

        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);
        //dd($data['order_details']);
        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $data['check_re_order_status'] = 0;
        $check_re_order = $this->db->where('clevel_order_id',$orderData->clevel_order_id)->get('b_to_b_level_quatation_tbl')->row();

        if(!empty($check_re_order))
            $data['check_re_order_status'] = 1;

        $productData = $this->db->select('a.product_id,a.product_name')
                    ->from('product_tbl a')
                    ->join('qutation_details b','b.product_id=a.product_id','left')
                    ->where('b.order_id',$orderData->clevel_order_id)
                    ->get()->result();



        $userData  = $this->db->select('a.id,a.first_name,a.last_name')
            ->from('user_info a')
            ->join('b_user_catalog_request b','b.b_user_id=a.id')
            ->where('b.requested_by',$this->level_id)
            ->get()->result()
        ;

        $data['b_user_data'] = $userData;
        $data['re_order_product_data'] = $productData;
        $data['clevel_order_id'] = $orderData->clevel_order_id;


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_receipt', $data);
        $this->load->view('b_level/footer');

    }


    public function order_payment_update() {

        $customer_id    = $this->input->post('customer_id');
        $customer_no    = $this->input->post('customer_no');
        $payment_method = $this->input->post('payment_method');
        $order_id       = $this->input->post('order_id');
        $c_level_order_id       = $this->input->post('c_level_order_id');

        $cHead = $this->db->select('HeadCode')->where('HeadName', $customer_no)->get('b_acc_coa')->row();

        if ($cHead->HeadCode != NULL) {

            $cusstomer_info = $this->db->where('customer_id',$customer_id)->get('customer_info')->row();
            
            if ($payment_method == 'paypal') {

                $orderData = array(
                    'order_id'      => $order_id,
                    'customer_id'   => $customer_id,
                    'paid_amount'   => $this->input->post('paid_amount'),
                );

                $orderDataPaypal = array(

                    'order_id'      => $order_id,
                    'headcode'      => $cHead->HeadCode,
                    'grand_total'   => $this->input->post('grand_total'),
                    'paid_amount'   => $this->input->post('paid_amount'),
                    'due'           => $this->input->post('due'),
                    'payment_method' => $this->input->post('payment_method'),
                    'card_number'   => $this->input->post('card_number'),
                    'issuer'        => $this->input->post('issuer'),
                    'cusstomer_user_id' => $cusstomer_info->customer_user_id,
                    'customer_id'   => $cusstomer_info->customer_id

                );

                $this->session->set_userdata($orderDataPaypal);

                $pyament = $this->payment_by_paypal($orderData);


            } else {


                $due = $this->input->post('due');

                if ($due > 0) {
                    $order_stage = 3;
                } else {
                    $order_stage = 2;
                }

                $rowData = $this->db->select('due,paid_amount,clevel_order_id,created_by,commission_amt')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();


                // Get B Level User info for get commission : START
                $created_by = @$rowData->created_by;
                $commission_amt = @$rowData->commission_amt;
                if($created_by != ''){
                    $user_data = $this->db->select('fixed_commission,percentage_commission')->where('id', @$rowData->created_by)->get('user_info')->row();

                    if(@$user_data->fixed_commission != '' && $commission_amt == 0){
                        $commission_amt_fixed = @$user_data->fixed_commission;
                    }else{
                        $commission_amt_fixed = 0;
                    } 

                    if(@$user_data->percentage_commission != '' && @$user_data->percentage_commission > 0){
                        $commission_amt_percentage = round((($this->input->post('paid_amount') * @$user_data->percentage_commission)/100),2);
                    }else{
                        $commission_amt_percentage = 0;
                    } 

                    $commission_amt +=  ($commission_amt_fixed + $commission_amt_percentage); 
                }    
                // Get B Level User info for get commission : END
                
                // quatation table update
                $orderData = array(
                    'paid_amount'   => @$rowData->paid_amount + $this->input->post('paid_amount'),
                    'due'           => $this->input->post('due'),
                    'order_stage'   => $order_stage,
                    'commission_amt'   => $commission_amt
                );

                if($rowData->clevel_order_id!=NULL){
                    $this->db->set('order_stage', $order_stage)->where('order_id', $rowData->clevel_order_id)->update('quatation_tbl');
                }

                //update to quatation table with pyament due 
                $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);
                //-----------------------------------------
                // quatation table update
                $payment_tbl = array(
                    'quotation_id'      => $order_id,
                    'payment_method'    => $this->input->post('payment_method'),
                    'paid_amount'       => $this->input->post('paid_amount'),
                    'payment_date'      => date('Y-m-d'),
                    'created_by'        => $cusstomer_info->customer_user_id,
                    'create_date'       => date('Y-m-d')
                );


                if ($this->db->insert('payment_tbl', $payment_tbl)) {

                    $payment_id = $this->db->insert_id();

                    if ($payment_method == 'check') {

                        if (@$_FILES['check_image']['name']) {

                            $config['upload_path'] = './assets/b_level/uploads/check_img/';
                            $config['allowed_types'] = 'jpeg|jpg';
                            $config['overwrite'] = false;
                            $config['max_size'] = 3000;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('check_image')) {
                                $error = $this->upload->display_errors();
                                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                                redirect("new-order");
                            } else {

                                $data = $this->upload->data();
                                $check_image = $config['upload_path'] . $data['file_name'];
                            }
                        } else {
                            @$check_image = '';
                        }

                        $payment_check_tbl = array(
                            'payment_id'    => $payment_id,
                            'check_number'  => $this->input->post('check_number'),
                            'check_image'   => $check_image,
                        );

                        $this->db->insert('payment_check_tbl', $payment_check_tbl);

                    }

                }

                    
                    $voucher_no = $order_id;
                    $Vtype      = "INV";
                    $VDate      = date('Y-m-d');
                    $paid_amount = $this->input->post('paid_amount');
                    $cAID       = $cHead->HeadCode;
                    $IsPosted   = 1;
                    $CreateBy   = $cusstomer_info->customer_user_id;
                    $createdate = date('Y-m-d H:i:s');


                    $payment_method = $this->input->post('payment_method');

                    if($payment_method=='cash'){
                        $COAID = '1020101';
                    }if($payment_method=='check'){
                        $COAID = '102010202';
                    }if($payment_method=='card'){
                        $COAID = '1020103';
                    }

                    // C level transection start
                    //C level credit insert b_acc_transaction
                    $customerCredit = array(

                        'VNo'           => $voucher_no,
                        'Vtype'         => $Vtype,
                        'VDate'         => $VDate,
                        'Debit'         => 0,
                        'Credit'        => $paid_amount,
                        'COAID'         => $COAID,
                        'Narration'     => "Paid for invoice #".$voucher_no,
                        'IsPosted'      => $IsPosted,
                        'CreateBy'      => $CreateBy,
                        'CreateDate'    => $createdate,
                        'IsAppove'      => 1
                    );

                    $this->db->insert('acc_transaction', $customerCredit);
                    //------------------------------------

                    //B level debit insert b_acc_transaction
                    
                    $bCOA = '502020101';
                    $b_levelDebit = array(

                        'VNo'       => $voucher_no,
                        'Vtype'     => $Vtype,
                        'VDate'     => $VDate,
                        'Debit'     => $paid_amount,
                        'Credit'    => 0,
                        'COAID'     => $bCOA,
                        'Narration' => "Amount received for invoice #".$voucher_no,
                        'IsPosted'  => $IsPosted,
                        'CreateBy'  => $CreateBy,
                        'CreateDate' => $createdate,
                        'IsAppove'  => 1
                    );//b_acc_transaction

                    $this->db->insert('acc_transaction', $b_levelDebit);
                    // C level transection END

                    //. B level transection start
                    //customer credit insert b_acc_transaction
                    $customerCredit = array(

                        'VNo'           => $voucher_no,
                        'Vtype'         => $Vtype,
                        'VDate'         => $VDate,
                        'Debit'         => 0,
                        'Credit'        => $paid_amount,
                        'COAID'         => $cAID,
                        'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                        'IsPosted'      => $IsPosted,
                        'CreateBy'      => $CreateBy,
                        'CreateDate'    => $createdate,
                        'IsAppove'      => 1
                    );

                    $this->db->insert('b_acc_transaction', $customerCredit);
                    //------------------------------------

                    //b_level debit insert b_acc_transaction                    
                    
                    $b_levelDebit = array(

                        'VNo'       => $voucher_no,
                        'Vtype'     => $Vtype,
                        'VDate'     => $VDate,
                        'Debit'     => $paid_amount,
                        'Credit'    => 0,
                        'COAID'     => $COAID,
                        'Narration' => "Amount received for invoice #".$voucher_no,
                        'IsPosted'  => $IsPosted,
                        'CreateBy'  => $CreateBy,
                        'CreateDate' => $createdate,
                        'IsAppove'  => 1
                    );//b_acc_transaction

                    $this->db->insert('b_acc_transaction', $b_levelDebit);
                    //- B level transection END----------------------

                    // C level notification

                    $cNotificationData = array(

                        'notification_text' => 'Payment has been done for '.$order_id,
                        'go_to_url'         => '#',
                        'created_by'        => $cusstomer_info->customer_user_id,
                        'date'              => date('Y-m-d')

                    );

                    $this->db->insert('c_notification_tbl',$cNotificationData);

                    //-------------------------
                    //

                    $bNotificationData = array(

                        'notification_text' =>'Payment has been received for '.$order_id,
                        'go_to_url'         =>'b_level/invoice_receipt/receipt/'.$order_id,
                        'created_by'        => $cusstomer_info->customer_user_id,
                        'date'              => date('Y-m-d')
                    );
                    $this->db->insert('b_notification_tbl',$bNotificationData);

                    //--------------------------

                   

                    $this->email_sender->send_email(
                        $data = array(
                            'customer_id' => $customer_id,
                            'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount:'.$paid_amount.', Payment method:'.$payment_method,
                            'subject'     => 'Order payment'
                        )
                    );


                     // Send sms
                    $this->Order_model->smsSend( $data = array(
                            'customer_id' => $customer_id,
                            'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                            'subject'     => 'Order payment'
                        ));



                //--------------------------
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                redirect('b_level/invoice_receipt/money_receipt/' . $order_id);

            }

        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer account code not found! </div>");
            redirect('order-view/' . $order_id);
            
        }
    }




    public function order_logo_print($order_id = NULL) {
//        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
//        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

//        $data['company_profile'] = $this->settings->company_profile();
//        $data['customer'] = $this->db->where('customer_id',$data['orderd']->customer_id)->get('customer_info')->row();
//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_logo_print', $data);
//        $this->load->view('b_level/footer');    
    }





    public function payment_by_paypal($orderData) {


        $customer_id = $orderData['customer_id'];
        $order_id = $orderData['order_id'];

        //$item_name = "Order :: Test";
        // ---------------------
        //Set variables for paypal form
        $returnURL = base_url("b_level/invoice_receipt/success/$order_id/$customer_id"); //payment success url
        $cancelURL = base_url("b_level/invoice_receipt/cancel/$order_id/$customer_id"); //payment cancel url
        $notifyURL = base_url('b_level/invoice_receipt/ipn'); //ipn url
        //set session token
        
        $this->session->unset_userdata('_tran_token');
        $this->session->set_userdata(array('_tran_token' => $order_id));
        // set form auto fill data
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);



        $paypal = $this->db->select('*')
        ->from('gateway_tbl')
        ->where('default_status', 1)
        ->where('created_by',$this->user_id)
        ->where('level_type','b' )
        ->get()
        ->row();

        $paypal_lib_currency_code = (!empty($paypal->currency) ? $paypal->currency : 'USD');
        $paypal_lib_ipn_log_file = BASEPATH . 'logs/paypal_ipn.log';
        $paypal_lib_ipn_log = TRUE;
        $paypal_lib_button_path = 'buttons';


        // my customization
        $this->paypal_lib->add_field('mode', $paypal->status);
        $this->paypal_lib->add_field('business', $paypal->payment_mail);
        $this->paypal_lib->add_field('paypal_lib_currency_code', $paypal_lib_currency_code);
        $this->paypal_lib->add_field('paypal_lib_ipn_log_file', $paypal_lib_ipn_log_file);
        $this->paypal_lib->add_field('paypal_lib_ipn_log', $paypal_lib_ipn_log);
        $this->paypal_lib->add_field('paypal_lib_button_path', $paypal_lib_button_path);
        //-----------------------


        // item information
        $this->paypal_lib->add_field('item_number', $order_id);
        //$this->paypal_lib->add_field('item_name', $item_name);
        $this->paypal_lib->add_field('amount', $orderData['paid_amount']);
        // $this->paypal_lib->add_field('quantity', $quantity);
        // $this->paypal_lib->add_field('discount_amount', $discount);
        // additional information 
        $this->paypal_lib->add_field('custom', $order_id);
        $this->paypal_lib->image('');
        // generates auto form
        $this->paypal_lib->paypal_auto_form();


    }

    public function success($order_id = null, $customer_id = null) {


        $customer_info = $this->db->select('*')->from('customer_info')->where('customer_id', $customer_id)->get()->row();

        $this->order_payment_update_save();

    }

    public function cancel($order_id = null, $customer_id = null) {

        //--------------------------
        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cancel this payment </div>");
        redirect('b_level/invoice_receipt/receipt/' . $order_id);

    }




    /*
     * Add this ipn url to your paypal account
     * Profile and Settings > My selling tools > 
     * Instant Payment Notification (IPN) > update 
     * Notification URL: (eg:- http://domain.com/website/paypal/ipn/)
     * Receive IPN messages (Enabled) 
     */

    public function ipn() {

        //paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["mc_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;

        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (preg_match("/VERIFIED/i", $result)) {

            //insert the transaction data into the database
            $this->load->model('Paypal_model');
            $this->Paypal_model->insertTransaction($data);

        }

        return true;
    }




    //Send Customer Email with invoice
    public function setmail($email, $file_path, $id = null, $name = null) {

        $mail_config_detail = $this->db->select('*')->from('mail_config_tbl')->get()->row();
        $subject = 'ticket Information';
        $message = "Congratulation Mr. " . ' ' . $name . "Your Purchase Order No-  " . '-' . $id;

        $config = Array(
            'protocol' => $mail_config_detail->protocol,
            'smtp_host' => $mail_config_detail->smtp_host,
            'smtp_port' => $mail_config_detail->smtp_port,
            'smtp_user' => $mail_config_detail->smtp_user,
            'smtp_pass' => $mail_config_detail->smtp_pass,
            'mailtype' => $mail_config_detail->mailtype,
            'charset' => 'utf-8'
        );


        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($mail_config_detail->smtp_user);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach($file_path);

        $check_email = $this->test_input($email);

        if (filter_var($check_email, FILTER_VALIDATE_EMAIL)) {

            if ($this->email->send()) {
                $this->session->set_flashdata(array('message' => "Email Sent Sucessfully"));
                return true;
            } else {
                $this->session->set_flashdata(array('exception' => "Please configure your mail."));
                return false;
            }

        } else {

            $this->session->set_userdata(array('message' => "Your Data Successfully Saved"));
        }
    }

    //Email testing for email
    public function test_input($data) {

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;


    }



    public function order_payment_update_save() {

        $order_id = $this->session->userdata('order_id');
        $cusstomer_user_id = $this->session->userdata('cusstomer_user_id');
        $customer_id = $this->session->userdata('customer_id');
        $payment_method = $this->session->unset_userdata('payment_method');

        if (!empty($order_id)) {


            $due = $this->session->userdata('due');
            
            if ($due > 0) {
                $order_stage = 3;
            } else {
                $order_stage = 2;
            }

            $rowData = $this->db->select('due,paid_amount')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
            
            // quatation table update
            $orderData = array(
                'paid_amount'    => @$rowData->paid_amount + $this->session->userdata('paid_amount'),
                'due'            => $this->session->userdata('due'),
                'order_stage'     => $order_stage
            );

            //update to quatation table with pyament due 
            $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);



            $this->db->where('order_id',$order_id)->update('b_level_quatation_tbl',$orderData);
            // quatation table update
            $payment_tbl = array(
                'quotation_id'                  => $order_id,
                'payment_method'                => $this->session->userdata('payment_method'),
                'paid_amount'                   => $this->session->userdata('paid_amount'),
                'description'                   => "Payment given for Order#".$order_id,
                'payment_date'                  => date('Y-m-d'),
                'created_by'                    => $cusstomer_user_id,
                'create_date'                   => date('Y-m-d')
            );
            $this->db->insert('payment_tbl',$payment_tbl);


            $voucher_no = $order_id;
            $Vtype = "INV";
            $VDate = date('Y-m-d');
            $paid_amount = $this->session->userdata('paid_amount');
            $cAID = $this->session->userdata('headcode');
            $IsPosted = 1;
            $CreateBy = $cusstomer_user_id;
            $createdate = date('Y-m-d H:i:s');
            $Paypal_COAID = '1020104';
            //C level transaction Start
            $customerCredit = array(

                'VNo'           => $voucher_no,
                'Vtype'         => $Vtype,
                'VDate'         => $VDate,
                'Debit'         => 0,
                'Credit'        => $paid_amount,
                'COAID'         => $Paypal_COAID,
                'Narration'     => "Paid for invoice #".$voucher_no,
                'IsPosted'      => $IsPosted,
                'CreateBy'      => $CreateBy,
                'CreateDate'    => $createdate,
                'IsAppove'      => 1
            );

            $this->db->insert('acc_transaction', $customerCredit);

            //------------------------------------

            $bCOA = '502020101';

            $b_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'Credit' => 0,
                'COAID' => $bCOA,
                'Narration' => "Amount received for invoice #".$voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('acc_transaction', $b_levelDebit);
            // C level transaction END
            // B level transaction start
            // 
            $customerCredit = array(

                'VNo'           => $voucher_no,
                'Vtype'         => $Vtype,
                'VDate'         => $VDate,
                'Debit'         => 0,
                'Credit'        => $paid_amount,
                'COAID'         => $cAID,
                'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                'IsPosted'      => $IsPosted,
                'CreateBy'      => $CreateBy,
                'CreateDate'    => $createdate,
                'IsAppove'      => 1
            );

            $this->db->insert('b_acc_transaction', $customerCredit);

            //------------------------------------

            $bCOA = '502020101';

            $b_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'Credit' => 0,
                'COAID' => $Paypal_COAID,
                'Narration' => "Amount received for invoice #".$voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('b_acc_transaction', $b_levelDebit);

            // B level transaction End 


            $this->session->unset_userdata('order_id');
            $this->session->unset_userdata('headcode');
            $this->session->unset_userdata('grand_total');
            $this->session->unset_userdata('paid_amount');
            $this->session->unset_userdata('due');
            $this->session->unset_userdata('payment_method');
            $this->session->unset_userdata('card_number');
            $this->session->unset_userdata('issuer');
            $this->session->unset_userdata('cusstomer_user_id');
            $this->session->unset_userdata('customer_id');


            $bNotificationData = array(
                'notification_text' => 'Payment has been submited for '. $order_id,
                'go_to_url'         => 'b_level/invoice_receipt/receipt/' . $order_id,
                'created_by'        =>  $cusstomer_user_id,
                'date'              =>  date('Y-m-d')
            );

            $this->db->insert('b_notification_tbl', $bNotificationData);



            // C level notification
            $cNotificationData = array(

                'notification_text' => 'Payment has been done for '.$order_id,
                'go_to_url'         => '#',
                'created_by'        => $cusstomer_user_id,
                'date'              => date('Y-m-d')

            );

            $this->db->insert('c_notification_tbl',$cNotificationData);


            $this->email_sender->send_email(
                $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject'     => 'Order payment'
                )
            );
            

            // Send sms
            $this->Order_model->smsSend( $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject'     => 'Order payment'
                ));



            //--------------------------
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
            redirect('b_level/invoice_receipt/money_receipt/' . $order_id);

        }
    }






//    ============= its for order_customer_info_edit =============
    
    public function order_customer_info_edit($order_id) {

        $data['order_id'] = $order_id;
        $data['get_customer_info'] = $this->db->select('*')->from('b_level_quatation_tbl a')->where('a.order_id', $order_id)->get()->result();
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['colors'] = $this->db->get('color_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_customer_info_edit', $data);
        $this->load->view('b_level/footer');
    }




//    =============== its for order_customer_info_update ============
    public function order_customer_info_update() {

        $order_id = $this->input->post('orderid');
        $order_date = $this->input->post('order_date');
        $customer_id = $this->input->post('customer_id');
        $side_mark = $this->input->post('side_mark');
        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');

        if (@$_FILES['file_upload']['name']) {
            $config['upload_path'] = '/assets/b_level/uploads/file/';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['overwrite'] = false;
            $config['max_size'] = 3000;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("b_level/invoice_receipt/receipt/" . $order_id);
            } else {
                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            $upload_file = $this->input->post('file_upload_hdn');
        }
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $orderData = array(
            'order_date' => $order_date,
            'customer_id' => $customer_id,
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $level_id,
            'side_mark' => $side_mark,
            'upload_file' => $upload_file,
            'updated_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d'),
        );
//        echo '<pre>';        print_r($orderData);die();
        $this->db->where('order_id', $order_id);
        $this->db->update('b_level_quatation_tbl', $orderData);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info updated successfully!</div>");
        redirect("b_level/invoice_receipt/receipt/" . $order_id);
    }

//    ============= its for order_single_product_edit ============
    public function order_single_product_edit($row_id) {
        $data['get_product_order_info'] = $this->db->select('*')->from('qutation_details a')->where('a.row_id', $row_id)->get()->result();
        $data['get_quatation_attributes_info'] = $this->db->select('*')->from('quatation_attributes a')->where('a.fk_od_id', $row_id)->get()->result();
        $product_attributes = json_decode($data['get_quatation_attributes_info'][0]->product_attribute);
        $data['order_id'] = $data['get_product_order_info'][0]->order_id;
        $data['get_quatation_info'] = $this->db->select('*')->from('quatation_tbl a')->where('a.order_id', $data['order_id'])->get()->result();
//        echo $product_attributes[0]->attribute_id;
//         dd($data['order_id']);
//        foreach ($product_attributes as $single){
//            echo $single->attribute_id."<br>";
//        }
//        dd($data['get_quatation_attributes_info']);
        $product_id = $data['get_product_order_info'][0]->product_id;
//        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['colors'] = $this->db->get('color_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "b_level/orders/customer_js.php";

//        ======== its for product id wise patterns info =============
        $get_products_patterns = $this->db->select('*')->from('product_tbl a')
                        ->where('a.product_id', $product_id)->get()->result();
        $patterns = $get_products_patterns[0]->pattern_models_ids;
        if ($patterns) {
            $get_pattern_sql = "SELECT * FROM pattern_model_tbl WHERE pattern_model_id IN($patterns)";
            $data['get_patterns'] = $this->db->query($get_pattern_sql)->result();
        }
//        ======== close ============
//        ======== its for product id wise color info =============
        $get_products_colors = $this->db->select('*')->from('product_tbl a')
                        ->where('a.product_id', $product_id)->get()->result();
        $colors = $get_products_colors[0]->colors;
        if ($colors) {
            $get_colors_sql = "SELECT * FROM color_tbl WHERE id IN($colors)";
            $data['get_colors'] = $this->db->query($get_colors_sql)->result();
        }
//        ======== close ============
        $data['attributes'] = $this->db->select('product_attribute.*,attribute_tbl.attribute_name,attribute_tbl.attribute_type')
                        ->join('attribute_tbl', 'attribute_tbl.attribute_id=product_attribute.attribute_id')
                        ->where('product_attribute.product_id', $product_id)
                        ->order_by('attribute_tbl.position', 'ASC')
                        ->get('product_attribute')->result();


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_single_product_edit', $data);
        $this->load->view('b_level/footer');
    }

//    ========= its for get_product_attr_option_option===========
    public function get_product_attr_option_option($pro_att_op_id, $attribute_id, $main_price) {
        $options = $this->db->select('attr_options.*')
                        ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                        ->where('product_attr_option.id', $pro_att_op_id)
                        ->order_by('attr_options.att_op_id', 'ASC')
                        ->get('product_attr_option')->row();

        if ($options->price_type == 1) {
            $price_total = $main_price + @$options->price;
            $contribution_price = (!empty($options->price) ? $options->price : 0);
            echo '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {
            $price_total = ($main_price * $options->price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            echo '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }



        if (@$options->option_type == 3) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                            ->get('product_attr_option_option')->result();


            foreach ($opops as $op_op) {

                echo '<br><div class="col-sm-12">
                            <label>' . $op_op->op_op_name . '</label>
                            <input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">
                            <input type="text" name="op_op_value_' . $attribute_id . '[]"  class="form-control">
                        </div>';

                echo $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price);
            }
        } elseif (@$options->option_type == 2) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                            ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                            ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                            ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                            ->get('product_attr_option_option')->result();

            //$q .= '<input type="hidden" name="op_id_'.$attribute_id.'[]" value="' . $options->att_op_id. '" class="form-control">';
            echo '<input type="hidden" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';

            echo '<br><div class="col-sm-12">
                            <select class="form-control select2 " id="op_' . $options->att_op_id . '" name="op_op_id_' . $attribute_id . '[]"  onChange="OptionOptionsOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                                    <option value="0">--Select one--</option>';

            foreach ($opops as $op_op) {
                echo '<option value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">' . $op_op->op_op_name . '</option>';
            }

            echo '</select></div><br>';

            echo '<div class="col-sm-12">
                                <div class="col-sm-12" style="margin-top:-7px;"></div>
                            </div><br>';
        } elseif (@$options->option_type == 1) {

            echo '<br>
                        <div class="col-sm-12">
                            <input type="text" name="op_value_' . $attribute_id . '[]" class="form-control">
                        </div>
                    <br>';
        } else {

            echo '';
        }



    }





    public function c_receipt($order_id){
        $orderData = $this->Order_model->get_orderd_by_id($order_id);
        $data['orderd'] = $orderData;



        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);
        //dd($data['order_details']);
        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

               
                        
        $data['company_profile'] = $this->settings->company_profile();
        $data['cinfo'] = $this->settings->single_company_profile($data['orderd']->level_id);

        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $data['check_re_order_status'] = 0;
        $check_re_order = $this->db->where('clevel_order_id',$orderData->clevel_order_id)->get('b_to_b_level_quatation_tbl')->row();

        if(!empty($check_re_order))
            $data['check_re_order_status'] = 1;
        $productData = $this->db->select('a.product_id,a.product_name')
            ->from('product_tbl a')
            ->join('qutation_details b','b.product_id=a.product_id','left')
            ->where('b.order_id',$orderData->clevel_order_id)
            ->get()->result();

        $userData  = $this->db->select('a.id,a.first_name,a.last_name')
            ->from('user_info a')
            ->join('b_user_catalog_request b','b.b_user_id=a.id')
            ->where('b.requested_by',$this->level_id)
            ->get()->result()
        ;


        $data['b_user_data'] = $userData;
        $data['re_order_product_data'] = $productData;
        $data['clevel_order_id'] = $orderData->clevel_order_id;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/orders/order_receipt_c', $data);
        $this->load->view('b_level/footer');

    }


}
