<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/BAccount_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    ============ its for add_category ===================
    public function add_category() {


        $this->permission->check_label('category')->create()->redirect();

        $data['parent_category'] = $this->db->select('category_id, category_name')->where('parent_category', 0)->where('status', 1)->get('category_tbl')->result();
//        dd($data['parent_category']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_add', $data);
        $this->load->view('b_level/footer');
    }

//==================== its for save_category ================
    public function save_category() {
        $createdate = date('Y-m-d');
        $category_name = $this->input->post('category_name');
        $parent_category = $this->input->post('parent_category');
        $description = $this->input->post('description');
        $status = $this->input->post('status');
        $category_data = array(
            'category_name' => $category_name,
            'parent_category' => $parent_category,
            'description' => $description,
            'status' => $status,
            'created_by' => $this->user_id,
            'created_date' => $createdate,
        );
//        echo '<pre>';        print_r($category_data);die();
        $this->db->insert('category_tbl', $category_data);
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Category information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category save successfully!</div>");
        redirect("manage-category");
    }

//    ============ its for manage_category ===================
    public function manage_category() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->permission->check_label('category')->read()->redirect();

        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $created_by)
            ->get('category_tbl')->result();

        $config["base_url"] = base_url('b_level/Category_controller/manage_category');
        $config["total_rows"] = $this->db->count_all('category_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["category_list"] = $this->Category_model->category_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_manage', $data);
        $this->load->view('b_level/footer');
    }

//    ============ its for category_edit ===================
    public function category_edit($id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $created_by)
            ->get('category_tbl')->result();
        $data['category_edit'] = $this->Category_model->category_edit($id);
        $data['category_assigned_product'] = $this->Category_model->category_assigned_product($id);
//        dd($data['category_edit']);
//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_edit', $data);
//        $this->load->view('b_level/footer');
    }

//==================== its for category_update ================
    public function category_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "Category information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $updatedate = date('Y-m-d');
        $category_name = $this->input->post('category_name');
        $parent_category = $this->input->post('parent_category');
        $description = $this->input->post('description');
        $status = $this->input->post('status');
        $category_data = array(
            'category_name' => $category_name,
            'parent_category' => $parent_category,
            'description' => $description,
            'status' => $status,
            'updated_by' => $this->user_id,
            'updated_date' => $updatedate,
        );
//        echo '<pre>';        print_r($category_data);die();
        $this->db->where('category_id', $id);
        $this->db->update('category_tbl', $category_data);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category updated successfully!</div>");
        redirect("manage-category");
    }

//    ============ its for category_assign ===================
    public function category_assign()
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->permission->check_label('assigned')->create()->redirect();

        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $created_by)
            ->get('category_tbl')->result();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_assign', $data);
        $this->load->view('b_level/footer');
    }

    public function load_category($cat_id) {

        $cat = $this->db->select('category_id, category_name')
                        ->where('parent_category', 0)->where('category_id', $cat_id)
                        ->get('category_tbl')->row();

        $category_wise_subcategory = $this->db->select('*')
                        ->where('parent_category', $cat_id)
                        ->get('category_tbl')->result();

        $q = "";
        $q .= '<li><a href="#">' . $cat->category_name . '</a><ul>';
        foreach ($category_wise_subcategory as $key => $value) {
            $q .= '<li>' . $value->category_name . '</li>';
        }
        $q .= '</ul></li>';
        echo $q;
    }

//    =============== its for category_filter ============
    public function category_filter() {
        $data['get_filter_data'] = '';

        if ($this->input->post('category_filter')) {
            $data['cat_name'] = $this->input->post('cat_name', TRUE);
            $data['parent_cat'] = $this->input->post('parent_cat', TRUE);
            $data['category_status'] = $this->input->post('category_status', TRUE);
            $data['get_filter_data'] = $this->Category_model->get_filter_data($data['cat_name'], $data['parent_cat'], $data['category_status']);
//            print_r($data['get_filter_data'] );
        } else {
            $data['cat_name'] = $this->input->post('cat_name', TRUE);
            $data['parent_cat'] = $this->input->post('parent_cat', TRUE);
            $data['category_status'] = $this->input->post('category_status', TRUE);
            $data['get_filter_data'] = $this->Category_model->get_filter_data($data['cat_name'], $data['parent_cat'], $data['category_status']);
        }
        $data['parent_categorylist'] = $this->db->select('category_id, category_name')->where('parent_category', 0)->where('status', 1)->get('category_tbl')->result();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_filter');
        $this->load->view('b_level/footer');
    }

//    ========== its for category delete =============
    public function b_category_delete($category_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "category information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $pattern_info = $this->db->select('*')->from('pattern_model_tbl')->where('category_id', $category_id)->get()->result();
        if ($pattern_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some patterns!</div>");
            redirect("manage-category");
////        ============ its for pattern model tbl info update =============
//            $pattern_model = array(
//                'category_id' => 0,
//                'status' => 0,
//            );
//            $this->db->where('category_id', $category_id)->update('pattern_model_tbl', $pattern_model);
        }
        $condition_info = $this->db->select('*')->from('product_conditions_tbl')->where('category_id', $category_id)->get()->result();
        if ($condition_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some conditions!</div>");
            redirect("manage-category");
//            $codition_data = array(
//                'category_id' => 0,
//                'is_active' => 0,
//            );
//            $this->db->where('category_id', $category_id)->update('product_conditions_tbl', $codition_data);
        }
        $product_cat_info = $this->db->select('*')->from('product_tbl')->where('category_id', $category_id)->get()->result();
        if ($product_cat_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some products!</div>");
            redirect("manage-category");
//            $product_cat_data = array(
//                'category_id' => 0,
//                'is_active' => 0,
//            );
//            $this->db->where('category_id', $category_id)->update('product_tbl', $product_cat_data);
        }
        $product_subcat_info = $this->db->select('*')->from('product_tbl')->where('subcategory_id', $category_id)->get()->result();
        if ($product_subcat_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some sub categories!</div>");
            redirect("manage-category");
//            $product_subcat_data = array(
//                'subcategory_id' => 0,
//                'is_active' => 0,
//            );
//            $this->db->where('subcategory_id', $category_id)->update('product_tbl', $product_subcat_data);
        }
        $attributes_info = $this->db->select('*')->from('attribute_tbl')->where('category_id', $category_id)->get()->result();
        if ($attributes_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some attributes!</div>");
            redirect("manage-category");
//            $attributes_data = array(
//                'category_id' => 0,
////                'is_active' => 0,
//            );
//            $this->db->where('category_id', $category_id)->update('attribute_tbl', $attributes_data);
        }
        $this->db->where('category_id', $category_id)->delete('category_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category deleted successfully!</div>");
        redirect("manage-category");
    }

//    ============= its for b_level_category_search ============
    public function b_level_category_search() {
        $keyword = $this->input->post('keyword');
        $data['category_list'] = $this->Category_model->get_category_search_result($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/category/category_search', $data);
    }

//    =========== its for assinged_category_product_delete ===========
    public function assinged_category_product_delete() {
        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $data = array(
            'category_id' => 0,
        );
        $this->db->where('product_id', $product_id)->update('product_tbl', $data);
    }
    /** Start added by insys */
    //    =========== its for manage_action ==============
        public function manage_action(){
            if($this->input->post('action')=='action_delete')
            {
                $this->load->model('Common_model');
                $res = $this->Common_model->DeleteSelected('category_tbl','category_id');
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
            }
            redirect("manage-category");
        } 
    /** End added by insys */
}
