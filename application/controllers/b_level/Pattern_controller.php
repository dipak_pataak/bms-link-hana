<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pattern_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Pattern_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    ===============its for add_pattern ===============
    public function add_pattern() {

        $this->permission->check_label('manage_pattern')->create()->redirect();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_add');
        $this->load->view('b_level/footer');
    }

//=============== its for pattern_save ==============
    public function pattern_save() {
        $createdate = date('Y-m-d');
        $category_id = $this->input->post('category_id');
        $pattern_name = $this->input->post('pattern_name');
        $pattern_type = $this->input->post('pattern_type');
        $status = $this->input->post('status');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Pattern information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $pattern_data = array(
            'category_id' => $category_id,
            'pattern_type' => $pattern_type,
            'pattern_name' => $pattern_name,
            'status' => $status,
            'created_by' => $this->user_id,
            'created_date' => $createdate,
            'status' => 1
        );
//        echo '<pre>';        print_r($pattern_data); die();
        $this->db->insert('pattern_model_tbl', $pattern_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern save successfully!</div>");
        redirect("manage-pattern");
    }

//    ===============its for manage_pattern ===============
    public function manage_pattern() {


        $this->permission->check_label('pattern')->create()->redirect();

        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $this->level_id)
            ->get('category_tbl')
            ->result();
        $this->db->where('created_by',$this->level_id);
        $this->db->from("pattern_model_tbl");
        $total_rows = $this->db->count_all_results();

        $config["base_url"] = base_url('b_level/Pattern_controller/manage_pattern');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["pattern_list"] = $this->Pattern_model->pattern_list($config["per_page"], $page);
//        echo '<pre>';        print_r($data["pattern_list"]);die();
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_manage', $data);
        $this->load->view('b_level/footer');
    }

//    ===============its for pattern_edit ===============
    public function pattern_edit($id) {
        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('created_by',$this->level_id)
            ->where('parent_category', 0)
                        ->where('status', 1)->get('category_tbl')->result();
        $data['pattern_edit'] = $this->Pattern_model->pattern_edit($id);
        $data['assigned_pattern_product'] = $this->Pattern_model->assigned_pattern_product($id);

//        $this->load->view('b_level/header', $data);
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_edit', $data);
//        $this->load->view('b_level/footer');
    }

//=============== its for pattern_update ==============
    public function pattern_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "Updated";
        $remarks = "pattern information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $update_date = date('Y-m-d');
        $category_id = $this->input->post('category_id');
        $pattern_name = $this->input->post('pattern_name');
        $pattern_type = $this->input->post('pattern_type');
        $status = $this->input->post('status');

        $pattern_data = array(
            'category_id' => $category_id,
            'pattern_type' => $pattern_type,
            'pattern_name' => $pattern_name,
            'updated_by' => $this->user_id,
            'updated_date' => $update_date,
            'status' => $status,
        );
//        echo '<pre>';        print_r($pattern_data); die();
        $this->db->where('pattern_model_id', $id);
        $this->db->update('pattern_model_tbl', $pattern_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern updated successfully!</div>");
        redirect("manage-pattern");
    }

//    ============== its for pattern_model_filter ============
    public function pattern_model_filter() {
        $data['parent_category'] = $this->db->select('category_id, category_name')->where('parent_category', 0)->where('status', 1)->get('category_tbl')->result();
        $data['pattern_name'] = $this->input->post('pattern_name');
        $data['parent_cat'] = $this->input->post('parent_cat');
        $data['pattern_status'] = $this->input->post('pattern_status');
        $data['pattern_model_filter'] = $this->Pattern_model->pattern_model_filter($data['pattern_name'], $data['parent_cat'], $data['pattern_status']);


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_model_filter');
        $this->load->view('b_level/footer');
    }

    public function category_wise_pattern($category_id = null) {
        if ($category_id == 'none') {
            $category_id = ' ';
            $where = "category_id < 0";
        } else {
            $where = "category_id = $category_id";
        }
        $category_wise_pattern = $this->db->select('*')
//                        ->where('category_id', $category_id)
                        ->where($where)
                        ->where('status', 1)
                        ->get('pattern_model_tbl')->result();
//         $this->load->view('b_level/pattern/category_wise_pattern', $data);

        $q = "";
        if (!empty($category_wise_pattern)) {

            $q .= '<select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true" required>
                                ';
            foreach ($category_wise_pattern as $value) {
                $q .= "<option value='$value->pattern_model_id'>$value->pattern_name</option>";
            }
            $q .= '</select>';
        }
        echo $q;
    }

//    ============ its for pattern_delete =============
    public function pattern_delete($pattern_id) {
        $this->db->select('pattern_models_ids');
        $this->db->from('product_tbl  a');
        $this->db->where("FIND_IN_SET($pattern_id, pattern_models_ids)");
        $check_pattern = $this->db->get();
//        echo '<pre>';        print_r($check_colors);die();
        if ($check_pattern->num_rows() > 0) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Can't delete it. pattern already assigned!</div>");
            redirect("manage-pattern");
        } else {
            $this->db->where('pattern_model_id', $pattern_id)->delete('pattern_model_tbl');
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "deleted";
            $remarks = "pattern information deleted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern deleted successfully!</div>");
            redirect("manage-pattern");
        }
    }

//    =============== b_level_pattern_search ============
    public function b_level_pattern_search() {
        $keyword = $this->input->post('keyword');
        $data['pattern_list'] = $this->Pattern_model->get_pattern_search_result($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/pattern/pattern_search', $data);
    }

//    ============= its for b_assinged_product_delete ==============
    public function b_assinged_product_delete() {
        $product_id = $this->input->post('product_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
//        echo $product_id;        echo $pattern_model_id;
        $array1 = Array($pattern_model_id);
        $all_patters = $this->db->select('pattern_models_ids')->from('product_tbl')->where('product_id', $product_id)->get()->row();
        $list = $all_patters->pattern_models_ids;
        $array2 = explode(',', $list);
        $array3 = array_diff($array2, $array1);
        $output = implode(',', $array3);
        $data = array(
            'pattern_models_ids' => $output,
        );
        $this->db->where('product_id', $product_id)->update('product_tbl', $data);
    }
/** Start added by insys */
//    =========== its for manage_action ==============
public function manage_action(){
    if($this->input->post('action')=='action_delete')
    {
        $this->load->model('Common_model');
        $res = $this->Common_model->DeleteSelected('pattern_model_tbl','pattern_model_id');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Pattern has been Deleted successfully.</div>");
    }
    redirect("manage-pattern");
} 
/** End added by insys */
}
