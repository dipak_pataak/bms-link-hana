<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menusetup_controller extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }

        $this->load->model(array(
            'b_level/Menusetup_model'
        ));
    }

    public function index() {
        
    }

    public function menu_setup($arg = null) {
        $this->permission->check_label('menuset')->create()->redirect();
//        $data['parent_menu'] = $this->db->select('id, menu_title')->where('parent_menu', 0)->get('b_menusetup_tbl')->result();
        $data['parent_menu'] = $this->db->select('id, menu_title')->get('b_menusetup_tbl')->result();

        $config["base_url"] = base_url('b_level/menusetup_controller/menu_setup');
        $config["total_rows"] = $this->db->count_all('b_menusetup_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["menusetuplist"] = $this->Menusetup_model->menu_setuplist($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/role/menusetup', $data);
        $this->load->view('b_level/footer');
    }

//============= its for menusetup_save ==================
    public function menusetup_save() {

        $menu_name = $this->input->post('menu_name');
        $korean_name = $this->input->post('korean_name');
        $url = $this->input->post('url');
        $module = $this->input->post('module');
        $order = $this->input->post('order');
        $parent_menu = $this->input->post('parent_menu');
        $menu_type = $this->input->post('menu_type');
        $icon = $this->input->post('icon');

        $checkMenuSetup = $this->Menusetup_model->checkMenuSetup($menu_name, $module);
        
        if ($checkMenuSetup) {

            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>its allready exists!</div>");
            redirect('b-menu-setup');
        } else {

            $allMenu = array(
                'menu_title' => strtolower($menu_name),
                'korean_name' => $korean_name,
                'page_url' => $url,
                'module' => $module,
                'ordering' => $order,
                'parent_menu' => $parent_menu,
                'menu_type' => $menu_type,
                'icon' => $icon,
                'created_by' => $this->user_id,
            );
            $this->Menusetup_model->menusetup_save($allMenu);
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Menu save successfully!</div>");
        redirect('b_level/menusetup_controller/menu_setup');
    }

    public function menusetup_edit($menu_id) {
        $data['single_menu_edit'] = $this->Menusetup_model->single_menu_edit($menu_id);
        $menu_type = $data['single_menu_edit']->menu_type;
        $data['parent_menu'] = $this->db->select('id, menu_title')->where('menu_type', $menu_type)->get('b_menusetup_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/role/menusetup_edit', $data);
        $this->load->view('b_level/footer');
    }

//    ============= its for menusetup_update ========== 
    public function menusetup_update($menu_id) {
        $menu_name = $this->input->post('menu_name');
        $korean_name = $this->input->post('korean_name');
        $url = $this->input->post('url');
        $module = $this->input->post('module');
        $order = $this->input->post('order');
        $parent_menu = $this->input->post('parent_menu');
        $menu_type = $this->input->post('menu_type');
        $icon = $this->input->post('icon');

        $allMenu = array(
            'menu_title' => strtolower($menu_name),
            'korean_name' => $korean_name,
            'page_url' => $url,
            'module' => $module,
            'ordering' => $order,
            'parent_menu' => $parent_menu,
            'menu_type' => $menu_type,
            'icon' => $icon,
            'created_by' => $this->user_id,
        );

        $this->db->where('id', $menu_id);
        $this->db->update('b_menusetup_tbl', $allMenu);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Menu update successfully!</div>");
//        redirect('b_level/menusetup_controller/menusetup_edit/'.$menu_id);

        redirect('b_level/menusetup_controller/menu_setup');
    }

//    ============= its for menu delete =============
    public function menusetup_delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('b_menusetup_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Menu deleted successfully!</div>");
        redirect($_SERVER['HTTP_REFERER']);
    }

//    ============ its for b_level_menu_search =============
    public function b_level_menu_search() {
        $keyword = $this->input->post('keyword');
        $data['get_menu_search_result'] = $this->Menusetup_model->b_level_menu_search($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/role/menu_search_result', $data);
    }

//    ========== its for menu_type_wise_parent_menu ==========
    public function menu_type_wise_parent_menu() {
        $menu_type = $this->input->post('type_id');
        $parent_menu = $this->db->select('*')->from('b_menusetup_tbl')->where('menu_type', $menu_type)->get()->result();
        $arr = array();
        $i = 0;
        foreach ($parent_menu as $single) {
            $arr[$i]['id'] = $single->id;
            $arr[$i]['menu_title'] = str_replace("_", " ", ucfirst($single->menu_title));
            $i++;
        }
//        echo '<pre>';        print_r($arr);
        echo json_encode($arr);
    }

//    ===================== its for menusetup_inactive ===============
    public function menusetup_inactive($id) {
        $data = array(
            'status' => 0,
        );
        $this->db->where('id', $id);
        $this->db->update('b_menusetup_tbl', $data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Menu inactive successfully!</div>");
        redirect('b-menu-setup');
    }

//    ===================== its for menusetup_active ===============
    public function menusetup_active($id) {
        $data = array(
            'status' => 1,
        );
        $this->db->where('id', $id);
        $this->db->update('b_menusetup_tbl', $data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Menu active successfully!</div>");
        redirect('b-menu-setup');
    }
//    ========= its for menu_export_csv ===========
    public function menu_export_csv() {
        $offset = $this->input->post('ofset');
        $limit = $this->input->post('limit');

        if(($offset>0) && ($limit>=1)) {
            if($offset == 1) { $offset = 0; }
            // file name 
            $filename = 'menu_' . date('Y-m-d') . '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");
            
            // get data 
            $usersData = $this->Menusetup_model->menu_csv_data($offset,$limit);
            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('menu_id', 'menu_title', 'korean_name', 'page_url', 'module', 'parent_menu');
            fputcsv($file, $header);
            foreach ($usersData as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            exit;
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>'Please check valid start and limit data!</div>");
            redirect('b-menu-setup');
        }
    }

//    =========== its for import_menu_update ===========
    public function import_menu_update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "update";
        $remarks = "menu csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['menu_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['korean_name'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                }
                if ($count > 0) {
                    $data = array(
                        'id' => $insert_csv['menu_id'],
                        'korean_name' => $insert_csv['korean_name'],
                    );
                    $this->db->where('id', $data['id']);
                    $this->db->update('b_menusetup_tbl', $data);
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Menu Updated successfully!</div>");
        redirect('b-menu-setup');
    }
}
