<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Return_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Supplier_model');
        $this->load->model('b_level/User_model');
        $this->load->model('b_level/purchase_model');
        $this->load->model('b_level/pattern_model');
        $this->load->model('b_level/Return_model');
        $this->load->model('common_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

    //    ==========its for own user customer count =========
    public function own_level_returned_order_count() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('order_return_tbl');
        return $num_rows;
    }

//    ================ its for customer_return ===============
    public function customer_return() {

        $this->permission->check_label('customer_return')->create()->redirect();

        $config["base_url"] = base_url('b_level/Return_controller/customer_return');
        //$config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_level_returned_order_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["customer_return"] = $this->Return_model->customer_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/returns/customer_return');
        $this->load->view('b_level/footer');
    }

    //    ================ its for customer_order_return =============
    public function customer_order_return($order_id) {
        $data['orders'] = $this->Return_model->get_order_info($order_id);
        $data['order_details'] = $this->Return_model->get_order_details($order_id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/returns/customer_order_return_form', $data);
        $this->load->view('b_level/footer');
    }

//    ============== its for customer_order_return_save =============
    public function customer_order_return_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "customer order return information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $product_id = $this->input->post('product_id');
        $return_qty = $this->input->post('return_qty');
        $return_comments = $this->input->post('return_comments');
        $customer_id = $this->input->post('customer_id');
        $order_id = $this->input->post('order_id');
        $is_approved = 0;
        $approved_by = 0;

        $order_return_data = array(
            'order_id' => $order_id,
            'customer_id' => $customer_id,
            'return_date' => date('Y-m-d'),
            'return_comments' => $return_comments,
            'returned_by' => $this->user_id,
            'is_approved' => $is_approved,
            'approved_by' => $approved_by,
            'level_id' => $level_id,
            'created_by' => $this->user_id,
            'create_date' => date('Y-m-d h:i:s'),
        );
        $check_order_id = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $order_id)->get()->row();
        if ($check_order_id->order_id) {
            $this->db->where('order_id', $order_id)->update('order_return_tbl', $order_return_data);
            $return_id = $check_order_id->return_id;
        } else {
            $this->db->insert('order_return_tbl', $order_return_data);
            $return_id = $this->db->insert_id();
        }
        for ($i = 0; $i < count($product_id); $i++) {
            $order_return_details = array(
                'return_id' => $return_id,
                'product_id' => $product_id[$i],
                'return_qty' => $return_qty[$i],
            );
            if ($return_qty[$i] != '') {
                $this->db->insert('order_return_details', $order_return_details);
            }
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Returned successfully!</div>");
        redirect("b-order-list");
    }

//    =========== its for show_return_resend ===========
    public function show_return_resend() {
        $data['return_id'] = $this->input->post('return_id');
        $data['order_id'] = $this->input->post('order_id');
        $data['returned_by'] = $this->input->post('returned_by');
        $data['order_details'] = $this->db->select('*')
                        ->from('order_return_details a')
                        ->join('product_tbl b', 'b.product_id = a.product_id')
                        ->where('a.return_id', $data['return_id'])
//                                           ->where('a.return_id', $data['product_id'])
                        ->where('a.replace_qty', 0)
                        ->get()->result();


        $this->load->view('b_level/returns/show_return_resend_form', $data);
    }

//    ============ its for return_resend_comment =============
    public function return_resend_comment() {
        $product_id = $this->input->post('product_id');
        $return_id = $this->input->post('return_id');
        $returned_by = $this->input->post('returned_by');
        $resend_comment = $this->input->post('resend_comment');
        $resend_status = $this->input->post('resend_status');
        $rma = $this->input->post('rma');
        $replace_qty = $this->input->post('replace_qty');
        $resend_date = date('Y-m-d');
        $resend_data = array(
            'is_approved' => $resend_status,
            'approved_by' => $this->user_id,
            'rma_no' => $rma,
            'resend_comment' => $resend_comment,
            'resend_date' => $resend_date,
        );
        $this->db->where('return_id', $return_id);
        $this->db->update('order_return_tbl', $resend_data);
        for ($i = 0; $i < count($product_id); $i++) {
            $order_return_details = array(
                'return_id' => $return_id,
                'product_id' => $product_id[$i],
                'replace_qty' => $replace_qty[$i],
                'replace_date' => date('Y-m-d'),
            );
            if ($replace_qty[$i] != '') {
                $this->db->insert('order_return_details', $order_return_details);
            }
        }
        if ($resend_status == 2) {
            $notification_text = 'Returned Rejected';
        } elseif ($resend_status == 1) {
            $notification_text = 'Returned Accepted';
        }
        $notifications_data = array(
            'notification_text' => $notification_text,
            'go_to_url' => 'customer-return',
            'created_by' => $returned_by,
            'date' => date('Y-m-d'),
        );
        $this->db->insert('c_notification_tbl', $notifications_data);


        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "return resend comment information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Resend successfully!</div>");
        redirect("b-customer-return");
    }

//    ================ its for supplier_return ===============
    public function raw_material_supplier_return() {
        $this->permission->check_label('raw_material_supplier_return')->create()->redirect();

        $total_rows = $this->db->where('created_by',$this->level_id)->count_all_results('raw_material_return_tbl');

        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $config["base_url"] = base_url('b_level/Return_controller/raw_material_supplier_return');
        $config["total_rows"] = $total_rows;

        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["get_raw_material_supplier_return"] = $this->Return_model->get_raw_material_supplier_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/returns/supplier_return');
        $this->load->view('b_level/footer');
    }

//    ================ its for raw_material_return_purchase =============
    public function raw_material_return_purchase($purchase_id) {
        $data['purchase'] = $this->purchase_model->get_purchase_by_id($purchase_id);
        $data['purchase_details'] = $this->purchase_model->get_purchase_details_by_id($purchase_id);
//        dd($data['purchase_details']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/returns/raw_material_return_purchase', $data);
        $this->load->view('b_level/footer');
    }

//    ==================== its for raw material return purchase save ============
    public function raw_material_return_purchase_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "raw material return puchase information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $purchase_id = $this->input->post('purchase_id');
        $return_comments = $this->input->post('return_comments');
        $is_approved = 0;
        $approved_by = 0;
        $raw_material_id = $this->input->post('raw_material_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $return_qty = $this->input->post('return_qty');

        $raw_material_return_data = array(
            'purchase_id' => $purchase_id,
            'return_date' => date('Y-m-d'),
            'return_comments' => $return_comments,
            'returned_by' => $this->user_id,
            'is_approved' => $is_approved,
            'approved_by' => $approved_by,
            'created_by' => $this->user_id,
            'create_date' => date('Y-m-d'),
        );
//        echo '<pre>';        print_r($raw_material_return_data);die();
        $check_purchase_id = $this->db->select('*')->from('raw_material_return_tbl a')->where('a.purchase_id', $purchase_id)->get()->row();
        if ($check_purchase_id->purchase_id) {
            $this->db->where('purchase_id', $purchase_id)->update('raw_material_return_tbl', $raw_material_return_data);
            $return_id = $check_purchase_id->purchase_id;
        } else {
            $this->db->insert('raw_material_return_tbl', $raw_material_return_data);
            $return_id = $this->db->insert_id();
        }
        for ($i = 0; $i < count($raw_material_id); $i++) {
            $raw_material_return_details = array(
                'return_id' => $return_id,
                'raw_material_id' => $raw_material_id[$i],
                'pattern_model_id' => $pattern_model_id[$i],
                'color_id' => $color_id[$i],
                'return_qty' => $return_qty[$i],
            );

            if ($return_qty[$i] != '') {
                $this->db->insert('raw_material_return_details_tbl', $raw_material_return_details);
            }
            $raw_material_return_stock_details = array(
//                'return_id' => $return_id,
                'row_material_id' => $raw_material_id[$i],
                'pattern_model_id' => $pattern_model_id[$i],
                'color_id' => $color_id[$i],
                'out_qty' => $return_qty[$i],
                'from_module' => "From Raw Material Return",
                'stock_date' => date('Y-m-d'),
            );
            if ($return_qty[$i] != '') {
                $this->db->insert('row_material_stock_tbl', $raw_material_return_stock_details);
            }
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Returned successfully!</div>");
        redirect("purchase-list");
    }

//    ============= its for raw_material_return_approved =========
    public function raw_material_return_purchase_approved() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "approved";
        $remarks = "raw material purchase  approved";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $return_id = $this->input->post('return_id');
        $raw_material_id = $this->input->post('raw_material_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $return_qty = $this->input->post('return_qty');

        $approve_data = array(
            'is_approved' => 1,
            'approved_by' => $this->user_id,
        );
        $this->db->where('return_id', $return_id);
        $this->db->update('raw_material_return_tbl', $approve_data);

        for ($i = 0; $i < count($raw_material_id); $i++) {
            $raw_material_return_stock_details = array(
//                'return_id' => $return_id,
                'row_material_id' => $raw_material_id[$i],
                'pattern_model_id' => $pattern_model_id[$i],
                'color_id' => $color_id[$i],
                'out_qty' => $return_qty[$i],
                'from_module' => "From Raw Material Return",
                'stock_date' => date('Y-m-d'),
            );
            $this->db->insert('row_material_stock_tbl', $raw_material_return_stock_details);
        }
//        echo "Returned approved successfully!";
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Approved successfully!</div>");
        redirect("raw-material-supplier-return");
    }

//    ============== its for supplier_return_filter =============
    public function supplier_return_filter() {
        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $data['invoice_no'] = $this->input->post('invoice_no');
        $data['supplier_name'] = $this->input->post('supplier_name');
//        $material_name = $this->input->post('material_name');
        $data['supplier_return_filter'] = $this->Return_model->supplier_return_filter($data['invoice_no'], $data['supplier_name']);


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/returns/supplier_return_filter');
        $this->load->view('b_level/footer');
    }

//    public function raw_material_return_approved($return_id){
//        $approve_data = array(
//            'is_approved' => 1,
//            'approved_by' => $this->user_id,
//        );
//        $this->db->where('return_id', $return_id);
//        $this->db->update('raw_material_return_tbl', $approve_data);
//        
//          $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Approved successfully!</div>");
//        redirect("raw-material-supplier-return");
//    }
////    ============= its for raw_material_return_rejected =========
//    public function raw_material_return_rejected($return_id){
//        $rejected_data = array(
//            'is_approved' => 0,
//            'approved_by' => $this->user_id,
//        );
//        $this->db->where('return_id', $return_id);
//        $this->db->update('raw_material_return_tbl', $rejected_data);
//        
//          $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Rejected successfully!</div>");
//        redirect("raw-material-supplier-return");
//    }
//    ============ its for product_return =============
    public function product_return() {
        $this->permission->check_label('product_return')->create()->redirect();
        echo 'Comming soon............';
    }

}
