<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Image_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');


        $this->load->model(array(
            'b_level/pattern_model', 'b_level/Settings'
        ));
    }

    public function index() {
        
    }

    public function add_image() {
        $this->permission->check_label('image')->create()->redirect();

        $config["base_url"] = base_url('b_level/Image_controller/add_image');        
        $data["tags"] = $this->db->from('tag_tbl')->get()->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/add_image', $data);
        $this->load->view('b_level/footer');
    }


    public function save_image() {
        $this->permission->check_label('image')->create()->redirect();
        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/gallery/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("add_b_gallery_image");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {

            @$upload_file = '';
        }

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $imageData = array(
            'image' => $upload_file,
            'updated_by' => $this->user_id,
            'created_by' => $level_id
        );

        if ($this->db->insert('gallery_image_tbl', $imageData)) {
            $image_id = $this->db->insert_id();
            $tagdata = $this->db->from('tag_tbl')->where('created_by',$this->level_id)->get()->result_array();
            foreach ($tagdata as $tag) {
                $tag_id = $tag['id'];
                $tag_value = $this->input->post($tag['tag_name']);

                if($tag_value != '') {
                    $galleryTagData = array(
                        'image_id' => $image_id,
                        'tag_id' => $tag_id,
                        'tag_value' => $tag_value,                    
                        'created_date' => date('Y-m-d'),
                        'updated_date' => date('Y-m-d'),
                        'updated_by' => $this->user_id,
                        'created_by' => $level_id
                    );
                    $this->db->insert('gallery_tag_tbl', $galleryTagData);
                }
            }
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Image added successfully! </div>");
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
        }
        redirect("manage_b_gallery_image");
    }

    public function manage_image() {

        $total_rows = $this->db->where('created_by',$this->level_id)->count_all_results('gallery_image_tbl');

        $this->permission->check_label('image')->create()->redirect();
        $config["base_url"] = base_url('b_level/Image_controller/manage_image');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $tags = $this->db->where('created_by',$this->level_id)->order_by('id')->get('tag_tbl')->result_array();

        $filter_arr = array();
        $a = 0 ;
        $final_img_ids = array();
        $is_filter = $is_valid = 0;
        foreach($tags as $key => $tag) {
            $filter_val = $this->input->post('tag_'.$tag['id']);
            if($filter_val != '') {
                $is_filter = 1;
                $image_ids = $this->db->select('image_id')
                        ->where('tag_id', $tag['id'])
                        ->where('tag_value', $filter_val)
                        ->where('created_by',$this->level_id)
                        ->get('gallery_tag_tbl')
                        ->result_array();
                if(count($image_ids) > 0) {
                    $ids = array_column($image_ids, 'image_id');
                    if(count($final_img_ids) > 0 || $is_valid == 1) {
                        $final_img_ids = array_intersect ($final_img_ids, $ids);
                        $is_valid = 1;
                    } else {
                        $final_img_ids = $ids;
                    }
                }
            }
            $tag_value = $this->db->select('DISTINCT(tag_value)')
                            ->where('tag_id', $tag['id'])
                            ->where('created_by',$this->level_id)
                            ->get('gallery_tag_tbl')
                            ->result_array();

            if(count($tag_value) > 0) {
                $filter_arr[$a]['value']= $tag_value;
                $filter_arr[$a]['tag_name'] = $tag['tag_name'];
                $filter_arr[$a]['tag_id'] = $tag['id'];
                $filter_arr[$a]['selected_val'] = $filter_val;
                $a++;
            }
            
        }


        $data['filter_arr'] = $filter_arr;
        if(count($final_img_ids) > 0) {
            $this->db->where_in('id', $final_img_ids);
        } else if($is_filter == 1){
            $this->db->where('1', '0');
        }
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["images"] = $this->db->from('gallery_image_tbl')
                            ->order_by('id', 'desc')
                            ->where('created_by',$this->level_id)
                            ->limit($config["per_page"], $page)
                            ->get()
                            ->result();

        $data["links"] = $this->pagination->create_links();
        $data["patterns"] = $this->db->get('pattern_model_tbl')->result();
        $data['pagenum'] = $page;


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/manage_image', $data);
        $this->load->view('b_level/footer');
    }

    public function delete_image($id) {
       
        $this->db->where('id', $id)->delete('gallery_image_tbl');
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "Tag information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                image deleted successfully! </div>");
        redirect('manage_b_gallery_image');
    }

    public function get_image($id) {
        // $data = $this->db->where('id', $id)->get('tag_tbl')->row();
        $tag_data = $this->db->select('gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value')
                                ->from('gallery_tag_tbl')
                                ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
                                ->where('image_id',$id)
                                ->get()->result_array();

        $data['tag_data'] = array();
        if(count($tag_data) > 0) {
            foreach($tag_data as $row) {
                $data['tag_ids'][] = $row['tag_id'];
                $data['tag_data'][$row['tag_id']] = $row['tag_value'];
            }
        }
        /* echo "<prE>";
        print_r($data);die('stop'); */
        $data['images_tag_data'] = $this->db->from('gallery_tag_tbl')
                                        ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
                                        ->join('gallery_image_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
                                        ->where('image_id',$id)
                                        ->get()
                                        ->row();
                                    
        // echo json_encode($result);
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/edit_image', $data);
        $this->load->view('b_level/footer');

    }
    public function view_image($id) {
        $tag_data = $this->db->select('gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value')
            ->from('gallery_tag_tbl')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->where('image_id',$id)
            ->get()->result_array();

        $data['tag_data'] = array();
        if(count($tag_data) > 0) {
            foreach($tag_data as $row) {
                $data['tag_ids'][] = $row['tag_id'];
                $data['tag_data'][$row['tag_id']] = $row['tag_value'];
            }
        }
        $data['images_tag_data'] = $this->db->from('gallery_tag_tbl')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->join('gallery_image_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
            ->where('image_id',$id)
            ->get()
            ->row();
                                    
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/view_image', $data);
        $this->load->view('b_level/footer');

    }

    public function update_image($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "Tag information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/gallery/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {

            @$upload_file = $this->input->post('hidden_image');
        }

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $imageData = array(
            'image' => $upload_file
        );

        $this->db->where('id', $id)
                ->update('gallery_image_tbl', $imageData);

        $tagdata = $this->db->from('tag_tbl')
                        ->get()
                        ->result_array();
        foreach ($tagdata as $tag) {
            $tag_value = $this->input->post('tag_'.$tag['id']);
            
            $record_exist = $this->db->from('gallery_tag_tbl')
                        ->where('image_id', $id)
                        ->where('tag_id', $tag['id'])
                        ->get()
                        ->row_array();
            if(!$record_exist) {
                if($tag_value != '') {
                    $galleryTagData = array(
                        'image_id' => $id,
                        'tag_id' => $tag['id'],
                        'tag_value' => $tag_value,                    
                        'created_date' => date('Y-m-d')
                    );
                    $this->db->insert('gallery_tag_tbl', $galleryTagData);
                }
            } else {
                $galleryTagData = array(
                    'tag_value' => $tag_value,                    
                    'updated_date' => date('Y-m-d')
                );
                $this->db->where('image_id', $id)
                    ->where('tag_id', $tag['id'])
                    ->update('gallery_tag_tbl', $galleryTagData);
                $this->db->where('tag_value', '')->delete('gallery_tag_tbl');
            }
        }
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Image update successfully </div>");
        redirect('manage_b_gallery_image');
    }
}
