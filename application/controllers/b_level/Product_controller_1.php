<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_controller extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model(['b_level/Category_model', 'b_level/pattern_model', 'b_level/attribute_model', 'b_level/product_model', 'b_level/condition_model']);
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//===============its for add_product ===============

    public function add_product() {

        $this->permission->check_label('add')->create()->redirect();
        $data['get_category'] = $this->Category_model->get_category();
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['get_product_conditions'] = $this->condition_model->get_product_conditions();
        // $data['attribute_types'] = $this->attribute_model->get_attribute_types();
        $data['style_slist'] = $this->db->get('row_column')->result();
        $data['js'] = "b_level/products/product_addjs";
        $data['colors'] = $this->db->order_by('color_name', 'ASC')->get('color_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/products/product_add', $data);
        $this->load->view('b_level/footer');
    }

    public function product_save() {

        $product_name = $this->input->post('product_name');
        $category_id = $this->input->post('category_id');
        $subcategory_id = $this->input->post('subcategory_id');
        $pattern_models = $this->input->post('pattern_model_id');
        $price_style_id = $this->input->post('price_style_id');
        $sqft_price = $this->input->post('sqft_price');
        $fixed_price = $this->input->post('fixed_price');
        $dealer_price = $this->input->post('dealer_price');
        $status = $this->input->post('status');
        $attribute_type_id = $this->input->post('attribute_type_id');

        //------------
        $colors = $this->input->post('color_id');
        $colors_id = '';
        foreach ($colors as $key => $color_id) {
            $colors_id .= $color_id . ',';
        }
        $colors_ids = rtrim($colors_id, ',');
//        =========== its for multiple id save one field use comma separator ============
        $patterns_id = '';
        foreach ($pattern_models as $key => $pattern_id) {
            $patterns_id .= $pattern_id . ',';
        }
        $patterns_ids = rtrim($patterns_id, ',');

//        $condition_id = $this->input->post('condition_id');
        $shipping = $this->input->post('shipping');
//        $tax = $this->input->post('tax');
//        $init_stock = $this->input->post('init_stock');
//        $individual_price = $this->input->post('individual_price');

        $productData = array(
            'product_name' => $this->input->post('product_name'),
            'category_id' => $this->input->post('category_id'),
            'subcategory_id' => ($this->input->post('subcategory_id') != NULL ? $this->input->post('subcategory_id') : 0),
            'pattern_models_ids' => $patterns_ids,
            'price_style_type' => $this->input->post('price_style_type'),
            'price_rowcol_style_id' => $this->input->post('price_style_id'),
            'sqft_price' => $this->input->post('sqft_price'),
            'fixed_price' => $this->input->post('fixed_price'),
            'dealer_price' => $this->input->post('dealer_price'),
//            'condition_id' => $this->input->post('condition_id'),
            'shipping' => $this->input->post('shipping'),
            'colors' => $colors_ids,
//            'init_stock' => $this->input->post('init_stock'),
//            'individual_price' => $this->input->post('individual_price'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d'),
            'active_status' => $this->input->post('status')
        );

//        echo '<pre>';        print_r($productData); die();

        if ($this->db->insert('product_tbl', $productData)) {

            $product_id = $this->db->insert_id();

            // if(!empty($attributes) && $product_id!=NULL){
            //     $attribute_mapping = [];
            //     foreach ($attributes as $key => $attribute_id) {
            //         $price = $this->input->post('attribute_price_'.$attribute_id);
            //         $price_type = $this->input->post('price_type_'.$attribute_id);
            //         $attribute_mapping[] = array(
            //             'product_id'        => $product_id,
            //             'attribute_id'      => $attribute_id,
            //             'price_type'        => $price_type,
            //             'price'             => (!empty($price)?$price:0),
            //             'created_by'         => $this->session->userdata('user_id'),
            //             'created_date'      => date('Y-m-d'),
            //         );
            //     }
            //     $this->db->insert_batch('product_attribute_mapping',$attribute_mapping);
            //     $productPriceStyleData = array(
            //         'product_id'        => $product_id,
            //         'style_id'          => $this->input->post('price_style_id'),
            //         'created_by'         => $this->session->userdata('user_id'),
            //         'created_date'      => date('Y-m-d'),
            //     );
            // }


            $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                " . $product_name . " Add successfull </div>");

////  =========== its for product condition mapping ==============
//            for ($i = 0; $i < count($condition_id); $i++) {
//                $product_condition_mapping = array(
//                    'condition_id' => $condition_id[$i],
//                    'product_id' => $product_id,
//                );
//                $this->db->insert('product_condition_mapping', $product_condition_mapping);
//            }
////            =========== close product condition mapping ============
        }

        redirect('b_level/product_controller/maping_product_attribute/' . $product_id);
    }

    public function get_price_style($id) {

        $data = $this->db->where('style_id', $id)->get('price_chaild_style')->result();
        $table = '';
        $table .= '<table class="table-bordered">';
        $i = 1;

        foreach ($data as $key => $value) {

            $rowData = explode(',', $value->row);
            $table .= '<tr>';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td>' . $val . '</td>';
                } else {
                    $table .= '<td>' . $val . '</td>';
                }
                $j++;
            }
            $table .= '</tr>';
            $i++;
        }

        $table .= '</table>';

        echo $table;
    }

    //its for product_manage
    public function product_manage() {
        $this->permission->check_label('manage_product')->create()->redirect();

        $total_rows = $this->db->count_all('product_tbl');
        $per_page = 15;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        /* ends of bootstrap */
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $data['get_category'] = $this->Category_model->get_category();
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['products'] = $this->product_model->get_product_list($per_page, $page);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/products/product_manage', $data);
        $this->load->view('b_level/footer');
    }

    function pagination($total_rows, $per_page, $page) {

        $config["base_url"] = base_url('product-manage');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $page;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        return $config;
    }

    public function product_edit($product_id) {
        //$data['subcategory_id'] = $this->db->select('subcategory_id')->get()->row()
        $data['get_category'] = $this->Category_model->get_category();
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        //$data['attribute_types']    = $this->attribute_model->get_attribute_types();
        $data['style_slist'] = $this->db->get('row_column')->result();
        $data['js'] = "b_level/products/product_addjs";
        $data['category_wise_condition'] = $this->condition_model->category_wise_condition($product_id);
        $data['checked_product_wise_condition'] = $this->condition_model->checked_product_wise_condition($product_id);
        $data['product'] = $this->product_model->get_product_by_id($product_id);

        $data['colors'] = $this->db->order_by('color_name', 'ASC')->get('color_tbl')->result();
//        echo '<pre>';        print_r($data['product']); die();
        //$data['product_attribute_mapping'] = $this->product_model->get_product_attribute_mapping_by_id($product_id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/products/product_edit', $data);
        $this->load->view('b_level/footer');
    }

    public function product_update() {
        $product_id = $this->input->post('product_id');

        $product_name = $this->input->post('product_name');
        $category_id = $this->input->post('category_id');
        $subcategory_id = $this->input->post('subcategory_id');
        $pattern_models = $this->input->post('pattern_model_id');
        $price_style_id = $this->input->post('price_style_id');
        $sqft_price = $this->input->post('sqft_price');
        $fixed_price = $this->input->post('fixed_price');
        $dealer_price = $this->input->post('dealer_price');
        $status = $this->input->post('status');
        $attribute_type_id = $this->input->post('attribute_type_id');

        //$attributes = $this->input->post('attribute_id');
//        $condition_id = $this->input->post('condition_id');
        $shipping = $this->input->post('shipping');
//        $tax = $this->input->post('tax');
//        $init_stock = $this->input->post('init_stock');
//        $individual_price = $this->input->post('individual_price');
        //------------
        $colors = $this->input->post('color_id');
        $colors_id = '';
        foreach ($colors as $key => $color_id) {
            $colors_id .= $color_id . ',';
        }
        $colors_ids = rtrim($colors_id, ',');
        //------------------
        //        =========== its for multiple id save one field use comma separator ============
        $patterns_id = '';
        foreach ($pattern_models as $key => $pattern_id) {
            $patterns_id .= $pattern_id . ',';
        }
        $patterns_ids = rtrim($patterns_id, ',');

        $productData = array(
            'product_name' => $this->input->post('product_name'),
            'category_id' => $this->input->post('category_id'),
            'subcategory_id' => ($this->input->post('subcategory_id') != NULL ? $this->input->post('subcategory_id') : 0),
            'pattern_models_ids' => $patterns_ids,
            'price_style_type' => $this->input->post('price_style_type'),
            'price_rowcol_style_id' => $this->input->post('price_style_id'),
            'sqft_price' => $this->input->post('sqft_price'),
            'fixed_price' => $this->input->post('fixed_price'),
            'dealer_price' => $this->input->post('dealer_price'),
//            'condition_id' => $this->input->post('condition_id'),
            'shipping' => $this->input->post('shipping'),
//            'tax' => $this->input->post('tax'),
            'colors' => $colors_ids,
//            'init_stock' => $this->input->post('init_stock'),
//            'individual_price' => $this->input->post('individual_price'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d'),
            'active_status' => $this->input->post('status')
        );
//        echo '<pre>';        print_r($productData);die();

        $result = $this->db->where('product_id', $product_id)->update('product_tbl', $productData);


////            =========== its for product condition mapping ==============
//        $this->db->where('product_id', $product_id);
//        $this->db->delete('product_condition_mapping');
//
//        for ($i = 0; $i < count($condition_id); $i++) {
//            $product_condition_mapping = array(
//                'condition_id' => $condition_id[$i],
//                'product_id' => $product_id,
//            );
//            $this->db->insert('product_condition_mapping', $product_condition_mapping);
//        }
////            =========== close product condition mapping ============

        if ($result) {
            // if(!empty($attributes) && $product_id!=NULL){
            //     $this->db->where('product_id',$product_id)->delete('product_attribute_mapping');
            //     $attribute_mapping = [];
            //     foreach ($attributes as $key => $attribute_id) {
            //         $price = $this->input->post('attribute_price_'.$attribute_id);
            //         $price_type = $this->input->post('price_type_'.$attribute_id);
            //         $attribute_mapping[] = array(
            //             'product_id'        => $product_id,
            //             'attribute_id'      => $attribute_id,
            //             'price_type'        => $price_type,
            //             'price'             => (!empty($price)?$price:0),
            //             'created_by'         => $this->session->userdata('user_id'),
            //             'created_date'      => date('Y-m-d'),
            //         );
            //     }
            //     $this->db->insert_batch('product_attribute_mapping',$attribute_mapping);
            // }

            $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                " . $product_name . " This Product are update successfull </div>");
        }
        //redirect('b_level/product_controller/maping_product_attribute/' . $product_id);

        redirect('product-manage');
    }

    public function delete_product($product_id) {

        $this->db->where('product_id', $product_id)->delete('product_tbl');
//        $this->db->where('product_id', $product_id)->delete('product_attribute_mapping');
//        $this->db->where('product_id', $product_id)->delete('product_condition_mapping');

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Product delete successfull </div>");

        redirect('product-manage');
    }

    public function maping_product_attribute($product_id) {

        $data['product_info'] = $product_info = $this->db->where('product_id', $product_id)->get('product_tbl')->row();

        $data['attributes'] = $this->get_attr_by_category($product_info);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/products/maping_product_attribute', $data);
        $this->load->view('b_level/footer');
    }

    public function get_attr_by_category($product_info) {
        return $this->db->where('category_id', $product_info->category_id)->get('attribute_tbl')->result();
    }

    public function get_attributes($attribute_id) {

        $attribute = $this->db->where('attribute_id', $attribute_id)->get('attribute_tbl')->row();

        $options = $this->db->where('attribute_id', $attribute_id)->get('attr_options')->result();

        $q = '';
        if ($attribute->attribute_type == 2) {

            echo $q .= "<div class='form-group col-md-12'>";

            foreach ($options as $key => $op) {

                $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                $option_option_result = $this->db->query($option_option)->result();

                $q .= "<div class='checkbox'>
                                  <label><input type='checkbox' name='option_name[]' value='" . $op->att_op_id . "'> " . $op->option_name . "</label>
                                </div>";
                $q .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div>";

                if ($op->option_type == 2) {

                    foreach ($option_option_result as $key => $opop) {

                        $q .= "<div class='checkbox'>";
                        $q .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='checkbox-inline'><input type='checkbox' name='option_option_" . $op->att_op_id . "[]' value='" . $opop->op_op_id . "'> " . $opop->op_op_name . "</label>";
                        $q .= "</div><br>";

                        $options_option_options = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();
                        if ($opop->type == 2) {
                            $q .= "<div style='margin-left:50px;'>";
                            foreach ($options_option_options as $key => $opopop) {
                                $q .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='checkbox-inline'><input type='checkbox' name='att_op_op_op_id_" . $op->att_op_id . "_" . $opop->op_op_id . "' value='" . $opopop->att_op_op_op_id . "'> " . $opopop->att_op_op_op_name . "</label>";
                            }
                            $q .= "</div><br>";
                        } else {
                            
                        }
                    }
                } elseif ($op->option_type == 1) {
                    
                }

                $q .= "</div>";
            }

            $q .= "</div>";
        } else {
            
        }


        echo $q;
    }

    public function save_maping_product_attribute() {

        $attribute_id = $this->input->post('attribute');
        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');


        $this->db->where('product_id', $product_id)->delete('product_attribute');
        $this->db->where('product_id', $product_id)->delete('product_attr_option');
        $this->db->where('product_id', $product_id)->delete('product_attr_option_option');
        $this->db->where('product_id', $product_id)->delete('product_attr_option_option_option');

        if (isset($attribute_id) && is_array($attribute_id)) {

            for ($i = 0; $i < count($attribute_id); $i++) {

                $attrData = array(
                    'attribute_id' => $attribute_id[$i],
                    'product_id' => $product_id,
                    'category_id' => $category_id
                );

                $check = $this->db->where('attribute_id', $attribute_id[$i])->where('product_id', $product_id)->get('product_attribute')->row();

                if (empty($check)) {

                    $this->db->insert('product_attribute', $attrData);

                    $pro_att_id = $this->db->insert_id();

                    $options = $this->input->post('option_' . $attribute_id[$i]);

                    if (!empty($options)) {

                        for ($j = 0; $j < count($options); $j++) {

                            $optionData = array(
                                'pro_attr_id' => $pro_att_id,
                                'product_id' => $product_id,
                                'attribute_id' => $attribute_id[$i],
                                'option_id' => $options[$j],
                            );

                            $this->db->insert('product_attr_option', $optionData);

                            $pro_att_op_id = $this->db->insert_id();

                            $option_options = $this->input->post('option_option_' . $options[$j]);

                            if (!empty($option_options)) {


                                for ($k = 0; $k < count($option_options); $k++) {

                                    $OptionOptionData = array(
                                        'pro_att_op_id' => $pro_att_op_id,
                                        'product_id' => $product_id,
                                        'attribute_id' => $attribute_id[$i],
                                        'op_op_id' => $option_options[$k]
                                    );

                                    $this->db->insert('product_attr_option_option', $OptionOptionData);
                                    $pro_att_op_op_id = $this->db->insert_id();

                                    $pro_op_op_op_id = $this->input->post('att_op_op_op_' . $options[$j] . '_' . $option_options[$k]);

                                    if (isset($pro_op_op_op_id) && !empty($pro_op_op_op_id)) {

                                        for ($ooop = 0; $ooop < count($pro_op_op_op_id); $ooop++) {

                                            $OpOpOpData = array(
                                                'pro_att_op_op_id' => $pro_att_op_op_id,
                                                'product_id' => $product_id,
                                                'attribute_id' => $attribute_id[$i],
                                                'op_op_op_id' => $pro_op_op_op_id[$ooop]
                                            );

                                            $this->db->insert('product_attr_option_option_option', $OpOpOpData);
                                        }
                                    }
                                }

                                $this->session->set_flashdata('message', "<div class='alert alert-success'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                Product attribute add successfull </div>");
                            }
                        }
                    } else {

                        // $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                        //     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        //     Please chose the attribute options </div>");
                    }
                } else {

                    $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                            This attribute already added to the product </div>");
                }
            }
        }

        redirect('product-manage');
    }

//    ================= its for product_filter ============
    public function product_filter() {
        $data['get_category'] = $this->Category_model->get_category();
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['product_name'] = $this->input->post('product_name');
        $data['category_name'] = $this->input->post('category_name');
        $data['pattern_model'] = $this->input->post('pattern_model');

        $data['get_product_filter'] = $this->product_model->get_product_filter($data['product_name'], $data['category_name'], $data['pattern_model']);


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/products/product_filter', $data);
        $this->load->view('b_level/footer');
    }

//    ============== its for product_bulk_csv_upload ============
    public function product_bulk_csv_upload() {
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['product_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['category_id'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['subcategory_id'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                    $insert_csv['pattern_models_ids'] = (!empty($csv_line[3]) ? $csv_line[3] : null);
                    $insert_csv['price_style_type'] = (!empty($csv_line[4]) ? $csv_line[4] : null);
                    $insert_csv['price_rowcol_style_id'] = (!empty($csv_line[5]) ? $csv_line[5] : null);
                    $insert_csv['sqft_price'] = (!empty($csv_line[6]) ? $csv_line[6] : null);
                    $insert_csv['fixed_price'] = (!empty($csv_line[7]) ? $csv_line[7] : null);
                    $insert_csv['color_id'] = (!empty($csv_line[8]) ? $csv_line[8] : null);
                    $insert_csv['default_discount'] = (!empty($csv_line[9]) ? $csv_line[9] : null);
                    $insert_csv['active_status'] = (!empty($csv_line[10]) ? $csv_line[10] : null);

                    if ($insert_csv['price_style_type'] == 2) {
                        $sqft_price = $insert_csv['sqft_price'];
                    } else {
                        $sqft_price = 0;
                    }
                    if ($insert_csv['price_style_type'] == 3) {
                        $fixed_price = $insert_csv['fixed_price'];
                    } else {
                        $fixed_price = 0;
                    }
                    if ($insert_csv['active_status'] == 'active') {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                }

                if ($count > 0) {
                    $colors_arr = explode(",", $insert_csv['color_id']);
                    $get_colors = '';
                    for ($j = 0; $j < count($colors_arr); $j++) {
                        $get_color_info = $this->db->select('*')->from('color_tbl')->where('color_name', $colors_arr[$j])->get()->row();
                        $get_colors .= $get_color_info->id . ',';
                    }
                    $get_colors = rtrim($get_colors, ',');

                    $patterns_arr = explode(",", $insert_csv['pattern_models_ids']);
                    $get_patterns = '';
                    for ($j = 0; $j < count($patterns_arr); $j++) {
                        $get_patterns_info = $this->db->select('*')->from('pattern_model_tbl')->where('pattern_name', $patterns_arr[$j])->get()->row();
                        $get_patterns .= $get_patterns_info->pattern_model_id . ',';
                    }
                    $get_patterns = rtrim($get_patterns, ',');
//                    dd($get_patterns);
                    $pattern_models_arr = explode(",", $insert_csv['pattern_models_ids']);
                    $get_category_info = $this->db->select('*')->from('category_tbl')->where('category_name', $insert_csv['category_id'])->get()->result();
                    $get_subcategory_info = $this->db->select('*')->from('category_tbl')->where('category_name', $insert_csv['subcategory_id'])->get()->result();
//                    $get_pattern_info = $this->db->select('*')->from('pattern_model_tbl')->where('pattern_name', $insert_csv['pattern_models_ids'])->get()->result();
                    $get_row_column_style_info = $this->db->select('*')->from('row_column')->where('style_name', $insert_csv['price_rowcol_style_id'])->get()->result();
//                    echo '<pre>';    print_r($get_row_column_style_info); echo $get_row_column_style_info[0]->style_id; die();
                    $data = array(
                        'product_name' => $insert_csv['product_name'],
                        'category_id' => $get_category_info[0]->category_id,
                        'subcategory_id' => $get_subcategory_info[0]->category_id,
                        'pattern_models_ids' => $get_patterns,
                        'price_style_type' => $insert_csv['price_style_type'],
                        'price_rowcol_style_id' => $get_row_column_style_info[0]->style_id,
                        'sqft_price' => $sqft_price,
                        'fixed_price' => $fixed_price,
                        'colors' => $get_colors,
                        'dealer_price' => $insert_csv['default_discount'],
                        'active_status' => $status,
                        'created_by' => $this->user_id,
                        'created_date' => date('Y-m-d'),
                    );
//                      echo '<pre>';    print_r($data); die();
                    $result = $this->db->select('*')
                            ->from('product_tbl')
                            ->where('product_name', $data['product_name'])
                            ->get()
                            ->num_rows();
//                    echo '<pre>';                    print_r($result);                    die();
                    if ($result == 0 && !empty($data['product_name'])) {
                        $this->db->insert('product_tbl', $data);
                        $product_lastid = $this->db->insert_id();
                    } else {
                        $check_product_name = $this->db->select('*')->from('product_tbl')->where('product_name', $data['product_name'])
                                        ->get()->result();
//                        echo $check_product_name[0]->product_id; die();
                        $data = array(
                            'product_name' => $insert_csv['product_name'],
                            'category_id' => $get_category_info[0]->category_id,
                            'subcategory_id' => $get_subcategory_info[0]->category_id,
                            'pattern_models_ids' => $get_patterns,
                            'price_style_type' => $insert_csv['price_style_type'],
                            'price_rowcol_style_id' => $get_row_column_style_info[0]->style_id,
                            'sqft_price' => $sqft_price,
                            'fixed_price' => $fixed_price,
                            'colors' => $get_colors,
                            'dealer_price' => $insert_csv['default_discount'],
                            'active_status' => $status,
                            'updated_by' => $this->user_id,
                            'updated_date' => date('Y-m-d'),
                        );
//                        echo $data['material_name'];                        echo '<pre>'; print_r($data);  die();
                        $this->db->where('product_id', $check_product_name[0]->product_id);
                        $this->db->update('product_tbl', $data);
//                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Raw materials bulk uploaded successfully!</div>");
//                        redirect('row-material-list');
                    }
                }
                $count++;
            }
        }
//        echo "DUke Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Product bulk uploaded successfully!</div>");
        redirect('product-manage');
    }


    public function get_product_pattern($id){

        $p = $this->db->where('product_id',$id)->get('product_tbl')->row();
        $pattern_ids = explode(',', $p->pattern_models_ids);
        

        $result = $this->db->where_in('pattern_model_id',$pattern_ids)->get('pattern_model_tbl')->result();
        $q='<option value="">--Select Pattern --</option>';
        foreach ($result as $key => $value) {
            $q .='<option value="'.$value->pattern_model_id.'">'.$value->pattern_name.'</option>';
        }

        echo $q;
        
    }

    public function group_price_mapping() {

        $data['product_info'] = $this->db->get('product_tbl')->result();
        $data['style_slist'] = $this->db->get('row_column')->result();

        $data['map_info']   = $this->db->select('price_model_mapping_tbl.*,
            product_tbl.product_name,
            pattern_model_tbl.pattern_name,
            row_column.style_name')

        ->join('product_tbl','product_tbl.product_id=price_model_mapping_tbl.product_id','left')
        ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=price_model_mapping_tbl.pattern_id','left')
        ->join('row_column','row_column.style_id=price_model_mapping_tbl.group_id','left')
        ->get('price_model_mapping_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/products/group_price_mapping', $data);
        $this->load->view('b_level/footer');
    }


    public function save_price_model_mapping(){

        $pattern_ids = $this->input->post('pattern_id');


        foreach ($pattern_ids as $key => $value) {

            $mapData = array(
                'group_id'      =>  $this->input->post('group_price'),
                'product_id'    =>  $this->input->post('product_id'),
                'pattern_id'    =>  $value
            );

            $this->db->insert('price_model_mapping_tbl', $mapData);           
        }


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping successfully!</div>");
        redirect('b_level/product_controller/group_price_mapping');

    }


    public function delete_price_model_mapping($id){

        $this->db->where('id',$id)->delete('price_model_mapping_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Delete successfully!</div>");
        redirect('b_level/product_controller/group_price_mapping');

    }

//    ========= its for product onkeyup search =========
    public function b_level_product_search(){
        $keyword = $this->input->post('keyword');
        $data['products'] = $this->product_model->product_search($keyword);
        $this->load->view('b_level/products/product_search', $data);
    }


}
