<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturer_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');


        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Order_model');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Customer_model');
        $this->load->model('b_level/settings');


    }



    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }



    public function save_blind_menufactur(){

        $order_id = $this->input->post('orderid');
        $product_id = $this->input->post('product_id');


        $sc_chk = $this->input->post('sc_chk');
        $val_cut_chk = $this->input->post('val_cut_chk');
        $chk = $this->input->post('chk');


        $hr = $this->input->post('hr');
        $br = $this->input->post('br');
        $tb = $this->input->post('tb');
        $sc = $this->input->post('sc');
        $cl = $this->input->post('cl');


        $blindData = [];
        foreach ($product_id as $key => $id) {

            $opdata =  array(
                'sc_chk'         => @$sc_chk[$id],
                'val_cut_chk'    =>@ $val_cut_chk[$id],
                'chk'            => @$chk[$id],
                'hr'            => @$hr[$id],
                'br'            => @$br[$id],
                'tb'            => @$tb[$id],
                'sc'            => @$sc[$id],
                'cl'            => @$cl[$id]
            );

            $blindData[] = array(
                'order_id'      =>$order_id,
                'product_id'    =>$id,
                'chk_option'    =>json_encode($opdata)
            );


            $this->db->where('order_id',$order_id)->where('product_id',$id)->delete('manufactur_data');
        }


        $this->db->insert_batch('manufactur_data',$blindData);

        echo '1';
    }





//    ============== its for add_manufacturer ===================
    public function add_manufacturer() {

        $this->permission->check_label('manufacturing')->create()->redirect();

        $search = (object) array(

            'product_id'    => $this->input->post('product_id'),
            'customer_id'   => $this->input->post('customer_id'),
            'order_date'    => $this->input->post('order_date'),
            'order_stage'   => $this->input->post('order_stage')

        );
        

        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->customer_id;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;
        
        $total_rows = $this->Order_model->count_table_row($search);
        $per_page = 15;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0; 
        /* ends of bootstrap */
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);
        
        $data["links"]  = $this->pagination->create_links();
        
        $data['orderd'] = $this->Order_model->get_all_manufactur_orderd($search, $per_page, $page);
        
        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;



        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/add_manufacturer',$data);
        $this->load->view('b_level/footer');

    }



    function pagination($total_rows, $per_page, $page) {

        $config["base_url"] = base_url('b_level/Manufacturer_controller/add_manufacturer');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $page;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        return $config;
    }



    public function quatation($order_id){

        $this->permission->check_label('manufacturing')->create()->redirect();

        $data['orderd']         = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details']  = $this->Order_model->get_orderd_details_by_id($order_id);
        $data['chit']  = $this->chit($data['orderd'],$data['order_details']);


        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();
        
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();
        
        $data['for'] = $this->db->where('user_id',$data['orderd']->level_id)->get('company_profile')->row();



        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manufacturing', $data);
        $this->load->view('b_level/footer');

    }




    public function manufacturing_level($order_id){

        $this->permission->check_label('manufacturing')->read()->redirect();

        $data['orderd']         = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details']  = $this->Order_model->get_orderd_details_by_id($order_id);
        $data['chit']  = $this->chit($data['orderd'],$data['order_details']);


        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();
        
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();
        
        $data['for'] = $this->db->where('user_id',$data['orderd']->level_id)->get('company_profile')->row();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manufacturing_level', $data);
        $this->load->view('b_level/footer');

    }



    function chit($orderd,$order_details){

        $this->load->model('b_level/Manufactur_model');

        $blinds = [];
        $Shades = [];
        if(!empty($order_details)){

            $i = 1;
            $its = '';
            foreach ($order_details as $key => $value) {

                if($value->category_name == 'Blinds'){

                    $attr = json_decode($value->product_attribute);

                    for($sls=1; $sls<=$value->product_qty; $sls++){

                        if($value->product_qty>1){

                        if($sls==1){
                            $its = "A";
                        }
                        if($sls==2){
                            $its = "B";
                        }
                        if($sls==3){
                            $its = "C";
                        }
                        if($sls==4){
                            $its = "D";
                        }
                        if($sls==5){
                            $its = "E";
                        }
                    }

                        $MountData = '';
                        $tile = '';

                        if(!empty($attr)){
                            foreach ($attr as $key => $att) {

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();

                                if($a->attribute_name=="Mount"){
                
                                    $MountData = $this->Manufactur_model->get_name($att,$a->attribute_name);
                                }


                                if($a->attribute_name=='Tilt Type'||$a->attribute_name=='Tilt type'){

                                    $tile = $this->Manufactur_model->get_name($att,$a->attribute_name);

                                }

                                $room =  $value->room;

                               // if($a->attribute_name=='Room'){
                               //      $room =  $att->attribute_value;
                               //  }
                            }
                        }


                        $barcode_img_path = '';

                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $orderd->order_id.' '.$i.@$its;
                        
                        $this->br_code->gcode1($barcode_img_path);
            

                        $blinds[] = '<div class="table col-sm-5 mt-3" style="margin-left: 20px;">
                            <div id="Blinds'.$i.'" style="width:80mm;">
                            <table  class="table table-hover" style="border-left:1px solid #ddd;">
                                
                                <tbody>
                                    <tr>
                                        <td>Order No : '.$orderd->order_id.'</td>
                                        <td>S/M :'.$orderd->side_mark.'</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        '.$value->product_name.'<br/> '.$value->width.' x '.$value->height.'  '.$MountData.'   '.$value->color_name.'</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> Tilt :    '.$tile.'</td>
                                    </tr>
                                    <tr>
                                        <td>'.$value->room.'</td>
                                        <td> Item# '.$i.$its.'</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">'.$this->br_code->gcode1($barcode_img_path).'</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <button type="button" class="btn btn-success" onclick="printContent(\'Blinds'.$i.'\')" style="margin-top: 10px;">Print labels</button>
                        </div>
                        ';

                    }
                }



                if($value->category_name == 'Shades'){

                    $attr = json_decode($value->product_attribute);

                    for($sls=1; $sls<=$value->product_qty; $sls++){

                        if($value->product_qty>1){

                            if($sls==1){
                                $its = "A";
                            }
                            if($sls==2){
                                $its = "B";
                            }
                            if($sls==3){
                                $its = "C";
                            }
                            if($sls==4){
                                $its = "D";
                            }
                            if($sls==5){
                                $its = "E";
                            }
                        }


                        $controlType = '';
                        $controlPosition = '';
                        $controlLength = '';



                        if(!empty($attr)){

                            foreach ($attr as $key => $att) {

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();

                          

                                if($a->attribute_name=='Control Type'||$a->attribute_name=='Control type'){

                                    $controlType = $this->Manufactur_model->get_name($att,$a->attribute_name);
                                }

                                if($a->attribute_name=='Control Position'||$a->attribute_name=='Control position'){

                                    $controlPosition = $this->Manufactur_model->get_name($att,$a->attribute_name);
                                }
                               if($a->attribute_name=='Control Length'||$a->attribute_name=='Control length'){

                                    $controlLength = $this->Manufactur_model->get_name($att,$a->attribute_name);
                                }

                                //$room =  $value->room;

                               // if($a->attribute_name=='Room'){
                               //      $room =  $att->attribute_value;
                               //  }
                            }

                        }


                        $barcode_img_path = '';
                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $orderd->order_id.' '.$i.@$its;
                        
                        $this->br_code->gcode1($barcode_img_path);
            

                        $Shades[] = '<div class="table col-sm-5 mt-3" style="margin-left: 20px; width:80mm;">
                            <div id="Shades'.$i.'" style="width:80mm;">
                            <table class="table table-hover" style="border-left:1px solid #ddd;">
                                
                                <tbody>
                                    <tr>
                                        <td>Order No : '.$orderd->order_id.'</td>
                                        <td>S/M :'.$orderd->side_mark.'</td>
                                    </tr>
                                    <tr>
                                        <td >
                                        '.$value->product_name.' '.$value->color_name.'<br/> size (in)-'.($value->width).' x '.$value->height.'<br>  size (cm)-'.($value->width*2.5).'x'.$value->height*2.5.'   </td>
                                        <td>'.$controlType. ': '.$controlPosition.'-'.$controlLength.'</td>
                                    </tr>
                                    <tr>
                                        <td>'.$value->room.'</td>
                                        <td> Item# '.$i.$its.'</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">'.$this->br_code->gcode1($barcode_img_path).'</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        <button type="button" class="btn btn-success" onclick="printContent(\'Shades'.$i.'\')" style="margin-top: 10px;">Print labels</button>
                        </div>
                        ';

                    }
                }


            }

            $i++;
        }

        return $arrayName = array('shades' =>@$Shades, 'blinds'=>@$blinds );

    }



//    ============== its for manage_manufacturer ===================
    public function manage_manufacturer() {

        $this->permission->check_label('manage_manufacturer')->create()->redirect();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manage_manufacturer');
        $this->load->view('b_level/footer');
        
    }


//    ============== its for manufacturer_edit ===================
    public function manufacturer_edit($id) {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manufacturer_edit');
        $this->load->view('b_level/footer');

    }



}
