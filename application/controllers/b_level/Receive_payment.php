<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Receive_payment extends CI_Controller {

    private $user_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');

       if ($session_id == NULL) {
           redirect('b-level-logout');
       }
      
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Order_model');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Customer_model');
        $this->load->model('b_level/settings');

        $this->load->library('gwapi');

    }


    // public function index($order_id) {

    //     $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
    //     $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

    //     $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
    //                     ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
    //                     ->where('order_id', $order_id)->get('shipment_data')->row();
                        
    //     $data['company_profile'] = $this->settings->company_profile();

    //     $this->load->view('b_level/header');
    //     $this->load->view('b_level/sidebar');
    //     $this->load->view('b_level/orders/order_view', $data);
    //     $this->load->view('b_level/footer');
    // }








    public function tms_payment(){


        $customer_id                    = $this->input->post('customer_id');
        $customer_no                    = $this->input->post('customer_no');

        $payment_method                 = $this->input->post('payment_method');
        $order_id                       = $this->input->post('order_id');
        $paid_ammount                   = $this->input->post('paid_amount');

        $ccnumber                       = str_replace('-','',$this->input->post('card_number'));

        $card_holder_name               = $this->input->post('card_holder_name');
        $ccexp                          = $this->input->post('expiry_month').$this->input->post('expiry_year');
        $cvv                            = $this->input->post('cvv');
        $ip                             = $this->input->ip_address();
       

        $cusstomer_info     = $this->db->where('customer_id',$customer_id)->get('customer_info')->row();

        $orderd             = $this->db->select("*")->from('b_level_quatation_tbl')->where('order_id', $order_id)->get()->row();
        
        $cHead              = $this->db->select('HeadCode')->where('HeadName',$customer_no)->get('b_acc_coa')->row();
        
        $cmp_info           = $this->settings->company_profile();

        $machentinfo        = $this->db->where('id',1)->get('tms_payment_setting')->row();


        #----------------------------
        #   payment gatway
        #----------------------------       

        $this->gwapi->setLogin($machentinfo);
        $this->gwapi->setBilling($cusstomer_info);
        $this->gwapi->setShipping($cusstomer_info);
        $this->gwapi->setOrder($orderd->order_id, $orderdescription='', $tax=0,$Shipping=0,$ip);
        
        //$r = (object)$this->gwapi->doAuth($paid_ammount, $ccnumber, $ccexp, $cvv);
       
        //$r = (object)$this->gwapi->doCredit($paid_ammount, $ccnumber, $ccexp);
    
        $r = (object)$this->gwapi->doSale($paid_ammount, $ccnumber, $ccexp, $cvv);

        #----------------------------

        if(empty($cHead->HeadCode)){

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><p>Customer head not found </div>");
            redirect('b_level/order_controller/order_view/'.$order_id);

        }


        #----------------------------
        #   payment set database
        #----------------------------


        if($r->response=='1'){


                    $due = $this->input->post('due');

                    if($due>0){
                        $order_stage = 3;
                    }else{
                        $order_stage = 2;
                    }


                    $rowData = $this->db->select('due,paid_amount,clevel_order_id')->where('order_id',$order_id)->get('b_level_quatation_tbl')->row();
                    // quatation table update
                    $orderData = array(
                        'paid_amount'                   => @$rowData->paid_amount+$this->input->post('paid_amount'),
                        'due'                           => $this->input->post('due'),
                        'order_stage'                   => $order_stage
                    );

                    //update to quatation table with pyament due 
                    $this->db->where('order_id',$order_id)->update('b_level_quatation_tbl',$orderData);
                    //-----------------------------------------


                    // quatation table update
                    $payment_tbl = array(
                        'quotation_id'                  => $order_id,
                        'payment_method'                => $this->input->post('payment_method'),
                        'paid_amount'                   => $this->input->post('paid_amount'),
                        'payment_date'                  => date('Y-m-d'),
                        'created_by'                    => $cusstomer_info->customer_user_id,
                        'create_date'                   => date('Y-m-d')
                    );


                    $this->db->insert('payment_tbl',$payment_tbl);


                    $voucher_no = $order_id;
                    $Vtype = "INV";
                    $VDate = date('Y-m-d');
                    $paid_amount = $this->input->post('paid_amount');
                    $cAID = $cHead->HeadCode;
                    $IsPosted = 1;
                    $CreateBy = $cusstomer_info->customer_user_id;
                    $createdate = date('Y-m-d H:i:s');

                    $payment_method = $this->input->post('payment_method');

                    $COAID = '1020103';
                    

                    // C level transection start
                    //C level credit insert b_acc_transaction
                    $customerCredit = array(

                        'VNo'           => $voucher_no,
                        'Vtype'         => $Vtype,
                        'VDate'         => $VDate,
                        'Debit'         => 0,
                        'Credit'        => $paid_amount,
                        'COAID'         => $COAID,
                        'Narration'     => "Paid for invoice #".$voucher_no,
                        'IsPosted'      => $IsPosted,
                        'CreateBy'      => $CreateBy,
                        'CreateDate'    => $createdate,
                        'IsAppove'      => 1
                    );

                    $this->db->insert('acc_transaction', $customerCredit);
                    //------------------------------------

                    //B level debit insert b_acc_transaction
                    
                    $bCOA = '502020101';

                    $b_levelDebit = array(

                        'VNo'       => $voucher_no,
                        'Vtype'     => $Vtype,
                        'VDate'     => $VDate,
                        'Debit'     => $paid_amount,
                        'Credit'    => 0,
                        'COAID'     => $bCOA,
                        'Narration' => "Amount received for invoice #".$voucher_no,
                        'IsPosted'  => $IsPosted,
                        'CreateBy'  => $CreateBy,
                        'CreateDate' => $createdate,
                        'IsAppove'  => 1
                    );//b_acc_transaction

                    $this->db->insert('acc_transaction', $b_levelDebit);
                    // C level transection END

                    //. B level transection start
                    //customer credit insert b_acc_transaction
                    $customerCredit = array(

                        'VNo'           => $voucher_no,
                        'Vtype'         => $Vtype,
                        'VDate'         => $VDate,
                        'Debit'         => 0,
                        'Credit'        => $paid_amount,
                        'COAID'         => $cAID,
                        'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                        'IsPosted'      => $IsPosted,
                        'CreateBy'      => $CreateBy,
                        'CreateDate'    => $createdate,
                        'IsAppove'      => 1
                    );

                    $this->db->insert('b_acc_transaction', $customerCredit);
                    //------------------------------------

                    //b_level debit insert b_acc_transaction                    
                    
                    $b_levelDebit = array(

                        'VNo'       => $voucher_no,
                        'Vtype'     => $Vtype,
                        'VDate'     => $VDate,
                        'Debit'     => $paid_amount,
                        'Credit'    => 0,
                        'COAID'     => $COAID,
                        'Narration' => "Amount received for invoice #".$voucher_no,
                        'IsPosted'  => $IsPosted,
                        'CreateBy'  => $CreateBy,
                        'CreateDate' => $createdate,
                        'IsAppove'  => 1
                    );//b_acc_transaction

                    $this->db->insert('b_acc_transaction', $b_levelDebit);
                    //- B level transection END----------------------



                    // C level notification

                    $cNotificationData = array(
                        'notification_text' => 'Payment has been submited for '.$order_id,
                        'go_to_url'         => '#',
                        'created_by'        =>  $cusstomer_info->customer_user_id,
                        'date'              =>  date('Y-m-d')
                    );

                    $this->db->insert('c_notification_tbl',$cNotificationData);

                    //-------------------------
                    //

                    $bNotificationData = array(
                        'notification_text' =>  'Payment has been received for '.$order_id,
                        'go_to_url'         =>  'b_level/invoice_receipt/c_receipt/'.$order_id,
                        'created_by'        =>  $cusstomer_info->customer_user_id,
                        'date'              =>  date('Y-m-d')
                    );
                    $this->db->insert('b_notification_tbl',$bNotificationData);

                    if($order_stage==2){
                        //--------------------------
                        $this->load->model('email_sender');
                        $this->email_sender->send_email(
                            $data = array(
                                'customer_id' => $cusstomer_info->customer_id,
                                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                                'subject'     => 'Order payment'
                            )
                        );
                    }

                    // Send sms
                    $this->Order_model->smsSend( $data = array(
                        'customer_id' => $cusstomer_info->customer_id,
                        'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                        'subject'     => 'Order payment'
                    ));




                    $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                    //redirect('b_level/order_controller/order_view/'.$order_id);
                    redirect('b_level/invoice_receipt/money_receipt/' . $order_id);


        } else{

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><p>".$r->response."</p><p>".$r->responsetext."</p> </div>");
            redirect('b_level/order_controller/order_view/'.$order_id);

        }
        #--------------------------
        
    }






}