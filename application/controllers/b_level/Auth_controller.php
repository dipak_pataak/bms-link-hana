<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->baseurl = $this->config->item('base_url');
        $this->load->model('b_level/Auth_model');
        $this->load->model('b_level/settings');

        $this->load->helper("cookie");
    }

    public function index()
    {

        $data['baseurl'] = $this->baseurl;
        $this->load->view('b_level/b_level_login', $data);
    }

    /*
      |-------------------------------------------------------
      |   B LEVEL USER LOGIN CHECK
      |-------------------------------------------------------
     */

    public function authentication() {
        $password = $this->input->post('password');
        $email = $this->input->post('email');

        $remember_me = $this->input->post("remember");
        if ($remember_me == 1) {
            $email_cookie = array(
                'name' => 'email',
                'value' => $email,
                'expire' => '86500',
            );
            $this->input->set_cookie($email_cookie);
            $pass_cookie = array(
                'name' => 'password',
                'value' => $password,
                'expire' => '86500',
            );
            $this->input->set_cookie($pass_cookie);
//            echo '<pre>';            print_r($email_cookie);            echo '<pre>';            print_r($pass_cookie);die();
        }
//        
        $data['user'] = (object) $userdata = array(
            'email' => $email,
            'password' => $password,
        );

        // CHECK USER
        $user = $this->Auth_model->check_user($userdata);


        if(empty($user->user_id)){

            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Unauthorized user</div>");
            redirect('b-level');

        }

//        echo '<pre>';        print_r($user);die();
        // CHECK PERMISSION 
        $checkPermission = $this->Auth_model->userPermission($user->user_id);
      //  print_r($checkPermission);die();
                   //dd($checkPermission);

        if ($checkPermission != NULL) {

            $permission = array();
            $permission1 = array();

            if (!empty($checkPermission)) {
                foreach ($checkPermission as $value) {

                    $permission[$value->module] = array(
                        'create' => $value->create,
                        'read' => $value->read,
                        'update' => $value->update,
                        'delete' => $value->delete
                    );
//                   dd($permission);
                    $permission1[$value->menu_title] = array(
                        'create' => $value->create,
                        'read' => $value->read,
                        'update' => $value->update,
                        'delete' => $value->delete
                    );
                }
            }
        }// END PERMISSION


        if ($user->user_type === 'b') {





            if ($user->is_admin == 1) {

                $c = $this->db->select('currency')
                                ->from('company_profile')
                                ->where('created_by', $user->user_id)
                                ->get()->row();

                $session_data = array(
                    'isLogIn' => true,
                    'isAdmin' => (($user->is_admin == 1) ? true : false),
                    'user_type' => $user->user_type,
                    'user_id' => $user->user_id,
                    'currency' => $c->currency,
                    'admin_created_by' => $user->created_by,
                    'name' => $user->first_name . " " . $user->last_name,
                    'email' => $user->email,
                    'permission' => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in' => TRUE,
                    'session_id' => session_id(),
                );

                // PUT THE USER DATA IN SESSION
                $this->session->set_userdata($session_data);

                // LAST LOGIN INFO UPDATE THE TABLE
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );

                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);

                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Welcome!" . ' ' . $user->first_name . " " . $user->last_name . "</div>");

                redirect('b-level-dashboard');
            } else if ($user->is_admin == 2 || $user->is_admin == 3) {

                $session_data = array(
                    'isLogIn' => true,
                    'isAdmin' => '', //(($user->is_admin == 2) ? true : false),
                    'user_type' => $user->user_type,
                    'user_id' => $user->user_id,
//                    'user_created_by' => $user->created_by,
                    'admin_created_by' => $user->created_by,
                    'name' => $user->first_name . " " . $user->last_name,
                    'email' => $user->email,
                    'permission' => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in' => TRUE,
                    'session_id' => session_id(),
                );
//                dd($session_data);
                $this->session->set_userdata($session_data);
                //========= its for last login info collect start ============
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );
                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);
                //========= its for last login info collect close============ 

                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Welcome!" . ' ' . $user->first_name . " " . $user->last_name . "</div>");

                redirect('b-level-dashboard');
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Email or password is invalid</div>");
                redirect('b-level');
            }
        } else {

            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> You are not a b-level user</div>");

            redirect('b-level');
        }

        // redirect('b-level-dashboard');
    }

    public function b_level_logout() {

        $last_logout_info = array(
            'logout_datetime' => date("Y-m-d H:i:s"),
        );
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('log_info', $last_logout_info);

        $this->session->sess_destroy();

        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You are not valid user!</div>");
        redirect('b-level');
    }

//    ============ its for b-level-forgot-password-form ==============
    public function b_level_forgot_password_form() {

        $this->load->view('b_level/forgot_password_form');
    }

//    ============ its for b_level_forgot_password_send ==============
    
    public function b_level_forgot_password_send() {  

        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "password forgot";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        $data['get_mail_config'] = $this->settings->get_mail_config();
        //        echo $data['get_mail_config'][0]->protocol;
        //        echo '<pre>';        print_r($data['get_mail_config']); die();
        $email = $this->input->post('email');
        $checkEmail = $this->Auth_model->checkEmail($email);
        //        echo '<pre>';        print_r($checkEmail);        die();
        if (!empty($checkEmail)) {
            $log_id = @$checkEmail->row_id;
            $random_key = ("RK" . date('y') . strtoupper($this->randstrGen(2, 4)));
            $random_array = array(
                'password' => md5($random_key),
            );
            //echo '<pre>';            print_r($random_array);die();
            $this->db->where('row_id', $log_id);
            $this->db->update('log_info', $random_array);
            $this->Auth_model->sendLink($log_id, $email, $data, $random_key);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please check your mail!</div>");
            redirect('b-level');
        } else {
            $this->session->set_flashdata('exception', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Invalid your mail!</div>");
            redirect('b-level-forgot-password-form');
        }
    }



    //    ========= its for randomly key generate ==========
    function randstrGen($mode = null, $len = null) {
        $result = "";
        if ($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4):
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }
}
