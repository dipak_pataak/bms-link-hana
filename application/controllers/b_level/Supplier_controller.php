<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Supplier_model');
        $this->load->model('b_level/User_model');
        $this->load->model('b_level/RowMaterial_model');
    }

    public function index() {
//        $this->load->view('welcome_message');
    }

//     =========== its for add_supplier =============
    public function add_supplier() {
        $this->permission->check_label('suppliers')->create()->redirect();

        $data['get_raw_material'] = $this->RowMaterial_model->get_raw_material();

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/suppliers/suppliers_add');
        $this->load->view('b_level/footer');
    }

//======== its for exists get_check_supplier_unique_email  ===========
    public function get_check_supplier_unique_email() {
        $email = $this->input->post('email');
        $query = $this->db->select('*')->from('supplier_tbl')->where('email', $email)
                        ->get()->row();
//        echo '<pre>';        print_r($query);die();
        if (!empty($query)) {
            echo $query->email;
        } else {
            echo 0;
        }
    }

//    ============= its for supplier_save =================
    public function supplier_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "supplier information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $created_date = date('Y-m-d');
        $supplier_sku = $this->input->post('supplier_sku');
        $supplier_name = $this->input->post('supplier_name');
        $company_name = $this->input->post('company_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $raw_material_id = $this->input->post('raw_material_id');
        $previous_balance = $this->input->post('previous_balance');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
//        $price_item = $this->input->post('price_item');
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        ============ its for accounts coa table ===============
        $coa = $this->Supplier_model->headcode();
        if ($coa->HeadCode != NULL) {
            $headcode = $coa->HeadCode + 1;
        } else {
            $headcode = "502020101";
        }
        $lastid = $this->db->select("*")->from('supplier_tbl')
                ->order_by('supplier_no', 'desc')
                ->get()
                ->row();
        $sl = $lastid->supplier_no;
        if (empty($sl)) {
            $sl = "SUP-0001";
        } else {
            $sl = $sl;
        }
        $supno = explode('-', $sl);
        $nextno = $supno[1] + 1;
        $si_length = strlen((int) $nextno);

        $str = '0000';
        $cutstr = substr($str, $si_length);
        $sino = "SUP" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;
//        $customer_name = $this->input->post('customer_name');
        $supplier_no = $sino . '-' . $supplier_name;
//        dd($supplier_no);
//        ================= close =======================

        $supplier_data = array(
            'supplier_sku' => $supplier_sku,
            'supplier_no' => $supplier_no,
            'supplier_name' => $supplier_name,
            'company_name' => $company_name,
            'email' => $email,
            'phone' => $phone,
//            'material_product' => $material_product,
            'previous_balance' => $previous_balance,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip' => $zip,
            'country_code' => $country_code,
//            'price_item' => $price_item,
            'created_by' => $this->user_id,
            'created_date' => $created_date,
        );
//        dd($supplier_data);
        $this->db->insert('supplier_tbl', $supplier_data);
        $supplier_id = $this->db->insert_id();
        //            =========== its for supplier_raw_material_mapping ==============
        for ($i = 0; $i < count($raw_material_id); $i++) {
            $supplier_raw_material_mapping = array(
                'raw_material_id' => $raw_material_id[$i],
                'supplier_id' => $supplier_id,
            );
            $this->db->insert('supplier_raw_material_mapping', $supplier_raw_material_mapping);
        }
//            =========== close product condition mapping ============
//        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadCode' => $headcode,
            'HeadName' => $supplier_no,
            'PHeadName' => 'Supplier',
            'HeadLevel' => '4',
            'IsActive' => '1',
            'IsTransaction' => '1',
            'IsGL' => '0',
            'HeadType' => 'L',
            'IsBudget' => '0',
            'IsDepreciation' => '0',
            'DepreciationRate' => '0',
            'CreateBy' => $this->user_id,
            'CreateDate' => $created_date,
        );
//        dd($customer_coa);
        $this->db->insert('b_acc_coa', $customer_coa);
//        ======= close ==============
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Supplier info save successfully!</div>");
        redirect('supplier-list');
    }

//     =========== its for supplier_list =============
    public function supplier_list() {

        $this->permission->check_label('suppliers')->create()->redirect();
        $data['get_raw_material'] = $this->RowMaterial_model->get_raw_material();

        $total_row = $this->db->where('created_by',$this->level_id)->count_all('supplier_tbl');

        //        echo $this->uri->segment(2);die();
        $config["base_url"] = base_url('b_level/Supplier_controller/supplier_list');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
//        dd($page);
        $data["supplier_list"] = $this->Supplier_model->supplier_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/suppliers/suppliers_list', $data);
        $this->load->view('b_level/footer');
    }

//     =========== its for supplier_invoice =============
    public function supplier_invoice($supplier_id) {
        $data['get_supplier_invoice'] = $this->Supplier_model->get_purchase_list($supplier_id);

        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/suppliers/supplier_invoice');
        $this->load->view('b_level/footer');
    }

//     =========== its for supplier_edit =============
    public function supplier_edit($id) {
        $data['supplier_edit'] = $this->Supplier_model->supplier_edit($id);
        $data['get_raw_material'] = $this->RowMaterial_model->get_raw_material();
        $data['supplier_wise_material'] = $this->Supplier_model->supplier_wise_material($id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/suppliers/supplier_edit', $data);
        $this->load->view('b_level/footer');
    }

//    ============= its for supplier_update ================ 
    public function supplier_update($id) {
        $get_supplier_info = $this->db->select('supplier_no')->where('supplier_id', $id)->get('supplier_tbl')->result();
        $get_supplier_no = $get_supplier_info[0]->supplier_no;
        $get_supplier_coa_info = $this->db->select('HeadCode')->where('HeadName', $get_supplier_no)->get('b_acc_coa')->result();
        $get_supplier_headCode = $get_supplier_coa_info[0]->HeadCode;

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Color information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $created_date = date('Y-m-d');
        $supplier_sku = $this->input->post('supplier_sku');
        $supplier_name = $this->input->post('supplier_name');
        $supp_no = $this->input->post('supplier_no');
        $supplier_no = $supp_no . "-" . $supplier_name;
        $company_name = $this->input->post('company_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $raw_material_id = $this->input->post('raw_material_id');
        $previous_balance = $this->input->post('previous_balance');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
//        $price_item = $this->input->post('price_item');
//        dd($supp_no);
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $supplier_data = array(
            'supplier_sku' => $supplier_sku,
            'supplier_no' => $supplier_no,
            'supplier_name' => $supplier_name,
            'company_name' => $company_name,
            'email' => $email,
            'phone' => $phone,
//            'material_product' => $material_product,
            'previous_balance' => $previous_balance,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip' => $zip,
            'country_code' => $country_code,
//            'price_item' => $price_item,
            'updated_by' => $this->user_id,
            'updated_date' => $created_date,
        );
//        dd($supplier_data);
        $this->db->where('supplier_id', $id);
        $this->db->update('supplier_tbl', $supplier_data);

        //            =========== its for supplier_raw_material_mapping ==============
        $this->db->where('supplier_id', $id);
        $this->db->delete('supplier_raw_material_mapping');
        for ($i = 0; $i < count($raw_material_id); $i++) {
            $supplier_raw_material_mapping = array(
                'raw_material_id' => $raw_material_id[$i],
                'supplier_id' => $id,
            );
            $this->db->insert('supplier_raw_material_mapping', $supplier_raw_material_mapping);
        }
//            =========== close product condition mapping ============
//        ======== its for customer COA data array ============
        $supplier_coa = array(
            'HeadName' => $supplier_no,
            'UpdateBy' => $this->user_id,
            'UpdateDate' => $created_date,
        );
//        dd($customer_coa);
        $this->db->where('HeadCode', $get_supplier_headCode);
        $this->db->update('b_acc_coa', $supplier_coa);
//        ======= close ==============

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Supplier info updated successfully!</div>");
        redirect('supplier-list');
    }

//    ============= its for supplier_filter =============
    public function supplier_filter() {
        $data['sku'] = $this->input->post('sku');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data["get_supplier_filter"] = $this->Supplier_model->get_supplier_filter($data['sku'], $data['name'], $data['email'], $data['phone']);


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/suppliers/supplier_filter');
        $this->load->view('b_level/footer');
    }

//    ============ its for supplier_delete ==============
    public function supplier_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "supplier information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('supplier_id', $id);
        $this->db->delete('supplier_tbl');
        $this->db->where('supplier_id', $id);
        $this->db->delete('supplier_raw_material_mapping');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Supplier deleted successfully!</div>");
        redirect('supplier-list');
    }

//    ========= its for supplier onkeyup search ============
    public function b_level_supplier_search() {
        $keyword = $this->input->post('keyword');
        $data["supplier_list"] = $this->Supplier_model->supplier_search($keyword);
        $this->load->view('b_level/suppliers/supplier_search', $data);
    }
/** Start added by insys */
//=========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('supplier_tbl','supplier_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Supplier has been Deleted successfully.</div>");
        }
        redirect("supplier-list");
    } 
/** End added by insys */
}
