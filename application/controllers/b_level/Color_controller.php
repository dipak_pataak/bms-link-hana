<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Color_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');


        $this->load->model(array(
            'b_level/pattern_model', 'b_level/Settings'
        ));
    }

    public function index() {
        
    }

    public function add_color() {
        $this->permission->check_label('color')->create()->redirect();
//        $data['colors'] = $this->db->from('color_tbl')->order_by('id', 'desc')->get()->result();
        $config["base_url"] = base_url('b_level/Color_controller/add_color');

        $this->db->where('created_by',$this->level_id);
        $this->db->from("color_tbl");
        $total_rolws = $this->db->count_all_results();

        $config["total_rows"] = $total_rolws;
//        $config["total_rows"] = $this->own_user_customer_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
//        $data["colors"] = $this->Color_model->color_list($config["per_page"], $page);
        $data["colors"] = $this->db->from('color_tbl')
                ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = color_tbl.pattern_id', 'LEFT')
                ->order_by('color_tbl.id', 'desc')->limit($config["per_page"], $page)
                ->where('color_tbl.created_by',$this->level_id)
                ->get()
                ->result();
        $data["links"] = $this->pagination->create_links();

       $patterns =  $this->db->where('created_by',$this->level_id)
            ->get('pattern_model_tbl')->result();


        $data["patterns"] = $patterns;
        $data['pagenum'] = $page;


        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/color/add_color', $data);
        $this->load->view('b_level/footer');
    }

    //======== its for exists get_color check ===========
    public function get_color_check($color) {
//        echo $mobile_phone; die();
        $query = $this->db->where('color_name', $color)->or_where('color_number', $color)
                ->get('color_tbl')
                ->row();
        if (!empty($query)) {
            echo $query->color_name;
        } else {
            echo 0;
        }
    }

    public function save_color() {
        $colorData = array(
            'pattern_id' => $this->input->post('pattern_id'),
            'color_name' => $this->input->post('color_name'),
            'color_number' => $this->input->post('color_number'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d')
        );
        $this->db->insert('color_tbl', $colorData);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Color information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color save successfull </div>");
        redirect('color');
    }

    public function delete_color($id) {
        $this->db->select('colors');
        $this->db->from('product_tbl  a');
        $this->db->where("FIND_IN_SET($id, colors)");
        $check_colors = $this->db->get();
//        echo '<pre>';        print_r($check_colors);die();
        if ($check_colors->num_rows() > 0) {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Can't delete it. Color already assigned!  </div>");
            redirect('color');
        } else {
            $this->db->where('id', $id)->delete('color_tbl');
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "deleted";
            $remarks = "Color information deleted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
            $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color deleted successfully! </div>");
            redirect('color');
        }
    }

    public function get_color($id) {
        $result = $this->db->where('id', $id)->get('color_tbl')->row();
        echo json_encode($result);
    }

    public function update_color() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "Color information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $colorData = array(
            'pattern_id' => $this->input->post('pattern_id'),
            'color_name' => $this->input->post('color_name'),
            'color_number' => $this->input->post('color_number'),
            'updated_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d')
        );

        $id = $this->input->post('id');

        $this->db->where('id', $id)->update('color_tbl', $colorData);

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color update successfull </div>");
        redirect('color');
    }

//    =========== its for import_color_save ===========
    public function import_color_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "color csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
//                    $insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['color_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['color_number'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['pattern_id'] = (!empty($csv_line[2]) ? $csv_line[2] : 0);
                }
                $data = array(
                    'pattern_id' => $insert_csv['pattern_id'],
                    'color_name' => $insert_csv['color_name'],
                    'color_number' => $insert_csv['color_number'],
                    'created_by' => $this->user_id,
                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
//                echo $data['email'];
                    $result = $this->db->select('*')
                            ->from('color_tbl')
                            ->where('color_name', $data['color_name'])
//                            ->or_where('color_number', $data['color_number'])
                            ->get()
                            ->num_rows();
//                echo '<pre>';                print_r($result);die();
                    if ($result == 0 && !empty($data['color_name'])) {
                        $this->db->insert('color_tbl', $data);
                    } else {
                        $data = array(
//                            'customer_id' => $insert_csv['customer_id'],
                            'color_name' => $insert_csv['color_name'],
                            'color_number' => $insert_csv['color_number'],
                            'updated_by' => $this->user_id,
                            'updated_date' => date('Y-m-d'),
                        );
                        $this->db->where('color_name', $data['color_name']);
                        $this->db->update('color_tbl', $data);
//                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Already exists!</div>");
//                        redirect('customer-import');
                    }
                }
                $count++;
            }
        }
//        echo "DUke Nai";die();
        fclose($fp) or die("can't close file");
//        $this->session->set_userdata(array('message' => display('successfully_added')));
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Color imported successfully!</div>");
        redirect('color');
    }

//    ========== its for color_filter ==================
    public function color_filter() {
        $data['colorname'] = $this->input->post('colorname');
        $data['colornumber'] = $this->input->post('colornumber');
        $data['get_color_filter'] = $this->Settings->color_filter($data['colorname'], $data['colornumber']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/color/color_filter', $data);
        $this->load->view('b_level/footer');
    }

//    =============== b_level_pattern_search ============
    public function b_level_color_search() {
        $keyword = $this->input->post('keyword');
        $data['colors'] = $this->db->select('*')->from('color_tbl')->like('color_name', $keyword, 'both')
                        ->or_like('color_number', $keyword, 'both')->order_by('id', 'desc')->limit(50)->get()->result();
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/color/color_search', $data);
    }
/** Start added by insys */
//    =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('color_tbl','id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Color has been Deleted successfully.</div>");
        }
        redirect("color");
    } 
/** End added by insys */
}
