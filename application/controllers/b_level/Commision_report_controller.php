<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Commision_report_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('b-level-logout');
        }
    }


    public function b_commission_report() {
       
        $config["base_url"] = base_url('b_level/Commision_report_controller/b_commission_report');
        $config["total_rows"] = count($this->b_level_commission_list());
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["commission_list"] = $this->b_level_commission_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/commission_report/b_commission_report',$data);
        $this->load->view('b_level/footer');
    }

    public function search_b($All='')
    {       
        if($this->input->post('Search'))
        {
            $this->session->set_userdata('search_employee_name',$this->input->post('search_employee_name'));
             $this->session->set_userdata('search_daterange',$this->input->post('search_daterange'));
        }
        if($this->input->post('All') || $All=='All')
        {
            $this->session->unset_userdata('search_employee_name');
            $this->session->unset_userdata('search_daterange');
        }
        redirect('b_level/Commision_report_controller/b_commission_report');
    }

    public function c_commission_report() {

        $config["base_url"] = base_url('b_level/Commision_report_controller/c_commission_report');
        $config["total_rows"] = count($this->b_level_commission_list());
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["commission_list"] = $this->c_level_commission_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/commission_report/c_commission_report',$data);
        $this->load->view('b_level/footer');
    }

    public function search_c($All='')
    {       
        if($this->input->post('Search'))
        {
            $this->session->set_userdata('search_employee_name',$this->input->post('search_employee_name'));
             $this->session->set_userdata('search_daterange',$this->input->post('search_daterange'));
        }
        if($this->input->post('All') || $All=='All')
        {
            $this->session->unset_userdata('search_employee_name');
            $this->session->unset_userdata('search_daterange');
        }
        redirect('b_level/Commision_report_controller/c_commission_report');
    }


    public function b_level_commission_list($offset = null, $limit = null) {
        
        $employee_name = $this->session->userdata('search_employee_name');
        $daterange_date = $this->session->userdata('search_daterange');


        if($employee_name != ''){
            $this->db->having("full_name LIKE '%".$employee_name."%' ");
        }
        if($daterange_date != ''){
            $range_date = explode(" - ", $daterange_date);
            $date1 = $range_date[0];
            $date2 = $range_date[1];

            $date11 = explode("/", $date1);
            $date22 = explode("/", $date2);

            if(count($date11) > 2 && count($date22) > 2){
                $from_date = $date11[2]."-".$date11[0]."-".$date11[1];
                $to_date = $date22[2]."-".$date22[0]."-".$date22[1];

                $this->db->where("q.created_date >=",$from_date);
                $this->db->where("q.created_date <=",$to_date);
            }
        }

        $this->db->select("SUM(q.commission_amt) as total_commission,q.created_date, CONCAT_WS(' ', u.first_name, u.last_name) as full_name");
        $this->db->from('b_level_quatation_tbl q');
        $this->db->join('user_info u','q.created_by = u.id','FULL');
        $this->db->group_by('q.created_by');
        $this->db->order_by('q.created_date', 'desc');
        $this->db->limit($offset, $limit);
        $query = $this->db->get()->result();                           
        return $query;
    }

    public function c_level_commission_list($offset = null, $limit = null) {
        
        $employee_name = $this->session->userdata('search_employee_name');
        $daterange_date = $this->session->userdata('search_daterange');


        if($employee_name != ''){
            $this->db->having("full_name LIKE '%".$employee_name."%' ");
        }
        if($daterange_date != ''){
            $range_date = explode(" - ", $daterange_date);
            $date1 = $range_date[0];
            $date2 = $range_date[1];

            $date11 = explode("/", $date1);
            $date22 = explode("/", $date2);

            if(count($date11) > 2 && count($date22) > 2){
                $from_date = $date11[2]."-".$date11[0]."-".$date11[1];
                $to_date = $date22[2]."-".$date22[0]."-".$date22[1];

                $this->db->where("q.created_date >=",$from_date);
                $this->db->where("q.created_date <=",$to_date);
            }
        }

        $this->db->select("SUM(q.commission_amt) as total_commission,q.created_date, CONCAT_WS(' ', u.first_name, u.last_name) as full_name");
        $this->db->from('quatation_tbl q');
        $this->db->join('user_info u','q.created_by = u.id','FULL');
        $this->db->group_by('q.created_by');
        $this->db->order_by('q.created_date', 'desc');
        $this->db->limit($offset, $limit);
        $query = $this->db->get()->result();                           
        return $query;
    }
}    