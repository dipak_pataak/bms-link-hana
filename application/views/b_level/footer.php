
</div>
<!-- end content -->  
<!-- footer -->
<div id="footer">
    <p>Copyright &copy; 2019 BMS Link. All Rights Reserved.</p>
</div>
<!-- end footert -->


<!-- scripts (jquery) -->
<!--[if IE]>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/b_level/resources/scripts/excanvas.min.js"></script>
<![endif]-->
<!-- <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-ui-1.8.16.custom.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/tiny_mce/jquery.tinymce.js"></script>
<!-- scripts (custom) -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.menu.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.chart.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.table.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.form.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.dialog.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.autocomplete.js"></script>-->
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/select2.full.js"></script>
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/bootstrap-datepicker.js"></script>
<!--========= time picker ==========-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
<script src="<?php echo base_url() ?>assets/b_level/resources//js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/b_level/resources/js/daterangepicker.min.js"></script>

<!--=========== its for add customer phone format .js ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/phoneformat.js"></script>
<!--=========== its for add customer phone format .js close ================-->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/b_level/toster/toastr.min.js"></script>

<!--=========== its for multi select ============-->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/bootstrap-select.min.js"></script>

<script src="<?php echo base_url(); ?>assets/c_level/plugins/theia-sticky-sidebar-master/dist/theia-sticky-sidebar.min.js"></script>

<script type="text/javascript">
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });
    //    =========== its for field_reset =========
    function field_reset() {
        $(".invoice_no").val('');
        $(".cat_name").val('');
        $(".pattern_status").val('');
        $(".parent_cat").val('');
        $(".category_name").val('');
        $(".parent_category").val('');
        $(".product_name").val('');
        $(".pattern_model").val('');
        $(".pattern_name").val('');
        $(".price_sheet_name").val('');
        $(".colorname").val('');
        $(".colornumber").val('');
        $(".company_name").val('');
        $(".phone").val('');
        $(".email").val('');
        $(".address").val('');
        $(".sku").val('');
        $(".name").val('');
        $(".supplier_name").val('');
        $(".category_name").val('');
        $(".attribute_name").val('');
        $('.select2').select2();
    }
    $(document).ready(function () {
//        ============ its for autocomplete off ===========
        $("form :input").attr("autocomplete", "off");
//        ============ close ===============
//        ================ Date picker =============
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            showOn: "focus",
        });
//        =========== close datepicker =============
        style_path = "assets/b_level/resources/css/colors";
//                =========== its for sidebar menu active and disabled ====================
        var uri_segment = "<?php echo $this->uri->segment(1) ?>";
        var uri_segment2 = "<?php echo $this->uri->segment(2) ?>";
        var uri_segment3 = "<?php echo $this->uri->segment(3) ?>";


        if (uri_segment == 'b-level-dashboard') {
            $(".dashboard_slt").addClass("selected");
        } else if (uri_segment == 'add-b-customer' || uri_segment == 'b-customer-list' || uri_segment == 'b-customer-edit' || uri_segment == 'top-search-customer-order-info' || uri_segment == 'b-customer-view' || uri_segment == 'customer-import' || uri_segment == 'b-level-customer-filter' ) {
//            $(".selected").removeClass("selected");
            $("#h-menu-customer").addClass("selected");
            $("#menu-customer").addClass("opened");
            $("#menu-customer").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
//           if ($("#" + uri_segment3).has('ul')) {
//               var ttt = $("#")
//           }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");

        } else if (uri_segment == 'new-order' || uri_segment == 'b-order-list' || uri_segment == 'customer-order-return' || uri_segment == 'order-kanban' || ( uri_segment3 == 'receipt' && uri_segment2 != 'Shared_catalog_controller' ) || ( uri_segment3 == 'order_view' && uri_segment2 != 'Shared_catalog_controller' ) || uri_segment == 'order-view' || uri_segment3 == 'c_receipt' || uri_segment3 == 'shipment' || uri_segment == 'order-single-product-edit' || uri_segment == 'order-customer-info-edit') {
            $("#h-menu-order").addClass("selected");
            $("#menu-order").addClass("opened");
            $("#menu-order").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");

//            ================its for static menu such as edit, view etc ==============
            if (uri_segment3 == 'receipt' || uri_segment == 'order-single-product-edit' || uri_segment3 == 'c_receipt') {
                $("#7").addClass("collapsible minus");
                $("#7").removeClass("collapsible plus");
                $("#manage_order").addClass("expanded");
                $("#manage_order").removeClass("collapsed");
            } else if (uri_segment3 == 'order_view' || uri_segment == 'order-view' || uri_segment == 'customer-order-return' || uri_segment == 'order-customer-info-edit') {
                $("#7").addClass("collapsible minus");
                $("#7").removeClass("collapsible plus");
                $("#manage_order").addClass("expanded");
                $("#manage_order").removeClass("collapsed");
            } else if (uri_segment3 == 'shipment') {
                $("#7").addClass("collapsible minus");
                $("#7").removeClass("collapsible plus");
                $("#manage_order").addClass("expanded");
                $("#manage_order").removeClass("collapsed");
            }


//            =========== close static menu ===============

        } else if (uri_segment == 'add-category' || uri_segment == 'manage-category' || uri_segment == 'category-edit' || uri_segment == 'category-assign' || uri_segment == 'category-filter' || uri_segment == 'add-product' || uri_segment == 'product-manage' || uri_segment == 'product-edit' || uri_segment2 == 'product_controller' || uri_segment == 'product-filter' || uri_segment3 == 'maping_product_attribute' || uri_segment == 'add-pattern' || uri_segment == 'manage-pattern' || uri_segment == 'pattern-edit' || uri_segment == 'pattern-model-filter' || uri_segment == 'add-attribute' || uri_segment == 'manage-attribute' || uri_segment == 'attribute-filter' || uri_segment == 'attribute-edit' || uri_segment == 'attribute-type' || uri_segment == 'attribute-filter' || uri_segment == 'add-condition' || uri_segment == 'condition-edit' || uri_segment == 'condition-filter' || uri_segment == 'add-tax' || uri_segment == 'shipping-add' || uri_segment == 'add-price' || uri_segment == 'manage-price' || uri_segment == 'add-group-price' || uri_segment == 'manage-group-price' || uri_segment3 == 'edit_style' || uri_segment3 == 'edit_group_price_style' || uri_segment == 'price-manage-filter' || uri_segment == 'add-group' || uri_segment == 'group-manage' || uri_segment == 'edit-group' || uri_segment == 'add-row-material' || uri_segment == 'row-material-list' || uri_segment == 'edit-row-material' || uri_segment == 'color' || uri_segment == 'b-level-color-filter' || uri_segment == 'group-manage-filter') {
            $("#h-menu-catalog").addClass("selected");
            $("#menu-catalog").addClass("opened");
            $("#menu-catalog").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");
            //            ================its for static menu such as edit, view etc ==============
            if (uri_segment == 'product-edit' || uri_segment == 'product-filter' || uri_segment3 == 'maping_product_attribute' || uri_segment2 == 'product_controller') {
                $("#15").addClass("collapsible minus");
                $("#15").removeClass("collapsible plus");
                $("#products").addClass("expanded");
                $("#products").removeClass("collapsed");
            } else if (uri_segment == 'attribute-edit' || uri_segment == 'attribute-filter') {
                $("#21").addClass("collapsible minus");
                $("#21").removeClass("collapsible plus");
                $("#attributes").addClass("expanded");
                $("#attributes").removeClass("collapsed");
            } else if (uri_segment == 'condition-edit') {
                $("#24").addClass("collapsible minus");
                $("#24").removeClass("collapsible plus");
                $("#condition").addClass("expanded");
                $("#condition").removeClass("collapsed");
            } else if (uri_segment3 == 'edit_style') {
                $("#28").addClass("collapsible minus");
                $("#28").removeClass("collapsible plus");
                $("#price_model").addClass("expanded");
                $("#price_model").removeClass("collapsed");
                $("#29").addClass("collapsible minus");
                $("#29").removeClass("collapsible plus");
                $("#row-Column_price").addClass("expanded");
                $("#row-Column_price").removeClass("collapsed");
            } else if (uri_segment3 == 'edit_group_price_style' || uri_segment == 'group-manage-filter') {
                $("#28").addClass("collapsible minus");
                $("#28").removeClass("collapsible plus");
                $("#price_model").addClass("expanded");
                $("#price_model").removeClass("collapsed");
                $("#132").addClass("collapsible minus");
                $("#132").removeClass("collapsible plus");
                $("#group_price").addClass("expanded");
                $("#group_price").removeClass("collapsed");
            } else if (uri_segment == 'price-manage-filter') {
//                $("#29").addClass("expanded minus");
//                $("#29").removeClass("expanded plus");
                $("#price_model").addClass("expanded");
                $("#price_model").removeClass("collapsed");
//                $("#132").addClass("collapsible minus");
//                $("#132").removeClass("collapsible plus");
//                $("#group_price").addClass("expanded");
//                $("#group_price").removeClass("collapsed");
            } else if (uri_segment == 'edit-row-material') {
                $("#79").addClass("collapsible minus");
                $("#79").removeClass("collapsible plus");
                $("#raw_materials").addClass("expanded");
                $("#raw_materials").removeClass("collapsed");
            } else if (uri_segment == 'category-filter') {
                $("#11").addClass("collapsible minus");
                $("#11").removeClass("collapsible plus");
                $("#Category").addClass("expanded");
                $("#Category").removeClass("collapsed");
            }
            if (uri_segment3 == 'group_price_mapping' || uri_segment3 == 'sqm_price_mapping') {

                $("#price_model").addClass("expanded");
                $("#price_model").removeClass("collapsed");

            }

//            =========== close static menu ===============


        } else if (uri_segment == 'add-manufacturer' || uri_segment == 'manage-manufacturer' || uri_segment == 'manufacturer-edit' || uri_segment == 'stock-availability' || uri_segment == 'stock-history' || uri_segment == 'add-supplier' || uri_segment == 'supplier-list' || uri_segment == 'supplier-edit' || uri_segment == 'supplier-invoice' || uri_segment == 'supplier-filter' || uri_segment == 'customer-return' || uri_segment == 'supplier-return' || uri_segment == 'raw-material-supplier-return' || uri_segment == 'b-supplier-return-filter' || uri_segment == 'b-customer-return' || uri_segment2 == 'Manufacturer_controller' || uri_segment == 'raw-material-usage') {
            $("#h-menu-production").addClass("selected");
            $("#menu-production").addClass("opened");
            $("#menu-production").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");
            //            ================its for static menu such as edit, view etc ==============
            if (uri_segment == 'supplier-edit') {
                $("#42").addClass("collapsible minus");
                $("#42").removeClass("collapsible plus");
                $("#suppliers").addClass("expanded");
                $("#suppliers").removeClass("collapsed");
            } else if (uri_segment == 'supplier-invoice') {
                $("#42").addClass("collapsible minus");
                $("#42").removeClass("collapsible plus");
                $("#suppliers").addClass("expanded");
                $("#suppliers").removeClass("collapsed");
            } else if (uri_segment2 == 'Manufacturer_controller') {
                $("#36").addClass("collapsible minus");
                $("#36").removeClass("collapsible plus");
                $("#manufacturer").addClass("expanded");
                $("#manufacturer").removeClass("collapsed");
            }
//            =========== close static menu ===============

        } else if (uri_segment == 'b-account-chart' || uri_segment == 'purchase-entry' || uri_segment == 'purchase-list' || uri_segment == 'view-purchase' || uri_segment == 'raw-material-return-purchase' || uri_segment == 'b-debit-voucher' || uri_segment == 'b-credit-voucher' || uri_segment == 'b-journal-voucher' || uri_segment == 'b-contra-voucher' || uri_segment == 'b-voucher-approval' || uri_segment == 'b-voucher-reports' || uri_segment == 'b-bank-book' || uri_segment == 'b-cash-book' || uri_segment == 'b-cash-flow' || uri_segment == 'b-general-ledger' || uri_segment == 'b-profit-loss' || uri_segment == 'b-trial-ballance' || uri_segment == 'b-voucher-edit' || uri_segment == 'b-cash-flow-report-search' || uri_segment == 'b-accounts-report-search' || uri_segment == 'b-profit-loss-report-search' || uri_segment == 'b-trial-balance-report' || uri_segment == 'b-vouchar-cash') {
            $("#h-menu-account").addClass("selected");
            $("#menu-account").addClass("opened");
            $("#menu-account").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");
            //            ================its for static menu such as edit, view etc ==============
            if (uri_segment == 'view-purchase' || uri_segment == 'raw-material-return-purchase') {
                $("#53").addClass("collapsible minus");
                $("#53").removeClass("collapsible plus");
                $("#purchase_list").addClass("expanded");
                $("#purchase_list").removeClass("collapsed");
            }
//            =========== close static menu ===============

        } else if (uri_segment == 'profile-setting' || uri_segment == 'shipping' || uri_segment == 'shipping-edit' || uri_segment == 'mail' || uri_segment == 'sms' || uri_segment == 'sms-configure' || uri_segment == 'sms-config-edit' || uri_segment == 'gateway' || uri_segment == 'gateway-edit' || uri_segment == 'b-level-users' || uri_segment == 'new-user' || uri_segment == 'create-user' || uri_segment == 'user-filter' || uri_segment2 == 'setting_controller' || uri_segment == 'uom-list' || uri_segment3 == 'uom_edit' || uri_segment == 'b-level-users' || uri_segment2 == 'role_controller' || uri_segment2 == 'menusetup_controller' || uri_segment == 'us-state' || uri_segment == 'us-state-edit' || uri_segment == 'b-iframe-code' || uri_segment == 'b-user-access-role-list' || uri_segment == 'b-user-role-assign' || uri_segment == 'b-role-list' || uri_segment == 'b-role-permission' || uri_segment == 'b-menu-setup') {
            $("#h-menu-setting").addClass("selected");
            $("#menu-setting").addClass("opened");
            $("#menu-setting").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");
            if (uri_segment == 'shipping-edit' || uri_segment == 'gateway-edit' || uri_segment == 'sms-config-edit') {
                $("#70").addClass("collapsible minus");
                $("#70").removeClass("collapsible plus");
                $("#integration").addClass("expanded");
                $("#integration").removeClass("collapsed");
            } else if (uri_segment3 == 'edit_user' || uri_segment3 == 'edit_user_access_role' || uri_segment3 == 'role_permission' || uri_segment3 == 'role_edit') {
                $("#70").addClass("collapsible minus");
                $("#70").removeClass("collapsible plus");
                $("#Employees_And_Permissions").addClass("expanded");
                $("#Employees_And_Permissions").removeClass("collapsed");
            }
        } else if (uri_segment == 'gallery_tag' || uri_segment == 'add_b_gallery_image' || uri_segment == 'manage_b_gallery_image' || uri_segment3 == 'view_image' || uri_segment3 == 'get_image') {
            $("#h-menu-Gallery").addClass("selected");
            $("#menu-Gallery").addClass("opened");
            $("#menu-Gallery").removeClass("closed");
            var test = $("#" + uri_segment).closest('ul').attr("id");
            var test2 = $("#" + test).closest('li').attr("id");
            $("#" + test2 + ' a#' + test2).addClass("collapsible minus");
            if ($("#" + uri_segment).has('ul')) {
                var test3 = $("#" + uri_segment).closest('ul').attr('id');
                var test4 = $("#" + test3).closest('ul li ul li').attr("id");
                var test5 = $("#" + test4).parent('ul').attr("id");
                var test6 = $("#" + test5).closest('li').attr("id");
                $("#" + test5).addClass("expanded");
                $("#" + test5).removeClass("collapsed");
                $("#" + test6 + ' a#' + test6).addClass("collapsible minus");
            }
            $("#" + test).addClass("expanded");
            $("#" + test).removeClass("collapsed");

//            ================its for static menu such as edit, view etc ==============
            if (uri_segment3 == 'view_image' || uri_segment3 == 'get_image') {
                $("#7").addClass("collapsible minus");
                $("#7").removeClass("collapsible plus");
                $("#image").addClass("expanded");
                $("#image").removeClass("collapsed");
            } 
//            =========== close static menu ===============
        }else if (uri_segment == 'commision_report' || uri_segment3 == 'b_commission_report' || uri_segment3 == 'c_commission_report') {

            $("#h-menu-setting").addClass("selected");
            $("#menu-setting").addClass("opened");
            $("#menu-setting").removeClass("closed");

            if (uri_segment3 == 'b_commission_report' || uri_segment3 == 'c_commission_report') {
                $("#141").children('a').addClass("collapsible minus");
                $("#commision_report").addClass("expanded");
                $("#commision_report").removeClass("collapsed");
            } 
        }

        else if (uri_segment == 'catalog-user' || uri_segment == 'catalog-request' || uri_segment == 'catalog-order-list' || uri_segment2 == 'Shared_catalog_controller' || uri_segment == 'manufacturing-catalog-order-list'|| uri_segment == 'catalog-b-user-order-list') {

            $("#h-menu-shared_catalog").addClass("selected");
            $("#menu-shared_catalog").addClass("opened");
            $("#menu-shared_catalog").removeClass("closed");


        }



    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }


    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>





<script type="text/javascript">

    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: '', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }

    jQuery('.lang-select').click(function () {
        var theLang = jQuery(this).attr('data-lang');
        //alert(theLang);
        jQuery('.goog-te-combo').val(theLang);

        //alert(jQuery(this).attr('href'));
        window.location = jQuery(this).attr('href');
        location.reload();

    });

    // For Multipe delete Insys: START
    $("#SellectAll").change(function () {
        $(".checkbox_list").prop('checked', $(this).prop("checked"));
    });

    $(".checkbox_list").click(function () {

        $("#SellectAll").prop('checked',true);

        $(".checkbox_list").each(function () {
            if (!$(this).is(":checked")) {
                $("#SellectAll").prop('checked',false);
                return;
            }
        });
    });

    function action_delete(frm)
    {
        with(frm)
        {
            var flag = false;
            str = '';
            field = document.getElementsByName('Id_List[]');
            for (i = 0; i < field.length; i++)
            {
                if(field[i].checked == true)
                { 
                    flag = true;
                    break;
                }
                else
                    field[i].checked = false;
            }
            if(flag == false)
            {
                alert("Please select atleast one record");
                return false;
            }
        }
        if(confirm("Are you sure to delete selected records ?"))
        {
            frm.action.value = "action_delete";
            frm.submit();
            return true ;
        }
    }

    function action_approved(frm)
    {
        with(frm)
        {
            var flag = false;
            str = '';
            field = document.getElementsByName('Id_List[]');
            for (i = 0; i < field.length; i++)
            {
                if(field[i].checked == true)
                {
                    flag = true;
                    break;
                }
                else
                    field[i].checked = false;
            }
            if(flag == false)
            {
                alert("Please select atleast one record");
                return false;
            }
        }
        if(confirm("Are you sure to approve selected records ?"))
        {
            frm.action.value = "action_update";
            frm.submit();
            return true ;
        }
    }

    function approved_product_check(frm)
    {
        if(confirm("Are you sure to approve selected records ?"))
        {
            frm.action.value = "action_update";
            frm.submit();
            return true ;
        }
    }

    $('.selectpicker.select-all').on('change', function () {
        var selectPicker = $(this);
        var selectAllOption = selectPicker.find('option.select-all');
        var checkedAll = selectAllOption.prop('selected');
        var optionValues = selectPicker.find('option[value!="[all]"][data-divider!="true"]');

        if (checkedAll) {
            // Process 'all/none' checking
            var allChecked = selectAllOption.data("all") || false;

            if (!allChecked) {
                optionValues.prop('selected', true).parent().selectpicker('refresh');
                selectAllOption.data("all", true);
            }
            else {
                optionValues.prop('selected', false).parent().selectpicker('refresh');
                selectAllOption.data("all", false);
            }

            selectAllOption.prop('selected', false).parent().selectpicker('refresh');
        }
        else {
            // Clicked another item, determine if all selected
            var allSelected = optionValues.filter(":selected").length == optionValues.length;
            selectAllOption.data("all", allSelected);
        }
    }).trigger('change');
    // For Multipe delete Insys : END
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</body>
</html>