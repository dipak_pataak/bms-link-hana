<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BMS Link</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <!-- stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/style.css" media="screen" />
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/colors/blue.css" />

    </head>
    <body>
        <div id="login">

             <div class="p-1">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>

            <!-- login -->
            <div class="title">
                <h5>Sign In to BMSLink</h5>
                <div class="corner tl"></div>
                <div class="corner tr"></div>
            </div>



            <div class="inner">
               
                <form action="<?php echo base_url(); ?>b_level/auth_controller/authentication" method="post">
                    <div class="form">
                        <!-- fields -->
                        <div class="fields">

                            <div class="field">
                                <div class="label">
                                    <label for="username">Username:</label>
                                </div>
                                <div class="input">
                                    <input type="text" id="username" name="email" size="40" autocomplete="off" class="focus" value="<?php echo get_cookie("email"); ?>" required />
                                </div>
                            </div>

                            <div class="field">
                                <div class="label">
                                    <label for="password">Password:</label>
                                </div>
                                <div class="input">
                                    <input type="password" id="password" name="password" size="40" class="focus" value="<?php echo get_cookie("password"); ?>" required />
                                </div>
                            </div>

                            <div class="field">
                                <div class="checkbox">
                                    <input type="checkbox" id="remember" name="remember" value="1" />
                                    <label for="remember">Remember me</label>
                                </div>
                            </div>

                            <div class="buttons">
                                <input type="submit" class="btn btn-success text-right"  value="Login">
                            </div>
                        </div>
                        <!-- end fields -->
                        <!-- links -->
                        <div class="links">
                            <a href="<?php echo  base_url(); ?>b-level-forgot-password-form">Forgot your password?</a>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo base_url(); ?>c-level">C-Level View</a>
                        </div>
                        <!-- end links -->
                    </div>
                </form>
            </div>


            <div class="panel-footer">
                <table style="cursor:pointer;font-size:12px" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Pass</th> 
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>admin@gmail.com</td>
                            <td>123456</td> 
                        </tr>

                        <tr>
                            <td>john@gmail.com</td>
                            <td>123456</td> 
                        </tr> 

                    </tbody>
                </table>
            </div>


            <!-- end login -->
<!--            <div id="colors-switcher" class="color">
                <a href="" class="blue" title="Blue"></a>
                <a href="" class="green" title="Green"></a>
                <a href="" class="brown" title="Brown"></a>
                <a href="" class="purple" title="Purple"></a>
                <a href="" class="red" title="Red"></a>
                <a href="" class="greyblue" title="GreyBlue"></a>
            </div>-->
        </div>

        <!-- scripts (jquery) -->
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-ui-1.8.16.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                var info = $('table tbody tr');
                info.click(function () {
                    var email = $(this).children().first().text();
                    var password = $(this).children().first().next().text();
                    $("input[name=email]").val(email);
                    $("input[name=password]").val(password);
                });
            });
        </script>

    </body>
</html>