
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Stock Availablity</h5>
        </div>
        <!-- end box / title -->
<!--                    <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Product Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Category Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Stock">
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="reset" class="btn btn-sm btn-danger default">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>-->

        <div class="px-3">
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Raw Material</th>
                        <th>Pattern</th>
                        <th>Color</th>
                        <th>In Qty</th>
                        <th>Out Qty</th>
                        <th>Available Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($get_allrecord as $single) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $single->material_name; ?></td>
                            <td><?php echo ($single->pattern_name!=null?$single->pattern_name:'n/a'); ?></td>
                            <td><?php echo ($single->color_name!=NULL?$single->color_name:'n/a'); ?></td>
                            <td><?php echo $single->inqty; ?></td>
                            <td><?php echo $single->outqty; ?></td>
                            <td>
                                <?php echo $availableqty = $single->inqty - $single->outqty; ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
                <?php if (empty($get_allrecord)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="7" class="text-center text-danger">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<!-- end content / right -->
