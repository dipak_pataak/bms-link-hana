
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Stock History</h5>
        </div>
        <!-- end box / title -->
<!--        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>-->
        <div class="collapses px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" method="post" action="#">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="material_id" class="form-control select2" id="material_id" data-placeholder ='-- select one --'>
                                    <option value=""></option>
                                    <?php
                                    foreach ($get_raw_materials as $material) {
                                        echo "<option value='$material->id'>$material->material_name</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3 datepicker" name="from_date" id="from_date" placeholder="YYYY-MM-DD">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3 datepicker" name="to_date" id="to_date" placeholder="YYYY-MM-DD">
                            </div>
                            <div class="col-md-12 text-right">
                                <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                <input type="button" class="btn btn-sm btn-success default" id="go_btn" onclick="filter_result()" value="Go">
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>

        <div class="px-3">
            <table class="table table-bordered text-center" id="results">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Raw Material</th>
                        <th>Pattern</th>
                        <th>Color</th>
                        <th>In Qty</th>
                        <th>Out Qty</th>
                        <th>Manufacturing</th>
                        <!-- <th>Measurement</th> -->
                        <th>Date</th>
                        <!--<th>Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($raw_material_stock_info as $single) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $single->material_name; ?></td>
                            <td><?php echo ($single->pattern_name!=null?$single->pattern_name:'n/a'); ?></td>
                            <td><?php echo ($single->color_name!=NULL?$single->color_name:'n/a'); ?></td>
                            <td><?php echo $single->in_qty; ?></td>
                            <td><?php echo $single->out_qty; ?></td>
                            <td><?php echo $single->from_module; ?></td>                       
                            <!-- <td><?php echo $single->measurment; ?></td>                        -->
                            <td><?php echo $single->stock_date; ?></td>                       
       <!--                        <td>
                             <a href="#" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                            </td>-->
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($raw_material_stock_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="10" class="text-center text-danger">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    function filter_result() {
        var material_id = $("#material_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        $.ajax({
            url: "b_level/Stock_controller/filter_result/",
            type: "POST",
            data: {material_id: material_id, from_date: from_date, to_date: to_date},
            success: function (r) {
                  $("#results").html(r);
            }
        });
    }
</script>