<table class="table table-bordered text-center" id="results">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Raw Material</th>
                        <th>Pattern</th>
                        <th>Color</th>
                        <th>In Qty</th>
                        <th>Out Qty</th>
                        <th>Manufacturing</th>
                        <th>Measurement</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($get_stock_history as $single) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $single->material_name; ?></td>
                            <td><?php echo $single->pattern_name; ?></td>
                            <td><?php echo $single->color_name; ?></td>
                            <td><?php echo $single->in_qty; ?></td>
                            <td><?php echo $single->out_qty; ?></td>
                            <td><?php echo $single->from_module; ?></td>                       
                            <td><?php echo $single->measurment; ?></td>                       
                            <td><?php echo $single->stock_date; ?></td>  
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($get_stock_history)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="10" class="text-center text-danger">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>