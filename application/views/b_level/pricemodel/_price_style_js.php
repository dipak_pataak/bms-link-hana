<script type="text/javascript">

    function generateTable() {

        var data = $('textarea[name=excel_data]').val();
        //console.log(data);
        var exdata = data.trim();

        if (exdata !== '') {

            var rows = data.split("\n");

            var table = $('<table class="table-bordered" />');

            var pr = 0;

            for (var y in rows) {

                pr++;
                var cells = rows[y].replace(/["]+/g, "''").split("\t");
                var row = $('<tr /><br/>');
                row.append('<input type="hidden" name="row[]" value="' + cells + '"/>');
                var n = 0;

                for (var x in cells) {
                    n++;

                    if (pr != 1 && n != 1) {
                        var clss = n + "_" + pr;
                        row.append('<input type="text" size="5"  name="test' + n + pr + '" value="' + cells[x].trim() + '" class="price_input fr" id="' + clss + '" autocomplete="off"/>');
                    } else {

                        var vv = (cells[x].replace(/["]+/g, "''"));

                        row.append('<input type="text" size="5" class="fr" name="test' + n + pr + '" value="' + vv.trim() + '" autocomplete="off"/>');
                    }
                }
                table.append(row);
            }

            // Insert into DOM
            $('#excel_table').html(table);
            //$('#exdata').slideUp();

        } else {

            alert('Enter your excel data');

            $('#exdata').show();

        }

    }



    $("#whole").on("click", function () {
        $('.price_input').css('border', ' 1px solid #cabdbd');
        $("#whole").val(1);
        $("#selection").val(0);
    });

//  $("input[type='checkbox']").change(function () {
//         $(this).siblings('div')
//             .find("input[type='checkbox']")
//             .prop('checked', this.checked);
// });

    $("#selection").on("click", function () {
        $('.price_input').css('border', '');
        $("#whole").val(0);
        $("#selection").val(1);
    });


    $("#percentage").on("click", function () {
        $("#percentage").val(1);
        $("#fixed").val(0);
    });

    $("#fixed").on("click", function () {
        $("#percentage").val(0);
        $("#fixed").val(1);
    });




    function plusData() {

        var percentage = $("#percentage").val();

        var setVal = $("#setVal").val();

        var whole = $("#whole").val();
        var selection = $("#selection").val();
        if (whole != 0) {

            if (percentage != 0) {

                $(".price_input").each(function () {
                    var id = $(this).attr('id');
                    var getVal = $("#" + id).val();
                    var newVal = (getVal / 100) * setVal;
                    var setv = parseFloat(getVal) + parseFloat(newVal);
                    $("#" + id).val(setv);
                });

            } else {

                $(".price_input").each(function () {
                    var id = $(this).attr('id');
                    var getVal = $("#" + id).val();
                    var setv = parseFloat(getVal) + parseFloat(setVal);
                    $("#" + id).val(setv);
                });

            }

        }

    }

    function minusData() {

        var percentage = $("#percentage").val();
        var setVal = $("#setVal").val();
        var whole = $("#whole").val();
        var selection = $("#selection").val();
        var setval;

        if (whole != 0) {
            if (percentage != 0) {

                $(".price_input").each(function () {
                    var id = $(this).attr('id');
                    var getVal = $("#" + id).val();
                    var newVal = (getVal / 100) * setVal;
                    var setv = parseFloat(getVal) - parseFloat(newVal);
                     if(setv<=0){ setval=0;}else{ setval =setv;}
                    $("#" + id).val(setv);
                });

            } else {

                $(".price_input").each(function () {
                    var setval;
                    var id = $(this).attr('id');
                    var getVal = $("#" + id).val();
                    var setv = parseFloat(getVal) - parseFloat(setVal);
                    if(setv<=0){ setval=0;}else{ setval =setv;}

                    $("#" + id).val(setval);
                });

            }

        }
    }



    function editTable() {
        var id = $("#style_id").val();
        var url = "<?php echo site_url('b_level/Pricemodel_controller/edit') ?>/" + id;
        $.ajax({
            url: url,
            type: "GET",
            success: function (data)
            {
                $('#excel_table').html(data)
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });

    }



    // submit form and add data
    $("#formStylePrice").on('submit', function (e) {
        e.preventDefault();

        var submit_url = "save-price-style";
        var style_type = $(".style_type").val();
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

//                alert(res);
                if (res === '1') {
                    alert('Save successfully');
                }
                if (res === '2') {
                    alert('Internal error plese try again');
                }
                if (style_type == '1') {
                    window.location.href = "<?php echo base_url(); ?>" + "add-price";
                }
                if (style_type == '4') {
                    window.location.href = "<?php echo base_url(); ?>" + "add-group-price";
                }

            }, error: function () {
                alert('error');
            }
        });
    });


    // submit form and add data
    $("#formStylePriceUpdate").on('submit', function (e) {
        e.preventDefault();
        var style_type = $(".style_type").val();
        var submit_url = "<?php echo base_url('b_level/pricemodel_controller/update_price_style') ?>";

        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                if (res === '1') {
                    if (style_type == '1') {
                        toastr.success('Update Successfully!');
                        setTimeout(function () {
                            window.location.href = "<?php echo base_url(); ?>" + "manage-price";
                        }, 3000);
                    }
                    if (style_type == '4') {
                        toastr.success('Update Successfully!');
                        setTimeout(function () {
                            window.location.href = "<?php echo base_url(); ?>" + "manage-group-price";
                        }, 3000);
                    }

                }
                if (res === '2') {
                    if (style_type == '1') {
                        toastr.error('Internal error plese try again');
                        setTimeout(function () {
                            window.location.href = "<?php echo base_url(); ?>" + "manage-price";
                        }, 3000);
                    }
                    if (style_type == '4') {
                        toastr.error('Internal error plese try again');
                        setTimeout(function () {
                            window.location.href = "<?php echo base_url(); ?>" + "manage-group-price";
                        }, 3000);
                    }
                }


            }, error: function () {
                alert('error');
            }
        });
    });




    $("#formId").on('submit', function (e) {
        e.preventDefault();

        var submit_url = "<?php echo base_url(); ?>" + "welcome/save_style";

        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {
                window.location.href = "<?php echo base_url(); ?>" + "welcome";

            }, error: function () {

            }
        });

    });




</script>