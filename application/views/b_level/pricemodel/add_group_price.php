
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Add Group Price</h5>
        </div>

        <div class="col-md-12">

            <div class="alert alert-warning">
                <strong>Notes! </strong> 1. Do not use any text. 2. Please use (') single quote twice to show inch symbol. ex: 60''
            </div>

        <!--<div class="">
                <select class="form-control select2" name="" id="style_id" onchange="editTable()" data-placeholder="-- select one --">
                    <option value=""></option>
                    <?php foreach ($style_slist as $s) { ?>
                        <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
                    <?php } ?>
                </select>
            </div> -->

            <!--            <div class="btn-group">
            <?php foreach ($style_slist as $s) { ?>
                                            <a  href="javascript:void" class="btn btn-primary btn-sm" onclick="editTable('<?php echo $s->style_id ?>')"><?= $s->style_name ?></a>
            <?php } ?>
                        </div>-->
        </div>

        <div class="col-md-12">
            <div id="exdata">
                <p>Paste excel data here:</p>
                <textarea name="excel_data" style="width:100%; height:150px;" onblur="javascript:generateTable()"></textarea>
            </div>
        </div>



        <!-- end box / title -->
        <?= form_open('#', array('id' => 'formStylePrice', 'name' => 'formStylePrice', 'class' => 'px-3')) ?>

        <div class="form-row">
            <div class="form-group mb-3 col-md-12">
                <label class="col-form-label">Price Style name</label>
                <input type="text" name="price_style_name" class="form-control" placeholder="Enter style name" required>
                <input type="hidden" name="style_type" class="form-control style_type" value="4">
            </div>


            <div class="form-group mb-3 col-md-12">
                <label for="productquantity" class="col-form-label">Price Sheet</label>
                <div id="excel_table"></div>
            </div>


            <div class="form-group mb-0 col-md-6 mb-0">
                <button type="submit" class="btn btn-primary my-4 float-right">Add</button>
            </div>
        </div>

        <?php echo form_close(); ?>


        <div class="col-md-6">
            <!-- <p>The .table-bordered class adds borders to a table:</p>             -->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Edit Price by</th>
                        <th>Apply to</th>
                        <th>Value</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>

                <tbody>

                    <tr>

                        <td>
                            <label class="radio-inline">
                                <input type="radio" name="optradio" id="percentage" value=""> Percentage
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="optradio" id="fixed" value=""> Fixed
                            </label>

                        </td>

                        <td>
                            <label class="radio-inline">
                                <input type="radio" name="apply" id="whole" value=""> Whole table 
                            </label>

                        </td>

                        <td>
                            <label class="radio-inline">
                                Enter value
                            </label>

                            <input type="number" name="val" id="setVal" class="form-control" min="0">

                        </td>

                        <td>
                            <button class="btn btn-sm btn-success" onclick="plusData()" style="font-size:20px;">+</button>
                            <button class="btn btn-sm btn-danger" onclick="minusData()" style="font-size:20px;" >-</button>

                        </td>

                    </tr>

                </tbody>
            </table>
        </div>



    </div>
</div>
<!-- end content / right -->

<style type="text/css">
    .fr {
        padding: 2px;
        font-size: 10px;
        color: #495057;
        background-color: #fff;
        border: 1px solid #ced4da;
        border-radius: 0;
    }

</style>

<?php
$this->load->view($price_style_js);
?>

