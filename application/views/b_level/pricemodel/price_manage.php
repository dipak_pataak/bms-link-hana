
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage Row-column Price</h5>

        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- end box / title -->
        <p class="px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
            <a href="javascript:void(0)" class="btn btn-danger mt-1 action-delete" style="margin-top: -3px !important;" onClick="return action_delete(document.recordlist)" >Delete</a>
        </p>
        <div class="collapse px-3 mt-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>price-manage-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3 price_sheet_name" name="price_sheet_name" placeholder="Price Sheet Name" tabindex="1" required>
                            </div>
                            <!--                            <div class="col-md-4">
                                                            <input type="text" class="form-control mb-3" placeholder="Assigned Product">
                                                        </div>-->
                            <div class="col-md-4 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default"  tabindex="2">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="keywordkeyup_search()" placeholder="Search..." tabindex="">
                </div>
            </div>          
        </div>
        <div class="table-responsive p-3" id="price_results">
        <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Pricemodel_controller/manage_action') ?>">
            <input type="hidden" name="action">
            <input type="hidden" name="action_type" value="price_manage">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th>SL No.</th>
                        <th>Price Sheet Name</th>
                        <th>Assigned Product</th>
                        <th>Is Active</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $sl = 0;
                    foreach ($style_slist as $val) {
                        $sl++;
                        ?>

                        <tr>
                            <td>
                                <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $val->style_id; ?>" class="checkbox_list">  
                            </td>
                            <td><?php echo $sl; ?></td>
                            <td><?= $val->style_name ?> </td>
                            <td><?= $val->product_name ?></td>
                            <td><?= ($val->status == 1 ? 'Active' : 'Inactive') ?></td>
                            <td>
                                <a href="<?= base_url('b_level/pricemodel_controller/edit_style/') . $val->style_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a>
                                <a href="<?= base_url('b_level/pricemodel_controller/delete_price_style/') . $val->style_id; ?>" onclick="return confirm('Are you sure')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>

                    <?php } ?>

                </tbody>
                <?php if (empty($style_slist)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="5" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>
        </form>
        </div>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
   function keywordkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>row-column-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#price_results").html(r);
            }
        });
    }
</script>
