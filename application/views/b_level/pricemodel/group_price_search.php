<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>SL No.</th>
            <th>Price Sheet Name</th>
            <th>Assigned Product</th>
            <th>Is Active</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>

        <?php
        $sl = 0;
        foreach ($style_slist as $val) {
            $sl++;
            ?>

            <tr>
                <td><?php echo $sl; ?></td>
                <td><?= $val->style_name ?> </td>
                <td><?= $val->product_name ?></td>
                <td><?= ($val->status == 1 ? 'Active' : 'Inactive') ?></td>
                <td>
                    <a href="<?= base_url('b_level/pricemodel_controller/edit_group_price_style/') . $val->style_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a>
                    <a href="<?= base_url('b_level/pricemodel_controller/delete_price_style/') . $val->style_id; ?>" onclick="return confirm('Are you sure')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                </td>
            </tr>

        <?php } ?>

    </tbody>
    <?php if (empty($style_slist)) { ?>
        <tfoot>
            <tr>
                <th colspan="5" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>