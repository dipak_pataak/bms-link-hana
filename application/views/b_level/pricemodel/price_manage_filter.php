
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Price Manage</h5>

        </div>
        <!-- end box / title -->
        <p class="px-3">
            <!--            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Filter
                        </button>-->
        </p>
        <div class="collapsexxx px-3 mt-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>price-manage-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3 price_sheet_name" name="price_sheet_name" placeholder="Price Sheet Name" value="<?php echo $price_sheet_name; ?>" tabindex="1">
                            </div>
                            <!--                            <div class="col-md-4">
                                                            <input type="text" class="form-control mb-3" placeholder="Assigned Product">
                                                        </div>-->
                            <div class="col-md-4 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" tabindex="2">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="col-sm-12 text-right" style="margin-top: 10px;">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="keywordkeyup_search()" placeholder="Search..." tabindex="">
                </div>
            </div>          
        </div>
        <div class="table-responsive p-3" id="price_results">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Price Sheet Name</th>
                        <th>Assigned Product</th>
                        <th>Is Active</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $sl=0;
                    foreach ($get_price_manage_filter_data as $val) { 
                        $sl++;
                        ?>

                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?= $val->style_name ?> </td>
                            <td><?= $val->product_name ?></td>
                            <td><?= ($val->active_status == 1 ? 'Active' : 'Inactive') ?></td>
                            <td>
                                <a href="<?= base_url('b_level/pricemodel_controller/edit_style/') . $val->style_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a>
                                <a href="<?= base_url('b_level/pricemodel_controller/delete_price_style/') . $val->style_id; ?>" onclick="return confirm('Are you sure')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>

                    <?php } ?>

                </tbody>
                <?php if (empty($get_price_manage_filter_data)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="5" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>

        </div>

    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
   function keywordkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>row-column-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#price_results").html(r);
            }
        });
    }
</script>
