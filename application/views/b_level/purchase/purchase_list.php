
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12 m-2">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
                ?> 
            </div>
        </div>

        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Purchase List</h5>
            <div class="col-sm-6 text-right">
<!--                <a href="<?php echo base_url(); ?>purchase-entry" class="btn btn-success mt-1">Add</a>-->
            </div>
        </div>

        <!-- end box / title -->
        <div class="px-3">
            <a href="<?php echo base_url(); ?>purchase-entry" class="btn btn-success mb-2">Add New</a>
            
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Supplier Name</th>
                        <th>Purchase id</th>
                        <th>Date</th>
                        <th>Total</th>
                        <th>Payment Type</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($purchases != NULL) {
                        $i = 1;
                        foreach ($purchases as $key => $val) {
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val->supplier_name; ?></td>
                                <td><?= $val->purchase_id; ?></td>
                                <td><?= date('M-d-Y', strtotime($val->date)); ?></td>
                                <td><?= $val->grand_total; ?></td>
                                <td><?= ($val->payment_type == 1 ? 'Cash' : 'Check'); ?></td>
                                <td class="text-center">
                                    <a href="view-purchase/<?= $val->purchase_id ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <!--<a href="edit-purchase/<?= $val->purchase_id ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="edit"><i class="fa fa-edit" aria-hidden="true"></i></a>-->
                                    <a href="raw-material-return-purchase/<?= $val->purchase_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>
                                    <a href="delete-purchase/<?= $val->purchase_id ?>" onclick="return confirm('Are you sure want to delete it? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($purchases)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="7" class="text-center text-danger">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>

        </div>
    </div>
</div>
<!-- end content / right -->
