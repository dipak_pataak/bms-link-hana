
<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12">
                <?php 
                    $message = $this->session->flashdata('message');
                    if($message)
                        echo $message;
                ?> 
            </div>
        </div>

        
        <!-- box / title -->
        <div class="title row">
            <h5> Edit Purchase</h5>
        </div>


        <!-- end box / title -->
        <form action="<?php echo base_url('update-purchase'); ?>" method="post" class="px-3" name="edit_purchase">

            <div class="row" id="supplier_info">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="supplier_sss" class="col-sm-3 col-form-label">Supplier
                        </label>
                        <div class="col-sm-9">
                            <select name="supplier_id" class="form-control select2" tabindex="1" data-placeholder="-- select one --" required>
                                <option value=""></option>
                                <?php
                                foreach ($get_supplier as $supplier) {
                                    echo "<option value='$supplier->supplier_id' ".($purchase->supplier_id==$supplier->supplier_id?'selected':'')." >$supplier->supplier_name</option>";
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="date" class="col-sm-3 col-form-label">Date </label>
                        <div class="col-sm-9">
                            <input type="text" name="date" class="form-control datepicker" value="<?php echo @$purchase->date; ?>" tabindex="2">
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive" style="margin-top: 10px">
                <table class="table table-bordered table-hover" id="normalinvoice">

                    <thead>
                        <tr>
                            <th class="text-center">Row material</th>
                            <th class="text-center">Color</th>
                            <th class="text-center">Pattern/Model</th>
                            <th class="text-center">Stock Qnt</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Discount </th>
                            <th class="text-center">Total </th>
                            <th class="text-center">Action </th>
                        </tr>
                    </thead>

                    <tbody id="addinvoiceItem">

                    <?php if(!empty($purchase_details)){
                        $i=1;
                        foreach ($purchase_details as $key => $p) {
                          
                    ?>    
                        <tr>
                            <td>
                                <select  id='rmtt_id_<?=$val->id?>' name='rmtt_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>
                                    <option value=''>-- select one --</option>
                                    <?php foreach($rmtts as $val){?>
                                        <option value='<?=$val->id?>' <?=($p->material_id==$val->id?'selected':'')?> > <?=$val->material_name;?></option>
                                    <?php }?>
                                </select>
                            </td>

                            <td>
                                <select  id='color_id_<?=$val->id?>' name='color_id[]' class='form-control select2' tabindex='2' data-placeholder='-- select one --' required>
                                    <option value=''>-- select one --</option>
                                    <?php foreach($colors as $val){?>
                                        <option value='<?=$val->id?>' <?=($p->color_id==$val->id?'selected':'')?> > <?=$val->color_name;?></option>
                                    <?php }?>
                                </select>
                            </td>

                            <td>
                                <select  id='pattern_model_id_<?=$val->id?>' name='pattern_model_id[]' class='form-control select2' tabindex='3' data-placeholder='-- select one --' required>
                                    <option value=''>-- select one --</option>
                                    <?php foreach($patern_model as $val){?>
                                        <option value='<?=$val->pattern_model_id?>' <?=($p->pattern_model_id==$val->pattern_model_id?'selected':'')?>> <?=$val->pattern_name;?></option>
                                    <?php }?>
                                </select>
                            </td>


                            <td class="text-right">
                                <input type="text" name="available_qnt[]" class="form-control text-right" id="available_qnt_<?=$i?>" tabindex="4" readonly>
                            </td>

                            <td class="text-right">
                                <input type="number" name="product_quantity[]" onkeyup="quantity_calculate(<?=$i?>);" onchange="quantity_calculate(<?=$i?>);" value="<?=$p->quantity;?>" tabindex="5" class="form-control text-center" id="product_quantity_<?=$i?>">
                            </td>

                            <td class="test">
                                <input type="text" name="product_rate[]" onkeyup="quantity_calculate(<?=$i?>);" onchange="quantity_calculate(<?=$i?>);" id="product_rate_<?=$i?>" value="<?=$p->purchase_price?>" class="form-control product_rate_<?=$i?> text-right"  tabindex="6" required=""/>
                            </td>

                            <td class="test">
                                <input type="text" name="product_discount[]" onkeyup="quantity_calculate(<?=$i?>);" onchange="quantity_calculate(<?=$i?>);" id="product_discount_<?=$i?>" value="<?=$p->discount?>" class="form-control product_discount text-right" tabindex="7"/>
                            </td>

                            <td class="text-right">
                                <input class="form-control total_price text-right" type="text" name="unit_total_price[]" value="<?=($p->purchase_price*$p->quantity)-$p->discount?>" id="total_price_<?=$i?>" value="0.00" readonly="readonly" tabindex="8" />
                            </td>

                            <td>
                                <button style="text-align: right;" class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)"><i class="fa fa-trash-o"></i></button>
                            </td>

                            

                        </tr>

                    <?php 
                    $i++;
                        } 
                    }
                    ?>

                    </tbody>

                    <tfoot>

                        <tr>
                            <td colspan="5" rowspan="1">
                                <center><label style="text-align:center;" for="purchase_description" class="  col-form-label">Purchase Details</label></center>
                                <textarea name="purchase_description" class="form-control"> <?=$purchase->purchase_description?></textarea>
                            </td>

                            <td style="text-align:right;" colspan="2"><b>Invoice Discount</b>:</td>
                            <td class="text-right">
                                <input type="text" onkeyup="quantity_calculate(1);"  onchange="quantity_calculate(1);" id="invoice_discount" value="<?=$purchase->invoice_discount?>" class="form-control text-right" name="invoice_discount" placeholder="0.00"  />
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:right;" colspan="7"><b>Total Discount</b>:</td>
                            <td class="text-right">
                                <input type="text"  id="total_discount" class="form-control text-right" name="total_discount" value="<?=$purchase->total_discount?>" placeholder="0.00" readonly />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="7"  style="text-align:right;"><b>Grand Total:</b></td>
                            <td class="text-right">
                                <input type="text" id="grandTotal" class="form-control text-right" value="<?=$purchase->grand_total?>" name="grand_total_price" value="0.00" readonly="readonly" />
                            </td>
                        </tr>

                        <tr>
                            <td align="center">
                                <input id="add-invoice-item" class="btn btn-info" name="add-new-item" onclick="addInputField('addinvoiceItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">
                            </td>
                            <td style="text-align:right;" colspan="6"><b>Paid Amount:</b></td>
                            <td class="text-right">
                                <input type="text" id="paidAmount" 
                                       onkeyup="invoice_paidamount();" class="form-control text-right" name="paid_amount" value="<?=$purchase->paid_amount?>" placeholder="0.00" tabindex="13"/>
                            </td>
                        </tr>

                        <tr>
                            <!-- <td align="center">
                                <input type="submit" id="add_invoice" class="btn btn-success" name="add-invoice" value="Submit" tabindex="15"/>
                            </td> -->

                            <td style="text-align:right;" colspan="7"><b>Due:</b></td>
                            <td class="text-right">
                                <input type="text" id="dueAmmount" class="form-control text-right" name="due_amount" value="<?=$purchase->due_amount?>" value="0.00" readonly="readonly"/>
                            </td>
                        </tr>
                    </tfoot>
                </table>          
            </div>
            <br/>

            <div class="row" id="supplier_info">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="" class="col-sm-3 col-form-label">Payment type
                        </label>
                        <div class="col-sm-9">
                            <select name="payment_type" class="form-control select2" id="theSelect" tabindex="1" data-placeholder="-- select one --" required>
                                <option value=""></option>
                                <?php foreach($payment_types as $key => $val){?>
                                    <option value="<?=$key?>" <?=($key==$purchase->payment_type?'selected':'')?>><?=$val;?></option>
                                <?php }?>
                                
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 hidden is2" >

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">Check No
                        </label>
                        <div class="col-sm-12">
                           <input type="text" name="check_no" value="<?=@$purchase->check_no?>" class="form-control" id="check_no">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">Bank and Branch
                        </label>
                        <div class="col-sm-12">
                           <input type="text" name="branch_name" value="<?=@$purchase->bank_branch_name?>" class="form-control" id="branch_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">A/C No
                        </label>
                        <div class="col-sm-12">
                           <input type="text" name="ac_no" value="<?=@$purchase->account_no?>" class="form-control" id="ac_no">
                        </div>
                    </div>

                </div>
                
            </div>

            <input type="hidden" name="purchase_id" value="<?=$purchase->purchase_id?>">


            <div class="form-group text-right mt-2">
                <input type="submit" id="add_purchase" class="btn btn-primary default btn-sm" name="add-purchase" value="Submit " />
                <input type="submit" value="Submit and Another" name="add-purchase-another" class="btn btn-sm default btn-success" id="add_purchase_another">
            </div>

        </form>

    </div>
</div>


<?php $this->load->view('b_level/purchase/edit_purchaseJs'); ?>
<!-- content / right -->
<!-- end content / right -->

<script type="text/javascript">

//discount.forms['edit_purchase'].elements['payment_type'].value="<?php echo $purchase->payment_type?>";

    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        if(value ==1){

            $('#check_no').prop("required", false);
            $('#branch_name').prop("required", false);
            $('#ac_no').prop("required", false);
            
            $('.is2').slideUp().addClass("hidden");

        } else {

            theDiv.slideDown().removeClass("hidden");
            $('#check_no').prop("required", true);
            $('#branch_name').prop("required", true);
            $('#ac_no').prop("required", true);
        }

    });


    $(document).ready(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);

        if(value ==1){

            $('#check_no').prop("required", false);
            $('#branch_name').prop("required", false);
            $('#ac_no').prop("required", false);
            $('.is2').slideUp().addClass("hidden");

        } else {

            theDiv.slideDown().removeClass("hidden");
            $('#check_no').prop("required", true);
            $('#branch_name').prop("required", true);
            $('#ac_no').prop("required", true);
        }

    });



</script>