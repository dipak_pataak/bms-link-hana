<?php $this->load->view('b_level/purchase/purchase_invoicejs'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>


        <!-- box / title -->
        <div class="title row">
            <h5>Purchase Entry</h5>
        </div>
        <!-- end box / title -->
        <form action="<?php echo base_url('purchase-save'); ?>" method="post" class="px-3" id="purchase_frm">
            <div class="row" id="supplier_info">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="supplier_sss" class="col-sm-3 col-form-label">Supplier</label>
                        <div class="col-sm-9">
                            <select name="supplier_id" class="form-control select2" tabindex="1" data-placeholder="-- select one --" required>
                                <option value=""></option>
                                <?php
                                foreach ($get_supplier as $supplier) {
                                    echo "<option value='$supplier->supplier_id'>$supplier->supplier_name</option>";
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="date" class="col-sm-3 col-form-label">Date </label>
                        <div class="col-sm-9">
                            <input type="text" name="date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" tabindex="2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="table-responsive col-sm-12" style="margin-top: 10px;">
                    <table class="table table-bordered table-hover" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center" width="15%">Material</th>
                                <th class="text-center" width="15%">Color</th>
                                <th class="text-center" width="15%">Pattern</th>
                                <th class="text-center" width="10%">Stock Qnt</th>
                                <th class="text-center" width="10%">Qnt</th>
                                <th class="text-center" width="10%">Price</th>
                                <th class="text-center" width="10%">Discount </th>
                                <th class="text-center" width="10%">Total </th>
                                <th class="text-center" width="10%">Action </th>
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <tr>
                                <td>
                                    <select  id='rmtt_id_1' onchange='service_cals(1)' name='rmtt_id[]' class='form-control select2' tabindex='3' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                        <?php foreach ($rmtts as $val) { ?>
                                            <option value='<?= $val->id ?>'> <?= $val->material_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>

                                <td>
                                    <select  id='color_id_1' onchange="color_cals(1)" name='color_id[]' class='form-control select2' tabindex='4' data-placeholder='-- select one --' required>
                                        <option value=''>-- select one --</option>
                                        <?php // foreach ($colors as $val) { ?>
                                            <!--<option value='<?= $val->id ?>'> <?= $val->color_name; ?></option>-->
                                        <?php // } ?>
                                    </select>
                                </td>

                                <td>
                                    <select  id='pattern_model_id_1' name='pattern_model_id[]' class='form-control select2' tabindex='5' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                        <?php // foreach ($patern_model as $val) { ?>
                                            <!--<option value='<?= $val->pattern_model_id ?>'> <?= $val->pattern_name; ?></option>-->
                                        <?php // } ?>
                                    </select>
                                </td>
                                <td class="text-right">
                                    <input type="text" name="available_qnt[]" class="form-control text-right" id="available_qnt_1" readonly>
                                </td>
                                <td class="text-right">
                                    <input type="number" name="product_quantity[]" onkeyup="quantity_calculate(1);" onchange="quantity_calculate(1);" class="form-control text-center" id="product_quantity_1" tabindex="6">
                                </td>
                                <td class="test">
                                    <input type="text" name="product_rate[]" onkeyup="quantity_calculate(1);" onchange="quantity_calculate(1);" id="product_rate_1" class="form-control product_rate_1 text-right" placeholder="0.00" value="" min="0" required="" tabindex="7"/>
                                </td>
                                <td class="test">
                                    <input type="text" name="product_discount[]" onkeyup="quantity_calculate(1);" onchange="quantity_calculate(1);" id="product_discount_1" class="form-control product_discount text-right" placeholder="0.00" value="" min="0" tabindex="8"/>
                                </td>
                                <td class="text-right">
                                    <input class="form-control total_price text-right" type="text" name="unit_total_price[]" id="total_price_1" value="0.00" readonly="readonly" />
                                </td>
                                <td>
                                    <button style="text-align: right;" class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>

                        </tbody>

                        <tfoot>

                            <tr>
                                <td colspan="5" rowspan="1">
                                    <input id="add-invoice-item" class="btn btn-info" name="add-new-item" onclick="addInputField('addinvoiceItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">
                                    <br>
                                    <label style="" for="purchase_description" class="  col-form-label">Purchase Details</label>
                                    <textarea name="purchase_description" class="form-control" placeholder="Purchase Details"></textarea>
                                </td>

                                <td style="text-align:right;" colspan="2"><b>Invoice Discount</b>:</td>
                                <td class="text-right">
                                    <input type="text" onkeyup="quantity_calculate(1);"  onchange="quantity_calculate(1);" id="invoice_discount" class="form-control text-right" name="invoice_discount" placeholder="0.00" tabindex="10" />
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:right;" colspan="7"><b>Total Discount</b>:</td>
                                <td class="text-right">
                                    <input type="text"  id="total_discount" class="form-control text-right" name="total_discount" placeholder="0.00" readonly />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="7"  style="text-align:right;"><b>Grand Total:</b></td>
                                <td class="text-right">
                                    <input type="text" id="grandTotal" class="form-control text-right" name="grand_total_price" value="0.00" readonly="readonly" />
                                </td>
                            </tr>

                            <tr>
    <!--                            <td align="center">
                                    <input id="add-invoice-item" class="btn btn-info" name="add-new-item" onclick="addInputField('addinvoiceItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">
                                </td>-->
                                <td style="text-align:right;" colspan="7"><b>Paid Amount:</b></td>
                                <td class="text-right">
                                    <input type="text" id="paidAmount" 
                                           onkeyup="invoice_paidamount();" class="form-control text-right" name="paid_amount" placeholder="0.00" tabindex="13"/>
                                </td>
                            </tr>

                            <tr>
                                <!-- <td align="center">
                                    <input type="submit" id="add_invoice" class="btn btn-success" name="add-invoice" value="Submit" tabindex="15"/>
                                </td> -->

                                <td style="text-align:right;" colspan="7"><b>Due:</b></td>
                                <td class="text-right">
                                    <input type="text" id="dueAmmount" class="form-control text-right" name="due_amount" value="0.00" readonly="readonly"/>
                                </td>
                            </tr>
                        </tfoot>

                    </table>          
                </div>

                <!--                <div class="table-responsive col-sm-2" style="margin-top: 10px">
                                    <table class="table table-bordered table-hover" id="">
                                        <thead>
                                            <tr>
                                                <th>Sub Total  </th>
                                                <td>
                                                    <input type="text" name="" id="" class="form-control" readonly>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Invoice Discount </th>
                                                <td>
                                                    <input type="text" onkeyup="quantity_calculate(1);"  onchange="quantity_calculate(1);" id="invoice_discount" class="form-control text-right" name="invoice_discount" placeholder="0.00" tabindex="10" />
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>-->

            </div>
            <br/>

            <div class="row" id="supplier_info">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="" class="col-sm-3 col-form-label">Payment type
                        </label>
                        <div class="col-sm-9">
                            <select name="payment_type" class="form-control select2" id="theSelect" tabindex="" data-placeholder="-- select one --" required>

                                <option value=""></option>
                                <option value="1">Cash</option>
                                <option value="2">Check</option>

                            </select>
                        </div>
                    </div>
                </div>



                <div class="col-sm-6 hidden is2" >

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">Check No
                        </label>
                        <div class="col-sm-12">
                            <input type="text" name="check_no" class="form-control" id="check_no">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">Bank and Branch
                        </label>
                        <div class="col-sm-12">
                            <input type="text" name="branch_name" class="form-control" id="branch_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">A/C No
                        </label>
                        <div class="col-sm-12">
                            <input type="text" name="ac_no" class="form-control" id="ac_no">
                        </div>
                    </div>

                </div>

            </div>



            <div class="form-group text-right mt-2">
                <input type="submit" id="add_purchase" class="btn btn-primary default btn-sm" name="add-purchase" value="Submit " />
                <input type="submit" value="Submit and Another" name="add-purchase-another" class="btn btn-sm default btn-success" id="add_purchase_another">
            </div>
        </form>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        if (value == 1) {

            $('#check_no').prop("required", false);
            $('#branch_name').prop("required", false);
            $('#ac_no').prop("required", false);

            $('.is2').slideUp().addClass("hidden");

        } else {

            theDiv.slideDown().removeClass("hidden");
            $('#check_no').prop("required", true);
            $('#branch_name').prop("required", true);
            $('#ac_no').prop("required", true);
        }

    });


</script>