
<?php
$user_id = $this->session->userdata('user_id');
$user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();
//echo $user_info[0]->language;
?>


<!-- content -->
<div id="content">
    <!-- end content / left -->
    <div id="left">
        <div id="menu">
            <h6 id="h-menu-dboard" class="dashboard_slt">

                <?php
                $modules = $this->db->select('*')->from('b_menusetup_tbl')->where('menu_type', 1)
                                ->group_by('module')->order_by('ordering', 'asc')->get()->result();



//                dd($modules);
                if ($user_info[0]->language == 'English') {
                    $menu_title = $modules[0]->menu_title;;
                } elseif ($user_info[0]->language == 'Korean') {
                    $menu_title = $modules[0]->korean_name;;
                }else{
                     $menu_title = $modules[0]->menu_title;;
                }
                ?>



                <a href="<?php echo base_url(); ?>b-level-dashboard">
                    <span>
                        <?php echo ucfirst($menu_title); ?>
                    </span>
                </a>
            </h6>

            <?php
            $modules = $this->db->select('*')->from('b_menusetup_tbl')->where('menu_type', 1)->where('status', 1)
                            ->group_by('module')->order_by('ordering', 'asc')->get()->result();



//            echo '<pre>';            print_r($modules);            echo '</pre>';
            foreach ($modules as $module)
            {

                if ($this->permission->module($module->module)->access())
                {
                  //  print_r(1);die();
                    $menu_item = $this->db->select('*')
                                    ->from('b_menusetup_tbl')
                                    ->where('module', $module->module)
                                    ->where('parent_menu =', $module->id)
                                    ->where('status', 1)
                                    ->order_by('ordering', 'asc')
                                    ->get()->result();
                                    

                    $module_id = $module->parent_menu;
                    $menu_title = '';
                    if ($user_info[0]->language == 'English') {
                        $menu_title = $module->menu_title;
                    } elseif ($user_info[0]->language == 'Korean') {
                        $menu_title = $module->korean_name;
                    }
                    if (empty($menu_item)) {
                        $link = base_url() . $module->page_url;
                    } else {
                        $link = "#" . $module->module;
                    }
                    ?>
                    <h6 id="h-menu-<?php echo $module->module; ?>" class=""><a href="<?php echo $link; ?>">
                            <span><?php echo ucwords(str_replace('_', ' ', $menu_title)); ?></span></a>
                    </h6>

                    <ul id="menu-<?php echo $module->module; ?>" class="closed">
                        <?php

                        foreach ($menu_item as $menu) {

                            if ($this->permission->check_label($menu->menu_title)->access()) {
                                $parent_id = $menu->id;
                                $sub_sub_menu = $this->db->select('*')
                                                ->from('b_menusetup_tbl')
                                                ->where('module', $menu->module)
                                                ->where('parent_menu =', $parent_id)
                                                ->where('status', 1)
                                                ->order_by('ordering', 'asc')
                                                ->get()->result();
//                        echo '<pre>';   print_r($sub_sub_menu); echo '</pre>'; 
                                if ($user_info[0]->language == 'English') {
                                    $submenu_title = $menu->menu_title;
                                } elseif ($user_info[0]->language == 'Korean') {
                                    $submenu_title = $menu->korean_name;
                                }
                                ?>
                                <li class="collapsible" id="<?php echo $menu->id; ?>">
                                    <?php if (!empty($sub_sub_menu)) { ?>
                                        <a href="javascript:void()" class="collapsible plus" id="<?php echo $menu->id; ?>">
                                            <?php echo ucwords(str_replace('_', ' ', $submenu_title)); ?></a>
                                        <ul class="collapsed"  id="<?php echo $menu->menu_title; ?>">
                                            <?php
                                            foreach ($sub_sub_menu as $child) {
                                                if ($this->permission->check_label($child->menu_title)->access()) {
                                                    $parent_child_id = $child->id;
                                                    $sub_child_menu = $this->db->select('*')
                                                                    ->from('b_menusetup_tbl')
                                                                    ->where('module', $child->module)
                                                                    ->where('parent_menu =', $parent_child_id)
                                                                    ->where('status', 1)
                                                                    ->order_by('ordering', 'asc')
                                                                    ->get()->result();
                                                    if ($user_info[0]->language == 'English') {
                                                        $childmenu_title = $child->menu_title;
                                                    } elseif ($user_info[0]->language == 'Korean') {
                                                        $childmenu_title = $child->korean_name;
                                                    }
                                                    $class_collaps = '';
                                                    $class_collaps_2 = '';
                                                    $url_link = base_url() . $child->page_url;
                                                    if (!empty($sub_child_menu)) {
                                                        $class_collaps = 'collapsible';
                                                        $class_collaps_2 = 'collapsible plus';
                                                        $url_link = '#';
                                                        $ids = $child->id;
                                                    } else {
                                                        $ids = $child->id;
                                                    }
                                                    ?>
                                                    <li class="<?php echo $class_collaps; ?>" id="<?php echo $ids; ?>">
                                                        <a href="<?php echo $url_link; ?>" class="<?php echo $class_collaps_2; ?>" id="<?php echo $ids; ?>">
                                                            <?php echo ucwords(str_replace('_', ' ', $childmenu_title)); ?>
                                                        </a>
                                                        <?php if (!empty($sub_child_menu)) { ?>
                                                            <ul class="collapsed" id="<?php echo $child->menu_title; ?>">
                                                                <?php
                                                                foreach ($sub_child_menu as $sub_child) {
                                                                    if ($user_info[0]->language == 'English') {
                                                                        $subchildmenu_title = $sub_child->menu_title;
                                                                    } elseif ($user_info[0]->language == 'Korean') {
                                                                        $subchildmenu_title = $sub_child->korean_name;
                                                                    }
                                                                    if ($this->permission->check_label($sub_child->menu_title)->access()) {
                                                                        ?>
                                                                        <li  id="<?php echo $sub_child->page_url; ?>">
                                                                            <a href="<?php echo base_url() . $sub_child->page_url; ?>">
                                                                                <?php echo ucwords(str_replace('_', ' ', $subchildmenu_title)); ?>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                    <?php
                                } else {
                                    $submenu_title = '';
                                    if ($user_info[0]->language == 'English') {
                                        $submenu_title = $menu->menu_title;
                                    } elseif ($user_info[0]->language == 'Korean') {
                                        $submenu_title = $menu->korean_name;
                                    }
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?><?php echo $menu->page_url; ?>">
                                            <?php echo ucwords(str_replace('_', ' ', $submenu_title)); ?></a>
                                    </li>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </ul>
                    <?php
                }
            }
            ?>
<!--            <h6 id="" class=""><a href="#"><span>------------ just test --------</span></a></h6>
<h6 id="h-menu-links" class=""><a href="#links"><span>Customer</span></a></h6>
<ul id="menu-links" class="closed">
<li><a href="<?php echo base_url(); ?>add-b-customer">Add</a></li>
<li><a href="<?php echo base_url(); ?>b-customer-list">Manage</a></li>
<li><a href="<?php echo base_url(); ?>customer-import">Bulk Upload</a></li>
</ul>
<h6 id="h-menu-order" class=""><a href="#order"><span>Order</span></a></h6>
<ul id="menu-order" class="closed">
<li><a href="<?php echo base_url(); ?>new-order">New</a></li>
<li class="collapsible">
<a href="#" class="collapsible plus">Manage Order</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>order-kanban">Kanban View</a></li>
<li><a href="<?php echo base_url(); ?>b-order-list">List View</a></li>
</ul>
</li>
</ul>
<h6 id="h-menu-catalogue" class=""><a href="#catalogue"><span>Catalog</span></a></h6>
<ul id="menu-catalogue" class="closed">
<li class="collapsible">
<a href="#" class="collapsible plus">Category</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-category">Add</a></li>
<li><a href="<?php echo base_url(); ?>manage-category">Manage</a></li>
<li><a href="<?php echo base_url(); ?>category-assign">Assigned</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Products</a>
<ul id="whatever" class="collapsed">
<li><a href="<?php echo base_url(); ?>add-product">Add</a></li>
<li><a href="<?php echo base_url(); ?>product-manage">Manage</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Pattern/Model</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-pattern">Add</a></li>
<li><a href="<?php echo base_url(); ?>manage-pattern">Manage</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Attributes</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-attribute">Add</a></li>
<li><a href="<?php echo base_url(); ?>manage-attribute">Manage</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Condition</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>cost-factor">Cost Factor</a></li>
<li><a href="<?php echo base_url(); ?>shipping">Shipping</a></li>
<li><a href="add-tax">Tax</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Price Model</a>
<ul class="collapsed">
<li class="collapsible">
<a href="#" class="collapsible plus">Row/Column Price</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-price">Add</a></li>
<li><a href="<?php echo base_url(); ?>manage-price">Manage</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Group</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-group">Add</a></li>
<li><a href="<?php echo base_url(); ?>group-manage">Manage</a></li>
</ul>
</li>
</ul>
</li>
</ul>

<h6 id="h-menu-prorules" class=""><a href="#prorules"><span>Production</span></a></h6>
<ul id="menu-prorules" class="closed">
<li class="collapsible">
<a href="#" class="collapsible plus">Manufacturer</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-manufacturer">Add</a></li>
<li><a href="<?php echo base_url(); ?>manage-manufacturer">Manage</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Stock</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>stock-availability">Stock Availability</a></li>
<li><a href="<?php echo base_url(); ?>stock-history">Stock History</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Suppliers</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>add-supplier">Suppliers Add</a></li>
<li><a href="<?php echo base_url(); ?>supplier-list">Suppliers List</a></li>
<li><a href="<?php echo base_url(); ?>supplier-invoice">Manage Invoice</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Return</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>customer-return">Customer Return</a></li>
<li><a href="<?php echo base_url(); ?>suppliers-return">Suppliers Return</a></li>
</ul>
</li>
<li><a href="<?php echo base_url(); ?>shipping">Shipping</a></li>
</ul>

<h6 id="h-menu-accounts" class=""><a href="#accounts"><span>Account</span></a></h6>
<ul id="menu-accounts" class="closed">
<li><a href="<?php echo base_url(); ?>b-account-chart">Account Chart</a></li>
<li><a href="<?php echo base_url(); ?>purchase-entry">Purchase Entry</a></li>
<li><a href="<?php echo base_url(); ?>purchase-list">Purchase List</a></li>
<li class="collapsible">
<a href="#" class="collapsible plus">Voucher</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>b-debit-voucher">Debit Voucher</a></li>
<li><a href="<?php echo base_url(); ?>b-credit-voucher">Credit Voucher</a></li>
<li><a href="<?php echo base_url(); ?>b-journal-voucher">Journal Voucher</a></li>
<li><a href="<?php echo base_url(); ?>b-contra-voucher">Contra Voucher</a></li>
<li><a href="<?php echo base_url(); ?>b-voucher-approval">Voucher Approval</a></li>
<li><a href="<?php echo base_url(); ?>b-voucher-reports">Voucher Reports</a></li>
</ul>
</li>
<li class="collapsible">
<a href="#" class="collapsible plus">Account Reports</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>b-bank-book">Bank Book</a></li>
<li><a href="<?php echo base_url(); ?>b-cash-book">Cash Book</a></li>
<li><a href="<?php echo base_url(); ?>b-cash-flow">Cash Flow</a></li>
<li><a href="<?php echo base_url(); ?>b-general-ledger">General Ledger</a></li>
<li><a href="<?php echo base_url(); ?>b-profit-loss">Profit Loss</a></li>
<li><a href="<?php echo base_url(); ?>b-trial-ballance">Trial Ballance</a></li>
</ul>
</li>
</ul>

<h6 id="h-menu-settings" class=""><a href="#settings"><span>Setting</span></a></h6>
<ul id="menu-settings" class="closed">
<li><a href="<?php echo base_url(); ?>profile-setting">Profile Settings</a></li>
<li class="collapsible">
<a href="#" class="collapsible plus">Integration</a>
<ul class="collapsed">
<li><a href="<?php echo base_url(); ?>shipping">Shipping</a></li>
<li><a href="<?php echo base_url(); ?>mail">Email</a></li>
<li><a href="<?php echo base_url(); ?>sms">SMS</a></li>
<li><a href="<?php echo base_url(); ?>gateway">Payment Gateway</a></li>
</ul>
</li>
<li><a href="<?php echo base_url(); ?>b-level-users">Users</a></li>
</ul>-->
        </div>

        <!--        <div id="menu">
                    <h6 id="h-menu-dboard" class="dashboard_slt"><a href="<?php echo base_url(); ?>b-level-dashboard"><span>Dashboard</span></a></h6>
                    <h6 id="h-menu-links" class=""><a href="#links"><span>Customer</span></a></h6>
                    <ul id="menu-links" class="closed">
                        <li><a href="<?php echo base_url(); ?>add-b-customer">Add</a></li>
                        <li><a href="<?php echo base_url(); ?>b-customer-list">Manage</a></li>
                        <li><a href="<?php echo base_url(); ?>customer-import">Bulk Upload</a></li>
                    </ul>
                    <h6 id="h-menu-order" class=""><a href="#order"><span>Order</span></a></h6>
                    <ul id="menu-order" class="closed">
                        <li><a href="<?php echo base_url(); ?>new-order">New</a></li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Manage Order</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>order-kanban">Kanban View</a></li>
                                <li><a href="<?php echo base_url(); ?>b-order-list">List View</a></li>
                            </ul>
                        </li>
                    </ul>
                    <h6 id="h-menu-catalogue" class=""><a href="#catalogue"><span>Catalog</span></a></h6>
                    <ul id="menu-catalogue" class="closed">
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Category</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>add-category">Add</a></li>
                                <li><a href="<?php echo base_url(); ?>manage-category">Manage</a></li>
                                <li><a href="<?php echo base_url(); ?>category-assign">Assigned</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Products</a>
                            <ul id="whatever" class="collapsed">
                                <li><a href="<?php echo base_url(); ?>add-product">Add</a></li>
                                <li><a href="<?php echo base_url(); ?>product-manage">Manage</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Pattern/Model</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>add-pattern">Add</a></li>
                                <li><a href="<?php echo base_url(); ?>manage-pattern">Manage</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Attributes</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>add-attribute">Add</a></li>
                                <li><a href="<?php echo base_url(); ?>manage-attribute">Manage</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Condition</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>cost-factor">Cost Factor</a></li>
                                <li><a href="<?php echo base_url(); ?>shipping">Shipping</a></li>
                                <li><a href="add-tax">Tax</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Price Model</a>
                            <ul class="collapsed">
                                <li class="collapsible">
                                    <a href="#" class="collapsible plus">Row/Column Price</a>
                                    <ul class="collapsed">
                                        <li><a href="<?php echo base_url(); ?>add-price">Add</a></li>
                                        <li><a href="<?php echo base_url(); ?>manage-price">Manage</a></li>
                                    </ul>
                                </li>
                                <li class="collapsible">
                                    <a href="#" class="collapsible plus">Group</a>
                                    <ul class="collapsed">
                                        <li><a href="<?php echo base_url(); ?>add-group">Add</a></li>
                                        <li><a href="<?php echo base_url(); ?>group-manage">Manage</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
        
                    <h6 id="h-menu-prorules" class=""><a href="#prorules"><span>Production</span></a></h6>
                    <ul id="menu-prorules" class="closed">
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Manufacturer</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>add-manufacturer">Add</a></li>
                                <li><a href="<?php echo base_url(); ?>manage-manufacturer">Manage</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Stock</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>stock-availability">Stock Availability</a></li>
                                <li><a href="<?php echo base_url(); ?>stock-history">Stock History</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Suppliers</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>add-supplier">Suppliers Add</a></li>
                                <li><a href="<?php echo base_url(); ?>supplier-list">Suppliers List</a></li>
                                <li><a href="<?php echo base_url(); ?>supplier-invoice">Manage Invoice</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Return</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>customer-return">Customer Return</a></li>
                                <li><a href="<?php echo base_url(); ?>suppliers-return">Suppliers Return</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>shipping">Shipping</a></li>
                    </ul>
        
                    <h6 id="h-menu-accounts" class=""><a href="#accounts"><span>Account</span></a></h6>
                    <ul id="menu-accounts" class="closed">
                        <li><a href="<?php echo base_url(); ?>b-account-chart">Account Chart</a></li>
                        <li><a href="<?php echo base_url(); ?>purchase-entry">Purchase Entry</a></li>
                        <li><a href="<?php echo base_url(); ?>purchase-list">Purchase List</a></li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Voucher</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>b-debit-voucher">Debit Voucher</a></li>
                                <li><a href="<?php echo base_url(); ?>b-credit-voucher">Credit Voucher</a></li>
                                <li><a href="<?php echo base_url(); ?>b-journal-voucher">Journal Voucher</a></li>
                                <li><a href="<?php echo base_url(); ?>b-contra-voucher">Contra Voucher</a></li>
                                <li><a href="<?php echo base_url(); ?>b-voucher-approval">Voucher Approval</a></li>
                                <li><a href="<?php echo base_url(); ?>b-voucher-reports">Voucher Reports</a></li>
                            </ul>
                        </li>
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Account Reports</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>b-bank-book">Bank Book</a></li>
                                <li><a href="<?php echo base_url(); ?>b-cash-book">Cash Book</a></li>
                                <li><a href="<?php echo base_url(); ?>b-cash-flow">Cash Flow</a></li>
                                <li><a href="<?php echo base_url(); ?>b-general-ledger">General Ledger</a></li>
                                <li><a href="<?php echo base_url(); ?>b-profit-loss">Profit Loss</a></li>
                                <li><a href="<?php echo base_url(); ?>b-trial-ballance">Trial Ballance</a></li>
                            </ul>
                        </li>
                    </ul>
        
                    <h6 id="h-menu-settings" class=""><a href="#settings"><span>Setting</span></a></h6>
                    <ul id="menu-settings" class="closed">
                        <li><a href="<?php echo base_url(); ?>profile-setting">Profile Settings</a></li>
        
        
        
                        <li class="collapsible">
                            <a href="#" class="collapsible plus">Integration</a>
                            <ul class="collapsed">
                                <li><a href="<?php echo base_url(); ?>shipping">Shipping</a></li>
                                <li><a href="<?php echo base_url(); ?>mail">Email</a></li>
                                <li><a href="<?php echo base_url(); ?>sms">SMS</a></li>
                                <li><a href="<?php echo base_url(); ?>gateway">Payment Gateway</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>b-level-users">Users</a></li>
                </ul>
            </div>-->
        <div id="date-picker"></div>
    </div>
    <!-- end content / left -->

