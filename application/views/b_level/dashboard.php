

<style type="text/css">
    .canvasjs-chart-credit {
        display: none;
    }
    .highcharts-credits, .highcharts-button-symbol{
        display: none;
    }
    .progress .progress-value {
        /*top: 0px !important;*/
    }
</style>
<?php // echo $this->uri->segment(1); ?>
<!--<script type="text/javascript">
    $(document).ready(function (){
        $("#en").click(function(){
           var data_lang = $("#en").attr('data-lang');
        });
        $("#ko").click(function(){
           var data_lang = $("#ko").attr('data-lang');
           $('.progress .progress-value').css({'top':'0 !important'});
        });
    });
</script>-->
<!-- content / right -->
<div id="right">
    <div class="box">

        <?php
        $message = $this->session->flashdata('message');
        if ($message)
            echo $message;
        ?>    
        <div class="row my-4">
            <div class="col-md-12">
                <div id="chart_div1"></div>
            </div>
            <!--            <div class="col-md-6">
                            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                        </div>-->
            <!--                        <div class="col-md-6">
                                        <div class="sales">
                                            <div class="legend">
                                                <h6>Units Sold (April 1st to April 15th)</h6>
                                                <ul>
                                                    <li class="monitors">Monitors</li>
                                                    <li class="memory">Memory</li>
                                                </ul>
                                            </div>
                                            <div class="sales1"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="sales">
                                            <div class="legend">
                                                <h6>Units Sold (April 1st to April 15th)</h6>
                                                <ul>
                                                    <li class="monitors">Monitors</li>
                                                    <li class="memory">Memory</li>
                                                </ul>
                                            </div>
                                            <div class="sales2"></div>
                                        </div>
                                    </div>-->
        </div>
        <!-- end box / title -->
        <div class="row my-4 mx-0">
            <div class="col-md-3 col-sm-6">
                <div class="progress_area">
                    <div class="progress">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value"><?php echo $pending_invoice; ?></div>
                    </div>
                    <div class="text_progress">
                        <p>Pending Invoice</p>
                    </div>
                </div>             

            </div>
            <div class="col-md-3 col-sm-6">
                <div class="progress_area">
                    <div class="progress">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value"><?php echo $pending_order; ?></div>
                    </div>
                    <div class="text_progress">
                        <p>Pending Orders</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="progress_area">
                    <div class="progress">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value"><?php echo $total_new_order_count; ?></div>
                    </div>
                    <div class="text_progress">
                        <p>New Orders</p>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-3 col-sm-6">
                <div class="progress_area">
                    <div class="progress">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value"><?php echo $total_payment_done; ?></div>
                    </div>
                    <div class="text_progress">
                        <p>Payments Done</p>
                    </div>
                </div>
            </div> -->

            <div class="col-md-3 col-sm-6">
                <div class="progress_area">
                    <div class="progress">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value"><?php echo @$manufacturing; ?></div>
                    </div>
                    <div class="text_progress">
                        <p>Manufacturing</p>
                    </div>
                </div>
            </div>


        </div>

        <div class="row mx-0">
            <div class="col-md-6 col-lg-12 col-xl-4">
                <div class="row">
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="fa fa-address-book-o icon-part mr-2 align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead"></p>
                                        <p class="text-small">Number of Customer</p>
                                    </div>
                                </div>

                                <div class="progress m-0">
                                    <span class="progress-left">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value"><?php echo $total_customer; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="fa fa-bell icon-part mr-2 align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead"></p>
                                        <p class="text-small">Number of Suppliers</p>
                                    </div>
                                </div>

                                <div class="progress m-0">
                                    <span class="progress-left">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value"><?php echo $total_supplier; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <a href="#" class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="fa fa-database icon-part mr-2 align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead"></p>
                                        <p class="text-small">No of New purchase</p>
                                    </div>
                                </div>

                                <div class="progress m-0">
                                    <span class="progress-left">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value"><?php echo $total_new_purchase; ?></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <a href="#" class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="fa fa-money icon-part mr-2 align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead"></p>
                                        <p class="text-small">Total Commission</p>
                                    </div>
                                </div>

                                <div class="progress m-0">
                                    <span class="progress-left">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value"><?php echo $total_commission; ?></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12 col-xl-8">
                <div id="chart_div" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                <!--                <div class="sales">
                                    <div class="legend">
                                        <h6>Units Sold (April 1st to April 15th) </h6>
                                        <ul>
                                            <li class="monitors">Monitors</li>
                                            <li class="memory">Memory</li>
                                        </ul>
                                    </div>
                                    <div class="sales3"></div>
                                </div>-->
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->
<script>
    window.onload = function () {
//    ========== First chart start ===========
        Highcharts.chart('chart_div1', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: ' Monthly Sales Amount & Order',
                align: 'left'
            },
            xAxis: [{
                    categories: [<?php echo $monthly_sales_month; ?>],
//                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
//                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    crosshair: true
                }],
            yAxis: [{// Primary yAxis
                    labels: {
                        format: '${value}',
                        style: {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    title: {
                        text: 'Sales Amount',
                        style: {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    opposite: true

                }, {// Secondary yAxis
                    gridLineWidth: 0,
                    title: {
                        text: 'Sales Order',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }

                }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
            },
            series: [{
                    name: 'Sales Order',
                    type: 'column',
                    yAxis: 1,
                    data: [<?php echo $monthlysaleorders; ?>],
                    tooltip: {
//                        valuePrefix: '$'
                    }

                }, {
                    name: 'Sales Amount',
                    type: 'spline',
                    data: [<?php echo $monthly_sales_amount; ?>],
                    tooltip: {
//                        valueSuffix: ' °C'
                        valuePrefix: '$'
                    }
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                floating: false,
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 0
                            }
                        }
                    }]
            }
        });
//    ========== First chart Close ===========

//    ========== second chart start ===========
        Highcharts.chart('chart_div', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Total sales in year, month, week, day'
            },
            subtitle: {
//                text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Current Sales Report'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
//                        format: '{point.y:.1f}%'
                        format: '{point.y:.1f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },

            series: [
                {
                    name: "Sales",
                    colorByPoint: true,
                    data: [
                        {
                            name: "Year",
                            y: <?php echo $year_wise_sale; ?>,
                            drilldown: "Year"
                        },
                        {
                            name: "Month",
                            y: <?php echo $month_wise_sale; ?>,
                            drilldown: "Month"
                        },
                        {
                            name: "Week",
                            y: <?php echo $week_wise_sale; ?>,
                            drilldown: "Week"
                        },
                        {
                            name: "Day",
                            y: <?php echo $day_wise_sale; ?>,
                            drilldown: "Day"
                        },
                    ]
                }
            ],
        });
//======== second chart close =============



    }


</script>
<!--<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>-->
<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>-->

<script src="<?php echo base_url(); ?>assets/highcharts/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/series-label.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/export-data.js"></script>
