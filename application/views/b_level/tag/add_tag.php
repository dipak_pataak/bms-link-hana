<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row m-1">
            <h5 class="col-sm-6">Manage Tag</h5>
            <div class="col-sm-6 float-right text-right">
                <!--                <a href="javascript:void" class="btn btn-success mt-1 " id="add_new_color">Add New Color</a>-->
            </div>
        </div>
        <div class="" style="margin: 10px;">
            <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importTag">Bulk Import Upload</button>
            <a href="javascript:void" class="btn btn-success mt-1 " id="add_new_tag" style="margin-top: -8px !important;">Add Tag</a>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="tagkeyup_search()" placeholder="Search..." tabindex="">
                </div>
            </div>          
        </div>
        <div id="results_color">
            <form class="p-3">
                <table class="table table-bordered mb-3">
                    <thead>
                        <tr>
                            <th>Sl no.</th>
                            <th>Tag Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $i = 0 + $pagenum;
                        if (!empty($tags)) {
                            foreach ($tags as $key => $val) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $val->tag_name; ?></td>
                                    <td width="100">
                                        <a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_tag" id="edit_tag" data-data_id="<?= $val->id ?>" ><i class="fa fa-pencil"></i></a>
                                        <a href="<?php echo base_url('b_level/tag_controller/delete_tag/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>

                                <?php
                            }
                        }
                        ?>


                    </tbody>
                    <?php if (empty($tags)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-center text-danger">No record found!</th>
                            </tr> 
                        </tfoot>
                    <?php } ?>
                </table>
                <?php echo $links; ?>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="importTag" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tags Bulk Upload</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <a href="<?php echo base_url('assets/b_level/csv/tag_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                    <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('import-tag-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
               <!--  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->


<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_tag').on('click', function () {
            $('#add_tag')[0].reset();
            $("#add_tag").attr("action", 'b_level/tag_controller/save_tag');
            $('#pattern_id').val('').trigger('change');
            $('#myTag').modal('show');
//            $('.modal-title').text('Add New Color');
        });


        $('.edit_tag').on('click', function () {

            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/tag_controller/get_tag/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_tag')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="tag_name"]').val(data.tag_name);

                    $("#add_tag").attr("action", 'b_level/tag_controller/update_tag')

                    $('#myTag').modal('show');
                    $('.modal-title').text('Update Tag');
                    $('#save').text('Update Tag');


                }, error: function () {

                }
            });
        });
    });
   function tagkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-tag-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
                $("#results_color").html(r);
            }
        });
    }
</script>
<?php
$this->load->view('b_level/tag/tagJs');
?>




