<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .pattern_select .select2-container {
        width: 100% !important;
    }
</style>
<div id="myTag" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Tag</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">



                <?php
                $attributes = array('id' => 'add_tag', 'name' => 'add_tag', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>


                <div class="col-lg-12 px-4">

                    <div class="form-group">
                        <label for="tag_name" class="mb-2">Tag Name</label>
                        <input class="form-control" type="text" name="tag_name" id="tag_name" required>
                         <span id="error"></span>
                    </div>

                </div>
                <input type="hidden" name="id" id="id" value="">


                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Add Tag</button>
                    </div>
                </div>

                <?php echo form_close(); ?>



            </div>

            

        </div>

    </div>
</div>
