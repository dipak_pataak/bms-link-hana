<table class="table table-bordered mb-3">
    <thead>
        <tr>
            <th>Sl No.</th>
            <th>Tag Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        if (!empty($tags)) {
            foreach ($tags as $key => $val) {
                $i++;
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $val->tag_name; ?></td>
                    <td width="100">
                        <a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_tag" id="edit_tag" data-data_id="<?= $val->id ?>" ><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url('b_level/tag_controller/delete_tag/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                    </td>

                </tr>

                <?php
            }
        }
        ?>


    </tbody>
    <?php if (empty($tags)) { ?>
        <tfoot>
            <tr>
                <th colspan="4" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_tag').on('click', function () {
            $('#add_tag')[0].reset();
            $("#add_tag").attr("action", 'b_level/tag_controller/save_tag')
            $('#myTag').modal('show');
//            $('.modal-title').text('Add New Color');
        });


        $('.edit_tag').on('click', function () {

            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/tag_controller/get_tag/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_tag')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="tag_name"]').val(data.tag_name);

                    $("#add_tag").attr("action", 'b_level/tag_controller/update_tag')

                    $('#myColor').modal('show');
                    $('.modal-title').text('Update Color');
                    $('#save').text('Update Color');


                }, error: function () {

                }
            });

        });
//        $('body').on('keyup', '#color_name', function () {
//            var color_name = $("#color_name").val();
//            $.ajax({
//                url: "get-color-check/" + color_name,
//                type: "get",
//                success: function (s) {
//                    if (s != 0) {
//                        $('button[type=submit]').prop('disabled', true);
//                        alert("This color already exists!");
////                        $("#error").html("Its number allready exists!");
//                        $("#error").css({'color': 'red', 'font-weight': 'bold'});
//                        $("#color_name").css({'border': '2px solid red'}).focus();
//                        return false;
//                    } else {
//                        $("#error").hide();
//                        $('button[type=submit]').prop('disabled', false);
//                        $("#color_name").css({'border': '2px solid green'}).focus();
//                    }
//                }
//            });
//        });
//        $('body').on('keyup', '#color_number', function () {
//            var color_number = $("#color_number").val();
//            $.ajax({
//                url: "get-color-check/" + color_number,
//                type: "get",
//                success: function (s) {
//                    if (s != 0) {
//                        $('button[type=submit]').prop('disabled', true);
//                        alert("This color already exists!");
////                        $("#error").html("Its number allready exists!");
//                        $("#error").css({'color': 'red', 'font-weight': 'bold'});
//                        $("#color_number").css({'border': '2px solid red'}).focus();
//                        return false;
//                    } else {
//                        $("#error").hide();
//                        $('button[type=submit]').prop('disabled', false);
//                        $("#color_number").css({'border': '2px solid green'}).focus();
//                    }
//                }
//            });
//        });


    });

</script>


<?php
$this->load->view('b_level/tag/tagJs');
?>