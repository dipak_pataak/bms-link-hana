<style type="text/css">
    .right_side{float: left; font-weight: bold; }
    .left_side{float: right; font-weight: bold; }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    a:hover{
        text-decoration: none;
    }
    .address{
        cursor: pointer;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Product Catalog</h5>
            <div class="col-sm-6 text-right">
                <a href="<?php echo base_url('/catalog-product-add-order/'.$request_id);?>" class="btn btn-success btn-sm mt-1">
                    <i class="fa fa-shopping-cart"></i>
                    New order
                </a>
            </div>
        </div>

        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <p class="px-3">
          <a href="javascript:void(0)" class="btn btn-primary btn-sm mt-1 action-update" style="margin-top: -3px !important;" onClick="return action_approved(document.recordlist)" >
              Approve product
          </a>

        </p>
        <div class="table mt-3" id="result_search">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Shared_catalog_controller/approved_catalog_requested_product') ?>">
                <input type="hidden" name="action">
                <input type="hidden" name="request_id" value="<?php echo $request_id;?>">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                    <tr>
                     <th width="1%"><input type="checkbox" id="SellectAll"/></th>
                        <th width="5%">#</th>
                        <th width="13%">Product name</th>
                        <th width="15%">Category</th>
                        <th width="15%">Price Style Type</th>
                        <th width="15%">Dflt Discnt.(%)</th>
                        <th width="15%">Link product</th>
                        <!--<th width="10%">Status</th>-->
                        <!--<th width="15%" class="text-center">Action</th>-->
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    if(isset($b_user_product_data))
                    {

                        foreach ($b_user_product_data as $value)
                        {
                            $sl++;

                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $value->request_product_id; ?>" <?php if(isset($value->request_approve_status) && $value->request_approve_status == 1) { echo 'checked'; } ?>  class="checkbox_list">
                                </td>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->product_name; ?></td>
                                <td>
                                    <?php
                                    if (empty($value->category_name))
                                    {
                                        echo "None";
                                    }
                                    else
                                    {
                                        echo $value->category_name;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($value->price_style_type == 1) {
                                        echo "Row/Column style";
                                    } elseif ($value->price_style_type == 2) {
                                        echo "Price by Sq.ft style";
                                    } elseif ($value->price_style_type == 4) {
                                        echo "Group Price";
                                    } elseif($value->price_style_type == 3){
                                        echo "Fixed product Price Style";
                                    }
                                    ?>
                                </td>
                                <td><?= $value->dealer_price ?></td>
                                <td>
                                    <select name="product_arr[<?php echo $value->request_product_id;?>]" id="product_arr[]" class="form-control">
                                        <option value="0">Select product</option>
                                        <?php if(isset($product_data) && count($product_data) != 0) { ?>
                                            <?php foreach ($product_data as $key2 => $value2) { ?>
                                                <option <?php if(isset($value->product_link) && $value->product_link == $value2->product_id) { echo 'selected'; } ?> value="<?php echo $value2->product_id;?>"><?php echo $value2->product_name;?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </td>
                              <!--  <td class="text-center">
                                    <a href="<?php /*echo base_url('/catalog-product-add-order/'.$value->request_id.'/'.$value->product_id);*/?>"
                                       class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top"
                                       data-original-title="Order">
                                        <i class="fa fa-shopping-cart"></i>
                                    </a>
                                </td>-->
                            </tr>
                        <?php }
                    }
                    ?>
                    </tbody>

                    <?php if (!isset($product_data) || empty($product_data)) { ?>
                        <tfoot>
                        <tr>
                            <th colspan="8" class="text-center  text-danger">No record found!</th>
                        </tr>
                        </tfoot>
                    <?php } ?>
                </table>
            </form>

        </div>



    </div>
</div>

