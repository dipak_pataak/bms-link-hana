<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<!-- content / right -->
<div id="right">
    <div class="box">
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $message = $this->session->flashdata('message');

            if ($message != '') {
                echo $message;
            }
            if($orderd->level_id!=1){

                $sql = "SELECT * FROM user_info WHERE id = $orderd->b_user_id";
                $cinfo = $this->db->query($sql)->row();



            }
            ?>
        </div>

        <h5>Shipment info</h5>
        <div class="separator mb-3"></div>

        <?=form_open_multipart('b_level/Shared_catalog_controller/shipping',array('id'=>''))?>

        <div class="form-row">

            <div class="form-group col-md-12">
                <div class="row m-0">
                    <label for="order_date" class="col-form-label col-sm-2">Date</label>
                    <div class="col-sm-8">
                        <input type="text" name="order_date" id="order_date" class="form-control" value="<?=$orderd->order_date ?>" readonly>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-12">
                <div class="row m-0">
                    <label for="orderid" class="col-form-label col-sm-2">Order Id</label>
                    <div class="col-sm-8">
                        <p><input type="text" name="orderid" id="orderid" value="<?=$orderd->order_id?>" class="form-control" readonly></p>
                    </div>
                </div>
            </div>



            <?php if($orderd->level_id!=1){ ?>

                <div class="form-group col-md-12">
                    <div class="row m-0">
                        <label for="customer_id" class="col-form-label col-sm-2">B user</label>
                        <div class="col-sm-8" id="custo">
                            <input type="text" name="b_user_name" class="form-control" value="<?=$cinfo->first_name.' '.$cinfo->last_name?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="row m-0">
                        <label for="" class="col-form-label col-sm-2">Phone</label>
                        <div class="col-sm-8" id="custo">
                            <input type="text" name="phone"  class="form-control" value="<?=$phone = ltrim($cinfo->phone,'+1');?> " >
                        </div>
                    </div>
                </div>


                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label for="orderid" class="col-form-label col-sm-2">Shipping Address</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="address"  name="shipping_address" value="<?=$cinfo->address?>, <?=$cinfo->city?> , <?=$cinfo->state?> , <?=$cinfo->zip_code?>, <?=$cinfo->country_code?>" placeholder="" required>
                            <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,US</p>
                        </div>

                    </div>

                </div>

            <?php } else{ ?>

                <div class="form-group col-md-12">
                    <div class="row m-0">
                        <label for="customer_id" class="col-form-label col-sm-2">Customer</label>
                        <div class="col-sm-8" id="custo">
                            <input type="text" name="b_user_name"  class="form-control" value="<?=$orderd->customer_name?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="row m-0">
                        <label for="" class="col-form-label col-sm-2">Phone</label>
                        <div class="col-sm-8" id="custo">
                            <input type="text" name="phone"  class="form-control" value="<?=$phone = ltrim($orderd->phone,'+1 ')?>" readonly>
                        </div>
                    </div>
                </div>

                <?php if($orderd->is_different_shipping!=0){?>




                    <div class="form-group col-md-12">
                        <div class="row m-0 ">
                            <label for="orderid" class="col-form-label col-sm-2">Shipping Address</label>
                            <div class="col-sm-8">
                                <!-- <input type="text" id="mapLocation" name="shippin_address" value="<?=$orderd->different_shipping_address?>" class="form-control" readonly> -->
                                <input type="text" class="form-control" id="address" name="shipping_address" value="<?=$orderd->different_shipping_address?>" placeholder="" required>
                                <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                            </div>
                        </div>
                    </div>

                <?php } else { ?>

                    <div class="form-group col-md-12">
                        <div class="row m-0 ">
                            <label for="orderid" class="col-form-label col-sm-2">Shipping Address</label>

                            <div class="col-sm-8">
                                <!-- <input type="hidden" id="mapLocation" name="shippin_address" value="<?=$orderd->address?>" class="form-control" readonly> -->
                                <!-- <input type="text" id=""  value="<?=$orderd->address?>, <?=$orderd->city?> , <?=$orderd->state?> , <?=$orderd->zip_code?>, <?=$orderd->country_code?>" class="form-control" readonly> -->
                                <input type="text" class="form-control" id="address"  name="shipping_address" value="<?=$orderd->address?>, <?=$orderd->city?> , <?=$orderd->state?> , <?=$orderd->zip_code?>, <?=$orderd->country_code?>" placeholder="" required>
                                <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                            </div>

                        </div>

                    </div>

                <?php }
            }
            ?>

            <div class="form-group col-md-12">
                <div class="row m-0 ">
                    <label for="Length" class="col-form-label col-sm-2">Ship by <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <select class="form-control" name="ship_method" id="ship_method" onchange="setMethod(this.value)" required>
                            <option value="">--Select--</option>
                            <?php foreach($methods as $val){?>
                                <option value="<?=$val->id;?>"><?=$val->method_name;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>

            <div id="up" class="col-md-12" style="display: none;">

                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label for="Length" class="col-form-label col-sm-2">Length <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="number" id="length" name="length" class="form-control" required="">
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label for="weight" class="col-form-label col-sm-2">Weight <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="number" id="weight" name="weight"  class="form-control" required="">
                            <p>Product unit only for Pounds</p>
                        </div>
                    </div>
                </div>

                <?php
                $service = array(
                    '01' => 'UPS Next Day Air',
                    '02' => 'UPS 2nd Day Air',
                    '03' => 'UPS Ground',
                    '07' => 'UPS Worldwide Express',
                    '08' => 'UPS Worldwide Expedited',
                    '11' => 'UPS Standard',
                    '12' => 'UPS 3 Day Select',
                    '13' => 'UPS Next Day Air Saver',
                    '14' => 'UPS Next Day Air Early A.M.',
                    '54' => 'UPS Worldwide Express Plus',
                    '59' => 'UPS 2nd Day Air AM',
                    '65' => 'UPS World Wide Saver'
                );
                ?>

                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label class="col-form-label col-sm-2">Service Type <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control " name="service_type" id="service_type" required>
                                <?php
                                foreach ($service as $key=> $val) {
                                    echo "<option value='".$key."' ".(@$method->service_type==$key?'selected':'')." >$val</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

            </div>


            <div id="dp_cp" class="col-md-12" style="display: none;">

                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label for="delivery_date" class="col-form-label col-sm-2">Date of Delivery <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" id="delivery_date" name="delivery_date" class="form-control datepicker" required="">
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label for="delivery_charge" class="col-form-label col-sm-2">Delivery Charge </label>
                        <div class="col-sm-8">
                            <input type="text" id="delivery_charge" name="delivery_charge" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="row m-0 ">
                        <label for="comment" class="col-form-label col-sm-2">Comment </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group col-md-12">
                <div class="row m-0 ">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary mb-0">Submit</button>
                    </div>

                    <!--                         <div class="col-sm-2">
                            <a href="<?=base_url()?>order-view/<?=$orderd->order_id?>" class="btn btn-warning mb-0">Not required</a>
                        </div> -->
                </div>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>

<script>

    $("form").submit(function(){
        return confirm('Please confirm your action. Shipment process will be done after this stage.');
    });



    function setMethod(v){

        if(v==2 || v==3){

            $("#length").removeAttr('required');
            $("#weight").removeAttr('required');
            $("#delivery_date").prop('required',true);

            $("#dp_cp").slideDown();
            $('#up').slideUp();
        }

        if(v==1){

            $("#length").prop('required',true);
            $("#weight").prop('required',true);
            $("#delivery_date").removeAttr('required');
            $("#up").slideDown();
            $('#dp_cp').slideUp();
        }

    }



    function generate() {
        customerFrm.password.value = randomPassword(customerFrm.length.value);
    }

    //    ============ close generate password =============
    //    =============== its for google place address geocomplete ===============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });
    });




</script>



