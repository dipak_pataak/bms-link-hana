<style type="text/css">
    .right_side{float: left; font-weight: bold; }
    .left_side{float: right; font-weight: bold; }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    a:hover{
        text-decoration: none;
    }
    .address{
        cursor: pointer;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>
        <!-- box / title -->
        <div class="title">
            <h5>Shared catalog users</h5>
        </div>
        <!-- end box / title -->


    <!--    <div class="collapse px-4 mt-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php /*echo base_url(); */?>b-level-customer-filter" method="post" id="customerFilterFrm">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 company_name" placeholder="Company Name" name="company_name" id="company_name">
                            </div><span class="or_cls">-- OR --</span>

                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 phone" placeholder="Phone" name="phone" id="phone">
                            </div><span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 email" placeholder="Email" name="email" id="email">
                            </div><span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 address" placeholder="Address" name="address" id="address">
                            </div>

                            <div class="col-md-2 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>-->

        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
<!--
        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title m-0 p-0 border-0" id="exampleModalPopoversLabel">Appointment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body" id="customer_info">

                    </div>
                </div>
            </div>
        </div>-->


      <!--  <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-6 text-right"></label>
                <div class="col-sm-2" style="margin-left: 35px;">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="customerkeyup_search()" placeholder="Search..." tabindex="">
                </div>
                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>customer-export-csv')" class="dropdown-item">Export to CSV</a></li>
                        <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>customer-export-pdf')"  class="dropdown-item">Export to PDF</a></li>
                        <li><a href="javascript:void(0)" class="dropdown-item action-delete" onClick="return action_delete(document.recordlist)" >Delete</a></li>
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="table mt-3" id="result_search">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Customer_controller/manage_action') ?>">
                <input type="hidden" name="action">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                    <tr>
                     <!--   <th><input type="checkbox" id="SellectAll"/></th>-->
                        <th width="5%">#</th>
                        <th width="13%">User Name</th>
                        <th width="15%">Company</th>
                        <th width="15%">Phone</th>
                        <th width="15%">Email</th>
                        <th width="22%">Address</th>
                        <!--<th width="10%">Status</th>-->
                        <th width="15%" class="text-center">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    if(isset($b_user_list))
                    {
                        foreach ($b_user_list as $value)
                        {
                            $sl++;

                            ?>
                            <tr>

                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->full_name; ?></td>
                                <td>
                                    <a href="javascript:void(0)">
                                        <?php echo $value->company; ?>
                                    </a>
                                </td>


                                <td>
                                    <a href="tel:<?php echo $value->phone; ?>"><?php echo $value->phone; ?></a>
                                </td>
                                <td>
                                    <a href="mailto:<?php echo $value->email; ?>"><?php echo $value->email; ?></a>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" id="address_<?php echo $value->id; ?>"
                                       class="address"
                                      >
                                        <?php echo $value->address . '<br>' . @$value->city . ', ' . @$value->state . ', ' . @$value->zip_code . ', ' . @$value->country_code; ?>
                                    </a>
                                </td>

                                <td class="text-center">
                                    <a href="#"
                                       onclick="show_request_modal('<?php echo $value->id;?>')"
                                       class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top"
                                       data-original-title="Catalog request">
                                        <i class="fa fa-share"></i>
                                    </a>

                                    <?php if(isset($value->request_status) && $value->request_status == 1)
                                    { ?>
                                        <a href="<?php echo base_url('/b-user-catalog-product/'.$value->request_id) ;?>"
                                           class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top"
                                           data-original-title="Approved request">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    <?php } ?>

                                </td>
                            </tr>
                        <?php }
                    }
                    ?>
                    </tbody>

                    <?php if (!isset($b_user_list) || empty($b_user_list)) { ?>
                        <tfoot>
                        <tr>
                            <th colspan="8" class="text-center  text-danger">No record found!</th>
                        </tr>
                        </tfoot>
                    <?php } ?>
                </table>
            </form>
            <?php echo $links; ?>
        </div>


        <div class="modal fade" id="shared_b_user_catalog_modal" role="dialog">

            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Send request for catalog</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <form  action="<?php echo base_url('/b-user-request-for-catalog');?>" method="post">

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label">Remark *</label>
                                <div class="col-xs-6">
                                    <textarea  name="catalog_remark"
                                               id="catalog_remark"
                                               class="form-control"
                                               rows="4"
                                               required=""
                                    ></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="b_user_id" id="b_user_id">


                            <div class="form-group row">
                                <label  class="col-xs-2 control-label"></label>
                                <div class="col-xs-6">
                                    <button class="btn-sm btn-success" id="closeModal">Send request</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
</div>


<!-- end content / right -->
<script>

function show_request_modal(id)
{
    $('#b_user_id').val(id);
    $('#shared_b_user_catalog_modal').modal('show');
}


</script>
