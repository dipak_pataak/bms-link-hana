<?php $currency = $company_profile[0]->currency; ?>


<style type="text/css">
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/b_level/card/card_style.css">

<div id="right">

    <div class="box">

        <div class="row">

            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>

        </div>

        <h5>Order info ||  Make payment</h5>

        <div class="separator mb-3"></div>

        <div class="row">

            <div class="form-group col-md-5" style="margin-left: 20px">

                <table class="table table-bordered mb-4">

                    <tr class="text-center">
                        <td>Order Date</td>
                        <td class="text-right"><?= date_format(date_create($orderd->order_date),'M-d-Y') ?></td>
                    </tr>

                    <tr class="text-center">
                        <td>Order Id</td>
                        <td class="text-right"><?= $orderd->order_id ?></td>
                    </tr>

                    <tr class="text-center">
                        <td>Subtotal</td>
                        <td class="text-right"><?= $currency; ?><?= $orderd->subtotal ?> </td>
                    </tr>

                    <tr class="text-center">
                        <td>Installation Charge</td>
                        <td class="text-right"><?= $currency; ?><?= $orderd->installation_charge ?> </td>
                    </tr>

                    <tr class="text-center">
                        <td>Other Charge</td>
                        <td class="text-right"><?= $currency; ?><?= $orderd->other_charge ?> </td>
                    </tr>

                    <?php if (isset($shipping->track_number) && $shipping->track_number != NULL) { ?>

                        <tr class="text-center">
                            <td>Tracking Number (<?= $shipping->method_name ?>) </td>
                            <td class="text-right"> <?= $shipping->track_number ?> </td>
                        </tr>

                    <?php } ?>

                    <?php if (isset($shipping->shipping_charges) && $shipping->shipping_charges != NULL) { ?>

                        <tr class="text-center">
                            <td>Shipping Charge</td>
                            <td class="text-right"><?= $currency; ?><?= $shipping->shipping_charges ?> </td>
                        </tr>

                    <?php } ?>

                    <tr class="text-center">
                        <td>Misc</td>
                        <td class="text-right"><?= $currency; ?><?= $orderd->misc ?> </td>
                    </tr>

                    <tr class="text-center">
                        <td>Discount</td>
                        <td class="text-right"> <?= $currency; ?><?= $orderd->invoice_discount ?></td>
                    </tr>

                    <tr class="text-center" style="background: #ddd;">

                        <td>Grand Total</td>
                        <td class="text-right" ><?= $currency; ?><?= $orderd->grand_total ?>
                        </td>

                    </tr>

                    <tr class="text-center text-success" >
                        <td>Paid amount</td>
                        <td class="text-right" ><?= $currency; ?><?= @$orderd->paid_amount; ?>
                        </td>
                    </tr>

                    <tr class="text-center text-danger">
                        <td>Due amount</td>
                        <td class="text-right" ><?= $currency; ?><?= @$orderd->due; ?>
                        </td>
                    </tr>

                </table>
            </div>


            <div class="form-group col-md-6">

                <?php
                if ($orderd->due > 0) {
                    ?>

                    <?php echo form_open('b_level/Shared_catalog_controller/make_order_payment_update', array('id'=>'MyForm','target'=>'_blank','onSubmit'=>'return PymentValidation()') ) ?>

                    <table class="table table-bordered ">

                        <?php
                        if($orderd->paid_amount==0){
                            $pval = sprintf ("%.2f", ($orderd->grand_total)/2 );
                        } else{
                            $pval = sprintf ("%.2f", $orderd->due);
                        }
                        ?>

                        <!-- <input type="hidden" value="<?= @$orderd->paid_amount; ?>" name="paid_amount" id="paid_amount"> -->
                        <input type="hidden" value="<?= ($orderd->grand_total) ?>" name="grand_total" id="grand_total">
                        <!-- <input type="hidden" value="<?/*= $orderd->customer_no */?>" name="customer_no" >-->
                        <input type="hidden" value="<?= $orderd->b_user_id ?>" name="b_user_id" id="b_user_id">
                        <input type="hidden" value="<?= $orderd->order_id ?>" name="order_id">
                        <input type="hidden" value="<?= $orderd->due; ?>" id="due_amm">
                        <input type="hidden" value="<?= @$orderd->clevel_order_id; ?>" id="c_level_order_id">

                        <tr>
                            <td>Paid Amount (<?= $currency ?>)</td>
                            <td><input type="text" name="paid_amount" onkeyup="calDuePaid(); isNumeric(this.id);" onchange="calDuePaid()" value="<?=$pval?>" id="paid_amount" placeholder="0" class="form-control text-right" required ></td>
                        </tr>

                        <tr>
                            <td>Due (<?= $currency ?>)</td>
                            <td><input type="text" name="due" id="due" class="form-control text-right" value="0" readonly=""></td>
                        </tr>


                        <tr>
                            <td>Payment method</td>
                            <td>
                                <select name="payment_method" class="form-control" onchange="setCard(this.value)">
                                    <option value="cash">Cash</option>
                                    <option value="card">Card</option>
                                    <option value="check">Check</option>
                                    <option value="paypal">Paypal</option>
                                </select>
                            </td>
                        </tr>

                    </table>

                    <div class="card_method" style="display: none;">

                        <div id="paymentForm">

                            <ul>
                                <li>
                                    <input type="text" name="card_number" placeholder="1234 5678 9012 3456" id="card_number">
                                    <small class="help">This demo supports Visa, Visa Electron, Maestro, MasterCard and Discover.</small>
                                </li>

                                <li class="vertical">

                                    <ul>
                                        <li>
                                            <label for="expiry_month">Expiry month</label>
                                            <input type="text" name="expiry_month" placeholder="MM" maxlength="2" id="expiry_month">
                                        </li>

                                        <li style="margin-left: 5px;">
                                            <label for="expiry_year">Expiry year</label>
                                            <input type="text" name="expiry_year" placeholder="YY" maxlength="2" id="expiry_year">
                                        </li>

                                        <li>
                                            <label for="cvv">CVV</label>
                                            <input type="text" name="cvv"  placeholder="123" maxlength="4" id="cvv">
                                        </li>

                                    </ul>

                                </li>

                                <li>
                                    <label for="name_on_card">Name on card</label>
                                    <input type="text" name="card_holder_name" placeholder="Name on card"  id="name_on_card">
                                </li>
                            </ul>

                        </div>


                        <table class="table table-bordered ">

                        </table>

                    </div>


                    <div class="check_method" style="display: none;">

                        <table class="table table-bordered ">

                            <tr id="check_area">
                                <td>Check Number</td>
                                <td><input type="text" name="check_number" id="check_number" class="form-control "></td>
                            </tr>

                            <tr id="check_area2">
                                <td>Check Image</td>
                                <td><input type="file" name="check_image" id="check_image" class="form-control "></td>
                            </tr>
                        </table>
                    </div>



                    <div class="col-lg-6 offset-lg-6 text-right" style="margin-top: 10px">
                        <button type="submit" class="btn btn-success" >Submit</button>
                    </div>
                    <?php echo form_close(); ?>

                <?php } ?>
            </div>
        </div>


        <h5>Order Details</h5>

        <div class="separator mb-3"></div>

        <div class="px-3" id="cartItems">

            <table class="datatable2 table table-bordered table-hover mb-4">Order Information || Make payment

                <thead>
                <tr>
                    <th>Item.</th>
                    <th>Description</th>
                    <th>Qty</th>
                    <th>Discount</th>
                    <th>Price</th>
                    <th>Notes</th>
                </tr>
                </thead>

                <tbody>

                <?php $sl = 1; ?>
                <?php
                foreach ($order_details as $items):
                    $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                    $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                    ?>

                    <tr>

                        <td><?= $sl++ ?></td>
                        <td>
                            <strong><?= $items->product_name; ?></strong><br>
                            <?= $items->pattern_name; ?><br/>
                            W <?= $items->width; ?> <?= @$width_fraction->fraction_value ?>,
                            H <?= $items->height; ?> <?= @$height_fraction->fraction_value ?>,
                            <?= $items->color_number; ?>
                            <?= $items->color_name; ?>



                        </td>

                        <td><?= $items->product_qty; ?></td>
                        <td><?= $items->discount;?></td>
                        <td><?= $currency; ?> <?= $items->unit_total_price ?> </td>
                        <td><?= $items->notes ?></td>

                    </tr>


                <?php endforeach; ?>

                </tbody>
            </table>

            <?php if (isset($shipping->graphic_image) && $shipping->graphic_image != NULL) { ?>

                <div class="row">

                    <div class="col-lg-6 offset-lg-4">
                        <a href="<?php echo base_url(); ?>order-logo-print/<?php echo $this->uri->segment(2); ?>" target='_new'>
                            <img src="<?php echo base_url() . $shipping->graphic_image ?>" width="" height='250' >
                        </a>
                    </div>


                    <!-- Modal -->
                    <div class="modal fade" id="order_logo" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-body" style="">
                                    <div class="col-md-5" id="printableImgArea">
                                        <img src="<?php echo base_url() . $shipping->graphic_image ?>" class="rotate90" style="-webkit-transform: rotate(90deg); margin-top: 90px; height: 250px;">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a  class="btn btn-warning " href="#" onclick="printDiv('printableImgArea')">Print</a>
                                    <a href="" class="btn btn-danger">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>

        </div>

    </div>
</div>

<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>




<script type="text/javascript">


    $(document).ready(function () {
        var grand_total = parseFloat($('#grand_total').val());
        $('#due').val(grand_total);
        calDuePaid();
    });



    // function calDuePaid() {

    //     var due = parseFloat($('#due').val());
    //     var due_amm = parseFloat($('#due_amm').val());
    //     var paid_amount = parseFloat($('#paid_amount').val());

    //     var pval = '<?=$pval?>';
    //     if(paid_amount<pval){
    //         alert("Can't pay less than "+pval);
    //         $('#paid_amount').val(pval);
    //     }

    //     var pval = '<?=$pval?>';
    //     if(due<pval){
    //         alert("Can't pay more than "+pval);
    //         $('#paid_amount').val(pval);
    //     }

    //     if (isNaN(paid_amount)) {
    //         var paid_amount = 0;
    //     } else {
    //         var paid_amount = parseFloat($('#paid_amount').val());
    //     }

    //     var due = (due_amm - paid_amount);
    //     $('#due').val(due.toFixed(2));

    // }


    function PymentValidation(){

        var grand_total = parseFloat($('#grand_total').val());
        var due_amm = parseFloat($('#due_amm').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var pval = '<?=$pval?>';

        if(grand_total == due_amm){


          /*  if(paid_amount<pval){

                alert("Can't pay less than "+pval);
                $('#paid_amount').val(pval);

                calDuePaid();
                return false;

            }*/
            if(paid_amount>grand_total){
                alert("Can't pay more than "+grand_total);
                $('#paid_amount').val(pval);
                calDuePaid();
                return false;
            }


        } else {

            if(1>paid_amount){
                alert("Can't pay less than 1");
                $('#paid_amount').val(pval);
                calDuePaid();
                return false;
            }

            if(due_amm<paid_amount){
                alert("Can't pay more than "+due_amm);
                $('#paid_amount').val(pval);
                calDuePaid();
                return false;
            }

        }

    }

    // $('#MyForm').on('submit', function() {




    // });




    function calDuePaid(){

        var grand_total = parseFloat($('#grand_total').val());
        var due_amm = parseFloat($('#due_amm').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var pval = '<?=$pval?>';




        if(isNaN(paid_amount)) {
            var paid_amount = 0;
        }else{
            var paid_amount = parseFloat($('#paid_amount').val());
        }

        var due = (due_amm-paid_amount);
        $('#due').val(due.toFixed(2));

    }



    $('#card_number').keyup(function ()
    {
        $(this).val(function (i, v)
        {
            if (v.length > 19) {
                var str = v.substring(0, 19);
                var str = str.replace(/[^\d]/g, '').match(/.{1,4}/g);
                return str ? str.join('-') : '';
            } else {
                var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
                return v ? v.join('-') : '';
            }
        });
    });




    function setCard(value) {

        if (value === 'card') {

            var b_user_id = $('#b_user_id').val();

            var submit_url = "<?php echo base_url(); ?>"+"b_level/setting_controller/getCardInfo/"+b_user_id;

            $.ajax({
                url: submit_url,
                type: 'post',
                success: function (data) {

                    var obj = jQuery.parseJSON(data);

                    $("#card_number").val(obj.card_number);
                    $("#expiry_month").val(obj.expiry_month);
                    $("#expiry_year").val(obj.expiry_year);
                    $("#name_on_card").val(obj.card_holder);

                    $('#card_number').trigger('keyup');

                },error: function() {

                }
            });



            $(".card_method").slideToggle();

            $('#check_number').val('');
            $('#check_image').val('');
            $('.check_method').slideUp();

            $("#MyForm").attr("action", '<?=base_url()?>b_level/receive_payment/tms_payment');



        } else if (value === 'cash') {

            $('#card_number').val('');
            $('#card_holder_name').val('');
            $('#expiry_date').val('');
            $('#cvv').val('');

            $('#check_number').val('');
            $('#check_image').val('');

            $('.card_method').slideUp();
            $('.check_method').slideUp();

            $("#MyForm").attr("action", '<?=base_url()?>b_level/Shared_catalog_controller/make_order_payment_update');

        } else if (value === 'check') {

            $(".check_method").slideToggle();

            $('#card_number').val('');
            $('#card_holder_name').val('');
            $('#expiry_date').val('');
            $('#cvv').val('');

            $('.card_method').slideUp();

            $("#MyForm").attr("action", '<?=base_url()?>b_level/Shared_catalog_controller/make_order_payment_update');

        } else if (value === 'paypal') {

            $('#card_number').val('');
            $('#card_holder_name').val('');
            $('#expiry_date').val('');
            $('#cvv').val('');

            $('#check_number').val('');
            $('#check_image').val('');

            $('.card_method').slideUp();
            $('.check_method').slideUp();
            $("#MyForm").attr("action", '<?=base_url()?>b_level/Shared_catalog_controller/make_order_payment_update');

        }

    }

</script>


<script src="<?php echo base_url(); ?>assets/b_level/card/creditCardValidator.js"></script>

<script>

    function cardFormValidate() {
        var cardValid = 0;

        //card number validation
        $('#card_number').validateCreditCard(function (result) {
            console.log(result);

            var cardType = (result.card_type == null) ? '' : result.card_type.name;
            if (cardType == 'visa') {
                var backPosition = result.valid ? '2px -163px, 260px -87px' : '2px -163px, 260px -61px';
            } else if (cardType == 'visa_electron') {
                var backPosition = result.valid ? '2px -205px, 260px -87px' : '2px -163px, 260px -61px';
            } else if (cardType == 'masterCard') {
                var backPosition = result.valid ? '2px -247px, 260px -87px' : '2px -247px, 260px -61px';
            } else if (cardType == 'maestro') {
                var backPosition = result.valid ? '2px -289px, 260px -87px' : '2px -289px, 260px -61px';
            } else if (cardType == 'discover') {
                var backPosition = result.valid ? '2px -331px, 260px -87px' : '2px -331px, 260px -61px';
            } else if (cardType == 'amex') {
                var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
            } else {
                var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
            }

            $('#card_number').css("background-position", backPosition);
            if (result.valid) {
                $("#card_type").val(cardType);
                $("#card_number").removeClass('required');
                cardValid = 1;
            } else {
                $("#card_type").val('');
                $("#card_number").addClass('required');
                cardValid = 0;
            }
        });

        //card details validation
        var cardName = $("#name_on_card").val();
        var expMonth = $("#expiry_month").val();
        var expYear = $("#expiry_year").val();
        var cvv = $("#cvv").val();
        var regName = /^[a-z ,.'-]+$/i;
        var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
        var regYear = /^19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45$/;
        var regCVV = /^[0-9]{3,4}$/;
        if (cardValid == 0) {
            $("#card_number").addClass('required');
            $("#card_number").focus();
            return false;
        } else if (!regMonth.test(expMonth)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").addClass('required');
            $("#expiry_month").focus();
            return false;
        } else if (!regYear.test(expYear)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").addClass('required');
            $("#expiry_year").focus();
            return false;
        } else if (!regCVV.test(cvv)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").removeClass('required');
            $("#cvv").addClass('required');
            $("#cvv").focus();
            return false;
        } else if (!regName.test(cardName)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").removeClass('required');
            $("#cvv").removeClass('required');
            $("#name_on_card").addClass('required');
            $("#name_on_card").focus();
            return false;
        } else {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").removeClass('required');
            $("#cvv").removeClass('required');
            $("#name_on_card").removeClass('required');
            $("#cardSubmitBtn").removeAttr('disabled');
            return true;
        }
    }


    $(document).ready(function () {




        //Demo card numbers
        $('.card-payment .numbers li').wrapInner('<a href="javascript:void(0);"></a>').click(function (e) {
            e.preventDefault();
            $('.card-payment .numbers').slideUp(100);
            cardFormValidate();
            return $('#card_number').val($(this).text()).trigger('input');
        });

        $('body').click(function () {
            return $('.card-payment .numbers').slideUp(100);
        });

        $('#sample-numbers-trigger').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            return $('.card-payment .numbers').slideDown(100);
        });

        //Card form validation on input fields
        $('#paymentForm input[type=text]').on('keyup', function () {
            cardFormValidate();
        });

        //Submit card form
        $("#cardSubmitBtn").on('click', function () {
            if (cardFormValidate()) {
                var card_number = $('#card_number').val();
                var valid_thru = $('#expiry_month').val() + '/' + $('#expiry_year').val();
                var cvv = $('#cvv').val();
                var card_name = $('#name_on_card').val();
                var cardInfo = '<p>Card Number: <span>' + card_number + '</span></p><p>Valid Thru: <span>' + valid_thru + '</span></p><p>CVV: <span>' + cvv + '</span></p><p>Name on Card: <span>' + card_name + '</span></p><p>Status: <span>VALID</span></p>';
                $('.cardInfo').slideDown('slow');
                $('.cardInfo').html(cardInfo);
            } else {
                $('.cardInfo').slideDown('slow');
                $('.cardInfo').html('<p>Wrong card details given, please try again.</p>');
            }
        });
    });

    function isNumeric(n) {
//alert(n);
        if (isNaN(document.getElementById(n).value)) {
            alert("Please enter numeric values!");
            document.getElementById(n).value = 0;
        }
    }
</script>