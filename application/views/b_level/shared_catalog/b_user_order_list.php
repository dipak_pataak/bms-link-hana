<?php $currency = $company_profile[0]->currency; ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Order List</h5>
            <div class="col-sm-6 text-right">
                <!--<a href="new-order" class="btn btn-success btn-sm mt-1">Add</a>-->
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
         <!--   <a href="new-order" class="btn btn-success btn-sm mt-1" style="margin-top: -3px !important; ">Add</a>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm mt-1 action-delete" style="margin-top: -3px !important;" onClick="return action_delete(document.recordlist)" >Delete</a>
  -->      </p>

        <div class="collapse show px-3 mb-3" id="collapseExample">

            <div class="border p-3">

                <form class="form-horizontal" action="<?= base_url('catalog-b-user-order-list') ?>" method="post">

                    <fieldset>

                        <div class="row">
                           <!-- <div class="col-md-4">
                                <select class="form-control" name="customer_id" >
                                    <option value="">--Select Customer--</option>
                                    <?php /*foreach ($customers as $c) { */?>
                                        <option value="<?/*= $c->customer_id */?>" <?php
/*                                        if ($customerid == $c->customer_id) {
                                            echo 'selected';
                                        }
                                        */?>>
                                            <?/*= $c->first_name; */?> <?/*= $c->last_name; */?>
                                        </option>
                                    <?php /*} */?>
                                </select>
                            </div>-->

                            <div class="col-md-4">
                                <input type="text"  name="order_date" id="order_date" class="form-control datepicker mb-3" value="<?php echo $order_date; ?>" placeholder="Order Date">
                            </div>


                            <div class="col-md-4">
                                <select class="form-control" name="order_stage" >
                                    <option value="">--Select Status--</option>

                                    <option value="1" <?php
                                    if ($order_stage == 1) {
                                        echo 'selected';
                                    }
                                    ?>>Quote/Order</option>

                                    <option value="2" <?php
                                    if ($order_stage == 2) {
                                        echo 'selected';
                                    }
                                    ?>>Paid</option>

                                    <option value="3" <?php
                                    if ($order_stage == 3) {
                                        echo 'selected';
                                    }
                                    ?>>Partially Paid</option>

                                    <option value="4"<?php
                                    if ($order_stage == 4) {
                                        echo 'selected';
                                    }
                                    ?>>Manufacturing</option>


                                    <option value="5" <?php
                                    if ($order_stage == 5) {
                                        echo 'selected';
                                    }
                                    ?>>Shipping</option>

                                    <option value="6" <?php
                                    if ($order_stage == 6) {
                                        echo 'selected';
                                    }
                                    ?>>Cancelled</option>


                                    <option value="7"<?php
                                    if ($order_stage == 7) {
                                        echo 'selected';
                                    }
                                    ?>>Delivered</option>

                                </select>
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>

            </div>

        </div>


        <!-- end box / title -->
        <div class="table-responsive px-3">
            <?= @$links; ?>
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Order_controller/manage_action') ?>">
                <input type="hidden" name="action">
                <table class="table table-bordered table-hover text-center">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th>Order/Quote No</th>
                        <th>B User Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <!-- <th>Price</th> -->
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    if (!empty($orderd)) {
                        foreach ($orderd as $key => $value) {

                            $products = $this->db->where('order_id', $value->order_id)->get('b_to_b_level_qutation_details')->result();
                            $attributes = $this->db->where('order_id', $value->order_id)->get('b_to_b_level_quatation_attributes')->row();
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $value->id; ?>" class="checkbox_list">
                                </td>
                                <td><?= $value->order_id; ?></td>
                                <td>
                                    <?php

                                        echo $value->b_user_name;

                                    ?></td>
                                <td><?= $value->side_mark; ?></td>

                                <td>
                                    <?= date_format(date_create($value->order_date), 'M-d-Y'); ?>
                                </td>
                                <!-- <td><?= $currency ?><?= $value->grand_total ?></td> -->

                                <td>
                                    <?php if($this->permission->check_label('order')->update()->access()){?>
                                        <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                            <option value="">--Select--</option>
                                            <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote/Order</option>
                                            <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                            <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                            <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Manufacturing</option>
                                            <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping</option>
                                            <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
                                            <option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>
                                        </select>
                                    <?php }?>
                                </td>

                                <td class="width_140">
                                    <?php if($this->permission->check_label('order')->read()->access()){?>

                                        <?php if($value->order_stage == 7){ ?>
                                            <!--<a href="customer-order-return/<?/*= $value->order_id */?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>-->
                                        <?php }  if ($value->level_id != 1) { ?>
                                            <a href="<?= base_url('b_level/Shared_catalog_controller/b_user_receipt/') ?><?= $value->order_id ; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url('b_level/Shared_catalog_controller/b_user_receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a>
                                        <?php } ?>

                                    <?php }?>

                                    <?php if($this->permission->check_label('order')->delete()->access()){?>
                                        <!-- <a href="order-edit/<?= $value->order_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a> -->
                                        <a href="<?php echo base_url('b_level/Shared_catalog_controller/b_user_delete_order');?>/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                                    <?php }?>
                                </td>

                            </tr>

                            <?php
                        }
                    } else {
                        ?>

                        <div class="alert alert-danger"> There have no order found..</div>
                    <?php } ?>

                    </tbody>
                </table>
            </form>
            <?= @$links; ?>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">


    function setOrderStage(stage_id, order_id) {


        if(stage_id!==''){


            if(stage_id==='2'|| stage_id==='3'){
                window.location.href = 'b_level/Shared_catalog_controller/payment_b_to_b/'+order_id;
            }

            if(stage_id==='5'){
                window.location.href = 'b_level/Shared_catalog_controller/shipment/'+order_id;
            }


            if(stage_id==='7' || stage_id==='6'||stage_id==='4' || stage_id==='1'){

                //alert(stage_id);

                $.ajax({
                    url: "b_level/Shared_catalog_controller/set_order_stage/" + stage_id + "/" + order_id,
                    type: 'GET',
                    success: function (r) {

                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function () {
                            window.location.href = window.location.href;
                        }, 2000);
                    }
                });
            }

        }

    }


</script>