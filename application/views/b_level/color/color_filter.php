<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row m-1">
            <h5 class="col-sm-6">Manage Color</h5>
            <div class="col-sm-6 float-right text-right">
                <!--                <a href="javascript:void" class="btn btn-success mt-1 " id="add_new_color">Add New Color</a>-->
            </div>
        </div>
        <div class="" style="margin: 10px;">
<!--            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="margin-top: -5px;">
                Filter
            </button>-->
            <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importColor">Bulk Import Upload</button>
            <a href="javascript:void" class="btn btn-success mt-1 " id="add_new_color" style="margin-top: -8px !important;">Add Color</a>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div class="collapsexxx px-4 mt-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>b-level-color-filter" method="post" id="customerFilterFrm">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 colorname" placeholder="Colors Name" name="colorname" id="colorname" value="<?php echo $colorname; ?>">
                            </div><span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 colornumber" placeholder="Colors Number" name="colornumber" id="colornumber" value="<?php echo $colornumber; ?>">
                            </div>
                            <div class="col-md-2 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
        <form class="p-3">
            <table class="table table-bordered mb-3">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>Color Name</th>
                        <th>Color Number</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $i = 1;
                    if (!empty($get_color_filter)) {
                        foreach ($get_color_filter as $key => $val) {
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val->color_name; ?></td>
                                <td><?= $val->color_number; ?></td>

                                <td width="100">
                                    <a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="<?= $val->id ?>" ><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url('b_level/color_controller/delete_color/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>

                            <?php
                        }
                    }
                    ?>


                </tbody>
                <?php if (empty($get_color_filter)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="4" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>
        </form>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="importColor" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Colors Bulk Upload</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <a href="<?php echo base_url('assets/b_level/csv/color_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                    <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('import-color-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->


<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_color').on('click', function () {
            $('#add_color')[0].reset();
            $("#add_color").attr("action", 'b_level/color_controller/save_color')
            $('#myColor').modal('show');
//            $('.modal-title').text('Add New Color');
        });


        $('.edit_color').on('click', function () {

            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/color_controller/get_color/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_color')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="color_name"]').val(data.color_name);
                    $('[name="color_number"]').val(data.color_number);

                    $("#add_color").attr("action", 'b_level/color_controller/update_color')

                    $('#myColor').modal('show');
                    $('.modal-title').text('Update Color');
                    $('#save').text('Update Color');


                }, error: function () {

                }
            });

        });
        $('body').on('keyup', '#color_name', function () {
            var color_name = $("#color_name").val();
            $.ajax({
                url: "get-color-check/" + color_name,
                type: "get",
                success: function (s) {
                    if (s != 0) {
                        $('button[type=submit]').prop('disabled', true);
                        alert("This color already exists!");
//                        $("#error").html("Its number allready exists!");
                        $("#error").css({'color': 'red', 'font-weight': 'bold'});
                        $("#color_name").css({'border': '2px solid red'}).focus();
                        return false;
                    } else {
                        $("#error").hide();
                        $('button[type=submit]').prop('disabled', false);
                        $("#color_name").css({'border': '2px solid green'}).focus();
                    }
                }
            });
        });
        $('body').on('keyup', '#color_number', function () {
            var color_number = $("#color_number").val();
            $.ajax({
                url: "get-color-check/" + color_number,
                type: "get",
                success: function (s) {
                    if (s != 0) {
                        $('button[type=submit]').prop('disabled', true);
                        alert("This color already exists!");
//                        $("#error").html("Its number allready exists!");
                        $("#error").css({'color': 'red', 'font-weight': 'bold'});
                        $("#color_number").css({'border': '2px solid red'}).focus();
                        return false;
                    } else {
                        $("#error").hide();
                        $('button[type=submit]').prop('disabled', false);
                        $("#color_number").css({'border': '2px solid green'}).focus();
                    }
                }
            });
        });


    });


</script>


<?php
$this->load->view('b_level/color/colorJs');
?>




