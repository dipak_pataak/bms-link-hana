<table class="table table-bordered mb-3">
    <thead>
        <tr>
            <th>Sl No.</th>
            <th>Color Name</th>
            <th>Color Number</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        if (!empty($colors)) {
            foreach ($colors as $key => $val) {
                $i++;
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $val->color_name; ?></td>
                    <td><?= $val->color_number; ?></td>

                    <td width="100">
                        <a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="<?= $val->id ?>" ><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url('b_level/color_controller/delete_color/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                    </td>

                </tr>

                <?php
            }
        }
        ?>


    </tbody>
    <?php if (empty($colors)) { ?>
        <tfoot>
            <tr>
                <th colspan="4" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_color').on('click', function () {
            $('#add_color')[0].reset();
            $("#add_color").attr("action", 'b_level/color_controller/save_color')
            $('#myColor').modal('show');
//            $('.modal-title').text('Add New Color');
        });


        $('.edit_color').on('click', function () {

            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/color_controller/get_color/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_color')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="color_name"]').val(data.color_name);
                    $('[name="color_number"]').val(data.color_number);

                    $("#add_color").attr("action", 'b_level/color_controller/update_color')

                    $('#myColor').modal('show');
                    $('.modal-title').text('Update Color');
                    $('#save').text('Update Color');


                }, error: function () {

                }
            });

        });
//        $('body').on('keyup', '#color_name', function () {
//            var color_name = $("#color_name").val();
//            $.ajax({
//                url: "get-color-check/" + color_name,
//                type: "get",
//                success: function (s) {
//                    if (s != 0) {
//                        $('button[type=submit]').prop('disabled', true);
//                        alert("This color already exists!");
////                        $("#error").html("Its number allready exists!");
//                        $("#error").css({'color': 'red', 'font-weight': 'bold'});
//                        $("#color_name").css({'border': '2px solid red'}).focus();
//                        return false;
//                    } else {
//                        $("#error").hide();
//                        $('button[type=submit]').prop('disabled', false);
//                        $("#color_name").css({'border': '2px solid green'}).focus();
//                    }
//                }
//            });
//        });
//        $('body').on('keyup', '#color_number', function () {
//            var color_number = $("#color_number").val();
//            $.ajax({
//                url: "get-color-check/" + color_number,
//                type: "get",
//                success: function (s) {
//                    if (s != 0) {
//                        $('button[type=submit]').prop('disabled', true);
//                        alert("This color already exists!");
////                        $("#error").html("Its number allready exists!");
//                        $("#error").css({'color': 'red', 'font-weight': 'bold'});
//                        $("#color_number").css({'border': '2px solid red'}).focus();
//                        return false;
//                    } else {
//                        $("#error").hide();
//                        $('button[type=submit]').prop('disabled', false);
//                        $("#color_number").css({'border': '2px solid green'}).focus();
//                    }
//                }
//            });
//        });


    });

</script>


<?php
$this->load->view('b_level/color/colorJs');
?>