<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .pattern_select .select2-container {
        width: 100% !important;
    }
</style>
<div id="myColor" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Color</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">



                <?php
                $attributes = array('id' => 'add_color', 'name' => 'add_color', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>


                <div class="col-lg-12 px-4">

                    <div class="form-group">
                        <label for="color_name" class="mb-2">Color Name</label>
                        <input class="form-control" type="text" name="color_name" id="color_name" required>
                         <span id="error"></span>
                    </div>

                    <div class="form-group">
                        <label for="color_number" class="mb-2">Color Number</label>
                        <input class="form-control" type="text" name="color_number" id="color_number">
                    </div>

                    <div class="form-group pattern_select">
                        <label for="pattern_id" class="mb-2">Pattern</label>
                        <select class="form-control select2" name="pattern_id" id="pattern_id" data-placeholder="-- select pattern --" required>
                            <option value="">-- select pattern --</option>';
                            <?php
                                foreach ($patterns as $pattern) { ?>
                                    <option value="<?=$pattern->pattern_model_id?>"><?=$pattern->pattern_name ?></option>
                            <?php } ?>
                        </select>
                        <span id="pattern_error"></span>
                    </div>

                </div>
                <input type="hidden" name="id" id="id" value="">


                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Add Color</button>
                    </div>
                </div>

                <?php echo form_close(); ?>



            </div>

            

        </div>

    </div>
</div>
