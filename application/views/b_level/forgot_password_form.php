<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BMS Link</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <!-- stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/style.css" media="screen" />

    </head>
    <body>
        <div id="header-outer">
            <div id="logo" style="margin: 24px 0px 0px 36px;">
                <h1><a href="<?php echo base_url(); ?>b-level-dashboard" title="BMS Link"><img src="<?php echo base_url(); ?>assets/b_level/resources/images/logo.png" alt="BMS Link" /></a></h1>
            </div>
        </div>
        <div id="login">
            <?php
            $exception = $this->session->flashdata('exception');
            if ($exception)
                echo $exception;
            ?>

            <?php
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>

            <!-- login -->
            <div class="title">
                <h5>Forgot Password</h5>
                <div class="corner tl"></div>
                <div class="corner tr"></div>
            </div>



            <div class="inner">
                <form action="<?php echo base_url(); ?>b-level-forgot-password-send" method="post">
                    <div class="form">
                        <!-- fields -->
                        <div class="fields">
                            <div class="field">
                                <div class="label">
                                    <label for="email">E-mail:</label>
                                </div>
                                <div class="input">
                                    <input type="email" id="email" name="email" size="40" autocomplete="off" class="focus" placeholder="Enter email address" tabindex="1" />
                                </div>
                            </div>
                            <div class="field">
                                <div class="label">
                                    <!--<label for="password">Password:</label>-->
                                </div>
                                <div class="input">
                                    <!--<input type="password" id="password" name="password" size="40" class="focus" />-->
                                </div>
                            </div>
                            <div class="buttons">
                                <a href="<?php echo base_url(); ?>b-level" class="btn btn-primary" tabindex="3">Back</a>
                                <input type="submit" class="btn btn-success text-right"  tabindex="2" value="Send">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>

        <!-- scripts (jquery) -->
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-ui-1.8.16.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#email").focus();
                var info = $('table tbody tr');
                info.click(function () {
                    var email = $(this).children().first().text();
                    var password = $(this).children().first().next().text();
                    $("input[name=email]").val(email);
                    $("input[name=password]").val(password);
                });
            });
        </script>

    </body>
</html>