<div id="right">
    <div class="box">

        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="separator mb-3"></div>
        <div class="px-3">
            <?php echo form_open('Paypal/buy', array('class' => 'row', 'id' => '')); ?>
            <table class="datatable2 table table-bordered table-hover mb-4">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name of Product Include Specifications</th>
                        <th>Qty</th>
                        <th>List Amount</th>
                        <th>Discount Amount </th>
                        <th>Price</th>
                        <th>Comments</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Wooden Blind - 2X2 1/2 - White - other Attributes</td>
                        <td>15</td>
                        <td>$220.00</td>
                        <td>$154.00</td>
                        <td>$66.00</td>
                        <td>special notes</td>
                        <td>
                            <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
<!--                    <tr>
                        <td>2</td>
                        <td>Wooden Blind - 4X2 1/2 - White - other Attributes</td>
                        <td>13</td>
                        <td>$330.00</td>
                        <td>$20.00</td>
                        <td>$310.00</td>
                        <td>special notes</td>
                        <td>
                            <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>-->
                </tbody>
            </table>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 offset-lg-8">
                        <table class="table table-bordered mb-4">
                            <tr>
                                <td>Sub Total</td>
                                <td>$376.00</td>
                            </tr>
                            <tr>
                                <td>State Tax</td>
                                <td>$7.00</td>
                            </tr>
                            <tr>
                                <td>Installation Charge</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Other Charge</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Discount</td>
                                <td>$10.00</td>
                            </tr>
                            <tr>
                                <td>Misc</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Grand Total</td>
                                <td>$373.00</td>
                            </tr>
                        </table>
                    </div>
                    <div class="">
                        <input type="hidden" name="customer_id" value="13">
                        <input type="hidden" name="order_id" value="14">
                        <input type="hidden" name="quantity" value="5">
                        <input type="hidden" name="price" value="10">
                    </div>
                    <div class="col-lg-3 offset-lg-3 text-right">
                        <button type="submit" class="btn btn-success">Paypal Submit</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>

        </div>
    </div>
</div>