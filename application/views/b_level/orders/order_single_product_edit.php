<?php $currency = @$company_profile[0]->currency; ?>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <?= form_open('b_level/order_controller/product_order_update', array('id' => 'AddToCart', 'name' => 'cform')) ?>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <div class="title row">
            <h5 class="col-sm-6">Order Update</h5>
        </div>
        <div class="row sticky_container m-0" id="row">
            <div class="col-sm-9">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <?php
//                            dd($get_product_order_info);
                            ?>
                            <label for="" class="col-sm-3">Select Category</label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="category_id" id="category_id" data-placeholder='-- select one --'>
                                    <option value=""></option>
                                    <?php foreach ($get_category as $category) { ?>
                                        <option value='<?php echo $category->category_id; ?>' <?php
                                        if ($get_product_order_info[0]->category_id == $category->category_id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                                    <?php echo $category->category_name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>

                    </div>

                    <div class="form-group col-md-12" id="subcategory_id">

                    </div>

                    <div class="form-group col-md-12">
                        <div class="row">
                            <label for="" class="col-sm-3">Select Product</label>
                            <div class="col-sm-6">
                                <?php
                                $catewise_product = $this->db->select('a.product_id, a.product_name')->from('product_tbl a')
                                                ->where('a.category_id', $get_product_order_info[0]->category_id)->get()->result();
//                                dd($catewise_product);
                                ?>
                                <select class="form-control select2" name="product_id" id="product_id" onchange="getAttribute(this.value)" data-placeholder="-- select one --">
                                    <option value=""></option>
                                    <?php foreach ($catewise_product as $product) { ?>
                                        <option value="<?php echo $product->product_id; ?>" <?php
                                        if ($get_product_order_info[0]->product_id == $product->product_id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                                    <?php echo $product->product_name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12" id="color_model">

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-sm-3">Pattern</label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="pattern_model_id" data-placeholder="-- select pattern/model --">
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_patterns as $pattern) {
                                            ?>
                                            <option value=<?php echo $pattern->pattern_model_id; ?>''>
                                                <?php echo $pattern->pattern_name; ?>
                                            </option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="" class="col-sm-3">Color</label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="color_id" id="color_id" data-placeholder='-- select one --'>
                                        <option value=""></option>
                                        <?php foreach ($get_colors as $color) { ?>
                                            <option value='<?php echo $color->id; ?>' <?php
                                            if ($get_product_order_info[0]->color_id == $color->id) {
                                                echo 'selected';
                                            }
                                            ?>>
                                                        <?php echo $color->color_name; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!--                         <div class="form-group col-md-12">
                                                <div class="row">
                                                    <label for="" class="col-sm-3">Select Pattern/Model</label>
                                                    <div class="col-sm-6">
                                                        <select class="form-control select2" name="pattern_model_id" data-placeholder="-- select pattern/model --">
                                                            <option value=""></option>
                    <?php
                    foreach ($get_patern_model as $pattern) {
                        echo "<option value=$pattern->pattern_model_id''>$pattern->pattern_name</option>";
                    }
                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="form-group col-md-12">
                                                <div class="row">
                                                    <label for="" class="col-sm-3">Color</label>
                                                    <div class="col-sm-6">
                                                        <select class="form-control select2" name="color_id" id="color_id" data-placeholder='-- select one --'>
                                                            <option value=""></option>
                    <?php
                    foreach ($colors as $color) {
                        echo "<option value='$color->id'>$color->color_name</option>";
                    }
                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->


                    <div class="form-group col-md-12">
                        <div class="row">
                            <label for="" class="col-sm-3">Width</label>
                            <div class="col-sm-4">
                                <input type="text" name="width" class="form-control" id="width" value="<?php echo $get_product_order_info[0]->width; ?>" onChange="loadPStyle()" onKeyup="loadPStyle()" >
                            </div>

                            <div class="col-sm-2">
                                <select class="form-control select2" name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                    <option value="">--Select one--</option>
                                    <?php foreach ($fractions as $f) { ?>
                                        <option value='<?php echo $f->id; ?>' <?php
                                        if ($get_product_order_info[0]->width_fraction_id == $f->id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                                    <?php echo $f->fraction_value; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="row">
                            <label class="col-sm-3">Height</label>
                            <div class="col-sm-4">
                                <input type="text" name="height" class="form-control" id="height" value="<?php echo $get_product_order_info[0]->height; ?>" onChange="loadPStyle()" onKeyup="loadPStyle()" >
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                    <option value="">--Select one--</option>
                                    <?php foreach ($fractions as $f) { ?>
                                        <option value='<?php echo $f->id; ?>' <?php
                                        if ($get_product_order_info[0]->height_fraction_id == $f->id) {
                                            echo 'selected';
                                        }
                                        ?>><?php echo $f->fraction_value; ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                    </div>

                    <div class="form-group col-md-12" id="attr">
                        <?php
//                        dd($attributes);
                        if ($attributes) {
                            foreach ($attributes as $attribute) {

                                if ($attribute->attribute_type == 3) {

                                    $options = $this->db->select('attr_options.*,product_attr_option.id')
                                                    ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                                                    ->where('product_attr_option.pro_attr_id', $attribute->id)
                                                    ->order_by('attr_options.att_op_id', 'ASC')
                                                    ->get('product_attr_option')->result();
                                    ?>
                                    <div class="row">
                                        <label class="col-sm-3"><?php echo $attribute->attribute_name; ?></label>

                                        <input type="hidden" name="attribute_id[]" value="<?php echo $attribute->attribute_id; ?>">
                                        <input type="hidden" name="attribute_value[]" value="">';
                                        <?php
                                        foreach ($options as $op) {
                                            ?>
                                            <div class="col-sm-3">
                                                <label><?php echo $op->option_name; ?></label>
                                                <input type="hidden" name="op_id_<?php echo $attribute->attribute_id; ?>[]" value="<?php echo $op->id . '_' . $op->att_op_id; ?>">';
                                                <input type="text" name=" []" min="0"  class="form-control">
                                            </div>
                                        <?php } ?>
                                        <div class="col-sm-6" style="display:none;"></div>
                                        <div class="col-sm-4" style="margin-top:-7px;"></div>

                                    </div><br>
                                    <?php
                                } elseif ($attribute->attribute_type == 2) {
                                    $options = $this->db->select('attr_options.*,product_attr_option.id')
                                                    ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                                                    ->where('product_attr_option.pro_attr_id', $attribute->id)
                                                    ->order_by('attr_options.att_op_id', 'ASC')
                                                    ->get('product_attr_option')->result();
                                    ?>
                                    <div class="row">
                                        <label class="col-sm-3"><?php echo $attribute->attribute_name; ?></label>
                                        <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                                        <input type="hidden" name="attribute_value[]" value="">

                                        <div class="col-sm-3">
                                            <select class="form-control select2 options_<?php echo $attribute->attribute_id; ?>" name="op_id_<?php echo $attribute->attribute_id; ?>"  onChange="OptionOptions(this.value, '<?php echo $attribute->attribute_id; ?>')" data-placeholder="-- select pattern/model --">
                                                <option value="0_0" >--Select one--</option>
                                                <?php foreach ($options as $op) { ?>
                                                    <option value="<?php echo $op->id; ?>_<?php echo $op->att_op_id; ?>">
                                                        <?php echo $op->option_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-6" style="display:none;"></div>
                                        <div class="col-sm-4" style="margin-top:-7px;"></div>

                                    </div>
                                    <br>
                                    <?php
                                } elseif ($attribute->attribute_type == 1) {
                                    $height = $attribute->attribute_name;
                                    ?>
                                    <div class="row">
                                        <lable class="col-sm-3"><?php echo $attribute->attribute_name; ?></lable>
                                        <input type="hidden" name="attribute_id[]" value="<?php echo $attribute->attribute_id; ?>">
                                        <div class="col-sm-6">
                                            <input type="text" name="attribute_value[]" id="<?php echo $attribute->attribute_name; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <?php
                                } else {
                                    
                                }
                            }
                        }
                        ?>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="row">
                            <lable class="col-sm-3">Note</lable>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="notes" rows="6"><?php echo $get_product_order_info[0]->notes; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-lg-12 offset-lg-2">
                            <table class="table table-bordered mb-4">
                                <tr>
                                    <td>Sub Total (<?= $currency ?>)</td>
                                    <td><input type="number" name="subtotal" id="subtotal" value="<?php echo @$get_quatation_info[0]->subtotal; ?>" readonly="" class="form-control text-right"></td>
                                </tr>

                                <tr>
                                    <td>Installation Charge (<?= $currency ?>)</td>
                                    <td><input type="number" name="install_charge" onchange="calculetsPrice()" onclick="calculetsPrice()" value="<?php echo @$get_quatation_info[0]->installation_charge; ?>" id="install_charge" class="form-control text-right"></td>
                                </tr>

                                <tr>
                                    <td>Other Charge (<?= $currency ?>)</td>
                                    <td><input type="number" name="other_charge" onchange="calculetsPrice()" onclick="calculetsPrice()" value="<?php echo @$get_quatation_info[0]->other_charge; ?>" id="other_charge" class="form-control text-right"></td>
                                </tr>

                                <tr>
                                    <td>Misc (<?= $currency ?>)</td>
                                    <td><input type="number" name="misc" onchange="calculetsPrice()" onclick="calculetsPrice()" value="<?php echo @$get_quatation_info[0]->misc; ?>" id="misc" class="form-control text-right"></td>
                                </tr>

                                <tr>
                                    <td>Discount (<?= $currency ?>)</td>
                                    <td><input type="number" name="invoice_discount" onchange="calculetsPrice()" onclick="calculetsPrice()" value="<?php echo @$get_quatation_info[0]->invoice_discount; ?>" id="invoice_discount" class="form-control text-right"></td>
                                </tr>

                                <tr>
                                    <td>Grand Total (<?= $currency ?>)</td>
                                    <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly=""></td>
                                </tr>
                            </table>

                        </div>

                        <input type="hidden" name="order_status" id="order_status">
                        <div class="col-lg-6 offset-lg-6 text-right">
                            <!--<button type="submit" class="btn btn-success" id="gq">Submit</button>-->
                        </div>
                    </div>

                </div>

            </div>

            <input type="hidden" name="total_price" id="total_price">

            <div class="col-sm-3 sticky_item">

                <div id="fist1"></div>
                <div id="fist2"></div>

                <div class="fixed_item">
                    <p id="tprice" style="position: relative; top: 160px;">Total Price = <?= $currency; ?> 0</p>
                </div>
            </div>

        </div>

    </div>

    <?= form_close(); ?>

</div>



<!-- end content / right -->
<script type="text/javascript">



    // function OptionOptionsOption(pro_att_op_id, attribute_id){


    //     var op_op_id = pro_att_op_id.split("_")[0];

    //     var op_op_op_id = pro_att_op_id.split("_")[1];

    //     var main_p = parseFloat($('#main_price').val());

    //     if(isNaN(main_p)){
    //         var main_price = 0;
    //     }else{
    //         var main_price = main_p;
    //     }

    //     if (op_op_id) {

    //         var wrapper = $(".op_" + op_op_id).parent().next().next();

    //         $.ajax({
    //             url: "b_level/order_controller/get_product_attr_op_op_op/" + op_op_op_id + "/" + attribute_id+"/"+main_price,
    //             type: 'get',
    //             success: function (r) {

    //                 $(wrapper).html(r);
    //             }
    //         });

    //     }

    //     cal();

    // }


    function OptionFive(att_op_op_op_op_id, attribute_id) {

        var op_op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var op_op_op_id = att_op_op_op_op_id.split("_")[1];


        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_op_id) {

            var wrapper = $("#op_op_op_" + op_op_op_id).next().next();
            $.ajax({
                url: "b_level/order_controller/get_product_attr_op_five/" + op_op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }


    function OptionOptionsOptionOption(pro_att_op_id, attribute_id) {

        var op_op_op_id = pro_att_op_id.split("_")[0];
        var op_op_id = pro_att_op_id.split("_")[1];
        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_id) {

            var wrapper = $("#op_op_" + op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op_op/'); ?>" + op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }


    function OptionOptionsOption(pro_att_op_id, attribute_id) {


        var op_op_id = pro_att_op_id.split("_")[0];

        var id = pro_att_op_id.split("_")[1];

        var op_id = pro_att_op_id.split("_")[2];

        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {

            var main_price = 0;

        } else {

            var main_price = main_p;

        }

        if (op_op_id) {

            var wrapper = $("#op_" + op_id).parent().next().next();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op/'); ?>" + op_op_id + "/" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });

        }

        cal();

    }



    function OptionOptions(pro_att_op_id, attribute_id) {
//        alert(attribute_id);
        if (pro_att_op_id) {
            //id
            var id = pro_att_op_id.split("_")[0];
            //option_id
            var optin_id = pro_att_op_id.split("_")[1];
            //mainprice
            var main_p = parseFloat($('#main_price').val());
            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var wrapper = $(".options_" + attribute_id).parent().next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/Invoice_receipt/get_product_attr_option_option/'); ?>" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                }
            });

        } else {

        }

        cal();

    }


    $('body').on('click', '#row', function () {
        cal();

        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);

    });


    function cal() {

        var unit_price = 0;
        $(".contri_price").each(function () {
            isNaN(this.value) || 0 == this.value.length || (unit_price += parseFloat(this.value))
        });

        var main_price = parseFloat($('#main_price').val());

        var total_price = (unit_price + main_price);
        var t = (isNaN(total_price) ? 0 : total_price);
        $("#total_price").val(t);
        $("#tprice").text("Total Price = $" + t);

    }



    //width fraction get 
    $('#width').on('keyup', function () {

        var hif = $(this).val().split(".")[1];

        if (hif) {
            $.ajax({
                url: "b_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {

                    $("#width_fraction_id ").val(r);

                }
            });
        }

    });
    //--end---


    // Height fraction 
    $('#height').on('keyup', function () {
        var hif = $(this).val().split(".")[1];
        if (hif) {
            $.ajax({
                url: "b_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id ").val(r);
                }
            });
        }
        ;
    });
    //---End----



    // $('#width_fraction_id').on('change',function(){

    //     var wif = ($("#width_fraction_id :selected").text().split("/")[0]/$("#width_fraction_id :selected").text().split("/")[1]);

    //     var width = parseFloat($('#width').val())+(isNaN(wif)?0:wif);

    //     $('#width').val((isNaN(width)?0:width));

    // });


    function loadPStyle() {

        var product_id = $('#product_id').val();

        var pattern_id = $('#pattern_id').val();

        var pricestyle = $('#pricestyle').val();

        var price = $("#height").parent().parent().parent().next();

        var hif = ($("#height_fraction_id :selected").text().split("/")[0] / $("#height_fraction_id :selected").text().split("/")[1]);

        var wif = ($("#width_fraction_id :selected").text().split("/")[0] / $("#width_fraction_id :selected").text().split("/")[1]);

        var width = parseFloat($('#width').val()) + (isNaN(wif) ? 0 : wif);

        var height = parseFloat($('#height').val()) + (isNaN(hif) ? 0 : hif);

        if (pricestyle !== '2') {

            $.ajax({

                url: "<?php echo base_url('b_level/order_controller/get_product_row_col_price/'); ?>" + height + "/" + width + "/" + product_id + "/" + pattern_id,

                type: 'get',

                success: function (r) {

                    var obj = jQuery.parseJSON(r);
                    $(price).html(obj.ht);

                    if (isNaN(parseFloat(obj.prince))) {

                        $("#tprice").text("Total Price = $0");

                    } else {

                        $("#tprice").text("Total Price = $" + parseFloat(obj.prince));

                    }

                    // $("#height").val(obj.col);
                    if (obj.st === 1) {

                        $('#cartbtn').removeAttr('disabled');

                    } else if (obj.st === 2) {
                        //$('#cartbtn').prop('disabled', true);
                    }
                }
            });

        }

        if (pricestyle === '2') {

            var main_p = parseFloat($('#sqr_price').val());

            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var sum = (width * height) / 144;
            var price = main_price * sum;

            $('#main_price').val(price.toFixed(2));

        }
        //---End--


        cal();

    }






    $(document).ready(function () {


        $('body').on('change', '#product_id', function () {

            var product_id = $(this).val();
            alert(product_id);
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_to_attribute/'); ?>" + product_id,
                type: 'get',
                success: function (r) {

                    $("#attr").html(r);
                    $('#cartbtn').removeAttr('disabled');

                }
            });

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_color_partan_model/'); ?>" + product_id,
                type: 'get',
                success: function (r) {

                    $('#color_model').html(r);
                }
            });
        });



        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });



        //        ============= its for order id generator =============
        $.ajax({
            url: "order-id-generate",
            type: 'get',
            success: function (r) {
                $("#orderid").val(r);
            }
        });


        //        ================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            //            alert(category_id);
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/category_wise_subcategory/'); ?>" + category_id,
                type: 'get',
                success: function (r) {
                    console.log(r);
                    $("#subcategory_id").html(r);
                }
            });
        });


        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_by_category/'); ?>" + category_id,
                type: 'get',
                success: function (r) {

                    $("#product_id").html(r);

                }
            });
        });


        $('body').on('change', '#sub_category_id', function () {
            var sub_category_id = $(this).val();
            //            alert(sub_category_id);
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_by_subcategory/'); ?>" + sub_category_id,
                type: 'get',
                success: function (r) {
                    $("#product_id").html(r);
                }
            });
        });


        //        ================= its for customer wise sidemark ==========



        $("body").on('change', '#customer_id', function () {

            var customer_id = $(this).val();

            $.ajax({

                url: 'customer-wise-sidemark/' + customer_id,
                type: 'get',
                success: function (data) {

                    if (data == 0) {

                        $("#side_mark").val("None");

                    } else {

                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate != null ? obj.tax_rate : 0);

                        $('#side_mark').val(obj.side_mark);
                        $('#tax').val(tax);

                        if (obj.side_mark) {
                            $('#side_mark').val(obj.side_mark);
                        }

                        $('#customertype').val(obj.customer_type);

                        customerWiseComission();
                    }
                }

            });



        });


        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);
        });

    });



    // submit form and add data
    $("#AddToCart").on('submit', function (e) {

        e.preventDefault();

        var submit_url = "b_level/order_controller/add_to_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                toastr.success('Success! - Product add to cart ');
                setTimeout(function () {
                    $("#cartItems").load(location.href + " #cartItems>*", "");
                }, 2000);



            }, error: function () {
                alert('error');
            }
        });
    });



    // submit form and add data
    $("#clearCart").on('click', function (e) {

        e.preventDefault();

        var submit_url = "<?= base_url(); ?>b_level/order_controller/clear_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                alert('error');
            }
        });
    });


    function deleteCartItem(id) {
        var submit_url = "<?= base_url(); ?>b_level/order_controller/delete_cart_item/" + id;
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                alert('error');
            }
        });

    }




    function customerWiseComission() {

        var customertype = $('#customertype').val();

        if (customertype == '') {
            alert('Please select customer');
            $("#customer_id").focus();

        } else {

            var i = 1;

            $(".product_id").each(function () {

                var productid = (this.value);
                var customer_id = $(this).val();
                var submit_url = "b_level/order_controller/getproductcomission/" + productid + '/' + customer_id;

                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function (res) {

                        var obj = jQuery.parseJSON(res);
                        var qty = $('#qty_' + i).val();
                        var list_price = parseFloat($('#list_price_' + i).val());

                        if (customertype === 'business') {

                            $('#discount_' + i).val(obj.dealer_price);
                            var discount = (list_price * obj.dealer_price) / 100;

                        } else {

                            $('#discount_' + i).val(obj.individual_price);
                            var discount = (list_price * obj.individual_price) / 100;

                        }

                        tdiscount = discount * qty;

                        $('#utprice_' + i).val((list_price * qty) - tdiscount);

                        calculetsPrice();

                        i++;
                    }, error: function () {
                        alert('error');
                    }
                });

                //i++;
            });
        }




    }

    function calculetsPrice() {

        var subtotal = 0;
        $(".utprice").each(function () {
            isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
        });

        $('#subtotal').val(subtotal);

        //var taxs = parseFloat($('#tax').val());
        //var tax = (subtotal*taxs)/100;
        var install_charge = parseFloat($('#install_charge').val());
        var other_charge = parseFloat($('#other_charge').val());
        var invoice_discount = parseFloat($('#invoice_discount').val());
        var misc = parseFloat($('#misc').val());
        var grandtotal = (subtotal + install_charge + other_charge + misc);
        $('#grand_total').val(grandtotal - invoice_discount);
        calDuePaid();
    }

    function calDuePaid() {

        var grand_total = parseFloat($('#grand_total').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var due = (grand_total - paid_amount);
        $('#due').val(due);

    }

</script>

<?php $this->load->view($customerjs) ?>