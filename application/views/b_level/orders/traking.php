
<!-- content / right -->
    <div id="right">
        <div class="box">
            <!-- end box / title -->     
            <div class="p-1">
                <?php
                $message = $this->session->flashdata('message');
                
                if ($message != '') {
                    echo $message;
                }
                ?>
            </div>

            <h5>Order traking</h5>

            <div class="separator mb-3"></div>

            <div class="form-row">
                
                <?=form_open('b_level/tracking/track',array('id' => 'TrakIng' ))?>
                

                    <div class=" col-md-12 text-center">
                        <h1 class="text-uppercase d-block">Order Traking Number (zin1/zmp1-XXXXXXXX)</h1>
                     

                        <div class="form-group row">
                            <label for="order_date" class="col-form-label col-sm-2"></label>
                                <div class="col-sm-8 ">
                                    <input type="text" name="trackNumber" class="form-control" placeholder="Enter your order track number">
                                </div>
                        </div>

                        <button type="submit" class="btn btn-primary btn-lg mb-5"  role="button">Check Status</button>

                        <p>Knowing your order status has never been this easy Just enter your order number to get your details immediately. Check Status Now.</p>
                    </div>

                <?php echo form_close(); ?>

            </div>

    </div>

</div>



