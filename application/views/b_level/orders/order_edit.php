<?php $currency = $this->session->userdata('currency');?>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->



        <div class="box">
            <!-- box / title -->
           <!--  <div class="title row">
                <h5 class="col-sm-6">Order New</h5>
            </div> -->
            <!-- end box / title -->
            <h5>Customer info</h5>
            <div class="separator mb-3"></div>
            
            <?=form_open_multipart('b_level/order_controller/save_order',array('id'=>''))?>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                        <div class="col-sm-8">
                            <p><input type="text" name="orderid" id="orderid" value="" class="form-control" readonly></p>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="order_date" class="col-form-label col-sm-4">Date</label>
                        <div class="col-sm-8">
                            <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6" >
                    <div class="row m-0">
                        <label for="customer_id" class="col-form-label col-sm-4">Select /Add Customer</label>
                        <div class="col-sm-8" id="custo">
                            <!--<input type="text" class="form-control">-->
                            <select class="form-control select2" name="customer_id" id="customer_id" required="" data-placeholder="--select one --">
                                <option value=""></option>
                                <?php
                                foreach ($get_customer as $customer) {
                                    echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" name="customertype" id="customertype" value="">

                            <a href="javascript:void(0)" onclick="AddNewCustomer()">Add New Customer</a>

                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="side_mark" class="col-form-label col-sm-4">Side Mark</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="side_mark" name="side_mark" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="row m-0 ship_addr" style="display: none;">
                        <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                        <div class="col-sm-8">
                            <input type="text" id="mapLocation" name="shippin_address" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group col-lg-6">
                    <div class="row m-0">
                        <div class="col-sm-8 offset-sm-4 mb-2">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="different_address" value="1" id="shipaddress">
                                <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control" name="file_upload" id="file_upload">
                            <p>Extension: PDF & DOC. File size: 2MB</p>
                        </div>
                    </div>
                </div>

            </div>
   

        
        <h5>Order Details</h5>
        <div class="separator mb-3"></div>
        <div class="px-3" id="cartItems">
            
            <table class="datatable2 table table-bordered table-hover mb-4">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name of Product Include Specifications</th>
                        <th>Qty</th>
                        <th>List Amount(<?=$currency?>)</th>
                        <th>Discount Amount(<?=$currency?>) </th>
                        <th>Price (<?=$currency?>)</th>
                        <th>Comments</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php $i = 1; ?>
                    <?php foreach ($this->cart->contents() as $items): ?>

                    
                    <input type="hidden" name="attributes[]" value='<?php echo $items['att_options'];?>'>
                    <input type="hidden" name="category_id[]" value='<?php echo $items['category_id'];?>'>
                    <input type="hidden" name="pattern_model_id[]" value='<?php echo $items['pattern_model_id'];?>'>
                    <input type="hidden" name="color_id[]" value='<?php echo $items['color_id'];?>'>
                    <input type="hidden" name="width[]" value='<?php echo $items['width'];?>'>
                    <input type="hidden" name="height[]" value='<?php echo $items['height'];?>'>
                    <input type="hidden" name="notes[]" value='<?php echo $items['notes'];?>'>

                    <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                    <tr>
                        <td><?=$i?></td>

                        <td>
                            <?=$items['name']?>
                            <input type="hidden" name="product_id[]" id="product_id_<?=$i?>" class="product_id" value="<?=$items['product_id']?>">
                        </td>

                        <td><input type="number" name="qty[]" onchange="customerWiseComission()" onclick="customerWiseComission()" value="<?=$items['qty']?>" id="qty_<?=$i;?>" class="form-control text-right"></td>
                        <td>  <input type="number" name="list_price[]" value="<?=$items['price']?>" id="list_price_<?=$i;?>" readonly="" class="form-control text-right"></td>
                        <td> <input type="number" name="discount[]" value="" readonly="" id="discount_<?=$i?>" class="form-control text-right"></td>
                        <td><input type="number" name="utprice[]" value="" id="utprice_<?=$i;?>" class="form-control utprice text-right" readonly="" ></td>
                        <td><?=$items['notes'];?></td>
                        
                        <td>
                            <a href="javascript:void(0)" onclick="deleteCartItem('<?=$items['rowid']?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>

                <?php $i++; ?>
            <?php endforeach; ?>

                    
                </tbody>
            </table>


            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <table class="table table-bordered mb-4">

                        <tr>
                            <td>Sub Total (<?=$currency?>)</td>
                            <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>State Tax (<?=$currency?>)</td>
                            <td><input type="number" name="tax" onchange="calculetsPrice()" onclick="calculetsPrice()" id="tax" value="0" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Installation Charge (<?=$currency?>)</td>
                            <td><input type="number" name="install_charge" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" id="install_charge" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Other Charge (<?=$currency?>)</td>
                            <td><input type="number" name="other_charge" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" id="other_charge" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Misc (<?=$currency?>)</td>
                            <td><input type="number" name="misc" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" id="misc" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Discount (<?=$currency?>)</td>
                            <td><input type="number" name="invoice_discount" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" id="invoice_discount" class="form-control text-right"></td>
                        </tr>
                        
                        <tr>
                            <td>Grand Total (<?=$currency?>)</td>
                            <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly=""></td>
                        </tr>
                    </table>
                </div>

                <input type="hidden" name="order_status" id="order_status">

                <div class="col-lg-6 offset-lg-6 text-right">

                    <button type="submit" class="btn btn-success" id="gq">Generate Quote</button>
                    <button type="submit" class="btn btn-warning" id="gqi" >Generate Quote + Invoice</button>
                    <button type="button" class="btn btn-danger btn-sm" id="clearCart"> Clear All</button>
                
                </div>

            </div>

        </div>
            <?=form_close()?>
    </div>
</div>



<!-- end content / right -->
<script type="text/javascript">

function OptionOptions(pro_att_op_id,attribute_id){

    //id
    var id = pro_att_op_id.split("_")[0];
    //option_id
    var optin_id = pro_att_op_id.split("_")[1];
    //mainprice
    var main_price = $('#main_price').val();

    var price = $(".options_"+attribute_id).parent().next();

    $.ajax({
        url: "b_level/order_controller/get_product_attr_option_option_price/"+optin_id+"/"+main_price,
        type: 'get',
        success: function (r) {
           $(price).html(r);
        }
    });



    var wrapper = $(".options_"+attribute_id).parent().next().next();

    $.ajax({
        url: "b_level/order_controller/get_product_attr_option_option/" + id+"/"+attribute_id,
        type: 'get',
        success: function (r) {
           $(wrapper).html(r);
        }
    });


    var unit_price = 0;
    $(".contri_price").each(function() {
        isNaN(this.value) || 0 == this.value.length || (unit_price += parseFloat(this.value))
    });

    var main_price = parseFloat($('#main_price').val());
    var total_price = (unit_price+main_price);


    $("#total_price").val(total_price);
    $("#tprice").text("Total Price = "+total_price);



      
}



function loadPStyle(value){

    var height = $('#height').val();
    var width = $('#width').val();
    var product_id = $('#product_id').val();
    var price = $("#height").parent().parent().parent().next();

    $.ajax({
        url: "b_level/order_controller/get_product_row_col_price/"+height+"/"+width+"/"+product_id,
        type: 'get',
        success: function (r) {
           $(price).html(r);
        }
    });

 
      
}



    $(document).ready(function () {


        $('body').on('change', '#product_id', function () {

            var product_id = $(this).val();
            $.ajax({
                url: "b_level/order_controller/get_product_to_attribute/" + product_id,
                type: 'get',
                success: function (r) {
                    $("#attr").html(r);
                    loadPStyle();
                }
            });
        });



        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });

//        ============= its for order id generator =============
        $.ajax({
            url: "order-id-generate",
            type: 'get',
            success: function (r) {

                $("#orderid").val(r);
            }
        });

        //        ================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    $("#subcategory_id").html(r);
                }
            });
        });


        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();

            $.ajax({
                url: "b_level/order_controller/get_product_by_category/" + category_id,
                type: 'get',
                success: function (r) {
                    $("#product_id").html(r);
                }
            });
            
        });


//        ================= its for customer wise sidemark ==========
        
     

        $("body").on('change', '#customer_id', function () {


            var customer_id = $(this).val();
            $.ajax({
                url: 'customer-wise-sidemark/' + customer_id,
                type: 'get',
                success: function (data) {
                    if (data == 0) {
                        $("#side_mark").val("None");
                    } else {
                        var obj = jQuery.parseJSON(data);
                        $('#side_mark').val(obj.side_mark);
                        if (obj.side_mark) {
                            $('#side_mark').val(obj.side_mark);
                        }

                        $('#customertype').val(obj.customer_type);

                        customerWiseComission();
                    }
                }
            });



        });


        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);   
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);   
        });

    });



    // submit form and add data
    $("#AddToCart").on('submit',function(e){

        e.preventDefault();

        var submit_url = "b_level/order_controller/add_to_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function(res) {
                $("#cartItems").load(location.href+" #cartItems>*",""); 
            },error: function() {
                alert('error');
            }
        });
    });



    // submit form and add data
    $("#clearCart").on('click',function(e){

        e.preventDefault();

        var submit_url = "<?=base_url();?>b_level/order_controller/clear_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function(res) {
         
            $("#cartItems").load(location.href+" #cartItems>*",""); 
                
            },error: function() {
                alert('error');
            }
        });
    });


    function deleteCartItem(id){

        var submit_url = "<?=base_url();?>b_level/order_controller/delete_cart_item/"+id;
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function(res) {
         
            $("#cartItems").load(location.href+" #cartItems>*",""); 
                
            },error: function() {
                alert('error');
            }
        });

    }





    function customerWiseComission(){

    var customertype = $('#customertype').val();

    if(customertype==''){
        alert('Please select customer');
        $( "#customer_id" ).focus();
    }else{

            var i=1;

            $(".product_id").each(function() {
              
                var productid = (this.value);
                var submit_url = "b_level/order_controller/getproductcomission/"+productid;

                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function(res) {

                        var obj = jQuery.parseJSON(res);
                        var qty = $('#qty_'+i).val();
                        var list_price = parseFloat($('#list_price_'+i).val());

                        if(customertype==='business'){
                            $('#discount_'+i).val(obj.dealer_price);
                            var discount = (list_price*obj.dealer_price)/100;

                        } else {
                            $('#discount_'+i).val(obj.individual_price);
                            var discount = (list_price*obj.individual_price)/100;
                        }

                        tdiscount = discount*qty;

                        $('#utprice_'+i).val(list_price-tdiscount);

                        calculetsPrice();
                        
                    i++;     
                    },error: function() {
                        alert('error');
                    }
                });

                //i++;
            });
        }




    }

       function calculetsPrice(){

            var subtotal=0;
            $(".utprice").each(function() {
                isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
            });

            $('#subtotal').val(subtotal);

            var taxs = parseFloat($('#tax').val());
            var tax = (subtotal*taxs)/100;
            var install_charge = parseFloat($('#install_charge').val());
            var other_charge = parseFloat($('#other_charge').val());
            var invoice_discount = parseFloat($('#invoice_discount').val());
            var misc = parseFloat($('#misc').val());
            var grandtotal = (subtotal+tax+install_charge+other_charge+misc);
            $('#grand_total').val(grandtotal-invoice_discount);

        }


    // submit form and add data
    $("#save_order").on('submit',function(e){

        e.preventDefault();

        var order_status = $(this).val();
        var order_status = $('#order_status').val(order_status);

        var submit_url = "b_level/order_controller/save_order";
        // $.ajax({
        //     type: 'POST',
        //     url: submit_url,
        //     data: $(this).serialize(),
        //     success: function(res) {
        //         $("#cartItems").load(location.href+" #cartItems>*",""); 
        //     },error: function() {
        //         alert('error');
        //     }
        // });
    });



</script>


<?php $this->load->view($customerjs)?>