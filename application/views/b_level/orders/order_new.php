<?php $currency = @$company_profile[0]->currency; ?>
<style type="text/css">
    .clr_btn{
        position: relative;
        right: -306px;
        top: -33px;
    }

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0; 
}
  


/*--- for b-level --*/

.col-sm-6 + .col-sm-4 {
    flex: 0 0 100%;
  max-width: 100%;
  clear: both;
  float: none;
}
.col-sm-4 .col-sm-12 {
  clear: both;
}
.col-sm-4 .col-sm-12 label, .col-sm-4 .col-sm-12 .form-control {
   width: 18.5%;
   float: left;
   margin: 5px 15px;
}

</style>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <?= form_open('b_level/order_controller/add_to_cart', array('id' => 'AddToCart','name'=>'cform')) ?>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <div class="title row">
            <h5 class="col-sm-6">Order New</h5>
        </div>
        <h5>Add Product to cart</h5>

        <div class="separator mb-3"></div>

        <div class="row  m-0 sticky_container" id="row">

            <div class="col-sm-9">

                <div class="form-row">

                    <div class="form-group col-md-12">

                        <div class="row">

                            <label for="" class="col-sm-3">Select Category</label>

                            <div class="col-sm-6">

                                <select class="form-control select2" name="category_id" id="category_id" data-placeholder='-- select one --'>

                                    <option value=""></option>
                                    
                                    <?php
                                        foreach ($get_category as $category) {
                                            echo "<option value='$category->category_id'>$category->category_name</option>";
                                        }
                                    ?>
                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="form-group col-md-12" id="subcategory_id">

                    </div>

                    <div class="form-group col-md-12">
                        <div class="row">
                            <label for="" class="col-sm-3">Select Product</label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="product_id" id="product_id" onchange="getAttribute(this.value)" data-placeholder="-- select one --">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12" id="color_model"></div>
                    
                    <div class="form-group col-md-12" id="pattern_color_model"></div>


                    <div class="form-group col-md-12">

                        <div class="row">

                            <label for="" class="col-sm-3">Width</label>
                            
                            <div class="col-sm-4">
                                <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()" onKeyup="loadPStyle()" required="">
                            </div>

                            <div class="col-sm-2">
                                <select class="form-control" name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --' data-placeholder="-- select one --">
                                    <option value="">-- select one --</option>
                                    <?php
                                    foreach ($fractions as $f) {
                                        echo "<option value='$f->id'>$f->fraction_value</option>";
                                    }
                                    ?>

                                </select>
                                
                            </div>

                        </div>

                    </div>

                    <div class="form-group col-md-12">
                        
                        <div class="row">
                            <label class="col-sm-3">Height</label>

                            <div class="col-sm-4">
                                <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()" onKeyup="loadPStyle()" required="">
                            </div>

                            <div class="col-sm-2">

                                <select class="form-control" name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                    
                                    <option value="">-- select one --</option>

                                        <?php
                                            foreach ($fractions as $f) {
                                                echo "<option value='$f->id'>$f->fraction_value</option>";
                                            }
                                        ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="form-group col-md-12"></div>

                    <div class="form-group col-md-12" id="attr"></div>

                    <div class="form-group col-md-12">
                        <div class="row">
                            <label class="col-sm-3">Room</label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="room" id="room"  data-placeholder='-- select one --'>
                                    <option value="">-- select one --</option>
                                    <?php
                                    foreach ($rooms as $r) {
                                        echo "<option value='$r->room_name'>$r->room_name</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group col-md-12">
                        <div class="row">
                            <lable class="col-sm-3">Note</lable>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="notes" rows="6"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-9 mb-0">
                        <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn" >Add product to list</button>
                    </div>

                </div>

            <input type="hidden" name="total_price" id="total_price">
            </div>

            
            <div class="col-sm-3 sticky_item">

                <div id="fist1"></div>
                <div id="fist2"></div>

                <div class="fixed_item">
                    <p id="tprice" style="border:1px solid #ddd; padding: 20px;margin-right: 5px;">Total Price = <?= $currency; ?> 0</p>
                </div>
                
            </div>

        </div>

    </div>

    <?= form_close(); ?>



    <div class="box">
        <!-- box / title -->
        <!--  <div class="title row">
             <h5 class="col-sm-6">Order New</h5>
         </div> -->
        <!-- end box / title -->
        <h5>Customer info</h5>
        <div class="separator mb-3"></div>

        <?= form_open_multipart('b_level/order_controller/save_order', array('id' => '')) ?>

        <div class="form-row">

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                    <div class="col-sm-8">
                        <p><input type="text" name="orderid" id="orderid" value="" class="form-control" readonly></p>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="order_date" class="col-form-label col-sm-4">Date</label>
                    <div class="col-sm-8">
                        <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6" >
                <div class="row m-0">
                    <label for="customer_id" class="col-form-label col-sm-4">Select /Add Customer</label>
                    <div class="col-sm-8" id="custo">
                        <!--<input type="text" class="form-control">-->
                        <select class="form-control select2" name="customer_id" id="customer_id" required="" data-placeholder="--select one --">
                            <option value=""></option>
                            <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" name="customertype" id="customertype" value="">
                        <!--<a href="<?= base_url('add-b-customer') ?>">Add New Customer</a>-->
                        <!--<a href="javascript:void(0)" id="openDialog">Window Popup</a>-->

                        <!--<a href="javascript:void(0)" onclick="AddNewCustomer()">Add New Customer</a>--> 
                        <a href="javascript:void(0)" onclick="new_customer_modal()">Add New</a> 
                    </div>
                </div>
            </div>

            <div class="modal fade" id="new_customer_modal_info" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Customer Information</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" id="new_customer_info"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="side_mark" class="col-form-label col-sm-4">Side Mark</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="side_mark" name="side_mark" readonly>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0 ship_addr" style="display: none;">
                    <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                    <div class="col-sm-8">
                        <input type="text" id="mapLocation" name="shippin_address" class="form-control">
                        <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,US</p>
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <div class="row m-0">
                    <div class="col-sm-8 offset-sm-4 mb-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="different_address" value="1" id="shipaddress">
                            <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="file_upload" id="file_upload">
                        <p>Extension:JPG/PNG PDF & DOC. File size: 2MB</p>
                        <img src="<?php echo base_url(); ?>assets/profile-pic.png" class="img-thumbnail" width="100" height="50" id="prevImg">
                    </div>
                </div>
            </div>

        </div>



        <h5>Order Details</h5>
        <div class="separator mb-3"></div>

        <div class="px-3" id="cartItems">

            <table class="datatable2 table table-bordered table-hover mb-4">

                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Name of Product Include Specifications</th>
                        <th>Qty</th>
                        <th>List Amount(<?= $currency ?>)</th>
                        <th>Discount Amount(%) </th>
                        <th>Price (<?= $currency ?>)</th>
                        <th>Comments</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    <?php $i = 1; ?>

                    <?php foreach ($this->cart->contents() as $items): ?>

                    <input type="hidden" name="attributes[]" value='<?php echo $items['att_options']; ?>'>
                    <input type="hidden" name="category_id[]" value='<?php echo $items['category_id']; ?>'>
                    <input type="hidden" name="pattern_model_id[]" value='<?php echo $items['pattern_model_id']; ?>'>
                    <input type="hidden" name="color_id[]" value='<?php echo $items['color_id']; ?>'>
                    <input type="hidden" name="width[]" value='<?php echo $items['width']; ?>'>
                    <input type="hidden" name="height[]" value='<?php echo $items['height']; ?>'>
                    <input type="hidden" name="notes[]" value='<?php echo $items['notes']; ?>'>
                    <input type="hidden" name="height_fraction_id[]" value='<?php echo $items['height_fraction_id']; ?>'>
                    <input type="hidden" name="width_fraction_id[]" value='<?php echo $items['width_fraction_id']; ?>'>

                    <input type="hidden" name="row_status[]" value="<?= $items['row_status'] ?>">
                    <input type="hidden" name="room[]" value="<?= $items['room'] ?>">

                    <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>

                    <tr>
                        <td><?= $i ?></td>

                        <td>
                            <?= $items['name'] ?>
                            <input type="hidden" name="product_id[]" id="product_id_<?= $i ?>" class="product_id" value="<?= $items['product_id'] ?>">
                        </td>

                        <td style="width: 150px;">
                            <div id="field1">
                                <button type="button" id="sub" class="sub" >-</button>
                                <!-- <input type="number" name="qty[]" onchange="customerWiseComission()" onclick="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" min="1" class="form-control text-right"> -->
                                <input type="number" name="qty[]"  onchange="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" min="1" class="qty_input text-center" style="width: 40px;">
                                <button type="button" id="add" class="add" >+</button>
                            </div>
                            <input type="hidden" value="<?= $i ?>">
                        </td>

                        <!-- <td><input type="number" name="qty[]" onchange="customerWiseComission()" onclick="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" class="form-control text-right"></td> -->
                        <td>  <input type="number" name="list_price[]" value="<?= $items['price'] ?>" id="list_price_<?= $i; ?>" readonly="" class="form-control text-right"></td>
                        <td> <input type="number" name="discount[]" value="" readonly="" id="discount_<?= $i ?>" class="form-control text-right"></td>
                        <td><input type="number" name="utprice[]" value="" id="utprice_<?= $i; ?>" class="form-control utprice text-right" readonly="" ></td>
                        <td><?= $items['notes']; ?></td>

                        <td>
                            <a href="javascript:void(0)" onclick="deleteCartItem('<?= $items['rowid'] ?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>

                    <?php $i++; ?>

                <?php endforeach; ?>

                </tbody>

            </table>


            <div class="row">

                <div class="col-lg-6 offset-lg-6">

                    <table class="table table-bordered mb-4">

                        <tr>
                            <td>Sub Total (<?= $currency ?>)</td>
                            <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                        </tr>

                        <!-- <tr>
                            <td>Installation Charge (<?= $currency ?>)</td>
                            <td><input type="number" name="install_charge" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" min="0"  step="0.05" id="install_charge" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Other Charge (<?= $currency ?>)</td>
                            <td><input type="number" name="other_charge" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" min="0"  step="0.05" id="other_charge" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Misc (<?= $currency ?>)</td>
                            <td><input type="number" name="misc" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" min="0"  step="0.05" id="misc" class="form-control text-right"></td>
                        </tr> -->

                        <tr>
                            <td>Discount (<?= $currency ?>)</td>
                            <td><input type="number" name="invoice_discount" onchange="calculetsPrice()" onclick="calculetsPrice()" value="0" min="0"  step="0.05" id="invoice_discount" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Grand Total (<?= $currency ?>)</td>
                            <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly=""></td>
                        </tr>

                      
                    </table>

                </div>



                <input type="hidden" name="order_status" id="order_status">

                <div class="col-lg-6 offset-lg-6 text-right">

                    <button type="submit" class="btn btn-success" id="gq">Submit</button>
                    <!-- <button type="submit" class="btn btn-warning" id="gqi" >Generate Quote + Invoice</button> -->
                    <a href="<?= base_url(); ?>b_level/order_controller/clear_cart" class="btn btn-danger btn-sm" id="clearCart"> Clear All</a>

                </div>

            </div>

        </div>
        <?= form_close() ?>
    </div>
</div>



<!-- end content / right -->
<script type="text/javascript">



// $('#install_charge').on('keyup', function () {
//     var value = $(this).val();
//     var att             = value.split(".")[1];
//     if (att.length > 2 ){
//         $(this).val(parseFloat(value).toFixed(2));
//     }
// })
    
// $('#other_charge').on('keyup', function () {
//     var value = $(this).val();
//     var att             = value.split(".")[1];
//     if (att.length > 2 ){
//         $(this).val(parseFloat(value).toFixed(2));
//     }
// })

// $('#misc').on('keyup', function () {
//     var value = $(this).val();
//     var att             = value.split(".")[1];
//     if (att.length > 2 ){
//         $(this).val(parseFloat(value).toFixed(2));
//     }
// })
    
// $('#invoice_discount').on('keyup', function () {
//     var value = $(this).val();
//     var att             = value.split(".")[1];
//     if (att.length > 2 ){
//         $(this).val(parseFloat(value).toFixed(2));
//     }
// })



    // function OptionOptionsOption(pro_att_op_id, attribute_id){


    //     var op_op_id = pro_att_op_id.split("_")[0];

    //     var op_op_op_id = pro_att_op_id.split("_")[1];

    //     var main_p = parseFloat($('#main_price').val());

    //     if(isNaN(main_p)){
    //         var main_price = 0;
    //     }else{
    //         var main_price = main_p;
    //     }

    //     if (op_op_id) {

    //         var wrapper = $(".op_" + op_op_id).parent().next().next();

    //         $.ajax({
    //             url: "b_level/order_controller/get_product_attr_op_op_op/" + op_op_op_id + "/" + attribute_id+"/"+main_price,
    //             type: 'get',
    //             success: function (r) {
                    
    //                 $(wrapper).html(r);
    //             }
    //         });

    //     }

    //     cal();

    // }


    function multiOptionPriceValue(att_op_op_op_op_id){

        var op_op_op_id     = att_op_op_op_op_id.split("_")[0];
        var att             = att_op_op_op_op_id.split("_")[1];
        var op_op_id        = att_op_op_op_op_id.split("_")[2];
        var main_p          = parseFloat($('#main_price').val());

        if(isNaN(main_p)){
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_id) {

            var wrapper = $("#mul_op_op_id" + op_op_id).parent().next();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/multioption_price_value/')?>" + op_op_op_id + "/" + att + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }

        cal();
    }



    function OptionFive(att_op_op_op_op_id, attribute_id){

        var op_op_op_op_id    = att_op_op_op_op_id.split("_")[0];
        var op_op_op_id    = att_op_op_op_op_id.split("_")[1];
        var main_p      = parseFloat($('#main_price').val());

        if(isNaN(main_p)){
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_op_id) {

            var wrapper = $("#op_op_op_" + op_op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_five/')?>" + op_op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }


    function OptionOptionsOptionOption(pro_att_op_id, attribute_id){

        var op_op_op_id    = pro_att_op_id.split("_")[0];
        var op_op_id = pro_att_op_id.split("_")[1];
        var main_p      = parseFloat($('#main_price').val());

        if(isNaN(main_p)){
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_id) {

            var wrapper = $("#op_op_" + op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op_op/')?>" + op_op_op_id + "/" + attribute_id+"/"+main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }


    function OptionOptionsOption(pro_att_op_id, attribute_id){


        var op_op_id    = pro_att_op_id.split("_")[0];

        var id = pro_att_op_id.split("_")[1];

        var op_id = pro_att_op_id.split("_")[2];

        var main_p      = parseFloat($('#main_price').val());

        if(isNaN(main_p)){

            var main_price = 0;

        } else {

            var main_price = main_p;

        }

        if (op_op_id) {

            var wrapper = $("#op_" + op_id).parent().next().next();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op/')?>" + op_op_id + "/" + id + "/" + attribute_id+"/"+main_price,
                type: 'get',
                success: function (r) {
                    
                    $(wrapper).html(r);
                }
            });

        }

        cal();

    }



    function OptionOptions(pro_att_op_id, attribute_id) {

        if (pro_att_op_id) {
            //id
            var id = pro_att_op_id.split("_")[0];
            //option_id
            var optin_id = pro_att_op_id.split("_")[1];
            //mainprice
            var main_p = parseFloat($('#main_price').val());

            if(isNaN(main_p)){

                var main_price = 0;

            } else {

                var main_price = main_p;
                
            }

            var wrapper = $(".options_" + attribute_id).parent().next().next();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_option_option/')?>" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });

        } else {

        }

        cal();

    }


    $('body').on('click', '#row', function () {
        cal();

        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);
        
    });


    function cal(){

        var w = $('#width').val();
        var h = $('#height').val();

        if(w!=='' && h!==''){

            var contribut_price = 0;
            $(".contri_price").each(function () {
                isNaN(this.value) || 0 == this.value.length || (contribut_price += parseFloat(this.value))
            });
            var main_price = parseFloat($('#main_price').val());

            if(isNaN(main_price)){
                var prc = 0;
            }else{
                var prc = main_price;
            }

            var total_price = (contribut_price + prc);
            var t = (isNaN(total_price)?0:total_price);

            $("#total_price").val(t.toFixed(2));
            $("#tprice").text("Total Price = $" + t);
        }

    }



//width fraction get 
    $('#width').on('keyup',function(){

        var hif = $(this).val().split(".")[1];
        
        if(hif){

            $.ajax({

                url: "<?php echo base_url('b_level/order_controller/get_height_width_fraction/')?>" + hif,
                type: 'get',
                success: function (r) {
                    $("#width_fraction_id").val(r);
                }

            });

        }else{

            $("#width_fraction_id").val('');
        }
    
    });
//--end---


// Height fraction 
    $('#height').on('keyup',function(){

        var hif = $(this).val().split(".")[1];
        if(hif){

            $.ajax({

                url: "<?php echo base_url('b_level/order_controller/get_height_width_fraction/')?>" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id").val(r);
                }
            });

        }else{

            $("#height_fraction_id").val('');
        }

    });
//---End----




    function loadPStyle() {

        var product_id = $('#product_id').val();
        
        var pattern_id = $('#pattern_id').val();

        var pricestyle = $('#pricestyle').val();

        var price = $("#height").parent().parent().parent().next();

        var hif = ($("#height_fraction_id :selected").text().split("/")[0]/$("#height_fraction_id :selected").text().split("/")[1]);
        
        var wif = ($("#width_fraction_id :selected").text().split("/")[0]/$("#width_fraction_id :selected").text().split("/")[1]);

        var width = parseFloat($('#width').val())+(isNaN(wif)?0:wif);

        var height = parseFloat($('#height').val())+(isNaN(hif)?0:hif);

        var width_w = (isNaN(width)?'':width);
        var height_w = (isNaN(height)?'':height);
        
            if(pricestyle!=='2'){
                
                $.ajax({

                    url: "<?php echo base_url('b_level/order_controller/get_product_row_col_price/')?>" + height_w + "/" + width_w + "/" + product_id + "/" + pattern_id,
                    
                    type: 'get',
                    
                    success: function (r) {

                        var obj = jQuery.parseJSON(r);
                        $(price).html(obj.ht);

                        if (isNaN(parseFloat(obj.prince))) {

                            $("#tprice").text("Total Price = $0");
                        
                        } else {
                      
                            $("#tprice").text("Total Price = $"+ parseFloat(obj.prince));
                      
                        }
                        
                        // $("#height").val(obj.col);
                        if (obj.st === 1) {

                            $('#cartbtn').removeAttr('disabled');

                        } else if (obj.st === 2) {
                            //$('#cartbtn').prop('disabled', true);
                        }

                        cal();
                    }
                });

            }

            if(pricestyle==='2'){

                var main_p = parseFloat($('#sqr_price').val());
                
                if(isNaN(main_p)){
                    var main_price = 0;
                } else {
                    var main_price = main_p;
                }

                var sum = (width*height)/144;
                var price = main_price*sum;

                $('#main_price').val(price.toFixed(2));

                cal();

            } 
            //---End--

        cal();
        
    }


    $(document).ready(function () {

        //---------------------------
        // Get attribute  by product id
        // --------------------------

        $('body').on('change', '#product_id', function () {

            var product_id = $(this).val();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_to_attribute/')?>" + product_id,
                type: 'get',
                success: function (r) {

                    $("#attr").html(r);
                    callTrigger();
                    $('#cartbtn').removeAttr('disabled');

                }
            });

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_color_partan_model/')?>" + product_id,
                type: 'get',
                success: function (r) {
                    
                    $('#color_model').html(r);
                }
            });
        });
        $('body').on('change', '#pattern_id', function () {

            var pattern_id = $(this).val();

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_color_model/')?>" + pattern_id,
                type: 'get',
                success: function (r) {
                    
                    $('#pattern_color_model').html(r);
                }
            });
        });

    //---------------------------
    // End
    // --------------------------



        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });


        $.ajax({
            url: "order-id-generate",
            type: 'get',
            success: function (r) {
                $("#orderid").val(r);
            }
        });


        //================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/category_wise_subcategory/')?>" + category_id,
                type: 'get',
                success: function (r) {
                    $("#subcategory_id").html(r);
                }
            });
        });


//---------------------------
// Get product  by category id
// --------------------------

        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();

            $("#total_price").val(0);
            $("#main_price").val(0);
            $("#product_id").val('');

            $("#tprice").text("Total Price = $0");
            $("#attr").load(location.href+" #attr>*","");

            loadPStyle();


            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_by_category/')?>" + category_id,
                type: 'get',
                success: function (r) {
                    
                    $("#product_id").html(r);

                }
            });
        });

//---------------------------
// Get sub_category  by category id
// --------------------------

        $('body').on('change', '#sub_category_id', function () {
            var sub_category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_by_subcategory/')?>" + sub_category_id,
                type: 'get',
                success: function (r) {
                    $("#product_id").html(r);
                }
            });
        });



//---------------------------
// Get customer-wise-sidemark
// --------------------------

        $("body").on('change', '#customer_id', function () {

            var customer_id = $(this).val();

            $.ajax({

                url: 'customer-wise-sidemark/' + customer_id,
                type: 'get',
                success: function (data) {


                    if (data == 0) {

                        $("#side_mark").val("None");

                    } else {

                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate != null ? obj.tax_rate : 0);

                        $('#side_mark').val(obj.side_mark);
                        $('#tax').val(tax);

                        if (obj.side_mark) {
                            $('#side_mark').val(obj.side_mark);
                        }

                        $('#customertype').val(obj.customer_type);

                        customerWiseComission();
                    }
                }

            });

        });


        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);
        });


    });





    function callTrigger(){

        $('.op_op_load').trigger('change');
        $('.op_op_op_load').trigger('change');
        $('.op_op_op_op_load').trigger('change');
        $('.op_op_op_op_op_load').trigger('change');
    }



//---------------------------
// submit form and add data
// --------------------------

    // submit form and add data
    $("#AddToCart").on('submit', function (e) {

        e.preventDefault();

        var submit_url = "<?php echo base_url('b_level/order_controller/add_to_cart')?>";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                window.location.reload();
                toastr.success('Success! - Product add to cart ');
                setTimeout(function(){
                    $("#cartItems").load(location.href + " #cartItems>*", "");
                }, 2000);
                
            }, error: function () {
                alert('error');
            }
        });

    });



//---------------------------
// submit to cleare cart 
// --------------------------

    // submit form and add data
    $("#clearCart").on('click', function (e) {

        e.preventDefault();

        var submit_url = "<?= base_url(); ?>b_level/order_controller/clear_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                alert('error');
            }
        });
    });




//---------------------------
// get color code by color id
// --------------------------

function getColorCode(id){

    var submit_url = "<?= base_url(); ?>b_level/order_controller/get_color_code/" + id;
        
    $.ajax({
        type: 'GET',
        url: submit_url,
        success: function (res) {

            $('#colorcode').val(res);

        }, error: function () {
            alert('error');
        }
    });

}





    function getColorCode_select(keyword){

        if(keyword!==''){

            var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code_select/" + keyword;
            
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    $('#color_id').val(res);

                }, error: function () {
                    alert('error');
                }
            });
        }

    }



//---------------------------
// Delete cart item
// --------------------------

    function deleteCartItem(id) {

        var submit_url = "<?= base_url(); ?>b_level/order_controller/delete_cart_item/" + id;
        
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                alert('error');
            }
        });

    }



//---------------------------
// customer Wise Comission
// --------------------------

    function customerWiseComission() {

        var customertype = $('#customertype').val();

        if (customertype == '') {

            alert('Please select customer');
            $("#customer_id").focus();

        } else {

            var i = 1;

            $(".product_id").each(function () {

                var productid = (this.value);
                var customer_id = $('#customer_id').val();

                var submit_url = "<?= base_url(); ?>b_level/order_controller/getproductcomission/" + productid + '/' + customer_id;

                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function (res) {

                        var obj = jQuery.parseJSON(res);

                        var qty = $('#qty_' + i).val();

                        var list_price = parseFloat($('#list_price_' + i).val());
                        var total_list_price = (list_price*qty);


                        //if (customertype === 'business') {

                        $('#discount_' + i).val(obj.dealer_price);

                        var discount = (total_list_price * obj.dealer_price) / 100;

                        // } else {

                        //     $('#discount_' + i).val(obj.individual_price);
                        //     var discount = (list_price * obj.individual_price) / 100;

                        // }

                        $('#utprice_' + i).val(total_list_price - discount.toFixed(2));

                        calculetsPrice();

                        i++;

                    }, error: function () {
                        alert('error');
                    }
                });



            });
        }

    }




    function calculetsPrice() {

        var subtotal = 0;
        $(".utprice").each(function () {
            isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
        });


        $('#subtotal').val(subtotal.toFixed(2));

        //var taxs = parseFloat($('#tax').val());
        //var tax = (subtotal*taxs)/100;
        var install_charge = parseFloat($('#install_charge').val());
        var other_charge = parseFloat($('#other_charge').val());
        var invoice_discount = parseFloat($('#invoice_discount').val());
        var misc = parseFloat($('#misc').val());


        // var install_charge  = $('#install_charge').val().split(".")[1];
        // if (install_charge.length > 2 ){
        //     $('#install_charge').val(parseFloat($('#install_charge').val()).toFixed(2));
        // }


        // var other_charge  = $('#other_charge').val().split(".")[1];
        // if (other_charge.length > 2 ){
        //     $('#other_charge').val(parseFloat($('#other_charge').val()).toFixed(2));
        // }


        // var invoice_discount  = $('#invoice_discount').val().split(".")[1];
        // if (invoice_discount.length > 2 ){
        //     $('#invoice_discount').val(parseFloat($('#invoice_discount').val()).toFixed(2));
        // }


        // var misc  = $('#misc').val().split(".")[1];
        // if (misc.length > 2 ){
        //     $('#misc').val(parseFloat($('#misc').val()).toFixed(2));
        // }


        //var grandtotal = (subtotal + install_charge + other_charge + misc);
        var grandtotal = (subtotal);
        var gtotal = grandtotal - invoice_discount;
        $('#grand_total').val(gtotal.toFixed(2));
        calDuePaid();
    }



    function calDuePaid() {

        var grand_total = parseFloat($('#grand_total').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var due = (grand_total - paid_amount);
        $('#due').val(due.toFixed(2));

    }



    $('#card_area').hide();
    $('#card_area2').hide();

    function setCard(value) {

        if (value === 'card') {
            $('#card_area').slideDown();
            $('#card_area2').slideDown();
        } else if (value === 'cash') {

            $('#card_number').val('');
            $('#issuer').val('');
            $('#card_area').slideUp();
            $('#card_area2').slideUp();

        }
    }



    // submit form and add data
    $("#save_order").on('submit', function (e) {

        e.preventDefault();

        var order_status = $(this).val();
        var order_status = $('#order_status').val(order_status);

        var submit_url = "<?= base_url(); ?>b_level/order_controller/save_order";
        // $.ajax({
        //     type: 'POST',
        //     url: submit_url,
        //     data: $(this).serialize(),
        //     success: function(res) {
        //         $("#cartItems").load(location.href+" #cartItems>*",""); 
        //     },error: function() {
        //         alert('error');
        //     }
        // });
    });


    // -------- Show Image Preview once File selected ----
    $("body").on("change", "#file_upload", function (e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            var img = document.getElementById('prevImg');
            var reader = new FileReader();
            reader.onloadend = function () {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("logo").after(img);

            $("#prevImg").show();
        }
    });
// -------- Image Preview Ends --------------
    $("#openDialog").click(function () {
        $.post("<?php echo base_url(); ?>show-customer-new-window-popup", function (t) {
            var w = window.open("", "popupWindow", "width=600, height=500, scrollbars=yes");
            var $w = $(w.document.body);
//        $w.html("<textarea></textarea>");
            $w.html(t);
        });
    });
//========== its for file reset starts =======
    var file_input_index = 0;
    $('input[type=file]').each(function () {
        file_input_index++;
        $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
        $(this).after('<input type="button" value="Clear" class="btn btn-danger clr_btn" onclick="reset_html(\'file_input_container_' + file_input_index + '\')" />');

    });
    function reset_html(id) {
        $('#' + id).html($('#' + id).html());
        $("#prevImg").hide();
    }
//========== its for file reset close=======
//========= its for new_customer_modal ==========
    function new_customer_modal() {
        $.post("<?php echo base_url(); ?>new-customer-modal", function (t) {
            $("#new_customer_info").html(t);
            $('#new_customer_modal_info').modal('show');
        });
    }



function customerWiseComission_Inc_Dic(qty, item){

    var customer_id = $("#customer_id").val();

        if (customer_id === '') {

            alert('Please select customer');
            $("#customer_id").focus();

        }else{

            var qty = $('#qty_' + item).val();

            var list_price = parseFloat($('#list_price_' + item).val());
            var total_list_price = list_price*qty;

            var dealer_price = $('#discount_' + item).val();

            var discount = (total_list_price * dealer_price) / 100;

            $('#utprice_' + item).val(total_list_price - discount.toFixed(2));
            calculetsPrice();
        }

}

    
</script>


<script>

    $('.add').click(function () {


            if ($(this).prev().val() < 1000) {
                 $(this).prev().val(+$(this).prev().val() + 1);
                var qty = $(this).prev().val();
                var item = $(this).parent().next().val();
                customerWiseComission_Inc_Dic(qty, item);
            }
        
    });
    
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1)
                $(this).next().val(+$(this).next().val() - 1);

            var qty = $(this).next().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);

        }
    });
</script>



<?php  $this->load->view($customerjs)?>


<script>  

    $(document).ready(function() {

        $('.sticky_container .sticky_item').theiaStickySidebar({
             additionalMarginTop: 0
        });

    });

</script>
