
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Order Manage (Kanban View)</h5>
            <div class="col-sm-6 text-right">
                <a href="customer_new.html" class="btn btn-success btn-sm mt-1">Add</a>
            </div>
        </div>
        <!-- end box / title -->
        <div class="px-2 d-flex">
            <table class="tables_ui" id="t_draggable1">
                <thead>
                    <tr>
                        <th>Quote</th>
                    </tr>
                </thead>
                <tbody class="t_sortable">
                    <?php foreach ($quote_orderd as $key => $val){?>
                            <tr data-data_id="<?=$val->order_id?>" id="<?=$val->order_id?>" class="draggable">
                                <td>
                                    <p>Order Date: <?=$val->order_date?></p>
                                    <p>Order: <?=$val->order_id?></p>
                                    <p>Client Sidemark: <?=$val->side_mark?></p>
                                    <p>Username: <?=$val->customer_name?></p>
                                    <a href="<?=base_url()?>order-view/<?=$val->order_id;?>">More Details</a>
                                </td>    
                            </tr>
                        <?php } ?>
                   
                </tbody>
            </table>

            <table class="tables_ui" id="t_draggable2">
                <thead>
                    <tr>
                        <th>Paid</th>
                    </tr>
                </thead>
                <tbody class="t_sortable">
                   <?php foreach ($paid_orderd as $key => $val){?>

                            <tr data-data_id="<?=$val->order_id?>" id="<?=$val->order_id?>" class="draggable">
                                <td>
                                    <p>Order Date: <?=$val->order_date?></p>
                                    <p>Order: <?=$val->order_id?></p>
                                    <p>Client Sidemark: <?=$val->side_mark?></p>
                                    <p>Username: <?=$val->customer_name?></p>
                                    <a href="<?=base_url()?>order-view/<?=$val->order_id;?>">More Details</a>
                                </td>                                                           
                            </tr>
                        <?php } ?>
                </tbody>
            </table>

            <table class="tables_ui" id="t_draggable3">
                <thead>
                    <tr>
                       <th>Partially Paid</th>
                    </tr>
                </thead>
                <tbody class="t_sortable">
                    <?php foreach ($partially_paid_orderd as $key => $val){?>
                            <tr data-data_id="<?=$val->order_id?>" id="<?=$val->order_id?>" class="draggable">
                                <td>
                                    <p>Order Date: <?=$val->order_date?></p>
                                    <p>Order: <?=$val->order_id?></p>
                                    <p>Client Sidemark: <?=$val->side_mark?></p>
                                    <p>Username: <?=$val->customer_name?></p>
                                    <a href="<?=base_url()?>order-view/<?=$val->order_id;?>">More Details</a>
                                </td>                                                           
                            </tr>
                        <?php } ?>
                </tbody>
            </table>

            <table class="tables_ui" id="t_draggable4">
                <thead>
                    <tr>
                        <th>Shipping</th>
                    </tr>
                </thead>
                <tbody class="t_sortable">
                    <?php foreach ($shipping_orderd as $key => $val){?>
                            <tr data-data_id="<?=$val->order_id?>" id="<?=$val->order_id?>" class="draggable">
                                <td>
                                    <p>Order Date: <?=$val->order_date?></p>
                                    <p>Order: <?=$val->order_id?></p>
                                    <p>Client Sidemark: <?=$val->side_mark?></p>
                                    <p>Username: <?=$val->customer_name?></p>
                                    <a href="<?=base_url()?>order-view/<?=$val->order_id;?>">More Details</a>
                                </td>                                                           
                            </tr>
                        <?php } ?>
                </tbody>
            </table>

            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Cancelled</th>
                    </tr>
                </thead>
                <tbody class="t_sortable">
                    <?php foreach ($cancelled_orderd as $key => $val){?>
                            <tr class="draggable"  data-data_id="<?=$val->order_id?>" id="<?=$val->order_id?>">
                                <td>
                                    <p>Order Date: <?=$val->order_date?></p>
                                    <p>Order: <?=$val->order_id?></p>
                                    <p>Client Sidemark: <?=$val->side_mark?></p>
                                    <p>Username: <?=$val->customer_name?></p>
                                    <a href="<?=base_url()?>order-view/<?=$val->order_id;?>">More Details</a>
                                </td>                                                           
                            </tr>
                        <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<!-- scripts (jquery) -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
<!--[if IE]><script language="javascript" type="text/javascript" src="resources/scripts/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<script type="text/javascript">

        $(document).ready(function() {
              var $tabs = $('#t_draggable2')
              $("tbody.t_sortable").sortable({

                connectWith: ".t_sortable",
                appendTo: $tabs,
                helper:"clone",
                zIndex: 999990,

                stop: function( event, ui ) {

                    alert(event);
                    console.log(event);
                    var id=event.target.id;
                    $('#orderids').val(id);
                },
                drop: function (event, ui) {
                    alert(event);
                    var idt=event.target.id;
                    $('#status').val(idt);
                }


              }).disableSelection();

                var $tab_items = $(".nav-tabs > li", $tabs).droppable({

                    accept: ".t_sortable tr",
                    hoverClass: "ui-state-hover",
                    drop: function( event, ui ) { return false; }

                });
        });
</script>
