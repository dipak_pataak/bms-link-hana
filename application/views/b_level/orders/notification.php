
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <!-- box / title -->
                    <div class="title row">
                        <h5 class="col-sm-6">Notification list</h5>
                    </div>
                    

                    <!-- end box / title -->
                    <p class="mb-3 px-3">
                        <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Filter
                        </button>
                    </p>


                    <div class="collapse show px-3 mb-3" id="collapseExample">

                        <div class="border p-3">

                            <form class="form-horizontal" action="<?=base_url('b_level/notification/notifications')?>" method="post">

                                <fieldset>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control" name="customer_id" >
                                                <option value="">--Select Customer--</option>
                                                 <?php
                                                foreach ($get_customer as $customer) {
                                                    echo "<option value='$customer->customer_user_id'>$customer->first_name $customer->last_name</option>";
                                                }
                                                ?>
                                                
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text"  name="from_date" id="from_date" class="form-control datepicker mb-3" value="" placeholder="From Date">
                                        </div>   

                                        <div class="col-md-4">
                                            <input type="text"  name="to_date" id="to_date" class="form-control datepicker mb-3" value="" placeholder="To Date">
                                        </div>   

                                        <div class="col-md-1 text-right">
                                            <div>
                                                <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                                <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>

                            </form>

                        </div>

                    </div>
               

                    <!-- end box / title -->
                    <div class="table-responsive px-3">
                        <table class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Notification</th>
                                    <th>Company Ordered</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php 
                            if(!empty($notification)){
                                $i=1;
                                foreach ($notification as $key => $value) { 

                            ?>
                                <tr>
                                    <td><?=$i++;?></td>
                                    <td><?=$value->notification_text;?></td>
                                    <td><?=$value->company;?></td>
                                    <td><?php echo date('M-d-Y', strtotime($value->date));?></td>

                                    <td class="width_140">
                                        <a href="<?=base_url().$value->go_to_url;?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a> 
                                       
                                    </td>
                                    
                                </tr>

                            <?php  
                                } 
                            }else{
                            ?>

                            <div class="alert alert-danger"> There have no notification found..</div>
                        <?php } ?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end content / right -->
