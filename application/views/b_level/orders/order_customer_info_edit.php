<div id="right">
    <div class="box">
        <h5>Customer information update</h5>
        <!--<div class="separator mb-3"></div>-->
        <?= form_open_multipart('b_level/Invoice_receipt/order_customer_info_update', array('id' => '')) ?>
        <div class="form-row">
            <?php
//            dd($get_customer_info);
            ?>
            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                    <div class="col-sm-8">
                        <p><input type="text" name="orderid" id="orderid" value="<?php echo $get_customer_info[0]->order_id; ?>" class="form-control" readonly></p>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="order_date" class="col-form-label col-sm-4">Date</label>
                    <div class="col-sm-8">
                        <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php
                        if ($get_customer_info[0]->order_date) {
                            echo $get_customer_info[0]->order_date;
                        } else {
                            echo date('Y-m-d');
                        }
                        ?>">
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6" >
                <div class="row m-0">
                    <label for="customer_id" class="col-form-label col-sm-4">Select /Add Customer</label>
                    <div class="col-sm-8" id="custo">
                        <select class="form-control select2" name="customer_id" id="customer_id" required="" data-placeholder="--select one --">
                            <option value=""></option>
                            <?php foreach ($get_customer as $customer) { ?>
                                <option value='<?php echo $customer->customer_id; ?>' <?php
                                if ($get_customer_info[0]->customer_id == $customer->customer_id) {
                                    echo 'selected';
                                }
                                ?>>
                                            <?php echo $customer->first_name . " " . $customer->last_name; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <!--<a href="<?= base_url('add-b-customer') ?>">Add New Customer</a>-->
                        <!--<a href="javascript:void(0)" onclick="new_customer_modal()">Add New</a>--> 
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="side_mark" class="col-form-label col-sm-4">Side Mark</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="side_mark" name="side_mark" value="<?php echo $get_customer_info[0]->side_mark; ?>" readonly>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0 ship_addr" style="display: none;">
                    <label for="mapLocation" class="col-form-label col-sm-4">Different Shipping Address</label>
                    <div class="col-sm-8">
                        <?php
                        if ($get_customer_info[0]->is_different_shipping == 1) {
                            $shipping_address = $get_customer_info[0]->different_shipping_address;
//                            echo 
                        } else {
                            $shipping_address = '';
                        }
                        ?>
                        <input type="text" id="mapLocation" name="shippin_address" class="form-control" value="<?php echo $shipping_address; ?>">
                        <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <div class="row m-0">
                    <div class="col-sm-8 offset-sm-4 mb-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="different_address" value="1" <?php
                            if ($get_customer_info[0]->is_different_shipping == 1) {
                                echo 'checked';
                            }
                            ?> id="shipaddress">
                            <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="file_upload" id="file_upload">                        
                        <input type="hidden" name="file_upload_hdn" value="<?php echo @$get_customer_info[0]->upload_file; ?>">
                        <p>Extension:JPG/PNG PDF & DOC. File size: 2MB</p>
                        <img src="<?php echo base_url(); ?>assets/profile-pic.png" class="img-thumbnail" width="100" height="50" id="prevImg">
                    </div>
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-success offset-3" id="gq">Update</button>
<?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
    $('document').ready(function () {
        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });
        
        var diff_status = "<?php echo $get_customer_info[0]->is_different_shipping; ?>"
        if(diff_status == '1'){
            $(".ship_addr").css({'display':'flex'});
        }
    });
    
//---------------------------
// Get customer-wise-sidemark
// --------------------------

        $("body").on('change', '#customer_id', function () {
            var customer_id = $(this).val();
            $.ajax({
                url: '<?php echo base_url('customer-wise-sidemark'); ?>/' + customer_id,
                type: 'get',
                success: function (data) {
                    if (data == 0) {
                        $("#side_mark").val("None");
                    } else {
                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate != null ? obj.tax_rate : 0);
                        $('#side_mark').val(obj.side_mark);
                        $('#tax').val(tax);

                        if (obj.side_mark) {
                            $('#side_mark').val(obj.side_mark);
                        }

                        $('#customertype').val(obj.customer_type);
//                        customerWiseComission();
                    }
                }

            });

        });
</script>