<style type="text/css">
    .modal-dialog {
        max-width: 850px;
    }
</style>

<div id="customerModal" class="modal fade" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Add New Customer</h4>
            </div>

            <div class="modal-body">

                <form action="#" method="post" class="p-3" id="SaveCustomer" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="first_name" class="mb-2">First name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John" required>
                        <div class="valid-tooltip">
                            Looks good!
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="last_name" class="mb-2">Last name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" required>
                        <div class="valid-tooltip">
                            Looks good!
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email" class="mb-2">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="johndoe@yahoo.com">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone" class="mb-2">Contact</label>
                        <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX) XXX-XXXX">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="company" class="mb-2">Company</label>
                        <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech">
                    </div>
                    <div class="col-sm-6">
                        <div class="custom-control custom-radio custom-control-inline mt-3">
                            <input type="radio" class="custom-control-input" id="personal" name="customer_type" checked value="personal">
                            <label class="custom-control-label" for="personal">Personal</label>
                        </div>

                         Default inline 2
                        <div class="custom-control custom-radio custom-control-inline mt-3">
                            <input type="radio" class="custom-control-input" id="business" name="customer_type" value="business">
                            <label class="custom-control-label" for="business">Business</label>
                        </div>
                    </div>

                     <div class="form-group col-md-12">
                        <label for="caddress" class="mb-2">Address</label>
                        <input type="text" class="form-control" id="caddress" name="address" autocomplete="off" placeholder="" required>
                    </div> 

<!--                <div class="form-group col-md-12">
                    <label for="address" class="mb-2">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="" autocomplete="off" required>
                </div>-->

                <div class="form-group col-md-3">
                    <label for="city" class="mb-2">City</label>
                    <input type="text" class="form-control" id="city" name="city" placeholder="">
                </div>

                <div class="form-group col-md-3">
                    <label for="state" class="mb-2">State</label>
                    <input type="text" class="form-control" id="state" name="state" placeholder="">
                </div>

                <div class="form-group col-md-3">
                    <label for="zip" class="mb-2">Zip</label>
                    <input type="text" class="form-control" id="zip" name="zip">
                </div>

                <div class="form-group col-md-3">
                    <label for="country_code" class="mb-2">Country Code</label>
                    <input type="text" class="form-control" id="country_code" name="country_code">
                </div>


                    <div class="form-group col-md-4">
                        <label for="c_file_upload" class="mb-2">File Upload</label>
                        <input class="form-control" type="file" id="c_file_upload" name="file_upload">
                    </div>               

                </div>

            <div class="form-row form-template">

                <div class="form-group col-md-6">
                    <label for="username" class="mb-2">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                </div>
                <div class="form-group col-md-6">
                    <label for="password" class="mb-2">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
            </div>


             
                <button type="submit" class="btn btn-sm d-block float-right btn-success customer_btn">Add</button>

            </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>

</div>



<!--<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>-->
<script type="text/javascript">
    $(function () {

        $('#personal').click(function () {
            $(".form-template").css("display", "none");
        });
        $('#business').click(function () {
            $(".form-template").css("display", "flex");
        });

    });
    
//    ============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {
        if ($('#business').is(":checked"))
        {
            if ($('#username').val() == '') {
                $('#username').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#username').css({'border': '2px; solid green'});
            }
            if ($('#password').val() == '') {
                $('#password').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#password').css({'border': '2px; solid green'});
            }
        }
    });



//    ============= its for  google geocomplete =====================
//google.maps.event.addDomListener(window, 'load', function () {
//        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
//
//
//        google.maps.event.addListener(places, 'place_changed', function () {
//            var place = places.getPlace();
//            console.log(place);
//            var address = place.formatted_address;
//            var latitude = place.geometry.location.lat();
//            var longitude = place.geometry.location.lng();
//            var geocoder = new google.maps.Geocoder;
//            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
//            geocoder.geocode({'location': latlng}, function (results, status) {
//                if (status === 'OK') {
//                    //console.log(results)
//                    if (results[0]) {
//                        //document.getElementById('location').innerHTML = results[0].formatted_address;
//                        var street = "";
//                        var city = "";
//                        var state = "";
//                        var country = "";
//                        var country_code = "";
//                        var zipcode = "";
//                        for (var i = 0; i < results.length; i++) {
//                            if (results[i].types[0] === "locality") {
//                                city = results[i].address_components[0].long_name;
//                                state = results[i].address_components[2].long_name;
//
//                            }
//                            if (results[i].types[0] === "postal_code" && zipcode == "") {
//                                zipcode = results[i].address_components[0].long_name;
//
//                            }
//                            if (results[i].types[0] === "country") {
//                                country = results[i].address_components[0].long_name;
//                            }
//                            if (results[i].types[0] === "country") {
//                                country_code = results[i].address_components[0].short_name;
//                            }
//                            if (results[i].types[0] === "route" && street == "") {
//                                for (var j = 0; j < 4; j++) {
//                                    if (j == 0) {
//                                        street = results[i].address_components[j].long_name;
//                                    } else {
//                                        street += ", " + results[i].address_components[j].long_name;
//                                    }
//                                }
//
//                            }
//                            if (results[i].types[0] === "street_address") {
//                                for (var j = 0; j < 4; j++) {
//                                    if (j == 0) {
//                                        street = results[i].address_components[j].long_name;
//                                    } else {
//                                        street += ", " + results[i].address_components[j].long_name;
//                                    }
//                                }
//
//                            }
//                        }
//                        if (zipcode == "") {
//                            if (typeof results[0].address_components[8] !== 'undefined') {
//                                zipcode = results[0].address_components[8].long_name;
//                            }
//                        }
//                        if (country == "") {
//                            if (typeof results[0].address_components[7] !== 'undefined') {
//                                country = results[0].address_components[7].long_name;
//                            }
//                            if (typeof results[0].address_components[7] !== 'undefined') {
//                                country_code = results[0].address_components[7].short_name;
//                            }
//                        }
//                        if (state == "") {
//                            if (typeof results[0].address_components[6] !== 'undefined') {
//                                state = results[0].address_components[6].long_name;
//                            }
//                        }
//                        if (city == "") {
//                            if (typeof results[0].address_components[5] !== 'undefined') {
//                                city = results[0].address_components[5].long_name;
//                            }
//                        }
//
//                        var address = {
//                            "street": street,
//                            "city": city,
//                            "state": state,
//                            "country": country,
//                            "country_code": country_code,
//                            "zipcode": zipcode,
//                        };
//
//                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
////                        console.log(zipcode);
//                        $("#city").val(city);
//                        $("#state").val(state);
//                        $("#zip").val(zipcode);
//                        $("#country_code").val(country_code);
//                        
//                    } else {
//                        window.alert('No results found');
//                    }
//                } else {
//                    window.alert('Geocoder failed due to: ' + status);
//                }
//            });
//
//        });
//
//
//    });
</script>



<script type="text/javascript">


    function AddNewCustomer(){
        
        $('#customerModal').modal('show');
    }


    // submit form and add data
    $("#SaveCustomer").on('submit',function(e){
        e.preventDefault();

        var submit_url = "b_level/order_controller/customer_save";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function(res) {

                var data = JSON.parse(res);

                $('#customer_id').append('<option value = "' + data.customer_id + '"  selected> '+ data.first_name +' '+ data.last_name +' </option>');

                $("#side_mark").val(data.side_mark);

                $('#customerModal').modal('hide'); 
                
            },error: function() {
                alert('error');
            }
        });
    });


</script>