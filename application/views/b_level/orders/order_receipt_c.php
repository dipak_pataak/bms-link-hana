<?php $currency = $company_profile[0]->currency; ?>
<style type="text/css">
    .rotate90 {
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
        margin-top: 50px;
    }

    @media print {

        .noprint{content:"" !important;display:none !important;visibility:hidden !important;}

    }

</style>


<div id="right">
    <div class="box" id="">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <h5>
            <div class="pull-right">
                <?php if(isset($check_re_order_status) && $check_re_order_status !=1) { ?>
                <a onclick="open_re_order_modal()" href="#" class="btn btn-success">Split order</a>
                <?php } ?>
                <?php if ($shipping == '') { ?>
                    <a href="<?= base_url('b_level/order_controller/shipment/'); ?><?= $orderd->order_id ?>" class="btn btn-success" >Go Shipment</a>
                <?php } ?>

                <?php if ($orderd->order_stage != 2) { ?>
                    <a href="<?= base_url('b_level/order_controller/order_view/'); ?><?= $orderd->order_id ?>" class="btn btn-success" >Receive Payment</a>
                <?php } ?>
                <button type="button" class="btn btn-success" onclick="printContent('printableArea')" >Print</button>
            </div>
            
            Order info
        </h5>



            <style type="text/css">
                div, p{
                    font-size: 11px !important;
                }
                table tr th{font-size: 12px !important; }
            </style>


        <div class="separator mb-3"></div>


        <div id="printableArea">

                <div class="row">

                    <div class="form-group col-md-6" style="margin-left: 20px;">
                        <img width="200" src="<?php echo base_url('assets/b_level/uploads/appsettings/') . $company_profile[0]->logo; ?>">

                        <p><?= $company_profile[0]->company_name; ?></p>
                        <p><?= $company_profile[0]->address; ?></p>
                        <p><?= $company_profile[0]->city; ?>, 
                            <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>, <?= $company_profile[0]->country_code; ?></p>
                        <p></p>
                        <p><?= $company_profile[0]->phone; ?></p>
                        <p><?= $company_profile[0]->email; ?></p>
                    </div>

                

                <?php

                    $CI =& get_instance();
                    $CI->load->library('barcode/br_code');
                    $barcode_img_path = $orderd->order_id;
                    
                ?>


                <div class="form-group col-md-5" style="margin-left: 20px">

                    <table class="table table-bordered mb-4">

                        <tr class="text-center">
                            <td>Order Date</td>
                            <td class="text-right"><?= date_format(date_create($orderd->order_date),'M-d-Y') ?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Order Id</td>
                            <td class="text-right"><?= $orderd->order_id ?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Sidemark</td>
                            <td class="text-right"><?= $orderd->side_mark; ?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Barcode</td>
                            <td class="text-right"><?= $CI->br_code->gcode1($barcode_img_path); ?></td>
                        </tr>

                        <?php if ($shipping != NULL) { ?>

                            <tr class="text-center">
                                <td>Tracking Number (<?= $shipping->method_name ?>) </td>
                                <td class="text-right"> <?= $shipping->track_number ?> </td>
                            </tr>

                        <?php } ?>

                    </table>
                </div>

                </div>


            <div class="row" style="margin-top: 20px;">
                
                <div class="form-group col-md-6" style="margin-left: 20px;">
                    <strong>Sold To: </strong>
                    <?= $cinfo[0]->company_name; ?> 
                    <p><?= $cinfo[0]->address; ?></p>
                    <p><?= $cinfo[0]->city; ?>, 
                        <?= $cinfo[0]->state; ?>, <?= $cinfo[0]->zip_code; ?>, 
                        <?= $cinfo[0]->country_code; ?></p>
                    <p><?= $cinfo[0]->phone; ?></p>
                    <p><?= $cinfo[0]->email; ?></p>
                </div>

                <?php 

                if ($shipping != NULL) { ?>
                    
                    <div class="form-group col-md-5">
                        <strong>Ship To: </strong>
                        <?= $shipping->customer_name; ?> 
                        <p><?= $shipping->shipping_addres; ?></p>
                        <p>+1<?= $shipping->customer_phone; ?></p>
                        <p><?= $cinfo[0]->email; ?></p>
                    </div>

                <?php } else{ ?>

                    <div class="form-group col-md-5">
                        <strong>Sold To: </strong>
                        <?= $cinfo[0]->company_name; ?> 
                        <p><?= $cinfo[0]->address; ?></p>
                        <p><?= $cinfo[0]->city; ?>, 
                            <?= $cinfo[0]->state; ?>, <?= $cinfo[0]->zip_code; ?>, 
                            <?= $cinfo[0]->country_code; ?></p>
                        <p><?= $cinfo[0]->phone; ?></p>
                        <p><?= $cinfo[0]->email; ?></p>
                    </div>

                <?php } ?>

                <a href="<?php echo base_url(); ?>order-customer-info-edit/<?php echo $orderd->order_id; ?>" class="btn btn-info btn-sm noprint" style="height: 30px; margin-right: 10px;"><i class="fa fa-edit"></i></a>

            </div>

            <h5 style="margin-left: 15px;">Order Details</h5>

            <div class="separator mb-3"></div>

            <div class="px-3" id="cartItems">

                <table class="datatable2 table table-bordered table-hover mb-4">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Description</th>
                            <th>List</th>
                            <th>Discount(%)</th>
                            <th>Price</th>
                            <th>Notes</th>
                            <!--<th class="noprint">Action</th>-->
                        </tr>
                    </thead>

                    <tbody>

                        <?php $i = 1; ?>

                        <?php
                        foreach ($order_details as $items):
                            $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                            $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                        ?>

                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $items->product_qty; ?></td>
                                <td width="400px;">
                                    <strong><?= $items->product_name; ?></strong><br/>
                                    <?= $items->pattern_name; ?><br/> 
                                    W <?= $items->width; ?> <?= @$width_fraction->fraction_value ?>, 
                                    H <?= $items->height; ?> <?= @$height_fraction->fraction_value ?>, 
                                    <?= $items->color_number; ?> 
                                    <?= $items->color_name; ?>
                                   
                                </td>

                                <td><?= $currency; ?><?= $items->list_price ?> </td>
                                <td><?= $items->discount ?> %</td>
                                <td><?= $currency; ?><?= $items->unit_total_price ?> </td>
                                <td><?= $items->notes ?></td>

<!--                                <td class="noprint">
                                    <a href="<?php echo base_url(); ?>order-single-product-edit/<?php echo $items->row_id;  //echo $orderd->order_id ."/". $items->product_id; ?>" class="btn btn-info btn-sm" style="height: 30px; margin-right: 10px;"><i class="fa fa-edit"></i></a>
                                </td>-->
                                

                            </tr>

                            <?php $i++; ?>

                        <?php endforeach; ?>

                    </tbody>
                </table>



                <table class="datatable2 table table-bordered table-hover mb-4">

                    <thead>
                        <tr>
                            <?php if ($shipping != NULL) { ?>
                                <th>Shipping cost</th>
                            <?php } ?>

                            <th>Sub-Total</th>
                            <th>Installation Charge</th>
                            <th>Other Charge</th>
                            <th>Misc</th>
                            <th>Discount</th>
                            <th>Grand Total</th>
                            <th>Deposit</th>
                            <th>Due </th>
                        </tr>
                    </thead>

                    <tbody>
                    <?php if ($shipping != NULL) { ?>
                        <td class="text-right"> <?= $currency; ?> <?= $shipping->shipping_charges ?> </td>
                    <?php } ?>
                    <!-- <td class="text-right"> <?= $currency; ?><?= $orderd->state_tax ?></td> -->

                    <td class="text-right"> <?= $currency; ?><?= $orderd->subtotal; ?> </td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->installation_charge ?> </td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->other_charge ?></td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->misc ?> </td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->invoice_discount ?></td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->grand_total ?> 
                    <td class="text-right"> <?= $currency; ?><?= $orderd->paid_amount ?> 
                    <td class="text-right"> <?= $currency; ?><?= $orderd->due ?> 
                    </tbody>

                </table>

                <?php
                if ($shipping != NULL) {
                    if ($shipping->method_name == 'UPS') {
                        ?>

                        <div class="col-lg-6 offset-lg-4">
                            <!--<a href="#"  data-toggle="modal" data-target="#order_logo">-->
                            <a href="<?php echo base_url(); ?>order-logo-print/<?php echo $this->uri->segment(4); ?>" target="_new">
                                <img src="<?php echo base_url() . $shipping->graphic_image ?>" width="" height='250' >
                            </a>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="order_logo" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                <!-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>-->
                                    <div class="modal-body" style="height: 455px;">
                                        <div class="col-md-5" id="printableImgArea">
                                            <img src="<?php echo base_url() . $shipping->graphic_image ?>" class="rotate90" style="-webkit-transform: rotate(90deg); margin-top: 90px; height: 250px;"> 
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a  class="btn btn-warning " href="#" onclick="printDiv('printableImgArea')">Print</a>
                                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                        <a href="" class="btn btn-danger">Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>

            </div>

        </div>

        <div class="col-lg-6 offset-lg-6 text-right">
            <button type="button" class="btn btn-success" onclick="printContent('printableArea')" >Print</button>
        </div>

    </div>

</div>


<div class="modal fade" id="re_order_modal" role="dialog">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Split order</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" >
                <form  action="<?php echo base_url('/b-user-re-order');?>" method="post">
                    <div class="form-group row">
                        <label  class="col-xs-2 control-label">Select b user</label>
                        <div class="col-xs-6">
                            <select name="b_user_id" class="form-control">
                                <?php if(isset($b_user_data) && !empty($b_user_data)) { ?>
                                    <?php foreach ($b_user_data as $key => $value) { ?>
                                        <option value="<?php echo $value->id; ?>"><?php echo $value->first_name . ' ' . $value->last_name; ?></option>
                                    <?php }

                                }?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label  class="col-xs-2 control-label">Select Product</label>
                        <div class="col-xs-6">
                            <select name="re_order_product_data[]" class="form-control selectpicker" multiple>
                                <?php if(isset($re_order_product_data) && !empty($re_order_product_data)) { ?>
                                    <?php foreach ($re_order_product_data as $key => $value) { ?>
                                        <option value="<?php echo $value->product_id; ?>"><?php echo $value->product_name; ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="clevel_order_id" value="<?php if(isset($clevel_order_id)) { echo $clevel_order_id; }?>">



                    <div class="form-group row">
                        <label  class="col-xs-2 control-label"></label>
                        <div class="col-xs-6">
                            <button class="btn-sm btn-success" id="closeModal">Split order</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    //print a div
    function printContent(el) {
         $('.noprint').hide();
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }

</script>


<script type="text/javascript">
    function printDiv(divName) {
         $('.noprint').hide();
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        // document.body.style.marginTop="-45px";
        window.print();
        document.body.innerHTML = originalContents;
    }

    function open_re_order_modal()
    {
        $('#re_order_modal').modal('show');
    }
</script>


