
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Edit Product Attribute</h5>
        </div>


        <div class="p-1">
            <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
            ?>
        </div>

        <!-- end box / title -->
        <form action="<?php echo base_url('b_level/Attribute_controller/attribute_update' ); ?>" method="post" class="form-row px-3">

            <div class="col-md-12">

                <div class="form-group">
                    <label for="category" class="mb-2">Attribute Category</label>
                    <select name="category" class="form-control select2" id="category" data-placeholder='-- select one --'>
                        <option value=" ">None</option> 
                        <?php foreach($categorys as $category){?>
                        <option value="<?=$category->category_id?>" <?=($category->category_id==$attribute->category_id?'selected':'');?>><?=$category->category_name?></option>
                        <?php }?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label class="mb-2">Attribute Name</label>
                    <input class="form-control" type="text" value="<?=$attribute->attribute_name;?>" name="attribute" required>
                </div>

                <div class="form-group">
                    <label class="mb-2">Attribute Position</label>
                    <input class="form-control" type="number" value="<?=$attribute->position;?>" name="position">
                </div>

                <div class="form-group">
                    <label for="attribute_type" class="mb-2">Attribute Type</label>
                    <select name="attribute_type" class="form-control select2" id="attribute_type" data-placeholder='-- select one --'>
                        <option value=""></option>
                        <option value="1" <?=($attribute->attribute_type==1?'selected':'');?>>Text</option>
                        <option value="2" <?=($attribute->attribute_type==2?'selected':'');?>>Option</option>
                        <option value="3" <?=($attribute->attribute_type==3?'selected':'');?>>Multi text</option>
                    </select>
                </div>

            <input type="hidden" name="attribute_id" value="<?=$attribute->attribute_id?>">


            <?php 
                if($attribute->attribute_type==2 || $attribute->attribute_type==3){
            ?>
                <div class="att_opt" style="margin-top: 10px">
                    <!-- <a style="margin-bottom: 10px" href="javascript:void(0);" class="btn btn-primary add_button "><span class="glyphicon glyphicon-plus"></span>Add Options</a> -->
                    <div class="field_wrapper">

            <?php 
                $i=0;
                foreach ($option as $key => $op) {
                    
                    $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                    $option_option_result = $this->db->query($option_option)->result();
            ?>
                        <div class='row' id='item_"<?=$i?>"'>

                            <div class='form-group col-md-3'>
                                <label for='attr_option' class='mb-2'>Attribute Option</label>
                                <input class='form-control' type='text' value="<?=$op->option_name?>" name='option_name[]' required>
                            </div>

                            <div class='form-group col-md-3'>
                                <label for='attr_option' class='mb-2'>Option Type</label>
                                <select class='form-control option_type' name='option_type[]' id='option_type' onchange='addOption("<?=$i?>")'>
                                    <option value='0'>--Select--</option>
                                    <option value='1' <?=($op->option_type==1?'selected':'')?>>Text</option>
                                    <option value='2' <?=($op->option_type==2?'selected':'')?>>Option</option>
                                    <option value='3' <?=($op->option_type==3?'selected':'')?>>Multi text</option>
                                    <option value='4' <?=($op->option_type==4?'selected':'')?>>Multi option</option>
                                </select>
                            </div>

                            <div class='form-group col-md-2'>
                                <label for='attr_option' class='mb-2'>Price Type</label>
                                <select class='form-control'  name='price_type[]'  >
                                    <option value='1' <?=($op->price_type==1?'selected':'')?>>$</option>
                                    <option value='2' <?=($op->price_type==2?'selected':'')?>>%</option>
                                </select>
                            </div>
                            <div class='form-group col-md-2'>
                                <label for='attr_option' class='mb-2'>Price </label>
                                <input class='form-control' type='text' value="<?=$op->price;?>" name='price[]' >
                            </div>
                            <div class='form-group col-md-2'>
                                <label for='attr_option' class='mb-2'> Option condition </label>
                                <input class='form-control' type='text' value="<?=$op->op_condition;?>" name='condition[]' >
                            </div> 
                            <input type="hidden" name="option_id[]" value="<?=$op->att_op_id?>">

                            <div class='col-md-12'>

                            <?php 
                            $k =0;
                            foreach ($option_option_result as $key => $opop) {

                                $op_op_op = "SELECT * FROM attr_options_option_option_tbl WHERE att_op_op_id = $opop->op_op_id";
                                $op_op_op_result = $this->db->query($op_op_op)->result();

                            ?>
                                <div class='row'>
                                    
                                    <div class='form-group col-md-3'>
                                        <label for='attr_option' class='mb-2'>Attribute Sub Option</label>
                                        <input class='form-control' type='text' value="<?=$opop->op_op_name?>" name='option_option_name[<?=$i?>][]' required>
                                    </div>

                                    <div class='form-group col-md-3'>
                                        <label for='attr_option' class='mb-2'>Sub option Type</label>
                                        <select class='form-control' name='option_option_type[<?=$i?>][]' id='option_option_type' >
                                            <option value='0'>--Select--</option>
                                            <option value='1' <?=($opop->type==1?'selected':'')?>> Text</option>
                                            <option value='2' <?=($opop->type==2?'selected':'')?> > Option</option>
                                            <option value='3' <?=($opop->type==3?'selected':'')?> > Multi text</option>
                                            <option value='4' <?=($opop->type==4?'selected':'')?> > Multi option</option>
                                        </select>
                                    </div>

                                    <div class='form-group col-md-2'>
                                        <label for='attr_option' class='mb-2'>Sub Price Type</label>
                                        <select class='form-control' name='att_op_op_price_type[<?=$i?>][]'  >
                                            <option value='1' <?=($opop->att_op_op_price_type==1?'selected':'')?>>$</option>
                                            <option value='2' <?=($opop->att_op_op_price_type==2?'selected':'')?>>%</option>
                                        </select>
                                    </div>

                                    <div class='form-group col-md-2'>
                                        <label for='attr_option' class='mb-2'>Sub Option Price </label>
                                        <input class='form-control' type='text' value="<?=$opop->att_op_op_price;?>" name='att_op_op_price[<?=$i?>][]' >
                                    </div>

                                    <div class='form-group col-md-2'>
                                        <label for='attr_option' class='mb-2'>Sub Option condition </label>
                                        <input class='form-control' type='text' value="<?=$opop->att_op_op_condition;?>" name='att_op_op_condition[<?=$i?>][]' >
                                    </div> 

                                    <input type="hidden" name="op_op_id[<?=$i?>][]" value="<?=$opop->op_op_id?>">

                                    <div class='col-md-12'>
                                        <?php 
                                            $j =0;
                                            foreach ($op_op_op_result as $key => $opopop) {
                                        ?>

                                                <div class='row'>
                                                    
                                                    <div class='form-group col-md-3'>
                                                        <label for='attr_option' class='mb-2'>Attribute Sub-Sub Option</label>
                                                        <input class='form-control' type='text' value="<?=$opopop->att_op_op_op_name?>" name='op_op_op_name[<?=$i?>][<?=$k?>][]' required>
                                                    </div>

                                                    <div class='form-group col-md-3'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub option Type</label>
                                                        <select class='form-control' name='op_op_op_type[<?=$i?>][<?=$k?>][]' id='option_option_type' >
                                                            <option value='0'>--Select--</option>
                                                            <option value='1' <?=($opopop->att_op_op_op_type==1?'selected':'')?>> Text </option>
                                                            <option value='2' <?=($opopop->att_op_op_op_type==2?'selected':'')?> > Option </option>
                                                            <option value='3' <?=($opopop->att_op_op_op_type==3?'selected':'')?> > Multi text </option>
                                                        </select>
                                                    </div>

                                                    <div class='form-group col-md-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub Price Type</label>
                                                        <select class='form-control'  name='op_op_op_price_type[<?=$i?>][<?=$k?>][]'  >
                                                            <option value='1' <?=($opopop->att_op_op_op_price_type==1?'selected':'')?>>$</option>
                                                            <option value='2' <?=($opopop->att_op_op_op_price_type==2?'selected':'')?>>%</option>
                                                        </select>
                                                    </div>

                                                    <div class='form-group col-md-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub Option Price </label>
                                                        <input class='form-control' type='text' value="<?=$opopop->att_op_op_op_price;?>" name='op_op_op_price[<?=$i?>][<?=$k?>][]' >
                                                    </div>

                                                    <div class='form-group col-md-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub condition </label>
                                                        <input class='form-control' type='text' value="<?=$opopop->att_op_op_op_condition;?>" name='op_op_op_condition[<?=$i?>][<?=$k?>][]' >
                                                    </div> 

                                                    <input type="hidden" name="op_op_op_id[<?=$i?>][<?=$k?>][]" value="<?=$opopop->att_op_op_op_id?>">
                                                </div>

                                        <?php $j++; } ?>

                                    </div>
                                    
                                </div>

                                <?php $k++; } ?>

                            </div>

                        </div>

                    <?php $i++; } ?>

                    </div>
                </div>
            <?php  } ?>

                <div class="table-responsive hidden" style="margin-top: 10px">
                    <input id="add-invoice-item" class="btn btn-info btn-xs fa fa-plus" name="add-new-item" onclick="addInputField('addRowtem');" value="Add New" type="button">
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Update Attribute</button>
                </div>
            </div>

        </form>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

        $(document).ready(function(){

            var maxField = 10; 
            var addButton = $('.add_button'); 
            var wrapper = $('.field_wrapper');
            var count = 0;

            $(addButton).on('click',function(){ 

                    var rowS="<div class='row' id='item_"+count+"'>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>Attribute Option</label>\n\
                                <input class='form-control' type='text' name='option_name[]' required>\n\
                            </div>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>Option Type</label>\n\
                                <select class='form-control option_type_"+count+"' name='option_type[]' id='option_type' onchange='addOption("+count+")'><option value=''>--Select--</option><option value='1'>Text</option><option value='2'>Option</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Price Type</label>\n\
                                <select class='form-control'  name='price_type["+count+"]'  ><option value='1'>$</option><option value='2'>%</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Price </label>\n\
                                <input class='form-control' type='text' name='price["+count+"]' >\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'> </label>\n\
                                <button  class='btn btn-danger delete_option' style='margin-top:20px;'>Delete</button>\n\
                            </div>\n\
                            <div class='col-md-12 col-md-offset-2'>\n\
                            </div>\n\
                        </div>";  
                    $(wrapper).append(rowS); 
               count++;
            });

            var a=0;
            $(wrapper).on('click', '.add_option_option', function(){

                var cls = $(this).prev();
                var id = $(this).parent().parent().attr('id').split("_")[1];
              
              
                var html="<div class='row' id='"+id+"_"+a+"'>\n\
                            <div class='form-group col-md-4'>\n\
                                <label for='attr_option' class='mb-2'>Attribute Option Option</label>\n\
                                <input class='form-control' type='text' name='option_option_name["+id+"][]' required>\n\
                            </div>\n\
                             <div class='form-group col-md-4'>\n\
                                <label for='attr_option' class='mb-2'>Option option Type</label>\n\
                                <select class='form-control option_option_type_"+id+"_"+a+"' name='option_option_type["+id+"][]' id='option_option_type' ><option value=''>--Select--</option><option value='1'>Text</option><option value='2'>Option</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-4'>\n\
                                <a style='margin-top:20px;' href='javascript:void(0);' class='btn btn-danger delete_option_option'>Delete</a>\n\
                            </div>\n\
                        </div>";   
                
     
                cls.append(html) 
                a++;
            });

            $(wrapper).on('click', '.delete_option_option', function(e){ 
                e.preventDefault();
                $(this).parent().parent().remove(); 
                a--; 
            });

            $(wrapper).on('click', '.delete_option', function(e){ 
                e.preventDefault();
                $(this).parent().parent().remove(); 
                count--; 
            });
        });




</script>