<style type="text/css">
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Product Attribute</h5>
        </div>
        <!-- end box / title -->
        <p class="mb-3 px-3">
            <!--            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Filter
                        </button>-->
            <a href="<?php echo base_url(); ?>add-attribute" class="btn btn-success mt-1" style="margin-top: -5px !important; ">Add Attribute</a>
        </p>
        <div class="collapsexxx px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>attribute-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3 attribute_name" name="attribute_name" placeholder="Attribute Name" value="<?php echo $attribute_name; ?>">
                            </div>                            
                            <span class="or_cls">-- OR --</span>
                            <div class="col-md-3">
                                <select name="category_name" class="form-control select2 category_name" data-placeholder="-- select category --">
                                    <option value=""></option>
                                    <?php foreach ($get_category as $category) { ?>
                                        <option value='<?php echo $category->category_id; ?>' <?php if ($category_name == $category->category_id) {
                                        echo 'selected';
                                    } ?>>
                                        <?php echo $category->category_name; ?>
                                        </option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2 text-left">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="attributekeyup_search()" placeholder="Search..." tabindex="">
                </div>
                <!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                    </ul>
                                </div>-->
            </div>          
        </div>
        <div class="table-responsive px-3" id="results_attribute">
            <div class="p-1">
                <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
                ?>
            </div>

            <table class="table table-bordered table-hover" id="att-tbl">
                <thead>
                    <tr>
                        <th>Position No.</th>
                        <th>Category Name</th>
                        <th>Attribute Name</th>
                        <th>Option</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($get_attribute_filter as $attribute) {
                        if ($attribute->attribute_type) {
                            $option = "SELECT * FROM attr_options WHERE attribute_id = $attribute->attribute_id";
                            $option_result = $this->db->query($option)->result();
                        }
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $attribute->position; ?></td>
                            <td><?php echo $attribute->category_name; ?></td>
                            <td><?php echo $attribute->attribute_name; ?></td>

                            <td>
                                <?php
                                if ($option_result != NULL) {
                                    foreach ($option_result as $key => $op) {
                                        $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                                        $option_option_result = $this->db->query($option_option)->result();
                                        ?>
                                        <li><?= $op->option_name ?>
                                            <ul>
                                                <?php
                                                foreach ($option_option_result as $key => $opop) {
                                                    $opopops = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();
                                                    
                                                    ?>
                                                    <li>
                                                        <a href="javascript:void(0)" <?= ($opop->type == 2 ? 'onclick="addOpOpOp(' . $opop->op_op_id . ')"' : '') ?> >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=><?= $opop->op_op_name ?></a>
                                                        <?php foreach (@$opopops as $key => $opopop) { ?>
                                                            <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=><?= @$opopop->att_op_op_op_name; ?> &nbsp;&nbsp;<!--<i class="fa fa-remove text-danger" onclick="addOpOpOpOp(' <?= @$opopop->op_op_op_id ?>')"></i>--></li>
                                                    <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </td>

                            <td class="width_140" valign="top" >
                                <a href="<?php echo base_url(); ?>attribute-edit/<?php echo $attribute->attribute_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a> 
                                <a href="<?= base_url('attribute-delete/') . $attribute->attribute_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure want to delete it')"><i class="fa fa-trash"></i></a>
                            </td>

                        </tr>
                <?php } ?>
                </tbody>

<?php if (empty($get_attribute_filter)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="5" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
            <?php } ?>
            </table>
<?php // echo $links;  ?>
        </div>
    </div>
</div>
<!-- end content / right -->

<?php $this->load->view($js) ?>
<script type="text/javascript">
    function attributekeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-attribute-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_attribute").html(r);
            }
        });
    }
</script>