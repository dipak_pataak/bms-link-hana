
<!-- end content / right -->
<script type="text/javascript">

        $(document).ready(function(){

            var maxField = 10; 
            var addButton = $('.add_button'); 
            var wrapper = $('.field_wrapper');
            var count = 0;

            $(addButton).on('click',function(){ 

                    var rowS="<div class='row' id='item_"+count+"'>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Attribute Option</label>\n\
                                <input class='form-control' type='text' name='option_name[]' required>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Option Type</label>\n\
                                <select class='form-control option_type_"+count+"' name='option_type[]' id='option_type' onchange='addOption("+count+")'><option value='0'>--Select--</option><option value='1'>Text</option><option value='2'>Option</option> <option value='3'>Multi text</option><option value='4'>Multi option</option> <option value='5'>Text+Fraction</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Price Type</label>\n\
                                <select class='form-control'  name='price_type[]'  ><option value='1'>$</option><option value='2'>%</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Price </label>\n\
                                <input class='form-control' type='text' name='price[]' >\n\
                            </div>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>Condition </label>\n\
                                <input class='form-control' type='text' name='condition[]' >\n\
                            </div>\n\
                            <div class='form-group col-md-1'>\n\
                                <label for='attr_option' class='mb-2'> </label>\n\
                                <button  class='btn btn-danger delete_option' style='margin-top:20px;'><i class='fa fa-trash'></i></button>\n\
                            </div>\n\
                            <div class='col-md-12 col-md-offset-2'>\n\
                            </div>\n\
                        </div>";  
                    $(wrapper).append(rowS); 
               count++;
            });


            var a=0;
            $(wrapper).on('click', '.add_option_option', function(){

                var cls = $(this).prev();
                var id = $(this).parent().parent().attr('id').split("_")[1];
                var tid = id+a;
              
                var html="<div class='row' id='"+id+"_"+a+"'>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2 text-primary'><b>Attribute Sub option</b></label>\n\
                                <input class='form-control' type='text' name='option_option_name["+id+"][]' required>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Sub option Type</label>\n\
                                <select class='form-control option_option_type_"+id+"_"+a+"' name='option_option_type["+id+"][]' id='option_option_type' onchange='addOptionOption("+id+","+a+")'><option value='0'>--Select--</option><option value='1'>Text</option><option value='2'>Option</option><option value='3'>Multi text</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Price Type</label>\n\
                                <select class='form-control'  name='op_op_price_type["+id+"][]'  ><option value='1'>$</option><option value='2'>%</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Sub option Price</label>\n\
                                <input class='form-control' type='text' name='op_op_price["+id+"][]'>\n\
                            </div>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>Sub option Condition</label>\n\
                                <input class='form-control' type='text' name='op_op_condition["+id+"][]'>\n\
                            </div>\n\
                            <div class='form-group col-md-1'>\n\
                                <a style='margin-top:20px;' href='javascript:void(0);' class='btn btn-danger delete_option_option'><i class='fa fa-trash'></i></a>\n\
                            </div>\n\
                            <div class='col-md-12 col-md-offset-2'>\n\
                            </div>\n\
                        </div>";   
                
     
                cls.append(html) 
                a++;
            });

            var x =0;
            $(wrapper).on('click', '.add_option_option_option', function(){

                var cls = $(this).prev();
                var id = $(this).parent().parent().attr('id').split("_")[0];
                var a = $(this).parent().parent().attr('id').split("_")[1];
                
              
                var html="<div class='row' id='"+id+"_"+a+"_"+x+"'>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'><b>Attribute Sub-Sub option</b></label>\n\
                                <input class='form-control' type='text' name='op_op_op_name["+id+"]["+a+"][]' required>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Sub-Sub option Type</label>\n\
                                <select class='form-control option_option_type_"+id+"_"+a+"' name='op_op_op_type["+id+"]["+a+"][]' id='option_option_type'><option value='0'>--Select--</option><option value='1'>Text</option><option value='2'>Option</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Price Type</label>\n\
                                <select class='form-control'  name='op_op_op_price_type["+id+"]["+a+"][]'  ><option value='1'>$</option><option value='2'>%</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>Sub-Sub Price</label>\n\
                                <input class='form-control' type='text' name='op_op_op_price["+id+"]["+a+"][]'>\n\
                            </div>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>Sub-Sub option Condition</label>\n\
                                <input class='form-control' type='text' name='op_op_op_condition["+id+"]["+a+"][]'>\n\
                            </div>\n\
                            <div class='form-group col-md-1'>\n\
                                <a style='margin-top:20px;' href='javascript:void(0);' class='btn btn-danger delete_option_option'><i class='fa fa-trash'></i></a>\n\
                            </div>\n\
                            <div class='col-md-12 col-md-offset-2'>\n\
                            </div>\n\
                        </div>";   
                
     
                cls.append(html) 
                x++;
            });




            $(wrapper).on('click', '.delete_option_option', function(e){ 
                e.preventDefault();
                $(this).parent().parent().remove(); 
                a--; 
            });

            $(wrapper).on('click', '.delete_option', function(e){ 
                e.preventDefault();
                $(this).parent().parent().remove(); 
                count--; 
            });
        });



   
    $(document).ready(function () {

        $('body').on('change', '#attribute_type', function () {

            var attribute_style = $("#attribute_type").val();
            if (attribute_style == '2' || attribute_style == '3' || attribute_style == '4') {
                $(".att_opt").removeClass("hidden");
            }

            if (attribute_style == '1') {
                $(".att_opt").addClass("hidden");
            }
            
        });

    });
   


    function addOption(id){

        var option_type = $(".option_type_"+id).val();

        if (option_type == '2'||option_type == '3' || option_type == '4' || option_type == '5') {

            $('.option_type_'+id).parent().next().next().next().next().next().children().remove();


            var cls = $('.option_type_'+id).parent().next().next().next().next().next();
            var html="<div></div>\n\
                    <a style='margin-bottom: 10px' href='javascript:void(0);'' class='btn btn-primary add_option_option' >Add Suboption</a>";   
            cls.append(html) 
        }

        if (option_type == '1' || option_type == '0') {
            $('.option_type_'+id).parent().next().next().next().next().next().children().remove();
        }
    }


    function addOptionOption(id,a){

        var tid = id+"_"+a;

        var option_type = $(".option_option_type_"+tid).val();

        if (option_type == '2'||option_type == '3') {

            $('.option_option_type_'+tid).parent().next().next().next().next().next().children().remove();


            var cls = $('.option_option_type_'+tid).parent().next().next().next().next().next();

            var html="<div></div>\n\
                    <a style='margin-bottom: 10px' href='javascript:void(0);'' class='btn btn-primary add_option_option_option' >Add Sub-Sub option</a>";   
            cls.append(html) 
        }

        if (option_type == '1') {
            $('.option_option_type_'+tid).parent().next().next().next().next().next().children().remove();
        }


    }

</script>