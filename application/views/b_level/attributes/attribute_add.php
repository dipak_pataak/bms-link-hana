
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-10">Product Attribute</h5>
        </div>
        <!-- end box / title -->     
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <form action="<?php echo base_url('b_level/Attribute_controller/save_attribute'); ?>" method="post" class="form-row px-3">
            
            <div class="col-md-12">

                <div class="form-group">
                    <label for="category" class="mb-2">Attribute Category</label>
                    <select name="category" class="form-control select2" id="category" data-placeholder='-- select one --'>
                        <option value=" ">None</option>
                        <?php foreach($categorys as $category){?>
                        <option value="<?=$category->category_id?>"><?=$category->category_name?></option>
                        <?php }?>
                    </select>
                </div>

                
                <div class="form-group">
                    <label class="mb-2">Attribute Name</label>
                    <input class="form-control" type="text" placeholder="Attribute name" name="attribute" required>
                </div>

                <div class="form-group">
                    <label class="mb-2">Attribute Position</label>
                    <input class="form-control" type="number" placeholder="Position" name="position">
                </div>

                <div class="form-group">
                    <label for="attribute_type" class="mb-2">Attribute Type</label>
                    <select name="attribute_type" class="form-control select2" id="attribute_type" data-placeholder='-- select one --'>
                        <option value=""></option>
                        <option value="1">Text</option>
                        <option value="2">Option</option>
                        <option value="3">Multi text</option>
                        
                    </select>
                </div>


                <div class="att_opt hidden" style="margin-top: 10px">
                    <a style="margin-bottom: 10px" href="javascript:void(0);" class="btn btn-primary add_button "><span class="glyphicon glyphicon-plus"></span>Add Options</a>
                    <div class="field_wrapper">
                    </div>
                </div>

                <div class="table-responsive hidden" style="margin-top: 10px">
                    <input id="add-invoice-item" class="btn btn-info btn-xs fa fa-plus" name="add-new-item" onclick="addInputField('addRowtem');" value="Add New" type="button">
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Add Attribute</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('b_level/attributes/att_add_js') ;?>

