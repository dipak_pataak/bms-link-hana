<style type="text/css">
    .modal-dialog {
        max-width: 850px;
    }
</style>

<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Add attribute 4th-Sub Option</h4>
            </div>

            <div class="modal-body">

                <div class="form-row">

                    <div class="col-md-12">
                        <?=form_open('#',array('id'=>'SaveOp','name'=>'formStylePrice'))?>

                        <div class="field_wrapper">

                            <div class="row">

                                <div class=" form-group col-md-2">
                                    <label class="">4th-Sub Option Name</label>
                                    <input type="text" name="option_name[]" class="form-control" required> 
                                </div>

                                <div class='form-group col-md-2'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Type</label>
                                    <select class='form-control ' name='op_op_op_op_type[]' id='option_option_type'><option value=''>--Select--</option><option value='1'>Text</option></select>
                                </div>

                                <div class='form-group col-md-2'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Price Type</label>
                                    <select class='form-control'  name='op_op_op_op_price_type[]'  ><option value='1'>$</option><option value='2'>%</option></select>
                                </div>
                                <div class='form-group col-md-2'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Price</label>
                                    <input class='form-control' type='text' name='op_op_op_op_price[]'>
                                </div>
                                <div class='form-group col-md-3'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Condition</label>
                                    <input class='form-control' type='text' name='op_op_op_op_condition[]'>
                                </div>



                                <div class="form-group col-md-1">
                                    <label class="col-form-label"></label>
                                    <a class="btn btn-xs btn-success add_opp" style="margin-top: 15px;"><i class="fa fa-plus"></i></a>
                                </div>

                            </div>

                        </div>

                        <input type="hidden" name="op_op_op_id" id="op_op_op_id" value="">
                          
                        <button type="submit" class="btn btn-success">Save</button><hr>
                       
                        <?php echo form_close();?>
                        
                    </div>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>



<script type="text/javascript">



    function addOpOpOpOp(att_op_op_op_op_id){

        var submit_url = "<?=base_url();?>b_level/Attribute_controller/option_option_option_option_delete/"+att_op_op_op_op_id.trim();
     
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function(res) {

            $("#att-tbl").load(" #att-tbl > *");
            //$("#cartItems").load(location.href+" #cartItems>*",""); 
                
            },error: function() {
                alert('error');
            }
        });
    }


    function addOpOpOp(op_op_op_id){
        $('#op_op_op_id').val(op_op_op_id);
        $('#myModal').modal('show');
    }


    // submit form and add data
    $("#SaveOp").on('submit',function(e){
        e.preventDefault();

            var submit_url = "b_level/Attribute_controller/option_option_option_option_save";
            $.ajax({
                type: 'POST',
                url: submit_url,
                data: $(this).serialize(),
                success: function(res) {
                
                    
                    if(res==='1'){
                        alert('Save successfully');
                    }
                    if(res==='2'){
                        alert('Internal error plese try again');
                    }
                        
                    $('#myModal').modal('hide'); 
                    $("#att-tbl").load(" #att-tbl > *");
                    
                },error: function() {
                    alert('error');
                }
            });
    });

        $(document).ready(function(){

            var maxField = 10; 
            var addButton = $('.add_opp'); 
            var wrapper = $('.field_wrapper');
            var count = 0;

            $(addButton).on('click',function(){ 

                    var rowS="<div class='row'>\n\
                                <div class='form-group col-md-2'>\n\
                                    <label class='label'>4th-Sub Option Name</label>\n\
                                    <input type='text' name='option_name[]' class='form-control' required>\n\
                                </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Type</label>\n\
                                <select class='form-control option_option_type' name='op_op_op_op_type[]' id='option_option_type'><option value=''>--Select--</option><option value='1'>Text</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Price Type</label>\n\
                                <select class='form-control'  name='op_op_op_op_price_type[]'  ><option value='1'>$</option><option value='2'>%</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Price</label>\n\
                                <input class='form-control' type='text' name='op_op_op_op_price[]'>\n\
                            </div>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Condition</label>\n\
                                <input class='form-control' type='text' name='op_op_op_op_condition[]'>\n\
                            </div>\n\
                                <div class='form-group col-md-1'>\n\
                                    <label class='col-form-label'></label>\n\
                                    <a class='btn btn-xs btn-danger remove_opp' style='margin-top: 15px;'><i class='fa fa-trash'></i></a>\n\
                                </div>\n\
                            </div>";  
                    $(wrapper).append(rowS); 
               count++;
            });


            $(wrapper).on('click', '.remove_opp', function(e){ 
                e.preventDefault();
                $(this).parent().parent().remove(); 
                count--; 
            });


        })





</script>