<table class="table table-bordered table-hover" id="att-tbl">
                <thead>
                    <tr>
                        <th>Position No.</th>
                        <th>Category Name</th>
                        <th>Attribute Name</th>
                        <th>Option</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($attribute_list as $attribute) {
                        if ($attribute->attribute_type) {
                            // get option
                            $option = "SELECT * FROM attr_options WHERE attribute_id = $attribute->attribute_id";
                            $option_result = $this->db->query($option)->result();
                        }
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $attribute->position; ?></td>
                            <td><?php echo $attribute->category_name; ?></td>
                            <td><?php echo $attribute->attribute_name; ?></td>

                            <td>
                                <?php
                                if ($option_result != NULL) {
                                    foreach ($option_result as $key => $op) {
                                        // get options option
                                        $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                                        $option_option_result = $this->db->query($option_option)->result();
                                        ?>
                            <li><?= $op->option_name ?>

                                <ul>
                                    <?php
                                    foreach ($option_option_result as $key => $opop) {
                                        $opopops = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();
                                        ?>

                                        <li>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=><?= $opop->op_op_name ?>
                                            <ul>
                                                <?php
                                                foreach ($opopops as $key => $opopop) {

                                                    $opopopops = $this->db->where('op_op_op_id', $opopop->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();
                                                    ?>
                                                    <li><a href="javascript:void(0)" <?= ($opopop->att_op_op_op_type == 2 ? 'onclick="addOpOpOp(' . $opopop->att_op_op_op_id . ')"' : '') ?> >
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=><?= @$opopop->att_op_op_op_name; ?> </a>
                                                        <?php foreach ($opopopops as $key => $opopopop) { ?>
                                                        <li>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=><?= @$opopopop->att_op_op_op_op_name; ?> &nbsp;&nbsp;<i class="fa fa-remove text-danger" onclick="addOpOpOpOp(' <?= $opopopop->att_op_op_op_op_id ?>')"></i></a>
                                                        </li>
                                                    <?php } ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                            </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                    </td>

                    <td class="width_140" valign="top" >
                        <a href="<?php echo base_url(); ?>attribute-edit/<?php echo $attribute->attribute_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a> 
                        <a href="<?= base_url('attribute-delete/') . $attribute->attribute_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure want to delete it')"><i class="fa fa-trash"></i></a>
                    </td>

                    </tr>
                <?php } ?>

                </tbody>
                <?php if (empty($attribute_list)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="5" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>