
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>
        <div class="title">
            <h5>Attribute Types</h5>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b_level/Attribute_controller/attribute_type_update/<?php echo $attribute_type_edit[0]['attribute_type_id']; ?>" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="panel">
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="types_name" class="col-sm-3 control-label">Types Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="types_name" value="<?php echo $attribute_type_edit[0]['attribute_type_name']; ?>" id="types_name" placeholder="Attribute Type" required tabindex="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Update" tabindex="7" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>            


        </div>        
    </div>
</div>
<!-- end content / right -->
