
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>
        <div class="title">
            <h5>Attribute Types</h5>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b_level/Attribute_controller/attribute_type_save" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="panel">
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="types_name" class="col-sm-3 control-label">Types Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="types_name" id="types_name" placeholder="Attribute Type" required tabindex="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Save" tabindex="7" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>            




            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No</th>
                        <th>Attribute Type</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($attribute_type_list)) {
                        $sl = 0 + $pagenum;
                        foreach ($attribute_type_list as $key => $value) {
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->attribute_type_name; ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url(); ?>b_level/Attribute_controller/attribute_type_edit/<?php echo $value->attribute_type_id; ?>" title="" class="btn btn-info btn-xs simple-icon-note"><i class="fa fa-edit"></i></a>
                                    <!--<a href="<?php echo base_url(); ?>menusetup-delete/<?php echo $value->attribute_type_id ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs simple-icon-trash"><i class="fa fa-trash-o"></i></a>-->
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table> 
            <?php echo $links; ?>

        </div>        
    </div>
</div>
<!-- end content / right -->
