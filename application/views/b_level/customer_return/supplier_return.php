
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <!-- box / title -->
                    <div class="title row">
                        <h5>Suppliers Return</h5>
                    </div>
                    <!-- end box / title -->
                    <p class="mb-3 px-3">
                        <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Filter
                        </button>
                    </p>
                    <div class="collapse px-3 mb-3" id="collapseExample">
                        <div class="border p-3">
                            <form class="form-horizontal" method="post">
                                <fieldset>

                                    <div class="row">

                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Invoice No.">
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Supplier Name">
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Product Name">
                                        </div>

                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn btn-sm btn-success default mt-3">Filter</button>
                                        </div>

                                    </div>

                                </fieldset>

                            </form>
                        </div>
                    </div>

                    <div class="px-3">

                        <table class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Invoice No</th>
                                    <th>Supplier Name</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Comment</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>1152</td>
                                    <td>john-112</td>
                                    <td>Roller Blind</td>
                                    <td>$1520.00</td>
                                    <td>Qty problem</td>
                                    <td>Accepted</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>1128</td>
                                    <td>james-132</td>
                                    <td>Faux Wooden Blind</td>
                                    <td>$953.00</td>
                                    <td>Damage replaced</td>
                                    <td>Exchanged</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>1165</td>
                                    <td>David-112</td>
                                    <td>Churchill Shutters</td>
                                    <td>$758.00</td>
                                    <td>Physical Damage</td>
                                    <td>Exchanged</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- end content / right -->
            