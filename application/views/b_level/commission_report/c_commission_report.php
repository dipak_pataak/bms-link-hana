<style type="text/css">
    .right_side{float: left; font-weight: bold; }
    .left_side{float: right; font-weight: bold; }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    a:hover{
        text-decoration: none;
    }
    .address{
        cursor: pointer;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/b_level/resources/css/daterangepicker.css" />
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>C Level Commission List</h5>
        </div>
        <!-- end box / title -->
       
        <div class="px-4 mt-3">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url('b_level/Commision_report_controller/search_c'); ?>" method="post" id="customerFilterFrm">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 employee_name" placeholder="Employee Name" name="search_employee_name" id="employee_name" value="<?=$this->session->userdata('search_employee_name')?>">
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control mb-3" id="search_daterange" name="search_daterange" value="<?=$this->session->userdata('search_daterange')?>" />
                            </div>
                            <div class="col-md-2 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn" name="Search" value="Search">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="table mt-3" id="result_search">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Commision_report_controller/search_c') ?>">
                <input type="hidden" name="action">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Employee Name</th>
                            <th>Date</th>
                            <th>Total Commission</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $sl = 0 + $pagenum;
                        foreach ($commission_list as $commission) {
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $commission->full_name; ?></td>
                                <td><?php echo $commission->created_date; ?></td>
                                <td><?php echo round($commission->total_commission,2); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <?php if (empty($commission_list)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="8" class="text-center  text-danger">No record found!</th>
                            </tr> 
                        </tfoot>
                    <?php } ?>
                </table>
            </form>
            <?php echo $links; ?>
        </div>
    </div>
</div>	

<script>
$(function() {
  $('#search_daterange').daterangepicker({
  	ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last Week': [moment().subtract(1, 'weeks'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  });
});
</script>