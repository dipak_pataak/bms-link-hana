
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box" style="height: 100%">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

        <div class="title">
            <h5>Assign User Role Update</h5>
        </div>
        <div class="px-3">

            <?php echo form_open_multipart("b_level/role_controller/assign_user_role_update/" . $edit_user_access_role->role_acc_id, array('class' => 'form-horizontal')) ?>

            <div class="form-group row">
                <label for="user_id" class="col-sm-3 col-form-label control-label">User Name</label>
                <div class="col-sm-9">
                    <select class="form-control select2" name="user_id" id="user_id">
                        <option value="">-- select one --</option>
                        <?php
                        foreach ($user_list as $user) {
                            if ($edit_user_access_role->user_id == $user->id) {
                                echo '<option selected value="' . $user->id . '">' . $user->first_name . " " . $user->last_name . " ->( " . $user->email . " ) " . '</option>';
                            } else {
                                echo '<option value="' . $user->id . '">' . $user->first_name . " " . $user->last_name . " ->( " . $user->email . " ) " . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <?php
            foreach ($assign_role as $role) {
                $role_id[] = $role->role_id;
            }
            ?>

            <div class="form-group row">
                <label for="role_id" class="col-sm-3 col-form-label control-label">Role</label>
                <div class="col-sm-9">
                    <select class="selectpicker form-control role_id" id="role_id" name="role_id[]" multiple data-live-search="true">
                        <?php foreach ($role_list as $val) { ?>
                            <option value="<?= $val->id ?>" <?php if (in_array($val->id, $role_id)) { echo 'selected'; }?>>
                                <?= ucwords($val->role_name); ?>
                            </option>
                        <?php } ?>
                    </select>
                    <?php foreach ($role_list as $val) { ?>
<!--                        <label class="radio-inline">
                            <input type="checkbox" id="role_id" name="role_id[]" value="<?php echo $val->id; ?>"
                            <?php
                            if (in_array($val->id, $role_id)) {
                                echo 'checked';
                            }
                            ?> >
                                   <?php echo $val->role_name; ?>
                        </label> -->
                    <?php } ?>
                </div>
            </div>

            <div class="form-group text-right" style="padding-right: 20px;">
                <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>
<!-- end content / right -->
