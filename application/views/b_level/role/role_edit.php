

<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <?php
            $message = $this->session->flashdata('message');
            if ($message != '') {
                echo $message;
            }
        ?>



        <div class="title">
            <h5>Edit Role Permissions</h5>
        </div>

        <!-- end box / title -->
        <form action="<?php echo base_url(); ?>b_level/role_controller/role_update" id="moduleFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="px-3">
                    <div class="form-group row">
                        <label for="role_name" class="col-sm-3 col-form-label">Role Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" value="<?php echo $roleInfo->role_name; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="role_description" class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="role_description" id="role_description" placeholder="Role Description"><?php echo $roleInfo->description; ?></textarea>
                        </div>
                    </div>
                <div class="float-right" style="margin: 15px 20px -20px 0px;">
                    <label for="select_deselect">
                        <span class="select_cls"><strong>Select / De-select</strong></span>
                        <input type="checkbox" id="select_deselect">
                    </label>
                </div>
                    <?php
                    $m = 0;
                    foreach ($modules as $module) {
                        $menu_item = $this->db->select('*')->from('b_menusetup_tbl')->where('module', $module->module)
                                        ->get()->result();
                        ?>
                        <input type="hidden" name="module[]" value="<?php echo $module->module; ?>">
                        <table class="table table-bordered table-hover">
                            <h2><?php echo str_replace("_", " ", ucwords($module->module)); ?></h2>
                            <thead>
                                <tr>
                                <th class="text-center" width="10%">Sl No</th>
                                <th class="" width="30%">Menu Title </th>
                                <th class="text-center" width="15%">Can Create &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_create_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_create_all" class="can_create_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                                <th class="text-center" width="15%">Can Read &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_read_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_read_all" class="can_read_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                                <th class="text-center" width="15%">Can Edit &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_edit_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_edit_all" class="can_edit_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                                <th class="text-center" width="15%">Can Delete &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_delete_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_delete_all" class="can_delete_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($menu_item)) {
                                    $sl = 0;
                                    foreach ($menu_item as $menu) {
                                        $ck_data = $this->db->select('*')
                                                        ->where('menu_id', $menu->id)
                                                        ->where('role_id', $roleInfo->id)->get('b_role_permission_tbl')->row();
                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo $sl + 1; ?></td>
                                            <td class="text-<?php echo ($menu->parent_menu ? 'right' : '') ?>"><?php echo str_replace("_", " ", ucwords($menu->menu_title)); ?></td>
                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="create[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_create == 1) ? "checked" : null) ?> id="create[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_create">
                                                    <label for="create[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="read[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_access == 1) ? "checked" : null) ?> id="read[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_read">
                                                    <label for="read[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td> 
                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="edit[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_edit == 1) ? "checked" : null) ?> id="edit[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_edit">
                                                    <label for="edit[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="delete[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_delete == 1) ? "checked" : NULL) ?> id="delete[<?php echo $m ?>]<?php echo $sl ?>"  class="sameChecked <?php echo $menu->module; ?>_can_delete">
                                                    <label for="delete[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td>

                                    <input type="hidden" name="menu_id[<?php echo $m ?>][<?php echo $sl ?>][]" value="<?php echo $menu->id ?>">

                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                <?php
                                $m++;
                            }
                            ?>
                            </tbody>
                        </table>
                    <?php }
                    ?>

                    <div class="form-group row">
                        <div class="col-md-2 pt-1">
                            <input type="hidden" name="role_id" value="<?php echo $roleInfo->id ?>">
                        <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-primary btn-sm text-white">Back</a>
                            <button type="submit" class="btn btn-info btn-sm module_btn">Update</button>
                        </div>
                    </div>
                
            </div>
        </form>

    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#select_deselect', function () {

            $(".sameChecked").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_create_all', function () {
            var create_value = $(this).val();
            $("." + create_value + "_can_create").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_read_all', function () {
            var read_value = $(this).val();
            $("." + read_value + "_can_read").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_edit_all', function () {
            var edit_value = $(this).val();
            $("." + edit_value + "_can_edit").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_delete_all', function () {
            var delete_value = $(this).val();
            $("." + delete_value + "_can_delete").prop('checked', $(this).prop('checked'));
        });
    });
</script>
