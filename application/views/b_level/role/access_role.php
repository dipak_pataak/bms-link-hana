


<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

        <div class="title">
            <h5>User Access Role</h5>
        </div>

        <div class="px-3">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>User Name</th>
                        <th>Role Name</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    // echo "<pre>";                            print_r($user_access_role);
                    if (!empty($user_access_role)) {
                        $sl = 0;
                        foreach ($user_access_role as $key => $value) {
                            $sql = "SELECT a.role_id, a.user_id, b.role_name FROM b_user_access_tbl a 
                            JOIN b_role_tbl b ON b.id = a.role_id 
                        WHERE a.user_id = '$value->user_id'";
//                            echo $sql;
                            $query = $this->db->query($sql)->result();
//                            echo "<pre>";   print_r($query);
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->first_name . " " . @$value->last_name; ?></td>
                                <td>
                                    <ul>
                                        <?php
                                        foreach ($query as $role) {
                                            echo "<li>" . $role->role_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo base_url(); ?>b_level/role_controller/edit_user_access_role/<?php echo $value->role_acc_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"> </i></a>
                                    <a href="<?php echo base_url(); ?>b_level/role_controller/delete_user_access_role/<?php echo $value->role_acc_id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="btn btn-xs btn-danger fa fa-trash"></i></a>

                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($user_access_role)) { ?>
                    <tfoot>
                        <tr>
                            <th class="text-danger text-center" colspan="6">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>        

    </div>
</div>
<!-- end content / right -->
