
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $message = $this->session->flashdata('message');
        if ($message)
            echo $message;
        ?>

        <div class="title">
            <h5>Role Permissions</h5>
        </div>

        <!-- end box / title -->
        <form action="<?php echo base_url('b_level/role_controller/role_save'); ?>" id="moduleFrm" method="post" enctype="multipart/form-data" class="form-horizontal">

            <div class="px-3">
                <div class="form-group row">
                    <label for="firstname" class="col-sm-3 col-form-label">Role Name *</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_description" class="col-sm-3 col-form-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="role_description" id="role_description" placeholder="Role Description"></textarea>
                    </div>
                </div>


                <div class="float-right" style="margin: 15px 20px -20px 0px;">
                    <label for="select_deselect">
                        <span class="select_cls"><strong>Select / De-select</strong></span>
                        <input type="checkbox" id="select_deselect">
                    </label>
                </div>
                <?php
                $m = 0;
                foreach ($modules as $module) {
                    $menu_item = $this->db->select('*')->from('b_menusetup_tbl')->where('module', $module->module)->where('status', 1)
                                    ->get()->result();
                    ?>
                    <input type="hidden" name="module[]" value="<?php echo $module->module; ?>">
                    <table class="table table-bordered table-hover">
                        <h2><?php echo str_replace("_", " ", ucwords($module->module)); ?></h2>
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">Sl No</th>
                                <th class="" width="30%">Menu Title </th>
                                <th class="text-center" width="15%">Can Create &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_create_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_create_all" class="can_create_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                                <th class="text-center" width="15%">Can Read &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_read_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_read_all" class="can_read_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                                <th class="text-center" width="15%">Can Edit &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_edit_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_edit_all" class="can_edit_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                                <th class="text-center" width="15%">Can Deelete &nbsp; &nbsp;
                                    <label for="<?php echo $module->module; ?>_can_delete_all" class="float-right">
                                        <span class="select_cls"><strong>All</strong></span>
                                        <input type="checkbox" id="<?php echo $module->module; ?>_can_delete_all" class="can_delete_all" value="<?php echo $module->module; ?>">
                                    </label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($menu_item)) {
                                $sl = 0;
                                foreach ($menu_item as $menu) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $sl + 1; ?></td>
                                        <td class="text-<?php echo ($menu->parent_menu ? 'right' : '') ?>"><?php echo str_replace("_", " ", ucwords($menu->menu_title)); ?></td>
                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="create[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="create[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_create">
                                                <label for="create[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="read[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="read[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_read">
                                                <label for="read[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td> 
                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="edit[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="edit[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_edit">
                                                <label for="edit[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td> 
                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="delete[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="delete[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_delete">
                                                <label for="delete[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td>
                                <input type="hidden" name="menu_id[<?php echo $m ?>][<?php echo $sl ?>][]" value="<?php echo $menu->id ?>">

                                </tr>
                                <?php
                                $sl++;
                            }
                            ?>
                            <?php
                            $m++;
                        }
                        ?>
                        </tbody>
                    </table>
                <?php }
                ?>

                <div class="form-group row">
                    <div class="col-md-2 pt-1">
                        <button type="submit" class="btn btn-info module_btn">Save & Next</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#select_deselect', function () {

            $(".sameChecked").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_create_all', function () {
            var create_value = $(this).val();
            $("." + create_value + "_can_create").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_read_all', function () {
            var read_value = $(this).val();
            $("." + read_value + "_can_read").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_edit_all', function () {
            var edit_value = $(this).val();
            $("." + edit_value + "_can_edit").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_delete_all', function () {
            var delete_value = $(this).val();
            $("." + delete_value + "_can_delete").prop('checked', $(this).prop('checked'));
        });
    });
</script>
