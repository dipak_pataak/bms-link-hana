
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $message = $this->session->flashdata('message');
        if ($message != '') {
            echo $message;
        }
        if ($error != '') {
            echo $error;
        }
        ?>

        <div class="title">
            <h5>Edit menu</h5>
        </div>

        <div class="px-3">

            <form action="<?php echo base_url(); ?>b_level/menusetup_controller/menusetup_update/<?php echo $single_menu_edit->id; ?>" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="panel">
                    <div class="panel-body">

                        <div class="form-group row">
                            <label for="menu_name" class="col-sm-3 text-right">Menu Name</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="menu_name" id="menu_name" placeholder="English Name" value="<?php echo $single_menu_edit->menu_title; ?>" readonly>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="korean_name" id="korean_name" placeholder="My Language" value="<?php echo $single_menu_edit->korean_name; ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="url" class="col-sm-3  text-right">Menu URL</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="url" id="url" placeholder="URL" value="<?php echo $single_menu_edit->page_url; ?>" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="module" class="col-sm-3  text-right">Module</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="module" id="module" placeholder="Enter Module" value="<?php echo $single_menu_edit->module; ?>" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="order" class="col-sm-3  text-right">Order</label>
                            <div class="col-sm-8">
                                <select  class="form-control select2" name="order" id="order" data-placeholder="-- select one --">
                                    <option value=""></option>
                                    <?php
                                    for ($i = 1; $i < 51; $i++) {
                                        if ($single_menu_edit->ordering == $i) {
                                            echo "<option selected value='$i'>$i</option>";
                                        } else {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="menu_type" class="col-sm-3  text-right">Menu Type <?php // echo $single_menu_edit->menu_type;    ?></label>
                            <div class="col-sm-8">
                                <select  class="form-control select2" name="menu_type" id="menu_type" onchange="menu_type_wise_parent_menu(this.value);" data-placeholder="-- select one --" tabindex="7">
                                    <option value=""></option>
                                    <option value="1" <?php
                                    if ($single_menu_edit->menu_type == 1) {
                                        echo 'selected';
                                    }
                                    ?>>Left Menu</option>
                                    <option value="2" <?php
                                    if ($single_menu_edit->menu_type == 2) {
                                        echo 'selected';
                                    }
                                    ?>>System Menu</option>
                                    <option value="3" <?php
                                    if ($single_menu_edit->menu_type == 3) {
                                        echo 'selected';
                                    }
                                    ?>>Top Menu</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="parent_menu" class="col-sm-3 text-right">Parent Menu</label>
                            <div class="col-sm-8">
                                <select  class="form-control select2" name="parent_menu" id="parent_menu" data-placeholder="-- select one --">
                                    <option value="0">None</option>
                                    <?php
                                    foreach ($parent_menu as $parent) {
                                        if ($single_menu_edit->parent_menu === $parent->id) {
                                            echo "<option selected value='$parent->id'>" . ucwords(str_replace('_', ' ', $parent->menu_title)) . "</option>'";
                                        } else {
                                            echo "<option value='$parent->id'>" . ucwords(str_replace('_', ' ', $parent->menu_title)) . "</option>'";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="icon" class="col-sm-3 text-right">Icon</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="icon" id="url" placeholder="Icon Class" value="<?php echo $single_menu_edit->icon; ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                                <input type="submit" id="" class="btn btn-success btn-large" name="add-user" value="Update" />
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>

        </div>        
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">

//    ========== its for menu_type_wise_parent_menu ===========
    function menu_type_wise_parent_menu(t) {
        $.ajax({
            url: "<?php echo base_url('b_level/Menusetup_controller/menu_type_wise_parent_menu'); ?>",
            type: 'post',
            data: {type_id: t},
            success: function (r) {
                r = JSON.parse(r);
//                    alert(r);
                $("#parent_menu").empty();
                $("#parent_menu").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $('#parent_menu').append($('<option>').text(typeval.menu_title).attr('value', typeval.id));
                });
            }
        });
    }
</script>