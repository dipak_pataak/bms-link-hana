
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <?php
                        $error = $this->session->flashdata('error');
                        $success = $this->session->flashdata('success');
                        if ($error != '') {
                            echo $error;
                        }
                        if ($success != '') {
                            echo $success;
                        }
                    ?>

                    <!-- box / title -->
                    <div class="title row">
                        <h5 class="col-sm-6">Role List</h5>
                    </div>
                     <a href="<?php echo base_url(); ?>b_level/role_controller/role_permission" class="btn btn-success ml-3 mb-1">New Role</a>
                    <!-- end box / title -->
                    <div class="px-3">
                        <table class="table table-bordered">
                            <thead>
                               <tr>
                                    <th>SL No</th>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($role_list)) {
                                    $sl = 0;
                                    foreach ($role_list as $key => $value) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->role_name; ?></td>
                                            <td><?php echo $value->description; ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url(); ?>b_level/role_controller/role_edit/<?php echo $value->id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                                <a href="<?php echo base_url(); ?>b_level/role_controller/role_delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if(empty($role_list)){ ?>
                            <tfoot>
                                <tr>
                                    <th class="text-danger text-center" colspan="6">Record not found!</th>
                                </tr>
                            </tfoot>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end content / right -->
            