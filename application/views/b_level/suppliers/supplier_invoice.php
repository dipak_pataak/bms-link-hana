
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Manage Invoice</h5>
            <div class="float-right mt-1 mr-2">
<!--                <a href="<?php echo base_url(); ?>purchase-entry" class="btn btn-success">Add Invoice</a>-->
            </div>
        </div>
        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
            <a href="<?php echo base_url(); ?>purchase-entry" class="btn btn-success" style="margin-top: -3px;">Add Invoice</a>
        </p>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Supplier ID">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Product">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Stock">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Phone">
                            </div>

                            <div class="col-md-4">
                                <input type="email" class="form-control mb-3" placeholder="email">
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="reset" class="btn btn-sm btn-danger default">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>

        <div class="px-3">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Supplier Name</th>
                        <th>Purchase id</th>
                        <th>Date</th>
                        <th>Total</th>
                        <th>Payment Type</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if ($get_supplier_invoice != NULL) {
                        $i = 1;
                        foreach ($get_supplier_invoice as $key => $val) {
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val->supplier_name; ?></td>
                                <td><?= $val->purchase_id; ?></td>
                                <td><?= date('M-d-Y', strtotime($val->date)); ?></td>
                                <td><?= $val->grand_total; ?></td>
                                <td><?= ($val->payment_type == 1 ? 'Cash' : 'Check'); ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url(); ?>view-purchase/<?= $val->purchase_id ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <!--<a href="edit-purchase/<?= $val->purchase_id ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="edit"><i class="fa fa-edit" aria-hidden="true"></i></a>-->
                                    <a href="<?php echo base_url(); ?>raw-material-return-purchase/<?= $val->purchase_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>
                                    <a href="<?php echo base_url(); ?>delete-purchase/<?= $val->purchase_id ?>" onclick="return confirm('Are you sure want to delete it? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
                <?php if(empty($get_supplier_invoice)){ ?>
                <tfoot>
                    <tr>
                        <th colspan="8" class="text-center text-danger">No record found!</th>
                    </tr> 
                </tfoot>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<!-- end content / right -->