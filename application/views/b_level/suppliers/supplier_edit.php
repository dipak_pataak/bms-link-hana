<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<style type="text/css">
    #content div.box {
        overflow: visible !important;
    }
    .pt-15{
        padding-top: 15px !important;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Edit Supplier</h5>
        </div>
        <!-- end box / title -->
        <?php
        $supplier_no = explode('-', $supplier_edit[0]['supplier_no']);
//        echo '<pre>';        print_r($supplier_edit); 
        ?>
        <form action="<?php echo base_url('b_level/Supplier_controller/supplier_update/' . $supplier_edit[0]['supplier_id']); ?>" method="post" class="form-row px-3">
                       <div class="form-group col-md-6">
                <label for="company_name" class="mb-2">Supplier’s Company</label>
                <input class="form-control" type="text" id="company_name" name="company_name" value="<?php echo $supplier_edit[0]['company_name']; ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="address" class="mb-2">Address <span class="text-danger"> * </span></label>
                <input class="form-control" type="text" name="address" id="address"  value="<?php echo $supplier_edit[0]['address']; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="supplier_name" class="mb-2">Contact Name <span class="text-danger"> * </span></label>
                <input class="form-control" type="text" id="supplier_name" name="supplier_name" value="<?php echo $supplier_edit[0]['supplier_name']; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="city" class="mb-2">City</label>
                <input class="form-control" type="text" id="city" name="city" value="<?php echo $supplier_edit[0]['city']; ?>">
            </div> 
            <div class="form-group col-md-6">
                <label for="supplier_sku" class="mb-2">Supplier SKU</label>
                <input class="form-control" type="text" id="supplier_sku" name="supplier_sku" value="<?php echo $supplier_edit[0]['supplier_sku']; ?>">
                <input class="form-control" type="hidden" id="supplier_no" name="supplier_no" value="<?php echo @$supplier_no[0] . "-" . @$supplier_no[1]; ?>" >
            </div>
            <div class="form-group col-md-6">
                <label for="state" class="mb-2">State</label>
                <input class="form-control" type="text" name="state" id="state" value="<?php echo $supplier_edit[0]['state']; ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="email" class="mb-2">Email <span class="text-danger"> * </span></label>
                <input class="form-control" type="email" id="email" name="email" value="<?php echo $supplier_edit[0]['email']; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="zip" class="mb-2">Zip</label>
                <input class="form-control" type="text" id="zip" name="zip" value="<?php echo $supplier_edit[0]['zip']; ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="phone" class="mb-2">Phone</label>
                <input class="form-control phone" type="text" id="phone" name="phone" value="<?php echo $supplier_edit[0]['phone']; ?>" placeholder="+1 (XXX) XXX-XXXX">
            </div>
            <div class="form-group col-md-6">
                <label for="country_code" class="mb-2">Country Code</label>
                <input class="form-control" type="text" id="country_code" name="country_code"  value="<?php echo $supplier_edit[0]['country_code']; ?>">
            </div> 
            <div class="form-group col-md-6">
                <label for="material_product" class="mb-2">Material/Product</label>
                <select class="selectpicker form-control" name="raw_material_id[]" id="raw_material_id" multiple data-live-search="true">
                    <?php
                  $raw_material = array();
                    foreach ($supplier_wise_material as $single_material) {
                        $raw_material[] = $single_material->raw_material_id;
                    }
                    foreach ($get_raw_material as $val) {
                        ?>
                        <option value="<?= $val->id ?>" <?php
                        if (in_array($val->id, $raw_material)) {
                            echo 'selected';
                        }
                        ?>><?= ucwords($val->material_name); ?></option>
                            <?php } ?>
                </select>
<!--                <div class="checkbox_area">
                    <?php
                    $raw_material = array();
                    foreach ($supplier_wise_material as $single_material) {
                        $raw_material[] = $single_material->raw_material_id;
                    }

                    foreach ($get_raw_material as $val) {
                        ?>
                        <label for="raw_material_id_<?php echo $val->id; ?>">
                            <input type="checkbox" name="raw_material_id[]" id="raw_material_id_<?php echo $val->id; ?>" value="<?php echo $val->id; ?>"
                            <?php
                            if (in_array($val->id, $raw_material)) {
                                echo 'checked';
                            }
                            ?>> 
                                   <?php echo ucwords($val->material_name); ?>
                        </label>
                    <?php } ?>
                </div>-->
            </div>            
            <div class="form-group col-md-6">
                <label for="previous_balance" class="mb-2">Previous Balance</label>
                <input class="form-control" type="text" id="previous_balance" name="previous_balance" value="<?php echo $supplier_edit[0]['previous_balance']; ?>">
            </div>
            <!--            <div class="form-group col-md-6">
                            <label for="stock" class="mb-2">Stock</label>
                            <input class="form-control" type="text" id="stock" name="stock" value="<?php echo $supplier_edit[0]['stock']; ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="price_item" class="mb-2">Price/Item</label>
                            <input class="form-control" type="text" id="price_item" name="price_item" value="<?php echo $supplier_edit[0]['price_item']; ?>">
                        </div>-->
            <div class="form-group col-md-12 text-right">
                <button type="submit" class="btn btn-success w-md m-b-5">Update Supplier</button>
            </div>
        </form>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
//    ============= its for  google geocomplete =====================
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });


    });
</script>