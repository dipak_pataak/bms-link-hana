<table class="table table-bordered text-center">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="15%">Supplier's Company</th>
            <th width="15%">Contact Name</th>
            <th width="18%">Material/Product</th>
            <th width="16%">Phone</th>
            <th width="10%">Email</th>
            <!--<th width="7%">Current-Balance</th>-->
            <th width="17%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sl = 0;
        foreach ($supplier_list as $supplier) {
            $this->db->select('a.id, a.material_name');
            $this->db->from('row_material_tbl a');
            $this->db->join('supplier_raw_material_mapping  b', 'b.raw_material_id = a.id');
            $this->db->where('b.supplier_id', $supplier->supplier_id);
            $supplier_material_results = $this->db->get()->result();
            $sl++
            ?>
            <tr>
                <td><?php echo $sl; ?></td>
                <td><?php echo $supplier->company_name; ?></td>
                <td><?php echo $supplier->supplier_name; ?></td>
                <td class="text-left">
                    <?php
                    $i = 0;
                    foreach ($supplier_material_results as $result) {
                        $i++;
                        echo "<ul>";
                        echo "<li>" . $i . ") " . $result->material_name . "</li>";
                        echo "</ul>";
                    }
                    ?>
                </td>
                <td>
                    <a href="tel:<?php echo $supplier->phone; ?>"><?php echo $supplier->phone; ?></a>
                </td>
                <td>
                    <a href="mailto:<?php echo $supplier->email; ?>"><?php echo $supplier->email; ?></a>
                </td>
                <!--<td>000</td>-->
                <td>
                    <a href="<?php echo base_url(); ?>supplier-invoice/<?php echo $supplier->supplier_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
                    <a href="<?php echo base_url(); ?>supplier-edit/<?php echo $supplier->supplier_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                    <a href="<?php echo base_url(); ?>supplier-delete/<?php echo $supplier->supplier_id; ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="Left" title="Delete" onclick="return confirm('Do you want to delete it!')"><i class="fa fa-trash"></i></a>
    <!--                                <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>-->
                </td>
            </tr>
        <?php } ?>
    </tbody>
    <?php if (empty($supplier_list)) { ?>
        <tfoot>
            <tr>
                <th colspan="9" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>