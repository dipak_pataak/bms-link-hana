<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box" style="height: 100%;">
        <!-- box / title -->
        <div class="title row">
            <h5>Add Suppliers</h5>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <form action="<?php echo base_url('b_level/Supplier_controller/supplier_save'); ?>" method="post" class="form-row px-3">
            <div class="form-group col-md-6">
                <label for="company_name" class="mb-2">Supplier’s Company</label>
                <input class="form-control" type="text" id="company_name" name="company_name">
            </div>
            <div class="form-group col-md-6">
                <label for="address" class="mb-2">Address</label>
                <input class="form-control" type="text" name="address" id="address" required>
            </div>
            <div class="form-group col-md-6">
                <label for="supplier_name" class="mb-2">Contact Name</label>
                <input class="form-control" type="text" id="supplier_name" name="supplier_name" required>
            </div>
            <div class="form-group col-md-6">
                <label for="city" class="mb-2">City</label>
                <input class="form-control" type="text" id="city" name="city" required>
            </div>
            <div class="form-group col-md-6">
                <label for="supplier_sku" class="mb-2">Supplier SKU</label>
                <input class="form-control" type="text" id="supplier_sku" name="supplier_sku" required>
            </div>
            <div class="form-group col-md-6">
                <label for="state" class="mb-2">State</label>
                <input class="form-control" type="text" name="state" id="state">
            </div>
            <div class="form-group col-md-6">
                <label for="email" class="mb-2">Email</label>
                <input class="form-control" type="email" id="email" name="email" onkeyup="check_email_keyup()">
                <span id="error"></span>
            </div>
            <div class="form-group col-md-6">
                <label for="zip" class="mb-2">Zip</label>
                <input class="form-control" type="text" id="zip" name="zip">
            </div> 
            <div class="form-group col-md-6">
                <label for="phone" class="mb-2">Phone</label>
                <input class="form-control phone" type="text" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX">
            </div>
            <div class="form-group col-md-6">
                <label for="country_code" class="mb-2">Country Code</label>
                <input class="form-control" type="text" id="country_code" name="country_code">
            </div> 
            <div class="form-group col-md-6">
                <label for="raw_material_id" class="mb-2">Material/Product</label>
                <select class="selectpicker form-control" id="raw_material_id" name="raw_material_id[]" multiple data-live-search="true">
                    <?php foreach ($get_raw_material as $val) { ?>
                        <option value="<?= $val->id ?>"><?= ucwords($val->material_name); ?></option>
                    <?php } ?>
                </select>
                <!--                <div class="checkbox_area">
                <?php foreach ($get_raw_material as $val) { ?>
                                                <label for="raw_material_id_<?php echo $val->id; ?>">
                                                    <input type="checkbox" name="raw_material_id[]" id="raw_material_id_<?php echo $val->id; ?>" value="<?php echo $val->id; ?>"> <?php echo ucwords($val->material_name); ?>
                                                </label>
                <?php } ?>
                                </div>-->
            </div>
            <div class="form-group col-md-6">
                <label for="previous_balance" class="mb-2">Previous Balance</label>
                <input class="form-control" type="text" id="previous_balance" name="previous_balance">
            </div>
            <!--            <div class="form-group col-md-6">
                            <label for="price_item" class="mb-2">Price/Item</label>
                            <input class="form-control" type="text" id="price_item" name="price_item">
                        </div>-->
            <div class="form-group col-md-12 text-right">
                <button type="submit" class="btn btn-success w-md m-b-5">Add Suppliers</button>
            </div>
        </form>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">

//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
//        console.log(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "get-check-supplier-unique-email",
            type: "post",
            data: data_string,
            success: function (s) {
                if (s != 0) {
                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }
//    ============= its for  google geocomplete =====================
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });


    });
</script>