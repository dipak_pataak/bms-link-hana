<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<style type="text/css">
    #content div.box {
        overflow: visible !important;
    }
    .pt-15{
        padding-top: 15px !important;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    a:hover{
        text-decoration: none;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box showSupplier" id="showSupplier">
        <div class="px-3 mb-3">
            <div class="box pt-15" style="height: 100%;">
                <!-- box / title -->
                <div class="title row">
                    <h5>Add Suppliers</h5>
                </div>
                <!-- end box / title -->
                <div class="p-1">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <form action="<?php echo base_url('b_level/Supplier_controller/supplier_save'); ?>" method="post" class="form-row px-3">
                    <div class="form-group col-md-6">
                        <label for="company_name" class="mb-2">Supplier’s Company</label>
                        <input class="form-control" type="text" id="company_name" name="company_name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="address" class="mb-2">Address <span class="text-danger"> * </span></label>
                        <input class="form-control" type="text" name="address" id="address" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="supplier_name" class="mb-2">Contact Name <span class="text-danger"> * </span></label>
                        <input class="form-control" type="text" id="supplier_name" name="supplier_name" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="city" class="mb-2">City</label>
                        <input class="form-control" type="text" id="city" name="city">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="supplier_sku" class="mb-2">Supplier SKU</label>
                        <input class="form-control" type="text" id="supplier_sku" name="supplier_sku" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="state" class="mb-2">State</label>
                        <input class="form-control" type="text" name="state" id="state">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email" class="mb-2">Email <span class="text-danger"> * </span></label>
                        <input class="form-control" type="email" id="email" name="email" onkeyup="check_email_keyup()" required>
                        <span id="error"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="zip" class="mb-2">Zip</label>
                        <input class="form-control" type="text" id="zip" name="zip">
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="phone" class="mb-2">Phone</label>
                        <input class="form-control phone" type="text" id="phone" name="phone" placeholder="+1 (XXX) XXX-XXXX">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="country_code" class="mb-2">Country Code</label>
                        <input class="form-control" type="text" id="country_code" name="country_code">
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="raw_material_id" class="mb-2">Material/Product</label>
                        <select class="selectpicker form-control" id="raw_material_id" name="raw_material_id[]" multiple data-live-search="true">
                            <?php foreach ($get_raw_material as $val) { ?>
                                <option value="<?= $val->id ?>"><?= ucwords($val->material_name); ?></option>
                            <?php } ?>
                        </select>
                        <!--                <div class="checkbox_area">
                        <?php foreach ($get_raw_material as $val) { ?>
                                                                            <label for="raw_material_id_<?php echo $val->id; ?>">
                                                                                <input type="checkbox" name="raw_material_id[]" id="raw_material_id_<?php echo $val->id; ?>" value="<?php echo $val->id; ?>"> <?php echo ucwords($val->material_name); ?>
                                                                            </label>
                        <?php } ?>
                                        </div>-->
                    </div>
                    <div class="form-group col-md-6">
                        <label for="previous_balance" class="mb-2">Previous Balance</label>
                        <input class="form-control" type="text" id="previous_balance" name="previous_balance">
                    </div>
                    <!--            <div class="form-group col-md-6">
                                    <label for="price_item" class="mb-2">Price/Item</label>
                                    <input class="form-control" type="text" id="price_item" name="price_item">
                                </div>-->
                    <div class="form-group col-md-12 text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Add Suppliers</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage Suppliers</h5>
        </div>
        <p class="" style="margin-left: 10px; margin-top: 15px;">
            <button class="btn btn-primary default mb-1" type="button" id="collapseExample_btn">
                Filter
            </button>
            <!--<a href="<?php echo base_url(); ?>add-supplier" class="btn btn-success mt-1" style="margin-top: -5px !important; ">Add</a>-->
            <button class="btn btn-success mb-1" type="button" id="addSupplier_btn">
                Add
            </button>
            <a href="javascript:void(0)" class="btn btn-danger mb-1 action-delete"onClick="return action_delete(document.recordlist)" >Delete</a>
        </p>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>supplier-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-2 sku" name="sku" placeholder="Enter Contact SKU">
                            </div><span class="or_cls">-- OR --</span>
                            <div class="col-md-3">
                                <input type="text" class="form-control mb-2 name" name="name" placeholder="Enter Contact Name">
                            </div><span class="or_cls">-- OR --</span>
                            <div class="col-md-3">
                                <input type="text" class="form-control mb-2 email" name="email" placeholder="Enter Email">
                            </div><span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-2 phone" name="phone" placeholder="Enter Phone No">
                            </div>
                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="supplierkeyup_search()" placeholder="Search..." tabindex="">
                </div>
                <!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                    </ul>
                                </div>-->
            </div>          
        </div>
        <div class="px-3" id="results_supplier">
            <div class="p-1">
                <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
                ?>
            </div>
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Supplier_controller/manage_action') ?>">
            <input type="hidden" name="action">
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th width="2%">#</th>
                        <th width="15%">Supplier's Company</th>
                        <th width="15%">Contact Name</th>
                        <th width="18%">Material/Product</th>
                        <th width="16%">Phone</th>
                        <th width="10%">Email</th>
                        <!--<th width="7%">Current-Balance</th>-->
                        <th width="17%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($supplier_list as $supplier) {
                        $this->db->select('a.id, a.material_name');
                        $this->db->from('row_material_tbl a');
                        $this->db->join('supplier_raw_material_mapping  b', 'b.raw_material_id = a.id');
                        $this->db->where('b.supplier_id', $supplier->supplier_id);
                        $supplier_material_results = $this->db->get()->result();
                        $sl++
                        ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $supplier->supplier_id; ?>" class="checkbox_list">  
                            </td>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $supplier->company_name; ?></td>
                            <td><?php echo $supplier->supplier_name; ?></td>
                            <td class="text-left">
                                <?php
                                $i = 0;
                                foreach ($supplier_material_results as $result) {
                                    $i++;
                                    echo "<ul>";
                                    echo "<li>" . $i . ") " . $result->material_name . "</li>";
                                    echo "</ul>";
                                }
                                ?>
                            </td>
                            <td>
                                <a href="tel:<?php echo $supplier->phone; ?>"><?php echo $supplier->phone; ?></a>
                            </td>
                            <td>
                                <a href="mailto:<?php echo $supplier->email; ?>"><?php echo $supplier->email; ?></a>
                            </td>
                            <!--<td>000</td>-->
                            <td>
                                <a href="<?php echo base_url(); ?>supplier-invoice/<?php echo $supplier->supplier_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
                                <a href="<?php echo base_url(); ?>supplier-edit/<?php echo $supplier->supplier_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url(); ?>supplier-delete/<?php echo $supplier->supplier_id; ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="Left" title="Delete" onclick="return confirm('Do you want to delete it!')"><i class="fa fa-trash"></i></a>
    <!--                                <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>-->
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($supplier_list)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="9" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>
            </form>
            <?php echo $links; ?>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    $(document).ready(function () {
        $("#showSupplier").hide();
        $("body").on("click", "#addSupplier_btn", function () {
            $("#showSupplier").slideToggle("slow");
            $("#collapseExample").hide();
        });
        $("#collapseExample").hide();
        $("body").on("click", "#collapseExample_btn", function () {
            $("#collapseExample").slideToggle("slow");
            $("#showSupplier").hide();
        });

    });



//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
//        console.log(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "get-check-supplier-unique-email",
            type: "post",
            data: data_string,
            success: function (s) {
                if (s != 0) {
                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }
    function supplierkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-supplier-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_supplier").html(r);
            }
        });
    }

//    ============= its for  google geocomplete =====================
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });

    });
//        ========= close google address api ==============
</script>