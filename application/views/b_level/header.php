

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BMS Link B</title>
        <meta charset="utf-8">
        <!-- stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/reset.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/toster/toastr.css" media="screen">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/custom_style.css" media="screen">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/colors/blue.css">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/select2.min.css">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/select2-bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap-datepicker3.min.css" />
        <!-- timepicker -->
        <link href="<?php echo base_url() ?>assets/b_level/resources/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
        <!--============ its for multiselects ============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap-select.css" />


        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>

        <style type="text/css">

            .pac-container:after{
                content:none !important;
            }
            .ct-topbar__list {
                margin-bottom: 0px;
            }
            .ct-language__dropdown{
                padding-top: 8px;
                max-height: 0;
                overflow: hidden;
                position: absolute;
                top: 110%;
                left: -3px;
                -webkit-transition: all 0.25s ease-in-out;
                transition: all 0.25s ease-in-out;
                width: 100px;
                text-align: center;
                padding-top: 0;
                z-index:200;
            }
            .ct-language__dropdown li{
                background: #222;
                padding: 5px;
            }
            .ct-language__dropdown li a{
                display: block;
            }
            .ct-language__dropdown li:first-child{
                padding-top: 10px;
                border-radius: 3px 3px 0 0;
            }
            .ct-language__dropdown li:last-child{
                padding-bottom: 10px;
                border-radius: 0 0 3px 3px;
            }
            .ct-language__dropdown li:hover{
                background: #444;
            }
            .ct-language__dropdown:before{
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                margin: auto;
                width: 8px;
                height: 0;
                border: 0 solid transparent;
                border-right-width: 8px;
                border-left-width: 8px;
                border-bottom: 8px solid #222;
            }
            .ct-language{
                position: relative;
                /*background: #00aced;*/
                color: #fff;
                padding: 5px 0;

            }
            .ct-language:hover .ct-language__dropdown{
                max-height: 200px;
                padding-top: 8px;
            }
            .list-unstyled {
                padding-left: 0;
                list-style: none;
                float: right;
            }

            .navbar-brand>img {
                padding-top: 11px;
                width: 130px;
            }

            .skiptranslate{
                display: none;

            }
            body{
                top:0px !important;
            }

        </style>


    </head>


    <body>
        <?php
//        echo $this->session->userdata('user_type');
//        if ($this->session->userdata('session_id') == '') {
//            redirect('b-level');
//        }
        ?>
        <!--        <div id="colors-switcher" class="color">
                    <a href="" class="blue" title="Blue"></a>
                    <a href="" class="green" title="Green"></a>
                    <a href="" class="brown" title="Brown"></a>
                    <a href="" class="purple" title="Purple"></a>
                    <a href="" class="red" title="Red"></a>
                    <a href="" class="greyblue" title="GreyBlue"></a>
                </div>-->

        <!-- header -->
        <div id="header">
            <div id="header-outer">
                <?php
                if ($this->session->userdata('isAdmin') == 1) {
                    $level_id = $this->session->userdata('user_id');
                } else {
                    $level_id = $this->session->userdata('admin_created_by');
                }
                $user_id = $level_id = $this->session->userdata('user_id');
                if ($level_id == $user_id) {
                    $level_id = $level_id;
                } else {
                    $level_id = $user_id;
                }
//                echo $level_id;
                $company_profile = $this->db->select('*')
                                ->from('company_profile')
                                ->where('user_id', $level_id)
                                ->get()->result();
                $myaccount_profile = $this->db->select('*')
                                ->from('user_info')
                                ->where('id', $level_id)
                                ->get()->result();
//                echo '<pre>'; print_r($myaccount_profile);die();           
                ?>
                <!-- logo -->
                <div id="logo">
                    <h1>
                        <a href="<?php echo base_url(); ?>b-level-dashboard" title="BMS Link">
                            <!--<img src="<?php echo base_url(); ?>assets/b_level/uploads/appsettings/<?php echo @$company_profile[0]->logo; ?>" alt="BMS Link" />-->
                            <img src="<?php echo base_url(); ?>assets/b_level/resources/images/bmslink-logo.png" alt="BMS Link" />
                        </a>
                    </h1>
                </div>
                <!-- end logo -->

                <?php
                $this->db->select('count(a.id) total_id');
                $this->db->from('customer_commet_tbl a');
                $this->db->where('a.is_visited', 0);
                $this->db->where('a.comment_to', $level_id);
                $customer_commet_count = $this->db->get()->num_rows();



                $date = date('Y-m-d');
                $this->db->select("b_notification_tbl.*,CONCAT(user_info.first_name, '.', user_info.last_name) as fullname, user_info.user_image");
                $this->db->join('user_info', 'user_info.id=b_notification_tbl.created_by', 'left');
                $this->db->where('date', $date);
                $this->db->order_by('id', 'DESC');
                $result = $this->db->get('b_notification_tbl')->result();

                $user_id = $this->session->userdata('user_id');
                $user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();
                ?>
                <ul id="user">
                    <?php
                    $allsystem_menu = $this->db->select('*')->from('b_menusetup_tbl')->where('menu_type', 2)->where('parent_menu', 104)
                                    ->where('status', 1)->order_by('ordering', 'asc')->get()->result();
                    $i = 0;
                    foreach ($allsystem_menu as $menu) {
                        $i++;
                        $menu_title = '';
                        if ($user_info[0]->language == 'English') {
                            $menu_title = $menu->menu_title;
                        } elseif ($user_info[0]->language == 'Korean') {
                            $menu_title = $menu->korean_name;
                        } else {
                            $menu_title = $menu->menu_title;
                        }
                        ?>
                        <li class="first">
                            <a href="<?php echo base_url(); ?><?php echo $menu->page_url; ?>">
                                <?php
                                if ($i == 1) {
                                    if (@$myaccount_profile[0]->user_image) {
                                        ?>
                                        <img src="<?php echo base_url(); ?>assets/b_level/uploads/users/<?php echo $myaccount_profile[0]->user_image; ?>" alt="BMS Link" class="" style="width: 17px;" />
                                        <?php
                                    }
                                }
                                ?>
                                <span style="/*line-height: 18px;*/  vertical-align: top; color: #000000; font-weight: bold">
                                    <?php echo str_replace("_", " ", ucfirst($menu_title)) ?>
                                </span>
                            </a>
                            <?php if ($i == 2) { ?>
                                <i class="<?php echo $menu->icon; ?>"></i>
                                <span class="n-count">
                                    <?= count($result); ?>
                                </span>
                            <?php } ?>
                        </li>
                    <?php } ?>

                    <li class="ct-language" style="color: #000000; font-weight: bold;">Language <i class="fa fa-arrow-down"></i>
                        <ul class="list-unstyled ct-language__dropdown">
                            <li><a href="#googtrans(en|en)" class="lang-en lang-select" id="en" data-lang="en"><img src="<?= base_url('assets/b_level/uploads/img/flag-usa.png') ?>" alt="USA"></a></li>
                            <li><a href="#googtrans(ko|ko)" class="lang-es lang-select" id="ko" data-lang="ko"><img src="<?= base_url('assets/b_level/uploads/img/korea.png') ?>" alt="Korea"></a></li>
                        </ul>
                    </li>
                    <!--</ul>-->
                    <li class="last"><a href="<?php echo base_url(); ?>b_level/Auth_controller/b_level_logout"  style="color: #000000; font-weight: bold">Logout</a></li>
                </ul>
                <!-- end user -->
                <div id="header-inner">
                    <div id="home">
                        <a href="<?php echo base_url(); ?>b-level-dashboard"></a>
                    </div>

                    <div class="col-sm-2" style="position: absolute;  right: 375px;   top: 10px;">

                        <form action="<?php echo base_url('top-search-customer-order-info'); ?>" method="post" style="display: flex;">
                            <input type="text" placeholder="Search.." name="keyword" required style=" padding: 0 10px;
                                   background: #e9f4ff;
                                   border: 0;
                                   height: 25px;
                                   border-radius: 3px;">
                            <button type="submit" style="margin-left: -5px;
                                    height: 25px;
                                    border: 0;
                                    background: #356a9f;
                                    color: #fff;"><i class="fa fa-search"></i></button>
                        </form>

                    </div>


                    <!-- quick -->
                    <ul id="quick">
                        <?php
                        $alltop_menu = $this->db->select('*')->from('b_menusetup_tbl')->where('menu_type', 3)
                                        ->where('parent_menu', 107)->where('status', 1)->order_by('ordering', 'asc')->get()->result();
                        foreach ($alltop_menu as $single_menu) {
                            if ($this->permission->check_label($single_menu->menu_title)->access()) {
                                if ($user_info[0]->language == 'English') {
                                    $menu_title = $single_menu->menu_title;
                                } elseif ($user_info[0]->language == 'Korean') {
                                    $menu_title = $single_menu->korean_name;
                                } else {
                                    $menu_title = $single_menu->menu_title;
                                }
                                $parent_id = $single_menu->id;
                                $sub_menu = $this->db->select('*')
                                                ->from('b_menusetup_tbl')
                                                ->where('parent_menu =', $parent_id)
                                                ->order_by('ordering', 'asc')
                                                ->get()->result();
                                if (!empty($sub_menu)) {
                                    ?>                        
                                    <li>
                                        <a href="" title="Pages">
                                            <span><?php echo str_replace("_", " ", ucfirst($menu_title)); ?></span>
                                        </a>
                                        <ul>
                                            <?php
                                            foreach ($sub_menu as $single_sub_menu) {
                                                if ($this->permission->check_label($single_sub_menu->menu_title)->access()) {
                                                    if ($user_info[0]->language == 'English') {
                                                        $sub_menu_title = $single_sub_menu->menu_title;
                                                    } elseif ($user_info[0]->language == 'Korean') {
                                                        $sub_menu_title = $single_sub_menu->korean_name;
                                                    } else {
                                                        $sub_menu_title = $single_sub_menu->menu_title;
                                                    }
                                                    $submenu_id = $single_sub_menu->id;
                                                    $sub_sub_menu = $this->db->select('*')
                                                                    ->from('b_menusetup_tbl')
                                                                    ->where('parent_menu =', $submenu_id)
                                                                    ->where('status', 1)
                                                                    ->order_by('ordering', 'asc')
                                                                    ->get()->result();
                                                    if (!empty($sub_sub_menu)) {
                                                        ?>
                                                        <li>
                                                            <a href="#" class="childs"><?php echo str_replace("_", " ", ucfirst($sub_menu_title)) ?></a>
                                                            <ul>
                                                                <?php
                                                                foreach ($sub_sub_menu as $single_sub_sub_menu) {
                                                                    if ($this->permission->check_label($single_sub_sub_menu->menu_title)->access()) {
                                                                        if ($user_info[0]->language == 'English') {
                                                                            $sub_sub_menu_title = $single_sub_sub_menu->menu_title;
                                                                        } elseif ($user_info[0]->language == 'Korean') {
                                                                            $sub_sub_menu_title = $single_sub_sub_menu->korean_name;
                                                                        } else {
                                                                            $sub_sub_menu_title = $single_sub_sub_menu->menu_title;
                                                                        }
                                                                        ?>
                                                                        <li>
                                                                            <a href="<?php echo base_url(); ?><?php echo $single_sub_sub_menu->page_url; ?>">
                                                                                <?php echo str_replace("_", " ", ucfirst($sub_sub_menu_title)) ?>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li><a href="<?php echo base_url(); ?><?php echo $single_sub_menu->page_url; ?>">
                                                                <?php echo str_replace("_", " ", ucfirst($sub_menu_title)) ?>
                                                            </a></li>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                <?php } else {
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?><?php echo $single_menu->page_url; ?>">
                                            <span><?php echo str_replace("_", " ", ucfirst($menu_title)) ?></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                        }
                        ?>

                        <!--                        <li>
                        <?php if ($this->permission->check_label('supplier', 'read')->access()) { ?> 
                                                                                                                                                        <a href="<?php echo base_url(); ?>supplier-list"><span class="normal">Suppliers</span></a>
                        <?php } ?>
                                                </li>
                                                <li>
                        <?php if ($this->permission->check_label('cost_factor', 'read')->access()) { ?> 
                                                                                                                                                        <a href="<?php echo base_url(); ?>b-cost-factor"><span>Cost Factor</span></a>
                        <?php } ?>
                                                </li>
                                                <li>
                        <?php if ($this->permission->module('account', 'read')->access()) { ?>
                                                                                                                                                        <a href="" title="Pages"><span>Accounting</span></a>
                                                                                                                                                        <ul>
                                                                                                                                                            <li>
                            <?php if ($this->permission->check_label('account_chart', 'read')->access()) { ?>
                                                                                                                                                                                                                                                                    <a href="<?php echo base_url(); ?>b-account-chart">Account Chart</a>
                            <?php } ?>
                                                                                                                                                            </li>
                                                                                                                                                            <li>
                            <?php if ($this->permission->check_label('purchase_entry', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                    <a href="<?php echo base_url(); ?>purchase-entry">Purchase Entry</a>
                            <?php } ?>
                                                                                                                                                            </li>
                                                                                                                                                            <li>
                            <?php if ($this->permission->check_label('purchase_list', 'read')->access()) { ?>
                                                                                                                                                                                                                                                                    <a href="<?php echo base_url(); ?>purchase-list">Purchase List</a>
                            <?php } ?>
                                                                                                                                                            </li>
                                                                                                                                                            <li>
                            <?php if ($this->permission->check_label('voucher', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                    <a href="#" class="childs">Voucher</a>
                                                                                                                                                                                                                                                                    <ul>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('debit_voucher', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-debit-voucher">Debit Voucher</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('credit_voucher', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-credit-voucher">Credit Voucher</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('journal_voucher', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-journal-voucher">Journal Voucher</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('contra_voucher', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-contra-voucher">Contra Voucher</a>
                                <?php } ?></li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('voucher_approval', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-voucher-approval">Voucher Approval</a>
                                <?php } ?></li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('voucher_reports', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-voucher-reports">Voucher Reports</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                    </ul>
                            <?php } ?>
                                                                                                                                                            </li>
                                                                                                                                                            <li>
                            <?php if ($this->permission->check_label('account_reports', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                    <a href="#" class="childs">Account Reports</a>
                                                                                                                                                                                                                                                                    <ul>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('bank_book', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-bank-book">Bank Book</a>
                                <?php } ?> 
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('cash_book', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-cash-book">Cash Book</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('cash_flow', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-cash-flow">Cash Flow</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('general_ledger', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-general-ledger">General Ledger</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('profit_loss', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-profit-loss">Profit Loss</a>
                                <?php } ?>
                                                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                                                        <li>
                                <?php if ($this->permission->check_label('trial_balance', 'read')->access()) { ?> 
                                                                                                                                                                                                                                                                                                                                                                                <a href="<?php echo base_url(); ?>b-trial-ballance">Trial Ballance</a>
                                <?php } ?></li>
                                                                                                                                                                                                                                                                    </ul>
                            <?php } ?>
                                                                                                                                                            </li>
                                                                                                                                                        </ul>
                        <?php } ?>
                                                </li>
                                                <li>
                                                    <a href="" title="Settings"><span>Settings </span></a>
                                                    <ul>
                                                        <li>
                        <?php if ($this->permission->check_label('company_profile_setting', 'read')->access()) { ?> 
                                                                                                                                                                <a href="<?php echo base_url(); ?>profile-setting">Company Profile Setting</a>
                        <?php } ?>
                                                        </li>
                                                        <li><a href="<?php echo base_url(); ?>gateway">Payment Settings</a></li>
                                                        <li><a href="<?php echo base_url(); ?>b-change-password">Change Password</a></li>
                                                    </ul>
                                                </li>-->
                    </ul>
                    <!-- end quick -->
                    <div class="corner tl"></div>
                    <div class="corner tr"></div>
                </div>
            </div>
        </div>
        <!-- end header -->
