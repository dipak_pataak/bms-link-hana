
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Trial Ballance</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <?= form_open_multipart('b-trial-balance-report') ?>
            <div class="row" id="">
                <div class="col-sm-6">

                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">From Date</label>
                        <div class="col-sm-8">
                            <input type="text" name="dtpFromDate" value="" placeholder="From Date" class="datepicker form-control" required="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">To Date</label>
                        <div class="col-sm-8">
                            <input type="text"  name="dtpToDate" value="" placeholder="To Date" class="datepicker form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-8">
                            <input type="checkbox" id="chkWithOpening" name="chkWithOpening" size="40"/>&nbsp;&nbsp;&nbsp;<label for="chkWithOpening">With Details</label>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Find</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>