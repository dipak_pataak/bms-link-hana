
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>General Ledger</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <?= form_open_multipart('b-accounts-report-search') ?>
            <div class="row" id="">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">GL Head</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="cmbGLCode" id="cmbGLCode" data-placeholder="-- select one --">
                                <option></option>
                                <?php
                                foreach ($general_ledger as $g_data) {
                                    ?>
                                    <option value="<?php echo $g_data->HeadCode; ?>"><?php echo $g_data->HeadName; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">Transaction Head</label>
                        <div class="col-sm-8">
                            <select name="cmbCode" class="form-control select2" id="ShowmbGLCode" data-placeholder="-- select one --">

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">From Date</label>
                        <div class="col-sm-8">
                            <input type="text" name="dtpFromDate" value="" placeholder="Date" class="datepicker form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">To Date</label>
                        <div class="col-sm-8">
                            <input type="text"  name="dtpToDate" value="" placeholder="Date" class="datepicker form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-8">
                            <input type="checkbox" id="chkIsTransction" name="chkIsTransction" size="40"/>&nbsp;&nbsp;&nbsp;<label for="chkIsTransction">With Details</label>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Find</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#cmbGLCode').on('change',function(){
           var Headid=$(this).val();
            $.ajax({
                 url: '<?php echo site_url('b-general-led'); ?>',
                type: 'POST',
                data: {
                    Headid: Headid
                },
                success: function (data) {
                   $("#ShowmbGLCode").html(data);
                }
            });

        });
    });


</script>