
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="title row">
            <h5 class="col-sm-6">Voucher Approval</h5>
            <div class="col-sm-6 text-right">
                <!--<a href="<?php echo base_url(); ?>add-b-customer" class="btn btn-success btn-sm m-1">Add</a>-->
            </div>
        </div>
        <!-- end box / title -->
        <div class="p-3">
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Voucher No</th>
                        <th>Remark</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($aprrove))  ?>
                    <?php $sl = 1; ?>
                    <?php foreach ($aprrove as $approve) { ?>
                        <tr>
                            <td><?php echo $sl++; ?></td>
                            <td><?php echo $approve->VNo; ?></td>
                            <td><?php echo $approve->Narration; ?></td>
                            <td><?php echo $approve->Debit; ?></td>
                            <td><?php echo $approve->Credit; ?></td>
                            <td>
                                <a href="<?php echo base_url("b-isactive/$approve->VNo/active") ?>" onclick="return confirm('Are you sure?')" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Inactive">Approve</a>
                                <a href="<?php echo base_url("b-voucher-edit/$approve->VNo") ?>" class="btn btn-info btn-sm" title="Update"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- end content / right -->