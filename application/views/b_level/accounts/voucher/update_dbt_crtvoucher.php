
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="title row">
            <h5 class="col-sm-6">Debit Voucher Update</h5>
            <div class="col-sm-6 text-right">
                <!--<a href="<?php echo base_url(); ?>add-b-customer" class="btn btn-success btn-sm mt-1">Add</a>-->
            </div>
        </div>
        <!-- end box / title -->
        <form class="p-3" action="<?php echo base_url(); ?>update-b-debit-voucher" method="post">
            <div class="form-group row">
                <label for="vo_no" class="col-sm-2 col-form-label">Voucher No</label>
                <div class="col-sm-4">
                    <input type="text" name="txtVNo" id="txtVNo" value="<?php echo $credit_info->VNo ?>" class="form-control" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="ac" class="col-sm-2 col-form-label">Debit Account Head</label>
                <div class="col-sm-4">
                    <select name="cmbDebit" id="cmbDebit" class="form-control select2">
                        <option value='1020101'>Cash In Hand</option>
                        <?php foreach ($crcc as $cracc) { ?>
                            <option value="<?php echo $cracc->HeadCode; ?>" <?php
                            if ($credit_info->COAID == $cracc->HeadCode) {
                                echo 'selected';
                            }
                            ?>><?php echo $cracc->HeadName ?></option>
                                <?php } ?>

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="date" class="col-sm-2 col-form-label">Date</label>
                <div class="col-sm-4">
                    <input type="text" name="dtpDate" id="dtpDate" class="form-control datepicker" value="<?php echo $credit_info->VDate; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="txtRemarks" class="col-sm-2 col-form-label">Remark</label>
                <div class="col-sm-4">
                    <textarea  name="txtRemarks" id="txtRemarks" class="form-control"><?php echo $credit_info->Narration; ?></textarea>
                </div>
            </div>
            <div class="table-responsive" style="margin-top: 10px">
                <table class="table table-bordered table-hover" id="debtAccVoucher">
                    <thead>
                        <tr>
                            <th class="text-center">Account Name</th>
                            <th class="text-center">Code</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="debitvoucher">
                        <?php
                        $sl = 1;
                        foreach ($dbvoucher_info as $debitvoucher) {
                            ?>
                            <tr>
                                <td class="" style="width: 200px;">
                                    <select name="cmbCode[]" id="cmbCode_<?php echo $sl; ?>" class="form-control select2" onchange="load_code(this.value,<?php echo $sl; ?>)">
                                                    <?php foreach ($acc as $acc1) { ?>
                                                        <option value="<?php echo $acc1->HeadCode; ?>"  <?php
                                                        if ($debitvoucher->COAID == $acc1->HeadCode) {
                                                            echo 'selected';
                                                        }
                                                        ?>><?php echo $acc1->HeadName; ?></option>
                                                            <?php } ?>
                                                </select>

                                </td>
                                <td>
                                    <input type="text" name="txtCode[]" value="<?php echo $debitvoucher->COAID; ?>" class="form-control "  id="txtCode_<?php echo $sl; ?>" >
                                </td>
                                <td>
                                    <input type="text" name="txtAmount[]" value="<?php echo $debitvoucher->Debit; ?>" class="form-control total_price text-right"  id="txtAmount_<?php echo $sl; ?>" onkeyup="calculation(<?php echo $sl; ?>)" >
                                </td>
                                <td class="text-center">
                                    <button  class="btn btn-danger default" type="button" value="Delete" onclick="deleteRow(this)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            <?php
                            $sl++;
                        }
                        ?> 
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <input type="button" class="btn btn-info default" name="add_more" onClick="addaccount('debitvoucher');"  value="Add more">
                            </td>
                            <td colspan="1" class="text-right">
                                <label for="reason" class="  col-form-label">Total</label>
                            </td>
                            <td class="text-right">
                                <input type="text" id="grandTotal" class="form-control text-right " name="grand_total" value="<?php echo $credit_info->Credit; ?>" readonly="readonly" />
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>
            <div class="form-group row">

                <div class="col-sm-12 text-right">

                    <input type="submit" class="btn btn-success default btn-large" name="save" value="Update" tabindex="9"/>

                </div>
            </div>
        </form> 


    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

    function load_code(id, sl) {
        $.ajax({
            url: "<?php echo site_url('b-debit-voucher-code/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {

                $('#txtCode_' + sl).val(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function addaccount(divName) {
        var row = $("#debtAccVoucher tbody tr").length;
        var count = row + 1;
        var limits = 500;
        var tabin = 0;
        if (count == limits)
            alert("You have reached the limit of adding " + count + " inputs");
        else {
            var newdiv = document.createElement('tr');
            var tabin = "cmbCode_" + count;
            var tabindex = count * 2;
            newdiv = document.createElement("tr");

            newdiv.innerHTML = "<td> <select name='cmbCode[]' id='cmbCode_" + count + "' class='form-control  select2-single' onchange='load_code(this.value," + count + ")'><option value=''>--select one -- </option><?php foreach ($acc as $acc2) { ?><option value='<?php echo $acc2->HeadCode; ?>'><?php echo $acc2->HeadName; ?></option><?php } ?></select></td><td><input type='text' name='txtCode[]' class='form-control'  id='txtCode_" + count + "' ></td><td><input type='text' name='txtAmount[]' class='form-control total_price' id='txtAmount_" + count + "' onkeyup='calculation(" + count + ")'></td><td class='text-center'><button style='text-align: right;' class='btn btn-danger red btn-xs' type='button' value='Delete' onclick='deleteRow(this)'><i class='fa fa-trash'></i></button></td>";
            document.getElementById(divName).appendChild(newdiv);
            document.getElementById(tabin).focus();
            count++;

            $("select.form-control:not(.dont-select-me)").select2({
                placeholder: "-- select one --",
                allowClear: true
            });
        }
    }

    function calculation(sl) {

        var gr_tot = 0;
        $(".total_price").each(function () {
            isNaN(this.value) || 0 == this.value.length || (gr_tot += parseFloat(this.value))
        });

        $("#grandTotal").val(gr_tot.toFixed(2, 2));
    }

    function deleteRow(e) {
        var t = $("#debtAccVoucher > tbody > tr").length;
        if (1 == t)
            alert("There only one row you can't delete.");
        else {
            var a = e.parentNode.parentNode;
            a.parentNode.removeChild(a)
        }
        calculation()
    }

</script>