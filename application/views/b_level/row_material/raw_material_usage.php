<?php $this->load->view('b_level/row_material/raw_materialusagejs.php'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="p-1">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- box / title -->
        <div class="title row">
            <h5>Raw Material Usage</h5>
        </div>
        <!-- end box / title -->
        <form action="<?php echo base_url('raw-material-usage-save'); ?>" method="post" class="px-3" id="purchase_frm">
            <div class="row" id="supplier_info">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="order_id" class="col-sm-3 col-form-label">Order ID</label>
                        <div class="col-sm-9">
                            <select name="order_id" class="form-control select2" tabindex="1" data-placeholder="-- select one --" required>
                                <option value=""></option>
                                <?php
                                foreach ($get_b_level_quatation as $b_order) {
                                    echo "<option value='$b_order->order_id'>$b_order->order_id</option>";
                                }
                                foreach ($get_quatation as $order) {
                                    echo "<option value='$order->order_id'>$order->order_id</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="date" class="col-sm-3 col-form-label">Date </label>
                        <div class="col-sm-9">
                            <input type="text" name="date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" tabindex="2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="table-responsive col-sm-12" style="margin-top: 10px;">
                    <table class="table table-bordered table-hover" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center" width="15%">Material</th>
                                <th class="text-center" width="15%">Color</th>
                                <th class="text-center" width="15%">Pattern</th>
                                <th class="text-center" width="10%">Quantity</th>
                                <th class="text-center" width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <tr>
                                <td>
                                    <select  id='rmtt_id_1' onchange='service_cals(1)' name='rmtt_id[]' class='form-control select2' tabindex='3' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                        <?php foreach ($rmtts as $val) { ?>
                                            <option value='<?= $val->id ?>'> <?= $val->material_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>

                                <td>
                                    <select  id='color_id_1' onchange="color_cals(1)" name='color_id[]' class='form-control select2' tabindex='4' data-placeholder='-- select one --' required>
                                        <option value=''>-- select one --</option>
                                        <?php // foreach ($colors as $val) { ?>
                                            <!--<option value='<?= $val->id ?>'> <?= $val->color_name; ?></option>-->
                                        <?php // } ?>
                                    </select>
                                </td>

                                <td>
                                    <select  id='pattern_model_id_1' name='pattern_model_id[]' class='form-control select2' tabindex='5' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                        <?php // foreach ($patern_model as $val) { ?>
                                            <!--<option value='<?= $val->pattern_model_id ?>'> <?= $val->pattern_name; ?></option>-->
                                        <?php // } ?>
                                    </select>
                                </td>
                                <td class="text-right">
                                    <input type="text" name="quantity[]"  class="form-control text-center" onkeyup="quantity_check('1')" id="quantity_1" tabindex="6" min="0">
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>

                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="5" rowspan="1">
                                    <input id="add-invoice-item" class="btn btn-info" name="add-new-item" onclick="addInputField('addinvoiceItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">
                                </td>
                            </tr>
                        </tfoot>

                    </table>          
                </div>

            </div>
            <br/>
            <div class="form-group text-right mt-2">
                <input type="submit" id="add_purchase" class="btn btn-primary default btn-sm" name="add-usage" value="Save "/>
            </div>
        </form>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

</script>