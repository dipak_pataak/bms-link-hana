<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 258px;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage raw material</h5>
            <div class="col-sm-6 float-right text-right">
                <!--                <a href="add-row-material" class="btn btn-success mt-1 " id="add_new_color">Add New Material</a>-->
            </div>                
        </div>
        <div class="p-1">
                     <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>

        <div class="">
            <button type="button" class="btn btn-success  btn-sm mb-2" data-toggle="modal" data-target="#materialFrm" style="margin-left: 15px; font-size: 12px;">Add New Material</button>
            <!-- Modal -->
            <div class="modal fade" id="materialFrm" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Raw Material Information</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open('b_level/row_materials/save', array('class' => 'form-row px-3', 'id' => '')); ?>
                            <div class="col-md-12 px-4">
                                <div class="form-group row">
                                    <label for="material_name" class="col-md-3 control-label">Material Name <span class="text-danger"> * </span></label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="material_name" placeholder="Row material name" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="material_name" class="col-md-3 control-label">Pattern</label>
                                    <div class="col-md-6">
                                        <select name="pattern_model_id" id="Pattern" class="form-control select2" data-placeholder="-- select one --">
                                            <option value="">-- select one --</option>
                                            <?php foreach ($patern_model as $val) { ?>
                                                <option value="<?= $val->pattern_model_id ?>"><?= $val->pattern_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>  
                                </div>  
                                <div class="form-group row">
                                    <label for="color_id" class="col-md-3 control-label">Color</label>
                                    <div class="col-md-6">
                                        <select class="selectpicker form-control" id="color_id" name="color_id[]" multiple data-live-search="true">
                                            <?php foreach ($colors as $val) { ?>
                                                <option value="<?= $val->id ?>"><?= ucwords($val->color_name); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <!--            <div class="form-group">
                                                <label for="color_id" class="mb-3">Color</label>
                                                <select name="color_id" id="color_id" class="form-control select2" data-placeholder="-- select one --">
                                                    <option value=""></option>
                                <?php foreach ($colors as $val) { ?>
                                                                                                                        <option value="<?= $val->id ?>"><?= $val->color_name; ?></option>
                                <?php } ?>
                                                </select>
                                                <div class="color_checkbox">
                                <?php foreach ($colors as $val) { ?>
                                                                                                                    <label for="color_id_<?php echo $val->id; ?>">
                                                                                                                        <input type="checkbox" name="color_id[]" id="color_id_<?php echo $val->id; ?>" value="<?php echo $val->id; ?>"> <?php echo ucwords($val->color_name); ?>
                                                                                                                    </label>
                                <?php } ?>
                                                </div>
                                            </div>  -->

                                <div class="form-group row">
                                    <label  for="uom" class="col-md-3 control-label">UOM</label>
                                    <div class="col-md-6">
                                        <select name="uom" id="uom" class="form-control select2" data-placeholder="-- select one --">
                                            <option value="">-- select one --</option>
                                            <?php foreach ($get_uom_list as $val) { ?>
                                                <option value="<?= $val->uom_id ?>"><?= $val->uom_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>  
                                </div>  
                            </div>


                            <div class="col-lg-8 px-4">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success w-md m-b-5">Add material</button>
                                </div>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:void(0)" class="btn btn-success  btn-sm mb-2"  data-toggle="modal" data-target="#rawMaterialBulk" style="font-size: 12px; ">Bulk Upload</a>
            <!-- Modal -->
            <div class="modal fade" id="rawMaterialBulk" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <!--<h4 class="modal-title">Modal Header</h4>-->
                        </div>
                        <div class="modal-body">
                            <a href="<?php echo base_url('assets/b_level/csv/raw_material_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                            <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>
                            <?php echo form_open_multipart('raw-material-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                            <div class="form-group row">
                                <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                                <div class="col-xs-6">
                                    <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group  text-right">
                                <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                            </div>

                            </form>
                        </div>
                        <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                    </div>
                </div>
            </div>
            <a href="javascript:void(0)" style="font-size: 12px; " class="btn btn-danger btn-sm mb-2 action-delete" onClick="return action_delete(document.recordlist)" >Delete</a>
        </div>


        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="rawmaterialkeyup_search()" placeholder="Search..." tabindex="">
                </div>
<!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                    </ul>
                </div>-->
            </div>          
        </div>
        <form class="p-3" name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Row_materials/manage_action') ?>">
            <input type="hidden" name="action">
            <table class="table table-bordered mb-3" id="results_rawmaterial">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th>Sl no.</th>
                        <th>Material name</th>
                        <th>Pattern</th>
                        <th>Color name</th>
                        <th>UOM</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $i = 1;
                    if (!empty($row_materials)) {
                        foreach ($row_materials as $key => $val) {
                            $sql = "SELECT a.id, a.color_name FROM color_tbl a 
                                        JOIN raw_material_color_mapping_tbl b ON b.color_id = a.id 
                                        WHERE b.raw_material_id = $val->id";
                            $results = $this->db->query($sql)->result();
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $val->id; ?>" class="checkbox_list">  
                                </td>
                                <td><?= $i++ ?></td>
                                <td><?= $val->material_name; ?></td>
                                <td><?= $val->pattern_name; ?></td>
                                <td>
                                    <?php
                                    $sl = 0;
                                    foreach ($results as $result) {
                                        $sl++;
                                        echo "<ul>";
                                        echo "<li>" . $sl . " . " . ucwords($result->color_name) . "</li>";
                                        echo "<ul>";
                                    }
                                    ?>
                                </td>
                                <td><?= $val->uom_name; ?></td>
                                <td width="100">
                                    <a href="edit-row-material/<?= $val->id; ?>" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="<?= $val->id ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                    <!--<a href="javascript:void(0)" class="btn btn-warning default btn-sm" onclick="material_edit_form(<?php echo $val->id; ?>);"><i class="fa fa-pencil"></i></a>-->
                                    <a href="delete-row-material/<?= $val->id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>

                            <?php
                        }
                    }
                    ?>


                </tbody>
                <?php if (empty($row_materials)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>
        </form>
        <div class="modal fade" id="material_model_info" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Material Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="material_info">

                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end content / right -->


<!-- end content / right -->
<script type="text/javascript">

    $(document).ready(function () {

        $('#add_new_color').on('click', function () {
            $('#add_color')[0].reset();
            $("#add_color").attr("action", 'b_level/color_controller/save_color')

            $('#myColor').modal('show');
            $('.modal-title').text('Add New Color');

        });


        $('.edit_color').on('click', function () {

            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/color_controller/get_color/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_color')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="color_name"]').val(data.color_name);
                    $('[name="color_number"]').val(data.color_number);

                    $("#add_color").attr("action", 'b_level/color_controller/update_color')

                    $('#myColor').modal('show');
                    $('.modal-title').text('Update Color');
                    $('#save').text('Update Color');


                }, error: function () {

                }
            });

        });


    });

//  function material_edit_form(id) {
//        $.post("<?php echo base_url(); ?>edit-row-material/" + id, function (t) {
//            $("#material_info").html(t);
//            $('#material_model_info').modal('show');
//        });
//    }
   function rawmaterialkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-rawmaterial-search",
            type: 'post',
            data: { keyword: keyword} ,
//            data: {'keyword: keyword'},
            success: function (data) {
//                console.log(r);
                $("#results_rawmaterial").html(data);
            }
        });
    }
</script>



