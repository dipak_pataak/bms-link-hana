<script type="text/javascript">

//    var count = 2;
//    limits = 500;
// ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#addinvoiceItem tr").length;
        var count = row + 1;
        var limits = 500;
        if (count == limits) {
            alert("You have reached the limit of adding" + count + "inputs");
        } else {
            var a = "rmtt_id_" + count, e = document.createElement("tr");

            e.innerHTML = "<td>\n\
                            <select  id='rmtt_id_" + count + "' onchange='service_cals(" + count + ")' name='rmtt_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
                                <option value=''>-- select one --</option>\n\
<?php foreach ($rmtts as $val) { ?> <option value='<?php echo $val->id ?>'> <?php echo $val->material_name; ?></option><?php } ?>  </select>\n\
                            </td>\n\
                            <td>\n\
                            <select  id='color_id_" + count + "'  onchange='color_cals(" + count + ")' name='color_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
                                <option value=''>-- select one --</option>\n\
                            </select>\n\
                            </td>\n\
                            <td>\n\
                            <select  id='pattern_model_id_" + count + "' name='pattern_model_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>>\n\
                                <option value=''>-- select one --</option>\n\
                            </select>\n\
                            </td>\n\
                <td><input type='text' class='form-control' name='quantity[]' id='quantity_" + count + "' min='0' placeholder='' onkeyup='quantity_check(" + count + ")'  style='text-align:center'></td>\n\
                <td class='text-center'>\n\
                <button class='btn btn-danger' type='button' value='Delete' onclick='deleteRow(this)'><i class='fa fa-trash-o'></i></button></td>\n\
                ",
                    document.getElementById(t).appendChild(e), document.getElementById(a).focus(), count++,
                    $('.select2').select2();
        }
    }
// ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalinvoice > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
        }
        calculateSum()
    }

//Calcucate Invoice Add Items
//    function quantity_calculate(item) {
//        var available_qnt = $("#available_qnt_" + item).val();
//        var product_quantity = $("#product_quantity_" + item).val();
//        var product_rate = $("#product_rate_" + item).val();
//        var product_discount = $("#product_discount_" + item).val();
//        var total_price = $("#total_price_" + item).val();
//        var invoice_discount = $("#invoice_discount").val();
//        var total_discount = $("#total_discount_" + item).val();
//        if (product_quantity > 0 && product_rate > 0) {
//            var total_amount = product_quantity * product_rate;
//            $("#total_price_" + item).val(total_amount);
//        }
//        if (product_discount > 0) {
//            $("#total_price_" + item).val(total_amount - product_discount);
//        }
//        calculateSum();
//    }
//
//
//    function calculateSum() {
//        var t = 0,
//                a = 0,
//                e = 0,
//                o = 0,
//                p = 0;
//        $(".total_price").each(function () {
//            isNaN(this.value) || 0 == this.value.length || (e += parseFloat(this.value))
//        }),
//                $(".product_discount").each(function () {
//            isNaN(this.value) || 0 == this.value.length || (p += parseFloat(this.value))
//        }),
//                $("#grandTotal").val(e.toFixed(2));
//        var gt = $("#grandTotal").val();
//        var invoiceDiscount = $("#invoice_discount").val();
//        var ttl_discount = +invoiceDiscount + +p.toFixed(2, 2);
//        $("#total_discount").val(ttl_discount);
//        var grandTotals = e.toFixed(2) - invoiceDiscount;
//        $("#grandTotal").val(grandTotals);
//        invoice_paidamount();
//    }
//
////Invoice Paid Amount
//    function invoice_paidamount() {
//        var t = $("#grandTotal").val(),
//                a = $("#paidAmount").val(),
//                e = t - a;
//        $("#dueAmmount").val(e.toFixed(2, 2))
//    }
//============== its for quantity_check ============
    function quantity_check(item) {
//        alert($("#quantity_" + item).val());
        var material_id = $("#rmtt_id_" + item).val();
        var color_id = $("#color_id_" + item).val();
        var pattern_model_id = $("#pattern_model_id_" + item).val();
        var quantity = $("#quantity_" + item).val();
//        if ($("#rmtt_id_" + item).val() == '') {
//            alert("Material must be required");
////            $("#rmtt_id_" + item).val('');
//            return false;
//        } else if ($("#color_id_" + item).val() == '') {
//            alert("Color must be required");
////            $("#color_id_" + item).val('');
//            return false;
//        } else if ($("#pattern_model_id_" + item).val() == '') {
//            alert("Pattern must be required");
////            $("#pattern_model_id_" + item).val('');
//            return false;
//        } else {
//        alert(quantity);
        $.ajax({
            url: "b_level/Row_materials/quantity_check/",
            type: "POST",
            data: {material_id: material_id, color_id: color_id, pattern_model_id: pattern_model_id, quantity: quantity},
            success: function (r) {
                if (r != '') {
                    alert(r);
                }
            }
        });
//        }
    }

    function service_cals(item) {
        var invoice_form = $("#purchase_frm").serializeArray();
        var raw_material_id = $("#rmtt_id_" + item).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>get-material-info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
//                console.log(obj);
                $("#color_id_" + item).empty();
                $("#color_id_" + item).html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
                    $('#color_id_' + item).append($('<option>').text(typevar.color_name).attr('value', typevar.id));
                });
            },
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>get-material-wise-pattern-info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
                $("#pattern_model_id_" + item).empty();
//                $("#pattern_model_id_" + item).html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
//                    console.log(typevar);
                    $('#pattern_model_id_' + item).append($('<option>').text(typevar.pattern_name).attr('value', typevar.pattern_model_id));
                });
            },
        });
    }
//============= its for material, color and pattern wise stock summation when change color_cals function =============
    function color_cals(item) {
        var raw_material_id = $("#rmtt_id_" + item).val();
        var color_id = $("#color_id_" + item).val();
        var pattern_model_id = $("#pattern_model_id_" + item).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>get-material-color-pattern-wise-stock-qnt/" + raw_material_id + "/" + color_id + "/" + pattern_model_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
//                console.log(obj.in_quantity);
                $('#available_qnt_' + item).val(obj.available_quantity);
//                $('#product_rate_' + item).val(obj.out_quantity);
            },
        });
    }

//    function flight_calculate(item) {
//        var invoice_form = $("#invoice_frm").serializeArray();
//        var service_id = $("#service_id_" + item).val();
//        var flight_id = $("#flight_id_" + item).val();
//        $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>get-service-info/" + service_id + "/" + flight_id,
//            success: function (data) {
//                var obj = jQuery.parseJSON(data);
////                        alert(obj.total_service);
//                $('#available_qnt_' + item).val(obj.total_service);
//
//            },
//        });
//    }

    var count = 2,
            limits = 500;
</script>