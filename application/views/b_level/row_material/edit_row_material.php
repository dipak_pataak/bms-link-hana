<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--============ its for multiselects ============-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<style type="text/css">
    .color_checkbox{
        font-size: 15px;
        overflow: scroll;
        height: 100px;
    }
</style>
<!-- content / right -->
<div id="right">

    <div class="box new_product" style="height: 100%">

        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <!-- box / title -->            
        <div class="title row">
            <h5 class="col-sm-6">Update raw material</h5>
        </div>
        <!-- end box / title -->
        <?php echo form_open('b_level/row_materials/update', array('class' => 'form-row px-3', 'name' => 'edit_matt')); ?>
        <div class="offset-3 col-lg-6 px-4">

            <div class="form-group">
                <label for="product_code" class="mb-2">Material Name</label>
                <input class="form-control" type="text" name="material_name" value="<?= $mtt->material_name ?>" required>
            </div>

            <div class="form-group">
                <label class="mb-2">Pattern/Model</label>
                <select name="pattern_model_id" id="Pattern" class="form-control select2" data-placeholder="-- select one --">
                    <option value=""></option>
                    <?php foreach ($patern_model as $val) { ?>
                        <option value="<?= $val->pattern_model_id ?>"><?= $val->pattern_name; ?></option>
                    <?php } ?>
                </select>
            </div>  
            <div class="form-group">
                <label class="mb-2">Color</label>
                <?php //  dd($material_id_wise_color); ?>
                <select class="selectpicker form-control" name="color_id[]" multiple data-live-search="true">
                    <?php
                    foreach ($material_id_wise_color as $single) {
                        $mapping_color[] = $single->color_id;
                    }
                    if ($mapping_color) {
                        foreach ($colors as $val) {
                            ?>
                            <option value="<?= $val->id ?>" <?php
                            if (in_array($val->id, $mapping_color)) {
                                echo 'selected';
                            }
                            ?>>
                                <?= ucwords($val->color_name); ?></option>
                            <?php
                        }
                    } else {
                        foreach ($colors as $val) {
                            ?>
                            <option value="<?= $val->id ?>">
                            <?= ucwords($val->color_name); ?></option>
                        <?php }
                    }
                    ?>

                </select>
            </div>
            <div class="form-group">
                <label class="mb-2" for="uom">Unit of Measurement</label>
                <select name="uom" id="uom" class="form-control select2" data-placeholder="-- select one --">
                    <option value="">-- select one --</option>
                    <?php foreach ($get_uom_list as $val) { ?>
                        <option value="<?= $val->uom_id ?>" <?php
                        if ($mtt->uom == $val->uom_id) {
                            echo 'selected';
                        }
                        ?>><?= $val->uom_name; ?></option>
<?php } ?>
                </select>
            </div>  
            <!--            <div class="form-group">
                            <label for="color_id" class="mb-3">Color</label>
                            <select name="color_id" id="color_id" class="form-control select2" data-placeholder="-- select one --">
                                <option value=""></option>
            <?php foreach ($colors as $val) { ?>
                                                                                               <option value="<?= $val->id ?>"><?= $val->color_name; ?></option>
            <?php } ?>
                            </select>
                            <div class="color_checkbox">
            <?php
            foreach ($material_id_wise_color as $single) {
                $mapping_color[] = $single->color_id;
            }
            foreach ($colors as $val) {
                ?>
                                                                            <label for="color_id_<?php echo $val->id; ?>">
                                                                                <input type="checkbox" name="color_id[]" id="color_id_<?php echo $val->id; ?>" value="<?php echo $val->id; ?>" <?php
                if (in_array($val->id, $mapping_color)) {
                    echo 'checked';
                }
                ?>>
                <?php echo ucwords($val->color_name); ?>
                                                                            </label>
<?php } ?>
                            </div>
                        </div>  -->
        </div>
        <input type="hidden" name="id" value="<?= $mtt->id; ?>">


        <div class="col-lg-6 px-4">
            <div class="form-group text-right">
                <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                <button type="submit" class="btn btn-success w-md m-b-5">Update material</button>
            </div>
        </div>

<?php echo form_close(); ?>
    </div>

</div>

<script type="text/javascript">

    document.forms['edit_matt'].elements['pattern_model_id'].value = "<?php echo $mtt->pattern_model_id ?>";
    document.forms['edit_matt'].elements['color_id'].value = "<?php echo $mtt->color_id ?>";

</script>