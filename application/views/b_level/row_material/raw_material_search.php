<table class="table table-bordered mb-3" id="results_rawmaterial">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>Material name</th>
                        <th>Pattern</th>
                        <th>Color name</th>
                        <th>UOM</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $i = 1;
                    if (!empty($row_materials)) {
                        foreach ($row_materials as $key => $val) {
                            $sql = "SELECT a.id, a.color_name FROM color_tbl a 
                                        JOIN raw_material_color_mapping_tbl b ON b.color_id = a.id 
                                        WHERE b.raw_material_id = $val->id";
                            $results = $this->db->query($sql)->result();
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val->material_name; ?></td>
                                <td><?= $val->pattern_name; ?></td>
                                <td>
                                    <?php
                                    $sl = 0;
                                    foreach ($results as $result) {
                                        $sl++;
                                        echo "<ul>";
                                        echo "<li>" . $sl . " . " . ucwords($result->color_name) . "</li>";
                                        echo "<ul>";
                                    }
                                    ?>
                                </td>
                                <td><?= $val->uom_name; ?></td>
                                <td width="100">
                                    <a href="edit-row-material/<?= $val->id; ?>" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="<?= $val->id ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                    <!--<a href="javascript:void(0)" class="btn btn-warning default btn-sm" onclick="material_edit_form(<?php echo $val->id; ?>);"><i class="fa fa-pencil"></i></a>-->
                                    <a href="delete-row-material/<?= $val->id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>

                            <?php
                        }
                    }
                    ?>


                </tbody>
                <?php if (empty($row_materials)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>