<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .gallery-image-wrapper .gallery-image-outer img{
        width: 100%;
        height: 145px;
    }
    .gallery-image-wrapper .gallery-image-outer {
        position: relative;
        margin-bottom: 20px;
        overflow: hidden;
    }
    .gallery-image-wrapper .gallery-manage-btnmain {
        text-align: center;
        position: absolute;
        top: auto;
        bottom: -10px;
        margin: 0 auto;
        left: 0;
        right: 0;
        opacity: 0;        
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        bottom: 10px;
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper {
        text-align: center;
    }
    .gallery-image-wrapper a.load-more-btn {
        font-size: 14px;
        color: #ffffff;
        background-color: #336699;
        vertical-align: middle;
        padding: 0 15px;
        height: 40px;
        line-height: 40px;
        border-radius: 3px;
        margin: 30px auto 0 auto;
        text-decoration: none;
        display: none;
    }
    /* grid & list */
    .grid-list-wrapper button {
        font-size: 14px;
        background-color: #369;
        color: #ffffff;
        text-shadow: none;
        padding: 0;
        height: 30px;
        width: 30px;
        border: none;
    }
    .gallery-image-wrapper .some-list-load.row {
        margin-left: 0;
        margin-right: 0;
    }
    .gallery-image-wrapper .some-list-load.list {
        display: block;
        text-align: left;
    }
    .gallery-image-wrapper .some-list-load.grid .gallery-manage-btnmain-list {
        display: none;
    }
    .gallery-image-wrapper .product-grid .gallery-manage-btnmain a span {
        display: none;
    }
    .gallery-image-wrapper .product-list img {
        width: 25%;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain {
        right: 15px;
        bottom: auto;
        top: 0;
        left: auto;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a {
        display: block;
        margin: 5px 0;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a i {
        margin-right: 5px;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer .gallery-manage-btnmain ,
    .gallery-image-wrapper .product-list .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%); 
        -o-transform: translateY(-50%); 
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        bottom: auto;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer {
        border: 1px solid #eeeeee;
        padding: 15px;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer:hover:before {
        opacity: 0;
    }
    @media(max-width: 1199px){
        .gallery-image-wrapper .gallery-image-outer img {
            min-height: 175px;
        }
    }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
         <div class="title">
            <h5>Manage Image</h5>
        </div>
        <!-- end box / title -->
        <p class="px-4">
            <a href="<?php echo base_url('b_level/Image_controller/add_image'); ?>" class="btn btn-success mt-1 " id="add_new_tag" style="margin-top: -6px !important;">Add Image</a>
        </p>
        <?php if(count($filter_arr) > 0) { ?>
        <div class="px-4 mt-3">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url('manage_b_gallery_image'); ?>" method="post" id="galleryFilterFrm">
                    <fieldset>
                        <div class="row">
                            <?php  foreach($filter_arr as $filter) { ?>
                            <div class="col-md-2">
                                <select class="form-control filter-select-box" name="tag_<?=$filter['tag_id']?>" id="tag_<?=$filter['tag_id']?>" >
                                    <option value="">--Select <?= $filter['tag_name']; ?>--</option>
                                    <?php foreach($filter['value'] as $value) { ?>
                                        <option value="<?= $value['tag_value']?>"><?= $value['tag_value']?></option>
                                    <?php }?>
                                </select>
                                <script>
                                    $('#tag_'+<?=$filter['tag_id']?>).val('<?= $filter['selected_val']; ?>');
                                </script>
                            </div>
                            <?php } ?>
                            <div class="col-md-2 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="filter_field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <?php }?>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div id="results_color">
            <form class="p-3">
                    <div class="gallery-image-wrapper">
                        <div class="some-list-load row grid">
                            <?php
                            $i = 0 + $pagenum;
                            if (!empty($images)) {                          
                                foreach ($images as $key => $val) {
                                    ?>
                                    <div class="col-xl-3 col-lg-4 col-sm-4 col-md-12 content product-grid">
                                        <div class="gallery-image-outer">
                                            <img src="<?= base_url($val->image); ?>" class='gallery_image'>
                                            <div class="gallery-manage-btnmain">
                                                <a href="<?php echo base_url('b_level/Image_controller/view_image/'.$val->id); ?>" class="btn btn-primary default btn-sm view_image" id="view_image" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i><span>view image</span>
                                                </a>
                                                <a href="<?php echo base_url('b_level/Image_controller/get_image/'.$val->id); ?>" class="btn btn-warning default btn-sm edit_image" id="edit_image"><i class="fa fa-pencil"></i><span>edit image</span></a>
                                                <a href="<?php echo base_url('b_level/Image_controller/delete_image/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm delete_image" ><i class="fa fa-trash"></i><span>delete image</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <a href="#" class="load-more-btn" id="loadMore">View More <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                    </div>

                    <?php if (empty($images)) { ?>
                        <div class="text-center text-danger">No record found!</div>
                    <?php } ?>
                <?php echo $links; ?>
            </form>
        </div>
    </div>
</div>
<script>
function filter_field_reset() {
    $(".filter-select-box").val('');
    $('#galleryFilterFrm').submit();
}
</script>
<!-- end content / right -->





