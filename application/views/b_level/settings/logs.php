
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <div class="title row">
            <h3>List of Access Logs</h3>
        </div>
        <div id="results_menu">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL no.</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>IP Address</th>
                        <th>Done</th>
                        <th>Remarks</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    foreach ($access_logs as $logs) {
                        $sl++;
                        $dateTimes = explode(" ", $logs->entry_date);
                        $date = $dateTimes[0];
                        $time = $dateTimes[1];
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo date('M-d-Y', strtotime($date)); ?></td>
                            <td><?php echo date('H:i:s', strtotime($time)); ?></td>
                            <td><?php echo $logs->ip_address; ?></td>
                            <td><?php echo $logs->action_done; ?></td>
                            <td><?php echo $logs->remarks; ?></td>
                                            <td><?php echo $logs->name; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $links; ?>
        </div>
    </div>  
</div>
<!-- end content / right -->

