
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Tax Rate Update Information</h5>
        </div>
        <!-- end box / title -->
        <div class="m-2">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url('us-state-update/' . $us_state_edit[0]['state_id']); ?>" method="post">
                <div class="form-group row">
                    <label for="state_name" class="col-sm-3 col-form-label">State Name <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select name="state_name" class="form-control select2-single" id="state_name" onchange="city_statename_wise_stateid(this.value)" data-placeholder="-- select one --" required>
                          <?php foreach ($get_city_state as $city_state) { ?>
                                <option value='<?php echo $city_state->state_name; ?>' <?php
                                if ($us_state_edit[0]['state_name'] == $city_state->state_name) {
                                    echo 'selected';
                                }
                                ?>>
                                            <?php echo $city_state->state_name; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <!--<input class="form-control" name="state_name" id="state_name" type="text" value="<?php echo $us_state_edit[0]['state_name']; ?>" placeholder="Enter State Name!" required="">-->
                    </div>
                </div>
                <!--                            <div class="form-group row">
                                                <label for="short_code" class="col-sm-3 col-form-label">Short Code <i class="text-danger">*</i></label>-->
                <div class="col-sm-6">
                    <input type="hidden" name="short_code" class="form-control short_code " id="short_code" value="<?php echo $us_state_edit[0]['shortcode']; ?>">
                    <!--<input type="text" name="short_code" class="form-control short_code" id="short_code" value="<?php echo $us_state_edit[0]['shortcode']; ?>" placeholder="Enter Short Code">-->
                </div>
                <!--</div>-->

                <div class="form-group row">
                    <label for="tax_rate" class="col-sm-3 col-form-label">Tax Rate <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="tax_rate" id="tax_rate" type="text" value="<?php echo $us_state_edit[0]['tax_rate']; ?>" placeholder="Enter Tax Rate!">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-9 text-right">
                        <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                        <input type="submit" id="add-shipping_method" class="btn btn-success btn-large" name="" value="Update">
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
<!-- end content / right -->
<script type="text/javascript">
    function city_statename_wise_stateid(t) {
        $.ajax({
            url: "<?php echo base_url('b_level/Setting_controller/city_statename_wise_stateid'); ?>",
            type: 'post',
            data: {state_name: t},
            success: function (r) {
                r = JSON.parse(r);
//                    alert(r);
                $("#short_code").empty();
//                $("#short_code").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $("#short_code").val(typeval.state_id);
//                    $('#short_code').append($('<option>').text(typeval.state_id).attr('value', typeval.state_id));
                });
            }
        });
    }
</script>