
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>
        <div class="title">
            <h5>Unit of Measurement</h5>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b_level/Setting_controller/uom_update/<?php echo $uom_edit[0]['uom_id']; ?>" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="panel">
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="uom_name" class="col-sm-3 control-label">Unit of Measurement</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="uom_name" id="uom_name" placeholder="Unit of measurement" value="<?php echo $uom_edit[0]['uom_name']; ?>" required tabindex="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" class="form-control select2" id="status" data-placeholder="-- select one --" tabindex="2">
                                    <option value=""></option>
                                    <option value="1" <?php
                                    if ($uom_edit[0]['status'] == '1') {
                                        echo 'selected';
                                    }
                                    ?>>Active</option>
                                    <option value="0" <?php
                                    if ($uom_edit[0]['status'] == '0') {
                                        echo 'selected';
                                    }
                                    ?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                                <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Update" tabindex="7" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>            

        </div>        
    </div>
</div>
<!-- end content / right -->
