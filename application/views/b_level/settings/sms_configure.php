
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>SMS Configuration</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php base_url(); ?>sms-config-save" method="post">

                <div class="form-group row">
                    <label for="provider_name" class="col-xs-3 col-form-label">Provider Name  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user_name" class="col-xs-3 col-form-label">User Name  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="user_name" class="form-control" id="user_name" placeholder="User Name" tabindex="2" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-3 col-form-label">Password  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-xs-3 col-form-label">Phone  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="+12062024567 This is the valid format" tabindex="4" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sender_name" class="col-xs-3 col-form-label">Sender Name <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" required>
                    </div>
                </div>

                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5"  tabindex="6">Save</button>
                    <button type="reset" class="btn btn-primary w-md m-b-5" tabindex="7">Reset</button>
                </div>

            </form>
        </div>
    </div>
    <div class="box">
        <div class="title">
            <h3>List Of SMS Configuration</h3>
        </div>
        <div class="table-responsive p-3">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#.</th>
                        <th width="10%">Name</th>
                        <th width="15%">User Name</th>
                        <th width="15%">Password</th>
                        <th width="10%">Phone</th>
                        <th width="25%">Sender</th>
                        <th width="5%">Status</th>
                        <th  width="15%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0; // + $pagenum;
                    foreach ($get_sms_config as $value) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->provider_name; ?></td>
                            <td><?php echo $value->user; ?></td>
                            <td><?php echo $value->password; ?></td>
                            <td><?php echo $value->phone; ?></td>
                            <td><?php echo $value->authentication; ?></td>

                            <td>
                                <?php
                                if ($value->default_status == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>
                            <td class="text-center">
                                <a href="<?php echo base_url(); ?>sms-config-edit/<?php echo $value->gateway_id; ?>" class="btn btn-warning default btn-sm"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url(); ?>sms-config-delete/<?php echo $value->gateway_id; ?>" class="btn btn-danger danger btn-sm" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
                <?php if (empty($get_sms_config)) { ?>
                    <tfoot>
                    <th colspan="6" class="text-center">
                        No result found!
                    </th>
                    </tfoot>
                <?php } ?>
            </table>

        </div>
    </div>
</div>
<!-- end content / right -->