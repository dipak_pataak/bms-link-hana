
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Mail</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3"><?php // echo '<pre>';            print_r($get_mail_config); ?>
            <form action="<?php echo base_url(); ?>update-mail-configure" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <div class="form-group row">
                    <label for="protocol" class="col-sm-3 col-form-label">Protocol <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="protocol" id="protocol" type="text" value="<?php echo $get_mail_config[0]->protocol; ?>">
                    </div>
                    <div class="col-sm-3">
                        <p> smtp </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_host" class="col-sm-3 col-form-label">SMTP Host <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_host" id="smtp_host" type="text" value="<?php echo $get_mail_config[0]->smtp_host; ?>">
                    </div>
                    <div class="col-sm-3">
                        <p> OR /usr/sbin/sendmail </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_port" class="col-sm-3 col-form-label">SMTP Port <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_port" id="smtp_port" type="text" value="<?php echo $get_mail_config[0]->smtp_port; ?>">
                    </div>

                    <div class="col-sm-3">
                        <p> 465 </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_user" class="col-sm-3 col-form-label">Sender Email <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_user" id="smtp_user" type="email" value="<?php echo $get_mail_config[0]->smtp_user; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_pass" class="col-sm-3 col-form-label">Password <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_pass" id="smtp_pass" type="password" value="<?php echo $get_mail_config[0]->smtp_pass; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mailtype" class="col-sm-3 col-form-label">Mail Type <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select class="form-control" name="mailtype" id="mailtype">
                            <option value="">Select One</option>
                            <option value="html" <?php if($get_mail_config[0]->mailtype == 'html'){ echo 'selected'; } ?>>Html</option>
                            <option value="text" <?php if($get_mail_config[0]->mailtype == 'text'){ echo 'selected'; } ?>>Text</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <p> html </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-6 text-right">
                        <input type="submit" class="btn btn-success btn-large" value="Save Changes">
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end content / right -->