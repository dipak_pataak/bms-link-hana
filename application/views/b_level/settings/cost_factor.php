<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
<style>
    #result_search_filter {
        display:none;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Cost Factor</h5>
            <div class="col-sm-6 text-right">
                <!--<a href="<?php echo base_url(); ?>add-b-customer" class="btn btn-success btn-sm mt-1">Add Customer</a>-->
            </div>
        </div>

        
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->

        <div class="table-responsive p-3">


            <form action="<?php echo base_url(); ?>b-cost-factor-save" method="post">



<!--                 <div class="form-group col-md-6">
                    
                    <label for="inputEmail4" class="mb-2">Customer <span class="text-danger"> * </span></label>
                    <select class="form-control selectpicker" name="customer_id[]" id="customer_id" required="" multiple data-actions-box="true"  data-placeholder="--select one --">
                        <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                            }
                        ?>

                    </select>
                </div>


                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Product <span class="text-danger"> * </span></label>
                    <select class="form-control selectpicker" name="product_id[]" id="product_id" multiple data-actions-box="true"  required=""   autocomplete="off" data-placeholder="--select one --">
                        <?php
                            foreach ($get_only_product as $product) {
                                echo "<option value='$product->product_id'>$product->product_name</option>";
                            }
                        ?>
                    </select>
                </div>
 -->


                <div class="form-group col-md-6">
                    
                    <label for="inputEmail4" class="mb-2">Customer <span class="text-danger"> * </span></label>
                    
                    <select class="form-control " name="customer_id[]" id="customer_id" required="" multiple=""  data-placeholder="--select one --">
                    
                        <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                            }
                        ?>

                    </select>

                    <input type="button" id="select_all_c" name="select_all" value="Select All">
                    <input type="button" id="dselect_all_c" name="select_all" value="DeSelect All">

                </div>


                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Product <span class="text-danger"> * </span></label>
                    <select class="form-control " name="product_id[]" id="product_id" multiple=""  required=""   autocomplete="off" data-placeholder="--select one --">
                        <?php
                            foreach ($get_only_product as $product) {
                                echo "<option value='$product->product_id'>$product->product_name</option>";
                            }
                        ?>
                    </select>
                    <input type="button" id="select_all_p" name="select_all" value="Select All">
                    <input type="button" id="dselect_all_p" name="select_all" value="DeSelect All">
                </div>


                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Cost Factor <span class="text-danger"> * </span></label>
                    <input type="text" name="dealer_cost_factor" class="form-control" required="">
                </div>

               <!--  <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Individual Cost Factor <span class="text-danger"> * </span></label>
                    <input type="text" name="individual_cost_factor" class="form-control" required="">
                </div> -->

                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2"></label>
                    <button type="submit" class="btn-success btn-sm "> Save</button>
                </div>

            </form>

<!--             <form action="<?php echo base_url(); ?>b-cost-factor-save" method="post">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Product List</th>
                            <th class="text-right">Dealer Cost Factor</th>
                            <th class="text-right">Individual Cost Factor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($get_only_product as $single_product) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $single_product->product_name; ?>
                                    <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single_product->product_id; ?>">
                                </td>
                                <td>
                                    <input type="text" name="dealer_cost_factor[]" class="form-control text-right" value="<?php echo $single_product->dealer_cost_factor; ?>">
                                </td>
                                <td>
                                    <input type="text" name="individual_cost_factor[]" class="form-control text-right" value="<?php echo $single_product->individual_cost_factor; ?>">
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3" class="text-right">
                                <input type="submit" class="btn btn-success" value="Save Change">
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </form> -->
        </div>


        <div class="title row">
            <h5 class="col-sm-6">Cost Factor List</h5>
            <div class="col-sm-6 text-right">
            </div>
        </div>
        <div class="table-responsive p-3">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Setting_controller/save_cost_factor') ?>">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="txt_dealer_cost_factor" class="mb-2">Cost Factor <span class="text-danger"> * </span></label>
                        <input type="text" name="txt_dealer_cost_factor" class="form-control col-md-8" required="">
                        <input type="submit" class="button col-md-2 password_generate_btn btn btn-success" value="Save">
                    </div>
                    <label for="keyword" class="col-sm-2 col-form-label offset-2 text-right"></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control pull-right" name="keyword" id="keyword" placeholder="Search..." tabindex="">
                    </div>
                </div>
                <table class="datatable2 table table-bordered table-hover" id="result_search">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>Customer Name</th>
                            <th>Product Name</th>
                            <th class="text-right"> Cost Factor</th>
                            <!-- <th class="text-right">Individual Cost Factor</th> -->
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($costfactor as $val) {
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $val->id; ?>" class="checkbox_list">  
                                </td>
                                <td>
                                    <?php echo $val->first_name.' '.$val->last_name; ?>
                                </td>

                                <td>
                                    <?php echo $val->product_name; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo $val->dealer_cost_factor; ?>
                                </td>
                                <!-- <td class="text-right">
                                   <?php echo $val->individual_cost_factor; ?>
                                </td> -->
                                <td>
                                   <a href="<?= base_url('b_level/setting_controller/edit_cust_factor/').$val->id; ?>" class="btn btn-success default btn-sm" ><i class="fa fa-edit"></i></a>
                                   <a href="<?= base_url('b_level/setting_controller/delete_cust_factor/').$val->id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
        </div>

    </div>
</div>
<!-- end content / right -->

            <script type="text/javascript">

                $('#select_all_p').click( function() {
                    $('select#product_id > option').prop('selected', 'selected');
                });
                $('#dselect_all_p').click( function() {
                    $('select#product_id > option').prop('selected', '');
                });

                $('#select_all_c').click( function() {
                    $('select#customer_id > option').prop('selected', 'selected');
                });
                $('#dselect_all_c').click( function() {
                    $('select#customer_id > option').prop('selected', '');
                });

            </script>
            <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
            <script>
            $(document).ready(function() {
                oTable = $('#result_search').DataTable( {
                    "paging": false,
                    "bInfo": false,
                });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
                $('#keyword').keyup(function(){
                    oTable.search($(this).val()).draw() ;
                })
            } );
            
            </script>