
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>SMS Configuration</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php echo base_url('sms-config-update/' . $sms_config_edit[0]['gateway_id']); ?>" method="post">
                <?php // echo '<pre>';                            print_r($sms_config_edit);?>
                <div class="form-group row">
                    <label for="provider_name" class="col-xs-3 col-form-label">Provider Name  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" value="<?php echo $sms_config_edit[0]['provider_name']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user_name" class="col-xs-3 col-form-label">User Name  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="user_name" class="form-control" id="user_name" placeholder="User Name" tabindex="2"  value="<?php echo $sms_config_edit[0]['user']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-3 col-form-label">Password  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" value="<?php echo $sms_config_edit[0]['password']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-xs-3 col-form-label">Phone  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone" tabindex="4" value="<?php echo $sms_config_edit[0]['phone']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sender_name" class="col-xs-3 col-form-label">Sender Name <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" value="<?php echo $sms_config_edit[0]['authentication']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="is_active" class="col-xs-3 col-form-label">Is Active <span class="text-danger"> </span></label>
                    <div class="col-xs-9">
                        <select name="is_active" class="is_active form-control select2" id="is_active" data-placeholder='-- select one --'>
                            <option value=""></option>
                            <option value="1" <?php if($sms_config_edit[0]['default_status'] == 1){ echo 'selected'; }?>>Active</option>
                            <option value="0" <?php if($sms_config_edit[0]['default_status'] == 0){ echo 'selected'; }?>>Inactive</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5"  tabindex="6">Update</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end content / right -->