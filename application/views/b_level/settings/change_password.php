
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Change Password</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b-update-password" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <div class="form-group row">
                    <label for="old_password" class="col-sm-3 col-form-label text-right">Old Password <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="old_password" id="old_password" placeholder="Old Password" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="new_password" class="col-sm-3 col-form-label  text-right">New Password<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="confirm_password" class="col-sm-3 col-form-label text-right">Confirm Password<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Re-Enter New Password" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-6 text-right">
                        <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
                        <input type="submit" class="btn btn-success btn-sm" id="update_btn" value="Update">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function () {
        $("body").on("click", "#update_btn", function () {
            var new_password = $("#new_password").val();
            var confirm_password = $("#confirm_password").val();
            if (new_password != confirm_password) {
                alert("New password and confirm password does not match");
                $("#confirm_password").val('').focus();
                return false;
            }
        });
    });
</script>