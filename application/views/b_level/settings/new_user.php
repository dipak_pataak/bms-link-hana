
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $message = $this->session->flashdata('message');
        if ($message)
            echo $message;
        ?>

        <div class="title">
            <h5>Add User</h5>
        </div>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo validation_errors() ?>
            </div>
        <?php } ?>

        <!-- end box / title -->
        <?= form_open('b_level/setting_controller/save_user', array('class' => 'p-3', 'name' => 'userFrm')); ?>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="first_name" class="mb-2">First Name <span class="text-danger"> * </span></label>
                <input type="text" class="form-control" name="first_name" id="first_name" value="<?= @$user->first_name ?>" placeholder="John" onkeyup="required_validation()" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="last_name" class="mb-2">Last Name <span class="text-danger"> * </span></label>
                <input type="text" class="form-control" name="last_name" id="last_name" value="<?= @$user->last_name ?>" onkeyup="required_validation()" placeholder="Doe" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="email" class="mb-2">Email <span class="text-danger"> * </span></label>
                <input type="email" class="form-control" name="email" id="email" onkeyup="check_email_keyup()" value="<?= @$user->email ?>"  placeholder="johndoe@yahoo.com" required>
                <span id="error"></span>
            </div>
            <div class="form-group col-md-6">
                <label for="phone" class="mb-2">Contact <span class="text-danger"> * </span></label>
                <input type="text" class="form-control phone" name="phone" id="phone" value="<?= @$user->phone ?>" onkeyup="required_validation()" placeholder="+1 (XXX)-XXX-XXXX" required>
            </div>
            <div class="form-group col-md-6">
                <label for="address" class="mb-2">Address</label>
                <input type="text" class="form-control" name="address" id="address" value="<?= @$user->address ?>" placeholder="1234 Main St">
            </div>

            <div class="form-group col-md-6">
                <label for="password" class="mb-2">Password <span class="text-danger"> * </span></label>
                <input type="password" class="form-control col-md-8" name="password" id="password" placeholder="Password" required>
                <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
                <input type="button" class="button col-md-2 password_generate_btn btn" value="Generate" onClick="generate();" >
            </div>
        </div>

        <button type="submit" class="btn btn-sm d-block float-right btn-success">Add User</button>
        <?= form_close(); ?>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

//    ============ its for show password ===============
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
//    ============ its for generate password ============
    function randomPassword(length = 6) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    function generate() {
        userFrm.password.value = randomPassword(userFrm.length.value);
    }
//    ============ close generate password =============
    
//        ========== some field validation ============   
    $('button[type=submit]').prop('disabled', true);
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '') {
//            $("#first_name").css({'border': '1px solid red'}).focus();
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }
//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
//        console.log(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "get-check-user-unique-email",
            type: "post",
            data: data_string,
            success: function (data) {
//                console.log(data);
                if (data != 0) {
//                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
//                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }
    
//    =============== its for google place address geocomplete ===============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });
    });
</script>
