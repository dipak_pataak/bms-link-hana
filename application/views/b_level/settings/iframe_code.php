
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $message = $this->session->flashdata('message');
        if ($message)
            echo $message;
        ?>

        <div class="title">
            <h5>Iframe Generate</h5>
        </div>
        <div class="ml-2">
                  <!--<a href="<?php echo base_url(); ?>b-customer-iframe-view" class="btn btn-success w-md m-b-5"  style="margin-bottom: 10px;" target="_new">View IFrame</a>-->
        </div>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo validation_errors() ?>
            </div>
        <?php } ?>

        <!-- end box / title -->
        <form action="<?php echo base_url(); ?>iframe-code-generate-save" method="post">
            <div class="form-group row">
                <div class="col-sm-12 ml-2 mr-2">
                    <?php
                    // echo @$check_iframe_code[0]->iframe_code;  
//                                    echo $this->session->userdata('user_id');
//                                    echo $this->session->userdata('user_type');
                    ?>

                    <textarea class="form-control" name="iframe_code" rows="5" placeholder="" readonly> <iframe width="100%" height="100%" src="<?php echo base_url() . "d-customer-iframe-generate/" . $this->session->userdata('user_id') . "/" . $this->session->userdata('user_type'); ?>" scrolling="no"  frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></textarea>
                </div>
            </div>
            <!--                            <div class="form-group text-right">
                                            <button type="reset" class="btn btn-danger w-md m-b-5">Cancel</button>
                                            <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
                                        </div>-->
        </form>
    </div>
</div>
