
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Gateway Settings</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">

            <form action="<?php echo base_url(); ?>b_level/setting_controller/update_tms_payment_setting" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">


                <div class="form-group row">
                    <label for="url" class="col-sm-3 col-form-label text-right">URL<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="text" name="url" id="url" class="form-control" value="<?= $gateway_edit->url; ?>"  required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user_name" class="col-sm-3 col-form-label text-right">User Name<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="text" name="user_name" id="user_name" class="form-control" value="<?= $gateway_edit->user_name ?>" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-sm-3 col-form-label text-right">Password<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="password" name="password" id="password" class="form-control" value="<?= @$gateway_edit->password ?>" placeholder="Payment Mail">
                    </div>
                </div>



                <!-- <div class="form-group row">
                    <label for="mode" class="col-sm-3 col-form-label">Mode<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                            <option value="1" <?php
            if ($gateway_edit->mode == 1) {
                echo 'selected';
            }
            ?>>Production</option>
                            <option value="0" <?php
                if ($gateway_edit->mode == 0) {
                    echo 'selected';
                }
            ?>>Development</option>
                        </select>
                    </div>
                </div> -->


                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-6 text-right">
                        <input type="submit" class="btn btn-success btn-large" value="Update">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end content / right -->