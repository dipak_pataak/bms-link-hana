
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $message = $this->session->flashdata('message');
        $exception = $this->session->flashdata('exception');
        if ($message)
            echo $message;
        if ($exception)
            echo $exception;
        ?>

        <div class="title">
            <h5>Edit User</h5>
        </div>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo validation_errors() ?>
            </div>
        <?php } ?>

        <!-- end box / title -->
        <?= form_open('b_level/setting_controller/update_user', array('class' => 'p-3', 'name' => 'userFrm')); ?>

        <input type="hidden" name="user_id" value="<?= $user->id ?>">
        <input type="hidden" name="redirect_url" value="b_level/setting_controller/edit_user/<?= $user->id ?>"/>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputFirstName" class="mb-2">First name <span class="text-danger"> * </span></label>
                <input type="text" class="form-control" name="first_name" id="inputFirstName" value="<?= @$user->first_name ?>" placeholder="John" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="inputLastName" class="mb-2">Last name <span class="text-danger"> * </span></label>
                <input type="text" class="form-control" name="last_name" id="inputLastName" value="<?= @$user->last_name ?>" placeholder="Doe" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4" class="mb-2">Email <span class="text-danger"> * </span></label>
                <input type="email" class="form-control" name="email" id="inputEmail4" value="<?= @$user->email ?>" placeholder="johndoe@yahoo.com">
            </div>
            <div class="form-group col-md-6">
                <label for="inputcontact" class="mb-2">Contact <span class="text-danger"> * </span></label>
                <input type="text" class="form-control phone" name="phone" id="inputcontact" value="<?= @$user->phone ?>" placeholder="+1 (XXX)-XXX-XXXX">
            </div>
            <div class="form-group col-md-6">
                <label for="address" class="mb-2">Address</label>
                <input type="text" class="form-control" name="address" id="address" value="<?= @$user->address ?>" placeholder="1234 Main St">
            </div>

            <!--            <div class="form-group row">
                            <label for="preview" class="col-sm-3 col-form-label">Preview</label>
                            <div class="col-sm-9">
                                <img src="<?= base_url() ?>" class="img-thumbnail" width="125" height="100">
                            </div>
                            <input type="hidden" name="old_image" value="<?= @$user->user_image ?>">
                        </div>
            
                        <div class="form-group row">
                            <label for="image" class="col-sm-3 col-form-label">Image</label>
                            <div class="col-sm-9">
                                <input type="file" name="image" id="image" aria-describedby="fileHelp">
                                <small id="fileHelp" class="text-muted"></small>
                            </div>
                        </div>-->


            <div class="form-group col-md-6">
                <label for="password" class="mb-2">Password <span class="text-danger"> * </span></label>
                <input type="password" class="form-control col-md-8" name="password" id="password" placeholder="Password">
                <input type="hidden" name="oldpassword" value="<?= @$user->password ?>">
                <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
                <input type="button" class="button col-md-2 password_generate_btn btn" value="Generate" onClick="generate();" >
            </div>
           
            <div class="form-group col-md-6">
                <label for="fixed_commission" class="mb-2">Fixed Commission</label>
                <input type="text" class="form-control NumbersAndDot" name="fixed_commission" id="fixed_commission"  placeholder="Fixed Commission" value="<?=@$user->fixed_commission;?>">
            </div>

            <div class="form-group col-md-6">
                <label for="percentage_commission" class="mb-2">Commission (%)</label>
                <input type="text" class="form-control NumbersAndDot percentage_valid" name="percentage_commission" id="percentage_commission"  placeholder="Commission (%)" value="<?=@$user->percentage_commission;?>">
            </div>

        </div>
        <div class="form-group row offset-10">
            <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>&nbsp; 
        <button type="submit" class="btn btn-sm d-block btn-success">Update User</button>
        </div>
        <?= form_close(); ?>
    </div>
</div>
<!-- end content / right -->


<script type="text/javascript">

//    ============ its for show password ===============
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
//    ============ its for generate password ============
    function randomPassword(length = 6) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    function generate() {
        userFrm.password.value = randomPassword(userFrm.length.value);
    }
//    ============ close generate password ============= 


//    =============== its for google place address geocomplete ===============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });
    });

    $('.NumbersAndDot').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

    $('.percentage_valid').keyup(function(){
      if ($(this).val() > 100){
        $(this).val('100');
      }
    });

</script>
