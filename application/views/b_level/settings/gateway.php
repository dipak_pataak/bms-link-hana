
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Paypal Information</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php echo base_url(); ?>save-gateway" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <div class="form-group row">
                    <label for="payment_gateway" class="col-sm-3 col-form-label">Payment Gateway <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="payment_gateway" id="payment_gateway" data-placeholder='-- select one --'>
                            <option value=""></option>
                            <option value="paypal" selected>Paypal</option>
                            <!--<option value="sandbox">Sandbox</option>-->
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="payment_mail" class="col-sm-3 col-form-label">Paypal Mail<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <input type="text" name="payment_mail" id="payment_mail" class="form-control" placeholder="Payment Mail" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="currency" class="col-sm-3 col-form-label">Currency <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="currency" id="currency" data-placeholder="-- select one --" required>
                            <option value=""></option>
                            <option value="USD">(USD) U.S. Dollar</option>
                            <option value="EUR">(EUR) Euro</option>
                            <option value="AUD">(AUD) Australian Dollar</option>
                            <option value="CAD">(CAD) Canadian Dollar</option>
                            <option value="CZK">(CZK) Czech Koruna</option>
                            <option value="DKK">(DKK) Danish Krone</option>
                            <option value="HKD">(HKD) Hong Kong Dollar</option>
                            <option value="Yen">(YEN) Japanese</option>
                            <option value="MXN">(MXN) Mexican Peso</option>
                            <option value="NOK">(NOK) Norwegian Krone</option>
                            <option value="NZD">(NZD) New Zealand Dollar</option>
                            <option value="PHP">(PHP) Philippine Peso</option>
                            <option value="PLN">(PLN) Polish Zloty</option>
                            <option value="SGD">(SGD) Singapore Dollar</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="mode" class="col-sm-3 col-form-label">Mode<i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                            <option value="0">Development</option>
                            <option value="1">Production</option>
                        </select>
                        <!--<input type="text" name="is_active" id="is_active" class="form-control" value="<?php echo $gateway_edit[0]['default_status']; ?>">-->
                    </div>
                </div>



                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-6 text-right">
                        <input type="submit" class="btn btn-success btn-large" value="Save Changes">
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="box">
        <div class="title row">
            <h5>Paypal Payment Gateway</h5>
        </div>
        <table class="datatable2 table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="8%">SL No.</th>
                    <th width="15%">Name</th>
                    <th width="10%">Email</th>
                    <th width="10%">Currency</th>
                    <th width="10%">Mode</th>
                    <th width="10%">Is Active</th>
                    <th width="12%" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sl = 0 + $pagenum;
                foreach ($gateway_list as $gateway) {
                    $sl++;
                    ?>
                    <tr>
                        <td><?php echo $sl; ?></td>
                        <td><?php echo $gateway->payment_gateway; ?></td>
                        <td><?php echo $gateway->payment_mail; ?></td>
                        <td><?php echo $gateway->currency; ?></td>
                        <td><?php
                            if ($gateway->status == 1) {
                                echo 'Production';
                            } else {
                                echo "Development";
                            }
                            ?></td>
                        <td><?php
                            if ($gateway->default_status == 1) {
                                echo 'Active';
                            } else {
                                echo "Inactive";
                            }
                            ?></td>
                        <td class="text-center">
                            <a href="<?php echo base_url(); ?>gateway-edit/<?php echo $gateway->id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo base_url(); ?>gateway-delete/<?php echo $gateway->id; ?>" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            <?php if(empty($gateway_list)){ ?>
            <tfoot>
                <tr>
                    <th class="text-danger text-center" colspan="7">Record not found!</th> 
                </tr>
            </tfoot>
            <?php } ?>
        </table>
        <?php echo $links; ?>
    </div>



</div>
<!-- end content / right -->