
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>
        <div class="title">
            <h5>Unit of Measurement</h5>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b_level/Setting_controller/uom_save" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="panel">
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="uom_name" class="col-sm-3 control-label">Unit of Measurement</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="uom_name" id="uom_name" placeholder="Unit of measurement" required tabindex="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" class="form-control select2" id="status" data-placeholder="-- select one --" tabindex="2">
                                    <option value=""></option>
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Save" tabindex="3" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>            




            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No</th>
                        <th>Unit of Measurement</th>
                        <th class="">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($uom_list)) {
                        $sl = 0 + $pagenum;
                        foreach ($uom_list as $key => $value) {
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->uom_name; ?></td>
                                <td>
                                    <?php if ($value->status == 1) {
                                        echo 'Active';
                                    } else {
                                        echo 'Inactive';
                                    } ?>
                                    </td>
                                <td class="text-center">
                                    <a href="<?php echo base_url(); ?>b_level/Setting_controller/uom_edit/<?php echo $value->uom_id; ?>" title="" class="btn btn-info btn-xs simple-icon-note" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url(); ?>b_level/Setting_controller/uom_delete/<?php echo $value->uom_id ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs simple-icon-trash" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
<?php if (empty($uom_list)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="3" class="text-center">No record found!</th>
                        </tr> 
                    </tfoot>
<?php } ?>
            </table> 
<?php echo $links; ?>

        </div>        
    </div>
</div>
<!-- end content / right -->
