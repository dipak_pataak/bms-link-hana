
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-12"> Category Nested Levels</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3 mb-3">
            <form class="form-row">
                <label for="assigned" class="col-lg-3 mt-2 text-right">Select Category</label>
                <div class="col-lg-3">
                    <select name="assigned" class="form-control" onchange="loadCategory(this.value)">
                        <option value=" ">None</option>
                        <?php foreach ($parent_category as $key => $value) { ?>
                            <option value="<?= $value->category_id ?>"><?= $value->category_name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </form>
        </div>

        <div class="px-3">
            <ul id="tree1">
                <!--                <li><a href="#">Blind</a>
                                    <ul>
                                        <li>Faux Wood Blind
                                            <ul>
                                                <li>2" Faux Wooden Blinds</li>
                                                <li>2 1/2" Shutters Blinds Beveled</li>
                                            </ul>
                                        </li>
                                        <li>Roller Blind</li>
                                    </ul>
                                </li>-->
            </ul>
        </div>
    </div>

</div>


<script>

    function loadCategory(cat_id) {
        if (cat_id == ' ') {
//            alert("Its Empty");
            $("#tree1").html("None");
        } else {
            var submit_url = "<?= base_url(); ?>b_level/Category_controller/load_category/" + cat_id;
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    $("#tree1").html(res);
                }, error: function () {
                    alert('error');
                }
            });
        }

    }



    $(document).ready(function () {
        "use strict"; // Start of use strict

        $.fn.extend({
            treed: function (o) {

                var openedClass = 'fa-folder-open-o';
                var closedClass = 'fa-folder-o';

                if (typeof o !== 'undefined') {
                    if (typeof o.openedClass !== 'undefined') {
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass !== 'undefined') {
                        closedClass = o.closedClass;
                    }
                }
                ;

                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator fa " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this === e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    });
                    branch.children().children().toggle();
                });
                //fire event from the dynamically added icon
                tree.find('.branch .indicator').each(function () {
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });

        //Initialization of treeviews
        $('#tree1').treed();

    });
</script>