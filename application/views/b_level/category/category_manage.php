<style type="text/css">
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 290px;
    }
    .filter_select2_cls .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 130px;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage Category</h5>
        </div>
        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
            <?php if ($this->permission->check_label('add_category')->create()->access()) { ?>
                <button type="button" class="btn btn-success  mb-1" data-toggle="modal" data-target="#categoryFrm">Add Category</button>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm mt-1 action-delete" style="margin-top: -3px !important;" onClick="return action_delete(document.recordlist)" >Delete</a>
            <?php } ?>
        </p>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>category-filter">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" class="form-control cat_name" placeholder="Enter Name" name="cat_name">
                            </div>
                            <span class="or_cls"> -- OR -- </span>
                            <div class="col-md-2">
                                <div class="filter_select2_cls">
                                    <select name="parent_cat" class="form-control parent_cat select2" id="parent_cat" data-placeholder="-- select category --">
                                        <option value=""></option>
                                        <?php foreach ($parent_category as $category) { ?>
                                            <option value='<?php echo $category->category_id; ?>'>
                                                <?php echo $category->category_name; ?>
                                            </option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <span class="or_cls"> -- OR -- </span>
                            <div class="col-md-2">
                                <div class="filter_select2_cls">
                                    <select name="category_status" class="form-control category_status select2" id="category_status" data-placeholder="-- select status --">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div>
                                    <input type="submit" name="category_filter" class="btn btn-sm btn-success default" value="Go">
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </form>
            </div>
        </div>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="categoryFrm" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Category Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url('b_level/Category_controller/save_category'); ?>" method="post" class="">
                            <div class="form-group row">
                                <label for="category_name" class="col-md-3 control-label">Category Name <span class="text-danger"> * </span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="category_name" id="category_name" onkeyup="special_character()" placeholder="Category Name"  required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="parent_category" class="col-md-3 control-label">Parent Category</label>
                                <div class="col-md-6">
                                    <select name="parent_category" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                                        <option value=" ">None</option>
                                        <?php
                                        foreach ($parent_category as $category) {
                                            echo "<option value='$category->category_id'>$category->category_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-3 control-label">Description</label>
                                <div class="col-md-6">
                                    <textarea name="description" class="form-control" type="text" placeholder="Description" rows="1" id="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 control-label">Status</label>
                                <div class="col-md-6">
                                    <select name="status" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                                        <option value=""></option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="offset-3 col-md-2">
                                    <button type="submit" class="btn btn-success w-md m-b-5">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="categorykeyup_search()" placeholder="Search..." tabindex="">
                </div>
                <!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                    </ul>
                                </div>-->
            </div>          
        </div>
        <div class="table-responsive px-3" id="results_category">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Category_controller/manage_action') ?>">
                <input type="hidden" name="action">
                <table class="table table-bordered table-hover text-center">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>Serial No.</th>
                            <th>Category Name</th>
                            <th>Parent Category</th>
                            <th>Description</th>
                            <th>Assigned Products</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sl = 0 + $pagenum;
                        foreach ($category_list as $category) {
                            $parent_category = $this->db->select('*')->where('category_id', $category->parent_category)->get('category_tbl')->row();
                            $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $category->category_id)
                                            ->get()->result();
                            $sl++
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $category->category_id; ?>" class="checkbox_list">  
                                </td>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $category->category_name; ?></td>
                                <td>
                                    <?php
                                    if (!empty($parent_category->category_name)) {
                                        echo $parent_category->category_name;
                                    } else {
                                        echo "None";
                                    }
                                    ?>
                                </td>
                                <td><?php echo $category->description; ?></td>
                                <td class="text-left">
                                    <?php
                                    if ($assigned_products) {
                                        foreach ($assigned_products as $product) {
                                            echo "<ul>";
                                            echo "<li> => " . $product->product_name . "</li>";
                                            echo "</ul";
                                        }
                                    } else {
                                        echo "The products will be assigned at the later stage";
                                    }
                                    ?>
                                </td>
                                <td><?php
                                    if ($category->status == '1') {
                                        echo "Active";
                                    } else {
                                        echo 'Inactive';
                                    }
                                    ?></td>
                                <td class="width_140">
                                    <!--<a href="<?php echo base_url(); ?>category-edit/<?php echo $category->category_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>-->
                                    <a href="javascript:void(0)" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="show_category_edit(<?php echo $category->category_id; ?>);"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url(); ?>b-category-delete/<?php echo $category->category_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <?php if (empty($category_list)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="7" class="text-center text-danger">No record found!</th>
                            </tr> 
                        </tfoot>
                    <?php } ?>
                </table>
            </form>
            <?php echo $links; ?>
        </div>

        <div class="modal fade" id="category_modal_info" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Category Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="category_info">

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    function show_category_edit(id) {
        $.post("<?php echo base_url(); ?>category-edit/" + id, function (t) {
            $("#category_info").html(t);
            $('#category_modal_info').modal('show');
        });
    }
//=========== its for category onkey search ============
    function categorykeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-category-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_category").html(r);
            }
        });
    }
    //=========== its for get special character =========
    function special_character() {
        var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\/~`-="
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#category_name').val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            alert(specialChars + " these special character are not allows");
            $("#category_name").focus();
        }
    }
</script>