<style type="text/css">
    #content div.box table.unique_td td{
        display: inline-block; 
    }
</style>
<form action="<?php echo base_url('b_level/Category_controller/category_update/' . $category_edit[0]->category_id); ?>" method="post" class="form-row px-3">
    <div class="col-md-6">
        <div class="form-group">
            <label for="category_name" class="mb-2">Category Name <span class="text-danger"> * </span></label>
            <input class="form-control" type="text" name="category_name" id="category_name" value="<?php echo $category_edit[0]->category_name; ?>" placeholder="Category Name"  required>
        </div>
        <div class="form-group">
            <label for="parent_category" class="mb-2">Parent Category</label>
            <select name="parent_category" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                <option value= >None</option>
                <?php foreach ($parent_category as $category) { ?>
                    <option value='<?php echo $category->category_id; ?>' <?php
                    if ($category_edit[0]->parent_category == $category->category_id) {
                        echo 'selected';
                    }
                    ?>><?php echo $category->category_name; ?></option>
                        <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="description" class="mb-2">Description</label>
            <textarea name="description" class="form-control" type="text" placeholder="Description" rows="1" id="description"><?php echo $category_edit[0]->description; ?></textarea>
        </div>
        <div class="form-group">
            <label for="status" class="mb-2">Status</label>
            <select name="status" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                <!--<option value=""></option>-->
                <option value="1" <?php
                if ($category_edit[0]->status == 1) {
                    echo 'selected';
                }
                ?>>Active</option>
                <option value="0" <?php
                if ($category_edit[0]->status == 0) {
                    echo 'selected';
                }
                ?>>Inactive</option>
            </select>
        </div>
        <!--                <div class="form-group">
                            <label for="assigned" class="mb-2">Assigned</label>
                            <select name="assigned" class="form-control">
                                <option value="" selected="selected">Assigning</option>
                                <option value="25">Adding</option>
                            </select>
                        </div>-->
        <div class="form-group text-right">
            <input type="hidden" name="category_id" id="category_id" value="<?php echo $category_edit[0]->category_id; ?>">
            <button type="submit" class="btn btn-success w-md m-b-5">Update Category</button>
        </div>
    </div>
    <div class="col-sm-6 gray-back">
        <div class="px-3 mb-3">
            <h6 class="mx-0">List of products already assigned </h6>
            <p>
            <table border="0" class="gray-back unique_td">
                <tr>
                    <?php
                    if ($category_assigned_product) {
                        $i = 0;
                        foreach ($category_assigned_product as $assinged_product) {
                            $i++;
                            ?>
                            <td id="Prod_0<?php echo $i; ?>" class="border-0 gray-back">
                                <input type="checkbox" class="checkboxes" checked value="<?php echo $assinged_product->product_id; ?>" onclick="assingned_category_product_delete(this.value)" onChange="hideData('Prod_0<?php echo $i; ?>');"> 
                                <b><?php echo $assinged_product->product_name; ?></b>
                            </td>
                            <?php
                        }
                    }
                    ?>
                </tr>
<!--                        <tr>
                    <td id="Prod_03" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_03');"> <b>Combi</b> </td>
                    <td id="Prod_04" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_04');"> <b>Wooden blind</b></td>
                </tr>
                <tr>
                    <td id="Prod_05" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_05');"> <b>Fox blind</b></td>

                </tr>-->
            </table>                                     
            </p>
        </div>
    </div>
</form>
<!-- end content / right -->
<script type="text/javascript">
    function hideData(loc) {
        document.getElementById(loc).innerHTML = '&nbsp;';
    }
    //========== its for assigned product delete ===========
    function assingned_category_product_delete(product_id) {
        var category_id = $("#category_id").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-assinged-category-product-delete",
            type: 'post',
            data: {category_id: category_id, product_id: product_id},
            success: function (r) {
//                console.log(r);
//                $("#results_category").html(r);
                alert("assigned remove successfully!");
            }
        });
    }
</script>