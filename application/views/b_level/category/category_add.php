
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>New Category</h5>
        </div>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <form action="<?php echo base_url('b_level/Category_controller/save_category'); ?>" method="post" class="form-row px-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category_name" class="mb-2">Category Name</label>
                    <input class="form-control" type="text" name="category_name" id="category_name" placeholder="Category Name"  required>
                </div>
                <div class="form-group">
                    <label for="parent_category" class="mb-2">Parent Category</label>
                    <select name="parent_category" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                         <option value=" ">None</option>
                        <?php
                        foreach ($parent_category as $category) {
                            echo "<option value='$category->category_id'>$category->category_name</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="description" class="mb-2">Description</label>
                    <textarea name="description" class="form-control" type="text" placeholder="Product Details" rows="1" id="description"></textarea>
                </div>

                <div class="form-group">
                    <label for="status" class="mb-2">Status</label>
                    <select name="status" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                        <option value=""></option>
                        <option value="1" selected>Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Add Category</button>
                </div>
            </div>
            <!--            <div class="col-sm-6 gray-back">
                            <div class="px-3 mb-3">
                                <h6 class="mx-0">List of products already assigned </h6>
                                <p>
                                <table width="80%" border="0" class="gray-back">
                                    <tr>
                                        <td id="Prod_01" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_01');"> <b>Honeycomb</b></td>
                                        <td id="Prod_02" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_02');"> <b>Roller</b></td>
                                    </tr>
                                    <tr>
                                        <td id="Prod_03" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_03');"> <b>Combi</b> </td>
                                        <td id="Prod_04" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_04');"> <b>Wooden blind</b></td>
                                    </tr>
                                    <tr>
                                        <td id="Prod_05" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_05');"> <b>Fox blind</b></td>
            
                                    </tr>
                                </table>                                     
                                </p>
                            </div>
                        </div>-->
        </form>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    function hideData(loc) {
        document.getElementById(loc).innerHTML = '&nbsp;';
    }
</script>