<style type="text/css">
    .nav-pills .nav-link {
        border: 1px groove;
    }
</style>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Blind/Shade Work</h5>
        </div>
        <!-- end box / title -->


        <div class="bs_tab">

            <ul class="nav nav-pills px-4" role="tablist">

                <?php
                foreach ($order_details as $key => $blind) {


                    // if($blind->category_name=='Blinds'){
                    //     $active = "";
                    // }else{
                    //     $active = "active";
                    // }

                    if ($blind->category_name == 'Blinds') {
                        $blind_active = "active";
                        ?>

                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#blind" role="tab">Blind Work</a>
                        </li>

                    <?php } if ($blind->category_name == 'Shades') { ?>

                        <li class="nav-item">
                            <a class="nav-link <?= @$active ?>" data-toggle="tab" href="#shade" role="tab">Shade Work</a>
                        </li>

                    <?php }
                } ?>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">


                <div class="tab-pane p-2 <?= @$blind_active ?>" id="blind" role="tabpanel">

                    <button type="button" class="btn btn-success pull-right" onclick="printContent('printBlind')" >Print Blind Work</button>

                    <div id="printBlind">

                        <div class="form-row px-2">
                            <div class="form-group col-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Order #</label>
                                    <div class="col-sm-8">
                                        <p><?= $orderd->order_id ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">S/M :</label>
                                    <div class="col-sm-6">
                                        <p><?= $orderd->side_mark ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Order Date</label>
                                    <div class="col-sm-8">
                                        <p><?= date_format(date_create($orderd->order_date), 'M-d-Y'); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">For:</label>
                                    <div class="col-sm-8">
                                        <p><?= @$for->company_name ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6 offset-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Bar code</label>
                                    <div class="col-sm-8">
                                        <img src="<?php echo base_url() . $orderd->barcode; ?>" alt="" style="max-width: 100%;" >
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="table mt-3">

                            <form action="<?= base_url('b_level/manufacturer_controller/save_blind_menufactur') ?>" id="blind_manufactur" method="post">

                                <input type="hidden" name="orderid" value="<?= $orderd->order_id ?>">
                                <table class="table table-bordered table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Qty</th>
                                            <th>Product</th>
                                            <th>W</th>
                                            <th>H</th>
                                            <th>Sc</th>
                                            <th>Sc Chk</th>
                                            <th>M</th>
                                            <th >Multi Blind</th>
                                            <th>HD</th>
                                            <th>Tilt</th>
                                            <th>Lift</th>
                                            <th>Routless</th>
                                            <th colspan="2">Valance</th>
                                            <th>Val cut chk</th>
                                            <th colspan="2">Tile cut Outs</th>
                                            <th>Cord Length</th>
                                            <th>Rooms</th>
                                            <th>Comment</th>
                                            <th>Chk</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        if (!empty($order_details)) {

                                            $CI = & get_instance();

                                            $CI->load->model('b_level/Manufactur_model');

                                            $i = 1;
                                            $bld = 1;
                                            foreach ($order_details as $key => $blind) {

                                                if ($blind->category_name == 'Blinds') {

                                                    $attr = json_decode($blind->product_attribute);

                                                    $ch = $this->db->where('product_id', $blind->product_id)->where('order_id', $orderd->order_id)->get('manufactur_data')->row();
                                                    $chk_op = (json_decode(@$ch->chk_option));
                                                    ?>  
                                                <input type="hidden" name="product_id[]" value="<?= $blind->product_id ?>">  
                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $blind->product_qty ?></td>
                                                    <td><?= $blind->product_name ?></td>
                                                    <td><?= $blind->width ?></td>
                                                    <td><?= $blind->height ?></td>

                                                    <!-- SC -->
                                                    <td>
            <?php
            if (!empty($attr)) {
                foreach ($attr as $key => $att) {

                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                    if ($a->attribute_name == 'Side Channel' || $a->attribute_name == 'Side channel') {

                        $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                    }
                }
            }
            ?>
                                                    </td>

                                                    <!-- Sc Chk   -->

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="sc_chk[<?= $blind->product_id ?>]" value="1" <?= (@$chk_op->sc_chk == '1' ? 'checked' : '') ?> class="custom-control-input" id="checkB<?= $bld ?>" >
                                                            <label class="custom-control-label" for="checkB<?= $bld ?>"></label>
                                                        </div>
                                                    </td>

                                                    <!-- Mount -->
                                                    <td>
            <?php
            if (!empty($attr)) {
                foreach ($attr as $key => $att) {

                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Mount') {

                        $MountData = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                    }
                }
            }
            ?>
                                                    </td>

                                                    <!-- Multi Blind -->
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Multi Blinds' || $a->attribute_name == 'Multi blinds') {

                                                                    $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>



                                                    <!-- HD -->
                                                    <td>
            <?php
            if (!empty($attr)) {
                foreach ($attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Hold down' || $a->attribute_name == 'Hold Down') {
                        $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                    }
                }
            }
            ?>
                                                    </td>

                                                    <!-- Tilt -->
                                                    <td><?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Tilt Type' || $a->attribute_name == 'Tilt type') {
                                                                    $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?></td>

                                                    <!-- Lift -->
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Lift Type' || $a->attribute_name == 'Lift type') {
                                                                    $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Routless -->
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Routless') {
                                                                    $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Valance -->
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Valance Type' || $a->attribute_name == 'Valance type') {
                                                                    $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Valance Type' || $a->attribute_name == 'Valance type') {
                                                                    if (!empty($att->opop)) {
                                                                        $opop = $this->db->select('op_op_name')->where('op_op_id', $att->opop[0]->op_op_id)->get('attr_options_option_tbl')->row();
                                                                        echo $opop->op_op_name;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" <?= (@$chk_op->val_cut_chk == '1' ? 'checked' : '') ?> name="val_cut_chk[<?= $blind->product_id ?>]" value="1" id="checkA<?= $bld ?>" >
                                                            <label class="custom-control-label" for="checkA<?= $bld ?>"></label>
                                                        </div>
                                                    </td>
                                                    <!-- Tile cut Outs -->
                                                    <td>

            <?php
            if (!empty($attr)) {
                foreach ($attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Tile Cut Outs' || $a->attribute_name == 'Tile cut outs') {
                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                    }
                }
            }
            ?>
                                                    </td>

                                                    <td>

                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Tile Cut Outs' || $a->attribute_name == 'Tile cut outs') {

                                                                    foreach ($att->opop as $key => $opops) {

                                                                        $opop = $this->db->select('op_op_name')->where('op_op_id', $opops->op_op_id)->get('attr_options_option_tbl')->row();
                                                                        if (!empty($opop)) {
                                                                            echo @$opop->op_op_name;
                                                                            if (!empty($opops->op_op_value)) {
                                                                                echo ' : ' . @$opops->op_op_value;
                                                                            }
                                                                            echo '<br>';
                                                                        }
                                                                    }


                                                                    // if(!empty($att->opop)){
                                                                    //     $opop = $this->db->select('op_op_name')->where('op_op_id', $att->opop[0]->op_op_id)->get('attr_options_option_tbl')->row();
                                                                    //     echo $opop->op_op_name;
                                                                    // }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <!-- Cord Length -->
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Custom Cord Length' || $a->attribute_name == 'Custom Cord Length') {
                                                                    $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Rooms -->
                                                    <td>
                                                        <?= @$blind->room; ?>
                                                        <!-- <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Room') {
                                                                    echo $att->attribute_value;
                                                                }
                                                            }
                                                        }
                                                        ?> -->
                                                    </td>
                                                    <!-- Comment -->
                                                    <td>
                                                        <?php
                                                        if (!empty($attr)) {
                                                            foreach ($attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Comment') {
                                                                    $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="chk[<?= $blind->product_id ?>]" value="1" <?= (@$chk_op->chk == '1' ? 'checked' : '') ?> class="custom-control-input" id="checkC<?= $bld ?>">
                                                            <label class="custom-control-label" for="checkC<?= $bld ?>"></label>
                                                        </div>
                                                    </td>
                                                </tr>
            <?php
        }
    }

    $bld++;
}
?>

                                    </tbody>
                                </table>

                                <button type="submit" class="btn btn-success btn-sm"> Save</button>

                            </form>

                        </div>
                    </div>

                </div>


                <div class="tab-pane p-2 <?= @$active ?>" id="shade"  role="tabpanel">

                    <button type="button" class="btn btn-success pull-right" onclick="printContent('printShade')" >Print Shade Work</button>

                    <div id="printShade">

                        <form action="<?= base_url('b_level/manufacturer_controller/save_blind_menufactur') ?>" id="shads_manufactur" method="post">


                            <input type="hidden" name="orderid" value="<?= $orderd->order_id ?>">

                            <div class="form-row px-2">

                                <div class="form-group col-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Order #</label>
                                        <div class="col-sm-8">
                                            <p><?= $orderd->order_id ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">S/M :</label>
                                        <div class="col-sm-6">
                                            <p><?= $orderd->side_mark ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Order Date</label>
                                        <div class="col-sm-8">
                                            <p><?= date_format(date_create($orderd->order_date), 'M-d-Y'); ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">For:</label>
                                        <div class="col-sm-8">
                                            <p><?= @$for->company_name ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6 offset-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Bar code</label>
                                        <div class="col-sm-8">
                                            <img src="<?php echo base_url() . $orderd->barcode; ?>" alt="" style="max-width: 100%;" >
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="table mt-3">



                                <table class=" table table-bordered table-hover table-responsive text-center">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">#</th>
                                            <th rowspan="2">Qty</th>
                                            <th rowspan="2">Product</th>
                                            <th colspan="2">Shades Size</th>
                                            <th colspan="2">Febric Size</th>
                                            <th rowspan="2">M</th>
                                            <th colspan="6">Control</th>
                                            <th colspan="6">Hardware</th>
                                            <!-- <th rowspan="2">chk</th> -->
                                            <th rowspan="2">Side by side</th>
                                            <th rowspan="2">Rooms</th>
                                            <th rowspan="2">Image Pattern</th>
                                            <th rowspan="2">Privacy Height</th>
                                            <th rowspan="2">Comment</th>
                                            <th rowspan="2">Chk</th>
                                        </tr>
                                        <tr>
                                            <th>w</th>
                                            <th>h</th>
                                            <th>w</th>
                                            <th>h</th>
                                            <th>T</th>
                                            <th>P</th>
                                            <th>L</th>
                                            <th>Mo</th>
                                            <th>Rcc</th>
                                            <th>Rch</th>
                                            <th colspan="2">Type</th>
                                            <th colspan="2">Color</th>
                                            <th colspan="2">Size</th>

                                        </tr>
                                    </thead>

                                    <tbody>

<?php
if (!empty($order_details)) {

    $sed = 1;

    foreach ($order_details as $key => $shads) {

        if ($shads->category_name == 'Shades') {

            $shades_attr = json_decode($blind->product_attribute);

            $chc = $this->db->where('product_id', $shads->product_id)->where('order_id', $orderd->order_id)->get('manufactur_data')->row();
            $chk_op = (json_decode(@$chc->chk_option));

            ?> 

                                                <input type="hidden" name="product_id[]" value="<?= $shads->product_id ?>">

                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $shads->product_qty ?></td>
                                                    <td><?= $shads->product_name ?></td>
                                                    <td><?= $shads->width ?></td>
                                                    <td><?= $shads->height ?></td>

                                                    <!-- Fabric size -->
                                                    <td>
            <?php
            if (!empty($shades_attr)) {
                foreach ($shades_attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Control Type') {

                        $as = $this->db->select('option_condition')
                                        ->where('attribute_id', $att->attribute_id)
                                        ->where('option_id', $att->options[0]->option_id)
                                        ->where('product_id', $shads->product_id)
                                        ->get('product_attr_option')->row();

                        echo @$shads->width + @($as->option_condition ? $as->option_condition : 0);
                    }
                }
            }
            ?>

                                                    </td>

                                                    <td><?= $shads->height ?></td>
                                                    <!-- ==== -->

                                                    <!-- Mount -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {

                                                            foreach ($shades_attr as $key => $mount) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $mount->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Mount') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($mount, $a->attribute_name);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <!-- T -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {

                                                            foreach ($shades_attr as $key => $att) {

                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                                                                if ($a->attribute_name == 'Control Type') {

                                                                    $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                                                                    echo @$ops->option_name;

                                                                    //$data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <!-- P -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Position') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <!-- L -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Length') {

                                                                    if ($shads->height < 55) {
                                                                        echo '<input type="text" name="cl['.trim($shads->product_id) .']" value="' . @$chk_op->cl. '" class="form-control" style="width:40px;">';
                                                                    }

                                                                    if ($shads->height > 95) {
                                                                        echo '<input type="text" name="cl['. trim($shads->product_id).']" value="' . @$chk_op->cl. '" class="form-control" style="width:40px;">';
                                                                    }

                                                                    if ($shads->height >= 55 && $shads->height <= 65) {
                                                                        echo '36"';
                                                                    }
                                                                    if ($shads->height > 65 && $shads->height <= 75) {
                                                                        echo '48"';
                                                                    }

                                                                    if ($shads->height > 75 && $shads->height <= 85) {
                                                                        echo '54"';
                                                                    }

                                                                    if ($shads->height > 85 && $shads->height <= 95) {
                                                                        echo '64"';
                                                                    }

                                                                    //$data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <!-- Mo -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                                                                if ($a->attribute_name == 'Control Type') {
                                                                    echo(@$att->opop[0]->op_op_value);
                                                                }

                                                                // if($a->attribute_name=='Control Motor'){
                                                                //     $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                // }
                                                            }
                                                        }
                                                        ?>
                                                    </td>


                                                    <!-- Rcc -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Type') {
                                                                    echo (@$att->opop[1]->op_op_value);
                                                                }
                                                                // if($a->attribute_name=='Remote Controller'){
                                                                //     $data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                // }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Rch -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Type') {
                                                                    echo(@$att->opop[2]->op_op_value);
                                                                }
                                                                // if($a->attribute_name=='Remote Channel Programming'){
                                                                //     $data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                // }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <!-- HR -->
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>HR</td>
                                                                <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Headrail') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>BR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Bottom rail' || $a->attribute_name == 'Bottom Rail') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>TB</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Tube') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>SC</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Side Channel') {
                                                                                $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                                                                                echo @$ops->option_name;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                                                         
                                                        </table>

                                                    </td>


                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>HR</td>
                                                                <td>
            <?php
            if (!empty($shades_attr)) {
                foreach ($shades_attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Headrail /Cord Color' || $a->attribute_name == 'Headrail Color') {
                        $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                    }
                }
            }
            ?>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>BR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Bottom Rail Color') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>CD</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Cord Color' || $a->attribute_name == 'Cord color') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>SC</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Side Channel') {
                                                                                if (!empty($att->opop)) {
                                                                                    $opop = $this->db->select('op_op_name')->where('op_op_id', $att->opop[0]->op_op_id)->get('attr_options_option_tbl')->row();
                                                                                    echo $opop->op_op_name;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                                                         
                                                        </table>

                                                    </td>


                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>HR</td>
                                                                <td>
            <?php
            if (!empty($shades_attr)) {
                foreach ($shades_attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Headrail') {

                        $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();

                        if (strpos($ops->option_name, 'Cassette') !== false) {

                            echo $hr_width = $shads->width - 0.25;
                        }
                        if (strpos($ops->option_name, 'Facia') !== false) {
                            echo $hr_width = $shads->width - 0.125;
                        }
                    }
                }
            }
            ?>

                                                                </td>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="hr[<?= $shads->product_id ?>]" <?= (@$chk_op->hr == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkHR<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkHR<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>BR</td>
                                                                <td>
            <?php
            if (!empty($shades_attr)) {
                foreach ($shades_attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Bottom rail' || $a->attribute_name == 'Bottom Rail') {
                        echo $shads->width - 0.875;
                    }
                }
            }
            ?>
                                                                </td>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="br[<?= $shads->product_id ?>]"  <?= (@$chk_op->br == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkBR<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkBR<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>TB</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Tube') {
                                                                                echo $shads->width - 0.875;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>


                                                                </td>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="tb[<?= $shads->product_id ?>]"  <?= (@$chk_op->tb == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkTB<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkTB<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>SC</td>
                                                                <td>
            <?php
            if (!empty($shades_attr)) {
                foreach ($shades_attr as $key => $att) {

                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Side Channel') {
                        $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                        if ($ops->option_name == 'Yes') {
                            echo @$shads->height - 2.25;
                        }
                    }
                }
            }
            ?>


                                                                </td>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="sc[<?= $shads->product_id ?>]"  <?= (@$chk_op->sc == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checksc<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checksc<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr>   

                                                        </table>

                                                    </td>

            <!-- <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="checkB">
                    <label class="custom-control-label" for="checkB"></label>
                </div>
            </td> -->

                                                    <!-- Side by side -->
                                                    <td>
            <?php
            if (!empty($shades_attr)) {
                foreach ($shades_attr as $key => $att) {
                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                    if ($a->attribute_name == 'Side By Side') {
                        $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                    }
                }
            }
            ?>
                                                    </td>

                                                    <!-- Rooms -->
                                                    <td>
                                                        <?= $shads->room ?>
                                                        <!-- <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Room') {
                                                                    echo $att->attribute_value;
                                                                }
                                                            }
                                                        }
                                                        ?> -->
                                                    </td>
                                                    <!-- Image Pattern -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Image Pattern') {
                                                                    echo $att->attribute_value;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Privacy Height   -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Privacy Height') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Comment -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Comment') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="chk[<?= $shads->product_id ?>]"  <?= (@$chk_op->chk == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkB<?= $sed ?>">
                                                            <label class="custom-control-label" for="checkB<?= $sed ?>"></label>
                                                        </div>
                                                    </td>

                                                </tr>
                                                        <?php
                                                    }
                                                    $sed++;
                                                }
                                            }
                                            ?>
                                    </tbody>
                                </table>

                                <button type="submit" class="btn btn-success btn-sm"> Save</button>

                            </div>
                        </form>
                    </div>

                </div>


                <a href="<?= base_url('b_level/Manufacturer_controller/manufacturing_level/') . $orderd->order_id; ?>" class="btn btn-success"  style="margin-left: 25px;">Go to Manufacture Label</a>

            </div>
        </div>
    </div>
</div>
<!-- end content / right -->

<style type="text/css">
    .rotated { 
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
</style>

<script type="text/javascript">
    //print a div
    function printContent(el) {

        //$('#' + el).toggleClass('rotated');
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }


    // submit form and add data
    $("#blind_manufactur").on('submit', function (e) {
        e.preventDefault();

        var submit_url = "<?= base_url() ?>b_level/manufacturer_controller/save_blind_menufactur";

        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                if (res === '1') {
                    toastr.success('Success! - Save successfully');
                }

            }, error: function () {
                alert('error');
            }
        });
    });


    // submit form and add data
    $("#shads_manufactur").on('submit', function (e) {
        e.preventDefault();

        var submit_url = "<?= base_url() ?>b_level/manufacturer_controller/save_blind_menufactur";

        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                if (res === '1') {
                    toastr.success('Success! - Save successfully');
                }

            }, error: function () {
                alert('error');
            }
        });
    });





</script>

