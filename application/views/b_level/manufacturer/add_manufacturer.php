<?php $currency = $company_profile[0]->currency; ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manufaturing</h5>
            <div class="col-sm-6 text-right">
                <!--<a href="new-order" class="btn btn-success btn-sm mt-1">Add</a>-->
            </div>
        </div>

        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
            <a href="new-order" class="btn btn-success btn-sm mt-1" style="margin-top: -3px !important; ">Add</a>
        </p>

        <div class="collapse show px-3 mb-3" id="collapseExample">

            <div class="border p-3">

                <form class="form-horizontal" action="<?= base_url('add-manufacturer') ?>" method="post">

                    <fieldset>

                        <div class="row">
                            <div class="col-md-4">
                                <select class="form-control" name="customer_id" >
                                    <option value="">--Select Customer--</option>
                                    <?php foreach ($customers as $c) { ?>
                                        <option value="<?= $c->customer_id ?>" <?php
                                        if ($customerid == $c->customer_id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                        <?= $c->first_name; ?> <?= $c->last_name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <input type="text"  name="order_date" id="order_date" class="form-control datepicker mb-3" value="<?php echo $order_date; ?>" placeholder="Order Date">
                            </div>                          

                            <div class="col-md-4">
                                <select class="form-control" name="order_stage" >
                                    <option value="">--Select Status--</option>
                                    <option value="1" <?php
                                    if ($order_stage == 1) {
                                        echo 'selected';
                                    }
                                    ?>>Quote</option>
                                    <option value="2" <?php
                                    if ($order_stage == 2) {
                                        echo 'selected';
                                    }
                                    ?>>Paid</option>
                                    <option value="3" <?php
                                    if ($order_stage == 3) {
                                        echo 'selected';
                                    }
                                    ?>>Partially Paid</option>
                                    <option value="4" <?php
                                    if ($order_stage == 4) {
                                        echo 'selected';
                                    }
                                    ?>>Shipping</option>
                                    <option value="5" <?php
                                    if ($order_stage == 5) {
                                        echo 'selected';
                                    }
                                    ?>>Cancelled</option>
                                </select>
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>

            </div>

        </div>


        <!-- end box / title -->
        <div class="table-responsive px-3">
            <?= @$links; ?>

            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>Order/Quote No</th>
                        <th>Client Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <!-- <th>Status</th> -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($orderd)) {
                        foreach ($orderd as $key => $value) {

                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                            ?>
                            <tr>
                                <td><?= $value->order_id; ?></td>
                                <td>
                                    <?php
                                    if ($value->level_id != 1) {
                                        $sql = "SELECT * FROM company_profile WHERE user_id = $value->level_id";
                                        $sql_result = $this->db->query($sql)->result();
//                                      dd($sql_result);
                                        echo @$sql_result[0]->company_name;
                                    } else {
                                        echo $value->customer_name;
                                    }
                                    ?></td>
                                <td><?= $value->side_mark; ?></td>

                                <td>
                                    <?=date_format(date_create($value->order_date),'M-d-Y');?>
                                </td>
                                <!-- <td><?= $currency ?><?= $value->grand_total ?></td> -->

<!--                                 <td>
                                    <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                        <option value="">--Select--</option>
                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                        <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Manufacturing</option>
                                    </select>
                                </td> -->

                                <td class="width_140">
                                    <a href="<?= base_url('b_level/Manufacturer_controller/quatation/') . $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-cogs"></i> </a>
                                </td>

                            </tr>

                            <?php
                        }
                    } else {
                ?>

                    <div class="alert alert-danger"> There have no order found..</div>
                
                <?php } ?>

                </tbody>
            </table>
            <?= @$links; ?>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">


    function setOrderStage(stage_id, order_id) {

        $.ajax({
            url: "b_level/order_controller/set_order_stage/" + stage_id + "/" + order_id,
            type: 'GET',
            success: function (r) {
                toastr.success('Success! - Order Stage Set Successfully');
                setTimeout(function () {
                    window.location.href = window.location.href;
                }, 2000);
            }
        });
    }



</script>