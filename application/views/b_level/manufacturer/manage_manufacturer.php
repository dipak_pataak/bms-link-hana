
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <!-- box / title -->
                    <div class="title row">
                        <h5>Manufacturer Formula</h5>
                    </div>
                    <!-- end box / title -->
                    <div class="px-3">

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sl#</th>
                                    <th>Variable Name</th>
                                    <th>Product Name</th>
                                    <th>Formula</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Chain Length</td>
                                    <td>Roller Blind</td>
                                    <td>Round (Drop - Roller_DropAllowance)</td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>manufacturer-edit/1" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Cord Length</td>
                                    <td>Roller Blind</td>
                                    <td>Drop - HeamAllowance</td>
                                    <td>
                                        <a href="manufacturer_add.html" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Febric Qty</td>
                                    <td>Roller Blind</td>
                                    <td>Drop - DropAllowance</td>
                                    <td>
                                        <a href="manufacturer_add.html" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Headrail Cut</td>
                                    <td>Roller Blind</td>
                                    <td>Round (Drop - Roller_DropAllowance)</td>
                                    <td>
                                        <a href="manufacturer_add.html" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Chain Length</td>
                                    <td>Roller Blind</td>
                                    <td>Round (Drop - Roller_DropAllowance)</td>
                                    <td>
                                        <a href="manufacturer_add.html" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- end content / right -->