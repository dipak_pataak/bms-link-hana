<!--============ its for multiselects ============-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />-->
<!-- content / right -->
<style type="text/css">
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 290px;
    }
    .filter_select2_cls .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 130px;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage Pattern</h5>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <p class="px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>            
            <?php if ($this->permission->check_label('add_pattern')->create()->access()) { ?>
                <button type="button" class="btn btn-success  mb-1" data-toggle="modal" data-target="#patternFrm">Add Pattern</button>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm mt-1 action-delete" style="margin-top: -3px !important;" onClick="return action_delete(document.recordlist)" >Delete</a>
            <?php } ?>
        </p>
        <div class="collapse px-3 mt-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>pattern-model-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" class="form-control mb-3 pattern_name" name="pattern_name" placeholder="Enter Pattern Name">
                            </div>
                            <span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <div class="filter_select2_cls">
                                    <select name="parent_cat" class="form-control select2 parent_cat" id="parent_cat" data-placeholder="-- select category --">
                                        <option value=""></option>
                                        <?php foreach ($parent_category as $category) { ?>
                                            <option value='<?php echo $category->category_id; ?>' <?php
                                            ?>><?php echo $category->category_name; ?></option>";
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <div class="filter_select2_cls">
                                    <select name="pattern_status" class="form-control pattern_status select2" id="pattern_status" data-placeholder="-- select status --">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="patternFrm" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pattern Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url('b_level/Pattern_controller/pattern_save'); ?>" method="post" class="">
                            <div class="form-group row">
                                <label for="category_id" class="col-md-3 control-label"> Category Name <span class="text-danger"> * </span></label>
                                <div class="col-md-6" style="width: 200px;">
                                    <select name="category_id" class="form-control select2" id="category_id" data-placeholder="-- select one --" required>
                                        <option value=" ">None</option>
                                        <?php foreach ($parent_category as $val) { ?>
                                            <option value="<?= $val->category_id ?>"><?= ucwords($val->category_name); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!--                            <div class="form-group row">
                                                            <label for="" class="col-md-3 control-label">Types</label>
                                                            <div class="col-md-6">
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" class="custom-control-input" id="pattern" value="Pattern" name="pattern_type" checked>
                                                                    <label class="custom-control-label" for="pattern">Pattern</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" class="custom-control-input" id="model" value="Model" name="pattern_type">
                                                                    <label class="custom-control-label" for="model">Model</label>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <div class="form-group row">
                                <label for="pattern_name" class="col-md-3 control-label">Name <span class="text-danger"> * </span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="pattern_name" id="pattern_name" required>
                                    <input type="radio" class="custom-control-input" id="pattern" value="Pattern" name="pattern_type" checked>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 control-label">Status</label>
                                <div class="col-md-6">
                                    <select name="status" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                                        <option value=""></option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="offset-3 col-md-2">
                                    <button type="submit" class="btn btn-success w-md m-b-5">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="patternkeyup_search()" placeholder="Search..." tabindex="">
                </div>
                <!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                    </ul>
                                </div>-->
            </div>          
        </div>
        <div class="table-responsive p-3" id="results_pattern">
        <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Pattern_controller/manage_action') ?>">
            <input type="hidden" name="action">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th>SL No.</th>
                        <th>Pattern Name</th>
                        <th>Category Name</th>
                        <!--<th>Pattern Type</th>-->
                        <th>Assigned Product</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    foreach ($pattern_list as $value) {
                        $sql = "SELECT product_name FROM product_tbl WHERE pattern_models_ids = $value->pattern_model_id";
                        $results = $this->db->query($sql)->result();
//                        echo '<pre>';                        print_r($results); echo '<pre>';
                        $sl++;
                        ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $value->pattern_model_id; ?>" class="checkbox_list">  
                            </td>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->pattern_name; ?></td>
                            <td>
                                <?php
                                if ($value->pattern_category_id == 0) {
                                    echo "None";
                                }
                                echo $value->category_name;
                                ?>
                            </td>
                            <!--<td><?php echo $value->pattern_type; ?></td>-->
                            <td>
                                <?php
                                if ($results) {
                                    echo "<ul>";
                                    $i = 0;
                                    foreach ($results as $result) {
                                        $i++;
                                        echo "<li>" . $i . ") " . $result->product_name . "</li>";
                                    }
                                    echo "</ul>";
                                } else {
                                    echo '<p>The products will be assigned at the later stage</p>';
                                }
                                ?>
                            </td>
                            <td><?php
                                if ($value->pattern_status == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>
                            <td>
                                <!--<a href="<?php echo base_url(); ?>pattern-edit/<?php echo $value->pattern_model_id; ?>" class="btn btn-warning default btn-sm"><i class="fa fa-pencil"></i></a>-->
                                <a href="javascript:void(0)" class="btn btn-warning default btn-sm" onclick="pattern_edit_form(<?php echo $value->pattern_model_id; ?>);" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url(); ?>pattern-delete/<?php echo $value->pattern_model_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
                <?php if (empty($pattern_list)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="6" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>
            </form>
            <?php echo $links; ?>
        </div>
        <div class="modal fade" id="pattern_model_info" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Pattern Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="pattern_info">

                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    function pattern_edit_form(id) {
        $.post("<?php echo base_url(); ?>pattern-edit/" + id, function (t) {
            $("#pattern_info").html(t);
            $('#pattern_model_info').modal('show');
        });
    }

    $("#pattern_name").on('mouseout', function (e) {

        $('#pattern_name').val(($('#pattern_name').val().replace(/['"]+/g, '&quot;')));

    });

    function patternkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-pattern-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_pattern").html(r);
            }
        });
    }

//=========== its for get special character =========
//    function special_character() {
//        var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\/~`-="
//        var check = function (string) {
//            for (i = 0; i < specialChars.length; i++) {
//                if (string.indexOf(specialChars[i]) > -1) {
//                    return true
//                }
//            }
//            return false;
//        }
//        if (check($('#pattern_name').val()) == false) {
//            // Code that needs to execute when none of the above is in the string
//        } else {
//            alert(specialChars + " these special character are not allows");
//            $("#pattern_name").focus();
//        }
//    }
</script>