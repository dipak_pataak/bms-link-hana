<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>SL No.</th>
            <th>Pattern Name</th>
            <th>Category Name</th>
            <th>Assigned Product</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sl = 0;
        foreach ($pattern_list as $value) {
            $sql = "SELECT product_name FROM product_tbl WHERE pattern_models_ids = $value->pattern_model_id";
            $results = $this->db->query($sql)->result();
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl; ?></td>
                <td><?php echo $value->pattern_name; ?></td>
                <td>
                    <?php
                    if ($value->pattern_category_id == 0) {
                        echo "None";
                    }
                    echo $value->category_name;
                    ?>
                </td>
                <!--<td><?php echo $value->pattern_type; ?></td>-->
                <td>
                    <?php
                    echo "<ul>";
                    foreach ($results as $result) {
                        echo "<li>" . $result->product_name . "</li>";
                    }
                    echo "</ul>";
                    ?>
                </td>
                <td><?php
                    if ($value->pattern_status == 1) {
                        echo 'Active';
                    } else {
                        echo "Inactive";
                    }
                    ?></td>
                <td>
                    <!--<a href="<?php echo base_url(); ?>pattern-edit/<?php echo $value->pattern_model_id; ?>" class="btn btn-warning default btn-sm"><i class="fa fa-pencil"></i></a>-->
                    <a href="javascript:void(0)" class="btn btn-warning default btn-sm" onclick="pattern_edit_form(<?php echo $value->pattern_model_id; ?>);" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                    <a href="<?php echo base_url(); ?>pattern-delete/<?php echo $value->pattern_model_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        <?php } ?>

    </tbody>
    <?php if (empty($pattern_list)) { ?>
        <tfoot>
            <tr>
                <th colspan="6" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>