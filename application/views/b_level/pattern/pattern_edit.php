<form action="<?php echo base_url('b_level/Pattern_controller/pattern_update/' . $pattern_edit[0]['pattern_model_id']); ?>" method="post" class="form-row px-3">
    <div class="col-md-6">        
        <div class="form-group">
            <label for="category_id" class="mb-2">Category Name <span class="text-danger"> * </span></label>
            <select class="form-control select2" id="category_id" name="category_id" data-placeholder='-- select one --' required>
                <option value=" ">None</option>
                <?php foreach ($parent_category as $category) { ?>
                    <option value="<?php echo $category->category_id; ?>" <?php
                    if ($category->category_id == $pattern_edit[0]['category_id']) {
                        echo 'selected';
                    }
                    ?>>
                        <?php echo $category->category_name; ?></option>
                <?php }
                ?>
            </select>
        </div>
        <!--        <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label" style="width: 75px;">
                            <input type="radio" class="form-check-input"id="pattern" value="Pattern" <?php
        if ($pattern_edit[0]['pattern_type'] == 'Pattern') {
            echo 'checked';
        }
        ?> name="pattern_type">Pattern
                        </label>
                        <label class="form-check-label" style="width: 75px;">
                            <input type="radio" class="form-check-input"  id="model" value="Model" <?php
        if ($pattern_edit[0]['pattern_type'] == 'Model') {
            echo 'checked';
        }
        ?> name="pattern_type"> Model
                        </label>
                    </div>
                </div>-->

        <div class="form-group">
            <label for="product_code" class="mb-2">Name <span class="text-danger"> * </span></label>
            <input class="form-control" type="text" name="pattern_name" value="<?php echo $pattern_edit[0]['pattern_name']; ?>" id="pattern_name" required>
            <input type="radio" class="custom-control-input" id="pattern" value="Pattern" name="pattern_type" checked>
        </div>
        <div class="form-group">
            <label for="status" class="mb-2">Status</label>
            <select class="form-control select2" id="status" name="status" data-placeholder='-- select one --'>
                <!--<option value=""></option>-->
                <option value="1" <?php
                if ($pattern_edit[0]['status'] == 1) {
                    echo "selected";
                }
                ?>>Active</option>
                <option value="0"  <?php
                if ($pattern_edit[0]['status'] == 0) {
                    echo "selected";
                }
                ?>>Inactive</option>
            </select>
        </div>
        <div class="form-group text-left">
            <input type="hidden" name="pattern_model_id" id="pattern_model_id" value="<?php echo $pattern_edit[0]['pattern_model_id']; ?>">
            <button type="submit" class="btn btn-success w-md m-b-5">Update Pattern</button>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="px-3 mb-3">
            <h6 class="mx-0">List of products already assigned </h6>
            <p>
            <table width="80%" border="0" class="gray-back">
                <?php
//                         print_r($assigned_pattern_product)
                ?>
                <tr>
                    <?php
                    $i = 0;
                    foreach ($assigned_pattern_product as $single) {
                        $i++;
                        ?>
                        <td id="Prod_0<?php echo $i; ?>" class="border-0 gray-back">
                            <input type="checkbox" class="checkboxes" checked value="<?php echo $single->product_id; ?>" onclick="assingned_product_delete(this.value)" onChange="hideData('Prod_0<?php echo $i; ?>');"> 
                            <b><?php echo $single->product_name; ?></b>
                        </td>
                    <?php } ?>
<!--<td id="Prod_02" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_02');"> <b>Roller</b></td>-->
                </tr>
<!--                        <tr>
                    <td id="Prod_03" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_03');"> <b>Combi</b> </td>
                    <td id="Prod_04" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_04');"> <b>Wooden blind</b></td>
                </tr>
                <tr>
                    <td id="Prod_05" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_05');"> <b>Fox blind</b></td>
                </tr>-->
            </table>
            </p>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        style_path = "resources/css/colors";
    });
    function hideData(loc) {
        document.getElementById(loc).innerHTML = '&nbsp;';
    }
//========== its for assigned product delete ===========
    function assingned_product_delete(product_id) {
        var pattern_model_id = $("#pattern_model_id").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-assinged-product-delete",
            type: 'post',
            data: {pattern_model_id: pattern_model_id, product_id: product_id},
            success: function (r) {
//                $("#results_category").html(r);
                alert("assigned remove successfully!");
            }
        });
    }
    $("#pattern_name").on('mouseout', function (e) {

        $('#pattern_name').val(($('#pattern_name').val().replace(/['"]+/g, '&quot;')));

    });

</script>