
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Add Pattern/Model</h5>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <form action="<?php echo base_url('b_level/Pattern_controller/pattern_save'); ?>" method="post" class="form-row px-3">
            <div class="offset-3 col-md-6">
                <div class="form-group">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="pattern" value="Pattern" name="pattern_type" checked>
                        <label class="custom-control-label" for="pattern">Pattern</label>
                    </div>

                    <!-- Default inline 2-->
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="model" value="Model" name="pattern_type">
                        <label class="custom-control-label" for="model">Model</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="product_code" class="mb-2">Name</label>
                    <input class="form-control" type="text" name="pattern_name" id="pattern_name" required>
                </div>
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-success w-md m-b-5">Add Pattern/Model</button>
                </div>
            </div>
            <div class="col-sm-6">
<!--                <div class="px-3 mb-3">
                    <h6 class="mx-0">List of products already assigned </h6>
                    <p>
                    <table width="80%" border="0" class="gray-back">
                        <tr>
                            <td id="Prod_01" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_01');"> <b>Honeycomb</b></td>
                            <td id="Prod_02" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_02');"> <b>Roller</b></td>
                        </tr>
                        <tr>
                            <td id="Prod_03" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_03');"> <b>Combi</b> </td>
                            <td id="Prod_04" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_04');"> <b>Wooden blind</b></td>
                        </tr>
                        <tr>
                            <td id="Prod_05" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_05');"> <b>Fox blind</b></td>

                        </tr>
                    </table>
                    </p>
                </div>-->
            </div>
        </form>

    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    $(document).ready(function () {
        style_path = "resources/css/colors";
    });

    function hideData(loc) {
        document.getElementById(loc).innerHTML = '&nbsp;';
    }

    $("#pattern_name").on('change',function(e){

        $('#pattern_name').val(($('#pattern_name').val().replace(/['"]+/g, '&quot;')));

    });
      
</script>