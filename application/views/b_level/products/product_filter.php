<style type="text/css">
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 130px;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage Products</h5>
        </div>
        <!-- end box / title -->
        <p class="px-3">
            <!--            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Filter
                        </button>-->
            <?php if ($this->permission->check_label('add')->create()->access()) { ?>
                <a href="add-product" class="btn btn-success mt-1" style="margin-top: -5px !important;">Add New Product</a>
            <?php } ?>
        </p>
        <div class="collapsexxx px-3 mt-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>product-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <!-- <div class="col-md-4">
                                <input type="text" class="form-control mb-3 product_name" name="product_name" placeholder="Product Name" value="<?php echo $product_name; ?>">
                            </div><span class="or_cls">-- OR --</span> -->
                            <div class="col-md-3">
                                <select class="form-control select2 category_name" name="category_name" data-placeholder="-- Select Category --">
                                    <option value=""></option>
                                    <?php
                                    foreach ($get_category as $category) {
                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                    }
                                    ?>
                                </select>
                            </div> 

                             <div class="col-md-3">
                                <select class="form-control select2 price_style_type" name="price_style_type" data-placeholder="-- Select Price Style --">
                                    <option value=""></option>
                                    <option value="1">Row/Column style</option>
                                    <option value="4">Group style</option>
                                    <option value="5">Sqm price style</option>
                                    <option value="2">Price by Sq.ft style </option>
                                    <option value="3">Fixed product Price Style</option>
                                </select>
                            </div>
                             <div class="col-md-2">
                                <select class="form-control select2 status" name="status" data-placeholder="-- Select Status --">
                                    <option value=""></option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                    
                                </select>
                            </div>

                            
<!--                            &nbsp; <span class="or_cls">-- OR --</span>
                            <div class="col-md-3">
                                <select class="form-control select2 pattern_model" name="pattern_model" data-placeholder="-- select Pattern Model --">
                                    <option value=""></option>
                                    <?php foreach ($patern_model as $single_pattern) { ?>
                                        <option value='<?php echo $single_pattern->pattern_model_id; ?>' <?php
                                        if ($pattern_model == $single_pattern->pattern_model_id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                                    <?php echo $single_pattern->pattern_name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>-->

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
   <div class="col-sm-12 text-right mt-1">
            <div class="form-group row">
                <label for="keyword" class="col-sm-2 col-form-label offset-8 text-right"></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="productkeyup_search()" placeholder="Search..." tabindex="">
                </div>
                <!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                    </ul>
                                </div>-->
            </div>          
        </div>
        <table class="table table-bordered mb-3 mt-2" id="results_product">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="25%">Product Name</th>
                    <th width="15%">Category</th>
                    <th width="15%">Price Style Type</th>
                    <th width="15%">Dflt Discnt.(%)</th>
                    <th width="10%">Status</th>
                    <th  width="15%" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                if (!empty($get_product_filter)) {

                    $i = 1;
                    foreach ($get_product_filter as $key => $p) {
                        ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $p->product_name ?></td>
                            <td><?= $p->category_name ?></td>
                            <!--<td><?= $p->pattern_name ?></td>-->

                            <td>
                                <?php
                                if ($p->price_style_type == 1) {
                                    echo "Row/Column style";
                                } elseif ($p->price_style_type == 2) {
                                    echo "Price by Sq.ft style";
                                } elseif ($p->price_style_type == 4) {
                                    echo "Group Price";
                                } elseif($p->price_style_type == 3){
                                    echo "Fixed product Price Style";
                                }
                                elseif($p->price_style_type == 5){
                                    echo "Sqm price style";
                                }
                                ?>
                            </td>
                           
                            <!--<td><?= $p->init_stock ?></td>-->
                            <td><?= $p->dealer_price ?></td>
                            <!--<td><?= $p->individual_price ?></td>-->
                            <td><?= ($p->active_status = 1 ? 'Active' : 'Inactive'); ?></td>
                            <td  class="text-center">
                                <a href="<?php echo base_url(); ?>product-edit/<?= $p->product_id ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url('b_level/product_controller/maping_product_attribute'); ?>/<?= $p->product_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Map product to Attributes" ><i class="fa fa-map"></i></a>
                                <a href="<?php echo base_url(); ?>delete-product/<?= $p->product_id ?>"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>

            </tbody>
            <?php if (empty($get_product_filter)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="10" class="text-center text-danger">No record found!</th>
                    </tr> 
                </tfoot>
            <?php } ?>
        </table>
<!--            <p class="text-right">
            <button type="submit" class="btn btn-success default w-md m-b-5">Save</button>
        </p>-->

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    function productkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>b-level-product-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_product").html(r);
            }
        });
    }
</script>