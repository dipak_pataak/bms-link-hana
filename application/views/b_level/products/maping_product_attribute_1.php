
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Maping Product Attribute</h5>
        </div>

        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('message');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <!-- end box / title -->
        <form action="<?php echo base_url('b_level/product_controller/save_maping_product_attribute'); ?>" method="post" class="form-row px-3">

            <div class="col-md-12">

                <div class="form-group">
                    <label for="attribute" class="mb-2">Product : <?= $product_info->product_name; ?></label>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label><input type="checkbox" value="" class="checkall"> <strong>Select All </strong></label>
                    </div>
                </div>

                <!--                 <div class="form-group">
                                    <label for="attribute" class="mb-2">Attribute name</label>
                                    <select name="attribute" class="form-control select2" id="attribute" required data-placeholder='-- select one --'>
                                        <option value=""></option>
                <?php foreach ($attributes as $attribute) { ?>
                                                <option value="<?= $attribute->attribute_id ?>"><?= $attribute->attribute_name; ?></option>
                <?php } ?>
                                    </select>
                                </div> -->

                <div id="make">
                    <div class='form-group col-md-12'>

                        <?php
                        $a = 0;
                        foreach ($attributes as $attribute) {
                            $options = $this->db->where('attribute_id', $attribute->attribute_id)->get('attr_options')->result();
                            $check_att = $this->db->where('product_id', $product_info->product_id)->where('attribute_id', $attribute->attribute_id)->get('product_attribute')->row();
                            ?>

                            <div class='checkbox'>

                                <input type='checkbox' name='attribute[]' id="matt" value='<?= $attribute->attribute_id ?>' <?= (@$check_att->attribute_id == $attribute->attribute_id ? 'checked' : ''); ?>> <?= $attribute->attribute_name; ?> 

                                <?php
                                foreach ($options as $key => $op) {
                                    $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                                    $option_option_result = $this->db->query($option_option)->result();

                                    $check_op = $this->db->where('product_id', $product_info->product_id)->where('attribute_id', $attribute->attribute_id)->where('option_id', $op->att_op_id)->get('product_attr_option')->row();
                                    ?>

                                    <div style="margin-left: 20px;">

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='checkbox'>
                                            <input type='checkbox' name='option_<?= $attribute->attribute_id ?>[]' value='<?= $op->att_op_id ?>' <?= (@$check_op->option_id == $op->att_op_id ? 'checked' : ''); ?> > <?= $op->option_name ?>


                                            <?php
                                            if ($op->option_type == 2 || $op->option_type == 3 || $op->option_type == 4) {

                                                foreach ($option_option_result as $key => $opop) {

                                                    $options_option_options = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();

                                                    $check_op_op = $this->db->where('product_id', $product_info->product_id)->where('attribute_id', $attribute->attribute_id)->where('op_op_id', $opop->op_op_id)->get('product_attr_option_option')->row();
                                                    ?>
                                                    <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='checkbox' style="margin-left: 20px;">
                                                        <input type='checkbox' name='option_option_<?= $op->att_op_id ?>[]' value='<?= $opop->op_op_id ?>' <?= (@$check_op_op->op_op_id == $opop->op_op_id ? 'checked' : ''); ?> > <?= $opop->op_op_name ?>



                                                        <?php if ($opop->type == 2 || $opop->type == 3) { ?>

                                                            <div style='margin-left:30px;'>

                                                                <?php
                                                                foreach ($options_option_options as $key => $opopop) {

                                                                    $op_op_op_ops = $this->db->where('op_op_op_id', $opopop->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();

                                                                    $check_op_op_op = $this->db->where('product_id', $product_info->product_id)->where('attribute_id', $attribute->attribute_id)->where('op_op_op_id', $opopop->att_op_op_op_id)->get('product_attr_option_option_option')->row();
                                                                    ?>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='checkbox' style="margin-left: 20px;"><input type='checkbox' name='att_op_op_op_<?= $op->att_op_id ?>_<?= $opop->op_op_id ?>[]' value='<?= $opopop->att_op_op_op_id ?>' <?= (@$check_op_op_op->op_op_op_id == $opopop->att_op_op_op_id ? 'checked' : ''); ?> > <?php echo $opopop->att_op_op_op_name; ?><br/>

                                                                        <div style='margin-left:30px;'>

                                                                            <?php foreach ($op_op_op_ops as $key => $opopopop) { ?>
                                                                                <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='att_op_op_op_op_<?= $op->att_op_id ?>_<?= $opop->op_op_id ?>_<?= $opopop->att_op_op_op_id ?>[]' value='<?= $opopopop->att_op_op_op_op_id ?>'> <?php echo $opopopop->att_op_op_op_op_name; ?><br/>

                                                                            <?php } ?>

                                                                        </div>
                                                                    </div>

                                                                <?php } ?>

                                                            </div>

                                                        <?php } ?>
                                                    </div>

                                                <?php }
                                            }
                                            ?>

                                        </div>

                                    </div>

    <?php } ?>

                            </div><hr>

                            <?php $a++;
                        }
                        ?>

                    </div>

                </div>

                <input type="hidden" name="product_id" value="<?= $product_info->product_id; ?>">
                <input type="hidden" name="category_id" value="<?= $product_info->category_id; ?>">

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                </div>


            </div>

        </form>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

    $('body').on('change', '#attribute', function () {
        var attribute_id = $(this).val();
        $.ajax({
            url: "<?= base_url() ?>b_level/Product_controller/get_attributes/" + attribute_id,
            type: 'get',
            success: function (r) {
                $("#make").html(r);
            }
        });
    });


    var checked = false;
    $(".checkall").on("click", function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        //$(".checkall").prop("checked", !clicked);
        clicked = !clicked;
    });

    // $("#matt").on("click", function() {

    //     $(this).parent().next().children().children().children().not(this).prop('checked', this.checked);
    //   //$(".checkall").prop("checked", !clicked);
    //   clicked = !clicked;
    // });


    $(function () {
        $("input[type='checkbox']").change(function () {
            $(this).siblings('div')
                    .find("input[type='checkbox']")
                    .prop('checked', this.checked);
        });
    });


</script>