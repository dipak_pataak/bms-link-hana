<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div id="right">
    <div class="box new_product" style="height: 100%">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>


        <!-- box / title -->            
        <div class="title row">
            <h5 class="col-sm-6">Edit Products</h5>
        </div>
        <?php //echo '<pre>';                print_r($product);  echo '</pre>';    ?>
        <?php echo form_open('b_level/Product_controller/product_update', array('class' => 'form-row px-3', 'id' => '', 'name' => 'edit_product')); ?>

        <div class="col-lg-6 px-4">

            <div class="form-group">
                <label for="product_name" class="mb-2">Product Name (Model Name)</label>
                <input class="form-control" type="text" name="product_name" value='<?= $product->product_name ?>'  id="product_name" onmouseout="replaceString(this.value)" required >
            </div>

            <input type="hidden" name="product_id" value="<?= $product->product_id ?>">

            <div class="form-group">

                <label for="category_id" class="mb-2">Select Category </label>
                <select class="form-control select2" name="category_id" id="category_id" data-placeholder='-- select one --'>
                    <option value="none">None</option>
                    <?php foreach ($get_category as $category) { ?>
                        <option value='<?= $category->category_id ?>' <?= ($category->category_id == $product->category_id ? 'selected' : ''); ?> >
                            <?= $category->category_name ?>
                        </option>"
                    <?php } ?>
                </select>
            </div> 

            <!--            <div class="form-group">
                            <label for="subcategory_id" class="mb-2">Select Sub-Category [display only when Category has SubCategory]</label>
                            <select class="form-control select2" name="subcategory_id" id="subcategory_id" data-placeholder='-- select firstly category --'>
            <?php foreach ($get_category as $category) { ?>
                                                                                                                                <option value='<?= $category->category_id ?>' <?= ($category->category_id == $product->subcategory_id ? 'selected' : ''); ?> >
                <?= $category->category_name ?>
                                                                                                                                </option>";
            <?php } ?>
                            </select>
            
                        </div> -->

            <div class="form-group" id="subcategory_id">
                <?php
                if ($product->subcategory_id) {
                    $this->db->select('*');
                    $this->db->from('category_tbl a');
                    $this->db->where(' a.parent_category', $product->category_id);
                    $subcategory_results = $this->db->get()->result();
                    ?>
                    <label for="" class="col-sm-3 mb-2">Sub Category</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="subcategory_id" id="sub_category_id" data-placeholder="-- select one --">
                            <option value=""></option>';
                            <?php foreach ($subcategory_results as $subcat) { ?>
                                <option value='<?php echo $subcat->category_id; ?>' <?php
                                if ($product->subcategory_id == $subcat->category_id) {
                                    echo 'selected';
                                }
                                ?>>
                                            <?php echo $subcat->category_name; ?>
                                </option>;
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
            </div> 

            <div class="form-group">
                <?php
//                if ($product->category_id) {
                $category_wise_pattern_list = $this->db->select('*')
                                ->where('category_id', $product->category_id)
                                ->where('status', 1)
                                ->get('pattern_model_tbl')->result();
                ?>
                <label for="pattern_model_id" class="mb-2">Pattern</label>
                <div class="pattern_model_id">
                    <select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true">
                        <?php
                        foreach ($category_wise_pattern_list as $single_pattern) {
                            $array_of_patterns = explode(",", $product->pattern_models_ids);
                            ?>
                            <option value="<?= $single_pattern->pattern_model_id ?>" <?php
                            if (in_array($single_pattern->pattern_model_id, $array_of_patterns)) {
                                echo 'selected';
                            }
                            ?>>
                                        <?= ucwords($single_pattern->pattern_name); ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <?php // } ?>
            </div>

            <div class="form-group">
                <label for="theSelect" class="mb-2">Price Model/Style</label>
                <select name="price_style_type" class="form-control price_select select2" onchange="price_model_wise_style(this.value)" id="theSelect" data-placeholder="-- select one --">
                    <option value=""></option>
                    <option value="1" <?= ($product->price_style_type == 1 ? 'selected' : '') ?>>Row/Column style</option>
                    <option value="4" <?= ($product->price_style_type == 4 ? 'selected' : '') ?>>Group Price</option>
                    <option value="5" <?= ($product->price_style_type == 5 ? 'selected' : '') ?>>Sqm Price</option>
                    <option value="2" <?= ($product->price_style_type == 2 ? 'selected' : '') ?>>Price by Sq.ft style </option>
                    <option value="3" <?= ($product->price_style_type == 3 ? 'selected' : '') ?>>Fixed product Price Style</option>
                </select>
            </div>

            <div class="hidden isnull_price mb-3"></div>
            <div class="hidden is1 mb-3 hidden">
                <div id="ss_pp">

                    <select name="price_rowcol_style_id" onchange="setValue(this.value)" class="form-control select2" data-placeholder="-- select style --">
                        <option value=""></option>
                        <?php foreach ($style_slist as $s) { ?>
                            <option value="<?php echo $s->style_id ?>" <?php echo ($s->style_id == $product->price_rowcol_style_id ? 'selected' : '') ?> >
                                <?= $s->style_name ?>
                            </option>
                        <?php } ?>
                    </select>

                    <input type="hidden" name="price_style_id" id="price_style_id" value="<?= $product->price_rowcol_style_id ?>">
                    <a href="#" data-toggle="modal" data-target="#myModal"  class="btn btn-success btn-xs">add new price style</a>
                </div><br>
                <label for="Row_Column" class="mb-2">Row Column Style</label>
                <div id="priceseet"></div>

            </div>

            <div class="hidden is2 mb-3">

                <label for="sqft" class="mb-2">SqFt. Style</label>
                <input type="text" name="sqft_price" class="form-control" value="<?= @$product->sqft_price ?>">

            </div>

            <div class="hidden is3 mb-3">
                <label for="fixedprice" class="mb-2">Fixed Price Style</label>
                <input type="text" name="fixed_price" class="form-control" value="<?= @$product->fixed_price ?>">
            </div>



        </div>



        <div class="col-lg-6 px-4">

            <!-- <div class="form-group">
                <label  class="mb-2">Select Attributes</label>
                <select name="attribute_type_id" class="form-control select2" id="attrSelect" data-placeholder="-- select one --">
                    <option value=""></option>
            <?php foreach ($attribute_types as $val) { ?>
                                                                                                                                                                                                <option value="<?= $val->attribute_type_id ?>"><?= $val->attribute_type_name ?></option>
            <?php } ?>
                </select>
            </div> 

            <div class="isnull_attr"></div>

            <div class="form-group  iscolor" id="iscolor">
            <?= $product_attribute_mapping; ?>
            </div> -->


            <!--            <div class="form-group">
            <?php
            $product_condition_array = array();
            foreach ($checked_product_wise_condition as $product_condition) {
                $product_condition_array[] = $product_condition->condition_id;
            }
            ?>
                            <label for="product_code" class="mb-2">Select Conditions</label>
                            <div class="condition_checkbox">
            <?php
            if ($category_wise_condition) {
                foreach ($category_wise_condition as $condition) {
                    ?>
                                                                                                                                                                                                                                <label for="condition_id_<?php echo $condition->condition_id; ?>" style="min-width: 190px; min-height: 20px;">
                                                                                                                                                                                                                                    <input type="checkbox" name="condition_id[]" id="condition_id_<?php echo $condition->condition_id; ?>" value="<?php echo $condition->condition_id; ?>"
                    <?php
                    if (in_array($condition->condition_id, $product_condition_array)) {
                        echo 'checked';
                    }
                    ?>> 
                    <?php echo ucwords($condition->condition_text); ?>
                                                                                                                                                                                                                                </label>
                    <?php
                }
            }
            ?>
                            </div>
                        </div>-->


            <div class="form-group">
                <label for="product_code" class="mb-2" >Color : <?php // echo $selected_colors = $product->colors;                            ?></label>
                <div class="">
                    <select class="selectpicker form-control" id="color_id" name="color_id[]" multiple data-live-search="true">
                        <?php
                        foreach ($colors as $color) {
                            $array_of_values = explode(",", $product->colors);
                            ?>
                            <option value="<?= $color->id ?>" <?php
                            if (in_array($color->id, $array_of_values)) {
                                echo 'selected';
                            }
                            ?>>
                                        <?php echo ucwords($color->color_name) . "->( " . $color->color_number . " ) "; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <!--                <div class="color_checkbox">
                <?php
                if ($colors) {
                    foreach ($colors as $color) {
                        $array_of_values = explode(",", $product->colors);
                        ?>
                                                                                                                                                                                                                                    <label for="color_<?php echo $color->id; ?>" style="min-width: 190px; min-height: 20px;">
                                                                                                                                                                                                                                        <input type="checkbox" name="color_id[]" id="color_<?php echo $color->id; ?>" value="<?php echo $color->id; ?>"
                        <?php
                        if (in_array($color->id, $array_of_values)) {
                            echo 'checked';
                        }
                        ?>> 
                        <?php echo ucwords($color->color_name); ?>
                                                                                                                                                                                                                                    </label>
                        <?php
                    }
                }
                ?>
                                </div>-->
                <!--                <div class="">
                <?php
//                foreach ($colors as $color) {
//                    echo '<label for="color_' . $color->id . '" style="min-width: 190px; min-height: 20px;">';
//                    echo '&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="color_' . $color->id . '" name="color_id[]" value="' . $color->id . '">';
//                    echo '&nbsp;' . $color->color_name;
//                    echo '</level>';
//                }
                ?>
                                </div>-->
            </div>

            <!--                <div class="form-group">
                                <label for="cond1" class="mb-2">Shipping</label>
                                <select name="shipping" class="form-control select2" data-placeholder="-- select one --">
                                    <option value=""></option>
                                    <option value="">None</option>
                                    <option value="c1">FedEx</option>
                                    <option value="c2">DHL</option>
                                    <option value="c3">Postal</option>
                                </select>
                            </div>-->

            <!--            <div class="form-group">
                            <label for="cond2" class="mb-2">TAX</label>
                            <select name="tax" class="form-control select2"  data-placeholder="-- select one --" >
                                <option value=""></option>
                                <option value="0" >None</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>-->

            <!--            <div class="form-group">
                            <label for="product_code" class="mb-2">Opening Stock</label>
                            <input type="number" name="init_stock" class="form-control" value="<?= @$product->init_stock ?>">
                        </div>-->

            <!--            <div class="form-group">
                            <label for="product_code" class="mb-2">Individual Price</label>
                            <input class="form-control" type="number" name="individual_price" value="<?= @$product->individual_price ?>" required>
                        </div>-->
            <div class="form-group">
                <label for="product_code" class="mb-2">Status</label>
                <select name="status" class="form-control select2" data-placeholder="-- select one --">

                    <option value="1" <?php
                    if ($product->active_status == 1) {
                        echo 'selected';
                    }
                    ?>>Active</option>
                    <option value="0" <?php
                    if ($product->active_status == 0) {
                        echo 'selected';
                    }
                    ?>>Inactive</option>
                </select>
            </div>            
            <div class="form-group">
                <label for="product_code" class="mb-2">Default Discount (%)</label>
                <input class="form-control" min="0" name="dealer_price" type="number" value="<?= @$product->dealer_price ?>" required>
            </div>   
        </div>
        <?php // echo '<pre>'; print_r($product); echo '</pre>';       ?>

        <div class="col-lg-12 px-4">
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>

</div>
<!-- end content / right -->
<script type="text/javascript">



    document.forms['edit_product'].elements['category_id'].value = "<?php echo $product->category_id ?>";

//    document.forms['edit_product'].elements['pattern_model_id'].value = "<?php echo $product->pattern_models_ids ?>";
//    document.forms['edit_product'].elements['price_style_type'].value = "<?php echo $product->price_style_type ?>";
    document.forms['edit_product'].elements['status'].value = "<?php echo $product->active_status ?>";
//    document.forms['edit_product'].elements['condition_id'].value = "<?php echo $product->condition_id ?>";
//    document.forms['edit_product'].elements['shipping'].value = "<?php echo $product->shipping ?>";
//    document.forms['edit_product'].elements['tax'].value = "<?php echo $product->tax ?>";


    $(document).ready(function () {

        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().removeClass("hidden");
                    } else {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().addClass("hidden");
                    }
                }
//                success: function (r) {
//                    r = JSON.parse(r);
//                    $("#subcategory_id").empty();
//                    $("#subcategory_id").html("<option value=''>-- select one -- </option>");
//                    $.each(r, function (ar, typeval) {
//                        $('#subcategory_id').append($('<option>').text(typeval.category_name).attr('value', typeval.category_id));
//                    });
//                }
            });
            //            ========== its for category wise condition ===========
//            $.ajax({
//                url: "<?php // echo base_url();                        ?>category-wise-condition/" + category_id,
//                type: 'get',
//                success: function (r) {
//                    console.log(r);
//                    if (r !== '') {
//                        $(".condition_checkbox").empty();
//                        $(".condition_checkbox").html(r);
////                        $('#subcategory_id').slideDown().removeClass("hidden");
//                    } else {
//                        $(".condition_checkbox").html("Not Found!");
////                        $('#subcategory_id').slideDown().addClass("hidden");
//                    }
//                }
//            });

//            ============= its for category-wise-pattern ==============
            $.ajax({
                url: "<?php echo base_url(); ?>category-wise-pattern/" + category_id,
                type: 'get',
                success: function (r) {
//                    console.log(r);
                    if (r !== '') {
                        $(".pattern_model_id").html(r);
                        $('.selectpicker').selectpicker();
                    } else {
                        $(".pattern_model_id").html("Not Found!");
                        $(".pattern_model_id").css({'color': 'red'});
                    }
                }
            });
        });

        //$('.is1').removeClass("hidden");

    });


    $(document).ready(function () {
        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);

        theDiv.slideDown().removeClass("hidden");
        theDiv.siblings('[class*=is]').slideUp(function () {
            $(this).addClass("hidden");
        });
    });


    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);


        theDiv.slideDown().removeClass("hidden");
        theDiv.siblings('[class*=is]').slideUp(function () {
            $(this).addClass("hidden");
        });
    });


//    ============ its for price_model_wise_style =============
    function price_model_wise_style(id) {

        $.ajax({
            url: "<?php echo base_url(); ?>price-model-wise-style",
            type: "post",
            data: {id: id},
            success: function (r) {
                r = JSON.parse(r);
                $("#price_rowcol_style_id").empty();
                $("#price_rowcol_style_id").html("<option value=''>-- select one -- </option>");

                $.each(r, function (ar, typeval) {
                    $('#price_rowcol_style_id').append($('<option>').text(typeval.style_name).attr('value', typeval.style_id));
                });
            }
        });
    }



    $("#attrSelect").change(function () {

        var attr_type_id = $("#attrSelect option:selected").val();
        var url = "<?php echo base_url('b_level/Attribute_controller/get_attr_by_attr_types/'); ?>/" + attr_type_id;

        $.ajax({
            url: url,
            type: 'get',
            success: function (r) {
                $("#iscolor").html(r);
                $('#iscolor').slideDown().removeClass("hidden");
            }
        });


    });


    function shuffle_pricebox(elem) {
        //alert(elem + ' - ' +document.getElementById(elem).disabled);
        if (!document.getElementById(elem).disabled)
            document.getElementById(elem).disabled = true;
        else
            document.getElementById(elem).disabled = false;
    }




</script>

<?php $this->load->view($js) ?>

<script type="text/javascript">
    function replaceString(v) {
        $('#product_name').val((v.replace(/['"]+/g, '&quot;')));
    }
</script>



