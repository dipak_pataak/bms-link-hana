
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Group Price Mapping</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <!-- end box / title -->
        <div class="px-3">

            <?=form_open('b_level/product_controller/save_price_model_mapping')?>

                <div class="form-group row">
                    <label for="group_price" class="col-xs-3 col-form-label"> Group price  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <select name="group_price" class="form-control select2 " >
                            <option value="">--Select group price--</option>
                            <?php foreach ($style_slist as $s) { ?>
                                <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="product_id" class="col-xs-3 col-form-label">Product  <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                         <select name="product_id" class="form-control select2 " onchange="getPatter(this.value)">
                            <option value="">--Select Product--</option>
                            <?php foreach ($product_info as $p) { ?>
                                <option value="<?php echo $p->product_id ?>"><?= $p->product_name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="pattern_idss" class="col-xs-3 col-form-label">Pattern   <span class="text-danger"> * </span></label>
                    <div class="col-xs-9">
                        <select class="select2 form-control" id="pattern_idss" name="pattern_id[]" multiple data-live-search="true" required="">

                        </select>
                    </div>
                </div>


                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5" >Save</button>
                    <button type="reset" class="btn btn-primary w-md m-b-5" >Reset</button>
                </div>

            <?=form_close();?>
        </div>
    </div>
    
    <div class="box">
        <div class="title">
            <h3>List Of Group Price Mapping</h3>
        </div>
        <div class="table-responsive p-3">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#.SL</th>
                        <th width="10%">Product Name</th>
                        <th width="15%">Pattern Name</th>
                        <th width="15%">Group Name</th>
                        <th  width="15%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0; // + $pagenum;
                    foreach ($map_info as $value) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->product_name; ?></td>
                            <td><?php echo $value->pattern_name; ?></td>
                            <td><?php echo $value->style_name; ?></td>
                            
                            <td class="text-center">
                                <?php if($this->permission->check_label('group_price_mapping')->delete()->access()){?>
                                <!-- <a href="<?php echo base_url(); ?>" class="btn btn-warning default btn-sm"><i class="fa fa-pencil"></i></a> -->
                                <a href="<?php echo base_url('b_level/product_controller/delete_price_model_mapping'); ?>/<?php echo $value->id; ?>" class="btn btn-danger danger btn-sm" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>

                        </tr>
                        
                    <?php } ?>

                </tbody>
                <?php if (empty($map_info)) { ?>
                    <tfoot>
                    <th colspan="6" class="text-center">
                        No result found!
                    </th>
                    </tfoot>
                <?php } ?>
            </table>

        </div>
    </div>

</div>
<!-- end content / right -->

<script type="text/javascript">
    
    function getPatter(id){

        var url = "<?php echo site_url('b_level/Product_controller/get_product_pattern')?>/"+id;

        $.ajax({

            url : url,
            type: "GET",
            success: function(data)
            {
                $('#pattern_idss').html(data)
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }

        });

    }


</script>