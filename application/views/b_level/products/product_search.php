<table class="table table-bordered mb-3" id="results_product">
    <thead>
        <tr>
            <th width="5%">#</th>
            <th width="25%">Product Name</th>
            <th width="15%">Category</th>
            <th width="15%">Price Style Type</th>
            <th width="15%">Dflt Discnt.(%)</th>
            <th width="10%">Status</th>
            <th  width="15%" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($products)) {
            $i = 1;
            foreach ($products as $key => $p) {
                ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <td><?= $p->product_name ?></td>
                    <td>
                        <?php
                        if ($p->product_cat_id == 0) {
                            echo "None";
                        }
                        echo $p->category_name;
                        ?>
                    </td>
                    <!--<td><?= $p->pattern_name ?></td>-->
                    <td>
                        <?php
                        if ($p->price_style_type == 1) {
                            echo "Row/Column style";
                        } elseif ($p->price_style_type == 2) {
                            echo "Price by Sq.ft style";
                        } elseif ($p->price_style_type == 4) {
                            echo "Group Price";
                        } elseif($p->price_style_type == 3){
                            echo "Fixed product Price Style";
                        }
                        ?>
                    </td>
                    <!--<td><?= $p->init_stock ?></td>-->
                    <td><?= $p->dealer_price ?></td>
                    <!--<td><?= $p->individual_price ?></td>-->
                    <td>
                        <?php
                        if ($p->active_status == 1) {
                            echo 'Active';
                        } else {
                            echo 'Inactive';
                        }
                        ?>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo base_url(); ?>product-edit/<?= $p->product_id ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url('b_level/product_controller/maping_product_attribute'); ?>/<?= $p->product_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Map product to Attributes" ><i class="fa fa-map"></i></a>
                        <a href="<?php echo base_url(); ?>delete-product/<?= $p->product_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" ><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>

    </tbody>
    <?php if (empty($products)) { ?>
        <tfoot>
            <tr>
                <th colspan="10" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>