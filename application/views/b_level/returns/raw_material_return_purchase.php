
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Raw Material Return</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">

            <div class="row" id="supplier_info">
                <div class="col-sm-6">
                    <table class="table-bordered">
                        <tr>
                            <td class="text-center">Purchase ID</td>
                            <td class="text-center"><?= ($purchase->purchase_id) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Purchase Date</td>
                            <td class="text-center"><?= (date('M-d-Y', strtotime($purchase->date))) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Supplier</td>
                            <td class="text-center"><?= $purchase->supplier_name ?></td>
                        </tr>
                        </tr>

                    </table>
                </div>
            </div>
            <div class="table-responsive" style="margin-top: 10px">
                <form action="<?php echo base_url(); ?>raw-material-return-purchase-save" method="post">
                    <table class="table table-bordered table-hover" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center">Raw material</th>
                                <th class="text-center">Color</th>
                                <th class="text-center">Pattern</th>
                                <th class="text-center">Stock Qnt</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Returned</th>
                                <th class="text-center">Return Qnt </th>
                                <!--<th class="text-center">Total </th>-->
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <?php
//                            dd($purchase_details);
                            if (!empty($purchase_details)) {
                                $i = 1;
                                foreach ($purchase_details as $key => $p) {
                                    $this->db->select('(SUM(a.in_qty)-sum(a.out_qty))  as available_quantity');
                                    $this->db->from('row_material_stock_tbl a');
                                    $this->db->where('a.row_material_id', $p->material_id);
                                    $this->db->where('a.color_id', $p->color_id);
                                    $this->db->where('a.pattern_model_id', $p->pattern_model_id);
                                    $results = $this->db->get()->row();

//                                    =========== its for already returned data =============
                                    $this->db->select('a.*, sum(a.return_qty) as returned_qty');
                                    $this->db->from('raw_material_return_details_tbl a');
                                    $this->db->where('a.return_id', $p->purchase_id);
                                    $this->db->where('a.raw_material_id', $p->material_id);
                                    $this->db->where('a.pattern_model_id', $p->pattern_model_id);
                                    $this->db->where('a.color_id', $p->color_id);
                                    $returned_data = $this->db->get()->result();
//                                    echo "<pre>";                                    print_r($returned_data);//die();
                                    ?>    
                                    <tr>
                                        <td> 
                                            <?= $p->material_name; ?>
                                            <input type="hidden" name="raw_material_id[]" value="<?= ($p->material_id) ?>">
                                        </td>
                                        <td>
                                            <?= $p->color_name; ?>
                                            <input type="hidden" name="color_id[]" value="<?= ($p->color_id) ?>">
                                        </td>
                                        <td>
                                            <?= $p->pattern_name; ?>
                                            <input type="hidden" name="pattern_model_id[]" value="<?= ($p->pattern_model_id) ?>">
                                        </td>
                                        <td class=""> 
                                            <input type="number" name="stock_quantity" value="<?= $results->available_quantity ?>" id="stock_quantity_<?php echo $i; ?>" class="form-control text-center" readonly>
                                        </td>
                                        <td class="">
                                            <input type="number" name="quantity" value="<?= $p->quantity; ?>" id="quantity_<?php echo $i; ?>"  class="form-control text-center" readonly>
                                        </td>
                                        <td class="">
                                            <input type="number" name="already_return" value="<?php if ($returned_data[0]->returned_qty) {
                                        echo $returned_data[0]->returned_qty;
                                    } else {
                                        echo '0';
                                    } ?>" id="already_return_<?php echo $i; ?>"  class="form-control text-center" readonly>
                                        </td>
                                        <td class="text-right">
                                            <?php if ($results->available_quantity == $p->quantity) { ?>
                                                <input type="number" name="return_qty[]" id="return_quantity_<?php echo $i; ?>" min="0" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" class="form-control" readonly>
        <?php } else { ?>
                                                <input type="number" name="return_qty[]" id="return_quantity_<?php echo $i; ?>" min="0" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" class="form-control">
                                    <?php } ?>
                                        </td>
                                        <!--<td class="text-right"> <?= ($p->purchase_price * $p->quantity) - $p->discount ?></td>-->
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                        <tr>
                            <td colspan="5" class="text-left"><strong>Comments</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <input type="text" name="return_comments" class="form-control">
                            </td>
                            <td>
                                <input type="hidden" name="purchase_id" value="<?= ($purchase->purchase_id) ?>">
                                <button type="submit" class="btn btn-success form-control">Confirm</button>
                            </td>
                        </tr>
                    </table> 
                </form>
            </div>
            <br/><br/>
        </div>
    </div>
</div>


<script type="text/javascript">
    function return_calculation(item) {
        var stock_quantity = parseInt($("#stock_quantity_" + item).val());
        var quantity = parseInt($("#quantity_" + item).val());
        var already_quantity = parseInt($("#already_return_" + item).val());
        var available_quantity = parseInt(quantity) - parseInt(already_quantity);
        var return_quantity = parseInt($("#return_quantity_" + item).val());
//        console.log(typeof (stock_quantity));
        if (available_quantity < return_quantity) {
            alert("Stock limited");
            $("#stock_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
            $('button[type=submit]').prop('disabled', true);
        } else if (return_quantity == '0') {
            alert("0 is not allowed");
            $("#stock_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
            $('button[type=submit]').prop('disabled', true);
        } else {
            $("#stock_quantity_" + item).css({'border': '2px solid green'});
            $("#return_quantity_" + item).css({'border': '2px solid green'});
            $('button[type=submit]').prop('disabled', false);
        }
        if (quantity < return_quantity) {
            alert("Return Quantity is not greater than quantity");
            $("#quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
            $('button[type=submit]').prop('disabled', true);
        } else {
            $("#quantity_" + item).css({'border': '2px solid green'});
            $("#return_quantity_" + item).css({'border': '2px solid green'});
            $('button[type=submit]').prop('disabled', false);
        }
    }
</script>


