<form name="frm" class="" action="<?php echo base_url('b_level/Return_controller/return_resend_comment'); ?>" method="post">
    <div class="" style="margin-bottom: 15px;">
        <label for="cancel">
            <input type="radio" name="resend_status" id="cancel" onclick="rma_code_generate(this.value)" value="2" required> 
            Cancel
        </label>
        <label for="approval">
            <input type="radio" name="resend_status" id="approval" onclick="rma_code_generate(this.value)" value="1" required> 
            Approval
        </label>
        <input type="hidden" name="rma" class="rma">
    </div>
    <div class="">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Return Qty</th>
                    <th>Replace Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //echo '<pre>';                print_r($order_details);
                $i = 0;
                foreach ($order_details as $single) {
                    $check_replace_qty = $this->db->select('a.*, sum(a.replace_qty) as replace_quantity')
                                    ->from('order_return_details a')
                                    ->where('a.return_id', $single->return_id)
                                    ->where('a.product_id', $single->product_id)
                                    ->where('a.replace_qty!=', 0)
                                    ->get()->result();
//                    echo $check_replace_qty[0]->replace_quantity; echo "<br>";
//                    echo '<pre>';                    print_r($check_replace_qty);
                    $i++;
                    ?>
                    <tr>
                        <td>
                            <?php echo $single->product_name; ?>
                            <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single->product_id; ?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="return_qty[]" id="return_qty_<?php echo $i; ?>" value="<?php echo $single->return_qty; ?>" readonly>
                        </td>
                        <td>
                            <?php if ($single->return_qty == $check_replace_qty[0]->replace_qty) { ?>
                                <input type="text" class="form-control" name="replace_qty[]" id="replace_qty_<?php echo $i; ?>" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" readonly>
                            <?php } else { ?>
                                <input type="text" class="form-control readonly_mode" name="replace_qty[]" id="replace_qty_<?php echo $i; ?>" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" >
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!--    <div class="replace_qty_div" style="margin-bottom: 10px;">
        </div>-->
    <textarea class="form-control" name="resend_comment" placeholder="Enter comments" required ></textarea>
    <input type="hidden" name="return_id" id="return_id" value="<?php echo $return_id; ?>">
    <input type="hidden" name="order_id" id="order_id"  value="<?php echo $order_id; ?>">
    <input type="hidden" name="returned_by" id="returned_by"  value="<?php echo $returned_by; ?>">
    <input type="submit" class="btn btn-info" style="margin-top: 10px;" value="Submit">
</form>

<script type="text/javascript">
    function randomCode(length = 10) {
        var chars = "6789ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDE12345";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    function rma_code_generate(t) {
        if (t == 1) {
            frm.rma.value = randomCode(frm.length.value);
            $('.readonly_mode').removeAttr('readonly');
//            $(".replace_qty_div").html('<input type="text" name="replace_qty" class="form-control" placeholder="Enter replace quantity!" required>');
        } else {
            $('.rma').val('');
            $('.readonly_mode').attr('readonly','1');
//            $(".replace_qty_div").html('');
        }
    }

    function return_calculation(item) {
        var return_qty = parseInt($("#return_qty_" + item).val());
        var replace_qty = parseInt($("#replace_qty_" + item).val());
//        console.log(typeof (stock_quantity));
//        $.ajax({
//            url: "<?php echo base_url('show-return-calculation'); ?>",
//            type: 'POST',
//            data: {'return_id': return_id, 'order_id': order_id},
//            success: function (data) {
//                console.log(data);
//                $("#resend_info").html(data);
//                $('#resend_modal_info').modal('show');
//            }
//
//        });
        if (return_qty < replace_qty) {
            alert("Replace Quantity is not greater than return quantity");
            $("#return_qty_" + item).css({'border': '2px solid red'});
            $("#replace_qty_" + item).css({'border': '2px solid red'});
            $("#replace_qty_" + item).val('');
            $('button[type=submit]').prop('disabled', true);
        } else if (replace_qty == '0') {
            alert("0 is not allowed");
            $("#return_qty_" + item).css({'border': '2px solid red'});
            $("#replace_qty_" + item).css({'border': '2px solid red'});
            $("#replace_qty_" + item).val('');
            $('button[type=submit]').prop('disabled', true);
        } else {
            $("#return_qty_" + item).css({'border': '2px solid green'});
            $("#replace_qty_" + item).css({'border': '2px solid green'});
            $('button[type=submit]').prop('disabled', false);
        }
    }
</script>