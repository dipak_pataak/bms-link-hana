<style type="text/css">
    .custom-success{display: none;}
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Suppliers Return</h5>
        </div>
        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>b-supplier-return-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="invoice_no" placeholder="Invoice No.">
                            </div>-- OR --
                            <div class="col-md-3">
                                <select class="form-control select2" name="supplier_name" data-placeholder="-- select supplier --">
                                    <option value=""></option>
                                    <?php
                                    foreach ($get_supplier as $supplier) { ?>
                                        <option value='<?php echo $supplier->supplier_id; ?>'>
                                            <?php echo $supplier->supplier_name; ?>
                                        </option>
                                   <?php  }                                    ?>
                                </select>
                            </div>
                            <!--                            -- OR --
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="material_name" placeholder="Material Name">
                            </div>-->
                            <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>

        <div class="px-3">
            <div class="alert alert-success alert-dismissible custom-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <!--<h4><i class="icon fa fa-ban"></i> Alert!</h4>-->
                <span class="alert-custom-txt row"></span>
            </div>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Purchase Invoice</th>
                        <th>Supplier Name</th>
                        <th>Material</th>
                        <th>Qty</th>
                        <th>Comment</th>
                        <th>Returned By</th>
                        <th>Approved By</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl_no = 0;
                    foreach ($get_raw_material_supplier_return as $single) {

                        $this->db->select('a.*, a.raw_material_id, b.material_name');
                        $this->db->from('raw_material_return_details_tbl a');
                        $this->db->join('row_material_tbl b', 'b.id = a.raw_material_id');
                        $this->db->where('a.return_id', $single->return_id);
                        $material_info_query = $this->db->get()->result();
//                        echo '<pre>';                        print_r($material_info_query);die();

                        $sl_no++;
                        ?>
                        <tr>
                            <td><?php echo $sl_no; ?></td>
                            <td><?php echo $single->purchase_id; ?></td>                            
                            <td><?php echo $single->supplier_name; ?></td>                            
                            <td>
                                <?php
                                $sl = 0;
                                foreach ($material_info_query as $material_single) {
                                    $sl++;
                                    echo "<p>";
                                    echo $sl . ". " . $material_single->material_name;
                                    echo "</p>";
                                }
                                ?>
                            </td>                            
                            <td>
                                <?php
                                $sl = 0;
                                foreach ($material_info_query as $material_single) {
                                    echo "<p>";
                                    echo $material_single->return_qty;
                                    echo "</p>";
                                }
                                ?>
                            </td>                            
                            <td><?php echo $single->return_comments; ?></td>            
                            <td>
                                <?php echo $single->name; ?>
                            </td>
                            <td>
                                <?php
                                if ($single->is_approved == 1) {
                                    echo $single->approve_name;
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($single->is_approved == 1) {
                                    echo "Approved";
                                } elseif ($single->is_approved == 0) {
                                    echo "Pending";
                                }
                                ?>
                            </td>
                            <td>
                                <form action="" method="post" id="return_stock_frm">
                                    <?php
                                    foreach ($material_info_query as $single_return_details) {
                                        ?>
                                        <input type="hidden" name="raw_material_id[]" value="<?php echo $single_return_details->raw_material_id; ?>" class="form-control">
                                        <input type="hidden" name="pattern_model_id[]" value="<?php echo $single_return_details->pattern_model_id; ?>" class="form-control">
                                        <input type="hidden" name="color_id[]" value="<?php echo $single_return_details->color_id; ?>" class="form-control">
                                        <input type="hidden" name="return_qty[]" value="<?php echo $single_return_details->return_qty; ?>" class="form-control">
                                    <?php } ?>
                                    <input type="hidden" name="return_id" value="<?php echo $single->return_id; ?>">
                                    <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" data-toggle="tooltip" data-placement="right" title="Approve " <?php
                                    if ($single->is_approved == 1) {
                                        echo "disabled";
                                    }
                                    ?>>
                                        <i class="fa fa-check-square" aria-hidden="true"></i>
                                    </button>
                                </form>                                
                            </td>
    <!--                            <td>
                            <?php
                            if ($single->is_approved == 0) {
                                ?>
                                                                                                <a href="<?php echo base_url(); ?>raw-material-return-approved/<?= $single->return_id ?>" onclick="return confirm('Are you sure want to approve it? ')" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="right" title="Approve "><i class="fa fa-check-square" aria-hidden="true"></i></a>
                            <?php } elseif ($single->is_approved == 1) { ?>
                                                                                                <a href="<?php echo base_url(); ?>raw-material-return-rejected/<?= $single->return_id ?>" onclick="return confirm('Are you sure want to reject it? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Rejected "><i class="fa fa-window-close" aria-hidden="true"></i></a>
                            <?php } ?>
                            </td>-->
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($get_raw_material_supplier_return)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="10" class="text-center text-danger">
                                No result found yet
                            </th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>

        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#return_approve_btn', function () {
            var frm = $("#return_stock_frm")[0];
            var form_data = new FormData(frm);
//            alert(form_data);
            $.ajax({
                url: "<?php echo base_url('raw-material-return-purchase-approved'); ?>",
//                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    return confirm("Are you sure approve it?");
                },
                success: function (data) {
                    $('.custom-success').css({'display': 'block'});
                    $('.alert-custom-txt').html("<i class='icon fa fa-warning'></i> Return Approved successfully!");
                    setTimeout(function () {
                        $('.custom-success').css({'display': 'none'});
                    }, 2000);
                    location.reload();
                }
            });
        });
    });
</script>