
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Customer Order Return</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">

            <div class="row" id="supplier_info">
                <div class="col-sm-6">
                    <table class="table-bordered">
                        <?php // dd($orders); ?>
                        <tr>
                            <td class="text-center">Order ID</td>
                            <td class="text-center"><?= ($orders->order_id) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Order Date</td>
                            <td class="text-center"><?= (date('M-d-Y', strtotime($orders->order_date))) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Client Name</td>
                            <td class="text-center"><?= $orders->customer_name ?></td>
                        </tr>
                        </tr>

                    </table>
                </div>
            </div>
            <div class="table-responsive" style="margin-top: 10px">
                <form action="<?php echo base_url(); ?>customer-order-return-save" method="post">
                    <table class="table table-bordered table-hover" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center">Product Nmae</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Returned</th>
                                <th class="text-center">Return Qnt </th>
                                <!--<th class="text-center">Total </th>-->
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <?php
                            if (!empty($order_details)) {
                                $i = 1;
                                foreach ($order_details as $key => $p) {
                                    $get_return_info = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $p->order_id)->get()->row();
                                    $return_id = @$get_return_info->return_id;
                                    $get_return_details_info = $this->db->select('a.*, sum(a.return_qty) as total_return')
                                                    ->from('order_return_details a')
                                                    ->where('a.return_id', $return_id)
                                                    ->where('a.product_id', $p->product_id)
                                                    ->get()->result();
                                    ?>    
                                    <tr>
                                        <td> 
                                            <?= $p->product_name; ?>
                                            <input type="hidden" name="product_id[]" value="<?= ($p->product_id) ?>">
                                        </td>
                                        <td class="">
                                            <input type="number" name="quantity" value="<?= $p->product_qty; ?>" id="quantity_<?php echo $i; ?>"  class="form-control text-center" readonly>
                                        </td>
                                            <td class="">
                                                <input type="number" name="already_return" value="<?php if($get_return_details_info[0]->total_return){echo $get_return_details_info[0]->total_return; }else{echo '0'; } ?>" id="already_return_<?php echo $i; ?>"  class="form-control text-center" readonly>
                                            </td>
                                        <td class="text-right">
                                            <?php if ($p->product_qty == $get_return_details_info[0]->total_return) { ?>
                                            <input type="number" name="return_qty[]" id="return_quantity_<?php echo $i; ?>" min="0" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" class="form-control text-right" readonly>
                                            <?php } else { ?>
                                                <input type="number" name="return_qty[]" id="return_quantity_<?php echo $i; ?>" min="0" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" class="form-control text-right">
                                            <?php } ?>
                                        </td>
                                        <!--<td class="text-right"> <?= ($p->purchase_price * $p->quantity) - $p->discount ?></td>-->
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                        <tr>
                            <td colspan="2" class="text-left"><strong>Comments</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="text" name="return_comments" class="form-control" required>
                            </td>
                            <td>
                                <input type="hidden" name="customer_id" value="<?= ($orders->customer_id) ?>">
                                <input type="hidden" name="order_id" value="<?= ($orders->order_id) ?>">
                                <button type="submit" class="btn btn-success form-control" style="float: right; width: 40%">Confirm</button>
                            </td>
                        </tr>
                    </table> 
                </form>
            </div>
            <br/><br/>
        </div>
    </div>
</div>


<script type="text/javascript">
    function return_calculation(item) {
        var stock_quantity = parseInt($("#stock_quantity_" + item).val());
        var quantity = parseInt($("#quantity_" + item).val());
          var already_quantity = parseInt($("#already_return_" + item).val());
                var available_quantity = parseInt(quantity)-parseInt(already_quantity);
        var return_quantity = parseInt($("#return_quantity_" + item).val());
//        console.log(typeof (stock_quantity));
        if (available_quantity < return_quantity) {
            alert("Return Quantity is not greater than stock quantity");
            $("#stock_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
//            $('button[type=submit]').prop('disabled', true);
        } else if (return_quantity == '0') {
            alert("0 is not allowed");
            $("#stock_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
//            $('button[type=submit]').prop('disabled', true);
        } else {
            $("#stock_quantity_" + item).css({'border': '2px solid green'});
            $("#return_quantity_" + item).css({'border': '2px solid green'});
//            $("#return_quantity_" + item).val('').focus();
//            $('button[type=submit]').prop('disabled', false);
        }
//        if (quantity < return_quantity) {
//            alert("Return Quantity is not greater than quantity");
//            $("#quantity_" + item).css({'border': '2px solid red'});
//            $("#return_quantity_" + item).css({'border': '2px solid red'});
//            $('button[type=submit]').prop('disabled', true);
//        } else {
//            $("#quantity_" + item).css({'border': '2px solid green'});
//            $("#return_quantity_" + item).css({'border': '2px solid green'});
//            $('button[type=submit]').prop('disabled', false);
//        }
    }
</script>


