
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Customer Return</h5>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>


        
<!--             <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>
<div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Invoice No.">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Supplier Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Product Name">
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-sm btn-success default mt-3">Filter</button>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>-->

        <div class="px-3">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Invoice No</th>
                        <th>Side Mark</th>
                        <th>Company</th>
                        <th>Comments</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    foreach ($customer_return as $single) {

                        $com = $this->db->where('user_id',$single->c_u_id)->get('company_profile')->row();
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $single->order_id; ?></td>
                            <td><?php echo $single->side_mark; ?></td>
                            <td><?php echo @$com->company_name; ?></td>
                            <td><?php echo $single->return_comments; ?></td>
                            <td><?php echo date('M-d-Y', strtotime($single->return_date)); ?></td>
                            <td>
                                <?php
                                if ($single->is_approved == 1) {
                                    echo "Resend";
                                } elseif ($single->is_approved == 0) {
                                    echo "Pending";
                                } elseif ($single->is_approved == 2) {
                                    echo "Cancel";
                                }
                                ?>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" onclick="show_return_resend('<?php echo $single->return_id; ?>', '<?php echo $single->order_id; ?>','<?php echo $single->returned_by; ?>');" data-toggle="tooltip" data-placement="right" title="Resend " <?php
//                                if ($single->is_approved == 1) {
//                                    echo "disabled";
//                                }
                                ?>>
                                    <i class="fa fa-share-square" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($customer_return)) { ?>
                    <tfoot>
                        <tr>
                            <th class="text-danger text-center" colspan="7">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
            <?php echo $links; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="resend_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Order Return Resend Information</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="resend_info">

            </div>
            <div class="modal-footer">
<!--                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    function show_return_resend(return_id, order_id, returned_by) {
        $.ajax({
            url: "<?php echo base_url('show-return-resend'); ?>",
            type: 'POST',
            data: {'return_id': return_id, 'order_id': order_id, 'returned_by' : returned_by},
            success: function (data) {
//                console.log(data);
                $("#resend_info").html(data);
                $('#resend_modal_info').modal('show');
//                r = JSON.parse(r);
//                $("#existrole ul").empty();
//                $.each(r, function (ar, typeval) {
//                    if (typeval.role_name == 'Not Found') {
//                        $("#existrole ul").html("Not Found!");
//                        $("#exitrole ul").css({'color': 'red'});
//                    } else {
//                        $("#existrole ul").append('<li>' + typeval.role_name + '</li>');
//                    }
//                });
            }

        });
//        $.post("<?php // echo base_url('b_level/Return_controller/show_return_resend');            ?>/" + id, function (t) {
//        $("#return_id").val(return_id);
//        $("#order_id").val(order_id);
//        $("#resend_info").html();
//        $('#resend_modal_info').modal('show');
//        });
    }

</script>