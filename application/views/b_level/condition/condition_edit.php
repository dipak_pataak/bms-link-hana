
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Condition Edit Information</h5>
        </div>
        <!-- end box / title -->
        <div class="m-2">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="px-3">
            <?php // print_r($condition_edit); ?>
            <form action="<?php echo base_url('condition-update/' . $condition_edit[0]->condition_id); ?>" method="post">
                <div class="form-group row">
                    <label for="category_id" class="col-sm-3 col-form-label">Category <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select name="category_id" class="form-control select2" id="category_id" data-placeholder="-- select one -- " required>
                            <option value=" ">None</option>
                            <?php
                            foreach ($get_category as $category) {
                                ?>
                                <option value="<?php echo $category->category_id; ?>" <?php
                                if ($condition_edit[0]->category_id == $category->category_id) {
                                    echo 'selected';
                                }
                                ?>>
                                            <?php echo $category->category_name; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!--                <div class="form-group row">
                                    <label for="condition" class="col-sm-3 col-form-label">Condition <i class="text-danger">*</i></label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="condition" id="condition" type="text" onmouseout="replaceString(this.value)" value="<?php echo $condition_edit[0]->condition_text; ?>" required="">
                                    </div>
                                </div>-->
                <div class="form-group row">
                    <label for="width_condition" class="col-sm-3 col-form-label">Width Condition <i class="text-danger"></i></label>
                    <div class="col-sm-3">
                        <input class="form-control" maxlength="2" name="width_condition" id="width_condition" onkeyup="width_condition_keyup(this.value)" type="text" value="<?php echo $condition_edit[0]->width_condition; ?>">
                        <span id="width_error" style="font-size: 10px; margin-top: 10px; color: Red; display: none;">Invalid character! please allows 0 to 9 and +/- </span>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control select2" name="width_fraction_id" data-placeholder="-- select one -- ">
                            <option value="0" selected="">None</option>
                            <?php foreach ($get_width_height as $width_height) { ?>
                                <option value="<?php echo $width_height->id; ?>" <?php
                                if ($condition_edit[0]->width_fraction_id == $width_height->id) {
                                    echo 'selected';
                                }
                                ?>>
                                            <?php echo $width_height->fraction_value; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div><span style="margin-top: 5px; ">Inch</span>
                </div>
                <div class="form-group row">
                    <label for="height_condition" class="col-sm-3 col-form-label">Height Condition <i class="text-danger"></i></label>
                    <div class="col-sm-3">
                        <input class="form-control" maxlength="2" name="height_condition" id="height_condition"  onkeyup="height_condition_keyup(this.value)" type="text" value="<?php echo $condition_edit[0]->height_condition; ?>">
                        <span id="height_error" style="font-size: 10px; margin-top: 10px; color: Red; display: none;">Invalid character! please allows 0 to 9 and +/- </span>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control select2" name="height_fraction_id" data-placeholder="-- select one -- ">
                            <option value="0" selected="">None</option>
                            <?php foreach ($get_width_height as $width_height) { ?>
                                <option value="<?php echo $width_height->id; ?>" <?php
                                if ($condition_edit[0]->height_fraction_id == $width_height->id) {
                                    echo 'selected';
                                }
                                ?>>
                                            <?php echo $width_height->fraction_value; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div><span style="margin-top: 5px; ">Inch</span>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-3 col-form-label">Status <i class="text-danger"></i></label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="status" id="status" data-placeholder="-- select one --">
                            <option value=""></option>
                            <option value="1" <?php
                            if ($condition_edit[0]->is_active == 1) {
                                echo 'selected';
                            }
                            ?>>Active</option>
                            <option value="0" <?php
                            if ($condition_edit[0]->is_active == 0) {
                                echo 'selected';
                            }
                            ?>>Inactive</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-9 text-right">
                        <input type="submit" id="add-shipping_method" class="btn btn-success btn-large" name="" value="Update">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="box">
        <div class="row title">
            <h4>Condition Information List</h4>
        </div>
        <table class="table table-bordered mb-3">
            <thead>
                <tr>
                    <th>Sl no.</th>
                    <th>Category</th>
                    <th>Width</th>
                    <th>Width Fraction</th>
                    <th>Height</th>
                    <th>Height Fraction</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 1;
                if (!empty($condition_list)) {
                    foreach ($condition_list as $key => $val) {
                        ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $val->category_name; ?></td>
                            <td><?= $val->width_condition; ?></td>
                            <td><?= $val->w_fraction_value; ?></td>
                            <td><?= $val->height_condition; ?></td>
                            <td><?= $val->h_fraction_value; ?></td>
                            <td><?php
                                if ($val->is_active == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>

                            <td width="100">
                                <a href="<?php echo base_url(); ?>condition-edit/<?php echo $val->condition_id; ?>" class="btn btn-success  btn-sm " ><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url(''); ?>condition-delete/<?= $val->condition_id ?>" onclick="return confirm('Are you sure want to delete it?')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
            <?php if (empty($condition_list)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="5" class="text-center">No record found!</th>
                    </tr> 
                </tfoot>
            <?php } ?>
        </table>
        <?php echo $links; ?>
    </div>

</div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    function replaceString(v) {
        $('#condition').val((v.replace(/['"]+/g, '&quot;')));
    }


//    ========= its for only number and +- check ============
    function width_condition_keyup(t) {
        var isValid = false;
        var regex = /^[0-9-+()]*$/;
        isValid = regex.test($("#width_condition").val());
//        alert(isValid);
//        $("#width_error").css("display", !isValid ? "block" : "none");
        if (isValid == true) {
            $("#width_error").css({"display": "none"});
            $('button[type=submit]').prop('disabled', false);
        } else {
            $("#width_error").css({"display": "block"});
            $('button[type=submit]').prop('disabled', true);
        }
//        return isValid;
    }
    function height_condition_keyup(t) {
        var isValid = false;
        var regex = /^[0-9-+()]*$/;
        isValid = regex.test($("#height_condition").val());
//        $("#height_error").css("display", !isValid ? "block" : "none");
//        return isValid;
        if (isValid == true) {
            $("#height_error").css({"display": "none"});
            $('button[type=submit]').prop('disabled', false);
        } else {
            $("#height_error").css({"display": "block"});
            $('button[type=submit]').prop('disabled', true);
        }
    }
</script>