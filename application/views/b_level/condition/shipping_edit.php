
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <?php
        if ($this->session->flashdata('message')) {
            echo $this->session->flashdata('message');
        }
        ?>
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Edit Shipping method</h5>
            <div class="col-sm-6 text-right float-right">
                <a href="<?php echo base_url(); ?>shipping" class="btn btn-success mt-1">Add Shipping method</a>
            </div>
        </div>
        <div class="px-3">

            <?php echo form_open('b_level/setting_controller/update_shipping_method'); ?>

            <div class="form-group row">
                <label for="method_name" class="col-sm-3 col-form-label">Shipping method name <i class="text-danger">*</i></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="method_name" id="method_name"  value="<?= $method->method_name; ?>" required="">
                </div>
            </div>
            <input type="hidden" name="id" value="<?= $method->id; ?>">



            <div class="form-group row">
                <label for="username" class="col-sm-3 col-form-label">User name <i class="text-danger">*</i></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="username"  value="<?= $method->username; ?>" id="username" required="">
                </div>
            </div>


            <div class="form-group row">
                <label for="password" class="col-sm-3 col-form-label">Password<i class="text-danger">*</i></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="password"  value="<?= $method->password; ?>" id="password" required="">
                </div>
            </div>


            <div class="form-group row">
                <label for="account_id" class="col-sm-3 col-form-label">Account id<i class="text-danger">*</i></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="account_id"  value="<?= $method->account_id; ?>" id="account_id" required="">
                </div>
            </div>



            <div class="form-group row">
                <label for="access_token" class="col-sm-3 col-form-label">Access Token<i class="text-danger">*</i></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="access_token"  value="<?= $method->access_token; ?>" id="access_token" required="">
                </div>
            </div>


            <?php
            $pickup_method = array(
                '01' => 'Daily Pickup',
                '03' => 'Customer Counter',
                '06' => 'One Time Pickup',
                '07' => 'On Call Air Pickup',
                '19' => 'Letter Center',
                '20' => 'Air Service Center'
            );
            ?>
            <div class="form-group row" style="display:none;">
                <label for="access_token" class="col-sm-3 col-form-label">Pickup Method<i class="text-danger">*</i></label>
                <div class="col-sm-6">

                    <select class="form-control select2" name="pickup_method" data-placeholder='-- select one --'>
                        <?php
                        foreach ($pickup_method as $key => $val) {
                            echo "<option value='$key' " . ($method->pickup_method == $key ? 'selected' : '') . ">$val</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="access_token" class="col-sm-3 col-form-label">Test or Production Mode<i class="text-danger">*</i></label>
                <div class="col-sm-6">

                    <select class="form-control select2" name="mode" data-placeholder='-- select one --'>
                        <?php
                        $mode = array(
                            'test' => 'Test',
                            'production' => 'Production'
                        );
                        foreach ($mode as $key => $val) {
                            echo "<option value='$key' " . ($method->mode == $key ? 'selected' : '') . ">$val</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="access_token" class="col-sm-3 col-form-label">Status<i class="text-danger">*</i></label>
                <div class="col-sm-6">

                    <select class="form-control select2" name="status" data-placeholder='-- select one --'>
                        <?php
                        $status = array(
                            '1' => 'Active',
                            '0' => 'Inactive'
                        );
                        foreach ($status as $key => $val) {
                            echo "<option value='$key' " . ($method->status == $key ? 'selected' : '') . ">$val</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-9 text-right">
                        <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-primary btn-large text-white">Back</a>
                    <input type="submit"  class="btn btn-success btn-large" value="Submit">
                </div>
            </div>

            <?php echo form_close(); ?>

        </div>

    </div>

</div>

