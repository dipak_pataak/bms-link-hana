
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Condition Information</h5>
        </div>
        <!-- end box / title -->
        <div class="m-2">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url('condition-save'); ?>" method="post">
                <div class="form-group row">
                    <label for="category_id" class="col-sm-3 col-form-label">Category <i class="text-danger">*</i></label>
                    <div class="col-sm-6">
                        <select name="category_id" class="form-control select2" id="category_id" data-placeholder="-- select one -- " required>
                            <option value=" ">None</option>
                            <?php
                            foreach ($get_category as $category) {
                                ?>
                                <option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!--                <div class="form-group row">
                                    <label for="condition" class="col-sm-3 col-form-label">Condition <i class="text-danger">*</i></label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="condition" id="condition" type="text" onmouseout="replaceString(this.value)" placeholder="Enter Condition Name!" required="">
                                    </div>
                                </div>-->
                <div class="form-group row">
                    <label for="width_condition" class="col-sm-3 col-form-label">Width Condition <i class="text-danger"></i></label>
                    <div class="col-sm-3">
                        <input class="form-control" name="width_condition" id="width_condition" onkeyup="width_condition_keyup(this.value)" type="text" value="0">
                        <span id="width_error" style="font-size: 10px; margin-top: 10px; color: Red; display: none;">Invalid types! please allows 0 to 9 and +- </span>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control select2" name="width_fraction_id" data-placeholder="-- select one -- ">
                            <option value="0" selected="">None</option>
                            <?php foreach ($get_width_height as $width_height) { ?>
                                <option value="<?php echo $width_height->id; ?>">
                                    <?php echo $width_height->fraction_value; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>Inch
                </div>
                <div class="form-group row">
                    <label for="height_condition" class="col-sm-3 col-form-label">Height Condition <i class="text-danger"></i></label>
                    <div class="col-sm-3">
                        <input class="form-control" name="height_condition" id="height_condition"  onkeyup="height_condition_keyup(this.value)" type="text" value="0">
                        <span id="height_error" style="font-size: 10px; margin-top: 10px; color: Red; display: none;">Invalid types! please allows 0 to 9 and +- </span>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control select2" name="height_fraction_id" data-placeholder="-- select one -- ">
                            <option value="0" selected="">None</option>
                            <?php foreach ($get_width_height as $width_height) { ?>
                                <option value="<?php echo $width_height->id; ?>">
                                    <?php echo $width_height->fraction_value; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>Inch
                </div>
                <div class="form-group row">
                    <div class="col-sm-9 text-right">
                        <button type="submit" id="add-shipping_method" class="btn btn-success btn-large" name="">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box">
        <div class="title">
            <h3>Condition List</h3>
        </div>
        <p class="mb-3 px-3">
            <!--            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Filter
                        </button>-->
        </p>
        <div class="collapsexxx px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>condition-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <select name="category_name" class="form-control select2 category_name" data-placeholder="-- select category --">
                                   <option value="0">None</option>
                                    <?php foreach ($get_category as $category) { ?>
                                        <option value='<?php echo $category->category_id; ?>' <?php
                                        if ($category_name == $category->category_id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                                    <?php echo $category->category_name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2 text-left">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
        <table class="table table-bordered mb-3">
            <thead>
                <tr>
                    <th>Sl no.</th>
                    <th>Category</th>
                    <th>Width</th>
                    <th>Height</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 1;
                if (!empty($get_condition_filter)) {
                    foreach ($get_condition_filter as $key => $val) {
                        ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $val->category_name; ?></td>
                            <td><?= $val->width_condition; ?></td>
                            <td><?= $val->height_condition; ?></td>
                            <td><?php
                                if ($val->is_active == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>

                            <td width="100">
                                <a href="<?php echo base_url(); ?>condition-edit/<?php echo $val->condition_id; ?>" class="btn btn-success  btn-sm " ><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url(''); ?>condition-delete/<?= $val->condition_id ?>" onclick="return confirm('Are you sure want to delete it?')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
            <?php if (empty($get_condition_filter)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="6" class="text-center text-danger">No record found!</th>
                    </tr> 
                </tfoot>
            <?php } ?>
        </table>
        <?php // echo $links;     ?>
    </div>
</div>
</div>
<!-- end content / right -->