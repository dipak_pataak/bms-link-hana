<style type="text/css">
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 260px;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<!-- content / right -->
<div id="right">

    <div class="box">
        <div class="title">
            <h3>Condition List</h3>
        </div>
        <div class="m-2">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>            
            <button type="button" class="btn btn-success  mb-1" data-toggle="modal" data-target="#conditionFrm">Add Condition</button>
        </p>


        <!-- Modal -->
        <div class="modal fade" id="conditionFrm" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Condition Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url('condition-save'); ?>" method="post">
                            <div class="form-group row">
                                <label for="category_id" class="col-sm-3 col-form-label">Category <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="category_id" class="form-control select2" id="category_id" data-placeholder="-- select one -- " required>
                                        <option value=" ">None</option>
                                        <?php
                                        foreach ($get_category as $category) {
                                            ?>
                                            <option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!--                <div class="form-group row">
                                                <label for="condition" class="col-sm-3 col-form-label">Condition <i class="text-danger">*</i></label>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="condition" id="condition" type="text" onmouseout="replaceString(this.value)" placeholder="Enter Condition Name!" required="">
                                                </div>
                                            </div>-->
                            <div class="form-group row">
                                <label for="width_condition" class="col-sm-3 col-form-label">Width Condition <i class="text-danger"></i></label>
                                <div class="col-sm-2">
                                    <input class="form-control" maxlength="2" name="width_condition" id="width_condition" onkeyup="width_condition_keyup(this.value)" type="text" value="0">
                                    <span id="width_error" style="width: 250px; font-size: 10px; margin-top: 10px; color: Red; display: none;">Invalid character! please allows 0 to 9 and +/- </span>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control select2" name="width_fraction_id" data-placeholder="-- select one -- " style="width: 100px !important;">
                                        <option value="0" selected="">None</option>
                                        <?php foreach ($get_width_height as $width_height) { ?>
                                            <option value="<?php echo $width_height->id; ?>">
                                                <?php echo $width_height->fraction_value; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-1"  style="margin-top: 5px; ">
                                    Inch
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="height_condition" class="col-sm-3 col-form-label">Height Condition <i class="text-danger"></i></label>
                                <div class="col-sm-2">
                                    <input class="form-control" maxlength="2" name="height_condition" id="height_condition"  onkeyup="height_condition_keyup(this.value)" type="text" value="0">
                                    <span  id="height_error" style="width: 250px; font-size: 10px; margin-top: 10px; color: Red; display: none;">Invalid character! please allows 0 to 9 and +/- </span>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control select2" name="height_fraction_id" data-placeholder="-- select one -- " style="width: 100px !important;">
                                        <option value="0" selected="">None</option>
                                        <?php foreach ($get_width_height as $width_height) { ?>
                                            <option value="<?php echo $width_height->id; ?>">
                                                <?php echo $width_height->fraction_value; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                                <div class="col-sm-1"  style="margin-top: 5px; ">
                                    Inch
                                </div>
                            </div>
                            <div class="form-group row text-warning col-sm-8" >
                                Note: Enter Minus (-) Sign to Deduct, Do not enter Plus (+) sign for addition. Do not enter any Text. Whatever you enter will be merged together for further calculation.
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9 text-right">
                                    <button type="submit" id="add-shipping_method" class="btn btn-success btn-large" name="">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>condition-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <select name="category_name" class="form-control select2 category_name" data-placeholder="-- select category --">
                                    <option value="0">None</option>
                                    <?php
                                    foreach ($get_category as $category) {
                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2 text-left">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
        <table class="table table-bordered mb-3">
            <thead>
                <tr>
                    <th>Sl no.</th>
                    <th>Category</th>
                    <th>Width</th>
                    <th>Width Fraction</th>
                    <th>Height</th>
                    <th>Height Fraction</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
//                echo '<pre>';                print_r($condition_list); echo '<pre>'; 
                $i = 1;
                if (!empty($condition_list)) {
                    foreach ($condition_list as $key => $val) {
                        ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td>
                                <?php
                                if ($val->condition_category_id == 0) {
                                    echo "None";
                                } 
                                echo $val->category_name;
                                ?>
                            </td>
                            <td><?= $val->width_condition; ?></td>
                            <td><?= $val->w_fraction_value; ?></td>
                            <td><?= $val->height_condition; ?></td>
                            <td><?= $val->h_fraction_value; ?></td>
                            <td><?php
                                if ($val->is_active == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>

                            <td width="100">
                                <a href="<?php echo base_url(); ?>condition-edit/<?php echo $val->condition_id; ?>" class="btn btn-success  btn-sm " data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url(''); ?>condition-delete/<?= $val->condition_id ?>" onclick="return confirm('Are you sure want to delete it?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
            <?php if (empty($condition_list)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="5" class="text-center text-danger">No record found!</th>
                    </tr> 
                </tfoot>
            <?php } ?>
        </table>
        <?php echo $links; ?>
    </div>
</div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    function replaceString(v) {

//        alert(v);
        $('#condition').val((v.replace(/['"]+/g, '&quot;')));
    }

//    ========= its for only number and +- check ============
    function width_condition_keyup(t) {
        var isValid = false;
        var regex = /^[0-9-+()]*$/;
        isValid = regex.test($("#width_condition").val());
//        alert(isValid);
//        $("#width_error").css("display", !isValid ? "block" : "none");
        if (isValid == true) {
            $("#width_error").css({"display": "none"});
            $('button[type=submit]').prop('disabled', false);
        } else {
            $("#width_error").css({"display": "block"});
            $('button[type=submit]').prop('disabled', true);
        }
//        return isValid;
    }
    function height_condition_keyup(t) {
        var isValid = false;
        var regex = /^[0-9-+()]*$/;
        isValid = regex.test($("#height_condition").val());
//        $("#height_error").css("display", !isValid ? "block" : "none");
//        return isValid;
        if (isValid == true) {
            $("#height_error").css({"display": "none"});
            $('button[type=submit]').prop('disabled', false);
        } else {
            $("#height_error").css({"display": "block"});
            $('button[type=submit]').prop('disabled', true);
        }
    }
</script>