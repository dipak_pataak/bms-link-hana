
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <?php if($this->session->flashdata('message')){
                        echo  $this->session->flashdata('message');
                    }?>

                    <!-- box / title -->
                    <div class="title row">
                        <h5>Shipping Method</h5>
                    </div>
                    <!-- end box / title -->
                    <div class="px-3">
                        <?php echo form_open('b_level/setting_controller/save_shipping_method');?>
                            
                            <div class="form-group row">
                                <label for="method_name" class="col-sm-3 col-form-label">Shipping method name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="method_name" id="method_name" placeholder="Shipping method name" required="">
                                </div>
                            </div>

                           

                            <div class="form-group row">
                                <label for="username" class="col-sm-3 col-form-label">User name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" required="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">Password<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="password" id="password" placeholder="Password" required="">
                                </div>
                            </div>

                           
                            <div class="form-group row">
                                <label for="account_id" class="col-sm-3 col-form-label">Account id<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="account_id" id="account_id" placeholder="Account ID" required="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="access_token" class="col-sm-3 col-form-label">Access Token<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="access_token" id="access_token" placeholder="Access Token" required="">
                                </div>
                            </div>

                        

                            <div class="form-group row">
                                <div class="col-sm-9 text-right">
                                    <input type="submit"  class="btn btn-success btn-large" value="Submit">
                                </div>
                            </div>

                            <?php echo form_close();?>

                    </div>


   <hr><br>
        <h3>Manage Shipping Method</h3>
        <form class="p-3">
            <table class="table table-bordered mb-3">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>Shipping method</th>
                        <th>User Name</th>
                        <th>Password</th>
                        <th>Account Id</th>
                        <th>Access token</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;
                    foreach ($lists as $key => $val) {
                    ?>
                    <tr>
                        <td><?=$i++?></td>
                        <td><?=$val->method_name?></td>
                        <td><?=$val->username?></td>
                        <td><?=$val->password?></td>
                        <td><?=$val->account_id?></td>
                        <td><?=$val->access_token?></td>
                                                        
                        <td>
                            <a href="<?php echo base_url(); ?>shipping-edit/<?=$val->id?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo base_url(); ?>shipping-method-delete/<?=$val->id?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php } ?>


                </tbody>
            </table>
            <!--<p class="text-right">
                <button type="submit" class="btn btn-success default w-md m-b-5">Save</button>
            </p>-->
        </form>

                </div>
            </div>
            <!-- end content / right -->