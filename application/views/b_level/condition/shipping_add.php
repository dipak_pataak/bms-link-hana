
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Shipping Conditions/Rules</h5>
            <!--<div class="col-sm-6 text-right">
<a href="customer_new.html" class="btn btn-success btn-sm mt-1">Add</a>
</div>-->
        </div>
        <!-- end box / title -->
        <form class="form-row px-3">
            <div class="col-lg-12 px-3">                            
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label for="product_code" class="mb-2">State</label>
                        <select name="state_name" class="form-control">
                            <option value="AL" selected="selected">Alabama</option>
                            <option value="NY">New York</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="product_code" class="mb-2">Free Shipping</label>
                        <select name="free_shipping" id="free_shipping" class="form-control">
                            <option value="" selected="selected">No</option>
                            <option value="yes">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="hidden" id="shipping_value_div">
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label for="product_code" class="mb-2">DHL Shipping Amount</label>
                            <input class="form-control" type="number" placeholder="$0.00" value="">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="product_code" class="mb-2">FedEx Shipping Amount</label>
                            <input class="form-control" type="number" placeholder="$0.00" value="">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="product_code" class="mb-2">Postal Shipping Amount</label>
                            <input class="form-control" type="number" placeholder="$0.00" value="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 px-3">
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                </div>
            </div>
        </form>
        <hr><br>
        <h3>Manage Shipping Rules</h3>
        <form class="p-3">
            <table class="table table-bordered mb-3">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>State</th>
                        <th>Free Shipping?</th>
                        <th>DHL</th>
                        <th>FedEx</th>
                        <th>Postal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Alabama</td>
                        <td>Yes</td>
                        <td>$10.00</td>
                        <td>$12.00</td>
                        <td>$5.05</td>                                    
                        <td>
                            <a href="<?php echo base_url(); ?>shipping-edit/1" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                            <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Ney York</td>
                        <td>Yes</td>
                        <td>$7.00</td>
                        <td>$2.00</td>
                        <td>$3.00</td>                                    
                        <td>
                            <a href="shipping_add.html" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                            <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!--<p class="text-right">
                <button type="submit" class="btn btn-success default w-md m-b-5">Save</button>
            </p>-->
        </form>
    </div>

</div>

<!-- end content / right -->
<script type="text/javascript">
    $("#free_shipping").change(function () {
        $("#shipping_value_div").toggle();
    });
</script>