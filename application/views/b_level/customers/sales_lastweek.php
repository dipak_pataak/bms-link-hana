
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <!-- box / title -->
                    <div class="title row">
                        <h5>Sales (Last Week)</h5>
                    </div>
                    <!-- end box / title -->
                    <div class="px-3">
                        <table class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Product Name</th>
                                    <th>Category</th>
                                    <th>Model</th>
                                    <th>Invoice No</th>
                                    <th>Date</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Refrigerator-Samsung-Samsung 218L</td>
                                    <td>Refrigerator</td>
                                    <td>Samsung 218L</td>
                                    <td>1004</td>
                                    <td>12/25/2018</td>
                                    <td>222000</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="#" onclick="return confirm('Are You Sure ? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-info btn-sm" title="Update "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Mobile-phone-Samsung-Galaxy j7</td>
                                    <td>Phone</td>
                                    <td>Samsung 54dk</td>
                                    <td>1258</td>
                                    <td>12/24/2018</td>
                                    <td>115204</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="#" onclick="return confirm('Are You Sure ? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-info btn-sm" title="Update "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Refrigerator-Samsung-Samsung 218L</td>
                                    <td>Refrigerator</td>
                                    <td>Samsung 218L</td>
                                    <td>5478</td>
                                    <td>12/23/2018</td>
                                    <td>458720</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="#" onclick="return confirm('Are You Sure ? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-info btn-sm" title="Update "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Refrigerator-Samsung-Samsung 218L</td>
                                    <td>Refrigerator</td>
                                    <td>Samsung 218L</td>
                                    <td>1004</td>
                                    <td>12/22/2018</td>
                                    <td>222000</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="#" onclick="return confirm('Are You Sure ? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-info btn-sm" title="Update "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Mobile-phone-Samsung-Galaxy j7</td>
                                    <td>Phone</td>
                                    <td>Samsung 54dk</td>
                                    <td>1258</td>
                                    <td>12/21/2018</td>
                                    <td>115204</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="#" onclick="return confirm('Are You Sure ? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-info btn-sm" title="Update "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Refrigerator-Samsung-Samsung 218L</td>
                                    <td>Refrigerator</td>
                                    <td>Samsung 218L</td>
                                    <td>5478</td>
                                    <td>12/20/2018</td>
                                    <td>458720</td>
                                    <td>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="#" onclick="return confirm('Are You Sure ? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-info btn-sm" title="Update "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end content / right -->