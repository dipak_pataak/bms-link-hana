<table class="datatable2 table table-bordered table-hover">
    <thead>
        <tr>
            <th width="5%">#</th>
            <th width="13%">Company Name</th>
            <th width="15%">Name</th>
            <th width="15%">Phone</th>
            <th width="15%">Email</th>
            <th width="22%">Address</th>
            <!--<th width="10%">Status</th>-->
            <th width="15%" class="text-center">Action</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sl = 0;
        foreach ($customer_list as $customer) {
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl; ?></td>
                <td> <a href="<?php echo base_url(); ?>b-customer-view/<?php echo $customer->customer_id; ?>"><?php echo $customer->company; ?></a></td>
                <td><?php echo $customer->first_name ." ". @$customer->last_name; ?></td>
                <!--<td><?php echo $customer->side_mark; ?></td>-->
                <td>
                    <a href="tel:<?php echo $customer->phone; ?>"><?php echo $customer->phone; ?></a>
                </td>
                <td>
                    <a href="mailto:<?php echo $customer->email; ?>"><?php echo $customer->email; ?></a>
                </td>
                <td>
                    <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                        <?php echo $customer->address . '<br>' . @$customer->city . ', ' . @$customer->state . ', ' . @$customer->zip_code . ', ' . @$customer->country_code; ?>
                    </a>
                </td>
                <td class="text-center">
                    <a href="<?php echo base_url(); ?>b-customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
                    <a href="<?php echo base_url(); ?>b-customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                    <a href="<?php echo base_url('b_level/customer_controller/delete_customer/'); ?><?php echo $customer->customer_id; ?>/<?= @$customer->customer_user_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
    <?php if (empty($customer_list)) { ?>
        <tfoot>
            <tr>
                <th colspan="8" class="text-center  text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>