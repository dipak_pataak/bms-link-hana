
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<style type="text/css">
    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 14px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
    .field-icon{
        margin-right: 45%;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<form action="#" method="post" name="customerFrm" class="p-3" id="SaveCustomer" enctype="multipart/form-data">
        <!--<form action="https://jquery-file-upload.appspot.com/<?php // echo base_url('b_level/Fileupload/index');                                           ?>" method="post" name="customerFrm" class="p-3" enctype="multipart/form-data">-->
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="first_name" class="mb-2">First name <span class="text-danger"> * </span></label>
            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John" onkeyup="required_validation()" required>
            <div class="valid-tooltip">
                Looks good!
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="last_name" class="mb-2">Last name <span class="text-danger"> * </span></label>
            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" onkeyup="required_validation()" required>
            <div class="valid-tooltip">
                Looks good!
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="email" class="mb-2">Email <span class="text-danger"> * </span></label>
            <input type="email" class="form-control" id="email" name="email" onkeyup="check_email_keyup()" placeholder="johndoe@yahoo.com" required>
            <span id="error"></span>
        </div>
        <div class="form-group col-md-6">
            <table class="" id="normalItem" style="margin-top: 10px;">
                <thead>
<!--                            <tr>
                        <th class="text-center">Phone</th>
                        <th class="text-center">Action </th>
                    </tr>-->
                </thead>
                <tbody id="addItem">
                    <tr>
                        <td>
                            <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                                <option value="">Type</option>
                                <option value="Phone">Phone</option>
                                <option value="Fax">Fax</option>
                                <option value="Mobile">Mobile</option>
                            </select>
                            <input id="phone_1" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" style="">
                        </td>
                        <td class="text-left">
                            <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                            <a style="font-size: 20px;" class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="fa fa-close"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button id="add-item-phone" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 5px 15px 15px;"><span class="fa fa-plus"></span> Add Phone</button>
            <!--                    <div class="phone-list">
                                    <div class="input-group phone-input">
                                                        <select class="form-control" >
                                            <option value="">Type</option>
                                            <option value="">Phone</option>
                                            <option value="">Fax</option>
                                            <option value="">Mobile</option>
                                        </select>
                                        <input type="text" name="" class="form-control">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="type-text">Type</span> <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a class="changeType" href="javascript:;" data-type-value="phone">Phone</a></li>
                                                <li><a class="changeType" href="javascript:;" data-type-value="fax">Fax</a></li>
                                                <li><a class="changeType" href="javascript:;" data-type-value="mobile">Mobile</a></li>
                                            </ul>
                                        </span>
                                        <input type="hidden" name="phone[1][type]" class="type-input phone" id="phone" value="" />
                                        <input type="text" name="phone[1][number]" class="form-control phone" placeholder="+1 (999) 999 9999" />
                                    </div>
                                </div>-->
                                <!--<button type="button" class="btn btn-success btn-sm btn-add-phone"><span class="fa fa-plus"></span> Add Phone</button>-->

        </div>
        <!--                <div class="form-group col-md-6">
                            <label for="phone" class="mb-2">Mobile No <span class="text-danger"> * </span></label>
                            <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX" onkeyup="required_validation()" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="work_phone" class="mb-2">Work Phone </label>
                            <input type="text" class="form-control phone" id="work_phone" name="work_phone" placeholder="+1 (XXX)-XXX-XXXX">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="home_phone" class="mb-2">Home Phone </label>
                            <input type="text" class="form-control phone" id="home_phone" name="home_phone" placeholder="+1 (XXX)-XXX-XXXX">
                        </div>-->
        <div class="form-group col-md-6">
            <label for="company" class="mb-2">Company <span class="text-danger"> * </span></label>
            <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech" onkeyup="required_validation()" required>
        </div>
        <div class="col-sm-6">
            <!--                    <div class="custom-control custom-radio custom-control-inline mt-3">
                                    <input type="radio" class="custom-control-input" id="personal" name="customer_type" checked value="personal">
                                    <label class="custom-control-label" for="personal">Personal</label>
                                </div>-->

            <!-- Default inline 2-->
            <div class="">
                <!--<input type="radio" class="custom-control-input" id="business" name="customer_type" checked value="business">-->
                <input type="hidden" class="" id="business" name="customer_type" value="business">
                <!--<label class="custom-control-label" for="business">Business</label>-->
                <label for="address" class="mb-2">Address <span class="text-danger"> * </span></label>
                <input type="text" class="form-control" id="address" name="address" placeholder="" autocomplete="off" onkeyup="required_validation()" required>
            </div>
        </div>
        <!--                <div class="form-group col-md-">
                            <label for="address" class="mb-2">Address</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="" autocomplete="off" required>
                        </div>-->
        <div class="form-group col-md-3">
            <label for="city" class="mb-2">City</label>
            <input type="text" class="form-control" id="city" name="city" placeholder="">
        </div>
        <div class="form-group col-md-3">
            <label for="state" class="mb-2">State</label>
            <input type="text" class="form-control" id="state" name="state" placeholder="">
        </div>
        <div class="form-group col-md-3">
            <label for="zip" class="mb-2">Zip</label>
            <input type="text" class="form-control" id="zip" name="zip">
        </div>
        <div class="form-group col-md-3">
            <label for="country_code" class="mb-2">Country Code</label>
            <input type="text" class="form-control" id="country_code" name="country_code">
        </div>
        <!--                <label>Zip Code:</label>
                        <div class="ui input">
                            <input id="txtZipCode1" type="text">
                        </div>-->

        <!--                <div class="form-group col-md-3">
                            <label for="file_upload_1" class="mb-2">File Upload 1</label>
                            <input class="form-control" type="file" id="file_upload_1" name="file_upload">
                            <span class="reset_pic"><i class="fa fa-times"></i></span>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="file_upload_2" class="mb-2">File Upload 2</label>
                            <input class="form-control" type="file" id="file_upload_2" name="file_upload_2">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="file_upload_3" class="mb-2">File Upload 3</label>
                            <input class="form-control" type="file" id="file_upload_3" name="file_upload_3">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="file_upload_4" class="mb-2">File Upload 4</label>
                            <input class="form-control" type="file" id="file_upload_4" name="file_upload_4">
                        </div>-->
<!--                <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>-->
    </div>
    <div class="form-row form-templates">
        <div class="form-group col-md-6">
            <label for="username" class="mb-2">Username <span class="text-danger"> * </span></label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
        </div>
        <div class="form-group col-md-6">
            <label for="password" class="mb-2">Password <span class="text-danger"> * </span></label>
            <input type="password" class="form-control col-md-7" id="password" name="password" required>
            <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
            <input type="button" class="button col-md-3 password_generate_btn btn" value="Generate" onClick="generate();" >
        </div>
    </div>
    <div class="form-group col-md-7">
        <table class="" id="normalItem_file"  style="margin-bottom: 5px;">
<!--                    <thead>
                <tr>
                    <th class="text-center">Files</th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>-->
            <tbody id="addItem_file">
                <tr id="1">
                    <td>
                        <input id="file_upload_1" class="form-control" type="file" name="file_upload[]" multiple>
                    </td>
                    <td class="text-left">
                        <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                        <a style="font-size: 20px;" class="text-danger" value="Delete" onclick="file_deleteRow(this)"><i class="fa fa-close"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
        <button id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" type="button" style="margin: 0px 15px 15px;"><span class="fa fa-plus"></span> Add File</button>
        <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
        <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
    </div> 
    <div class="form-group col-md-12" style="float: right; ">
        <button type="submit" class="btn btn-sm btn-success customer_btn" style="float: right; ">Add Customer</button>
    </div>
</form>
<script type="text/javascript">
//=========== its for phone format when  add new ==============
    $('.phone').on('keypress', function (e) {
        var key = e.charCode || e.keyCode || 0;
        var phone = $(this);
        if (phone.val().length === 0) {
            phone.val(phone.val() + '+1 (');
        }
        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {
            //alert("D");
            if (phone.val().length === 6) {

                phone.val(phone.val());
                phone = phone;
            }
            if (phone.val().length === 7) {
                phone.val(phone.val() + ') ');
            }
            if (phone.val().length === 12) {
                phone.val(phone.val() + '-');
            }
            if (phone.val().length >= 17) {
                phone.val(phone.val().slice(0, 16));
            }
        }
        // Allow numeric (and tab, backspace, delete) keys only
        return (key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    })
            .on('focus', function () {
                phone = $(this);

                if (phone.val().length === 0) {

                    phone.val('+1 (');
                } else {
                    var val = phone.val();
                    phone.val('').val(val); // Ensure cursor remains at the end
                }
            })

            .on('blur', function () {
                $phone = $(this);

                if ($phone.val() === '(') {
                    $phone.val('');
                }
            });


    // submit form and add data
    $("#SaveCustomer").on('submit', function (e) {
        e.preventDefault();
        var submit_url = "b_level/order_controller/customer_save";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {
                var data = JSON.parse(res);
                $('#customer_id').append('<option value = "' + data.customer_id + '"  selected> ' + data.first_name + ' ' + data.last_name + ' </option>');
                $("#side_mark").val(data.side_mark);
                $('#new_customer_modal_info').modal('hide');
            }, error: function () {
                alert('error');
            }
        });
    });

//    ============ its for show password ===============
//    $(".toggle-password").click(function () {
    $("body").on('click', '.toggle-password', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
//    ============ its for generate password ============
    function randomPassword(length = 6) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    function generate() {
        customerFrm.password.value = randomPassword(customerFrm.length.value);
        $("#password").focus();
    }

//    ============ close generate password =============

//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        $("#username").val(email);
        var email = encodeURIComponent(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "get-check-customer-unique-email",
            type: "post",
            data: data_string,
            success: function (s) {
                if (s != 0) {
//                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
//                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }

//    ============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {
        if ($('#business').is(":checked"))
        {
            if ($('#username').val() == '') {
                $('#username').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#username').css({'border': '2px; solid green'});
            }
            if ($('#password').val() == '') {
                $('#password').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#password').css({'border': '2px; solid green'});
            }
        }

    });
//        ========== some field validation ============   
//    $('button[type=submit]').prop('disabled', true);
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '' && $("#company").val() != '' && $("#address").val() != '') {
//            $("#first_name").css({'border': '1px solid red'}).focus();
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }
</script>

<script type="text/javascript">
//    var count = 2;
//    limits = 4;
//    ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            alert("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count, e = document.createElement("tr");
            e.innerHTML = "\n\
                                     <td class='text-center'><select id='phone_type_" + count + "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value=''>Type</option><option value='Phone'>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" + count + "' class='phone form-control phone_no_type current_row' type='text' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                    <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='fa fa-close'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    count++;
//=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function (e) {
                var key = e.charCode || e.keyCode || 0;
                var phone = $(this);
                if (phone.val().length === 0) {
                    phone.val(phone.val() + '(');
                }
                // Auto-format- do not expose the mask as the user begins to type
                if (key !== 8 && key !== 9) {
                    //alert("D");
                    if (phone.val().length === 6) {

                        phone.val(phone.val());
                        phone = phone;
                    }
                    if (phone.val().length === 7) {
                        phone.val(phone.val() + ') ');
                    }
                    if (phone.val().length === 12) {
                        phone.val(phone.val() + '-');
                    }
                    if (phone.val().length >= 17) {
                        phone.val(phone.val().slice(0, 16));
                    }
                }
                // Allow numeric (and tab, backspace, delete) keys only
                return (key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
            })
                    .on('focus', function () {
                        phone = $(this);

                        if (phone.val().length === 0) {

                            phone.val('+1 (');
                        } else {
                            var val = phone.val();
                            phone.val('').val(val); // Ensure cursor remains at the end
                        }
                    })

                    .on('blur', function () {
                        $phone = $(this);

                        if ($phone.val() === '(') {
                            $phone.val('');
                        }
                    });

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
        }
    }
//    ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);

            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function () {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });

            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function () {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }
//    ======== close ===========
//========== its for customer multiple file upload ==============
    function addInputFile(t) {
        var table_row_count = $("#normalItem_file > tbody > tr").length;
        var file_count = table_row_count + 1;
        file_limits = 5;
        if (file_count == file_limits) {
//            alert("You have reached the limit of adding " + file_count + " files");
            alert("You have reached the limit of adding 4 files");
        } else {

//            alert(table_row_count);
            var a = "file_upload_" + file_count, e = document.createElement("tr");
            e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                    <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='fa fa-close'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    file_count++;
        }
    }
    function file_deleteRow(t) {
        var a = $("#normalItem_file > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_file_count = 1;
            $("#normalItem_file > tbody > tr td input.current_file_count").each(function () {
                current_file_count++;
                $(this).attr('class', 'form-control');
                $(this).attr('id', 'file_upload_' + current_file_count);
                $(this).attr('name', 'file_upload_' + current_file_count);
            });

        }
    }
    var file_count = 2,
            file_limits = 5;
//    ======== close multiple file uploadd add row===========
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            showOn: "focus",
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $(document.body).on('click', '.changeType', function () {
            $(this).closest('.phone-input').find('.type-text').text($(this).text());
            $(this).closest('.phone-input').find('.type-input').val($(this).data('type-value'));
        });

        $(document.body).on('click', '.btn-remove-phone', function () {
            $(this).closest('.phone-input').remove();
        });


        $('.btn-add-phone').click(function () {

            var index = $('.phone-input').length + 1;

            $('.phone-list').append('' +
                    '<div class="input-group phone-input">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="type-text">Type</span> <span class="caret"></span></button>' +
                    '<ul class="dropdown-menu" role="menu">' +
                    '<li><a class="changeType" href="javascript:;" data-type-value="phone">Phone</a></li>' +
                    '<li><a class="changeType" href="javascript:;" data-type-value="fax">Fax</a></li>' +
                    '<li><a class="changeType" href="javascript:;" data-type-value="mobile">Mobile</a></li>' +
                    '</ul>' +
                    '</span>' +
                    '<input type="text" name="phone[' + index + '][number]" class="form-control" placeholder="+1 (999) 999 9999" />' +
                    '<input type="hidden" name="phone[' + index + '][type]" class="type-input" value="" />' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-danger btn-remove-phone" type="button"><span class="fa fa-times"></span></button>' +
                    '</span>' +
                    '</div>'
                    );
        });
    });


////============ its for google place api ================
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);

                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });


    });

</script>