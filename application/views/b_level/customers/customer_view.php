<style type="text/css">
    .right_side{
        float: right;
        font-weight: bold;
    }
    .left_side{
        float: left;
        font-weight: bold;
    }
    a:hover{text-decoration: none;}
    .address{cursor: pointer;}
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Customer View</h5>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="row m-0">
            <div class="col-sm-8 px-3 mb-3">
                <h6 class="mx-0">Customer Info: <a href="<?php echo base_url(); ?>b-customer-edit/<?php echo $customer_view[0]['customer_id']; ?>" class="btn btn-sm btn-success" title="Edit Customer" style="font-size: 10px;">Edit</a></h6>

                <p>
                    <b>Company Name</b> : <?php
                    echo $company = $customer_view[0]['company'];
//                    $cn = strtoupper(substr($company, 0, 3));
//                    $cid = $customer_view[0]['customer_id'];
//                    if (strlen($cid) == 1) {
//                        $cid = "00" . $cid;
//                    } elseif (strlen($cid) == 2) {
//                        $cid = "0" . $cid;
//                    }
                    ?>  <br>
                    <b>Customer Name</b> : <?php echo $customer_view[0]['full_name']; ?>  <br>
                    <b>Customer ID</b>: <?php echo $customer_view[0]['company_customer_id']; //$cn . "-" . $cid;      ?>  <br>
                    <b>Address</b> : <a href="javascript:void(0)" id="address_<?php echo $customer_view[0]['customer_id']; ?>" class="address" onclick="show_address_map(<?php echo $customer_view[0]['customer_id']; ?>);">
                        <?php echo @$customer_view[0]['address'] . ", " . @$customer_view[0]['city'] . ", " . @$customer_view[0]['state'] . ", " . @$customer_view[0]['zip_code'] . ", " . @$customer_view[0]['country_code']; ?> </a> <br>
                    <b>Email</b> :  <a href="mailto:<?php echo $customer_view[0]['email']; ?>"><?php echo $customer_view[0]['email']; ?> </a> <br>
                    <?php foreach ($get_customer_phones as $phones) { ?>
                        <b><?php echo $phones->phone_type; ?></b> : 
                        <a href="tel:<?php echo $phones->phone; ?>"><?php echo $phones->phone; ?> </a> <br>
                    <?php } ?>

                    <?php // } ?>
                    <!--                    <b>Status</b>: <?php echo $customer_view[0]['now_status']; ?> -->
                </p>
                <div style="margin-top: 10px;">
                    <strong>Uploaded Files</strong>
                    <?php
                    $this->db->select('*');
                    $this->db->from('customer_file_tbl');
                    $this->db->where('customer_id', $customer_view[0]['customer_id']);
                    $customer_uploded_files = $this->db->get()->result();
                    foreach ($customer_uploded_files as $upload_file) {
                        ?>
                        <a href="<?php echo base_url(); ?>assets/b_level/uploads/customers/<?php echo $upload_file->file_upload; ?>" target="_new">
                            <i class="fa fa-folder-open" style="padding: 5px; font-size: 20px;" title="<?php echo $upload_file->file_upload; ?>"></i>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-4">
                <?php
                // echo '<pre>'; print_r($customer_view); echo '</pre>';
                if ($customer_view[0]['customer_type'] == 'business') {
                    if ($customer_view[0]['logo']) {
                        ?>
                        <img src="<?php echo base_url(); ?>assets/c_level/uploads/appsettings/<?php echo $customer_view[0]['logo']; ?>" style="width: 30%;">
                        <?php
                    }
                }
                ?>
            </div>
        </div>

        <div class="px-3">
            <div class="table-responsive">
                <table class="table table-bordered table-hover m-0">
                    <thead>
                        <tr>
                            <th>Order/Quote No</th>
                            <th>Order details (Name of product include specifications)</th>
                            <th>Price</th>
                            <!--<th>Status</th>-->
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        if ($orderd != NULL)
                            foreach ($orderd as $key => $value) {
                                $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
            b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
                                                ->from('b_level_qutation_details')
                                                ->join('product_tbl', 'product_tbl.product_id=b_level_qutation_details.product_id', 'left')
                                                ->join('category_tbl', 'category_tbl.category_id=b_level_qutation_details.category_id', 'left')
                                                ->join('b_level_quatation_attributes', 'b_level_quatation_attributes.fk_od_id=b_level_qutation_details.row_id', 'left')
                                                ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id', 'left')
                                                ->join('color_tbl', 'color_tbl.id=b_level_qutation_details.color_id', 'left')
                                                ->where('b_level_qutation_details.order_id', $value->order_id)
                                                ->get()->result();
                                ?>

                                <tr>
                                    <td>
                                        <a href="<?php echo base_url('b_level/invoice_receipt/receipt/'); ?><?= $value->order_id ?>" target="_new">
                                            <?= $value->order_id ?>
                                        </a>
                                    </td>
                                    <td><?php
                                        $products = '';
                                        foreach ($query as $key => $val) {
                                            $products .= $val->product_name . ', ';
                                        }
                                        echo rtrim($products, ', ');
                                        ?></td>

                                    <td><?= $currency ?><?= $value->grand_total ?></td>
<!--                                    <td>
                                        <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                            <option value="">--Select--</option>
                                            <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                            <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                            <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                            <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                            <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                        </select>
                                    </td>-->

                                                                                                                                                                                                                                                                                                               <!--  <td>
                                                                                                                                                                                                                                                                                                                    <a href="<?php echo base_url(); ?>add-b-customer" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                                                                                                                                                                                                                                                                                                    <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                                                                                                                                                                                                                                                                                                </td> -->
                                </tr>
                            <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="separator mb-3"></div>

        <!--        <div class="px-3">
                    <h6 class="mx-0">Remarks Info:</h6>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover m-0">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Appointment date</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
        
                            <tbody>
        <?php
        if ($remarks != NULL) {
            $i = 1;
            foreach ($remarks as $key => $value) {
                ?>
                                        
                                                                        <tr>
                                                                            <td><?= $i++ ?></td>
                                                                            <td><?= date('M-d-Y', strtotime($value->appointment_date)); ?></td>
                                                                            <td><?= $value->remarks ?></td>
                                                                        </tr>
                <?php
            }
        }
        ?>
                            </tbody>
                        </table>
                    </div>
                </div>-->
        <?php if ($customer_view[0]['customer_type'] == 'business') { ?>
            <div class="px-3">
                <h6 class="mx-0">Comments Information:</h6>
                <div class="table-responsive"  style="height: 180px;    overflow-y: scroll;">
                    <table class="table table-bordered table-striped m-0">
                        <thead>
                            <tr>
                                <th width="10%">SL</th>
                                <th width="40%">My Message</th>
                                <th  width="40%" class="text-right">Client Message</th>
                                <th width="10%" class="text-center">Date </th>
                            </tr>
                        </thead>
                        <tbody style="height: 10px !important; overflow: scroll;" >
                            <?php

                            if ($this->session->userdata('isAdmin') == 1) {
                    $level_id = $this->session->userdata('user_id');
                } else {
                    $level_id = $this->session->userdata('admin_created_by');
                }
                
                            $customer_user_id = $customer_view[0]['customer_user_id'];
                            $user_id = $this->session->userdata('user_id');
                            
 //                            $sql = "SELECT comment_from, comment_to, comments, status, created_at FROM customer_commet_tbl 
	// WHERE (comment_from = '$customer_user_id' AND comment_to = '$level_id') 
 //                    or (comment_from = '$user_id' AND comment_to = '$customer_user_id')  ORDER BY id DESC";
 //                            $customer_commet_results = $this->db->query($sql)->result();

                    $sql = "SELECT comment_from, comment_to, comments, status, created_at FROM customer_commet_tbl 
                    WHERE (comment_from = '$customer_user_id') 
                    or (comment_to = '$customer_user_id')  ORDER BY id DESC";
                    $customer_commet_results = $this->db->query($sql)->result();


                        
                            $customer_commet_results = $this->db->query($sql)->result();

                            if ($customer_commet_results != NULL) {
                                $i = 1;
                                foreach ($customer_commet_results as $key => $value) {
                                    ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td>
                                            <?php
                                            if ($value->status == 'IN') {
                                                echo "<span class='left_side'>$value->comments</span>";
                                            }
//                                            echo $single->comments;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($value->status == 'OUT') {
                                                echo "<span class='right_side'>$value->comments</span>";
                                            }
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
//                                            echo $value->created_at;
                                            $strtotime = strtotime($value->created_at);
                                            echo date('M-d-Y', $strtotime);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="px-3">
                <form action="<?php echo base_url('send-customer-comment'); ?>" method="post">
                    <div class="form-group row mt-3">
                        <!--<label for="comment" class="col-xs-3 col-form-label">Comments <span class="text-danger"> * </span></label>-->
                        <div class="col-xs-12">
                            <textarea name="comment" class="form-control" placeholder="Write Your Comments" rows="7"  tabindex="1" required></textarea>
                        </div>
                    </div>

                    <div class="form-group  text-right">
                        <input type="hidden" name="customer_id" value="<?php echo $customer_view[0]['customer_id']; ?>">
                        <input type="hidden" name="customer_user_id" value="<?php echo $customer_view[0]['customer_user_id']; ?>">
                        <button type="submit" class="btn btn-success w-md m-b-5" tabindex="2">Send</button>
                        <button type="reset" class="btn btn-primary w-md m-b-5"  tabindex="3">Reset</button>
                    </div>
                </form>
            </div>
        <?php } ?>
    </div>
</div>
<!-- end content / right -->


<div class="modal fade" id="customer_address_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Category Information</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="customer_address_info">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    function setOrderStage(stage_id, order_id) {
        $.ajax({
            url: "<?php echo base_url('b_level/order_controller/set_order_stage') ?>/" + stage_id + "/" + order_id,
            type: 'GET',
            success: function (r) {

                toastr.success('Success! - Order Stage Set Successfully');
                setTimeout(function () {
                    window.location.href = window.location.href;
                }, 2000);
            }
        });
    }


//============== its for show_address_map ==========
    function show_address_map(id) {
//    alert(id);
        var address = $("#address_" + id).text();
        var location = $.trim(address)
        $.post("<?php echo base_url(); ?>show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }


</script>