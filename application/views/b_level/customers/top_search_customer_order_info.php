<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Customer List</h5>
        </div>
        <table class="datatable2 table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="13%">Company Name</th>
                    <th width="15%">Name</th>
                    <th width="15%">Phone</th>
                    <th width="15%">Email</th>
                    <th width="22%">Address</th>
                    <!--<th width="10%">Status</th>-->
                    <th width="15%" class="text-center">Action</th>
                </tr>
            </thead>

            <tbody>
                <?php
                $sl = 0;
                foreach ($get_top_search_customer_info as $customer) {
                    $sl++;
                    ?>
                    <tr>
                        <td><?php echo $sl; ?></td>
                        <td>
                            <a href="<?php echo base_url(); ?>b-customer-view/<?php echo $customer->customer_id; ?>">
                                <?php echo $customer->company; ?>
                            </a>
                        </td>
                        <td><?php echo $customer->first_name . " " . @$customer->last_name; ?></td>
                        <td>
                            <a href="tel:<?php echo $customer->phone; ?>"><?php echo $customer->phone; ?></a>
                        </td>
                        <td>
                            <a href="mailto:<?php echo $customer->email; ?>"><?php echo $customer->email; ?></a>
                        </td>
                        <td>
                            <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                                <?php echo $customer->address . '<br>' . @$customer->city . ', ' . @$customer->state . ', ' . @$customer->zip_code . ', ' . @$customer->country_code; ?>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="<?php echo base_url(); ?>b-customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
                            <a href="<?php echo base_url(); ?>b-customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo base_url('b_level/customer_controller/delete_customer/'); ?><?php echo $customer->customer_id; ?>/<?= @$customer->customer_user_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            <?php if (empty($get_top_search_customer_info)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="8" class="text-center  text-danger">No record found!</th>
                    </tr> 
                </tfoot>
            <?php } ?>
        </table>

    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Order List</h5>
        </div>
        <table class="table table-bordered table-hover text-center">
            <thead>
                <tr>
                    <th>Order/Quote No</th>
                    <th>Client Name </th>
                    <th>Side mark</th>
                    <th>Order date</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                if (!empty($get_top_search_order_info)) {
                    foreach ($get_top_search_order_info as $key => $value) {

                        $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                        $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                        ?>
                        <tr>
                            <td><?= $value->order_id; ?></td>
                            <td><?= $value->customer_name; ?></td>
                            <td><?= $value->side_mark; ?></td>
                            <td><?= (@$value->order_date); ?></td>
                            <td><?= $company_profile[0]->currency ?><?= $value->grand_total ?></td>

                            <td>
                                <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                    <option value="">--Select--</option>
                                    <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                    <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                    <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                    <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                    <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                </select>
                            </td>

                            <td class="width_140">
                                <!-- <a href="<?= base_url('order-view/') . $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a> -->
                                <a href="<?= base_url('b_level/invoice_receipt/receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a> 
                                <!-- <a href="order-edit/<?= $value->order_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a> -->
                                <a href="delete-order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                            </td>

                        </tr>

                        <?php
                    }
                }
                ?>
            </tbody>
            <?php if (empty($get_top_search_order_info)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="8" class="text-center text-danger">
                            Record not found!
                        </th>
                    </tr>
                </tfoot>
            <?php } ?>
        </table>
    </div>
</div>
    <div class="modal fade" id="customer_address_modal_info" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Customer Address Map Show</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="customer_address_info">

                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>

<script type="text/javascript">
//============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)
//        alert(location);
        $.post("<?php echo base_url(); ?>show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }
</script>