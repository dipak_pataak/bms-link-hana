<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<style type="text/css">
    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 115px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 14px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Edit Customer  </h5>
        </div>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <?php


//        echo '<pre>'; print_r($customer_edit); 
        $customer_no = explode('-', $customer_edit[0]['customer_no']);
//        dd($customer_no);
        ?>
        <!-- end box / title -->
        <form action="<?php echo base_url('b_level/Customer_controller/customer_update/' . $customer_edit[0]['customer_id']); ?>" method="post" name="customerFrm" class="p-3" enctype="multipart/form-data">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="first_name" class="mb-2">First name <span class="text-danger"> * </span></label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John" value="<?php echo $customer_edit[0]['first_name']; ?>" required>
                    <input type="hidden" class="form-control" id="customer_no" name="customer_no" value="<?php echo @$customer_no[0] . "-" . @$customer_no[1]; ?>">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="last_name" class="mb-2">Last name <span class="text-danger"> * </span></label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" value="<?php echo $customer_edit[0]['last_name']; ?>" required>
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label for="email" class="mb-2">Email <span class="text-danger"> * </span></label>
                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $customer_edit[0]['email']; ?>" placeholder="johndoe@yahoo.com" readonly required>
                </div>

                <div class="form-group col-md-6">
                    <table class="" id="normalItem" style="margin-top: 10px;">
                        <thead>
<!--                            <tr>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Action </th>
                            </tr>-->
                        </thead>
                        <tbody id="addItem">
                            <?php
                            $this->db->select('*');
                            $this->db->from('customer_phone_type_tbl');
                            $this->db->where('customer_id', $customer_edit[0]['customer_id']);
                            $customer_phone_types = $this->db->get()->result();
                            $j = 0;
                            foreach ($customer_phone_types as $phone_type) {
                                $j++;
                                ?>
                                <tr>
                                    <td>
                                        <select class="form-control phone_type_select" id="phone_type_<?php echo $j; ?>" name="phone_type[]" required>
                                            <option value="">Type</option>
                                            <option value="Phone" <?php
                                            if ($phone_type->phone_type == 'Phone') {
                                                echo 'selected';
                                            }
                                            ?>>Phone</option>
                                            <option value="Fax" <?php
                                            if ($phone_type->phone_type == 'Fax') {
                                                echo 'selected';
                                            }
                                            ?>>Fax</option>
                                            <option value="Mobile" <?php
                                            if ($phone_type->phone_type == 'Mobile') {
                                                echo 'selected';
                                            }
                                            ?>>Mobile</option>
                                        </select>
                                        <input id="phone_<?php echo $j; ?>" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" value="<?php echo $phone_type->phone ?>">
                                    </td>
                                    <td class="text-left">
                                        <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                        <a style="font-size: 20px;" class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
<!--                            <tr>
                <td>
                    <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                        <option value="">Type</option>
                        <option value="Phone">Phone</option>
                        <option value="Fax">Fax</option>
                        <option value="Mobile">Mobile</option>
                    </select>
                    <input id="phone_1" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX)-XXX-XXXX" style="">
                </td>
                <td class="text-left">
                    <input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">
                    <a style="font-size: 20px;" class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="fa fa-close"></i></a>
                </td>
            </tr>-->
                        </tbody>
                    </table>
                    <button id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 5px 15px 15px;"><span class="fa fa-plus"></span> Add Phone</button>
                </div>
                <div class="form-group col-md-6">
                    <label for="company" class="mb-2">Company <span class="text-danger"> * </span></label>
                    <input type="text" class="form-control" id="company" name="company" value="<?php echo $customer_edit[0]['company']; ?>" placeholder="ABC Tech" required>
                </div>
                <div class="col-sm-6">
                    <!--                    <div class="custom-control custom-radio custom-control-inline mt-3">
                                            <input type="radio" class="custom-control-input" id="personal" name="customer_type" value="personal" <?php
//                    if ($customer_edit[0]['customer_type'] == 'personal') {
//                        echo 'checked';
//                    }
                    ?>>
                                            <label class="custom-control-label" for="personal">Personal</label>
                                        </div>-->

                    <!-- Default inline 2-->
                    <div class="">
                        <input type="hidden" class="" id="business" name="customer_type" value="business" <?php
//                        if ($customer_edit[0]['customer_type'] == 'business') {
//                            echo 'checked';
//                        }
                        ?>>
                        <!--<label class="custom-control-label" for="business">Business</label>-->
                        <label for="address" class="mb-2">Address <span class="text-danger"> * </span></label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $customer_edit[0]['address']; ?>" placeholder="" required>
                    </div>
                </div>
                <!--                <div class="form-group col-md-12">
                                    <label for="address" class="mb-2">Address</label>
                                    <input type="text" class="form-control" id="address" name="address" value="<?php echo $customer_edit[0]['address']; ?>" placeholder="" required>
                                </div>-->
                <div class="form-group col-md-3">
                    <label for="city" class="mb-2">City</label>
                    <input type="text" class="form-control" id="city" name="city" value="<?php echo $customer_edit[0]['city']; ?>" placeholder="">
                </div>
                <div class="form-group col-md-3">
                    <label for="state" class="mb-2">State</label>
                    <input type="text" class="form-control" id="state" name="state" value="<?php echo $customer_edit[0]['state']; ?>" placeholder="">
                </div>
                <div class="form-group col-md-3">
                    <label for="zip" class="mb-2">Zip</label>
                    <input type="text" class="form-control" id="zip" name="zip" value="<?php echo $customer_edit[0]['zip_code']; ?>">
                </div>
                <div class="form-group col-md-3">
                    <label for="country_code" class="mb-2">Country Code</label>
                    <input type="text" class="form-control" id="country_code" name="country_code" value="<?php echo $customer_edit[0]['country_code']; ?>">
                </div>


                <!--                <div class="form-group col-md-3">
                                    <label for="file_upload" class="mb-2">File Upload 1</label>
                                    <input class="form-control" type="file" id="file_upload" name="file_upload">
                                    <input class="form-control" type="hidden" id="file_upload_hdn" name="file_upload_hdn" value="<?php echo $customer_edit[0]['file_upload']; ?>">
                <?php if ($customer_edit[0]['file_upload']) { ?>
                                                                                                                            <div style="margin-top: 5px;">
                                                                                                                                <a href="<?php echo base_url(); ?>assets/b_level/uploads/customers/<?php echo $customer_edit[0]['file_upload']; ?>">
                                                                                                                                    <i class="fa fa-folder-open" style="padding: 5px; font-size: 20px;" title="Show"></i>
                                                                                                                                </a>
                                                                                                                                <a href="<?php echo base_url(); ?>b-customer-file-delete/<?php echo $customer_edit[0]['customer_id'] . "/" . $customer_edit[0]['file_upload']; ?>" onclick="return confirm('Do you want to delete it?')">
                                                                                                                                    <i class="fa fa-times btn-danger" style="padding: 1px; font-size: 15px;" title="Delete"></i>
                                                                                                                                </a>
                                                                                                                            </div>
                <?php } ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="file_upload_2" class="mb-2">File Upload 2</label>
                                    <input class="form-control" type="file" id="file_upload_2" name="file_upload_2">
                                    <input class="form-control" type="hidden" id="file_upload_2_hdn" name="file_upload_2_hdn" value="<?php echo $customer_edit[0]['file_upload_2']; ?>">
                <?php if ($customer_edit[0]['file_upload_2']) { ?>
                                                                                                                            <div style="margin-top: 5px;">
                                                                                                                                <a href="<?php echo base_url(); ?>assets/b_level/uploads/customers/<?php echo $customer_edit[0]['file_upload_2']; ?>">
                                                                                                                                    <i class="fa fa-folder-open" style="padding: 5px; font-size: 20px;" title="Show"></i>
                                                                                                                                </a>
                                                                                                                                <a href="<?php echo base_url(); ?>b-customer-file-delete/<?php echo $customer_edit[0]['customer_id'] . "/" . $customer_edit[0]['file_upload_2']; ?>" onclick="return confirm('Do you want to delete it?')">
                                                                                                                                    <i class="fa fa-times btn-danger" style="padding: 1px; font-size: 15px;" title="Delete"></i>
                                                                                                                                </a>
                                                                                                                            </div>
                <?php } ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="file_upload_3" class="mb-2">File Upload 3</label>
                                    <input class="form-control" type="file" id="file_upload" name="file_upload_3">
                                    <input class="form-control" type="hidden" id="file_upload_3_hdn" name="file_upload_3_hdn" value="<?php echo $customer_edit[0]['file_upload_3']; ?>">
                <?php if ($customer_edit[0]['file_upload_3']) { ?>
                                                                                                                            <div style="margin-top: 5px;">
                                                                                                                                <a href="<?php echo base_url(); ?>assets/b_level/uploads/customers/<?php echo $customer_edit[0]['file_upload_3']; ?>">
                                                                                                                                    <i class="fa fa-folder-open" style="padding: 5px; font-size: 20px;" title="Show"></i>
                                                                                                                                </a>
                                                                                                                                <a href="<?php echo base_url(); ?>b-customer-file-delete/<?php echo $customer_edit[0]['customer_id'] . "/" . $customer_edit[0]['file_upload_3']; ?>" onclick="return confirm('Do you want to delete it?')">
                                                                                                                                    <i class="fa fa-times btn-danger" style="padding: 1px; font-size: 15px;" title="Delete"></i>
                                                                                                                                </a>
                                                                                                                            </div>
                <?php } ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="file_upload_4" class="mb-2">File Upload 4</label>
                                    <input class="form-control" type="file" id="file_upload_4" name="file_upload_4">
                                    <input class="form-control" type="hidden" id="file_upload_4_hdn" name="file_upload_4_hdn" value="<?php echo $customer_edit[0]['file_upload_4']; ?>">
                <?php if ($customer_edit[0]['file_upload_4']) { ?>
                                                                                                                            <div style="margin-top: 5px;">
                                                                                                                                <a href="<?php echo base_url(); ?>assets/b_level/uploads/customers/<?php echo $customer_edit[0]['file_upload_4']; ?>">
                                                                                                                                    <i class="fa fa-folder-open" style="padding: 5px; font-size: 20px;" title="Show"></i>
                                                                                                                                </a>
                                                                                                                                <a href="<?php echo base_url(); ?>b-customer-file-delete/<?php echo $customer_edit[0]['customer_id'] . "/" . $customer_edit[0]['file_upload_4']; ?>" onclick="return confirm('Do you want to delete it?')">
                                                                                                                                    <i class="fa fa-times btn-danger" style="padding: 1px; font-size: 15px;" title="Delete"></i>
                                                                                                                                </a>
                                                                                                                            </div>
                <?php } ?>
                                </div>
                                <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>-->
            </div>
            <div id="user_pass_div" class="form-row <?php
            if ($customer_edit[0]['customer_type'] != 'business') {
                echo 'form-template';
            }
            ?> ">
                <div class="form-group col-md-6 hidden">
                    <label for="username" class="mb-2">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $customer_edit[0]['email']; ?>" required placeholder="Username">
                </div>

                <div class="form-group col-md-6 hidden" >
                    <label for="password" class="mb-2">Password</label>
                    <input type="password" class="form-control col-md-8" id="password" name="password">
                    <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
                    <input type="button" class="button col-md-2 password_generate_btn btn" value="Generate" onClick="generate();" >
                </div>
            </div>
            <div class="form-group col-md-6">
                <table class="" id="normalItem_file"  style="margin-bottom: 5px;">
<!--                    <thead>
                        <tr>
                            <th class="text-center">Files</th>
                            <th class="text-center">Action </th>
                        </tr>
                    </thead>-->
                    <tbody id="addItem_file">
                        <?php
                        $this->db->select('*');
                        $this->db->from('customer_file_tbl');
                        $this->db->where('customer_id', $customer_edit[0]['customer_id']);
                        $customer_uploded_files = $this->db->get()->result();

                        $i = 0;

                        foreach ($customer_uploded_files as $upload_file) {
                            $exp = explode('.', $upload_file->file_upload);
                            $extent    = end($exp);
                            ?>
                            <tr>
                                <td class="text-left">
                                    <?php if($extent=='png' || $extent=='jpg' || $extent=='jpeg'){?>
                                    <img src="<?php echo base_url(); ?>assets/b_level/uploads/customers/<?php echo $upload_file->file_upload; ?>" style="width: 30%">
                                    <?php } ?>
                                    <p style="margin-top: 10px;">
                                        <?php echo $upload_file->file_upload; ?>
                                    </p>
                                </td>
                                <td>
                                    <a href="<?php echo base_url(); ?>b-customer-file-delete/<?php echo $upload_file->id . "/" . $upload_file->customer_id; ?>" class="text-danger" style="font-size: 20px;" onclick="return confirm('Do you want to delete it?')">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php $i++; } 

                            if($i<4){
                        ?>

                        <tr id="1">
                            <td>
                                <input id="file_upload_1" class="form-control" type="file" name="file_upload[]" multiple>
                            </td>
                            <td class="text-left">
                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                <a style="font-size: 20px;" class="text-danger" value="Delete" onclick="file_deleteRow(this)"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                    <?php }?>

                    </tbody>

                </table>

                <?php  if($i<4){ ?>

                <button id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" type="button" style="margin: 0px 15px 15px;"><span class="fa fa-plus"></span> Add File</button>
                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
                <?php }?>

            </div> 

            <div class="form-group col-md-12" style="float: right; ">
                <button type="submit" class="btn btn-sm btn-success customer_btn" style="float: right; ">Update</button>
            </div>

        </form>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    $(function () {
        $('#personal').click(function () {
            $(".form-template").css("display", "none");
            $("#user_pass_div").addClass("form-template");
        });
        $('#business').click(function () {
            $(".form-template").css("display", "flex");
        });
    });
//    ============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {
        if ($('#business').is(":checked"))
        {
            if ($('#username').val() == '') {
                $('#username').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#username').css({'border': '2px; solid green'});
            }
        }
    });
//    ============ its for show password ===============
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
//    ============ its for generate password ============
    function randomPassword(length = 6) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }


    function generate() {
        customerFrm.password.value = randomPassword(customerFrm.length.value);
    }

//    ============ close generate password =============
//    =============== its for google place address geocomplete ===============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });
    });
//    =========== close google address api ===================



        $("body").on("change", "#file_upload_1", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });




        $("body").on("change", "#file_upload_2", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });




        $("body").on("change", "#file_upload_3", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });


        $("body").on("change", "#file_upload_4", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });






//    ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            alert("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count, e = document.createElement("tr");
            e.innerHTML = "\n\
                                     <td class='text-left'><select id='phone_type_" + count + "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value=''>Type</option><option value='Phone'>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" + count + "' class='phone form-control phone_no_type current_row' type='text' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                    <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='fa fa-close'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    count++;
//=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function (e) {
                var key = e.charCode || e.keyCode || 0;
                var phone = $(this);
                if (phone.val().length === 0) {
                    phone.val(phone.val() + '+1 (');
                }
                // Auto-format- do not expose the mask as the user begins to type
                if (key !== 8 && key !== 9) {
                    //alert("D");
                    if (phone.val().length === 6) {

                        phone.val(phone.val());
                        phone = phone;
                    }
                    if (phone.val().length === 7) {
                        phone.val(phone.val() + ') ');
                    }
                    if (phone.val().length === 12) {
                        phone.val(phone.val() + '-');
                    }
                    if (phone.val().length >= 17) {
                        phone.val(phone.val().slice(0, 16));
                    }
                }
                // Allow numeric (and tab, backspace, delete) keys only
                return (key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
            })
                    .on('focus', function () {
                        phone = $(this);

                        if (phone.val().length === 0) {

                            phone.val('+1 (');
                        } else {
                            var val = phone.val();
                            phone.val('').val(val); // Ensure cursor remains at the end
                        }
                    })

                    .on('blur', function () {
                        $phone = $(this);

                        if ($phone.val() === '(') {
                            $phone.val('');
                        }
                    });
//            =========== close phone format ============

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
        }
    }
//    ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);

            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function () {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });

            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function () {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }
//    ======== close ===========

//========== its for customer multiple file upload ==============
    function addInputFile(t) {
        var table_row_count = $("#normalItem_file > tbody > tr").length;
        var file_count = table_row_count + 1;
        file_limits = 5;
        if (file_count == file_limits) {
//            alert("You have reached the limit of adding " + file_count + " files");
            alert("You have reached the limit of adding 4 files");
        } else {

//            alert(table_row_count);
            var a = "file_upload_" + file_count, e = document.createElement("tr");
            e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                    <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='fa fa-close'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    file_count++;
        }
    }
    function file_deleteRow(t) {
        var a = $("#normalItem_file > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_file_count = 1;
            $("#normalItem_file > tbody > tr td input.current_file_count").each(function () {
                current_file_count++;
                $(this).attr('class', 'form-control');
                $(this).attr('id', 'file_upload_' + current_file_count);
                $(this).attr('name', 'file_upload_' + current_file_count);
            });

        }
    }
    var file_count = 2,
            file_limits = 5;
//    ======== close multiple file uploadd add row===========

</script>