
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>BMSLink D-Level Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $baseurl = 'http://soft9.bdtask.com/bmslink_apps/'; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/reset.css">


        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/toster/toastr.css" media="screen">


        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/custom_style.css" media="screen">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/colors/blue.css">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/select2.min.css">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/select2-bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap-datepicker3.min.css" />


        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
        <style>
            .pac-container:after{
                content:none !important;
            }
            .custom_p_5{
                padding: 2rem 3rem !important;
            }
            .custom_form_group{
                margin-bottom: 0.5rem;
            }
        </style>
    </head>
    <script type="text/javascript">
        var mybase_url = '<?php echo $baseurl; //base_url();  ?>';
    </script>
    <body class="background show-spinner">
        <div class="fixed-background"></div>
        <main>
            <div class="container">
                <div class="card form_m_width">
                    <div class="ui small">
                        <div class="content p-5 custom_p_5">
                            <?php
                            $user_id = $this->uri->segment(2);
                            $company_profile = $this->db->select('*')
                                            ->from('company_profile')
                                            ->where('user_id', $user_id)
                                            ->get()->result();
//                            echo $baseurl."assets/b_level/uploads/appsettings/".$company_profile[0]->logo;
                            ?>
                            <a href="index.html" class="d-block text-center mb-5">
                                <!--<img src="<?php // echo base_url();  ?>assets/c_level/img/logo.png" alt="">-->
                                <img src="<?php echo $baseurl; //base_url();  ?>assets/b_level/uploads/appsettings/<?php echo @$company_profile[0]->logo; ?>" alt=""  style="width: 10%; margin-top: -15px !important; ">
                            </a>
                            <div class="">
                                <?php
                                $error = $this->session->flashdata('error');
                                $success = $this->session->flashdata('success');
                                if ($error != '') {
                                    echo $error;
                                }
                                if ($success != '') {
                                    echo $success;
                                }
                                ?>
                            </div>
                            <?php
// echo '<pre>'; print_r($d_level_form);
//                            echo $d_level_form[0]->iframe_code;
                            ?>
                            <form action="<?php echo $baseurl; //base_url();  ?>b-d-customer-info-save" method="post">
                                <div class="form-row">
                                    <?php // echo $d_level_form[0]->iframe_code; ?>
                                    <div class="form-group col-md-6">
                                        <label for="first_name" class="mb-2">First name</label>
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John" required>
                                        <div class="valid-tooltip">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name" class="mb-2">Last name</label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" required>
                                        <div class="valid-tooltip">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email" class="mb-2">Email</label>
                                        <input type="email" class="form-control" id="email" onkeyup="" name="email" placeholder="johndoe@yahoo.com">
                                        <span id="error"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone" class="mb-2">Contact</label>
                                        <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="company" class="mb-2">Company</label>
                                        <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="address" class="mb-2">Address</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="city" class="mb-2">City</label>
                                        <input type="text" class="form-control" id="city" name="city">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="state" class="mb-2">State</label>
                                        <input type="text" class="form-control" id="state" name="state">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="zip_code" class="mb-2">Zip</label>
                                        <input type="text" class="form-control" id="zip_code" name="zip_code">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="country_code" class="mb-2">Country Code</label>
                                        <input type="text" class="form-control" id="country_code" name="country_code">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="reference" class="mb-2">Reference</label>
                                        <input type="text" class="form-control" id="reference" name="reference" placeholder="johndoe18" required>
                                    </div>
                                    <!--                                    <div class="form-group col-md-6">
                                                                            <a href="<?php echo base_url(); ?>c-level-dashboard" class="d-block f-14">C-Level View</a>
                                                                        </div>-->
                                    <div class="form-group col-md-6">
                                        <input type="hidden" name="user_id" value="<?php echo $this->uri->segment(2); ?>">
                                        <input type="hidden" name="level_type" value="<?php echo $this->uri->segment(3); ?>">
                                        <button type="submit" class="btn btn-primary d-block float-right">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/jquery.flot.min.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/tiny_mce/jquery.tinymce.js"></script>
        <!-- scripts (custom) -->
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/smooth.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/smooth.menu.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/smooth.chart.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/select2.full.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/js/bootstrap-datepicker.js"></script>
        <!--========= time picker ==========-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
        <script src="<?php echo base_url() ?>assets/b_level/resources//js/bootstrap-datetimepicker.js"></script>

        <!--=========== its for add customer phone format .js ================-->
        <script src="<?php echo base_url(); ?>assets/c_level/js/phone-format.js"></script>
        <!--=========== its for add customer phone format .js close ================-->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/b_level/toster/toastr.min.js"></script>
        <!--        <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/dore.script.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/scripts.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/phone-format.js"></script>-->

        <script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
        <script type="text/javascript">
//    ======= its for google place address geocomplete =============
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));

            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                //console.log(place);
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var geocoder = new google.maps.Geocoder;
                var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        //console.log(results)
                        if (results[0]) {
                            //document.getElementById('location').innerHTML = results[0].formatted_address;
                            var street = "";
                            var city = "";
                            var state = "";
                            var country = "";
                            var country_code = "";
                            var zipcode = "";
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].types[0] === "locality") {
                                    city = results[i].address_components[0].long_name;
                                    state = results[i].address_components[2].short_name;

                                }
                                if (results[i].types[0] === "postal_code" && zipcode == "") {
                                    zipcode = results[i].address_components[0].long_name;

                                }
                                if (results[i].types[0] === "country") {
                                    country = results[i].address_components[0].long_name;
                                }
                                if (results[i].types[0] === "country") {
                                    country_code = results[i].address_components[0].short_name;
                                }
                                if (results[i].types[0] === "route" && street == "") {
                                    for (var j = 0; j < 4; j++) {
                                        if (j == 0) {
                                            street = results[i].address_components[j].long_name;
                                        } else {
                                            street += ", " + results[i].address_components[j].long_name;
                                        }
                                    }

                                }
                                if (results[i].types[0] === "street_address") {
                                    for (var j = 0; j < 4; j++) {
                                        if (j == 0) {
                                            street = results[i].address_components[j].long_name;
                                        } else {
                                            street += ", " + results[i].address_components[j].long_name;
                                        }
                                    }

                                }
                            }
                            if (zipcode == "") {
                                if (typeof results[0].address_components[8] !== 'undefined') {
                                    zipcode = results[0].address_components[8].long_name;
                                }
                            }
                            if (country == "") {
                                if (typeof results[0].address_components[7] !== 'undefined') {
                                    country = results[0].address_components[7].long_name;
                                }
                                if (typeof results[0].address_components[7] !== 'undefined') {
                                    country_code = results[0].address_components[7].short_name;
                                }
                            }
                            if (state == "") {
                                if (typeof results[0].address_components[5] !== 'undefined') {
                                    state = results[0].address_components[5].short_name;
                                }
                            }
                            if (city == "") {
                                if (typeof results[0].address_components[5] !== 'undefined') {
                                    city = results[0].address_components[5].long_name;
                                }
                            }

                            var address = {
                                "street": street,
                                "city": city,
                                "state": state,
                                "country": country,
                                "country_code": country_code,
                                "zipcode": zipcode,
                            };
                            //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                            $("#city").val(city);
                            $("#state").val(state);
                            $("#zip_code").val(zipcode);
                            $("#country_code").val(country_code);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });

            });


        });
        </script>

    </body>

</html>