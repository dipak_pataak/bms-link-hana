


<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Payment log</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Payment log</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>


        <!-- 
                <div class="card mb-4 " >
                    <div class="card-body">
                        <p class="mb-0">
                            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Filter
                            </button>
                        </p>
                        <div class="collapse" id="collapseExample">
                            <div class="p-4 border mt-4">
        
                                <form class="form-horizontal" action="<?php echo base_url('c_level/payment_log/get_log'); ?>" method="post">
        
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="text" class="form-control datepicker mb-3 " name="fromdate" placeholder="From date">
                                            </div>
        
                                            <div class="col-md-3">
                                                <input type="text" class="form-control datepicker mb-3 " name="todate" placeholder="To date">
                                            </div>
                                            
                                            <div class="col-md-2 text-right">
                                                <div>
                                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>   --> 


        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <!-- end box / title -->
                        <div class="table-responsive px-3">
                            <table class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Order Id</th>
                                        <th>Payment Method</th>
                                        <th>Paid amount</th>
                                        <th>Date</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($logs)) {
                                        $i = 1;
                                        foreach ($logs as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?= $i++; ?></td>
                                                <td><?= $value->quotation_id; ?></td>
                                                <td><?= $value->payment_method; ?></td>
                                                <td> <?= $cmp_info[0]->currency; ?><?= $value->paid_amount; ?></td>
                                                <td><?= date_format(date_create($value->create_date), 'M-d-Y'); ?></td>
                                            </tr>

                                            <?php
                                        }
                                    } else {
                                        ?>

                                        <!--<div class="alert alert-danger"> There have no notification found..</div>-->

                                    <?php } ?>
                                </tbody>
                                <?php if (empty($logs)) { ?>
                                    <tr>
                                        <th class="text-center text-danger" colspan="5">Record not found!</th>
                                    </tr>
                                <?php } ?>
                            </table>

                            <?= @$links ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</main>
