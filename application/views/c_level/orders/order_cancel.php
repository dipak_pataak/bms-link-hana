<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 262.594px !important;
        left: 744.5px;
        z-index: 10;
        display: block;
    }
</style>
<?php $currency = $company_profile[0]->currency; ?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Cancelled Order</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cancelled Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>



        <div class="card mb-4">
            <div class="card-body">

                <p class="mb-0">
                    <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Filter
                    </button>
                </p>
                <div class="collapse show" id="collapseExample">
                    <div class="p-4 border mt-4">

                        <form class="form-horizontal" action="<?= base_url('order-cancel') ?>" method="post">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-3">
                                        <input type="text" class="form-control mb-3" placeholder="Order No." name="order_id">
                                    </div>

                                    <div class="col-md-3">
                                        <select class="form-control select2-single" name="customer_id" data-placeholder="--Select customer--">
                                            <option value=""></option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option value="<?= $c->customer_id ?>"><?= $c->first_name; ?> <?= $c->last_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <input type="text"  name="order_date" id="order_date" class="form-control datepicker mb-3" placeholder="Order Date">
                                    </div>                          

                                    <div class="col-md-3">
                                        <select class="form-control select2-single" name="order_stage" data-placeholder="--Select Status--">
                                            <option value=""></option>
                                            <option value="1">Quote</option>
                                            <option value="2">Paid</option>
                                            <option value="3">Partially Paid</option>
                                            <option value="4">Manufacturing</option>
                                            <option value="5">Shipping</option>
                                            <option value="6">Cancelled</option>
                                            <option value="7">Delivered</option>
                                        </select>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <div>
                                            <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                            <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Order No</th>
                                        <th>Customer Name</th>
                                        <th>Product List</th>
                                        <th>Order Date</th>
                                        <th>Status</th>
                                        <th>Cancel Comment</th>
                                        <th>Grand Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($orderd)) {
                                        foreach ($orderd as $key => $value) {

                                            $query = $this->db->select("qutation_details.*,
                                                product_tbl.product_name,
                                                quatation_attributes.product_attribute,
                                                pattern_model_tbl.pattern_name,
                                                color_tbl.color_name")
                                                            ->from('qutation_details')
                                                            ->join('product_tbl', 'product_tbl.product_id=qutation_details.product_id', 'left')
                                                            ->join('quatation_attributes', 'quatation_attributes.fk_od_id=qutation_details.row_id', 'left')
                                                            ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id', 'left')
                                                            ->join('color_tbl', 'color_tbl.id=qutation_details.color_id', 'left')
                                                            ->where('qutation_details.order_id', $value->order_id)
                                                            ->get()->result();
                                            ?>

                                            <tr>
                                                <td>
                                                    <a href="<?php echo base_url('c_level/invoice_receipt/receipt/'); ?><?php echo $value->order_id; ?>">
                                                        <?= $value->order_id; ?>
                                                    </a>
                                                </td>
                                                <td><?= $value->customer_name . " ( " . $value->side_mark . " )"; ?></td>
                                                <td>
                                                    <?php
                                                    $products = '';
                                                    foreach ($query as $key => $val) {
                                                        $products .= $val->product_name . ', ';
                                                    }
                                                    echo rtrim($products, ', ');
                                                    ?>    
                                                </td>
                                                <td><?= $value->order_date; ?></td>
                                                
                                                <?php if ($this->permission->check_label('manage_order')->update()->redirect()) { ?>

                                                    <td>
                                                        <select class="form-control select2-single" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')" data-placeholder="-- select one --">
                                                            <option value=""></option>
                                                            <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                                            <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                                            <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                                            <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>In Process</option>
                                                            <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping</option>
                                                            <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
                                                            <option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>
                                                        </select>
                                                    </td>
                                                    
                                                <?php } ?>

                                                   
                                                <td><?= $value->cancel_comment; ?></td>
                                                <td><?= $currency . " " . $value->grand_total ?></td>


                                            </tr>

                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <!--<div class="alert alert-danger"> There have no order found..</div>-->
                                    <?php } ?>
                                </tbody>
                                <?php if (empty($orderd)) { ?>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="7">Record not found!</th>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</main>


<script type="text/javascript">

    function setOrderStage(stage_id, order_id) {

        if (stage_id != '') {
            if (stage_id === '2' || stage_id === '3') {
                window.location.href = 'c_level/order_controller/order_view/' + order_id;
            }
            if (stage_id === '7' || stage_id === '4' || stage_id === '5' || stage_id === '1') {

                $.ajax({
                    url: "<?php echo base_url('c_level/order_controller/set_order_stage/'); ?>" + stage_id + "/" + order_id,
                    type: 'GET',
                    success: function (r) {
                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function () {
                            window.location.href = window.location.href;
                        }, 2000);
                    }
                });

            } else {
                $("#order_id").val(order_id);
                $("#stage_id").val(stage_id);
                $("#cancel_comment_info").html();
                $('#cancel_comment_modal_info').modal('show');
            }

        }


    }

</script>   
