<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Step 1</h5>
                        <div class="separator mb-5"></div>
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                                        <div class="col-sm-8">
                                            <p>1135</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Date</label>
                                        <div class="col-sm-8">
                                            <p>11/30/18</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Select /Add Customer</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Side Mark</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row m-0 ship_addr">
                                        <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="mapLocation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="row m-0">
                                        <div class="col-sm-8 offset-sm-4 mb-2">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="shipaddress" checked>
                                                <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Step 2 (Quote/Order)</h5>
                        <div class="separator mb-5"></div>
                        <div class="row sticky_container">
                            <div class="col-sm-8">
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <label for="" class="col-sm-6">Select Category</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control select2-single">
                                                        <option value="d1">Category</option>
                                                        <option value="d2">Category B</option>
                                                        <option value="d3">Category C</option>
                                                        <option value="d4">Category D</option>
                                                        <option value="d5">Category E</option>
                                                        <option value="d6">Category F</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <label for="" class="col-sm-6">Select Sub Category</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control select2-single">
                                                        <option value="d1">Sub-Category</option>
                                                        <option value="d2">Sub-Category B</option>
                                                        <option value="d3">Sub-Category C</option>
                                                        <option value="d4">Sub-Category D</option>
                                                        <option value="d5">Sub-Category E</option>
                                                        <option value="d6">Sub-Category F</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <label for="" class="col-sm-6">Select Product</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control select2-single">
                                                        <option value="e1">product</option>
                                                        <option value="e2">product B</option>
                                                        <option value="e3">product C</option>
                                                        <option value="e4">product D</option>
                                                        <option value="e5">product E</option>
                                                        <option value="e6">product F</option>
                                                        <option value="e7">product G</option>
                                                        <option value="e8">product H</option>
                                                        <option value="e9">product I</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <label for="" class="col-sm-6">Select Pattern/Model</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control select2-single">
                                                        <option value="f1">model A</option>
                                                        <option value="f2">model B</option>
                                                        <option value="f3">model C</option>
                                                        <option value="f4">model D</option>
                                                        <option value="f5">model E</option>
                                                        <option value="f6">model F</option>
                                                        <option value="f7">model G</option>
                                                        <option value="f8">model H</option>
                                                        <option value="f9">model I</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Width</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Height</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 1</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 2</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 3</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 4</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 5</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 6</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 7</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Attribute 8</lable>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <lable class="col-sm-6">Note</lable>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control" rows="6"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 mb-0">
                                            <button type="submit" class="btn btn-primary mb-0 float-right">Add product to list</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-4 sticky_item">
                                <div class="fixed_item">
                                    <p>woodenblink33*60 + attributes $220</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Step 3</h5>
                        <div class="separator mb-5"></div>
                        <table class="datatable2 table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Name of Product Include Specifications</th>
                                    <th>Qty</th>
                                    <th>List Amount</th>
                                    <th>Discount Amount </th>
                                    <th>Price</th>
                                    <th>Comments</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Wooden Blind - 2X2 1/2 - White - other Attributes</td>
                                    <td>
                                        <div id="field1">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input type="number" id="1" value="4" min="1" class="qty_input">
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </td>
                                    <td>$220.00</td>
                                    <td>$154.00</td>
                                    <td>$66.00</td>
                                    <td>special notes</td>
                                    <td>
                                        <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Wooden Blind - 4X2 1/2 - White - other Attributes</td>
                                    <td>
                                        <div id="field1">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input type="number" id="1" value="9" min="1" class="qty_input">
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </td>
                                    <td>$330.00</td>
                                    <td>$20.00</td>
                                    <td>$310.00</td>
                                    <td>special notes</td>
                                    <td>
                                        <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div>

            </div>

            <div class="col-lg-4 offset-lg-8">
                <div class="card mb-4">
                    <div class="card-body">
                        <table class="table table-bordered m-0">
                            <tr>
                                <td>Sub Total</td>
                                <td>$376.00</td>
                            </tr>
                            <tr>
                                <td>State Tax</td>
                                <td>$7.00</td>
                            </tr>
                            <tr>
                                <td>Installation Charge</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Other Charge</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Discount</td>
                                <td>$10.00</td>
                            </tr>
                            <tr>
                                <td>Misc</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Grand Total</td>
                                <td>$373.00</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-8 text-right">
                <a href="invoice_print.html" class="btn btn-success">Generate Quote</a>
                <a href="invoice_print.html" class="btn btn-warning">Generate Quote + Invoice</a>
            </div>
            <div class="col-lg-12">
                <div class="foku">
                    <span class="potu">some content</span>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $('.add').click(function () {
        if ($(this).prev().val() < 1000) {
            $(this).prev().val(+$(this).prev().val() + 1);
        }
    });
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1)
                $(this).next().val(+$(this).next().val() - 1);
        }
    });

    $("#shipaddress").click(function () {
        $(".ship_addr").slideToggle();
    });
</script>
<script>
    $(document).ready(function () {
        var autocomplete = new google.maps.places.Autocomplete($("#mapLocation")[0]);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            $('#data').html('<div>Postal Code: ' + place.address_components[0].long_name + '</div><div>Road Name:' + place.address_components[1].long_name + '</div><div>City:' + place.address_components[2].long_name + '</div><div>Country:' + place.address_components[2].long_name + '</div>');
            console.log(place.address_components);
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.sticky_container .sticky_item').theiaStickySidebar({
            additionalMarginTop: 110
        });
    });
</script>
