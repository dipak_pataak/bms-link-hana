
<script type="text/javascript">


            // var install_charge  = $('#install_charge').val().split(".")[1];
            // if (install_charge.length > 2 ){
            //     $('#install_charge').val(parseFloat($('#install_charge').val()).toFixed(2));
            // }


            // var other_charge  = $('#other_charge').val().split(".")[1];
            // if (other_charge.length > 2 ){
            //     $('#other_charge').val(parseFloat($('#other_charge').val()).toFixed(2));
            // }


            // var invoice_discount  = $('#invoice_discount').val().split(".")[1];
            // if (invoice_discount.length > 2 ){
            //     $('#invoice_discount').val(parseFloat($('#invoice_discount').val()).toFixed(2));
            // }


            // var misc  = $('#misc').val().split(".")[1];
            // if (misc.length > 2 ){
            //     $('#misc').val(parseFloat($('#misc').val()).toFixed(2));
            // }


        $('#install_charge').on('keyup', function () {
            var value = $(this).val();
            var att = value.split(".")[1];

            if (att.length > 2 ){

                $(this).val(parseFloat(value).toFixed(2));
            }
        })
            
        $('#other_charge').on('keyup', function () {
            var value = $(this).val();
            var att             = value.split(".")[1];
            if (att.length > 2 ){
                $(this).val(parseFloat(value).toFixed(2));
            }
        })
            
        $('#invoice_discount').on('keyup', function () {
            var value = $(this).val();
            var att             = value.split(".")[1];
            if (att.length > 2 ){
                $(this).val(parseFloat(value).toFixed(2));
            }
        })
            
        $('#misc').on('keyup', function () {
            var value = $(this).val();
            var att             = value.split(".")[1];
            if (att.length > 2 ){
                $(this).val(parseFloat(value).toFixed(2));
            }
        })




    $(function () {

        $('#personal').click(function () {
            $(".form-template").css("display", "none");
        });
        $('#business').click(function () {
            $(".form-template").css("display", "flex");
        });

    });

    
//    ============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {

        if ($('#business').is(":checked"))
        {
            if ($('#username').val() == '') {
                $('#username').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#username').css({'border': '2px; solid green'});
            }
            if ($('#password').val() == '') {
                $('#password').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#password').css({'border': '2px; solid green'});
            }
        }
    });



    function multiOptionPriceValue(att_op_op_op_op_id){

        var op_op_op_id     = att_op_op_op_op_id.split("_")[0];
        var att             = att_op_op_op_op_id.split("_")[1];
        var op_op_id        = att_op_op_op_op_id.split("_")[2];
        var main_p          = parseFloat($('#main_price').val());

        if(isNaN(main_p)){
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_id) {

            var wrapper = $("#mul_op_op_id" + op_op_id).parent().next();

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/multioption_price_value/')?>" + op_op_op_id + "/" + att + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }

        cal();
    }





    function OptionFive(att_op_op_op_op_id, attribute_id){

        var op_op_op_op_id    = att_op_op_op_op_id.split("_")[0];
        var op_op_op_id    = att_op_op_op_op_id.split("_")[1];

        var main_p      = parseFloat($('#main_price').val());

        if(isNaN(main_p)){
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_op_id) {

            var wrapper = $("#op_op_op_" + op_op_op_id).next().next();
            $.ajax({
                url: "b_level/order_controller/get_product_attr_op_five/" + op_op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                }
            });
        }

        cal();

    }


    function OptionOptionsOptionOption(pro_att_op_id, attribute_id){

        var op_op_op_id    = pro_att_op_id.split("_")[0];
        var op_op_id = pro_att_op_id.split("_")[1];
        var main_p      = parseFloat($('#main_price').val());

        if(isNaN(main_p)){
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_id) {

            var wrapper = $("#op_op_" + op_op_id).next().next();
            $.ajax({
                url: "b_level/order_controller/get_product_attr_op_op_op_op/" + op_op_op_id + "/" + attribute_id+"/"+main_price,
                type: 'get',
                success: function (r) {
                
                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }



    function OptionOptionsOption(pro_att_op_id, attribute_id){


        var op_op_id    = pro_att_op_id.split("_")[0];

         var id = pro_att_op_id.split("_")[1];

         var op_id = pro_att_op_id.split("_")[2];

         var main_p      = parseFloat($('#main_price').val());


        if(isNaN(main_p)){

            var main_price = 0;

        } else {

            var main_price = main_p;

        }

        if (op_op_id) {

            var wrapper = $("#op_" + op_id).parent().next().next();

            $.ajax({
                url: "b_level/order_controller/get_product_attr_op_op_op/" + op_op_id + "/" + id + "/" + attribute_id+"/"+main_price,
                type: 'get',
                success: function (r) {
                    
                    $(wrapper).html(r);
                }
            });

        }


    }




    function OptionOptions(pro_att_op_id, attribute_id) {

        if (pro_att_op_id) {
            //id
            var id = pro_att_op_id.split("_")[0];
            //option_id
            var optin_id = pro_att_op_id.split("_")[1];
            //mainprice
            var main_p = parseFloat($('#main_price').val());

            if(isNaN(main_p)){
                var main_price = 0;
            }else{
                var main_price = main_p;
            }

            //alert(main_price);

            var wrapper = $(".options_" + attribute_id).parent().next().next();

            $.ajax({
                url: "c_level/order_controller/get_product_attr_option_option/" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });

        } else {

        }


    }


    function getColorCode(id){

        var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code/" + id;
            
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {

                $('#colorcode').val(res);

            }, error: function () {
                alert('error');
            }
        });

    }


    function getColorCode_select(keyword){

        if(keyword!==''){

            var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code_select/" + keyword;
            
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    $('#color_id').val(res);

                }, error: function () {
                    alert('error');
                }
            });
        }

    }



    $('body').on('change', function () {
        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);

    })


    $('body').on('click', function () {

        $("#pattern_id").css("border-color", "");
        cal();
        
    })


    function cal(){

        var w = $('#width').val();
        var h = $('#height').val();

        if(w!=='' && h!==''){
        

            var contibut_price = 0;
            $(".contri_price").each(function () {
                isNaN(this.value) || 0 == this.value.length || (contibut_price += parseFloat(this.value))
            });

            var main_price = parseFloat($('#main_price').val());

            if(isNaN(main_price)){
                var prc = 0;
            }else{
                
                var prc = main_price;
            }

            var total_price = (contibut_price + prc);
            var t = (isNaN(total_price)?0:total_price);

            $("#total_price").val(t);
            $("#tprice").text("Total Price = $" + t);

        }

    }



    function getSalesTax(v){


        var formData = {
                'vs': v 
            };

        $.ajax({
            url: '<?=base_url()?>c_level/order_controller/different_shipping_tax',
            type: 'post',
            data: formData,
            success: function (data) {

                var obj = jQuery.parseJSON(data);
                var tax = (obj.tax_rate!=null?obj.tax_rate:0);
                $('#tax').val(tax);
                customerWiseComission();
               
            }

        });

    }  



//width fraction get 
    $('#width').on('keyup',function(){

        var hif = $(this).val().split(".")[1];
        
        if(hif){
            $.ajax({
                url: "b_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {

                    $("#width_fraction_id ").val(r);

                },error: function() {
                    alert('error');
                }
            });
        }else{

            $("#width_fraction_id ").val('');
        }
    });
//--end---


// Height fraction 
    $('#height').on('keyup',function(){

        var hif = $(this).val().split(".")[1];

        if(hif){
            $.ajax({
                url: "b_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id ").val(r);
                }
            });
        }else{

            $("#height_fraction_id ").val('');
        }

    
    });
//---End----



    function loadPStyle(){


        var product_id = $('#product_id').val();
        var pattern_id = $('#pattern_id').val();
        var pricestyle = $('#pricestyle').val();


        var hif = ($("#height_fraction_id :selected").text().split("/")[0]/$("#height_fraction_id :selected").text().split("/")[1]);
        var wif = ($("#width_fraction_id :selected").text().split("/")[0]/$("#width_fraction_id :selected").text().split("/")[1]);

        var height = parseFloat($('#height').val())+(isNaN(hif)?0:hif);
        var width = parseFloat($('#width').val())+(isNaN(wif)?0:wif);

        var width_w = (isNaN(width)?'':width);
        var height_w = (isNaN(height)?'':height);

        var price = $("#height").parent().parent().parent().next();

        if( pricestyle === '4' ){

            if(pattern_id==''){
               alert('Please select the Pattern/Model');
               $("#pattern_id").css("border-color", "red").focus();
            } 

        }
        

        if(pricestyle === '1' || pricestyle === '4' || pricestyle === '5' ){

            $.ajax({

                url: "c_level/order_controller/get_product_row_col_price/"+height_w+"/"+width_w+"/"+product_id+"/"+pattern_id,
                type: 'get',
                
                success: function (r) {
                    
                    var obj = jQuery.parseJSON(r);

                    $(price).html(obj.ht);

                    cal();

                    //$("#tprice").text("Total Price = $"+obj.prince);
                   
                    if(obj.st===1){

                        $('#cartbtn').removeAttr('disabled');
                    }else if(obj.st === 2){
                        
                    }
                }

            });

        }


        if(pricestyle==='2'){

            var main_p = parseFloat($('#sqr_price').val());

            if(isNaN(main_p)){
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var sum = (width*height)/144;
            
            var price = sum*main_price;

            $('#main_price').val(price.toFixed(2));

        } 
        //---End--
          
        cal();
    }





    $(document).ready(function () {


//-----------------------------
// GET Product Attribute
// ----------------------------

        $('body').on('change', '#product_id', function () {

            var product_id = $(this).val();

            $.ajax({

                url: "c_level/order_controller/get_product_to_attribute/" + product_id,
                type: 'get',
                success: function (r) {
                    $("#attr").html(r);
                    callTrigger();
                    $('#cartbtn').removeAttr('disabled');
                }

            });


            $.ajax({

                url: "c_level/order_controller/get_color_partan_model/" + product_id,
                type: 'get',
                success: function (r) {
                    $('#color_model').html(r);
                }

            });

        });

        $('body').on('change', '#pattern_id', function () {

            var pattern_id = $(this).val();

            $.ajax({

                url: "c_level/order_controller/get_color_model/" + pattern_id,
                type: 'get',
                success: function (r) {
                    $('#pattern_color_model').html(r);
                }

            });

        });


//-----------------------------
// END
// ----------------------------



        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });


        $.ajax({

            url: "<?=base_url()?>c_level/order_controller/order_id_generate",
            type: 'get',
            success: function (r) {
                $("#orderid").val(r);
            }

        });


//---------------------------
// GET Product by category
// --------------------------

        $('body').on('change', '#category_id', function () {

            var category_id = $(this).val();
            $.ajax({
                url: "c_level/order_controller/category_wise_subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    $("#subcategory_id").html(r);
                }
            });

        });

//---------------------------
// END
// --------------------------


//---------------------------
// GET Product by category
// --------------------------
        
        $('body').on('change', '#category_id', function () {

            var category_id = $(this).val();

            $("#total_price").val(0);
            $("#main_price").val(0);
            $("#product_id").val('');

            $("#tprice").text("Total Price = $0");
            $("#attr").load(location.href+" #attr>*","");

            loadPStyle(); 

         /*   $.ajax({
                url: "b_level/order_controller/get_product_by_category/" + category_id,
                type: 'get',
                success: function (r) {
                    $("#product_id").html(r);
                }
            });*/

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_customer_product_by_category/');?>" + category_id,
                type: 'get',
                success: function (r) {
                    $("#product_id").html(r);
                }
            });
            
        });
//---------------------------
// END
// --------------------------



    function callTrigger(){

        $('.op_op_load').trigger('change');
        $('.op_op_op_load').trigger('change');
        $('.op_op_op_op_load').trigger('change');
        $('.op_op_op_op_op_load').trigger('change');
    }


    
// ================= its for customer wise sidemark ==========
        

        $("body").on('change', '#customer_id', function () {

            var customer_id = $(this).val();
            
            $.ajax({
                url: '<?=base_url()?>c_level/order_controller/customer_wise_sidemark/' + customer_id,
                type: 'get',
                success: function (data) {

                    if (data == 0) {
                        $("#side_mark").val("None");
                    } else {

                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate!=null?obj.tax_rate:0);

                        $('#customertype').val(obj.level_id);
                        $('#side_mark').val(obj.side_mark);

                        $('#tax').val(tax);

                        customerWiseComission();

                    }
                }
            });

            
        });

//======================================       


        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);   
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);   
        });
        

    });




    // submit form and add data
    $("#AddToCart").on('submit',function(e){

        e.preventDefault();

        var submit_url = "c_level/order_controller/add_to_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function(res) {

                window.location.reload();

                // $("#cartItems").load(location.href+" #cartItems>*",""); 

                toastr.success('Success! - Add to cart Successfully');
                setTimeout(function(){
                }, 2000);

            },error: function() {
                alert('error');
            }
        });
    });



    // submit form and add data
    $("#clearCart").on('click',function(e){

        e.preventDefault();

        var submit_url = "<?=base_url();?>b_level/order_controller/clear_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function(res) {
         
            window.location.reload();

            toastr.success('Success! - Cleare cart Successfully');
            setTimeout(function(){
            }, 2000); 
                
            },error: function() {
                alert('error');
            }
        });
    });


    function deleteCartItem(id){

        var submit_url = "<?=base_url();?>b_level/order_controller/delete_cart_item/"+id;
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function(res) {
         
            $("#cartItems").load(location.href+" #cartItems>*",""); 
                
            },error: function() {
                alert('error');
            }
        });

    }



    function customerWiseComission(){

        var customertype = $('#customertype').val();

        if(customertype==''){
            alert('Please select customer');
            $( "#customer_id" ).focus();
        }else{

            var i=1;

            $(".product_id").each(function() {
              
                var productid = (this.value);
                var customer_id = $('#customer_id').val();
                var submit_url = "c_level/order_controller/getproductcomission/"+productid+'/'+customer_id;


                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function(res) {

                        var obj = jQuery.parseJSON(res);

                        var qty = $('#qty_'+i).val();
                        
                        var list_price = parseFloat($('#list_price_'+i).val());

                        var total_list_price = (list_price*qty);

                        $('#discount_'+i).val(obj.individual_price);

                        var discount = (total_list_price*obj.individual_price)/100;

                        var utprice = total_list_price-discount;
                       
                        $('#utprice_'+i).val(utprice.toFixed(2));

                        calculetsPrice();
                        
                    i++;     
                    },error: function() {
                        alert('error');
                    }
                });

                //i++;
            });
        }

    }



        function calculetsPrice(){


            var install_charge = parseFloat($('#install_charge').val());
            var other_charge = parseFloat($('#other_charge').val());
            var invoice_discount = parseFloat($('#invoice_discount').val());
            var misc = parseFloat($('#misc').val());

           
            var subtotal=0;
            $(".utprice").each(function() {
                isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
            });

            $('#subtotal').val(subtotal.toFixed(2));

            var taxs = parseFloat($('#tax').val());
            var tax = (subtotal*taxs)/100;

            $('#tax_text').text('Sales tax '+taxs+'%');
            $('#tax_val').val(tax.toFixed(2));



            var grandtotal = (subtotal+tax+install_charge+other_charge+misc)-invoice_discount;


            $('#grand_total').val(grandtotal.toFixed(2));

            calDuePaid();
        }



        function calDuePaid(){

            var grand_total = parseFloat($('#grand_total').val());
        
            var paid_amount = parseFloat($('#paid_amount').val());
            var due = (grand_total-paid_amount);
            $('#due').val(due);

        }



        $('#card_area').hide();
        $('#card_area2').hide();

        function setCard(value){

            if(value==='card'){
                $('#card_area').slideDown();
                $('#card_area2').slideDown();
            }else if(value==='cash'){

                $('#card_number').val('');
                $('#issuer').val('');
                $('#card_area').slideUp();
                $('#card_area2').slideUp();

            }
        }



    // submit form and add data
    $("#save_order").on('submit',function(e){

        e.preventDefault();
        var order_status = $(this).val();
        var order_status = $('#order_status').val(order_status);
        var submit_url = "b_level/order_controller/save_order";

    });




function customerWiseComission_Inc_Dic(qty, item){

    var customer_id = $("#customer_id").val();

        if (customer_id === '') {

            alert('Please select customer');
            $("#customer_id").focus();

        }else{

            var qty = $('#qty_' + item).val();

            var list_price = parseFloat($('#list_price_' + item).val());

            var total_list_price = (list_price*qty);

            var dealer_price = $('#discount_' + item).val();

            var discount = (total_list_price * dealer_price) / 100;


            $('#utprice_' + item).val(total_list_price - discount.toFixed(2));
            calculetsPrice();
        }

}






</script>


<script>

    $('.add').click(function () {

            if ($(this).prev().val() < 1000) {
                 $(this).prev().val(+$(this).prev().val() + 1);
                var qty = $(this).prev().val();
                var item = $(this).parent().next().val();
                customerWiseComission_Inc_Dic(qty, item);
            }
        
    });
    
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1)
                $(this).next().val(+$(this).next().val() - 1);

            var qty = $(this).next().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);

        }
    });
</script>


<script>        
    $(document).ready(function() {
        $('.sticky_container .sticky_item').theiaStickySidebar({
             additionalMarginTop: 110
        });
    });
</script>

