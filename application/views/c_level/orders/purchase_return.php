<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 262.594px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
</style>
<?php $currency = $company_profile[0]->currency; ?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Purchase Quotation/Order</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Order</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>

            <?php
            $message = $this->session->flashdata('message');

            if ($message != '') {
                echo $message;
            }
            ?>


        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="table-responsive">

                            <table class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>Order/Quote No</th>
                                        <th>Client Name </th>
                                        <th>Side mark</th>
                                        <th>Order date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    if (!empty($orderd)) {

                                        foreach ($orderd as $key => $value) {

                                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                                            $order_return_info = $this->db->where('order_id', $value->order_id)->get('order_return_tbl')->row();

//                                            echo '<pre>';                                            print_r($order_return_info);
                                            ?>

                                            <tr style="">

                                                <td><?= $value->order_id; ?></td>
                                                <td><?= $value->customer_name; ?></td>
                                                <td><?= $value->side_mark; ?></td>
                                                <td><?= date_format(date_create($value->order_date), 'M-d-Y'); ?></td>
                                                <td><?= $currency; ?><?= $value->grand_total ?></td>
                                                <td>
                                                    <?php 
                                                    $is_approved = @$order_return_info->is_approved;
                                                    if(@$is_approved == 0){
                                                        echo "Pending";
                                                    }
                                                    if(@$is_approved == 1){
                                                        echo "Approved";
                                                    }
                                                    ?>
                                                </td>

                                                <?php if ($this->permission->check_label('manage_order')->update()->redirect()) { ?>

                                    <!--                                                    <td>
                                                                                            <select class="form-control select2-single" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')" data-placeholder="-- select one --">
                                                                                                <option value=""></option>
                                                                                                <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                                                                                <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                                                                                <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                                                                                 <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Manufacturing</option> 
                                                                                                <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping</option>
                                                                                                <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
                                                                                                <option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>
                                                                                            </select>
                                                                                        </td>-->
                                                <?php } ?>

                                                <td>
                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>

                                                        <?php if ($value->order_stage == 7) { ?>
                                                            <a href="c-customer-order-return/<?= $value->order_id ?>" class="btn btn-primary btn-sm" title="Return "><i class="simple-icon-trophy" aria-hidden="true"></i></a>
                                                        <?php } ?>

                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>
                                                        <a href="<?php echo base_url('c-order-return-detials-show/'); ?><?php echo $value->order_id; ?>" class="btn btn-success btn-sm" title="View">
                                                            <i class="simple-icon-eye"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($this->permission->check_label('manage_order')->read()->access()) { ?>

                                    <!--                                                        <a href="<?= base_url('c_level/invoice_receipt/receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-xs default" title="View">
                                                                                                <i class="simple-icon-eye"></i>
                                                                                            </a>-->

                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->delete()->access()) { ?>
                                                                                            <!--<a href="c_level/order_controller/delete_order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>-->
                                                    <?php } ?>
                                                    <a href="<?php echo base_url(); ?>c-purchase-order-return/<?= $value->order_id ?>" class="btn btn-primary btn-sm" title="Return "><i class="simple-icon-trophy" aria-hidden="true"></i></a>
                                                </td>


                                            </tr>

                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <?php if (empty($orderd)) { ?>
                                    <tfoot>
                                        <tr>
                                            <th colspan="8" class="text-center text-danger">Record not found!</th>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="cancel_comment_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Comments</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="cancel_comment_info">
                <form class="" action="<?php echo base_url('c_level/Order_controller/cancel_comment'); ?>" method="post">
                    <textarea class="form-control" name="cancel_comment" required ></textarea>
                    <input type="hidden" name="order_id" id="order_id">
                    <input type="hidden" name="stage_id" id="stage_id">
                    <input type="submit" class="btn btn-info" style="margin-top: 10px;">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function setOrderStage(stage_id, order_id) {
        if (stage_id != 6) {
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/set_order_stage/'); ?>" + stage_id + "/" + order_id,
                type: 'GET',
                success: function (r) {
                    toastr.success('Success! - Order Stage Set Successfully');
                    setTimeout(function () {
                        window.location.href = window.location.href;
                    }, 2000);
                }
            });
        } else {
            $("#order_id").val(order_id);
            $("#stage_id").val(stage_id);
            $("#cancel_comment_info").html();
            $('#cancel_comment_modal_info').modal('show');
        }

    }

</script>   
