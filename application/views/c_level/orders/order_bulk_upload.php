
<main>

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Order bulk upload</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Order bulk upload</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <?php
        $message = $this->session->flashdata('message');

        if ($message != '') {
            echo $message;
        }
        ?>


        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <h5>Order bulk upload</h5>

                        <div class="separator mb-3"></div>

                        <div class="row mb-4">

                            <div class="col-md-6">

                                <div class="form-group">

                                    <?php echo form_open_multipart('c_level/order_bulk_upload/csv_upload_save') ?>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <lable class="col-sm-3">File</lable>
                                            <div class="col-sm-9">
                                                <input type="file" name="upload_csv_file" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-6 offset-lg-6 text-right" style="margin-top: 10px">
                                        <button type="submit" class="btn btn-success" id="gqi" >Upload</button>
                                    </div>

                                    <?php echo form_close(); ?>

                                </div>

                            </div>


                            <div class="col-md-6">

                                <div class="form-group">

                                    <a href="<?php echo base_url('assets/c_level/csv/order_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>    

