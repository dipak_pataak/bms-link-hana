
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="mb-3">
                        <h1>Track Order</h1>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Track Order</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="separator mb-5"></div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">

                            <div id="wait" style="display:none; position:absolute;top:50%;left:50%;padding:2px;">
                                <img src='<?php echo base_url('assets/c_level/img/loader.gif')?>' width="64" height="64" /><br>Loading..</div>

                            <?=form_open('c_level/tracking/track',array('id' => 'TrakIng' ))?>
                            
                            <div class="jumbotron text-center">
                                <h1 class="text-uppercase d-block">Order Traking Number (zin1/zmp1-XXXXXXXX)</h1>
                                <input type="text" name="trackNumber" class="form-control" placeholder="Enter your order track number"><br>
                                <button type="submit" class="btn btn-primary btn-lg mb-5"  role="button">Check Status</button>

                                <p>Knowing your order status has never been this easy Just enter your order number to get your details immediately. Check Status Now.</p>
                            </div>
                            <?php echo form_close(); ?>

                        </div>
                    </div>
                </div>
            </div>


        </div>

    </main>

