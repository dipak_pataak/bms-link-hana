
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-3">
                        <h1>Invoice</h1>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="separator mb-5"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-12 mb-4">

                    <div class="col-lg-6 offset-lg-6 text-right">
                        <?php if($orderd->due>0){?>

                        <a href="<?=base_url('c_level/order_controller/order_view/');?><?=$orderd->order_id?>" class="btn btn-success" >Receive Payment</a>
                        <?php } ?>

                        <?php if($orderd->synk_status==1){?>
                            <a href="<?=base_url('c_level/make_payment/payment_to_b/');?><?=$orderd->order_id?>" class="btn btn-success" >Make Payment</a>
                        <?php } ?>
                        <button type="button" class="btn btn-success" onclick="printContent('printableArea')" >Print</button>
                    </div>


                    <div class="card mb-4">
                        <div class="card-body" id="printableArea">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div class="" style="width:200px; height: 120px;">
                                        <img src="<?php echo base_url('assets/c_level/uploads/appsettings/').$company_profile[0]->logo;?>" style="width:200px; height: auto;">
                                    </div>
                                    <p><?=$company_profile[0]->company_name;?></p>
                                    <p><?=$company_profile[0]->address;?></p>
                                    <p><?=$company_profile[0]->city;?>, 
                                    <?=$company_profile[0]->state;?>, <?=$company_profile[0]->zip_code;?>, 
                                    <?=$company_profile[0]->country_code;?></p>
                                    <p><?=$company_profile[0]->phone;?></p>
                                    <p><?=$company_profile[0]->email;?></p>
                                </div>

                                <div class="form-group col-md-5" style="margin-left: 20px">

                                    <table class="table table-bordered mb-4">

                                        <tr class="text-center">
                                            <td>Order Date</td>
                                            <td class="text-right"><?=date_format(date_create($orderd->order_date),'M-d-Y');?></td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>Order Id</td>
                                            <td class="text-right"><?=$orderd->order_id?></td>
                                        </tr>

                                        <?php if($shipping!=NULL){ ?>
                                            <tr class="text-center">
                                                <td>Tracking Number (<?=$shipping->method_name?>) </td>
                                                <td class="text-right"> <?=$shipping->track_number?> </td>
                                            </tr>
                                        <?php } ?>

                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="form-group col-md-6">
                                    <strong>Sold To: </strong><?=$customer->first_name;?> <?=$customer->last_name;?>
                                    <p><?=$customer->address;?></p>
                                    <p><?=$customer->city;?>, 
                                    <?=$customer->state;?>, <?=$customer->zip_code;?>, 
                                    <?=$customer->country_code;?></p>
                                    <p><?=$customer->phone;?></p>
                                    <p><?=$customer->email;?></p>
                                </div>

                                <div class="form-group col-md-5" style="margin-left: 20px;">

                                    <strong>Ship To: </strong><?=$customer->first_name;?> <?=$customer->last_name;?>
                                    <?php if($orderd->is_different_shipping==1){?>
                                        <p><?=$orderd->different_shipping_address?></p>
                                    <?php }else{?>
                                        <p><?=$customer->address;?></p>
                                        <p><?=$customer->city;?>, 
                                        <?=$customer->state;?>, <?=$customer->zip_code;?>, 
                                        <?=$customer->country_code;?></p>
                                        <p><?=$customer->phone;?></p>
                                        <p><?=$customer->email;?></p>
                                    <?php }?>

                                </div>
                            </div>
                
                            <h5>Order Details</h5>

                            <div class="separator mb-3"></div>

                            <div class="table-responsive m-b-20">

                                <table class="table table-bordered mb-4">

                                    <thead>
                                        <tr>
                                            <th>Item.</th>
                                            <th>Qty</th>
                                            <th width="400px;">Discription</th>
                                            <th>List</th>
                                            <th>Discount(%)</th>
                                            <th>Price</th>
                                            <th>Notes</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $i = 1; ?>

                                        <?php 

                                        foreach ($order_details as $items): 
                                            $width_fraction = $this->db->where('id',$items->width_fraction_id)->get('width_height_fractions')->row();
                                            $height_fraction = $this->db->where('id',$items->height_fraction_id)->get('width_height_fractions')->row();
                                           
                                        ?>

                                            <tr>

                                                <td><?=$i?></td>
                                                <td><?=$items->product_qty;?></td>
                                                <td align="left">
                                                <strong><?=$items->product_name;?></strong><br/> 
                                                <?php 
                                                if($items->pattern_name){
                                                    echo $items->pattern_name.'<br/>';
                                                }
                                                ?>
                                                W <?=$items->width;?> <?=@$width_fraction->fraction_value?>, 
                                                H <?=$items->height;?> <?=@$height_fraction->fraction_value?>, 
                                                <?=$items->color_number;?> 
                                                <?=$items->color_name;?>
                                                
                                                </td>
                                                
                                                <td><?=$company_profile[0]->currency;?><?=number_format($items->list_price,2)?></td>
                                                <td><?=($items->discount)?></td>
                                                <td><?=$company_profile[0]->currency;?> <?=@number_format($items->unit_total_price,2)?> </td>
                                                <td><?=$items->notes?></td>
                                                
                                            </tr>

                                            <?php $i++; ?>

                                        <?php endforeach; ?>
                                            
                                    </tbody>
                                </table>
                    

                                <table class="table table-bordered mb-4">

                                    <thead>
                                        <tr>
                                            <?php if(isset($shipping->shipping_charges) && $shipping->shipping_charges!=NULL){ ?>
                                            <th>Shipping cost</th>
                                            <?php }?>
                                            <th>Sales Tax</th>
                                            <th>Sub-Total</th>
                                            <th>Installation Charge</th>
                                            <th>Other Charge</th>
                                            <th>Misc</th>
                                            <th>Discount</th>
                                            <th>Grand Total</th>
                                            <th>Deposit</th>
                                            <th>Due </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        if(isset($shipping->shipping_charges) && $shipping->shipping_charges!=NULL){  ?>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($shipping->shipping_charges,2)?> </td>
                                        <?php }?>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->state_tax,2)?></td>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->subtotal,2)?> </td>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->installation_charge,2)?> </td>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->other_charge,2)?></td>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->misc,2)?> </td>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->invoice_discount,2)?></td>
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->grand_total,2)?> 
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->paid_amount,2)?> 
                                        <td class="text-right"> <?=$company_profile[0]->currency;?> <?=number_format($orderd->due,2)?> 
                                    </tbody>
                                </table>

                                <?php if(isset($shipping->graphic_image) && $shipping->graphic_image!=NULL){ ?>
                                    <div class="col-lg-6 offset-lg-4">
                                        <img src="<?php echo base_url().$shipping->graphic_image ?>" width="300" height='200' >
                                    </div>
                                <?php }?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<script type="text/javascript">

    function printContent(el){

        $('body').css({"background-color": "#fff"});
        var restorepage  = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
        
    }

</script>



<script>
    $(".dropdown-item").click(function(){
        $(".btn-pay").slideToggle();
    });
</script>