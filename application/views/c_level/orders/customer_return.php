
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Customer Return</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Return</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Invoice No.</th>
                                    <th>Client Name</th>
                                    <th>Side Mark</th>
                                    <th>Comments</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sl = 0 + $pagenum;
                                foreach ($customer_return as $single) {
                                    $sl++;
                                    ?>
                                    <tr>
                                        <td><?php echo $sl; ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>c_level/invoice_receipt/receipt/<?php echo $single->order_id; ?>" title="order id">
                                                <?php echo $single->order_id; ?>    
                                            </a>
                                        </td>
                                        <td><?php echo $single->customer_name; ?></td>
                                        <td><?php echo $single->side_mark; ?></td>
                                        <td><?php echo $single->return_comments; ?></td>
                                        <td><?php echo date('M-d-Y', strtotime($single->return_date)); ?></td>
                                        <td>
                                            <?php
                                            if ($single->is_approved == 1) {
                                                echo "Resend";
                                            } elseif ($single->is_approved == 0) {
                                                echo "Pending";
                                            } elseif ($single->is_approved == 2) {
                                                echo "Cancel";
                                            }
                                            ?>
                                        </td>
                                        <td>
<!--                                            <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" onclick="show_return_resend('<?php echo $single->return_id; ?>', '<?php echo $single->order_id; ?>');" title="Resend " <?php
//                                            if ($single->is_approved == 1) {
//                                                echo "disabled";
//                                            }
                                            ?>>
                                                <i class="simple-icon-refresh" aria-hidden="true"></i>
                                            </button>-->
                                            <button type="button" class="btn btn-primary btn-sm" id="return_show" onclick="show_return_view('<?php echo $single->return_id; ?>')" title="Show"><i class="simple-icon-eye"></i></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <?php if (empty($customer_return)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="text-center text-danger">Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
<div class="modal fade" id="resend_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Order Return Resend Information</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="resend_info">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function show_return_resend(return_id, order_id) {
        $.ajax({
            url: "<?php echo base_url('c-show-return-resend'); ?>",
            type: 'POST',
            data: {'return_id': return_id, 'order_id': order_id},
            success: function (data) {
//                console.log(data);
                $('.modal-title').html("Order Return Resend Information");
                $("#resend_info").html(data);
                $('#resend_modal_info').modal('show');
            }

        });
//        $("#return_id").val(id);
//        $("#resend_info").html();
//        $('#resend_modal_info').modal('show');
    }
//    ============= its for show_return_view =============
    function show_return_view(return_id) {
        $.ajax({
            url: "<?php echo base_url('c-show-return-view'); ?>",
            type: "POST",
            data: {'return_id': return_id},
            success: function (d) {
                console.log(d);
                $('.modal-title').html("Order Return Information");
                $("#resend_info").html(d);
                $('#resend_modal_info').modal('show');
            }
        });
    }

</script>

