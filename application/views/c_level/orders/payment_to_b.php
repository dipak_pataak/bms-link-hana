<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/c_level/card/card_style.css">
<main>

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Order info || Make payment</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Order info</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <?php
            $message = $this->session->flashdata('message');
            
            if ($message != '') {
                echo $message;
            }
        ?>

    	<div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
				    <div class="card-body">
	    				<h5>Order Information || Make payment</h5>
					    <div class="separator mb-3"></div>

		    			<div class="row mb-4">
				        	<div class="col-md-6">
				 				<div class="form-group">
				                    <table class="table table-bordered mb-4">
				                    	
				                        <tr class="text-center">
				                            <td>Order Date</td>
				                            <td class="text-right"><?=date_format(date_create($orderd->order_date),'M-d-Y')?></td>
				                        </tr>

				                        <tr class="text-center">
				                            <td>Order Id</td>
				                            <td class="text-right"><?=$orderd->order_id?></td>
				                        </tr>

				                        <tr class="text-center">
				                            <td>Sub Total</td>
				                            <td class="text-right"><?=$cmp_info[0]->currency;?> <?=$orderd->subtotal?> </td>
				                        </tr>
				                        <tr class="text-center">
				                            <td>Sales Tax(<?=$orderd->state_tax?>%)</td>
				                            <td class="text-right"><?=$cmp_info[0]->currency;?> <?=($orderd->subtotal/100)*$orderd->state_tax;?> </td>
				                        </tr>

				                        <tr class="text-center">
				                            <td>Installation Charge</td>
				                            <td class="text-right"><?=$cmp_info[0]->currency;?> <?=$orderd->installation_charge?> </td>
				                        </tr>

				                        <tr class="text-center">
				                            <td>Other Charge</td>
				                            <td class="text-right"><?=$cmp_info[0]->currency;?> <?=$orderd->other_charge?> </td>
				                        </tr>

				                        <?php if(isset($shipping->track_number) && $shipping->track_number!=NULL){ ?>

				                            <tr class="text-center">
				                                <td>Tracking Number (<?=$shipping->method_name?>) </td>
				                                <td class="text-right"> <?=$shipping->track_number?> </td>
				                            </tr>
				                        <?php } ?>
				                        
				                        <?php if(isset($shipping->shipping_charges) && $shipping->shipping_charges!=NULL){ ?>

				                            <tr class="text-center">
				                                <td>Shipping Charge</td>
				                                <td class="text-right"><?=$cmp_info[0]->currency;?> <?=$shipping->shipping_charges?> </td>
				                            </tr>

				                        <?php } ?>


				                        <tr class="text-center">
				                            <td>Misc</td>
				                            <td class="text-right"><?=$cmp_info[0]->currency;?> <?=$orderd->misc?> </td>
				                        </tr>

				                        <tr class="text-center">
				                            <td>Discount</td>
				                            <td class="text-right"> <?=$cmp_info[0]->currency;?> <?=$orderd->invoice_discount?></td>
				                        </tr>
				                        
				                        <tr class="text-center" style="background: #ddd;">
				                            <td>Grand Total</td>
				                            <td class="text-right" ><?=$cmp_info[0]->currency;?> <?=$orderd->grand_total?> 
				                            </td>
				                        </tr>

				                        <tr class="text-center text-success" >
				                            <td>Paid amount</td>
				                            <td class="text-right" ><?=$cmp_info[0]->currency;?> <?=$orderd->paid_amount;?> 
				                            </td>
				                        </tr>

				                        <tr class="text-center text-danger">
				                            <td>Due amount</td>
				                            <td class="text-right" ><?=$cmp_info[0]->currency;?> <?=$orderd->due;?> 
				                            </td>
				                        </tr>				                        				                        

				                    </table>
				                </div>
				            </div>

			            	<?php 
		                        if($orderd->due>0){
		                    ?>
		                    
				            <div class="col-md-6">

				            	<div class="form-group">

				            		<?php echo form_open('c_level/make_payment/order_payment_update',array('id' => 'MyForm','onSubmit'=>'return PymentValidation()')) ?>

					                    <table class="table table-bordered ">

					                        <input type="hidden" value="<?=$orderd->grand_total?>" name="grand_total" id="grand_total">
					                        <input type="hidden" value="<?=@$cusstomer->customer_no?>" name="customer_no">
					                        <input type="hidden" value="<?=@$cmp_info[0]->user_id?>" name="customer_id">
					                        <input type="hidden" value="<?=$orderd->order_id?>" name="order_id">
					                        <input type="hidden" value="<?=$orderd->clevel_order_id?>" name="clevel_order_id">
					                        <input type="hidden" value="<?=$orderd->due;?>" id="due_amm">

					                       
					                        <?php 
					                        	if($orderd->paid_amount==0){
					                        		$pval = sprintf ("%.2f", ($orderd->grand_total)/2 );
					                        	} else{
					                        		$pval = sprintf ("%.2f", $orderd->due);
					                        	}
					                        ?>

					                        <tr>
					                            <td>Paid Amount (<?=$cmp_info[0]->currency?>)</td>
					                            <td><input type="decimal" name="paid_amount" onkeyup="calDuePaid()" value="<?=$pval;?>" id="paid_amount" placeholder="0" class="form-control text-right" required=""></td>
					                        </tr>

					                        <tr>
					                            <td>Due (<?=$cmp_info[0]->currency?>)</td>
					                            <td><input type="text" name="due" id="due" class="form-control text-right" value=""  readonly=""></td>
					                        </tr>

					                        <tr>
					                            <td>Payment method</td>
					                            <td>
					                                <select name="payment_method" class="form-control" onchange="setCard(this.value)">
					                                    <option value="cash">Cash</option>
					                                    <option value="card">Card</option>
					                                    <option value="check">Check</option>
					                                    <option value="paypal">Paypal</option>
					                                </select>
					                            </td>
					                        </tr>

					                    </table>


					                    <div class="card_method" style="display: none;">

									    <div id="paymentForm">

				                            <ul>
				                                <li>
				                                    <input type="text" name="card_number" placeholder="1234 5678 9012 3456" id="card_number">
				                                    <small class="help">This demo supports Visa, Visa Electron, Maestro, MasterCard and Discover.</small>
				                                </li>

				                                <li class="vertical">

				                                    <ul>
				                                        <li>
				                                            <label for="expiry_month">Expiry month</label>
				                                            <input type="text" name="expiry_month" placeholder="MM" maxlength="2" id="expiry_month">
				                                        </li>
				                                        <li style="margin-left: 5px;">
				                                            <label for="expiry_year">Expiry year</label>
				                                            <input type="text" name="expiry_year" placeholder="YY" maxlength="2" id="expiry_year">
				                                        </li>
				                                        <li>
				                                            <label for="cvv">CVV</label>
				                                            <input type="text" name="cvv" minlength="3" maxlength="4" id="cvv">
				                                        </li>
				                                    </ul>
				                                </li>

				                                <li>
				                                    <label for="name_on_card">Name on card</label>
				                                    <input type="text" name="card_holder_name" placeholder="Name on card"  id="name_on_card">
				                                </li>
				                            </ul>

				                        </div>

					                    </div>


					                    <div class="check_method" style="display: none;">

					                    	<table class="table table-bordered ">

						                    	<tr id="check_area">
						                            <td>Check Number</td>
						                            <td><input type="text" name="check_number" id="check_number" class="form-control "></td>
						                        </tr>

						                        <tr id="check_area2">
						                            <td>Check Image</td>
						                            <td><input type="file" name="check_image" id="check_image" class="form-control "></td>
						                        </tr>
						                    </table>
					                    </div>



					                    <input type="hidden" name="order_status" id="order_status">

					                    <div class="col-lg-6 offset-lg-6 text-right" style="margin-top: 10px">
					                        <button type="submit" class="btn btn-warning" id="gqi" >Pay / Invoice</button>
					                    </div>

				                    <?php echo form_close();?>
				                </div>
				            </div>
				            <?php } ?>

				        </div>
				    </div>
		        </div>
	        </div>


            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
				    <div class="card-body">
				        <h5>Order Details</h5>
				        <div class="separator mb-3"></div>
				        <div class="px-3" >
				            <table class="datatable2 table table-bordered mb-4">
				            	
				                <thead>
				                    <tr>
				                        <th>SL No.</th>
				                        <th>Name of Product</th>
				                        <th>Qty</th>
				                        <th>List Price</th>
				                        <th>Discount(%)</th>
				                        <th>Price</th>
				                        <th>Notes</th>
				                    </tr>
				                </thead>

				                <tbody>

					                <?php $sl = 1; ?>

					                <?php foreach ($order_details as $items):

					                	$width_fraction = $this->db->where('id',$items->width_fraction_id)->get('width_height_fractions')->row();
                                        $height_fraction = $this->db->where('id',$items->height_fraction_id)->get('width_height_fractions')->row();
                                            
					                ?>
					                    <tr>

					                        <td><?=$sl++?></td>

					                        <td>
                                                <strong><?=$items->product_name;?></strong>,  
                                                <?php 
                                                if($items->pattern_name){
                                                    echo $items->pattern_name.'<br/>';
                                                }
                                                ?>
                                                W <?=$items->width;?> <?=@$width_fraction->fraction_value?>, 
                                                H <?=$items->height;?> <?=@$height_fraction->fraction_value?>, 
                                                <?=$items->color_number;?> 
                                                <?=$items->color_name;?>
					                    	</td>

					                        <td><?=$items->product_qty;?></td>
					                        <td><?=$cmp_info[0]->currency;?><?=$items->list_price;?></td>
					                        <td><?=$items->discount;?></td>
					                        <td><?=$cmp_info[0]->currency;?> <?=$items->unit_total_price?> </td>
					                        <td><?=$items->notes?></td>
					                    </tr>

					                <?php endforeach; ?>
				                </tbody>
				            </table>

				        </div>

			        </div>
			    </div>
			</div>
        </div>
    </div>
</main>    


<script type="text/javascript">


	    $(document).ready(function () {
	        //var grand_total = parseFloat($('#grand_total').val());
	        //$('#due').val(grand_total);
	        calDuePaid();
	    });


function PymentValidation(){

    var grand_total = parseFloat($('#grand_total').val());
    var due_amm = parseFloat($('#due_amm').val());

    var paid_amount = parseFloat($('#paid_amount').val());
    var pval = '<?=$pval?>';

    if(grand_total == due_amm){


        if(paid_amount<pval){
        	alert("Can't pay less than "+pval);
        	$('#paid_amount').val(pval);
        	calDuePaid();
            	return false;
        }
        if(paid_amount>grand_total){
        	alert("Can't pay more than "+grand_total);
        	$('#paid_amount').val(pval);
        	calDuePaid();
            	return false;
        }


    } else {

        if(1>paid_amount){
        	alert("Can't pay less than 1");
        	$('#paid_amount').val(pval);
        	calDuePaid();
            	return false;
        }

        if(due_amm<paid_amount){
        	alert("Can't pay more than "+due_amm);
        	$('#paid_amount').val(pval);
        	calDuePaid();
            	return false;
        }

    }
}

// $('#MyForm').on('submit', function() {

   
// });


        function calDuePaid(){

            var grand_total = parseFloat($('#grand_total').val());
            var due_amm = parseFloat($('#due_amm').val());

	        var paid_amount = parseFloat($('#paid_amount').val());
	        var pval = '<?=$pval?>';

        	if(isNaN(paid_amount)) {
				var paid_amount = 0;
			}else{
				var paid_amount = parseFloat($('#paid_amount').val());
			}

            var due = (due_amm-paid_amount);
            $('#due').val(due.toFixed(2));

        }




		$('#card_number').keyup(function()
		{
		    $(this).val(function(i, v)
		    {
		    	if(v.length>19){
		    		var str = v.substring(0,19);
		    		var str = str.replace(/[^\d]/g, '').match(/.{1,4}/g);
			        return str ? str.join('-') : '';
		    	}else{
			        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
			        return v ? v.join('-') : '';
			    }
		    });

		});

        function setCard(value){

            if(value==='card'){

            	$(".card_method").slideToggle();
                $('#check_number').val('');
                $('#check_image').val('');


        		var submit_url = "<?php echo base_url(); ?>"+"c_level/setting_controller/getCard/";

			    $.ajax({
			        url: submit_url,
			        type: 'post',
			        success: function (data) {

			            var obj = jQuery.parseJSON(data);

	                    $("#card_number").val(obj.card_number);
	                    $("#expiry_month").val(obj.expiry_month);
	                    $("#expiry_year").val(obj.expiry_year);
	                    $("#name_on_card").val(obj.card_holder);

	                    $('#card_number').trigger('keyup');

			           
			        },error: function() {

			        }

			    });


                $("#MyForm").attr("action", '<?=base_url()?>c_level/tmspayment_gateway');

                $('.check_method').slideUp();


            }else if(value==='cash'){

                $("#MyForm").attr("action", '<?=base_url()?>c_level/make_payment/order_payment_update');

            	$('#card_number').val('');
            	$('#card_holder_name').val('');
            	$('#expiry_date').val('');
            	$('#cvv').val('');

            	$('#check_number').val('');
                $('#check_image').val('');

                $('.card_method').slideUp();
                $('.check_method').slideUp();


            }else if(value==='check'){
            	$("#MyForm").attr("action", '<?=base_url()?>c_level/make_payment/order_payment_update');

            	$(".check_method").slideToggle();

            	$('#card_number').val('');
            	$('#card_holder_name').val('');
            	$('#expiry_date').val('');
            	$('#cvv').val('');
            	
                $('.card_method').slideUp();

            }else if(value==='paypal'){

            	$("#MyForm").attr("action", '<?=base_url()?>c_level/make_payment/order_payment_update');

            	$('#card_number').val('');
            	$('#card_holder_name').val('');
            	$('#expiry_date').val('');
            	$('#cvv').val('');

            	$('#check_number').val('');
                $('#check_image').val('');

                $('.card_method').slideUp();
                $('.check_method').slideUp();

            }

        }




        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);   
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);   
        });


</script>

<script src="<?php echo base_url();?>assets/c_level/card/creditCardValidator.js"></script>

<script>
function cardFormValidate(){
    var cardValid = 0;
      
    //card number validation
    $('#card_number').validateCreditCard(function(result){ console.log(result);

        var cardType = (result.card_type == null)?'':result.card_type.name;
        if(cardType == 'visa'){
            var backPosition = result.valid?'2px -163px, 260px -87px':'2px -163px, 260px -61px';
        }else if(cardType == 'visa_electron'){
            var backPosition = result.valid?'2px -205px, 260px -87px':'2px -163px, 260px -61px';
        }else if(cardType == 'masterCard'){
            var backPosition = result.valid?'2px -247px, 260px -87px':'2px -247px, 260px -61px';
        }else if(cardType == 'maestro'){
            var backPosition = result.valid?'2px -289px, 260px -87px':'2px -289px, 260px -61px';
        }else if(cardType == 'discover'){
            var backPosition = result.valid?'2px -331px, 260px -87px':'2px -331px, 260px -61px';
        }else if(cardType == 'amex'){
            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
        }else{
            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
        }

        $('#card_number').css("background-position", backPosition);
        if(result.valid){
            $("#card_type").val(cardType);
            $("#card_number").removeClass('required');
            cardValid = 1;
        }else{
            $("#card_type").val('');
            $("#card_number").addClass('required');
            cardValid = 0;
        }
    });
      
    //card details validation
    var cardName = $("#name_on_card").val();
    var expMonth = $("#expiry_month").val();
    var expYear = $("#expiry_year").val();
    
    var cvv = $("#cvv").val();
    var regName = /^[a-z ,.'-]+$/i;
    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
    var regYear = /^19|20|21|22|23|24|25|26|27|28|29|30|31$/;
    var regCVV = /^[0-9]{3,4}$/;
    if (cardValid == 0) {
        $("#card_number").addClass('required');
        $("#card_number").focus();
        return false;
    }else if (!regMonth.test(expMonth)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").addClass('required');
        $("#expiry_month").focus();
        return false;
    }else if (!regYear.test(expYear)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").addClass('required');
        $("#expiry_year").focus();
        return false;
    }else if (!regCVV.test(cvv)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").addClass('required');
        $("#cvv").focus();
        return false;
    }else if (!regName.test(cardName)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").addClass('required');
        $("#name_on_card").focus();
        return false;
    }else{
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").removeClass('required');
        $("#cardSubmitBtn").removeAttr('disabled');
        return true;
    }
}



$(document).ready(function() {
    //Demo card numbers
    $('.card-payment .numbers li').wrapInner('<a href="javascript:void(0);"></a>').click(function(e) {
        e.preventDefault();
        $('.card-payment .numbers').slideUp(100);
        cardFormValidate();
        return $('#card_number').val($(this).text()).trigger('input');
    });
    $('body').click(function() {
        return $('.card-payment .numbers').slideUp(100);
    });
    $('#sample-numbers-trigger').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        return $('.card-payment .numbers').slideDown(100);
    });
    
    //Card form validation on input fields
    $('#paymentForm input[type=text]').on('keyup',function(){
        cardFormValidate();
    });
    
    //Submit card form
    $("#cardSubmitBtn").on('click',function(){
        if(cardFormValidate()){
            var card_number = $('#card_number').val();
            var valid_thru = $('#expiry_month').val()+'/'+$('#expiry_year').val();
            var cvv = $('#cvv').val();
            var card_name = $('#name_on_card').val();
            var cardInfo = '<p>Card Number: <span>'+card_number+'</span></p><p>Valid Thru: <span>'+valid_thru+'</span></p><p>CVV: <span>'+cvv+'</span></p><p>Name on Card: <span>'+card_name+'</span></p><p>Status: <span>VALID</span></p>';
            $('.cardInfo').slideDown('slow');
            $('.cardInfo').html(cardInfo);
        }else{
            $('.cardInfo').slideDown('slow');
            $('.cardInfo').html('<p>Wrong card details given, please try again.</p>');
        }
    });
});
</script>
