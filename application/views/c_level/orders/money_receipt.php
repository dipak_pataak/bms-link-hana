<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        
        <title>Money Receipt</title>
        
        <link rel='stylesheet' type='text/css' href='<?=base_url()?>assets/m-receipt/css/style.css' />
        <link rel='stylesheet' type='text/css' href='<?=base_url()?>assets/m-receipt/css/print.css' media="print" />
        <script type='text/javascript' src='<?=base_url()?>assets/m-receipt/js/jquery-1.3.2.min.js'></script>
        <!--<script type='text/javascript' src='js/example.js'></script>-->

    </head>

<body>


<?php $currency = $company_profile[0]->currency; ?>

    <div id="page-wrap">

        <div id="print_div">

        <div id="header">Money Receipt</div>
        
        <div class="row">

            <table width="100%" cellpadding="2" cellspacing="2" border="0">
                
                <tr>

                    <td align="left" style="border:none">

                        <p><?= $company_profile[0]->company_name; ?></p>
                        <p><?= $company_profile[0]->address; ?></p>
                        <p><?= $company_profile[0]->city; ?>, 
                            <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>, <?= $company_profile[0]->country_code; ?></p>
                        <p><?= $company_profile[0]->phone; ?></p>
                        <p><?= $company_profile[0]->email; ?></p>

                    </td>


                    <td align="right" style="border:none">
                        <img src="<?php echo base_url('assets/c_level/uploads/appsettings/') . $company_profile[0]->logo; ?>" width="150px">
                    </td>

                </tr>

            </table>
            
        </div>
        
        
        
        <div style="clear:both"></div>
        
        <div id="customer">

            <div id="customer-title" style="font-size: 14px;"><br>
                <strong>Sold To: </strong><?= $customer->first_name; ?> <?= $customer->last_name; ?>
                    <p><?= $customer->address; ?></p>
                    <p><?= $customer->city; ?>, 
                        <?= $customer->state; ?>, <?= $customer->zip_code; ?>, 
                        <?= $customer->country_code; ?></p>
                    <p><?= $customer->phone; ?></p>
                    <p><?= $customer->email; ?></p>

                </div>

            <table id="meta">

                <tr>
                    <td class="meta-head">Order #</td>
                    <td><?= $orderd->order_id ?></td>
                </tr>

                <tr>

                    <td class="meta-head">Date</td>
                    <td><?= date_format(date_create(@$orderd->order_date),'M-d-Y') ?></td>
                </tr>

                <tr>
                    <td class="meta-head">Grand Total</td>
                    <td><?= $currency; ?><?= @$orderd->grand_total ?></td>
                </tr>
                
                <tr>
                    <td class="meta-head">Amount Due</td>
                    <td><?= $currency; ?><?= @$orderd->due ?></td>
                </tr>

            </table>
        
        </div>
        
        <table id="items">
        
          <tr>
              <th>Description</th>
              <th>Payment Method</th>
              <th>Date</th>
              <th align="right">Amount</th>
          </tr>
          
          <tr class="item-row">
              
              <td class="description">Payment Received for Order# <?= $orderd->order_id ?> with thanks.</td>
              <td><?=@$payment->payment_method?></td>
              <td align="center"><?= date_format(date_create(@$payment->payment_date),'M-d-Y') ?></td>
              <td align="right"><?= $currency; ?><?=@$payment->paid_amount?></td>
          </tr>
        
        </table>
        
        <div id="terms">
          <h5>&nbsp;</h5>
          This is a computer generated receipt. No signature required.<br />
        Please contact <a href="http://bmslink.net">BMSLink</a> for further details.
        </div>

    </div>

        <button type="button" onclick="printDiv('print_div')" style="float: right;">Print</button>

    </div>
    

    <script type="text/javascript">

        function printDiv(divName) {

            $('.noprint').hide();
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            // document.body.style.marginTop="-45px";
            window.print();
            document.body.innerHTML = originalContents;
            
        }

    </script>


</body>

</html>