<style type="text/css">
    .col-sm-3 + .col-sm-3 br+.col-sm-12 .form-control {
        margin-left: 26%;
        margin-bottom: 1rem;
    }
    .col-sm-3 + .col-sm-3 br+.col-sm-12 input+.form-control {
        margin-left: 0!important;
    }
    .form-group {
        margin: 0px;
    }
    .form-group.col-md-12 .row div.col-sm-4, .form-group.col-md-12 .row div.col-sm-6 {
        padding-left: 0px;
    }
    .col-sm-3, label.col-sm-3 {
        line-height: 36px;
    }
    .row {
        margin-bottom: 1rem;
        clear:both;
    }
    div.col-sm-3 + div.col-sm-3 {
        margin:0 !important;
    }
    br {
        display:none;
    }
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
                top: 958.55px !important;
            left: 188px;
            z-index: 10;
            display: block;
        }
    
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }


    
</style>

<!-- add customer css-->
<style>
    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-container{
        width: inherit !important;
        left: inherit  !important;
        top: inherit  !important;
        display: block  !important;
    }
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
</style>
<!-- add customer css end-->
<main id="orderPage">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Order</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Add new Order </h5>
                        <div class="separator mb-5"></div>
                        
                        <div class="row sticky_container">
                            <div class="col-sm-8">
                                <?= form_open('c_level/order_controller/add_to_cart', array('id' => 'AddToCart')) ?>
                                <div class="form-row">
                                    
                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Category</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2-single" name="category_id" id="category_id"  required="" data-placeholder="--select one --">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($get_category as $category) {
                                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="subcategory_id">
                                        
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Product</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2-single" name="product_id" id="product_id" onchange="getAttribute(this.value)" required="" data-placeholder="-- select one --">

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="color_model"></div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Width</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()" onKeyup="loadPStyle()" min="0"  required="" >
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                                    <option value="">--Select one--</option>
                                                    <?php
                                                    foreach ($fractions as $f) {
                                                        echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="col-sm-3">Height</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()" onKeyup="loadPStyle()" min="0" required="" >
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                                    <option value="">--Select one--</option>
                                                    <?php
                                                        foreach ($fractions as $f) {
                                                            echo "<option value='$f->id'>$f->fraction_value</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="ssssttt14"></div>

                                    <div class="form-group col-md-12">
                                    </div>

                                    <!-- atributs area -->
                                    <div class="form-group col-md-12" id="attr">
                                        
                                    </div>
                                    <!-- End atributs area -->


                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Room</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2-single" name="room" id="room"  required="" data-placeholder="--select one --">
                                                    <option value=""></option>
                                                    <?php
                                                        foreach ($rooms as $r) {
                                                            echo "<option value='$r->room_name'>$r->room_name</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <lable class="col-sm-3">Note</lable>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" name="notes" rows="6"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-9 mb-0">
                                        <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn" >Add product to cart</button>
                                    </div>

                                </div>
                                <input type="hidden" name="total_price" id="total_price">
                                <?= form_close(); ?>
                            </div>


                            <div class="col-sm-4 sticky_item">
                                <div class="fixed_item">
                                    <p id="tprice">Total Price = <?= $currencys[0]->currency; ?> 0</p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>


            <?= form_open_multipart('c_level/order_controller/save_order', array('id' => '')) ?>

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        
                        <h5 class="mb-4">Customer Info</h5>
                        <div class="separator mb-5"></div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                                    <div class="col-sm-8">
                                        <p><input type="text" name="orderid" id="orderid" class="form-control" readonly></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Date</label>
                                    <div class="col-sm-8">
                                        <p>
                                            <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">

                                <div class="row m-0">
                                    <label for="customer_id" class="col-form-label col-sm-4">Select Customer</label>
                                    <div class="col-sm-8">
                                        <!--<input type="text" class="form-control">-->
                                        <select class="form-control select2-single" name="customer_id" id="customer_id" required data-placeholder="-- select one --">
                                            <option value=""></option>
                                            <?php
                                            foreach ($get_customer as $customer) {
                                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                            }
                                            ?>
                                        </select>

                                        <input type="hidden" name="customertype" id="customertype" value="">
                                        
                                        <a class="btn btn-xs btn-success quick_add_customer" onclick="addCustomer()" style="margin-top: 10px;">Add Customer</a>

                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group col-md-6">

                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Side Mark</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="side_mark" name="side_mark" readonly>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group col-md-6" >
                                <div class="row m-0 ship_addr" style="display: none;">
                                    <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="mapLocation" class="form-control" onkeyup="getSalesTax(this.value)">
                                        <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <div class="row m-0">
                                    <div class="col-sm-8 offset-sm-4 mb-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="shipaddress">
                                            <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                    <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" name="file_upload" id="file_upload">
                                        <p>Extension: pdf|doc|docx|xls|xlsx. File size: 2MB</p>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group col-lg-6">
                                <div class="row m-0">
                                    <div class="col-sm-8 offset-sm-4 mb-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="synk_status" value="1" id="synk_status">
                                            <label class="custom-control-label" for="synk_status">You want order to <?=$binfo->company_name;?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <h5>Order Details</h5>
                        <div class="separator mb-3"></div>

                        <div class="" id="cartItems">
                            <table class="datatable2 table table-bordered table-hover">

                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>Name of Product Include Specifications</th>
                                        <th >Qty</th>
                                        <th>List Amount</th>
                                        <th>Discount Amount (%) </th>
                                        <th>Price</th>
                                        <th>Comments</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $i = 1; ?>

                                    <?php foreach ($this->cart->contents() as $items): ?>

                                    <input type="hidden" name="attributes[]" value='<?php echo $items['att_options']; ?>'>
                                    <input type="hidden" name="category_id[]" value='<?php echo $items['category_id']; ?>'>
                                    <input type="hidden" name="pattern_model_id[]" value='<?php echo $items['pattern_model_id']; ?>'>
                                    <input type="hidden" name="color_id[]" value='<?php echo $items['color_id']; ?>'>
                                    <input type="hidden" name="width[]" value='<?php echo $items['width']; ?>'>
                                    <input type="hidden" name="height[]" value='<?php echo $items['height']; ?>'>
                                    <input type="hidden" name="width_fraction_id[]" value='<?php echo $items['width_fraction_id']; ?>'>
                                    <input type="hidden" name="height_fraction_id[]" value='<?php echo $items['height_fraction_id']; ?>'>
                                    <input type="hidden" name="notes[]" value='<?php echo $items['notes']; ?>'>

                                    <input type="hidden" name="row_status[]" value="">
                                    <input type="hidden" name="room[]" value="<?php echo $items['room']; ?>">

                                    <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                                    <tr>

                                        <td><?= $i ?></td>

                                        <td>
                                            <?= $items['name'] ?>
                                            <input type="hidden" name="product_id[]" id="product_id_<?= $i ?>" class="product_id" value="<?= $items['product_id'] ?>">
                                        </td>

                                        <td style="width: 150px;">
                                            <div id="field1">
                                                <button type="button" id="sub" class="sub" >-</button>
                                                <input type="number" name="qty[]"  onchange="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" min="1" class="qty_input" style="width: 40px;">
                                                <button type="button" id="add" class="add">+</button>
                                            </div>
                                            <input type="hidden" value="<?= $i ?>">
                                        </td>
                                        
                                        <td><input type="number" name="list_price[]" value="<?= $items['price'] ?>" id="list_price_<?= $i; ?>" readonly="" class="form-control text-right"></td>
                                        <td><input type="number" name="discount[]" value="" readonly="" id="discount_<?= $i ?>" class="form-control text-right"></td>
                                        <td><input type="number" name="utprice[]" value="" id="utprice_<?= $i; ?>" class="form-control utprice text-right" readonly="" ></td>
                                        <td><?= $items['notes']; ?></td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="deleteCartItem('<?= $items['rowid'] ?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="glyph-icon simple-icon-trash"></i></a>
                                            <!-- <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></button> -->
                                        </td>

                                    </tr>

                                    <?php $i++; ?>

                                <?php endforeach; ?>



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-lg-5 offset-lg-7">

                <div class="card mb-4">

                    <div class="card-body">

                        <table class="table table-bordered mb-4">

                            <tr>
                                <td>Sub Total (<?= $currencys[0]->currency; ?>)</td>
                                <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                            </tr>

                            <tr>
                                <td id='tax_text'>Sales Tax (%)</td>
                                <td>
                                    <input type="hidden" name="tax" onchange="calculetsPrice()" onclick="calculetsPrice()" id="tax" value="0" class="form-control text-right" readonly="">
                                    <input type="text"  id="tax_val" value="0" class="form-control text-right" readonly="">
                                </td>
                            </tr>

                            <tr>
                                <td>Installation Charge (<?= $currencys[0]->currency; ?>)</td>
                                <td><input type="number" name="install_charge" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" id="install_charge" step="any" class="form-control text-right"></td>
            
                            </tr>

                            <tr>
                                <td>Other Charge (<?= $currencys[0]->currency; ?>)</td>
                                <td><input type="number" name="other_charge" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" id="other_charge" step="any"  class="form-control text-right"></td>
                            </tr>

                            <tr>
                                <td>Misc (<?= $currencys[0]->currency; ?>)</td>
                                <td><input type="number" name="misc" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" id="misc" min="0" step="any"  class="form-control text-right"></td>
                            </tr>

                            <tr>
                                <td>Discount (<?= $currencys[0]->currency; ?>)</td>
                                <td><input type="decimal" name="invoice_discount" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" step="any"   id="invoice_discount" class="form-control text-right"></td>
                            </tr>

                            <tr>
                                <td>Grand Total (<?= $currencys[0]->currency; ?>)</td>
                                <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly="" required></td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>

            <input type="hidden" name="order_status" id="order_status">

            <div class="col-lg-6 offset-lg-6 text-right">

                <button type="submit" class="btn btn-success" id="gq" >Submit</button>
                <a href="<?= base_url(); ?>c_level/order_controller/clear_cart" class="btn btn-danger" id="clearCart"> Clear All</a>
            
            </div>


            <?= form_close() ?>
        </div>
    </div>
    <?php $this->load->view($orderjs) ?>
</main>


<!-- Add customer Modal -->
<div id="Add-New-Customer-Modal" class="modal fade" role="dialog" style="overflow-y: scroll; height: 800px;">
    <div class="modal-dialog modal-xl">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" >

                <script src="//code.jquery.com/jquery.min.js"></script>
                <div class="card-body">

                    <form id="add-customer-form" action="<?php echo base_url(); ?>customer-save" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="first_name">First Name <span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John"  onkeyup="required_validation()" required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="last_name">Last Name <span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" onkeyup="required_validation()" required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email <span class="text-danger"> * </span></label>
                                <input type="email" class="form-control" id="email" onkeyup="check_email_keyup()" name="email" placeholder="johndoe@yahoo.com">
                                <span id="error"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <table class="" id="normalItem" style="margin-top: 25px;">
                                    <thead>
            <!--                            <tr>
                                            <th class="text-center">Phone</th>
                                            <th class="text-center">Action </th>
                                        </tr>-->
                                    </thead>
                                    <tbody id="addItem">
                                        <tr>
                                            <td>
                                                <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                                                    <option value="">Type</option>
                                                    <option value="Phone">Phone</option>
                                                    <option value="Fax">Fax</option>
                                                    <option value="Mobile">Mobile</option>
                                                </select>
                                                <input id="phone_1" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" onkeyup="special_character(1)" style="">
                                            </td>
                                            <td class="text-left">
                                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                <a style="font-size: 20px;" class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="add-item" class=" btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 5px 15px 15px;"><span class="simple-icon-plus"></span></button>
                            </div>
                            <!--                                <div class="form-group col-md-6">
                                                                <label for="phone">Mobile No <span class="text-danger"> * </span></label>
                                                                <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX" onkeyup="required_validation()" required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="work_phone">Work Phone</label>
                                                                <input type="text" class="form-control phone" id="work_phone" name="work_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="home_phone">Home Phone</label>
                                                                <input type="text" class="form-control phone" id="home_phone" name="home_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                            </div>-->
                            <div class="form-group col-md-6">
                                <label for="address">Address <span class="text-danger"> * </span></label>


                                <input type="text" class="form-control" id="address" name="address" placeholder=""  required autocomplete="off">

                            </div>
                            <div class="form-group col-md-6">
                                <label for="company">Company </label>
                                <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="state">State</label>
                                <input type="text" class="form-control" id="state" name="state">
                            </div>
                            <!--                                <div class="form-group col-md-4">
                                                                <label for="state">State</label>
                                                                <select class="form-control select2" id="state" name="state">
                                                                    <option value="">-- select one -- </option>
                            <?php
                            foreach ($get_states as $states) {
                                echo "<option value='$states->state_id'>$states->state_name</option>";
                            }
                            ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="city">City</label>
                                                                <input type="text" class="form-control" id="city" name="city" placeholder="Ariana">
                                                                <select class="form-control select2" id="city" name="city" data-placeholder="-- select one --">
                                                                    <option value="">-- select one --</option>
                                                                </select>
                                                            </div>-->
                            <div class="form-group col-md-3">
                                <label for="zip_code">Zip</label>
                                <input type="text" class="form-control" id="zip_code" name="zip_code">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="country_code">Country Code</label>
                                <input type="text" class="form-control" id="country_code" name="country_code">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="reference">Referred By</label>
                                <input type="text" class="form-control" id="reference" name="reference" placeholder="John Clark">
                            </div>
                            <div class="form-group col-md-6">
                                <table class="" id="normalItem_file"  style="margin-bottom: 5px; margin-top: 25px;">
                <!--                    <thead>
                                        <tr>
                                            <th class="text-center">Files</th>
                                            <th class="text-center">Action </th>
                                        </tr>
                                    </thead>-->
                                    <tbody id="addItem_file">
                                        <tr id="1">
                                            <td>
                                                <input id="file_upload_1" class="form-control" type="file" name="file_upload[]" multiple>
                                            </td>
                                            <td class="text-left" width="25%">
                                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                <a style="font-size: 20px;" class="text-danger" value="Delete" onclick="file_deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table> 
                                <button id="add-item" class="btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" type="button" style="margin: 0px 15px 15px;"><span class="simple-icon-plus"></span></button>
                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
                            </div> 
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn">Add</button>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

$(document).on('submit', '#myForm', function(e){

    e.preventDefault();

    var formData = $(this).serialize();
   
});


    
 

    function addCustomer() {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        console.log(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            console.log('place changed');
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;
                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip_code").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
        });
        $('#Add-New-Customer-Modal').modal('show');
        $('#Add-New-Customer-Modal .customer_btn').prop('disabled', false);
        $('#add-customer-form').submit(function (event) {
            var formData = $(this).serialize();
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/customer_save_new",
                data: formData,
            })
            .done(function (response) {
                if (response.msg == 'success') {
                    //  $('#Add-New-Customer-Modal').modal();
                    $('#Add-New-Customer-Modal').modal('hide');
                    toastr.success("Customer info save successfully!");
                    $.ajax({
                        type: 'GET',
                        url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_customers_opt",
                    })
                            .done(function (data) {
                                $('#customer_id').html('');
                                $('#customer_id').html(data);
                                $('select#customer_id').val(response.id);
                                $('select#customer_id').change();
                            });
                } else {
                    toastr.alert("Customer not saved successfully!");
                }
            });
            event.preventDefault();
        });
    }


    function special_character(t) {
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            alert(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }
    $('#address').on('keyup', function () {
      if($(this).parent().find('pac-container') > 0){}else{
        var pacContainer = $('.pac-container');
        $('#address').parent().append(pacContainer);
         }
    });
    
    
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '' && $("#address").val() != '') {
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }


    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "<?php echo base_url(); ?>get-check-c-customer-unique-email",
            type: "post",
            data: data_string,
            success: function (data) {
                if (data != 0) {
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }
</script>