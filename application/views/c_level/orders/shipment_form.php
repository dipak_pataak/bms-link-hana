<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                        <?php
                        $message = $this->session->flashdata('message');
                        
                        if ($message != '') {
                            echo $message;
                        }
                        ?>

                    <div class="mb-3">
                        <h1>Shipment info</h1>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Shipment info</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="separator mb-5"></div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xl-12 mb-4">
                <?=form_open_multipart('c_level/order_controller/shipping',array('id'=>''))?>

                <div class="card mb-4">
                    <div class="card-body">
                        
                            <div class="form-group col-md-12">
                                <div class="row m-0">
                                    <label for="order_date" class="col-form-label col-sm-2">Date</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="order_date" id="order_date" class="form-control" value="<?=$orderd->order_date ?>" readonly>
                                    </div>
                                </div>
                            </div>
                      

                            <div class="form-group col-md-12">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-2">Order Id</label>
                                    <div class="col-sm-8">
                                        <p><input type="text" name="orderid" id="orderid" value="<?=$orderd->order_id?>" class="form-control" readonly></p>
                                    </div>
                                </div>
                            </div>

                        

                        <div class="form-group col-md-12">
                            <div class="row m-0">
                                <label for="customer_id" class="col-form-label col-sm-2">Customer</label>
                                <div class="col-sm-8" id="custo">
                                    <input type="text"  class="form-control" value="<?=$orderd->customer_name?>" readonly>
                                </div>
                            </div>
                        </div>

                        <?php if($orderd->is_different_shipping!=0){?>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="orderid" class="col-form-label col-sm-2">Shipping Address</label>
                                
                                <div class="col-sm-8">
                                    
                                    <input type="text" id="address" name="shippin_address" value="<?=$orderd->different_shipping_address?>" class="form-control" placeholder="" required>
                                    <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                                    
                                </div>
                            </div>
                        </div>

                        <?php } else{ ?>

                            <div class="form-group col-md-12">
                                <div class="row m-0 ">
                                    <label for="orderid" class="col-form-label col-sm-2">Shipping Address</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="address"  name="shippin_address" value="<?=$orderd->address?>, <?=$orderd->city?> , <?=$orderd->state?> , <?=$orderd->zip_code?>, <?=$orderd->country_code?>" placeholder="" required>
                                        <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                                    </div>
                                </div>
                            </div>
                            <!-- <input type="hidden" id="address" name="shippin_address" value="<?=$orderd->address?>" class="form-control" > -->

                        <?php } ?>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="Length" class="col-form-label col-sm-2">Ship by <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="ship_method" onchange="setMethod(this.value)" required>
                                        <option value="">--Select--</option>
                                        <?php foreach($methods as $val){?>
                                            <option value="<?=$val->id;?>"><?=$val->method_name;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>


                    <div id="dp_cp" style="display: none;">

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="delivery_date" class="col-form-label col-sm-2">Date of Delivery <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" id="delivery_date" name="delivery_date" class="form-control datepicker" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="delivery_charge" class="col-form-label col-sm-2">Delivery Charge </label>
                                <div class="col-sm-8">
                                    <input type="text" id="delivery_charge" name="delivery_charge" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="comment" class="col-form-label col-sm-2">Comment </label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="up" style="display: none;">
                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="Length" class="col-form-label col-sm-2">Length <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="number" id="length" name="length" class="form-control" required="">
                                 
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label for="weight" class="col-form-label col-sm-2">Weight <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="number" id="weight" name="weight" class="form-control" required="">
                                 
                                </div>
                            </div>
                        </div>

                        <?php 
                            $service = array(
                                '01' => 'UPS Next Day Air',
                                '02' => 'UPS 2nd Day Air',
                                '03' => 'UPS Ground',
                                '07' => 'UPS Worldwide Express',
                                '08' => 'UPS Worldwide Expedited',
                                '11' => 'UPS Standard',
                                '12' => 'UPS 3 Day Select',
                                '13' => 'UPS Next Day Air Saver',
                                '14' => 'UPS Next Day Air Early A.M.',
                                '54' => 'UPS Worldwide Express Plus',
                                '59' => 'UPS 2nd Day Air AM',
                                '65' => 'UPS World Wide Saver'
                            );
                        ?>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <label class="col-form-label col-sm-2">Service Type <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <select class="form-control " name="service_type" id="service_type"required>
                                        <?php
                                            foreach ($service as $key=> $val) {
                                                echo "<option value='$key' ".($method->service_type==$key?'selected':'')." >$val</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>



                    </div>

                        <div class="form-group col-md-12">
                            <div class="row m-0 ">
                                <div class="col-sm-2"></div>

                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-primary mb-0">Submit</button>
                                </div>

                                <!-- <div class="col-sm-2">
                                    <a href="<?=base_url('c_level/invoice_receipt/receipt/')?><?=$orderd->order_id?>" class="btn btn-warning mb-0">Not required</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <?=form_close()?>
            </div>
            </div>
        </div>
    </main>


<script>
    
    function setMethod(v){

        if(v==2 || v==3){

            $("#length").removeAttr('required');
            $("#weight").removeAttr('required');
            $("#delivery_date").prop('required',true);

            $("#dp_cp").slideDown();
            $('#up').slideUp();
        }

        if(v==1){

            $("#length").prop('required',true);
            $("#weight").prop('required',true);
            $("#delivery_date").removeAttr('required');
            $("#up").slideDown();
            $('#dp_cp').slideUp();
        }
            
    }



    function generate() {
        customerFrm.password.value = randomPassword(customerFrm.length.value);
    }

//    ============ close generate password =============
//    =============== its for google place address geocomplete ===============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });
    });


</script>