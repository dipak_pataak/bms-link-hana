<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>BMS Link</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/iconsmind/style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/simple-line-icons/css/simple-line-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-float-label.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/main.css" />
    </head>
    <script type="text/javascript">
        var mybase_url = '<?php echo base_url(); ?>';
    </script>
    <style type="text/css">
        .fixed-background {
            background: none !important;
            /*background: url(../img/balloon.jpg) no-repeat center center fixed;*/
            background-size: cover;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <body class="background show-spinner">
        <div class="fixed-background"></div>
        <main>
            <div class="container">
                <div class="row h-100">
                    <div class="col-12 col-md-6 mx-auto my-auto">
                        <div class="card auth-card">
                            <div class="form-side">
                                <a href="" class="mb-5 text-center d-block">
                                    <img src="<?php echo base_url(); ?>assets/c_level/img/logo.png" alt="">
                                </a>
                                <div class="">
                                    <?php
                                    $exception = $this->session->flashdata('exception');
                                    if ($exception)
                                        echo $exception;
                                    ?>

                                    <?php
                                    $message = $this->session->flashdata('message');
                                    if ($message)
                                        echo $message;
                                    ?>
                                </div>
                                <h6 class="mb-4">Forgot Password</h6>
                                <form action="<?php echo base_url(); ?>c-level-forgot-password-send" method="post">
                                    <label class="form-group has-float-label mb-4">
                                        <input class="form-control" type="text" name="email" placeholder="Enter your valid mail" tabindex="1" required />
                                        <span>E-mail</span>
                                    </label>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a href="<?php echo base_url(); ?>c-level" class="btn btn-success" tabindex="3">Back</a>
                                        <input type="submit" class="btn btn-primary btn-lg btn-shadow" tabindex="2" value="Send">
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/c_level/js/dore.script.js"></script>
        <script src="<?php echo base_url(); ?>assets/c_level/js/scripts.js"></script>
    </body>
</html>