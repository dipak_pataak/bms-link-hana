

<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/Chart.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/chartjs-plugin-datalabels.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/perfect-scrollbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/progressbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery.barrating.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/select2.full.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/nouislider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/Sortable.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/mousetrap.min.js"></script>
<!--========== its for chart of account tree ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/treeview.js"></script>
<!--=========== its for close ================-->
<!--=========== its for new quotation ================-->
<script src="<?php echo base_url(); ?>assets/c_level/plugins/theia-sticky-sidebar-master/dist/theia-sticky-sidebar.min.js"></script>
<!--=========== its for new quotation close ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/dore.script.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/scripts.js"></script>
<!--=========== its for add customer phone format .js ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/phoneformat.js"></script>
<!--=========== its for add customer phone format .js close ================-->
<!-- date time picker -->
<script src="<?php echo base_url() ?>assets/c_level/js/bootstrap-datetimepicker.js"></script>
 <!--=========== its ckeditor start =============-->
        <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
        <script src="<?php echo base_url(); ?>assets/ckeditor/samples/js/sample.js"></script>
        <!--=========== its ckeditor close=============-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/b_level/toster/toastr.min.js"></script>
<!--=========== its for multi select ============-->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/bootstrap-select.min.js"></script>


<?php

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');

        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }


    $app = $this->db->select('*,COUNT(appointment_date)as title')
                ->from('appointment_calendar')
                ->where('c_level_id', $level_id)
                ->group_by('appointment_date')
                ->get()->result();

    $r = '';
    foreach ($app as $key => $value) {
        $r .= "'" . $value->appointment_date . "',";
    }

?>

<script type="text/javascript">

    $("form :input").attr("autocomplete", "off");
        // ================ Date picker =============
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            showOn: "focus",
        });
    
    //=========== close datepicker =============
    $("main").on('click', function () {
        $("#app-container").addClass("menu-sub-hidden sub-hidden");
    });




    $(".date-inline").datepicker('update', <?php echo $r; ?>);

    if ($().fullCalendar) {

        var testEvent = new Date(new Date().setHours(new Date().getHours()));
        var day = testEvent.getDate();
        var month = testEvent.getMonth() + 1;
        $(".calendar_app").fullCalendar({
            themeSystem: "bootstrap4",
            height: "auto",
            buttonText: {
                today: "Today",
                month: "Month",
                week: "Week",
                day: "Day",
                list: "List"
            },
            bootstrapFontAwesome: {
                prev: " simple-icon-arrow-left",
                next: " simple-icon-arrow-right",
                prevYear: "simple-icon-control-start",
                nextYear: "simple-icon-control-end"
            },
            eventSources: [
                {
                    color: '#37a000',
                    events: function (start, end, timezone, callback) {

                        $.ajax({
                            url: '<?php echo base_url() ?>c_level/Appointment_controller/get_appointment_info',
                            dataType: 'json',
                            data: {
                                // our hypothetical feed requires UNIX timestamps
                                start: start.unix(),
                                end: end.unix()
                            },
                            success: function (msg) {
                                var newevents = msg.events;
                                callback(newevents);

                            }
                        });
                    }
                },
            ],


            eventClick: function (event, jsEvent, view) {
                var mydate = event.mydate;

                var dataString = 'mydate=' + mydate;

                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() ?>c_level/Appointment_controller/get_appointment_info_by_date/' + mydate,
                    success: function (msg) {
                        $('#showTr').html(msg);
                        $('#appointmentListBydate').modal("show");

                    }
                });

            }

        });
    }

  function field_reset() {
        $(".first_name").val('');
        $(".sidemark").val('');
        $(".phone").val('');
        $(".address").val('');
        $(".order_id").val('');
        $(".customer_id").val('');
        $(".order_date").val('');
        $(".order_stage").val('');
        $('.select2-single').select2();
    }

</script>




<script type="text/javascript">

    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: '', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }

    jQuery('.lang-select').click(function() {
      var theLang = jQuery(this).attr('data-lang');
      //alert(theLang);
      jQuery('.goog-te-combo').val(theLang);

      //alert(jQuery(this).attr('href'));
      window.location = jQuery(this).attr('href');
      location.reload();

    });
  </script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>

</html>
