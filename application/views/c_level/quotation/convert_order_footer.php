<div class="col-lg-12 mb-4 footer_section">
    <?= form_open_multipart('c_level/order_controller/save_order', array('id' => 'save_order_form')) ?>


    <div class="card mb-4">
        <div class="card-body">

            <h5 class="mb-4">Customer Info</h5>
            <div class="separator mb-5"></div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                        <div class="col-sm-8">
                            <p><input type="text" name="orderid" id="orderid" value="<?php echo $orderid; ?>" class="form-control" readonly></p>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="orderid" class="col-form-label col-sm-4">Date</label>
                        <div class="col-sm-8">
                            <p>
                                <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                            </p>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6">

                    <div class="row m-0">
                        <label for="customer_id" class="col-form-label col-sm-4">Select Customer</label>
                        <div class="col-sm-8">
                            <!--<input type="text" class="form-control">-->
                            <select class="form-control select2-single" name="customer_id" id="customer_id" required data-placeholder="-- select one --">
                                <option value=""></option>
                                <?php
                                foreach ($get_customer as $customer) {
                                    echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                }
                                ?>
                            </select>

                            <input type="hidden" name="customertype" id="customertype" value="">

                            <a href="<?= base_url('add-customer') ?>" target='_blank' class="btn btn-xs btn-success" style="margin-top: 10px;">Add Customer</a>

                        </div>
                    </div>

                </div>

                <div class="form-group col-md-6">

                    <div class="row m-0">
                        <label for="orderid" class="col-form-label col-sm-4">Side Mark</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="side_mark" name="side_mark" readonly>
                        </div>
                    </div>

                </div>

                <div class="form-group col-md-6" >
                    <div class="row m-0 ship_addr" style="display: none;">
                        <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                        <div class="col-sm-8">
                            <input type="text" id="mapLocation" class="form-control" onkeyup="getSalesTax(this.value)">
                            <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                        </div>
                    </div>
                </div>

                <div class="form-group col-lg-6">
                    <div class="row m-0">
                        <div class="col-sm-8 offset-sm-4 mb-2">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="shipaddress">
                                <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control" name="file_upload" id="file_upload">
                            <p>Extension: pdf|doc|docx|xls|xlsx. File size: 2MB</p>
                        </div>
                    </div>
                </div>


                <div class="form-group col-lg-6">
                    <div class="row m-0">
                        <div class="col-sm-8 offset-sm-4 mb-2">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="synk_status" value="1" id="synk_status">
                                <label class="custom-control-label" for="synk_status">You want order to <?= $binfo->company_name; ?></label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <h5>Order Details</h5>
            <div class="separator mb-3"></div>

            <div class="" id="cartItems">
                <table class="datatable2 table table-bordered table-hover">

                    <thead>
                        <tr>
                            <th>SL#</th>
                            <th>Name of Product Include Specifications</th>
                            <th >Qty</th>
                            <th>List Amount</th>
                            <th>Discount Amount (%) </th>
                            <th>Price</th>
                            <th>Comments</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $i = 1; ?>

                        <?php foreach ($this->cart->contents() as $items): ?>

                        <input type="hidden" name="attributes[]" value='<?php echo $items['att_options']; ?>'>
                        <input type="hidden" name="category_id[]" value='<?php echo $items['category_id']; ?>'>
                        <input type="hidden" name="pattern_model_id[]" value='<?php echo $items['pattern_model_id']; ?>'>
                        <input type="hidden" name="color_id[]" value='<?php echo $items['color_id']; ?>'>
                        <input type="hidden" name="width[]" value='<?php echo $items['width']; ?>'>
                        <input type="hidden" name="height[]" value='<?php echo $items['height']; ?>'>
                        <input type="hidden" name="width_fraction_id[]" value='<?php echo $items['width_fraction_id']; ?>'>
                        <input type="hidden" name="height_fraction_id[]" value='<?php echo $items['height_fraction_id']; ?>'>
                        <input type="hidden" name="notes[]" value='<?php echo $items['notes']; ?>'>

                        <input type="hidden" name="row_status[]" value="">
                        <input type="hidden" name="room[]" value="<?php echo $items['room']; ?>">

                        <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                        <tr>

                            <td><?= $i ?></td>

                            <td>
                                <?= $items['name'] ?>
                                <input type="hidden" name="product_id[]" id="product_id_<?= $i ?>" class="product_id" value="<?= $items['product_id'] ?>">
                            </td>

                            <td style="width: 150px;">
                                <div id="field1">
                                    <button type="button" id="sub" class="sub" >-</button>
                                    <input type="number" name="qty[]"  onchange="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" min="1" class="qty_input" style="width: 40px;">
                                    <button type="button" id="add" class="add">+</button>
                                </div>
                                <input type="hidden" value="<?= $i ?>">
                            </td>

                            <td><input type="number" name="list_price[]" value="<?= $items['price'] ?>" id="list_price_<?= $i; ?>" readonly="" class="form-control text-right"></td>
                            <td><input type="number" name="discount[]" value="" readonly="" id="discount_<?= $i ?>" class="form-control text-right"></td>
                            <td><input type="number" name="utprice[]" value="" id="utprice_<?= $i; ?>" class="form-control utprice text-right" readonly="" ></td>
                            <td><?= $items['notes']; ?></td>
                            <td>
                                <a href="javascript:void(0)" onclick="deleteCartItem('<?= $items['rowid'] ?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="glyph-icon simple-icon-trash"></i></a>
                                <!-- <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></button> -->
                            </td>

                        </tr>

                        <?php $i++; ?>

                    <?php endforeach; ?>



                    </tbody>
                </table>
            </div>

        </div>
    </div>



    <div class="col-lg-5 offset-lg-7">

        <div class="card mb-4">

            <div class="card-body">

                <table class="table table-bordered mb-4">

                    <tr>
                        <td>Sub Total (<?= $currencys[0]->currency; ?>)</td>
                        <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                    </tr>

                    <tr>
                        <td id='tax_text'>Sales Tax (%)</td>
                        <td>
                            <input type="hidden" name="tax" onchange="calculetsPrice()" onclick="calculetsPrice()" id="tax" value="0" class="form-control text-right" readonly="">
                            <input type="text"  id="tax_val" value="0" class="form-control text-right" readonly="">
                        </td>
                    </tr>

                    <tr>
                        <td>Installation Charge (<?= $currencys[0]->currency; ?>)</td>
                        <td><input type="number" name="install_charge" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" id="install_charge" step="any" class="form-control text-right"></td>

                    </tr>

                    <tr>
                        <td>Other Charge (<?= $currencys[0]->currency; ?>)</td>
                        <td><input type="number" name="other_charge" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" id="other_charge" step="any"  class="form-control text-right"></td>
                    </tr>

                    <tr>
                        <td>Misc (<?= $currencys[0]->currency; ?>)</td>
                        <td><input type="number" name="misc" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" id="misc" min="0" step="any"  class="form-control text-right"></td>
                    </tr>

                    <tr>
                        <td>Discount (<?= $currencys[0]->currency; ?>)</td>
                        <td><input type="decimal" name="invoice_discount" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" step="any"   id="invoice_discount" class="form-control text-right"></td>
                    </tr>

                    <tr>
                        <td>Grand Total (<?= $currencys[0]->currency; ?>)</td>
                        <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly="" required></td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

    <input type="hidden" name="order_status" id="order_status">

    <div class="col-lg-6 offset-lg-6 text-right">
        <button type="submit" class="btn btn-success" id="gq">Submit</button>
        <a class="btn btn-danger" id="clearCart" onclick="ClearDataFromCart()">Clear All</a>
    </div>


    <?= form_close() ?>
</div>