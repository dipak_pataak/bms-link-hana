<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 713.391px !important;
        left: 383.5px;
        z-index: 10;
        display: block;
    }
    .right{
        float: right;
    }
    .detail_row{
        width: 1924px;
        overflow-x: scroll;
    }
    .scrollable_width{
        width: 100%  !important;
        overflow-x: scroll;
    }
    .card-body{
        width: 100%;
        overflow-x: scroll;
    }
    .rooms_section{
        width: 100%;

    }
    .add_more_room_btn{
        text-align: center;
    }
    .onek{
        width: 100px;
        display: inline;
    }
    .twok{
        width: 200px;
        display: inline;
    }
    .threek{
        width: 300px;
        display: inline;
    }
    .fivek{
        width: 500px;
        display: inline;
    }
    .sixk{
        width: 500px;
        display: inline;
    }
    .sevenk{
        width: 700px;
        display: inline;
    }
    .in_row{
        display: inline;
    }
    .row_form{
        width: 100%;
        overflow-x: scroll;
    }
    body{
        overflow: scroll;
    }
    .room_details_section{
        width: 2400px;
    }
    .delete_window, .delete_category, .delete_room{
        cursor: pointer;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/add_q.css" />

<main>
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New quotation</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <?php
        $message = $this->session->flashdata('message');

        if ($message != '') {
            echo $message;
        }
        ?>

        <div class="row">

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <?= form_open('c_level/quotation_controller/addToList', array('id' => 'AddToCart')) ?>
                    <div class="scrollable_width">
                        <h5>
                            <span class="left pull-left col-md-2">Roomwise Window Details </span>
                            <!--<div class="form-group col-md-2 right pull-right"> <button type="button" class="btn btn-xs btn-info pull-right right add_room_btn" onclick="">+ Add Room</button></div>-->

                        </h5>


                        <div class="separator mb-5"></div>

                        <fieldset class="rooms_section room_0">
                            <legend>Room 1</legend>
                            <div class="row room_details_section room_section_0">

                                <div class="form-group col-md-2">
                                    <label>Room</label>
                                    <select class="form-control select2 twok" name="room_name[]" id="room_name" onchange="reload()" required="" data-placeholder='-- select Room Type --'>
                                        <option value="">--Select room type--</option>
                                        <?php
                                        foreach ($rooms as $room) {
                                            echo "<option value='$room->room_name'>$room->room_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>No</label>
                                    <input type="number" name="no_of_windows[0]" class="form-control twok no_of_windows" value="0" data-room_no="0" placeholder="No of Windows" min="0" max="100"> 
                                </div>
                                <div class="height_width_sec_0 col-md-2">


                                </div>
                                <div class="form-group col-md-1 add_category_section_0"> <label>&nbsp;</label>
                                    <button type="button" class="btn btn-xs btn-success pull-right right add_category_btn" data-room_no="0" >+ Add Cat.</button>
                                </div>

                            </div>
                            <!--<span class="delete_room">X</span>-->
                        </fieldset>
                        <div class="row col-md-12 add_more_room_btn">
                            <button type="button" class="btn btn-xs center add_room_btn" data-room_no="1" >+ Add Room</button>
                        </div>

                        <div class="row form-group col-md-12 mb-0">
                            <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn123" autocomplete="off">Add To List</button>
                        </div>

                    </div>
                </div>
                <br/>
            </div>
            <?= form_close() ?>
        </div>
    </div>




    <?= form_open('c_level/quotation_controller/save_quotation', array('id' => '', 'class' => 'col-lg-12 mb-4')) ?>

    <div class="">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4">Customer Info</h5>
                <div class="separator mb-5"></div>

                <div class="form-row">

                    <div class="form-group col-md-12">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-2">Date</label>

                            <p>
                                <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                            </p>

                        </div>
                    </div>

                    <div class="form-group col-md-12">


                        <label for="customer_id" class="col-form-label col-sm-2">Select Customer</label>


            <!--<input type="text" class="form-control">-->
                        <select class="form-control select2-single" name="customer_id" id="customer_id" required data-placeholder="-- select one --">
                            <option value=""></option>
                            <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                            }
                            ?>
                        </select>

                        <input type="hidden" name="customertype" id="customertype" value="">




                    </div>

                </div>


                <h5>Order Details</h5>

                <div class="separator mb-3"></div>

                <div id="cartItems">

                    <table class="datatable2 table table-bordered table-hover">

                        <thead>
                            <tr>
                                <th>SL#</th>
                                <th>Room Name</th>
                                <th>WXH</th>
                                <th>Category</th>
                                <th>Product</th>
                                <th>Upcharge</th>
                                <th>List Amount</th>
                                <th>Discount Amount (%) </th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($this->cart->contents() as $items): ?>
                                <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>

                            <input type="hidden" name="category_id[]" id="category_id_<?= $i ?>" class="category_id" value='<?= $items['category_id'] ?>'>
                            <input type="hidden" name="room_name[]" id="room_name_<?= $i ?>" class="room_name" value='<?= $items['name'] ?>'>
                            <input type="hidden" name="product_data[]" id="product_data_<?= $i ?>" class="product_data" value='<?= $items['product_data'] ?>'>
                            <input type="hidden" name="upchargeData[]" id="upchargeData_<?= $i ?>" class="upchargeData" value='<?= $items['upchargeData'] ?>'>

                            <tr>
                                <td><?= $i ?></td>
                                <td>
                                    <?= $items['name'] ?>
                                    <input type="hidden" name="product_id[]" id="product_id_<?= $i ?>" class="product_id" value="<?= $items['product_id'] ?>">
                                </td>

                                <td>
                                    <?= $items['category_name'] ?>
                                    <input type="hidden" name="qty[]"  onchange="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" min="1" class="qty_input"> 
                                </td>

                                <td><input type="number" name="room_price[]" value="<?= $items['room_price'] ?>" readonly="" id="room_price_<?= $i ?>" class="form-control text-right"></td>
                                <td><input type="number" name="upcharge[]" value="<?= $items['upcharge'] ?>" readonly="" id="upcharge_<?= $i ?>" class="form-control text-right"></td>
                                <td><input type="number" name="list_price[]" value="<?= $items['price'] ?>" id="list_price_<?= $i; ?>" readonly="" class="form-control text-right"></td>
                                <td><input type="number" name="discount[]" value="0" id="discount_<?= $i ?>" onchange="setDiscount(<?= $i; ?>)" onclick="setDiscount(<?= $i; ?>)" class="form-control text-right"></td>
                                <td><input type="number" name="utprice[]" value="<?= $items['price'] ?>" id="utprice_<?= $i; ?>" class="form-control utprice text-right" readonly="" ></td>

                                <td>
                                    <a href="javascript:void(0)" onclick="deleteCartItem('<?= $items['rowid'] ?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="glyph-icon simple-icon-trash"></i></a>
                                    <!-- <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></button> -->
                                </td>

                            </tr>

                            <?php $i++; ?>

                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>


    <div class="col-lg-5 offset-lg-7">

        <div class="card mb-4">

            <div class="card-body">

                <table class="table table-bordered mb-4">

                    <tr>
                        <td>Sub Total (<?= @$company_profile[0]->currency; ?>)</td>
                        <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                    </tr>

                    <tr>
                        <td>Sales Tax (%)</td>
                        <td><input type="text" name="tax" onchange="calculetsPrice()" onclick="calculetsPrice()" id="tax" value="0" class="form-control text-right" readonly=""></td>
                    </tr>

                    <tr>
                        <td>Grand Total (<?= @$company_profile[0]->currency; ?>)</td>
                        <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly="" required></td>
                    </tr>

                </table>

            </div>

        </div>

    </div>


    <div class="col-lg-4 offset-lg-8 text-right">
        <button type="submit" class="btn btn-danger" id="clearCart" >Clear data</button>
        <button type="submit" class="btn btn-success" id="gq" >Submit</button>
    </div>

    <?php echo form_close(); ?>
    <div class="page_modals">

    </div>

</div>
</div>
</main>



<?php $this->load->view($qtjs) ?>

<script>
    function getPattern(room, window, cat_sec_no) {
        var windows = $('.container-fluid .room_section_' + room + ' .no_of_windows').val();
        var product_id = $('#product_id_' + room + '_' + window + '_' + cat_sec_no).val();
        var pattern = $('#pattern_id_' + +room + '_' + window + '_' + cat_sec_no);
        $.ajax({

            url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_color_partan_model/" + product_id,
            type: 'get',
            success: function (r) {
                var obj = jQuery.parseJSON(r);
                if (window == '0') {
                    for (var j = 0; j < windows; j++) {
                        var window_no = j;
                        //  $('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val(product_id);
                        if ($('#pattern_id_' + +room + '_' + window_no + '_' + cat_sec_no).val() == '') {
                            $('#pattern_id_' + +room + '_' + window_no + '_' + cat_sec_no).html(obj.pattern);
                        }
                    }
                } else {
                    $('#pattern_id_' + +room + '_' + window + '_' + cat_sec_no).html(obj.pattern);
                }
                $('#product_price_' + room + '_' + window).val(obj.price);
            }
        });

        var html = '';
        // duplicate product drop down when first drop down chnaged, and when other are null
        if (window == '0') {
            for (var j = 0; j < windows; j++) {
                var window_no = j;
                if ($('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val() == '') {
                    $('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val(product_id);
                }
            }
        }

        for (var j = 0; j < windows; j++) {
            var window_no = j;
            priceCalculate(room, window_no, cat_sec_no);
        }
    }
    function priceCalculate(room, window, cat_sec_no) {
        var windows = $('.container-fluid .room_section_' + room + ' .no_of_windows').val();
        console.log('no of windows ' + windows);
        if (window == '0') {
            console.log('window is 0');
            for (var j = 1; j < windows; j++) {
                var window_no = j;
                //  $('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val(product_id);
                if ($('#pattern_id_' + room + '_' + window_no + '_' + cat_sec_no).val() == '' && window_no) {
                    $('#pattern_id_' + room + '_' + window_no + '_' + cat_sec_no).val($('#pattern_id_' + room + '_0_' + cat_sec_no).val());

                    priceCalculate(room, window_no, cat_sec_no);

                }
            }
        } else {
            console.log('window not 0');
        }


        var product_id = $('#product_id_' + room + '_' + window + '_' + cat_sec_no).val();
        var patter_id = $('#pattern_id_' + room + '_' + window + '_' + cat_sec_no).val();
        var width = $('#width_' + room + '_' + window).val();
        var height = $('#height_' + +room + '_' + window).val();
        var product_price = $('#product_price_' + room + '_' + window).val();
        var windows = $('.container-fluid .room_section_' + room + ' .no_of_windows').val();

        $.ajax({
            url: "<?php echo base_url(); ?>c_level/Quotation_controller/price_call/" + width + "/" + height + "/" + product_id + "/" + patter_id,
            type: 'get',
            success: function (r) {
                var obj = jQuery.parseJSON(r);
                $('#price_' + room + '_' + window + '_' + cat_sec_no).val(obj.price);
            }

        }
        );
    }


    $(document).ready(function () {


        $('.container-fluid').on('click', '.add_room_btn', function () {
            var room_no = $(this).data('room_no');

            var this_btn = $(this);
            if (room_no > 10) {
                alert('Exeeded max room count!');
            } else {
                var x = confirm('You want to add one more room ?');
                if (x) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>c_level/Quotation_controller/getMoreRoom',
                        dataType: 'JSON',
                        type: 'POST',
                        data: {
                            room_no: room_no,
                        },
                        success: function (data) {
                            $('.delete_room').hide();
                            this_btn.data('room_no', data.room_cnt_ro);
                            $(data.html).insertBefore('.add_more_room_btn');
                            $('.room_' + data.room_no + ' .delete_room').show();
                        },
                        error: function (err) {
                            alert('there was some error ' + err);
                        }
                    });
                }
            }
        });
        $('.container-fluid').on('click', '.remove_item', function () {
            var room_no = $(this).data('room_no');
            var window_no = $(this).data('window_no');
            $('.detail_row_' + room_no + '').remove();
        });
        $('.container-fluid').on('click', '.add_window_btn', function () {
            var room_cnt = $(this).data('room_cnt');
            var tot_windows = $('.room_' + room_cnt + ' .no_of_window_div').children().length;
            //  console.log('.room_' + room_cnt + ' .no_of_window_div');
            if (tot_windows > 10) {
                alert('Exeeded max window count!');
            } else {
                var x = confirm('You want to add one more window ?');
                if (x) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>c_level/Quotation_controller/getWindowRow',
                        dataType: 'JSON',
                        type: 'POST',
                        data: {
                            room_cnt: room_cnt,
                            window_cnt: tot_windows
                        },
                        success: function (data) {
                            $('.room_' + room_cnt + ' .no_of_window_div').append(data.html);
                        },
                        error: function (err) {
                            alert('there was some error ' + err);
                        }
                    });
                }
            }
        });
        $('.container-fluid').on('change', '.category_dp', function () {
//            var room_no = $(this).data('room_no');
            var cat_sec_no = $(this).data('cat_sec_no');
            var category_id = $(this).val();
            var category_name = $(this).children("option:selected").text();

            var room_no = $(this).data('room_no');
            var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
            $('.room_' + room_no).find('.no_of_windows').attr('disabled', 'true');
            $('.category_data_' + room_no + '_' + cat_sec_no).html('');
            var html = '';
            for (var j = 0; j < windows; j++) {
                var window_no = j;
                html += '<div class="row window_cat_' + room_no + '_' + window_no + '_' + cat_sec_no + '">';
                html += '<div class="col-sm-3"><select name="product_id[ ' + room_no + '][' + window_no + ']" class="form-control select2 threek product_id" id="product_id_' + room_no + '_' + window_no + '_' + cat_sec_no + '" onchange="getPattern(' + room_no + ',' + window_no + ',' + cat_sec_no + ')" required><option value="">--Select Product--</option>';
                html += '</select></div>';
                html += '<div class="col-sm-3"><select class="form-control select2 threek" id="pattern_id_' + room_no + '_' + window_no + '_' + cat_sec_no + '" onchange="priceCalculate(' + room_no + ',' + window_no + ',' + cat_sec_no + ')"><option value="">--Select Pattern--</option>';
                html += '</select></div>';
                html += '<div class="col-sm-3"><input type="text" id="price_' + room_no + '_' + window_no + '_' + cat_sec_no + '" value="0" class="form-control price onek" readonly></div><div class="col-sm-3"><span class="upcharge_btn" data-toggle="modal" data-target="#myModal_' + room_no + '_' + window_no + '_' + cat_sec_no + '">Upcharge</span></div></div>';
            }
            $('.category_data_' + room_no + '_' + cat_sec_no).html(html);
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_product_by_category/" + category_id,
                type: 'get',
                success: function (r) {
                    for (var j = 0; j < windows; j++) {
                        var window_no = j;
                        $("#product_id_" + room_no + '_' + window_no + '_' + cat_sec_no).html(r);
                    }
                }
            });
//get upchardes modal
            for (var j = 0; j < windows; j++) {
                var window_no = j;
                if ($('#myModal_' + room_no + '_' + window_no + '_' + cat_sec_no).length) {
                    $('#myModal_' + room_no + '_' + window_no + '_' + cat_sec_no).remove();
                }
                var upChargeModal = '<div id="myModal_' + room_no + '_' + window_no + '_' + cat_sec_no + '" class="modal fade" role="dialog"><div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"><h4 class="modal-title">Upcharges</h4><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body">';
                upChargeModal += '<p class=" modal_sec_' + room_no + '_' + window_no + '_' + cat_sec_no + '">Some text in the modal.</p>';
                upChargeModal += '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';

                $('.page_modals').append(upChargeModal);

            }
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/getUpcharge",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    category_name: category_name,
                    room_no: room_no,
                    window_no: window_no,
                    category_id: category_id,
                    cat_sec_no: cat_sec_no
                },
                success: function (r) {
                    for (var j = 0; j < windows; j++) {
                        var window_no = j;
                        console.log(r.html);
                        $('.modal_sec_' + room_no + '_' + window_no + '_' + cat_sec_no).html(r.html);
                    }
                }
            });

        });
        $('.container-fluid').on('change', '.no_of_windows', function () {
            var room_no = $(this).data('room_no');
            //  console.log('changed');
            //var window_no = $(this).data('window_no');
            var windows_count = $(this).val();
            var tot_windows = $('.container-fluid .height_width_sec_' + room_no).children().length;
            if (windows_count > tot_windows) {
                // add window height width
                //     console.log('if');
                var window_no = windows_count - 1;
                var html = '<div class="window_section row window_no_' + room_no + '_' + window_no + '"> <div class="col-sm-5"><label>Height</label><input type="number" id="height_' + room_no + '_' + window_no + '" name="height[' + room_no + '][' + window_no + ']" data-window_no="' + window_no + '" data-room_no="' + room_no + '"  placeholder="Enter Height" class="form - control twok in_row height_' + room_no + '_' + window_no + ' height_input" min="0" max="100"></div> <div class="col-sm-5"><label>Width</label><input type="number"  data-window_no="' + window_no + '" data-room_no="' + room_no + '"  class="form - control twok in_row width_' + room_no + '_' + window_no + ' width_input" name="width[' + room_no + '][' + window_no + ']" id="width_' + room_no + '_' + window_no + '" placeholder="Enter Width"  min="0" max="100"></div><div class="col-sm-2"><label>&nbsp;</label><label  data-room_no="' + room_no + '" data-window_no="' + window_no + '" class="delete_row delete_window">x</label></div></div>';
                $('.container-fluid .height_width_sec_' + room_no).append(html);
                $('.delete_window').hide();
                //   console.log('.container-fluid .window_no_' + room_no + '_' + window_no + ' .delete_window');
                $('.container-fluid .window_no_' + room_no + '_' + window_no + ' .delete_window').show();

            } else {
                //  console.log('else');
                // remove last window
                $('.delete_window').hide();
                $('.container-fluid .height_width_sec_' + room_no).children().last().remove();
                var win_show = windows_count - 1;

                $('.container-fluid .window_no_' + room_no + '_' + win_show).find('.delete_window').show();
            }

//update price when widdth height change
// find no of section, traverse through current room with all windows and all cat sections

            var cat_sec = $('.container-fluid .category_details_sec').length;


        });
        $('.container-fluid').on('click', '.delete_window', function () {
            //  console.log('clicked');
            var window_no = $(this).data('window_no');
            var room_no = $(this).data('room_no');
            if (window_no > 0) {
                var x = confirm('Delete the window no ' + window_no + '?');
                if (x) {
                    $('.window_no_' + room_no + '_' + window_no).remove();
                    $('.window_cat_' + room_no + '_' + window_no).remove();
                    $("input[name='no_of_windows[" + room_no + "]']").val(parseInt($("input[name='no_of_windows[" + room_no + "]']").val() - 1));
                    $('.delete_window').hide();
                    var show_btn_no = window_no - 1;
                    if (show_btn_no > 0) {
                        $('.container-fluid .window_no_' + room_no + '_' + show_btn_no).find('.delete_window').show();
                    }
                }

                // delete window from categories too
                // get current room and find how much category in that room
                var no_of_cats = $('.room_section_' + room_no).find('.category_details_sec').length;
                for (var k = 1; k <= no_of_cats; k++) {

                    $('.container-fluid .window_cat_' + room_no + '_' + window_no + '_' + k).remove();
                }

            }
        });
        $('.container-fluid').on('click', '.delete_category', function () {
            var cat_sec_no = $(this).data('cat_sec_no');
            var room_no = $(this).data('room_no');
            var x = confirm('Delete the category ?');
            if (x) {
                $('.category_details_' + room_no + '_' + cat_sec_no).remove();
            }
        });
        $('.container-fluid').on('click', '.delete_room', function () {
            var room_no = $(this).data('room_no');
            var x = confirm('Delete the room ?');
            if (x) {
                $('.room_' + room_no).remove();
                $('.add_room_btn').data('room_no', $('.add_room_btn').data('room_no') - 1);
                $('.delete_room').hide();
                var r_no = parseInt(room_no) - 1;
                console.log('.room_' + r_no + ' .delete_room');
                $('.room_' + r_no + ' .delete_room').show();

            }
        });
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/Quotation_controller/getCategoryDP",
            type: 'get',
            success: function (r) {
                $('.category_dp').html(r);
            }
        });
        $('.container-fluid').on('click', '.add_category_btn', function () {

            var room_no = $(this).data('room_no');
            var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
            var category_sections = $('.container-fluid .category_details_sec').length;
            var cat_sec_no = category_sections + 1;
            var html = '<div class="col-md-4 category_details_sec sevenk category_details_' + room_no + '_' + cat_sec_no + '"><div class="select_category"><select name="category_id[' + room_no + '][]" class="form-control select2 sevenk category_dp category_dp_sec_' + cat_sec_no + '" data-room_no="' + room_no + '" required="" data-cat_sec_no="' + cat_sec_no + '"><option value="">--Select category--</option></select> <i class="simple-icon-close delete_category" data-room_no="' + room_no + '" title="Delete category" data-cat_sec_no="' + cat_sec_no + '"></i></div><div class="row sevenk category_data_' + room_no + '_' + cat_sec_no + '"></div></div>';
            $(html).insertBefore($('.add_category_section_' + room_no));
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/getCategoryDP",
                type: 'get',
                success: function (r) {
                    $('.container-fluid .category_dp_sec_' + cat_sec_no).html(r);
                }
            });
        });
    });
    // document.addEventListener('contextmenu', event => event.preventDefault());

</script>
