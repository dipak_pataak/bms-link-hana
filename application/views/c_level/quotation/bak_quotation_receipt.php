<main>
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Quotation View</li>
                        </ol>
                    </nav>
                    <button type="button" class="btn btn-success pull-right" onclick="printContent('printableArea')" >Print</button>
                </div>
                <div class="separator mb-5"> </div>
                
            </div>
        </div>

        <div class="row" id="printableArea">

            <div class="col-lg-5 col-md-5 col-sm-5">
                <img src="<?php echo base_url('assets/c_level/uploads/appsettings/').$company_profile[0]->logo;?>">
            </div>

            <div class="col-lg-7 col-md-7 col-sm-7" >
                <h2 style="margin-top: 100px;">Quotation Comparison</h2>
            </div>


            <div class="col-lg-5 col-md-5 col-sm-5" >

                <div class="card mb-4">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <td>Date</td>
                                <td><?=date_format(date_create($quotation->created_date),'Y-M-d');?></td>
                            </tr>
                            <tr>
                                <td>Customer name</td>
                                <td><?=$quotation->customer_name;?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>




            <div class="col-lg-12 mb-4">

                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Details</h5>
                        <div class="separator mb-5"></div>

                        <table class=" table table-bordered table-hover ">

                            <thead>

                                <tr> 
                                    <th >SL</th> 
                                    <th> Room </th> 
                                    <th colspan="2"> Window size </th> 

                                    <?php 
                                        $catadata = [];
                                        foreach ($cat as $key => $val) {
                                        $catadata[] = $val->category_id;

                                    ?>
                                    <th colspan="3"><?=$val->category_name?></th> 
                                    <?php } ?>

                                </tr>

                                <tr>
                                    <th>SL#</th>
                                    <th>Room</th>

                                    <th>Width</th>
                                    <th>Height</th>

                                    <?php 
                                        foreach ($cat as $key => $val) {

                                    ?>

                                    <th>Product </th>
                                    <th>Pattern</th>
                                    <th>Price</th>

                                    <?php } ?>

                                </tr>

                            </thead>


                            <tbody>

                                <?php 

                                    foreach ($room as $key => $value) {

                                    $dd= $this->db->where('quote_id',$value->quote_id)->where('room_name',$value->room_name)->where('category_id',$value->category_id)->get('quote_comparison_details')->row();
                                     
                                    $pdata = json_decode($dd->product_data);

                                ?>

                                <tr>
                                    <td>1</td>
                                    <td><?=$value->room_name;?></td>

                                    <td colspan="2" style="padding: 0; margin: 0; ">

                                        <table style=" width: 100%; border-right: none; height: 100% ">
                                            <?php foreach ($pdata as $key => $wh) { ?>
                                                <tr>
                                                    <td><?=$wh->width;?></td>
                                                    <td><?=$wh->height;?></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        
                                    </td>

                                    

                                    <?php 
                                        foreach ($catadata as $key => $cc) {
                                       
                                        $ssss = $this->db->where('category_id',$cc)->where('quote_id',$value->quote_id)->where('room_name',$value->room_name)->get('quote_comparison_details')->row();
                                        $pdata = json_decode($ssss->product_data);
                                    ?>


                                        <td colspan="3" style="padding: 0; margin: 0; ">
                                         
                                            <table style=" width: 100%; border-right: none; ">
                                                                                
                                            <?php

                                                foreach ($pdata as $key => $fa) {

                                                    $p = $this->db->select('product_name')->where('product_id',$fa->product_id)->get('product_tbl')->row();
                                                    $pt = $this->db->select('pattern_name')->where('pattern_model_id',$fa->patter_id)->get('pattern_model_tbl')->row();
                                            
                                            ?>

                                                <tr>
                                                    <td><?=$p->product_name?></td>
                                                    <td><?=(@$pt->pattern_name?$pt->pattern_name:'--')?></td>
                                                    <td><?=$fa->price;?></td>
                                                </tr>

                                             <?php  } ?>

                                            </table>
                                        </td>

                                    <?php } ?>

                                </tr>
                                
                            <?php } ?>


                            </tbody>



                                    <tfoot>
                                        
                                        <tr>
                                            <td colspan="4"  class="text-right"></td>
                                            <?php 
                                            $catadata = [];
                                            foreach ($cat as $key => $val) {

                                                $fo = $this->db->select('SUM(quote_price)as total, 
                                                    SUM(product_price) as tota_product_price,
                                                    SUM(subtotal)as total_subtotal,
                                                    SUM(upcharge)as total_upcharge,
                                                    SUM(discount)as total_discount')
                                                ->where('category_id',$val->category_id)
                                                ->where('quote_id',$val->quote_id)
                                                ->get('quote_comparison_details')->row();

                                                $dis = $fo->total_subtotal-$fo->total;

                                            ?>

                                            <td  colspan="3" class="text-right">
                                            <p>Product Price : <?=number_format($fo->tota_product_price,2,'.','')?></p> 
                                            <p>Upcharge : <?=number_format($fo->total_upcharge,2,'.','')?></p> 
                                            <p>Subtotal : <?=number_format($fo->total_subtotal,2,'.','')?></p> 
                                            <p>Discount : <?=number_format($dis,2,'.','')?></p> 
                                            <p>Total    : <?=number_format($fo->total,2,'.','')?></p> 
                                                
                                            </td>
                                            <?php } ?>

                                            
                                        </tr>
                                    </tfoot>                             

                        </table>
       

                    </div>
                </div>

            </div>

            <div class="col-lg-5 col-md-5 col-sm-5 offset-lg-7 offset-md-7 offset-sm-7">
                
                <div class="card mb-4">
                    
                    <div class="card-body">
                    
                        <table class="table table-bordered mb-4">

                            <tr>
                                <td>Sub Total (<?=@$company_profile[0]->currency;?>)</td>
                                <td><?=number_format($quotation->sub_total,2,'.','');?></td>
                            </tr>

                            <tr>
                                <td>Sales Tax (%)</td>
                                <td><?=number_format($quotation->sales_tax,2,'.','');?></td>
                            </tr>

                            <tr>
                                <td>Grand Total (<?=@$company_profile[0]->currency;?>)</td>
                                <td><?=number_format($quotation->grand_total,2,'.','');?></td>
                            </tr>

                        </table>

                    </div>

                </div>

            </div>

            <hr/>

            <div class="col-lg-12" style="border-top:1px solid #000; text-align: center;">
                <p><?=$company_profile[0]->company_name;?>, Address: <?=$company_profile[0]->address;?>, <?=$company_profile[0]->city;?>, 
                <?=$company_profile[0]->state;?>, <?=$company_profile[0]->zip_code;?>, 
                <?=$company_profile[0]->country_code;?>,
                Phone: <?=$company_profile[0]->phone;?></p>
                
            </div>



        </div>


    </div>
</main>



<script type="text/javascript">

    function printContent(el){
        var restorepage  = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }

</script>




