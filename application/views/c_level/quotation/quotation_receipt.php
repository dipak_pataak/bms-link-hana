<style>
    table.table_print tr th, table.table_print tr td {
        border-color: #000 !important;
    }

    .leftalign{
        text-align: right !important;
        padding-left: 20% !important;
    }

    .selected-prouct{
        float: left;
        height: 20px;
        width: 20px;
    }

    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }

    .modal .modal-header, .modal .modal-body, .modal .modal-footer {
        padding: 0.5rem !important;
    }

    /*    #Modal-Place-Order input.form-control{
            padding: 5px !important;
        }*/

    .tab-content{
        padding: 5px !important;
    }
    .tab-content main{
        margin: 0px !important; 
    }
    .modal .footer_section{
        overflow-y: scroll;
        height: 300px;
    }
</style>
<main>
    <div class="container-fluid">
        <input type="hidden" name="curQuotId" id="curQuotId" value="<?php echo $quote_id; ?>"/>
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Quotation View</li>
                        </ol>
                    </nav>
                    <button type="button" class="btn btn-success pull-right" onclick="printContent('printableArea')">Print</button>
                    <button type="button" class="btn btn-default pull-right" onclick="mailContent('printableArea')">Send On Mail</button>
                </div>
                <div class="separator mb-5"> </div>

            </div>
        </div>

        <div class="row" id="printableArea" style="background-color: #FFF;">

            <div class="col-lg-5 col-md-5 col-sm-5">
                <img src="<?php echo base_url('assets/c_level/uploads/appsettings/') . $company_profile[0]->logo; ?>">
            </div>

            <div class="col-lg-7 col-md-7 col-sm-7 mb-4 title-center">
                <h2 style="margin-top: 100px;">Quotation Comparison</h2>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-6 left-content">
                <div class="card mb-4">
                    <div class="card-body">
                        <h2><?= $company_profile[0]->company_name; ?>,</h2> 
                        <h4><?= $company_profile[0]->address; ?>, <?= $company_profile[0]->city; ?>,</h4>
                        <h4><?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>, <?= $company_profile[0]->country_code; ?></h4> 
                        <h4><?= $company_profile[0]->phone; ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 offset-lg-1 offset-md-1 right-content">

                <div class="card mb-4">
                    <div class="card-body">
                        <table>
                            <tr>
                                <td style="width: 40%;"><strong>Date</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= date_format(date_create($quotation->qt_order_date), 'Y-M-d'); ?></td>
                            </tr>
                            <tr><td colspan="3">&nbsp;<br/></td></tr>
                            <tr>
                                <td><strong>Customer Name</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->first_name . " " . $quotation->last_name; ?></td>
                            </tr>
                            <?php if ($quotation->company != "") { ?>
                                <tr>
                                    <td><strong>Comopany Name</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= $quotation->company; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><strong>Address</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->street_no; ?>, <?= $quotation->address; ?>, <?= $quotation->city; ?>, <?= $quotation->state; ?>, <?= $quotation->zip_code; ?>, <?= $quotation->country_code; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Phone Number</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->phone; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Email ID</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->email; ?></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>

            <?php $invoiceDetailsArray = []; ?>

            <div class="col-lg-12 mb-4">

                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Details</h5>
                        <div class="separator mb-5"></div>
                        <table class=" table table-bordered table-hover table_print" style="border:1px solid black;">
                            <thead>
                                <tr style="border:1px solid black;"> 
                                    <th style="border:1px solid black;">SL</th> 
                                    <th style="border:1px solid black;">Room</th>
                                    <th style="border:1px solid black;" colspan="2">Window size</th> 
                                    <?php
                                    $catNo = 1;
                                    foreach ($categories as $key => $val) {
                                        if ($val == "Misc") {
                                            echo "<th style='border:1px solid black;' colspan='2'>" . $val . "</th>";
                                        } else {
                                            echo "<th style='border:1px solid black;' colspan='3'>" . $val . "</th>";
                                        }

                                        $invoiceDetailsArray[$val . "-" . $catNo]['subtotal'] = 0;
                                        $invoiceDetailsArray[$val . "-" . $catNo]['discount'] = 0;
                                        $invoiceDetailsArray[$val . "-" . $catNo]['upcharge'] = 0;
                                        $invoiceDetailsArray[$val . "-" . $catNo]['salestax'] = 0;
                                        $invoiceDetailsArray[$val . "-" . $catNo]['grandtotal'] = 0;
                                        $catNo++;
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <th style="border:1px solid black;">SL#</th>
                                    <th style="border:1px solid black;">Room</th>
                                    <th style="border:1px solid black;">Width</th>
                                    <th style="border:1px solid black;">Height</th>
                                    <?php foreach ($categories as $key => $val) { ?>
                                        <th style="border:1px solid black;">Product </th>
                                        <?php if ($val != 'Misc') { ?><th style="border:1px solid black;">Pattern</th><?php } ?>
                                        <th style="border:1px solid black;">Price</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($roomWiseArray as $key => $room) { ?>  
                                    <tr>
                                        <td rowspan="<?php echo sizeof($room); ?>"><?php echo $key + 1; ?></td>
                                        <td rowspan="<?php echo sizeof($room); ?>"><?php echo $room[0]->room_name; ?></td>
                                        <?php
                                        for ($i = 0; $i < sizeof($room); $i++) {
                                            if ($i == 0) {
                                                ?>
                                                <td><?php echo $room[$i]->win_width . "<sup style='color:red !important;'>" . $room[$i]->win_wfraction . "</sup>"; ?></td>
                                                <td><?php echo $room[$i]->win_height . "<sup style='color:red !important;'>" . $room[$i]->win_htfraction . "</sup>" ?></td>
                                                <?php
                                                $roomNo = $room[$i]->qd_id;
                                                $winCategories = json_decode($room[$i]->win_categories);
                                                $newWinCategories = [];
                                                $isPrinted = false;
                                                $catNo = 1;
                                                foreach ($categories as $cat => $catVal) {
                                                    if ($room[$i]->win_categories != null || $room[$i]->win_categories != "") {
                                                        for ($j = 0; $j < sizeof($winCategories); $j++) {
                                                            if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                                                echo "<td><input class='selected-prouct' type='checkbox' val='this value' name='" . $roomNo . "_" . $winCategories[$j]->win_category_name . "_" . $catNo . "'/>" . $winCategories[$j]->win_product_name . "</td>";
                                                                if ($winCategories[$j]->win_category_name != "Misc") {
                                                                    echo "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                                                }
                                                                echo "<td>" . $winCategories[$j]->win_price . "</td>";
                                                                $isPrinted = true;

                                                                if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                                                    $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                                                    $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                                                } else {
                                                                    $fSubTotal = 0;
                                                                }

                                                                if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                                                    $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                                                    $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                                                } else {
                                                                    $fUpcharges = 0;
                                                                }

                                                                if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                                                    $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                                                    $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                                                } else {
                                                                    $fDiscount = 0;
                                                                }

                                                                $fTotal = $fSubTotal + $fUpcharges;

                                                                if ($quotation->qt_tax != 0 || $quotation->qt_tax != "") {
                                                                    $fsalestax = $fTotal * (floatval($quotation->qt_tax) / 100);
                                                                    $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                                                } else {
                                                                    $fsalestax = 0;
                                                                }

                                                                if ($fSubTotal != 0) {
                                                                    $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;
                                                                    $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                                                }
                                                            } else {
                                                                array_push($newWinCategories, $winCategories[$j]);
                                                            }
                                                        }
                                                        if ($isPrinted != true) {
                                                            if ($catVal == 'Misc') {
                                                                echo "<td colspan='2'>&nbsp;</td>";
                                                            } else {
                                                                echo "<td colspan='3'>&nbsp;</td>";
                                                            }
                                                        }
                                                        $winCategories = $newWinCategories;
                                                        $newWinCategories = [];
                                                        $isPrinted = false;
                                                    } else {
                                                        if ($catVal == "Misc") {
                                                            echo "<td colspan='2'>empty</td>";
                                                        } else {
                                                            echo "<td colspan='3'>empty</td>";
                                                        }
                                                    }
                                                    $catNo++;
                                                }
                                            }
                                        }
                                        ?> 
                                    </tr>
                                    <?php
                                    if (sizeof($room) > 0) {
                                        ?><tr><?php
                                            for ($i = 0; $i < sizeof($room); $i++) {
                                                if ($i != 0) {
                                                    ?>                                                    
                                                    <td><?php echo $room[$i]->win_width . "<sup style='color:red !important;'>" . $room[$i]->win_wfraction . "</sup>"; ?></td>
                                                    <td><?php echo $room[$i]->win_height . "<sup style='color:red !important;'>" . $room[$i]->win_htfraction . "</sup>" ?></td>

                                                    <?php
                                                    $roomNo = $room[$i]->qd_id;
                                                    $winCategories = json_decode($room[$i]->win_categories);
                                                    $newWinCategories = [];
                                                    $isPrinted = false;
                                                    $catNo = 1;
                                                    foreach ($categories as $cat => $catVal) {
                                                        if ($room[$i]->win_categories != null || $room[$i]->win_categories != "") {
                                                            for ($j = 0; $j < sizeof($winCategories); $j++) {
                                                                if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                                                    echo "<td><input class='selected-prouct' type='checkbox' val='this value' name='" . $roomNo . "_" . $winCategories[$j]->win_category_name . "_" . $catNo . "'/>" . $winCategories[$j]->win_product_name . "</td>";
                                                                    if ($winCategories[$j]->win_category_name != "Misc") {
                                                                        echo "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                                                    }
                                                                    echo "<td>" . $winCategories[$j]->win_price . "</td>";
                                                                    $isPrinted = true;

                                                                    if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                                                        $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                                                    } else {
                                                                        $fSubTotal = 0;
                                                                    }

                                                                    if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                                                        $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                                                    } else {
                                                                        $fUpcharges = 0;
                                                                    }

                                                                    if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                                                        $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                                                    } else {
                                                                        $fDiscount = 0;
                                                                    }

                                                                    $fTotal = $fSubTotal + $fUpcharges;

                                                                    if ($quotation->qt_tax != 0 || $quotation->qt_tax != "") {
                                                                        $fsalestax = $fTotal * (floatval($quotation->qt_tax) / 100);
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                                                    } else {
                                                                        $fsalestax = 0;
                                                                    }

                                                                    if ($fSubTotal != 0) {
                                                                        $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                                                    }
                                                                } else {
                                                                    array_push($newWinCategories, $winCategories[$j]);
                                                                }
                                                            }
                                                            if ($isPrinted != true) {
                                                                if ($catVal == 'Misc') {
                                                                    echo "<td colspan='2'>&nbsp;</td>";
                                                                } else {
                                                                    echo "<td colspan='3'>&nbsp;</td>";
                                                                }
                                                            }
                                                            $winCategories = $newWinCategories;
                                                            $newWinCategories = [];
                                                            $isPrinted = false;
                                                        } else {
                                                            if ($catVal == "Misc") {
                                                                echo "<td colspan='2'>empty</td>";
                                                            } else {
                                                                echo "<td colspan='3'>empty</td>";
                                                            }
                                                        }
                                                        $catNo++;
                                                    }
                                                }
                                            }
                                            ?></tr><?php
                                    }
                                    ?>                                    
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td colspan="4">Sub Total (<?= @$company_profile[0]->currency; ?>)</td>
                                    <?php
                                    $catNo = 1;
                                    foreach ($categories as $cat => $catVal) {
                                        if ($catVal == "Misc") {
                                            echo "<td style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
                                        } else {
                                            echo "<td colspan='2' style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
                                        }
                                        $catNo++;
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td colspan="4">Total Discount(<?= @$company_profile[0]->currency; ?>)</td>
                                    <?php
                                    $catNo = 1;
                                    foreach ($categories as $cat => $catVal) {
                                        if ($catVal == "Misc") {
                                            echo "<td style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
                                        } else {
                                            echo "<td colspan='2' style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
                                        }
                                        $catNo++;
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td colspan="4">Upcharge</td>
                                    <?php
                                    $catNo = 1;
                                    foreach ($categories as $cat => $catVal) {
                                        if ($catVal == "Misc") {
                                            echo "<td style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
                                        } else {
                                            echo "<td colspan='2' style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
                                        }
                                        $catNo++;
                                    }
                                    ?>
                                </tr>
                                <?php
                                $total = $quotation->qt_subtotal - $quotation->total_discount + $quotation->total_upcharge;
                                ?>
                                <tr>
                                    <td colspan="4">Sales Tax (<?= number_format($quotation->qt_tax, 2, '.', ''); ?>%)</td>
                                    <?php
                                    $catNo = 1;
                                    foreach ($categories as $cat => $catVal) {
                                        if ($catVal == "Misc") {
                                            echo "<td style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
                                        } else {
                                            echo "<td colspan='2' style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
                                        }
                                        $catNo++;
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td colspan="4">Grand Total (<?= @$company_profile[0]->currency; ?>)</td>
                                    <?php
                                    $catNo = 1;
                                    foreach ($categories as $cat => $catVal) {
                                        if ($catVal == "Misc") {
                                            echo "<td style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '') . "</td>";
                                        } else {
                                            echo "<td colspan='2' style='border-right:0;'></td>";
                                            echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '') . "</td>";
                                        }
                                        $catNo++;
                                    }
                                    ?>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <button type="button" class="btn btn-success place-order" onclick="placeOrder();" style="float: right;">Place Order</button>
                <div style="clear: both;"></div>
            </div>

            <hr/>

            <div class="col-lg-12" style="border-top:1px solid #000; text-align: center;">
                <p><?= $company_profile[0]->company_name; ?>, Address: <?= $company_profile[0]->address; ?>, <?= $company_profile[0]->city; ?>, 
                    <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>, 
                    <?= $company_profile[0]->country_code; ?>,
                    Phone: <?= $company_profile[0]->phone; ?></p>
            </div>
        </div>


    </div>
</main>

<!-- Modal -->
<div id="Modal-Place-Order" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Place Order</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <div class="container-fluid">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <!-- dynamic data appear here -->
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" style="min-height: 250px; max-height: 250px; overflow-y: scroll; background-color: #F3F3F3;">
                            <!-- dynamic data appear here -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-md btn-success" onclick="saveOrder()">Save & Proceed</button>
                                <button type="button" class="btn btn-md btn-default" onclick="addMoreCats()">Add More Categories</button>
                                <button type="button" class="btn btn-md btn-danger" onclick="cancelOrder()">Cancel Order</button>
                -->
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="quote_cutomer_id" value="<?php echo $quotation->customer_id; ?>">
<?php $this->load->view($orderjs); ?>

<script type="text/javascript">

    function printContent(el) {
        $('.place-order').remove();
        $('.selected-prouct').remove();
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }

    function mailContent(el) {
        $('.place-order').remove();
        $('.selected-prouct').remove();
        var printcontent = $('#' + el).html();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/Quotation_controller/send_invoice_email') ?>",
            data: {printcontent: printcontent, custemail: "<?php echo $quotation->email; ?>"},
            success: function (res) {
                alert(res);
                location.reload();
            }, error: function () {
                alert('error');
            }
        });
    }

    var catArray = JSON.parse('<?php echo json_encode($categories) ?>');
    var orderData = {};
    var fetchedProductsArray = [];
    function placeOrder() {
        var newCatArray = [];
        for (var i = 0; i < catArray.length; i++) {
            var elementCount = 1;
            var isElementAdded = false;
            while (isElementAdded == false) {
                if (newCatArray.indexOf(catArray[i] + "_" + elementCount) < 0) {
                    newCatArray.push(catArray[i] + "_" + elementCount);
                    isElementAdded = true;
                } else {
                    elementCount++;
                }
            }
        }

        var selectedProductsIndexArray = [];
        var selectedProducts = {};
        $(".selected-prouct").each(function (index) {
            if ($(this).is(":checked")) {
                var productName = $(this).attr('Name');
                var productArray = productName.split("_");
                var catNoFromnewCatArray = parseInt(productArray[2]) - 1;
                var newCatArrayA = newCatArray[catNoFromnewCatArray].split("_");
                var newProductArray = {
                    roomno: productArray[0],
                    catname: productArray[1],
                    catpos: newCatArrayA[1],
                }
                if (fetchedProductsArray.indexOf(productName) < 0) {
                    selectedProducts[productName] = newProductArray;
                    orderData[productName] = {};
                    selectedProductsIndexArray.push(productName);
                }
            }
        });

        if (jQuery.isEmptyObject(selectedProducts)) {
            if (fetchedProductsArray.length <= 0) {
                alert("Please select products for place order!");
            } else {
                showOrderModal();
                console.log(orderData);
            }
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/Quotation_controller/get_product_details') ?>",
                data: selectedProducts,
                success: function (data) {
                    data = JSON.parse(data);
                    for (var i = 0; i < selectedProductsIndexArray.length; i++) {
                        orderData[selectedProductsIndexArray[i]]['win_category_id'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_category'];
                        orderData[selectedProductsIndexArray[i]]['win_category_name'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_category_name'];
                        orderData[selectedProductsIndexArray[i]]['win_product_id'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_product'];
                        orderData[selectedProductsIndexArray[i]]['win_product_name'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_product_name'];
                        orderData[selectedProductsIndexArray[i]]['win_pattern_id'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_pattern'];
                        orderData[selectedProductsIndexArray[i]]['win_pattern_name'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_pattern_name'];
                        orderData[selectedProductsIndexArray[i]]['win_price_amt'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_price'];
                        orderData[selectedProductsIndexArray[i]]['win_upcharges_arr'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_upcharges_array'];
                        orderData[selectedProductsIndexArray[i]]['win_upcharges_amt'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_upcharges'];
                        orderData[selectedProductsIndexArray[i]]['win_discount_amt'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_discount_amt'];
                        orderData[selectedProductsIndexArray[i]]['win_room_name'] = data[selectedProductsIndexArray[i]]['selWinDet']['room_name'];
                        orderData[selectedProductsIndexArray[i]]['win_height'] = data[selectedProductsIndexArray[i]]['selWinDet']['win_height'];
                        orderData[selectedProductsIndexArray[i]]['win_htfraction'] = data[selectedProductsIndexArray[i]]['selWinDet']['win_htfraction'];
                        orderData[selectedProductsIndexArray[i]]['win_width'] = data[selectedProductsIndexArray[i]]['selWinDet']['win_width'];
                        orderData[selectedProductsIndexArray[i]]['win_wfraction'] = data[selectedProductsIndexArray[i]]['selWinDet']['win_wfraction'];
                        fetchedProductsArray.push(selectedProductsIndexArray[i]);
                    }
                    showOrderModal();
                    console.log(orderData);
                }, error: function () {
                    alert('error');
                }
            });
        }
    }

    var OrderForm = null;
    function showOrderModal() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/Quotation_controller/get_new_order_form') ?>",
            data: {
                'order_form': 1
            },
            success: function (data) {
                OrderForm = data;
                var selectedProductID = null;
                var navTabsHTML = "";
                var tabsContentHTML = "";
                for (var i = 0; i < fetchedProductsArray.length; i++) {
                    if (i === 0) {
                        navTabsHTML += '<li role="presentation" class="active"><a id="btn-' + fetchedProductsArray[i] + '" href="#' + fetchedProductsArray[i] + '" aria-controls="' + fetchedProductsArray[i] + '" class="active show" role="tab" data-toggle="tab" onclick="generateOrderForm(\'' + fetchedProductsArray[i] + '\')">Product ' + (i + 1) + '</a></li>';
                        tabsContentHTML += '<div role="tabpanel" class="tab-pane active" id="' + fetchedProductsArray[i] + '">' + OrderForm + '</div>';
                        selectedProductID = fetchedProductsArray[i];
                    } else {
                        navTabsHTML += '<li role="presentation"><a href="#' + fetchedProductsArray[i] + '" aria-controls="' + fetchedProductsArray[i] + '" class="show" role="tab" data-toggle="tab" onclick="generateOrderForm(\'' + fetchedProductsArray[i] + '\')">Product ' + (i + 1) + '</a></li>';
                        tabsContentHTML += '<div role="tabpanel" class="tab-pane" id="' + fetchedProductsArray[i] + '"></div>';

                    }
                }
                $('#Modal-Place-Order .modal-body .nav-tabs').empty();
                $('#Modal-Place-Order .modal-body .nav-tabs').html(navTabsHTML);
                $('#Modal-Place-Order .modal-body .tab-content').empty();
                $('#Modal-Place-Order .modal-body .tab-content').html(tabsContentHTML);
                generateOrderForm(selectedProductID);
            },
            error: function () {
                alert('error');
            }
        });
        var quote_cutomer_id = $('#quote_cutomer_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/Quotation_controller/get_new_order_form_footer') ?>",
            data: {
                order_form: 1,
                quote_cutomer_id: quote_cutomer_id
            },
            success: function (data) {
                $('#Modal-Place-Order .modal-footer').html(data);
                $('#Modal-Place-Order #customer_id').val(quote_cutomer_id);
                $('#Modal-Place-Order #customer_id').change();

                $('#save_order_form').submit(function (event) {
                    var formData = $(this).serialize();
                    save_order(formData);
                    event.preventDefault();
                });
                $.ajax({
                    url: "<?= base_url() ?>c_level/order_controller/order_id_generate",
                    type: 'get',
                    success: function (r) {
                        $("#orderid").val(r);
                    }
                });
                $('#Modal-Place-Order').modal('show');

            }, error: function () {
                alert('error');
            }
        });


    }


    function generateOrderForm(tarElement) {
        for (var i = 0; i < fetchedProductsArray.length; i++) {
            if (tarElement == fetchedProductsArray[i]) {
                $('#Modal-Place-Order .modal-body .tab-content #' + fetchedProductsArray[i]).empty();
                $('#Modal-Place-Order .modal-body .tab-content #' + fetchedProductsArray[i]).html(OrderForm);

                $('#Modal-Place-Order #' + fetchedProductsArray[i] + ' select#category_id').val(orderData[fetchedProductsArray[i]]['win_category_id']);
                $('#Modal-Place-Order #' + fetchedProductsArray[i] + ' select#room').val(orderData[fetchedProductsArray[i]]['win_room_name']);

                category_wise_subcategory(orderData[fetchedProductsArray[i]]['win_category_id']);

                get_product_by_category(orderData[fetchedProductsArray[i]]['win_category_id'], orderData[fetchedProductsArray[i]]['win_product_id']);

                if (orderData[fetchedProductsArray[i]]['win_category_name'] !== "Misc") {
                    get_product_to_attribute(orderData[fetchedProductsArray[i]]['win_product_id']);
                    get_color_partan_model(orderData[fetchedProductsArray[i]]['win_product_id'], orderData[fetchedProductsArray[i]]['win_pattern_id']);
                }

                $('#Modal-Place-Order #' + fetchedProductsArray[i] + ' input#width').val(orderData[fetchedProductsArray[i]]['win_width']);
                if (orderData[fetchedProductsArray[i]]['win_wfraction'] != "") {
                    $("#Modal-Place-Order #" + fetchedProductsArray[i] + " select#width_fraction_id option:contains(" + orderData[fetchedProductsArray[i]]['win_wfraction'] + ")").attr('selected', 'selected');
                }
                $('#Modal-Place-Order #' + fetchedProductsArray[i] + ' input#height').val(orderData[fetchedProductsArray[i]]['win_height']);
                if (orderData[fetchedProductsArray[i]]['win_htfraction'] != "") {
                    $("#Modal-Place-Order #" + fetchedProductsArray[i] + " select#height_fraction_id option:contains(" + orderData[fetchedProductsArray[i]]['win_htfraction'] + ")").attr('selected', 'selected');
                }
                $("#AddToCart").on('submit', function (e) {
                    e.preventDefault();
                    var submit_url = "<?= base_url(); ?>c_level/order_controller/add_to_cart";
                    var data = $(this).serialize();
                    AddDataToCart(submit_url, data);
                });

                // submit form and add data
                $("#clearCart").on('click', function () {
                    console.log('hello');
                    ClearDataFromCart()
                });
            } else {
                $('#Modal-Place-Order .modal-body .tab-content #' + fetchedProductsArray[i]).empty();
            }
        }
    }
</script>