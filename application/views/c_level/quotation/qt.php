<script type="text/javascript">

    //Add Invoice Field
    // Counts and limit for invoice

    function reload() {
        $(".add_input").load(location.href + " .add_input>*", "");
        var ca = $('#category_id').find('option:selected').text();
        $('#category_name').val(ca);

        $.ajax({
            url: "<?php echo base_url('c_level/Quotation_controller/upcharge/'); ?>" + ca,
            type: 'get',
            success: function (r) {

                $("#footer").html(r);
            }
        });

    }

    var count = 1;
    var limits = 500;
    var wrapper = $('.add_input');

    function addInputField() {

        if ($('#category_id').val() === '') {
            alert('Please select category');
            return fales;
        }


        var html = '<tr>' +
                '<td>' + count + '</td>' +
                '<td>' +
                '<input type="number" name="width[]" id="width_' + count + '" class=" form-control width" onkeyup="PricCalculet(' + count + ')">' +
                '</td>' +
                '<td>' +
                '<input type="number" name="height[]" id="height_' + count + '" class=" form-control height" onkeyup="PricCalculet(' + count + ')">' +
                '</td>' +
                '<td>' +
                '<select name="product_id[]" class="form-control select2 product_id" id="product_id_' + count + '" onchange="setPattern(' + count + ')" required>' +
                '<option>-- Select Catagory --</option>' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<select name="patter_id[]" class="form-control select2" id="patter_id_' + count + '" onchange="PricCalculet(' + count + ')">' +
                '<option value="">-- Select Catagory --</option>' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<input type="hidden" name="product_price[]" id="product_price_' + count + '" class=" form-control product_price" readonly>' +
                '<input type="text" name="price[]" id="price_' + count + '" value="0" class=" form-control price" readonly>' +
                '</td>' +
                '<td>' +
                '<a href="javascript:void(0)" class="btn btn-danger default btn-xs remove_item" id="delete_cart_item" ><i class="glyph-icon simple-icon-trash"></i></a>' +
                '</td>' +
                '</tr>'

        $(wrapper).append(html);
        setProduct('product_id_' + count);
        count++;
    }

    function setProduct(product_id) {

        var category_id = $('#category_id').val();
        $.ajax({
            url: "<?php echo base_url('c_level/Quotation_controller/get_product_by_category/'); ?>" + category_id,
            type: 'get',
            success: function (r) {
                $("#" + product_id).html(r);
            }
        });
    }

    //Delete a row 
    $(wrapper).on('click', '.remove_item', function (e) {
        e.preventDefault();
        $(this).parent().parent('tr').remove();
        x--;
    });


    function setPattern(item) {

        var product_id = $('#product_id_' + item).val();
        var pattern = $('#product_id_' + item).parent().next().children();

        $.ajax({

            url: "<?php echo base_url('c_level/Quotation_controller/get_color_partan_model/'); ?>" + product_id,
            type: 'get',
            success: function (r) {
                var obj = jQuery.parseJSON(r);
                $(pattern).html(obj.pattern);
                $("#product_price_" + item).val(obj.price);
            }
        });

        PricCalculet(item);
    }


    function PricCalculet(item) {
        var product_id = $('#product_id_' + item).val();
        var patter_id = $('#patter_id_' + item).val();
        var width = $('#width_' + item).val();
        var wfraction = $('#wfraction_' + item).val();
        var height = $('#height_' + item).val();
        var htfraction = $('#htfraction_' + item).val();
        var product_price = $('#product_price_' + item).val();
        $.ajax({
            url: "<?php echo base_url('c_level/Quotation_controller/price_call/'); ?>" + width + "/" + height + "/" + product_id + "/" + patter_id + "/" + wfraction + "/" + htfraction,
            type: 'get',
            success: function (r) {
                var obj = jQuery.parseJSON(r);
                $('#price_' + item).val(obj.price);
            }
        });
    }


    $('body').on('click', function () {
        var unit_price = 0;
        $(".price").each(function () {
            isNaN(this.value) || 0 == this.value.length || (unit_price += parseFloat(this.value))
        });
        $('#room_sub_price').val(unit_price);
        var upcharge = 0;
        $(".up_price").each(function () {
            isNaN(this.value) || 0 == this.value.length || (upcharge += parseFloat(this.value))
        });
        $('#upcharge').val(upcharge);
    });


    $('body').on('change', '#no_panel', function () {
        var no_plan = parseFloat($(this).val());
        $('.plan_cost').text(no_plan * 150);
        $('#plan_price').val(no_plan * 150);
    });



    $('body').on('change', '#no_window', function () {
        var no_window = parseFloat($(this).val());
        $('.window_cost').text(no_window * 30);
        $('#window_price').val(no_window * 30);
    });


    $('body').on('change', '#no_door', function () {
        var no_door = parseFloat($(this).val());
        $('.door_cost').text(no_door * 150);
        $('#door_price').val(no_door * 150);
    });


    $('body').on('change', '#no_shutters', function () {
        var no_shutters = parseFloat($(this).val());
        $('.shutters_cost').text(no_shutters * 30);
        $('#shutters_price').val(no_shutters * 30);
    });


    $('body').on('change', '#no_t_window', function () {
        var no_t_window = parseFloat($(this).val());
        $('.t_window_cost').text(no_t_window * 40);
        $('#t_window_price').val(no_t_window * 40);
    });


    $('body').on('change', '#no_blinds', function () {
        var no_blinds = parseFloat($(this).val());
        $('.blind_cost').text(no_blinds * 4);
        $('#blinds_price').val(no_blinds * 4);
    });

    $('body').on('change', '#Macro', function () {
        var Macro = parseFloat($(this).val());
        $('.Macro_cost').text((Macro / 100) * 10);
        $('#macro_price').val((Macro / 100) * 10);
    });

    $('body').on('change', '#custom_paint', function () {
        var custom_paint = parseFloat($(this).val());
        $('.custom_paint_cost').text((custom_paint / 100) * 10);
        $('#custom_paint_price').val((custom_paint / 100) * 10);
    });


    // submit form and add data
    $("#AddToCart").on('submit', function (e) {
        e.preventDefault();

        var isNoOfWinAva = null;
        $('.no_of_windows').each(function () {
            if (parseInt($(this).val()) === 0) {
                isNoOfWinAva = false;
            } else if (isNoOfWinAva !== false) {
                isNoOfWinAva = true;
            }
        });

        var isCatSelected = null;
        $('.category_dp').each(function () {
            if ($(this).val() === "--Select Category--") {
                isCatSelected = false;
            } else if (isCatSelected !== false) {
                isCatSelected = true;
            }
        });

        // var isAtleastMisc = null;
        // $('.misc_input').each(function(){
        //     var miscRow = $(this).parent().parent();
        //     if (miscRow.find('.misc_input').val() == "" && miscRow.find('.misc_input_val').val() == "") {
        //         isAtleastMisc = false;
        //     } else if (isAtleastMisc !== false){
        //         isAtleastMisc = true;
        //     } 
        // });

        if (isNoOfWinAva === true && (isCatSelected === true || isCatSelected === null)) {
            $(this).find('.category_dp').removeAttr('disabled');
            var submit_url = "<?php echo base_url('c_level/Quotation_controller/add_to_cart') ?>";
            $.ajax({
                type: 'POST',
                url: submit_url,
                data: $(this).serialize(),
                success: function (res) {
                    res = JSON.parse(res);
                    $("#cartItems table tbody").html('');
                    $("#cartItems table tbody").html(res.cartItemsHtml);
                    $("#subtotal").val(res.subtotal);
                    $("#total_discount").val('0.00');
                    $("#total_upcharge").val(res.upcharge.toFixed(2));
                    toastr.success('Success! - Add to cart Successfully');
                    setTimeout(function () {
                    }, 2000);
                    calculetsPrice();
                }, error: function () {
                    alert('error');
                }
            });
        } else if (isNoOfWinAva === false) {
            alert('no of window for room should not be zero!');
        } else if (isCatSelected === false) {
            alert('category for window must be selected!');
        } else if (isAtleastMisc === false) {
            alert('atleast one input should be selected for Misc category fields!');
        }
    });



    // submit form and add data
    $("#clearCart").on('click', function (e) {
        e.preventDefault();
        var submit_url = "<?= base_url(); ?>c_level/Quotation_controller/clear_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                alert('error');
            }
        });
    });


    // function deleteCartItem(id){

    //     var submit_url = "<?= base_url(); ?>c_level/Quotation_controller/delete_cart_item/"+id;
    //     $.ajax({
    //         type: 'GET',
    //         url: submit_url,
    //         success: function(res) {

    //             $("#cartItems").load(location.href+" #cartItems>*",""); 

    //         },error: function() {
    //             alert('error');
    //         }
    //     });

    // }




    $("body").on('change', '#customer_id', function () {

        var customer_id = $(this).val();

        $.ajax({
            url: '<?= base_url() ?>c_level/Quotation_controller/customer_wise_sidemark/' + customer_id,
            type: 'get',
            success: function (data) {

                if (data == 0) {
                    $("#side_mark").val("None");
                } else {
                    var obj = jQuery.parseJSON(data);
                    var tax = (obj.tax_rate != null ? obj.tax_rate : 0);
                    $('#customertype').val(obj.level_id);
                    $('#side_mark').val(obj.side_mark);
                    $('#tax').val(tax);
                    setDiscountAmount();
                    calculetsPrice();
                }
            }
        });
    });

    /*function setDiscount(item){
     var list_price = $('#list_price_'+item).val();
     var dis = $('#discount_'+item).val();
     var price = (list_price*dis)/100;
     var dis = $('#utprice_'+item).val(list_price-price);
     calculetsPrice();
     }*/


    function setDiscountAmount() {
        var productids = [];
        $("#cartItems .discount_input").each(function () {
            if ($(this).data('productid') != "") {
                productids.push($(this).data('productid'));
            }
        });

        $.ajax({
            url: '<?= base_url() ?>c_level/Quotation_controller/get_product_discount',
            type: 'POST',
            data: {productids: JSON.stringify(productids)},
            success: function (res) {
                if (res != "") {
                    $taxRatesData = JSON.parse(res);
                    for (var i = 0; i < $taxRatesData.length; i++) {
                        $("#cartItems .discount_input").each(function () {
                            if ($(this).data('productid') != "" && $(this).data('productid') == $taxRatesData[i]['product_id']) {
                                $(this).val($taxRatesData[i]['individual_cost_factor']);
                                updatePrice($(this));
                            }
                        });
                    }
                }
            }, error: function () {
                alert('error');
            }
        });
    }

    function calculetsPrice() {
        var subtotal = 0;
        $('#cartItems tbody tr .list-amount').each(function () {
            subtotal = subtotal + parseFloat($(this).text().substr(1));
        });
        $("#subtotal").val(subtotal.toFixed(2));
        var taxs = parseFloat($('#tax').val());
        var upcharge_total = parseFloat($('#total_upcharge').val());
        var total_discount = parseFloat($('#total_discount').val());
        if (!total_discount > 0) {
            total_discount = 0;
        }

        var tax = ((subtotal + upcharge_total)) * (taxs / 100);

        if (isNaN(tax)) {
            tax = 0;
        }

        $('#tax_text').text('Sales tax ' + taxs + '%');
        $('#tax_val').val(tax.toFixed(2));


        var grandtotal = subtotal + tax + upcharge_total;
        $('#grand_total').val(grandtotal.toFixed(2));
    }


    function deleteCartItem(cart_item_index) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>c_level/Quotation_controller/deleteCartItem",
            data: {cart_item_index: cart_item_index},
            success: function (res) {
                res = JSON.parse(res);
                $("#cartItems table tbody").html('');
                $("#cartItems table tbody").html(res.cartItemsHtml);
                toastr.success('Success! - Remove from cart Successfully');
                setTimeout(function () {
                }, 2000);
                calculetsPrice();
            }, error: function () {
                alert('error');
            }
        });
    }

    function updatePrice(that) {
        var discount = 0;
        var total_discount = 0;
        if (($(that).val() > -1 && $(that).val() < 101) || $(that).val() === '') {
            if ($(that).val() == '') {
                $(that).val() = 0;
            }
            discount = parseFloat($(that).val()) / 100;
        } else {
            alert('Please enter discount in ragne of 1 - 100');
            return false;
        }
        var tabRow = $(that).parent().parent();
        var price = parseFloat(tabRow.find('.window_price').text().substr(1));
        var upcharge = parseFloat(tabRow.find('.upcharge').text().substr(1));
        if (discount > -1 && discount < 101) {
            var priceWithDiscount = price - (price * discount);
            tabRow.find('.list-amount').text('$' + priceWithDiscount.toFixed(2));

            var rowPrice = upcharge + priceWithDiscount;
            console.log("priceWithDiscount == " + priceWithDiscount);
            console.log("upcharge == " + upcharge);
            console.log("rowPrice == " + rowPrice);
            tabRow.find('.price').text('$' + rowPrice.toFixed(2));

            $('#cartItems tbody tr').each(function () {
                var w_price = parseFloat($(this).find('.window_price').text().substr(1));
                var dis_inp = parseFloat($(this).find('.discount_input').val());
                if (dis_inp > -1) {
                    var w_dis = dis_inp / 100;
                    var w_dis_amount = w_price - (w_price * w_dis);
                    total_discount = total_discount + parseFloat(w_price - w_dis_amount);
                }
            });
            $('#total_discount').val(total_discount.toFixed(2));
            calculetsPrice();
        } else {
            $(that).val('');
        }
    }




// // ================= its for customer wise sidemark ==========


//         $("body").on('change', '#customer_id', function () {

//             var customer_id = $(this).val();
//             $.ajax({
//                 url: '<?= base_url() ?>c_level/order_controller/customer_wise_sidemark/' + customer_id,
//                 type: 'get',
//                 success: function (data) {
//                     if (data == 0) {
//                         $("#side_mark").val("None");
//                     } else {
//                         var obj = jQuery.parseJSON(data);
//                         var tax = (obj.tax_rate!=null?obj.tax_rate:0);
//                         $('#customertype').val(obj.level_id);
//                         $('#side_mark').val(obj.side_mark);
//                         $('#tax').val(tax);
//                         calculetsPrice();
//                     }
//                 }
//             });
//         });

// //======================================  


</script>