
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<style>
    .pac-container.pac-logo{
        top: 400px !important;
    }
    .pac-container:after{
        content:none !important;
    }
    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
   #Add-New-Customer-Modal .modal-body{ overflow-y: scroll;
    height: 900px;
   }
</style>

        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">

                        <form id="add-customer-form" action="<?php echo base_url(); ?>customer-save" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="first_name">First Name <span class="text-danger"> * </span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John"  onkeyup="required_validation()" required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="last_name">Last Name <span class="text-danger"> * </span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" onkeyup="required_validation()" required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email <span class="text-danger"> * </span></label>
                                    <input type="email" class="form-control" id="email" onkeyup="check_email_keyup()" name="email" placeholder="johndoe@yahoo.com">
                                    <span id="error"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <table class="" id="normalItem" style="margin-top: 25px;">
                                        <thead>
                <!--                            <tr>
                                                <th class="text-center">Phone</th>
                                                <th class="text-center">Action </th>
                                            </tr>-->
                                        </thead>
                                        <tbody id="addItem">
                                            <tr>
                                                <td>
                                                    <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                                                        <option value="">Type</option>
                                                        <option value="Phone">Phone</option>
                                                        <option value="Fax">Fax</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                    <input id="phone_1" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" onkeyup="special_character(1)" style="">
                                                </td>
                                                <td class="text-left">
                                                    <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                    <a style="font-size: 20px;" class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button id="add-item" class=" btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 5px 15px 15px;"><span class="simple-icon-plus"></span></button>
                                </div>
                                <!--                                <div class="form-group col-md-6">
                                                                    <label for="phone">Mobile No <span class="text-danger"> * </span></label>
                                                                    <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX" onkeyup="required_validation()" required>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="work_phone">Work Phone</label>
                                                                    <input type="text" class="form-control phone" id="work_phone" name="work_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="home_phone">Home Phone</label>
                                                                    <input type="text" class="form-control phone" id="home_phone" name="home_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                                </div>-->
                                <div class="form-group col-md-6">
                                    <label for="address">Address <span class="text-danger"> * </span></label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder=""  required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="company">Company </label>
                                    <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="city">City</label>
                                    <input type="text" class="form-control" id="city" name="city">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="state">State</label>
                                    <input type="text" class="form-control" id="state" name="state">
                                </div>
                                <!--                                <div class="form-group col-md-4">
                                                                    <label for="state">State</label>
                                                                    <select class="form-control select2" id="state" name="state">
                                                                        <option value="">-- select one -- </option>
                                <?php
                                foreach ($get_states as $states) {
                                    echo "<option value='$states->state_id'>$states->state_name</option>";
                                }
                                ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="city">City</label>
                                                                    <input type="text" class="form-control" id="city" name="city" placeholder="Ariana">
                                                                    <select class="form-control select2" id="city" name="city" data-placeholder="-- select one --">
                                                                        <option value="">-- select one --</option>
                                                                    </select>
                                                                </div>-->
                                <div class="form-group col-md-3">
                                    <label for="zip_code">Zip</label>
                                    <input type="text" class="form-control" id="zip_code" name="zip_code">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="country_code">Country Code</label>
                                    <input type="text" class="form-control" id="country_code" name="country_code">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="reference">Referred By</label>
                                    <input type="text" class="form-control" id="reference" name="reference" placeholder="John Clark">
                                </div>
                                <div class="form-group col-md-6">
                                    <table class="" id="normalItem_file"  style="margin-bottom: 5px; margin-top: 25px;">
                    <!--                    <thead>
                                            <tr>
                                                <th class="text-center">Files</th>
                                                <th class="text-center">Action </th>
                                            </tr>
                                        </thead>-->
                                        <tbody id="addItem_file">
                                            <tr id="1">
                                                <td>
                                                    <input id="file_upload_1" class="form-control" type="file" name="file_upload[]" multiple>
                                                </td>
                                                <td class="text-left" width="25%">
                                                    <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                    <a style="font-size: 20px;" class="text-danger" value="Delete" onclick="file_deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> 
                                    <button id="add-item" class="btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" type="button" style="margin: 0px 15px 15px;"><span class="simple-icon-plus"></span></button>
                                    <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                    <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
                                </div> 
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm float-right customer_btn">Add</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>


<script type="text/javascript">
    //    ============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {
//        if ($('#business').is(":checked"))
//        {
//            if ($('#username').val() == '') {
//                $('#username').css({'border': '2px; solid red'}).focus();
//                return false;
//            } else {
//                $('#username').css({'border': '2px; solid green'});
//            }
//            if ($('#password').val() == '') {
//                $('#password').css({'border': '2px; solid red'}).focus();
//                return false;
//            } else {
//                $('#password').css({'border': '2px; solid green'});
//            }
//        }

        if ($('#phone_type_1').val() == '' || $('#phone_1').val() == '') {
            alert("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
        if ($('#phone_type_2').val() == '' || $('#phone_2').val() == '') {
            alert("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
        if ($('#phone_type_3').val() == '' || $('#phone_3').val() == '') {
            alert("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
    });
    //        ========== some field validation ============   
    $('button[type=submit]').prop('disabled', true);
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '' && $("#address").val() != '') {
//            $("#first_name").css({'border': '1px solid red'}).focus();
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }
//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
//        console.log(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "<?php echo base_url(); ?>get-check-c-customer-unique-email",
            type: "post",
            data: data_string,
            success: function (data) {
//                console.log(data);
                if (data != 0) {
//                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
//                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }
    $(document).ready(function () {
        //        ================== its for state wise city show ======================
        $('body').on('change', '#state', function () {
            var state = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>state-wise-city/" + state,
                type: 'get',
                success: function (r) {
                    r = JSON.parse(r);
//                    alert(r);
                    $("#city").empty();
                    $("#city").html("<option value=''>-- select one -- </option>");
                    $.each(r, function (ar, typeval) {
                        $('#city').append($('<option>').text(typeval.city).attr('value', typeval.id));
                    });
                }
            });
        });
    });
//    ======= its for google place address geocomplete =============





    // function validFile(){
    //     var a = $('#file_upload_1').files[0].size;
    //     alert(a);
    //     var sizee = $("#fileup_1")[0].files[0].size;
    // }

        $("body").on("change", "#file_upload_1", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

    
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' &&  ext!='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });




        $("body").on("change", "#file_upload_2", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });




        $("body").on("change", "#file_upload_3", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });


        $("body").on("change", "#file_upload_4", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });






//    ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            alert("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count, e = document.createElement("tr");
            e.innerHTML = "\n\
                                     <td class='text-center'><select id='phone_type_" + count + "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value=''>Type</option><option value='Phone'>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" + count + "' class='phone form-control phone_no_type current_row' type='text' onkeyup='special_character(" + count + ")' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                    <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    count++;
//=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function (e) {
                var key = e.charCode || e.keyCode || 0;
                var phone = $(this);
                if (phone.val().length === 0) {
                    phone.val(phone.val() + '+1 (');
                }
                // Auto-format- do not expose the mask as the user begins to type
                if (key !== 8 && key !== 9) {
                    //alert("D");
                    if (phone.val().length === 6) {

                        phone.val(phone.val());
                        phone = phone;
                    }
                    if (phone.val().length === 7) {
                        phone.val(phone.val() + ') ');
                    }
                    if (phone.val().length === 12) {
                        phone.val(phone.val() + '-');
                    }
                    if (phone.val().length >= 17) {
                        phone.val(phone.val().slice(0, 16));
                    }
                }
                // Allow numeric (and tab, backspace, delete) keys only
                return (key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
            })
                    .on('focus', function () {
                        phone = $(this);

                        if (phone.val().length === 0) {

                            phone.val('+1 (');
                        } else {
                            var val = phone.val();
                            phone.val('').val(val); // Ensure cursor remains at the end
                        }
                    })

                    .on('blur', function () {
                        $phone = $(this);

                        if ($phone.val() === '(') {
                            $phone.val('');
                        }
                    });

         
        }
    }
//    ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);

            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function () {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });

            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function () {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }
//    ======== close ===========

//========== its for customer multiple file upload ==============
    function addInputFile(t) {
        var table_row_count = $("#normalItem_file > tbody > tr").length;
        var file_count = table_row_count + 1;
        file_limits = 5;
        if (file_count == file_limits) {
//            alert("You have reached the limit of adding " + file_count + " files");
            alert("You have reached the limit of adding 4 files");
        } else {

//            alert(table_row_count);
            var a = "file_upload_" + file_count, e = document.createElement("tr");
            e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                    <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    file_count++;
        }
    }
    function file_deleteRow(t) {
        var a = $("#normalItem_file > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_file_count = 1;
            $("#normalItem_file > tbody > tr td input.current_file_count").each(function () {
                current_file_count++;
                $(this).attr('class', 'form-control');
                $(this).attr('id', 'file_upload_' + current_file_count);
                $(this).attr('name', 'file_upload_' + current_file_count);
            });

        }
    }

    //=========== its for get special character =========
    function special_character(t) {
//        alert(t);
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            alert(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }
</script>