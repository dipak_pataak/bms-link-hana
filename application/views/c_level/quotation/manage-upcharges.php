<style>
    .active12{
        background-color: #fff;
    }
    table.dataTable {
        border-collapse: collapse !important;
    }
    .page-item .page-link {
        padding: .55rem 10px;
    }
</style>
<main>
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage Upcharges</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <?php
        $message = $this->session->flashdata('message');

        if ($message != '') {
            echo $message;
        }
        ?>

        <div class="row">

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <section class="content-header">
                        <div class="col-md-3 pull-right right">
                            <?php
                            $error = $this->session->flashdata('error');
                            if ($error) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $this->session->flashdata('error'); ?>                    
                                </div>
                            <?php } ?>
                            <?php
                            $success = $this->session->flashdata('success');
                            if ($success) {
                                ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $this->session->flashdata('success'); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </section>


                    <div class="card-body Send_To_Area"> 
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <div class="form-group">
                                    <span class="btn btn-primary add_street_name" data-toggle="modal" data-target="#addSendToModal"><i class="fa fa-plus"></i> Add New</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title"></h3>
                                        <div class="box-tools">

                                        </div>
                                    </div><!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <div class="table-responsive">
                                        <table class="table table-hover" id="example">
                                            
                                            <thead>
                                                <tr>
                                                    <th>Upcharge Name</th>
                                                    <th>Type</th>
                                                    <th>Value</th>

                                                    <th>Category</th>
                                                    <th>Created On</th>
                                                    <th class="text-center">Actions</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                if (!empty($upcharges)) {
                                                    foreach ($upcharges->result() as $record) {
                                                ?>
                                                        <tr>
                                                            <td><?php echo $record->name; ?></td>
                                                            <td><?php echo $record->type; ?></td>
                                                            <td><?php echo $record->value; ?></td>
                                                            <td><?php echo $categories[$record->category_id]; ?></td>
                                                            <td><?php echo date("d-m-Y", strtotime($record->createdDtm)); ?></td>
                                                            <td class="text-center">
                                                                <span class="btn btn-sm btn-info update_upcharge"  title="Edit" data-name="<?php echo $record->name; ?>" data-type="<?php echo $record->type; ?>" data-value="<?php echo $record->value; ?>" data-category_id="<?php echo $record->category_id; ?>" data-id="<?php echo $record->id; ?>"> <i class="simple-icon-eye"></i></span>
                                                                <span class="btn btn-sm btn-danger delete_btn" href="#" data-del_tbl="<?php echo $upcharge_table; ?>" data-del_id="<?php echo $record->id; ?>" title="Delete"><i class="glyph-icon simple-icon-trash"></i></span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div><!-- /.box-body -->

                                </div><!-- /.box -->
                            </div>
                            <!-- add department modal -->
                            <!-- Modal -->
                            <div id="addSendToModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <form action="<?php echo base_url(); ?>c_level/quotation_controller/addUpcharges" method="post">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Add New Upcharge</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group"><label for="department">Category</label>
                                                    <select name="category_id" class="category_id form-control">
                                                        <?php
                                                        if (!empty($categories)) {
                                                            foreach ($categories as $key => $val) {
                                                                ?>
                                                                <option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group"><label>Name</label>
                                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" >
                                                </div>
                                                <div class="form-group"><label>Type</label>
                                                    <select class="form-control" name="type" >
                                                        <option value="Amount">Amount</option>
                                                        <option value="Percentage(%)">Percentage(%)</option>
                                                    </select>
                                                </div>
                                                <div class="form-group"><label>Value</label>
                                                    <input type="text" class="form-control" name="value" placeholder="Enter Value" >
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-default">Create</button> &nbsp;
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- update department modal-->
                            <!-- Modal -->
                            <div id="updateUpchargeModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <form action="<?php echo base_url(); ?>c_level/quotation_controller/updateUpcharges" method="post">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Upcharge</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group"><label for="department">Category</label>
                                                    <select name="category_id" class="update_category_id form-control">
                                                        <?php
                                                        if (!empty($categories)) {
                                                            foreach ($categories as $key => $val) {
                                                                ?>
                                                                <option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group"><label>Name</label>
                                                    <input type="text" class="form-control update_name" name="name" placeholder="Enter Name" >
                                                </div>
                                                <div class="form-group"><label>Type</label>
                                                    <select class="form-control update_type" name="type" >
                                                        <option value="Amount">Amount</option>
                                                        <option value="Percentage(%)">Percentage(%)</option>
                                                    </select>
                                                </div>
                                                <div class="form-group"><label>Value</label>
                                                    <input type="text" class="form-control update_value" name="value" placeholder="Enter Value" >
                                                </div>
                                                <input type="hidden" name="upcharge_id" class="upcharge_id" value="">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-default">Update</button> &nbsp;
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        var table = $('#example').DataTable({"order": [[0, 'desc']]});


                        $('.update_upcharge').on('click', function () {
                            var id = $(this).data('id');
                            var name = $(this).data('name');
                            var type = $(this).data('type');
                            var value = $(this).data('value');
                            var category_id = $(this).data('category_id');
                            $('#updateUpchargeModal .upcharge_id').val(id);
                            $('#updateUpchargeModal .update_name').val(name);
                            $('#updateUpchargeModal .update_type').val(type);
                            $('#updateUpchargeModal .update_value').val(value);
                            $('#updateUpchargeModal .update_category_id').val(category_id);

                            $('#updateUpchargeModal').modal('show');

                        });

                        $('.delete_btn').on('click', function () {
                            var del_id = $(this).data('del_id');
                            var del_tbl = $(this).data('del_tbl');
                            var del_this = $(this);
                            var x = confirm('are you sure you want to delete this ?');
                            if (x) {
                                $.ajax({
                                    url: "<?php echo base_url() . 'c_level/quotation_controller/deleteRow'; ?>",
                                    type: 'POST',
                                    dataType: 'JSON',
                                    data: {
                                        del_id: del_id,
                                        del_tbl: del_tbl,
                                    },
                                    success: function (data) {
                                        del_this.parent().parent().hide();
                                    }
                                });
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</main>