<style type="text/css">
    .col-sm-3 + .col-sm-3 br+.col-sm-12 .form-control {
        margin-left: 26%;
        margin-bottom: 1rem;
    }
    .col-sm-3 + .col-sm-3 br+.col-sm-12 input+.form-control {
        margin-left: 0!important;
    }
    .form-group {
        margin: 0px;
    }
    .form-group.col-md-12 .row div.col-sm-4, .form-group.col-md-12 .row div.col-sm-6 {
        padding-left: 0px;
    }
    .col-sm-3, label.col-sm-3 {
        line-height: 36px;
    }
    .row {
        margin-bottom: 1rem;
        clear:both;
    }
    div.col-sm-3 + div.col-sm-3 {
        margin:0 !important;
    }
    br {
        display:none;
    }
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 958.55px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }

    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }



</style>


<main id="orderPage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="row sticky_container">
                            <div class="col-sm-8">
                                <?= form_open('c_level/order_controller/add_to_cart', array('id' => 'AddToCart_order')) ?>
                                <div class="form-row">

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Category</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2-single" name="category_id" id="category_id"  required="" data-placeholder="--select one --">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($get_category as $category) {
                                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="subcategory_id">

                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Product</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2-single" name="product_id" id="product_id" onchange="getAttribute(this.value)" required="" data-placeholder="-- select one --">

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="color_model"></div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Width</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()" onKeyup="loadPStyle()" min="0"  required="" >
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                                    <option value="">--Select one--</option>
                                                    <?php
                                                    foreach ($fractions as $f) {
                                                        echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="col-sm-3">Height</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()" onKeyup="loadPStyle()" min="0" required="" >
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                                    <option value="">--Select one--</option>
                                                    <?php
                                                    foreach ($fractions as $f) {
                                                        echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="ssssttt14"></div>

                                    <div class="form-group col-md-12">
                                    </div>

                                    <!-- atributs area -->
                                    <div class="form-group col-md-12" id="attr">

                                    </div>
                                    <!-- End atributs area -->


                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Room</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2-single" name="room" id="room"  required="" data-placeholder="--select one --">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($rooms as $r) {
                                                        echo "<option value='$r->room_name'>$r->room_name</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <lable class="col-sm-3">Note</lable>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" name="notes" rows="6"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-9 mb-0">
                                        <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn" >Add product to cart</button>
                                    </div>

                                </div>
                                <input type="hidden" name="total_price" id="total_price">
                                <?= form_close(); ?>
                            </div>


                            <div class="col-sm-4 sticky_item">
                                <div class="fixed_item">
                                    <p id="tprice">Total Price = <?= $currencys[0]->currency; ?> 0</p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php
    if (isset($orderjs)) {
        $this->load->view($orderjs);
    }
    ?>
</main>

<script type="text/javascript">

    $(document).on('submit', '#myForm', function (e) {

        e.preventDefault();

        var formData = $(this).serialize();

    });

</script>



