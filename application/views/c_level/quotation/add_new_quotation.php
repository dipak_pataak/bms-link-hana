<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 713.391px !important;
        left: 383.5px;
        z-index: 10;
        display: block;
    }
    .right{
        float: right;
    }
    .detail_row{
        width: 1924px;
        overflow-x: scroll;
    }
    .scrollable_width{
        width: 100%  !important;
        overflow-x: scroll;
    }
    .card-body{
        width: 100%;
        overflow-x: scroll;
    }
    .rooms_section{
        width: 100%;

    }
    .add_more_room_btn{
        text-align: center;
    }
    .onek{
        width: 100px;
        display: inline;
    }
    .twok{
        width: 200px;
        display: inline;
    }
    .threek{
        width: 300px;
        display: inline;
    }
    .fivek{
        width: 500px;
        display: inline;
    }
    .sixk{
        width: 500px;
        display: inline;
    }
    .sevenk{
        width: 700px;
        display: inline;
    }
    .in_row{
        display: inline;
    }
    .row_form{
        width: 100%;
        overflow-x: scroll;
    }
    body{
        overflow: scroll;
    }
    .room_details_section{
        width: 2400px;
    }
    .delete_window, .delete_category, .delete_room{
        cursor: pointer;
    }

    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>

<!-- add customer css-->
<style>
    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-container{
        width: inherit !important;
        left: inherit  !important;
        top: inherit  !important;
        display: block  !important;
    }
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
</style>
<!-- add customer css end-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/add_q.css" />

<main>
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New quotation</li>


                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>

                <li class="btn btn-info" style="floar:right; color:white; font-weight:bold;">
                    <a href="<?php echo base_url(); ?>c_level/quotation_controller/manageUpcharges" target="_blank">Manage Upcharges</a>
                </li>

            </div>
        </div>

        <?php
        $message = $this->session->flashdata('message');

        if ($message != '') {
            echo $message;
        }
        ?>

        <div class="row">

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <?= form_open('c_level/quotation_controller/add_to_cart', array('id' => 'AddToCart')) ?>
                    <div class="scrollable_width">
                        <h5>
                            <span class="left pull-left col-md-2">Roomwise Window Details </span>
                            <!--<div class="form-group col-md-2 right pull-right"> <button type="button" class="btn btn-xs btn-info pull-right right add_room_btn" onclick="">+ Add Room</button></div>-->
                        </h5>
                        <div class="separator mb-5"></div>
                        <fieldset class="rooms_section room_0">
                            <legend>Room 1</legend>
                            <div class="row room_details_section room_section_0">

                                <div class="form-group col-md-2">
                                    <label>Room</label>
                                    <select class="form-control select2 twok" name="room_name[]" id="room_name" onchange="reload()" required="" data-placeholder='-- select Room Type --'>
                                        <option value="">--Select room type--</option>
                                        <?php
                                        foreach ($rooms as $room) {
                                            echo "<option value='$room->room_name'>$room->room_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>No</label>
                                    <input type="number" name="no_of_windows[0]" class="form-control twok no_of_windows" value="0" data-room_no="0" placeholder="No of Windows" min="0" max="100"> 
                                </div>
                                <div class="height_width_sec_0 col-md-5">


                                </div>
                                <div class="form-group col-md-1 add_category_section_0"> <label>&nbsp;</label>
                                    <button type="button" class="btn btn-xs btn-success pull-right right add_category_btn" data-room_no="0" >+ Add Cat.</button><br>
                                    <!--<button type="button" class="btn btn-xs btn-success pull-right right add_misc_btn" data-misc="1" data-room_no="0" >+ Add Misc.</button>-->
                                </div>

                            </div>
                            <!--<span class="delete_room">X</span>-->
                        </fieldset>
                        <div class="row col-md-12 add_more_room_btn">
                            <button type="button" class="btn btn-xs center add_room_btn" data-room_no="1" >+ Add Room</button>
                        </div>
                        <div class="row form-group col-md-12 mb-0">
                            <button type="submit" class="btn btn-primary mb-0 float-right" autocomplete="off">Add To List</button>
                        </div>
                    </div>
                    <div class="page_modals">

                    </div>
                    <?= form_close() ?>
                </div>
                <br/>

            </div>


        </div>
    </div>


    <?= form_open('c_level/quotation_controller/save_quotation', array('id' => 'save-quotation', 'class' => 'col-lg-12 mb-4')) ?>

    <div class="">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4">Customer Info</h5>
                <div class="separator mb-5"></div>

                <div class="form-row">

                    <div class="form-group col-sm-6 col-md-4">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-2">Date</label>
                            <p>
                                <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                            </p>
                        </div>
                    </div>

                    <div class="form-group col-sm-6 col-md-8">

                        <div class="row">
                            <label for="customer_id" class="col-form-label col-sm-3">Select Customer</label>
                            <div class=" col-sm-6">
                            <!--<input type="text" class="form-control">-->
                                <select class="form-control select2-single" name="customer_id" id="customer_id" required data-placeholder="-- select one --">
                                    <option value=""></option>
                                    <?php
                                    foreach ($get_customer as $customer) {
                                        echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <input type="hidden" name="customertype" id="customertype" value="">
                            <span class="btn btn-info quick_add_customer" onclick="addCustomer()"><i class="fa fa-plus"></i>Add Customer</span>
                        </div>
                    </div>
                </div>



                <h5>Order Details</h5>
                <div class="separator mb-3"></div>
                <div id="cartItems">
                    <table class="datatable2 table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>SL#</th>
                                <th>Room Name</th>
                                <th>WXH</th>
                                <th>Category</th>
                                <th>Product</th>
                                <th>Window Price</th>
                                <th>Discount Amount (%) </th>
                                <th>Window Sub Total</th>
                                <th>Upcharge</th>
                                <th>Window Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>


    <div class="col-lg-5 offset-lg-7">
        <div class="card mb-4">
            <div class="card-body">
                <table class="table table-bordered mb-4">
                    <tr>
                        <td>Sub Total (<?= @$company_profile[0]->currency; ?>)</td>
                        <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                    </tr>
                    <tr>
                        <td>Total Discount(<?= @$company_profile[0]->currency; ?>)</td>
                        <td><input type="number" name="total_discount" id="total_discount" readonly="" class="form-control text-right"></td>
                    </tr>
                    <tr>
                        <td>Upcharge</td>
                        <td><input type="number" name="total_upcharge" id="total_upcharge" readonly="" class="form-control text-right"></td>
                    </tr>
                    <tr>
                        <td id='tax_text'>Sales Tax (%)</td>
                        <td>
                            <input type="hidden" name="tax"  id="tax" value="0" class="form-control">
                            <input type="number"  id="tax_val" value="" class="form-control text-right" readonly="">
                        </td>
                    </tr>
                    <tr>
                        <td>Grand Total (<?= @$company_profile[0]->currency; ?>)</td>
                        <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly="" required></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-4 offset-lg-8 text-right">
        <button type="submit" class="btn btn-danger" id="clearCart" >Clear data</button>
        <button type="button" class="btn btn-info" id="pq">Preview</button>
        <button type="submit" class="btn btn-success" id="gq" >Submit</button>
    </div>
    <?php echo form_close(); ?>
</div>
</div>
</main>



<!-- Modal -->
<div id="Modal-quot-preview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Header</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="overflow-y: scroll;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- Add customer Modal -->
<div id="Add-New-Customer-Modal" class="modal fade" role="dialog" style="overflow-y: scroll; height: 800px;">
    <div class="modal-dialog modal-xl">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" >

                <script src="//code.jquery.com/jquery.min.js"></script>
                <div class="card-body">

                    <form id="add-customer-form" action="<?php echo base_url(); ?>customer-save" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="first_name">First Name <span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John"  onkeyup="required_validation()" required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="last_name">Last Name <span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" onkeyup="required_validation()" required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email <span class="text-danger"> * </span></label>
                                <input type="email" class="form-control" id="email" onkeyup="check_email_keyup()" name="email" placeholder="johndoe@yahoo.com">
                                <span id="error"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <table class="" id="normalItem" style="margin-top: 25px;">
                                    <thead>
            <!--                            <tr>
                                            <th class="text-center">Phone</th>
                                            <th class="text-center">Action </th>
                                        </tr>-->
                                    </thead>
                                    <tbody id="addItem">
                                        <tr>
                                            <td>
                                                <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                                                    <option value="">Type</option>
                                                    <option value="Phone">Phone</option>
                                                    <option value="Fax">Fax</option>
                                                    <option value="Mobile">Mobile</option>
                                                </select>
                                                <input id="phone_1" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" onkeyup="special_character(1)" style="">
                                            </td>
                                            <td class="text-left">
                                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                <a style="font-size: 20px;" class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="add-item" class=" btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 5px 15px 15px;"><span class="simple-icon-plus"></span></button>
                            </div>
                            <!--                                <div class="form-group col-md-6">
                                                                <label for="phone">Mobile No <span class="text-danger"> * </span></label>
                                                                <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX" onkeyup="required_validation()" required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="work_phone">Work Phone</label>
                                                                <input type="text" class="form-control phone" id="work_phone" name="work_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="home_phone">Home Phone</label>
                                                                <input type="text" class="form-control phone" id="home_phone" name="home_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                            </div>-->
                            <div class="form-group col-md-6">
                                <label for="address">Address <span class="text-danger"> * </span></label>


                                <input type="text" class="form-control" id="address" name="address" placeholder=""  required autocomplete="off">

                            </div>
                            <div class="form-group col-md-6">
                                <label for="company">Company </label>
                                <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="state">State</label>
                                <input type="text" class="form-control" id="state" name="state">
                            </div>
                            <!--                                <div class="form-group col-md-4">
                                                                <label for="state">State</label>
                                                                <select class="form-control select2" id="state" name="state">
                                                                    <option value="">-- select one -- </option>
                            <?php
                            foreach ($get_states as $states) {
                                echo "<option value='$states->state_id'>$states->state_name</option>";
                            }
                            ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="city">City</label>
                                                                <input type="text" class="form-control" id="city" name="city" placeholder="Ariana">
                                                                <select class="form-control select2" id="city" name="city" data-placeholder="-- select one --">
                                                                    <option value="">-- select one --</option>
                                                                </select>
                                                            </div>-->
                            <div class="form-group col-md-3">
                                <label for="zip_code">Zip</label>
                                <input type="text" class="form-control" id="zip_code" name="zip_code">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="country_code">Country Code</label>
                                <input type="text" class="form-control" id="country_code" name="country_code">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="reference">Referred By</label>
                                <input type="text" class="form-control" id="reference" name="reference" placeholder="John Clark">
                            </div>
                            <div class="form-group col-md-6">
                                <table class="" id="normalItem_file"  style="margin-bottom: 5px; margin-top: 25px;">
                <!--                    <thead>
                                        <tr>
                                            <th class="text-center">Files</th>
                                            <th class="text-center">Action </th>
                                        </tr>
                                    </thead>-->
                                    <tbody id="addItem_file">
                                        <tr id="1">
                                            <td>
                                                <input id="file_upload_1" class="form-control" type="file" name="file_upload[]" multiple>
                                            </td>
                                            <td class="text-left" width="25%">
                                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                <a style="font-size: 20px;" class="text-danger" value="Delete" onclick="file_deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table> 
                                <button id="add-item" class="btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" type="button" style="margin: 0px 15px 15px;"><span class="simple-icon-plus"></span></button>
                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
                            </div> 
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn">Add</button>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



<?php $this->load->view($qtjs) ?>

<script>

    $('#pq').click(function () {
        if ($('#save-quotation #customer_id').val() == "") {
            alert("please select customer before preview");
        } else if ($('#cartItems table tbody tr').length <= 0) {
            alert("please fill invoice information before preview");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/Quotation_controller/generate_receipt_preview') ?>",
                data: $('#save-quotation').serialize(),
                success: function (res) {
                    $('#Modal-quot-preview .modal-body').html(res);
                    $('#Modal-quot-preview').modal('show');
                }, error: function () {
                    alert('error');
                }
            });
        }
    });
    function getPattern(room, window, cat_sec_no) {
        var windows = $('.container-fluid .room_section_' + room + ' .no_of_windows').val();
        var product_id = $('#product_id_' + room + '_' + window + '_' + cat_sec_no).val();
        var pattern = $('#pattern_id_' + +room + '_' + window + '_' + cat_sec_no);
        $.ajax({

            url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_color_partan_model/" + product_id,
            type: 'get',
            success: function (r) {
                var obj = jQuery.parseJSON(r);
                if (window == '0') {
                    for (var j = 0; j < windows; j++) {
                        var window_no = j;
                        //  $('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val(product_id);
                        if ($('#pattern_id_' + +room + '_' + window_no + '_' + cat_sec_no).val() == '') {
                            $('#pattern_id_' + +room + '_' + window_no + '_' + cat_sec_no).html(obj.pattern);
                        }
                    }
                } else {
                    $('#pattern_id_' + +room + '_' + window + '_' + cat_sec_no).html(obj.pattern);
                }
                $('#product_price_' + room + '_' + window).val(obj.price);
            }
        });
        var html = '';
        // duplicate product drop down when first drop down chnaged, and when other are null
        if (window == '0') {
            for (var j = 0; j < windows; j++) {
                var window_no = j;
                if ($('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val() == '') {
                    $('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val(product_id);
                }
            }
        }

        for (var j = 0; j < windows; j++) {
            var window_no = j;
            priceCalculate(room, window_no, cat_sec_no);
        }
    }

    // $('.container-fluid').on('blur', '.misc_input', function () {
    //     var window = $(this).data('window_no');
    //     var room_no = $(this).data('room_no');
    //     var cat_sec_no = $(this).data('cat_sec_no');
    //     var misc_val = $(this).val();
    //     var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
    //     if (window == '0') {
    //         for (var j = 0; j < windows; j++) {
    //             var window_no = j;
    //             if ($('#misc_input_' + room_no + '_' + window_no + '_' + cat_sec_no).val() == '') {
    //                 $('#misc_input_' + room_no + '_' + window_no + '_' + cat_sec_no).val(misc_val);
    //             }
    //         }
    //     }

    // });
    $('.container-fluid').on('blur', '.misc_input_val', function () {
        var window = $(this).data('window_no');
        var room_no = $(this).data('room_no');
        var cat_sec_no = $(this).data('cat_sec_no');
        var misc_val = $(this).val();
        var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
        if (window == '0') {
            for (var j = 0; j < windows; j++) {
                var window_no = j;
                if ($('#price_' + room_no + '_' + window_no + '_' + cat_sec_no).val() == '') {
                    $('#price_' + room_no + '_' + window_no + '_' + cat_sec_no).val(misc_val);
                }
            }
        }

    });
    function priceCalculate(room, window, cat_sec_no) {
        var windows = $('.container-fluid .room_section_' + room + ' .no_of_windows').val();
        if (window == '0') {
            for (var j = 1; j < windows; j++) {
                var window_no = j;
                //  $('#product_id_' + room + '_' + window_no + '_' + cat_sec_no).val(product_id);
                if ($('#pattern_id_' + room + '_' + window_no + '_' + cat_sec_no).val() == '' && window_no) {
                    $('#pattern_id_' + room + '_' + window_no + '_' + cat_sec_no).val($('#pattern_id_' + room + '_0_' + cat_sec_no).val());
                    priceCalculate(room, window_no, cat_sec_no);
                }
            }
        }


        var product_id = $('#product_id_' + room + '_' + window + '_' + cat_sec_no).val();
        var patter_id = $('#pattern_id_' + room + '_' + window + '_' + cat_sec_no).val();
        var width = $('#width_' + room + '_' + window).val();
        var height = $('#height_' + +room + '_' + window).val();
        var product_price = $('#product_price_' + room + '_' + window).val();
        var windows = $('.container-fluid .room_section_' + room + ' .no_of_windows').val();
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/Quotation_controller/price_call/" + width + "/" + height + "/" + product_id + "/" + patter_id,
            type: 'get',
            success: function (r) {
                var obj = jQuery.parseJSON(r);
                $('#price_' + room + '_' + window + '_' + cat_sec_no).val(obj.price);
            }

        }
        );
    }


    $(document).ready(function () {

        $('.container-fluid').on('click', '.add_room_btn', function () {
            var room_no = $(this).data('room_no');
            var this_btn = $(this);
            if (room_no > 10) {
                alert('Exeeded max room count!');
            } else {
                //  var x = confirm('You want to add one more room ?');
                //  if (x) {
                $.ajax({
                    url: '<?php echo base_url(); ?>c_level/Quotation_controller/getMoreRoom',
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        room_no: room_no,
                    },
                    success: function (data) {
                        $('.delete_room').hide();
                        this_btn.data('room_no', data.room_cnt_ro);
                        $(data.html).insertBefore('.add_more_room_btn');
                        $('.room_' + data.room_no + ' .delete_room').show();
                    },
                    error: function (err) {
                        alert('there was some error ' + err);
                    }
                });
                //}
            }
        });
        $('.container-fluid').on('click', '.remove_item', function () {
            var room_no = $(this).data('room_no');
            var window_no = $(this).data('window_no');
            $('.detail_row_' + room_no + '').remove();
        });
        $('.container-fluid').on('click', '.add_window_btn', function () {
            var room_cnt = $(this).data('room_cnt');
            var tot_windows = $('.room_' + room_cnt + ' .no_of_window_div').children().length;
            if (tot_windows > 10) {
                alert('Exeeded max window count!');
            } else {
                var x = confirm('You want to add one more window ?');
                if (x) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>c_level/Quotation_controller/getWindowRow',
                        dataType: 'JSON',
                        type: 'POST',
                        data: {
                            room_cnt: room_cnt,
                            window_cnt: tot_windows
                        },
                        success: function (data) {
                            $('.room_' + room_cnt + ' .no_of_window_div').append(data.html);
                        },
                        error: function (err) {
                            alert('there was some error ' + err);
                        }
                    });
                }
            }
        });
        $('.container-fluid').on('change', '.category_dp', function () {
            var room_no = $(this).data('room_no');
            var cat_sec_no = parseInt($(this).data('cat_sec_no'));
            var cat_ind_no = parseInt($(this).data('cat_sec_no')) - 1;
            var category_id = $(this).val();
            var category_name = $(this).children("option:selected").text();
            var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
            $('.room_' + room_no).find('.no_of_windows').attr('disabled', 'true');
            $('.category_data_' + room_no + '_' + cat_sec_no).html('');
            var html = '';
            for (var j = 0; j < windows; j++) {
                var window_no = j;
                html += '<div class="row window_cat_' + room_no + '_' + window_no + '_' + cat_sec_no + '">';
                if (category_name == 'Misc') {
                    html += '<div class="col-sm-3"><input type="text" name="product_id[' + room_no + '][' + window_no + '][' + cat_ind_no + ']" class="form-control select2 threek product_id misc_input" id="misc_input_' + room_no + '_' + window_no + '_' + cat_sec_no + '" data-window_no="' + window_no + '" data-room_no="' + room_no + '"  data-cat_sec_no="' + cat_sec_no + '" placeholder="Product Name"></div>';
                    html += '<div class="col-sm-3"><input type="number"  name="price_id[' + room_no + '][' + window_no + '][' + cat_ind_no + ']"  id="price_' + room_no + '_' + window_no + '_' + cat_sec_no + '" value="" class="form-control price onek misc_input_val"  data-window_no="' + window_no + '" data-room_no="' + room_no + '"  data-cat_sec_no="' + cat_sec_no + '"  placeholder="Price"></div></div>';
                } else {
                    html += '<div class="col-sm-3"><select name="product_id[' + room_no + '][' + window_no + '][' + cat_ind_no + ']" class="form-control select2 threek product_id" id="product_id_' + room_no + '_' + window_no + '_' + cat_sec_no + '" onchange="getPattern(' + room_no + ',' + window_no + ',' + cat_sec_no + ')" required><option value="">--Select Product--</option>';
                    html += '</select></div>';
                    html += '<div class="col-sm-3"><select name="pattern_id[' + room_no + '][' + window_no + '][' + cat_ind_no + ']"  class="form-control select2 threek" id="pattern_id_' + room_no + '_' + window_no + '_' + cat_sec_no + '" onchange="priceCalculate(' + room_no + ',' + window_no + ',' + cat_sec_no + ')"><option value="">--Select Pattern--</option>';
                    html += '</select></div>';
                    html += '<div class="col-sm-3"><input type="text"  name="price_id[' + room_no + '][' + window_no + '][' + cat_ind_no + ']"  id="price_' + room_no + '_' + window_no + '_' + cat_sec_no + '" value="0" class="form-control price onek" readonly></div><div class="col-sm-3"><span style="color:green !important;" class="upchargebox_' + room_no + '_' + window_no + '_' + cat_sec_no + '"></span> <span class="upcharge_btn upcharge_label_' + room_no + '_' + window_no + '_' + cat_sec_no + '" data-toggle="modal" data-target="#myModal_' + room_no + '_' + window_no + '_' + cat_sec_no + '">Upcharge</span></div></div>';
                }
            }
            $('.category_data_' + room_no + '_' + cat_sec_no).html(html);
            if (category_name == 'Misc') {
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_product_by_category/" + category_id,
                    type: 'get',
                    success: function (r) {
                        for (var j = 0; j < windows; j++) {
                            var window_no = j;
                            $("#product_id_" + room_no + '_' + window_no + '_' + cat_sec_no).html(r);
                        }
                    }
                });
                //get upchardes modal
                for (var j = 0; j < windows; j++) {
                    var window_no = j;
                    if ($('#myModal_' + room_no + '_' + window_no + '_' + cat_sec_no).length) {
                        $('#myModal_' + room_no + '_' + window_no + '_' + cat_sec_no).remove();
                    }

                    var upChargeModal = '<div id="myModal_' + room_no + '_' + window_no + '_' + cat_sec_no + '" class="modal fade" role="dialog"><div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"><h4 class="modal-title">Upcharges</h4><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body">';
                    upChargeModal += '<p class=" modal_sec_' + room_no + '_' + window_no + '_' + cat_sec_no + '">Some text in the modal.</p>';
                    upChargeModal += '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="displayUpchargeAmount(\'myModal_' + room_no + '_' + window_no + '_' + cat_sec_no + '\');">Close</button></div></div></div></div>';
                    $('.page_modals').append(upChargeModal);
                }
                for (var k = 0; k < windows; k++) {
                    let window_no1 = k;
                    $.ajax({
                        url: "<?php echo base_url(); ?>c_level/Quotation_controller/getUpcharge",
                        type: 'POST',
                        dataType: 'JSON',
                        async: "false",
                        ajaxcounter: window_no1,
                        data: {
                            category_name: category_name,
                            room_no: room_no,
                            window_no: window_no1,
                            category_id: category_id,
                            cat_sec_no: cat_sec_no
                        },
                        success: function (r) {
                            $('.modal_sec_' + room_no + '_' + window_no1 + '_' + cat_sec_no).html(r.html);
                        }
                    });
                }

            }
        });
        $('.container-fluid').on('click', '.delete_category', function () {
            var cat_sec_no = $(this).data('cat_sec_no');
            var room_no = $(this).data('room_no');
            var x = confirm('Delete the category ?');
            if (x) {
                var sel_cat_id = $('.container-fluid .room_section_' + room_no + ' .category_dp_sec_' + cat_sec_no).val();
                $('.category_details_' + room_no + '_' + cat_sec_no).remove();
                $('.room_' + room_no + ' .add_category_btn').show();
            }
        });
        $('.container-fluid').on('change', '.no_of_windows', function () {
            var this_div = $(this);
            getWindows(this_div, 0);
        });
        $('.container-fluid').on('keydown', '.no_of_windows', function () {
            alert('please use up down arrow to add and remove rooms');
            return false;
        });
        function getWindows(this_div, on_blur) {
            var room_no = this_div.data('room_no');
            var windows_count = this_div.val();
            var tot_windows = $('.container-fluid .height_width_sec_' + room_no).children().length;
            if (windows_count > tot_windows) {
                $.ajax({
                    url: "<?php echo base_url(); ?>c_level/Quotation_controller/getFractionsOptList",
                    type: 'get',
                    success: function (r) {
                        var window_no = windows_count - 1;
                        var html = `
                            <div class="row window_no_` + room_no + `_` + window_no + ` mb-4">
                                <div class="col-md-5" style="padding:0px !important;">
                                    <div class="row" style="margin:0px !important">
                                        <div class="col-md-6">
                                            <label>Width</label>
                                            <input type="number" data-window_no="` + window_no + `" data-room_no="` + room_no + `"  class="form - control twok in_row width_` + room_no + `_` + window_no + ` width_input" name="width[` + room_no + `][` + window_no + `]" id="width_` + room_no + `_` + window_no + `" placeholder="Enter Width"  min="0" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>&nbsp;</label>
                                            <select id="wfraction_` + room_no + `_` + window_no + `" name="wfraction[` + room_no + `][` + window_no + `]" class="form-control select2" data-placeholder="-- select width fraction --"><option value="">--Select width fraction--</option>` + r + `</select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding:0px !important;">
                                    <div class="row" style="margin:0px !important">
                                        <div class="col-md-6">
                                            <label>Height</label>
                                            <input type="number" id="height_` + room_no + `_` + window_no + `" name="height[` + room_no + `][` + window_no + `]" data-window_no="` + window_no + `" data-room_no="` + room_no + `"  placeholder="Enter Height" class="form - control twok in_row height_` + room_no + `_` + window_no + ` height_input" min="0" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>&nbsp;</label>
                                            <select id="htfraction_` + room_no + `_` + window_no + `" name="htfraction[` + room_no + `][` + window_no + `]" class="form-control select2"  data-placeholder="-- select height fraction --"><option value="">--Select height fraction--</option>` + r + `</select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <label>&nbsp;</label>
                                    <label  data-room_no="` + room_no + `" data-window_no="` + window_no + `" class="delete_row delete_window">x</label>
                                </div>
                            </div>
                        `;
                        $('.container-fluid .height_width_sec_' + room_no).append(html);
                        $('.delete_window').hide();
                        $('.container-fluid .window_no_' + room_no + '_' + window_no + ' .delete_window').show();
                    }
                });
            } else {
                $('.delete_window').hide();
                $('.container-fluid .height_width_sec_' + room_no).children().last().remove();
                var win_show = windows_count - 1;
                $('.container-fluid .window_no_' + room_no + '_' + win_show).find('.delete_window').show();
            }

//update price when widdth height change
// find no of section, traverse through current room with all windows and all cat sections

            var cat_sec = $('.container-fluid .category_details_sec').length;
        }
        $('.container-fluid').on('click', '.delete_window', function () {
            var window_no = $(this).data('window_no');
            var room_no = $(this).data('room_no');
            if (window_no > 0) {
                var x = confirm('Delete the window no ' + window_no + '?');
                if (x) {
                    $('.window_no_' + room_no + '_' + window_no).remove();
                    $('.window_cat_' + room_no + '_' + window_no).remove();
                    $("input[name='no_of_windows[" + room_no + "]']").val(parseInt($("input[name='no_of_windows[" + room_no + "]']").val() - 1));
                    $('.delete_window').hide();
                    var show_btn_no = window_no - 1;
                    if (show_btn_no > 0) {
                        $('.container-fluid .window_no_' + room_no + '_' + show_btn_no).find('.delete_window').show();
                    }
                }

                // delete window from categories too
                // get current room and find how much category in that room
                var no_of_cats = $('.room_section_' + room_no).find('.category_details_sec').length;
                for (var k = 1; k <= no_of_cats; k++) {

                    $('.container-fluid .window_cat_' + room_no + '_' + window_no + '_' + k).remove();
                }

            }
        });
        $('.container-fluid').on('click', '.delete_room', function () {
            var room_no = $(this).data('room_no');
            var x = confirm('Delete the room ?');
            if (x) {
                $('.room_' + room_no).remove();
                $('.add_room_btn').data('room_no', $('.add_room_btn').data('room_no') - 1);
                $('.delete_room').hide();
                var r_no = parseInt(room_no) - 1;
                $('.room_' + r_no + ' .delete_room').show();
            }
        });
        /*  $.ajax({
         url: "<?php echo base_url(); ?>c_level/Quotation_controller/getCategoryDP",
         type: 'get',
         success: function (r) {
         $('.category_dp').html(r);
         }
         }); */
        $('.container-fluid').on('click', '.add_category_btn', function () {
            var isHeightWidthEntered = null;
            $('.width_input').each(function () {
                if (parseInt($(this).val()) < 0 || $(this).val() == "") {
                    isHeightWidthEntered = false;
                } else if (isHeightWidthEntered !== false) {
                    isHeightWidthEntered = true;
                }
            });
            $('.height_input').each(function () {
                if (parseInt($(this).val()) < 0 || $(this).val() == "") {
                    isHeightWidthEntered = false;
                } else if (isHeightWidthEntered !== false) {
                    isHeightWidthEntered = true;
                }
            });
            if (isHeightWidthEntered === true) {
                var room_no = $(this).data('room_no');
                var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
                var category_sections = $('.container-fluid .room_section_' + room_no + ' .category_details_sec').length;
                if (category_sections > 0) {
                    var div_sec = $('.container-fluid .room_section_' + room_no + ' .category_details_sec').last();
                    var list_secs_cl_list = div_sec.attr('class');
                    var list_secs_split = list_secs_cl_list.split(" ");
                    var list_secs = list_secs_split[list_secs_split.length - 1];
                    var last_tem = list_secs.split('_');
                    var last_item_cnt = last_tem[last_tem.length - 1];
                } else {
                    var last_item_cnt = category_sections;
                }

                var cat_sec_no = parseInt(last_item_cnt) + 1;
                var html = '<div class="col-md-4 category_details_sec sevenk category_details_' + room_no + '_' + cat_sec_no + '"><div class="select_category"><select name="category_id[' + room_no + '][]" class="form-control select2 sevenk category_dp category_dp_sec_' + cat_sec_no + '" data-room_no="' + room_no + '" required="" data-cat_sec_no="' + cat_sec_no + '"><option value="">--Select category--</option></select> <i class="simple-icon-close delete_category" data-room_no="' + room_no + '" title="Delete category" data-cat_sec_no="' + cat_sec_no + '"></i></div><div class="row sevenk category_data_' + room_no + '_' + cat_sec_no + '"></div></div>';
                $(html).insertBefore($('.add_category_section_' + room_no));
                $.ajax({
                    url: "<?php echo base_url(); ?>c_level/Quotation_controller/getCategoryDP",
                    type: 'get',
                    success: function (r) {
                        $('.container-fluid .room_section_' + room_no + ' .category_dp_sec_' + cat_sec_no).html(r);
                        // for (let sel_cat of selectedCategories) {
                        //     var tst_room = sel_cat.split('_')[0];
                        //     var tst_id = sel_cat.split('_')[1];
                        //     if (room_no == tst_room) {
                        //         $(".container-fluid .room_section_" + room_no + " .category_dp_sec_" + cat_sec_no + " option[value='" + tst_id + "']").remove();
                        //     }
                        // }
                        if ($(".container-fluid .room_section_" + room_no + " .category_dp_sec_" + cat_sec_no + " option").length < 3) {
                            $('.room_' + room_no + ' .add_category_btn').hide();
                        } else {
                            $('.room_' + room_no + ' .add_category_btn').show();
                        }
                    }
                });
            } else if (isHeightWidthEntered === false) {
                alert('height and width of windows should be selected!');
            }
        });
        $('.container-fluid').on('click', '.add_misc_btn', function () {
            var room_no = $(this).data('room_no');
            var windows = $('.container-fluid .room_section_' + room_no + ' .no_of_windows').val();
            var category_sections = $('.container-fluid .category_details_sec').length;
            var cat_sec_no = category_sections + 1;
            var html = '<div class="col-md-4 misc_details_sec sevenk misc_details_' + room_no + '_' + cat_sec_no + '"><div class="select_category"><span name="misc_id[' + room_no + '][]" class="form-control select2 sevenk category_dp category_dp_sec_' + cat_sec_no + '" data-room_no="' + room_no + '" required="" data-cat_sec_no="' + cat_sec_no + '">Misc. Charges</span> <i class="simple-icon-close delete_category" data-room_no="' + room_no + '" title="Delete category" data-cat_sec_no="' + cat_sec_no + '"></i></div><div class="row sevenk category_data_' + room_no + '_' + cat_sec_no + '"></div></div>';
            $(html).insertBefore($('.add_category_section_' + room_no));
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/getMiscDP",
                type: 'get',
                success: function (r) {
                    $('.container-fluid .category_dp_sec_' + cat_sec_no).html(r);
                }
            });
        });
    });
    // document.addEventListener('contextmenu', event => event.preventDefault());

    function displayUpchargeAmount(modalID) {
        var positionArray = modalID.split('_');
        var currentPriceID = "price_" + positionArray[1] + "_" + positionArray[2] + "_" + positionArray[3];
        var currentPrice = $("#" + currentPriceID).val();
        var totalUpcharge = 0;
        $("#" + modalID + " .amount_calc:checked").each(function () {
            var newName = $(this).attr("name");
            newName = newName.replace(/\]/g, '');
            newName = newName.replace(/\[/g, '%');
            var newNameArray = newName.split('%');
            var upchargeIDType = newNameArray[4].split('_');
            var upchargeType = upchargeIDType[1];
            if (upchargeType == "Amount") {
                totalUpcharge = totalUpcharge + parseInt($(this).val());
            } else {
                totalUpcharge = totalUpcharge + (currentPrice * (parseInt($(this).val()) / 100));
            }
        });
        if (totalUpcharge > 0) {
            $(".upchargebox_" + positionArray[1] + "_" + positionArray[2] + "_" + positionArray[3]).html('+' + totalUpcharge.toFixed(2));
        } else {
            $(".upchargebox_" + positionArray[1] + "_" + positionArray[2] + "_" + positionArray[3]).html('');
        }
    }


    function fracSelection(currItem1) {
        var currItem = currItem1;
        var hif = currItem.val().split(".")[1];
        var hif1 = currItem.val().split(".")[0];
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_height_width_fraction",
                type: 'post',
                data: {
                    hif: hif
                },
                success: function (r) {
                    currItem.parent().next().find('.select2').val(r);
                    //$("#width_fraction_id ").val(r);
                    // currItem.val(hif1);
                }, error: function () {
                    alert('error');
                }
            });
        } else {
            currItem.parent().next().find('.select2').val('');
        }
    }
    function fracSelection1(currItem1) {
        var currItem = currItem1;
        var hif = currItem.val().split(".")[1];
        var hif1 = currItem.val().split(".")[0];
        currItem.val(hif1);
    }
    $('.container-fluid').on('keyup', '.width_input, .height_input', function () {
        var currItem1 = $(this);
        fracSelection(currItem1);
    });
    $('.container-fluid').on('blur', '.width_input, .height_input', function () {
        var currItem1 = $(this);
        fracSelection1(currItem1);
    });
    function addCustomer() {

//        $.ajax({
//            url: '<?php echo base_url(); ?>c_level/Quotation_controller/add_customer',
//            type: 'POST',
//            data: {
//                addcustomer: 1
//            },
//            success: function (data) {
//                $('#Add-New-Customer-Modal .modal-body').html(data);

        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        console.log(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            console.log('place changed');
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;
                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip_code").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
        });
        $('#Add-New-Customer-Modal').modal('show');
        $('#Add-New-Customer-Modal .customer_btn').prop('disabled', false);
        $('#add-customer-form').submit(function (event) {
            var formData = $(this).serialize();
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/customer_save_new",
                data: formData,
            })
                    .done(function (response) {
                        if (response.msg == 'success') {
                            //  $('#Add-New-Customer-Modal').modal();
                            $('#Add-New-Customer-Modal').modal('hide');
//                                    $('#Add-New-Customer-Modal .modal-body').html("");
                            toastr.success("Customer info save successfully!");
                            $.ajax({
                                type: 'GET',
                                url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_customers_opt",
                            })
                                    .done(function (data) {
                                        $('#customer_id').html('');
                                        $('#customer_id').html(data);
                                        $('select#customer_id').val(response.id);
                                        $('select#customer_id').change();
                                    });
                        } else {
                            toastr.alert("Customer not saved successfully!");
                        }
                    });
            event.preventDefault();
        });
//            },
//            error: function (err) {
//                alert('there was some error ' + err);
//            }
    }
    //);
</script>


<!-- add customer script-->

<script type="text/javascript">
    //    ============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {
//        if ($('#business').is(":checked"))
//        {
//            if ($('#username').val() == '') {
//                $('#username').css({'border': '2px; solid red'}).focus();
//                return false;
//            } else {
//                $('#username').css({'border': '2px; solid green'});
//            }
//            if ($('#password').val() == '') {
//                $('#password').css({'border': '2px; solid red'}).focus();
//                return false;
//            } else {
//                $('#password').css({'border': '2px; solid green'});
//            }
//        }

        if ($('#phone_type_1').val() == '' || $('#phone_1').val() == '') {
            alert("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
        if ($('#phone_type_2').val() == '' || $('#phone_2').val() == '') {
            alert("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
        if ($('#phone_type_3').val() == '' || $('#phone_3').val() == '') {
            alert("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
    });
    //        ========== some field validation ============   
    // $('button[type=submit]').prop('disabled', true);
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '' && $("#address").val() != '') {
//            $("#first_name").css({'border': '1px solid red'}).focus();
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }
//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
//        console.log(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "<?php echo base_url(); ?>get-check-c-customer-unique-email",
            type: "post",
            data: data_string,
            success: function (data) {
//                console.log(data);
                if (data != 0) {
//                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
//                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }
    $(document).ready(function () {
        //        ================== its for state wise city show ======================
        $('body').on('change', '#state', function () {
            var state = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>state-wise-city/" + state,
                type: 'get',
                success: function (r) {
                    r = JSON.parse(r);
//                    alert(r);
                    $("#city").empty();
                    $("#city").html("<option value=''>-- select one -- </option>");
                    $.each(r, function (ar, typeval) {
                        $('#city').append($('<option>').text(typeval.city).attr('value', typeval.id));
                    });
                }
            });
        });
    });
//    ======= its for google place address geocomplete =============
    google.maps.event.addDomListener(window, 'load', function () {
        console.log('listener added');
    });
    // function validFile(){
    //     var a = $('#file_upload_1').files[0].size;
    //     alert(a);
    //     var sizee = $("#fileup_1")[0].files[0].size;
    // }

    $("body").on("change", "#file_upload_1", function (e) {

        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext != 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            alert("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }


    });
    $("body").on("change", "#file_upload_2", function (e) {

        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            alert("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }


    });
    $("body").on("change", "#file_upload_3", function (e) {

        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            alert("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }


    });
    $("body").on("change", "#file_upload_4", function (e) {

        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            alert("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }


    });
//    ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            alert("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count, e = document.createElement("tr");
            e.innerHTML = "\n\
                                     <td class='text-center'><select id='phone_type_" + count + "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value=''>Type</option><option value='Phone'>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" + count + "' class='phone form-control phone_no_type current_row' type='text' onkeyup='special_character(" + count + ")' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                    <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    count++;
//=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function (e) {
                var key = e.charCode || e.keyCode || 0;
                var phone = $(this);
                if (phone.val().length === 0) {
                    phone.val(phone.val() + '+1 (');
                }
                // Auto-format- do not expose the mask as the user begins to type
                if (key !== 8 && key !== 9) {
                    //alert("D");
                    if (phone.val().length === 6) {

                        phone.val(phone.val());
                        phone = phone;
                    }
                    if (phone.val().length === 7) {
                        phone.val(phone.val() + ') ');
                    }
                    if (phone.val().length === 12) {
                        phone.val(phone.val() + '-');
                    }
                    if (phone.val().length >= 17) {
                        phone.val(phone.val().slice(0, 16));
                    }
                }
                // Allow numeric (and tab, backspace, delete) keys only
                return (key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
            })
                    .on('focus', function () {
                        phone = $(this);
                        if (phone.val().length === 0) {

                            phone.val('+1 (');
                        } else {
                            var val = phone.val();
                            phone.val('').val(val); // Ensure cursor remains at the end
                        }
                    })

                    .on('blur', function () {
                        $phone = $(this);
                        if ($phone.val() === '(') {
                            $phone.val('');
                        }
                    });
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
        }
    }
//    ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function () {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });
            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function () {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }
//    ======== close ===========

//========== its for customer multiple file upload ==============
    function addInputFile(t) {
        var table_row_count = $("#normalItem_file > tbody > tr").length;
        var file_count = table_row_count + 1;
        file_limits = 5;
        if (file_count == file_limits) {
//            alert("You have reached the limit of adding " + file_count + " files");
            alert("You have reached the limit of adding 4 files");
        } else {

//            alert(table_row_count);
            var a = "file_upload_" + file_count, e = document.createElement("tr");
            e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                    <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    file_count++;
        }
    }
    function file_deleteRow(t) {
        var a = $("#normalItem_file > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_file_count = 1;
            $("#normalItem_file > tbody > tr td input.current_file_count").each(function () {
                current_file_count++;
                $(this).attr('class', 'form-control');
                $(this).attr('id', 'file_upload_' + current_file_count);
                $(this).attr('name', 'file_upload_' + current_file_count);
            });
        }
    }

    //=========== its for get special character =========
    function special_character(t) {
//        alert(t);
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            alert(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }




    function offset(el) {
        var rect = el.getBoundingClientRect(),
                scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
                scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return {top: rect.top + scrollTop, left: rect.left + scrollLeft}
    }
    var div1 = document.querySelector('#address');
    var divOffset = parseInt(offset(div1)) + parseInt(80);
    console.log('offset is ' + divOffset);
    // $('.pac-container .pac-logo').css('top', divOffset + 'px');

    $('#address').on('keyup', function () {
        if ($(this).parent().find('pac-container') > 0) {
        } else {
            var pacContainer = $('.pac-container');
            $('#address').parent().append(pacContainer);
        }
    });

</script>

<!--  add customer script end-->