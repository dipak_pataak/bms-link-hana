<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>BMS Link</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/iconsmind/style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/simple-line-icons/css/simple-line-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/fullcalendar.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/datatables.responsive.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/select2.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/select2-bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/perfect-scrollbar.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/owl.carousel.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-stars.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/nouislider.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-datepicker3.min.css" />
        <!-- timepicker -->
        <link href="<?php echo base_url() ?>assets/c_level/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/main.css" />


        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>
        <script type="text/javascript">
            var mybase_url = '<?php echo base_url(); ?>';
        </script>
    </head>

    <body id="app-container" class="menu-default show-spinner">
        <nav class="navbar fixed-top">
            <a href="#" class="menu-button d-none d-md-block">
                <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1" />
                <rect x="0.48" y="7.5" width="7" height="1" />
                <rect x="0.48" y="15.5" width="7" height="1" />
                </svg>
                <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1" />
                <rect x="1.56" y="7.5" width="16" height="1" />
                <rect x="1.56" y="15.5" width="16" height="1" />
                </svg>
            </a>

            <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1" />
                <rect x="0.5" y="7.5" width="25" height="1" />
                <rect x="0.5" y="15.5" width="25" height="1" />
                </svg>
            </a>

            <div class="search" data-search-path="Layouts.Search.html?q=">
                <input placeholder="Search...">
                <span class="search-icon">
                    <i class="simple-icon-magnifier"></i>
                </span>
            </div>

            <a class="navbar-logo" href="<?php echo base_url(); ?>c-level-dashboard">
                <img src="<?php echo base_url(); ?>assets/c_level/img/logo.png" alt="">
            </a>

            <div class="ml-auto">
                <div class="header-icons d-inline-block align-middle">

                    <div class="position-relative d-none d-sm-inline-block">
                        <button class="header-icon btn btn-empty" type="button" id="iconMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="simple-icon-grid"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right mt-3  position-absolute" id="iconMenuDropdown">
                            <a href="<?php echo base_url(); ?>customer-list" class="icon-menu-item">
                                <i class="iconsmind-MaleFemale d-block"></i>
                                <span>Customers</span>
                            </a>

                            <a href="<?php echo base_url(); ?>manage-order" class="icon-menu-item">
                                <i class="iconsmind-Address-Book2 d-block"></i>
                                <span>Orders</span>
                            </a>

                            <a href="<?php echo base_url(); ?>appointment-form" class="icon-menu-item">
                                <i class="iconsmind-Clock d-block"></i>
                                <span>Appointment</span>
                            </a>

                            <a href="<?php echo base_url(); ?>account-chart" class="icon-menu-item">
                                <i class="iconsmind-Pencil d-block"></i>
                                <span>Accounts</span>
                            </a>

                            <a href="<?php echo base_url(); ?>payment-setting" class="icon-menu-item">
                                <i class="iconsmind-Settings-Window d-block"></i>
                                <span>Payment Settings</span>
                            </a>

                            <a href="<?php echo base_url(); ?>faq" class="icon-menu-item">
                                <i class="iconsmind-Suitcase d-block"></i>
                                <span>Knowledge Base</span>
                            </a>
                        </div>
                    </div>

                    <div class="position-relative d-inline-block">
                        <button class="header-icon btn btn-empty" type="button" id="notificationButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="simple-icon-bell"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right mt-3 scroll position-absolute" id="notificationDropdown">

                            <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-2.jpg" alt="Notification Image" class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                </a>
                                <div class="pl-3 pr-2">
                                    <a href="#">
                                        <p class="font-weight-medium mb-1">Joisse Kaycee just sent a new comment!</p>
                                        <p class="text-muted mb-0 text-small">9/27/2018 - 12:45</p>
                                    </a>
                                </div>
                            </div>

                            <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/c_level/img/notification-thumb.jpg" alt="Notification Image" class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                </a>
                                <div class="pl-3 pr-2">
                                    <a href="#">
                                        <p class="font-weight-medium mb-1">1 item is out of stock!</p>
                                        <p class="text-muted mb-0 text-small">9/27/2018 - 12:45</p>
                                    </a>
                                </div>
                            </div>


                            <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/c_level/img/notification-thumb-2.jpg" alt="Notification Image" class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                </a>
                                <div class="pl-3 pr-2">
                                    <a href="#">
                                        <p class="font-weight-medium mb-1">New order received! It is total $147,20.</p>
                                        <p class="text-muted mb-0 text-small">9/27/2018 - 12:45</p>
                                    </a>
                                </div>
                            </div>

                            <div class="d-flex flex-row mb-3 pb-3 ">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/c_level/img/notification-thumb-3.jpg" alt="Notification Image" class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                </a>
                                <div class="pl-3 pr-2">
                                    <a href="#">
                                        <p class="font-weight-medium mb-1">3 items just added to wish list by a user!</p>
                                        <p class="text-muted mb-0 text-small">9/27/2018 - 12:45</p>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <button class="header-icon btn btn-empty d-none d-sm-inline-block" type="button" id="fullScreenButton">
                        <i class="simple-icon-size-fullscreen"></i>
                        <i class="simple-icon-size-actual"></i>
                    </button>

                </div>

                <div class="user d-inline-block">
                    <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        <span class="name"><?php echo $this->session->userdata('name'); ?></span>
                        <span>
                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l.jpg" />
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right mt-4">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>company-profile"><i class="glyph-icon simple-icon-user"></i>Profile</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>my-orders"><i class="glyph-icon simple-icon-puzzle"></i>My Orders</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>logs"><i class="glyph-icon simple-icon-refresh"></i>Logs</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>logout"><i class="glyph-icon simple-icon-action-redo"></i>Sign out</a>
                    </div>
                </div>
            </div>
        </nav>