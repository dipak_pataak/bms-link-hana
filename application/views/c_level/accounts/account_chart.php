
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="mb-3">
                        <h1>Account Chart</h1>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Chart</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="separator mb-5"></div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-xl-12 mb-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <ul id="tree1">
                                        <li><a href="#">TECH</a>
                                            <ul>
                                                <li>Company Maintenance</li>
                                                <li>Employees
                                                    <ul>
                                                        <li>Reports
                                                            <ul>
                                                                <li>Report1</li>
                                                                <li>Report2</li>
                                                                <li>Report3</li>
                                                            </ul>
                                                        </li>
                                                        <li>Employee Maint.</li>
                                                    </ul>
                                                </li>
                                                <li>Human Resources</li>
                                            </ul>
                                        </li>
                                        <li><a href="#">XRP</a>
                                            <ul>
                                                <li>Company Maintenance</li>
                                                <li>Employees
                                                    <ul>
                                                        <li>Reports
                                                            <ul>
                                                                <li>Report1</li>
                                                                <li>Report2</li>
                                                                <li>Report3</li>
                                                            </ul>
                                                        </li>
                                                        <li>Employee Maint.</li>
                                                    </ul>
                                                </li>
                                                <li>Human Resources</li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Tree View One</a>
                                            <ul>
                                                <li>Company Maintenance</li>
                                                <li>Employees
                                                    <ul>
                                                        <li>Reports
                                                            <ul>
                                                                <li>Report1</li>
                                                                <li>Report2</li>
                                                                <li>Report3</li>
                                                            </ul>
                                                        </li>
                                                        <li>Employee Maint.</li>
                                                    </ul>
                                                </li>
                                                <li>Human Resources</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
            
        </div>
    </main>
