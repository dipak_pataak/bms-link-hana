<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Account Chart</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Chart</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <ul id="tree1">
                                    <?php
                                    $visit = array();

                                    for ($i = 0; $i < count($userList); $i++) {
                                        $visit[$i] = false;
                                    }

                                    $this->Account_model->dfs("COA", "0", $userList, $visit, 0);
                                    
                                    ?>
                                </ul>
                            </div>
                            <?php // if ($this->permission->method('accounts', 'update')->access() || $this->permission->method('accounts', 'create')->access()): ?>
                            <div class="col-md-6" id="newform"></div>
                            <?php // endif; ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</main>

<script>
    $(document).ready(function () {
        "use strict"; // Start of use strict

        $.fn.extend({
            treed: function (o) {

                var openedClass = 'simple-icon-people';
                var closedClass = 'fa-folder-o';

                if (typeof o !== 'undefined') {
                    if (typeof o.openedClass !== 'undefined') {
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass !== 'undefined') {
                        closedClass = o.closedClass;
                    }
                }
                ;

                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator fa " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this === e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    });
                    branch.children().children().toggle();
                });
                //fire event from the dynamically added icon
                tree.find('.branch .indicator').each(function () {
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });

        //Initialization of treeviews

        $('#tree3').treed({openedClass: 'fa-folder-open-o', closedClass: 'fa-folder'});

    });

</script>


<script type="text/javascript">
    function loadData(id) {
//         alert(id);
        $.ajax({
            url: "<?php echo site_url('show-selected-form/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {

                $('#newform').html(data);
                $('#btnSave').hide();

            },
            error
            : function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }


</script>
<script type="text/javascript">
    function newdata(id) {
        $.ajax({
            url: "<?php echo site_url('new-account-form/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {
                // console.log(data.headcode);
                console.log(data.rowdata);
                var headlabel = data.headlabel;
                $('#txtHeadCode').val(data.headcode);
                document.getElementById("txtHeadName").value = '';
                $('#txtPHead').val(data.rowdata.HeadName);
                $('#txtHeadLevel').val(headlabel);
                $('#btnSave').prop("disabled", false);
                $('#btnSave').show();
                $('#btnUpdate').hide();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

</script>