
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Contra Voucher</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Contra Voucher</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <?php // dd($contraCrebitVoucher_edit); ?>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>update-contra-voucher" method="post">
                            <div class="form-group row">
                                <label for="vo_no" class="col-sm-2 col-form-label">Voucher No</label>
                                <div class="col-sm-4">
                                    <input type="text" name="txtVNo" id="txtVNo" value="<?php echo $contraCrebitVoucher_edit[0]->VNo; ?>" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date" class="col-sm-2 col-form-label">Date</label>
                                <div class="col-sm-4">
                                    <input type="text" name="dtpDate" id="dtpDate" class="form-control datepicker" value="<?php echo $contraCrebitVoucher_edit[0]->VDate; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="txtRemarks" class="col-sm-2 col-form-label">Remark</label>
                                <div class="col-sm-4">
                                    <textarea name="txtRemarks" id="txtRemarks" class="form-control"><?php echo $contraCrebitVoucher_edit[0]->Narration; ?></textarea>
                                </div>
                            </div>
                            <div class="table-responsive" style="margin-top: 10px">
                                <table class="table table-bordered table-hover" id="debtAccVoucher">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Account Name</th>
                                            <th class="text-center">Code</th>
                                            <th class="text-center">Debit</th>
                                            <th class="text-center">Credit</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="debitvoucher">
                                        <?php
                                        $sl = 1;
                                        $dbt = $cdt = 0;
                                        foreach ($contraCrebitVoucher_edit as $single) {
                                            ?>
                                            <tr>
                                                <td class="" style="width: 200px;">  
                                                    <select name="cmbCode[]" id="cmbCode_<?php echo $sl; ?>" class="form-control select2-single" onchange="load_code(this.value,<?php echo $sl; ?>)">
                                                        <?php foreach ($acc as $acc1) { ?>
                                                            <option value="<?php echo $acc1->HeadCode; ?>" <?php
                                                            if ($single->COAID == $acc1->HeadCode) {
                                                                echo 'selected';
                                                            }
                                                            ?>><?php echo $acc1->HeadName; ?></option>
                                                                <?php } ?>
                                                    </select>

                                                </td>
                                                <td><input type="text" name="txtCode[]"  class="form-control text-center"  id="txtCode_<?php echo $sl; ?>" required value="<?php echo $single->COAID; ?>"></td>
                                                <td><input type="text" name="txtAmount[]" class="form-control total_price text-right" value="<?php echo $single->Debit;
                                                            $dbt += $single->Debit; ?>" placeholder="0"  id="txtAmount_<?php echo $sl; ?>" onkeyup="calculation(<?php echo $sl; ?>)" >
                                                </td>
                                                <td>
                                                    <input type="text" name="txtAmountcr[]"  class="form-control total_price1 text-right" value="<?php echo $single->Credit;
                                                            $cdt += $single->Credit; ?>" placeholder="0"  id="txtAmount1_1" onkeyup="calculation(<?php echo $sl; ?>)" >
                                                </td>
                                                <td>
                                                    <button style="text-align: right;" class="btn btn-danger red btn-xs" type="button" value="Delete" onclick="deleteRow(this)"><i class="glyph-icon simple-icon-trash"></i></button>
                                                </td>
                                            </tr>                              
                                            <?php
                                            $sl++;
                                        }
                                        ?>
                                    </tbody>                               
                                    <tfoot>
                                        <tr>
                                            <td >
                                                <input type="button" id="add_more" class="btn btn-info btn-xs" name="add_more"  onClick="addaccount('debitvoucher');" value="Add More" />
                                            </td>
                                            <td colspan="1" class="text-right"><label  for="reason" class="  col-form-label">Total</label>
                                            </td>
                                            <td class="text-right">
                                                <input type="text" id="grandTotal" class="form-control text-right " name="grand_total" value="<?php echo number_format($dbt, 2, '.', ','); ?>" readonly="readonly"  placeholder="0"/>
                                            </td>
                                            <td class="text-right">
                                                <input type="text" id="grandTotal1" class="form-control text-right " name="grand_total1" value="<?php echo number_format($cdt, 2, '.', ','); ?>" readonly="readonly" placeholder="0"/>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 text-right">
                                    <input type="submit" id="add_receive" class="btn btn-success btn-sm" name="save" value="Update" tabindex="9" />
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
<script type="text/javascript">

    function load_code(id, sl) {

        $.ajax({
            url: "<?php echo site_url('debit-voucher-code/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {

                $('#txtCode_' + sl).val(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }
    function addaccount(divName) {
        var row = $("#debtAccVoucher tbody tr").length;
        var count = row + 1;
        var limits = 500;
        var tabin = 0;
        if (count == limits)
            alert("You have reached the limit of adding " + count + " inputs");
        else {
            var newdiv = document.createElement('tr');
            var tabin = "cmbCode_" + count;
            var tabindex = count * 2;
            newdiv = document.createElement("tr");

            newdiv.innerHTML = "<td> <select name='cmbCode[]' id='cmbCode_" + count + "' class='form-control' onchange='load_code(this.value," + count + ")'><?php foreach ($acc as $acc2) { ?><option value='<?php echo $acc2->HeadCode; ?>'><?php echo $acc2->HeadName; ?></option><?php } ?></select></td><td><input type='text' name='txtCode[]' class='form-control text-center'  id='txtCode_" + count + "' ></td><td><input type='text' name='txtAmount[]' class='form-control total_price text-right' value='' placeholder='0' id='txtAmount_" + count + "' onkeyup='calculation(" + count + ")'></td><td><input type='text' name='txtAmountcr[]' class='form-control total_price1 text-right' id='txtAmount1_" + count + "' value='' placeholder='0' onkeyup='calculation(" + count + ")'></td><td><button style='text-align: right;' class='btn btn-danger red btn-xs' type='button' value='Delete' onclick='deleteRow(this)'><i class='glyph-icon simple-icon-trash'></i></button></td>";
            document.getElementById(divName).appendChild(newdiv);
            document.getElementById(tabin).focus();
            count++;

            $("select.form-control:not(.dont-select-me)").select2({
                placeholder: "Select option",
                allowClear: true
            });
        }
    }

    function calculation(sl) {
        var gr_tot1 = 0;
        var gr_tot = 0;
        $(".total_price").each(function () {
            isNaN(this.value) || 0 == this.value.length || (gr_tot += parseFloat(this.value))
        });

        $(".total_price1").each(function () {
            isNaN(this.value) || 0 == this.value.length || (gr_tot1 += parseFloat(this.value))
        });
        $("#grandTotal").val(gr_tot.toFixed(2, 2));
        $("#grandTotal1").val(gr_tot1.toFixed(2, 2));
    }

    function deleteRow(e) {
        var t = $("#debtAccVoucher > tbody > tr").length;
        if (1 == t)
            alert("There only one row you can't delete.");
        else {
            var a = e.parentNode.parentNode;
            a.parentNode.removeChild(a)
        }
        calculation()
    }

</script>