<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Voucher Approval</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Account</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Credit</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Voucher No</th>
                                    <th>Remark</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($aprrove))  ?>
                                <?php $sl = 1; ?>
                                <?php foreach ($aprrove as $approve) { ?>
                                    <tr>
                                        <td><?php echo $sl++; ?></td>
                                        <td><?php echo $approve->VNo; ?></td>
                                        <td><?php echo $approve->Narration; ?></td>
                                        <td><?php echo $approve->Debit; ?></td>
                                        <td><?php echo $approve->Credit; ?></td>
                                        <td>

                                            <a href="<?php echo base_url("isactive/$approve->VNo/active") ?>" onclick="return confirm('Are you sure?')" class="btn btn-warning btn-sm" title="Approved">Approved</a>
                                            <a href="<?php echo base_url("voucher-edit/$approve->VNo") ?>" class="btn btn-info btn-sm" title="Edit"><i class="simple-icon-pencil"></i></a>

                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                            <?php
                            if (empty($aprrove)) {
                                ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-center text-danger" colspan="6">Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>
</main>