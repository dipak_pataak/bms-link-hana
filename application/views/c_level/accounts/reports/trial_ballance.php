<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 166.132px !important;
        left: 413.455px;
        z-index: 10;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Trial Ballance</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Trial Ballance</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <?= form_open_multipart('c-trial-balance-report/') ?>
                        <div class="row" id="">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="date" class="col-sm-4 col-form-label">From Date</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="dtpFromDate" value="" placeholder="From Date" class="datepicker form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="date" class="col-sm-4 col-form-label">To Date</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="dtpToDate" value="" placeholder="To Date" class="datepicker form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="date" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" id="chkWithOpening" name="chkWithOpening" size="40"/>&nbsp;&nbsp;&nbsp;<label for="chkWithOpening">With Details</label>
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Find</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>