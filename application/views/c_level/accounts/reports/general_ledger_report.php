<script type="text/javascript">
    function printDiv() {
        var divName = "printArea";
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        // document.body.style.marginTop="-45px";
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>General Ledger</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">General Ledger</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="printArea">
                            <table width="99%" align="center" style="margin:5px 5px;" cellpadding="5" cellspacing="5" border="2"> 

                                <thead>
                                    <tr align="center">

                                        <td colspan="7"><font size="+1" style="font-family:'Arial'"> 
                                            <strong><?php echo "General Ledger of" . ' ' . $ledger->HeadName . '  ' . date('M-d-Y', strtotime($dtpFromDate)) . ' To ' . date('M-d-Y', strtotime($dtpToDate)); ?></strong>
                                            </font><strong></th></strong>
                                    </tr>

                                    <tr>
                                        <td height="25"><strong>SL#</strong></td>
                                        <td><strong><?php echo $Trans ? "Transaction Date" : "Head Code"; ?></strong></td>
                                        <td><strong><?php echo $Trans ? "Voucher No" : "Head Name"; ?></strong></td>
                                        <?php
                                        if ($chkIsTransction) {
                                            ?>
                                            <td><strong><?php echo "Particulars";  ?></strong></td>
                                            <?php
                                        }
                                        ?>
                                        <td align="right"><strong>Debit</strong></td>
                                        <td align="right"><strong>Credit</strong></td>
                                        <td align="right"><strong><?php echo 'Balance'; ?></strong></td>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if ($error) {
                                        ?>

                                        <tr>
                                            <td height="25"></td>
                                            <td></td>
                                            <td><?php echo "No report" ?>.</td>
                                            <?php
                                            if ($chkIsTransction) {
                                                ?>
                                                <td></td>
                                                <?php
                                            }
                                            ?>
                                            <td align="right"></td>
                                            <td align="right"></td>
                                            <td align="right"></td>
                                        </tr>

                                        <?php
                                    } else {
                                        $TotalCredit = 0;
                                        $CurBalance = $prebalance;
                                        foreach ($HeadName2 as $key => $data) {
                                            ?>
                                            <tr>
                                                <td height="25"><?php echo ++$key; ?></td>
                                                <td><?php echo $data->COAID; ?></td>
                                                <td><?php echo $data->HeadName; ?></td>
                                                <?php
                                                if ($chkIsTransction) {
                                                    ?>
                                                    <td><?php echo $data->Narration; ?></td>
                                                    <?php
                                                }
                                                ?>

                                                <td align="right"><?php echo number_format($data->Debit, 2, '.', ','); ?></td>
                                                <td align="right"><?php echo number_format($data->Credit, 2, '.', ','); ?></td>
                                                <?php
                                                $TotalDebit += $data->Debit;
                                                $CurBalance += $data->Debit;

                                                $TotalCredit += $data->Credit;
                                                $CurBalance -= $data->Credit;
                                                ?>
                                                <td align="right"><?php echo number_format($CurBalance, 2, '.', ','); ?></td>
                                            </tr>
                                        <?php } ?>

                                    <tfoot>
                                        <tr class="table_data">
                                            <?php
                                            if ($chkIsTransction)
                                                $colspan = 4;
                                            else
                                                $colspan = 3;
                                            ?>
                                            <td colspan="<?php echo $colspan; ?>" align="right"><strong>Total</strong></td>                    
                                            <td align="right"><strong><?php echo number_format($TotalDebit, 2, '.', ','); ?></strong></td>
                                            <td align="right"><strong><?php echo number_format($TotalCredit, 2, '.', ','); ?></strong></td>
                                            <td align="right"><strong><?php echo number_format($CurBalance, 2, '.', ','); ?></strong></td>
                                        </tr>
                                    </tfoot>
                                    <?php
                                }
                                ?>
                                </tbody>
                                <h4>
                                    Pre Balance : <?php echo number_format($prebalance, 2, '.', ','); ?>
                                    <br /> Current Balance : <?php echo number_format($CurBalance, 2, '.', ','); ?>
                                </h4>
                            </table>
                        </div>
                        <div class="text-center" id="print" style="margin: 20px">
                            <input type="button" class="btn btn-warning" name="btnPrint" id="btnPrint" value="Print" onclick="printDiv();"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>