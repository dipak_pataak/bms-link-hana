<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 140.147px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Voucher Reports</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Voucher Reports</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-body">
                        <div class="row" id="">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="date" class="col-sm-4 col-form-label">Date</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="sales_date" placeholder="Date" class="datepicker form-control serach_date">
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success btn-sm w-md m-b-5" id="btnSerach">Find</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <table class="datatable2 table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Voucher No</th>
                                    <th>Remarks</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $date = date('Y-m-d');
                                ?>
                                <tr id="show_vouchar">
                                    <td>
                                        <a href="<?php echo base_url("c-vouchar-cash/" . $date) ?>">
                                            <?php echo "CV-BAC-" . date('M-d-Y', strtotime($date)); ?>
                                        </a>
                                    </td>
                                    <td>Aggregated Cash Credit Voucher of <?php echo date('M-d-Y', strtotime($date)); ?></td>
                                    <td><?php
                                        if ($get_cash->Amount == '') {
                                            echo '0.00';
                                        } else {
                                            echo $get_cash->Amount;
                                        }
                                        ?></td>
                                    <td align="center"><?php echo date('M-d-Y', strtotime($date)); ?></td>
                                </tr>
                                <?php
                                foreach ($get_vouchar as $v_data) {
                                    ?>
                                    <tr>
                                        <td><a href="<?php echo base_url("accounts/accounts/vouchar_view/$v_data->VNo") ?>"><?php echo $v_data->VNo;
                                    ; ?></a></td>
                                        <td><?php echo $v_data->Narration; ?></td>
                                        <td><?php echo number_format($v_data->Amount); ?></td>
                                        <td><?php echo $v_data->VDate; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btnSerach').on('click',function(){
            var vouchar=$("#sales_date").val();
            $.ajax({
                url: '<?php echo site_url('c-voucher-report-serach'); ?>',
                type: 'POST',
                data: {
                    vouchar: vouchar
                },
                success: function (data) {
                    $("#show_vouchar").html(data);
                }
            });

        });
    });


</script>