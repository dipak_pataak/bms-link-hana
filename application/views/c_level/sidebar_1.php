<?php

$user_id = $this->session->userdata('user_id');
$user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();
//dd($user_id);
?>
<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="<?php if($this->uri->segment(1) == 'c-level-dashboard'){ echo 'active'; }?>" id="remove_active">
                    <?php
//                    ========= its for only dashboard show by statically ===============
                    $allmoduless = $this->db->select('*')->from('menusetup_tbl')->where('menu_type', 1)
                                    ->group_by('module')->order_by('ordering', 'asc')->get()->result();
                      if ($user_info[0]->language == 'English') {
                        $menu_title = $allmoduless[0]->menu_title;
                    } elseif ($user_info[0]->language == 'Korean') {
                        $menu_title = $allmoduless[0]->korean_name;
                    } else {
                        $menu_title = $allmoduless[0]->menu_title;
                    }
                    ?>
                    <a href="<?php echo base_url(); ?>c-level-dashboard">
                        <i class="iconsmind-Shop-4"></i>
                        <span><?php echo ucfirst($menu_title); ?></span>
                    </a>
                </li>

                <?php
                $allmodule = $this->db->select('*')->from('menusetup_tbl')->where('menu_type', 1)->where('status', 1)
                                ->group_by('module')->order_by('ordering', 'asc')->get()->result();
                //$i = 0;
                foreach ($allmodule as $module) {
                    $menu_item = $this->db->select('*')
                                    ->from('menusetup_tbl')
                                    ->where('module', $module->module)
                                    ->where('parent_menu =', $module->id)
                                    ->where('status', 1)
                                    ->order_by('ordering', 'asc')
                                    ->get()->result();
                    $module_id = $module->parent_menu;

                    if ($user_info[0]->language == 'English') {
                        $menu_title = $module->menu_title;
                    } elseif ($user_info[0]->language == 'Korean') {
                        $menu_title = $module->korean_name;
                    } else {
                        $menu_title = $module->menu_title;
                    }
                    // $i++;
                    if (empty($menu_item)) {
                        $link = base_url() . $module->page_url;
                    } else {
                        $link = "#" . $module->module;
                    }
                    if ($this->permission->module($module->module)->access()) {
                        ?>
                        <li class="" id="<?php echo $module->module; ?>">
                            <a href="<?php echo $link; ?>">
                                <i class="<?php echo $module->icon; ?>"></i> <?php echo ucfirst(@$menu_title); ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>
                <?php if ($this->session->userdata('isAdmin') == '1') { ?>
                    <!--                    <li>
                                            <a href="#setting">
                                                <i class="simple-icon-settings"></i> Settings
                                            </a>
                                        </li>-->
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <?php
            foreach ($allmodule as $module) {
                $menu_item = $this->db->select('*')
                                ->from('menusetup_tbl')
                                ->where('module', $module->module)
                                ->where('parent_menu =', $module->id)
                                ->where('status', 1)
                                ->order_by('ordering', 'asc')
                                ->get()->result();
                $module_id = $module->parent_menu;
//              
                ?>
                <ul class="list-unstyled" data-link="<?php echo $module->module; ?>">
                    <?php
                    foreach ($menu_item as $menu) {
                        if ($user_info[0]->language == 'English') {
                            $submenu_title = $menu->menu_title;
                        } elseif ($user_info[0]->language == 'Korean') {
                            $submenu_title = $menu->korean_name;
                        } else {
                            $submenu_title = $menu->menu_title;
                        }
//                        var_dump($this->permission->check_label($menu->menu_title)->access());exit();
                        if ($this->permission->check_label($menu->menu_title)->access()) {
                            $parent_id = $menu->id;
                            $sub_sub_menu = $this->db->select('*')
                                            ->from('menusetup_tbl')
                                            ->where('module', $menu->module)
                                            ->where('parent_menu =', $parent_id)
                                            ->where('status', 1)
                                            ->order_by('ordering', 'asc')
                                            ->get()->result();
                            ?>
                            <li>
                                <?php if (!empty($sub_sub_menu)) { ?>
                                    <a href="#" class="menu-link"> 
                                        <?php echo ucwords(str_replace('_', ' ', $submenu_title)); ?> <i class="simple-icon-arrow-down"></i>
                                    </a>

                                    <ul class="list-unstyled">
                                        <li>
                                            <?php
                                            foreach ($sub_sub_menu as $child) {
                                                if ($user_info[0]->language == 'English') {
                                                    $childmenu_title = $child->menu_title;
                                                } elseif ($user_info[0]->language == 'Korean') {
                                                    $childmenu_title = $child->korean_name;
                                                } else {
                                                    $childmenu_title = $child->menu_title;
                                                }
//                                                var_dump($this->permission->check_label($child->menu_title)->access());exit();
                                                if ($this->permission->check_label($child->menu_title)->access()) {
                                                    ?>
                                                    <a href="<?php echo base_url(); ?><?php echo $child->page_url; ?>">
                                                        <?php echo ucwords(str_replace('_', ' ', $childmenu_title)); ?>
                                                    </a>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                <?php } else {
                                    ?>
                                    <a href="<?php echo base_url(); ?><?php echo $menu->page_url; ?>" class="menu-link">
                                        <?php echo ucwords(str_replace('_', ' ', @$submenu_title)); ?>
                                    </a>
                                <?php } ?>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            <?php } ?>

            <?php if ($this->session->userdata('isAdmin') == '1') { ?>
                <!--                <ul class="list-unstyled" data-link="setting">
                                    <li>
                                        <a href="<?php echo base_url(); ?>company-profile"> Company Profile Info</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>payment-setting"> Payment Option</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>c-cost-factor"> Cost Factor</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>iframe-code"> Web Iframe Code</a>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-link"> User &amp; Access<i class="simple-icon-arrow-down"></i></a>
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="<?php echo base_url(); ?>users">Users</a>
                                                <a href="<?php echo base_url(); ?>menu-setup">Menu Setup</a>
                                                <a href="<?php echo base_url(); ?>role-permission">Role Permission</a>
                                                <a href="<?php echo base_url(); ?>role-list">Role List</a>
                                                <a href="<?php echo base_url(); ?>user-role">Add User Roles</a> 
                                                <a href="<?php echo base_url(); ?>access-role">Access Role</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>c-us-state"> US State</a>
                                    </li>
                                </ul>-->
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var segment = "<?php echo $this->uri->segment(1) ?>";
        var segment2 = "<?php echo $this->uri->segment(2) ?>";
        if (segment == 'c-level-dashboard') {
            $("#dashboard").addClass("active");
        } else if (segment == 'add-customer' || segment == 'customer-edit' || segment == 'customer-view' || segment == 'customer-list' || segment == 'c-customer-bulk-upload') {
            $("#customer").addClass("active");
        } else if (segment == 'new-quotation' || segment == 'manage-order' || segment2 == 'invoice_receipt' || segment == 'manage-invoice' || segment == 'order-cancel' || segment == 'customer-return' || segment == 'track-order' || segment2 == 'order_controller' || segment2 == 'make_payment' || segment == 'c-customer-order-return') {
            $("#order").addClass("active");
        } else if (segment2 == 'quotation_controller') {
            $("#quotation").addClass('active');
        } else if (segment == 'chart-of-account' || segment == 'account-chart' || segment == 'voucher-debit' || segment == 'voucher-credit' || segment == 'voucher-journal' || segment == 'contra-voucher' || segment == 'voucher-approval' || segment == 'cash-book' || segment == 'bank-book' || segment == 'cash-flow' || segment == 'voucher-reports' || segment == 'general-ledger' || segment == 'profit-loss' || segment == 'trial-balance' || segment == 'c-accounts-report-search' || segment == 'c-cash-flow-report-search' || segment == 'c-profit-loss-report-search' || segment == 'c-trial-balance-report' || segment == 'voucher-edit') {
            $("#account").addClass('active');
        } else if (segment == 'company-profile' || segment == 'payment-setting' || segment == 'menu-setup' || segment == 'c-cost-factor' || segment == 'iframe-code' || segment == 'users' || segment == 'add-user' || segment == 'user-edit' || segment == 'menusetup-edit' || segment2 == 'Setting_controller' || segment2 == 'setting_controller' || segment == 'c-gateway-edit' || segment == 'role-permission' || segment == 'role-list' || segment == 'role-edit' || segment == 'edit-user-access-role' || segment == 'user-role' || segment == 'access-role' || segment == 'c-us-state' || segment == 'c-us-state-edit' || segment == 'c-my-account' || segment == 'logs' || segment == 'my-orders' || segment == 'c-password-change' || segment == 'appointment-form' || segment == 'payment-gateway' || segment2 == 'notification' || segment == 'c-menu-setup' || segment == 'c-menusetup-edit') {
            $("#settings").addClass('active');
        }
    });
</script>