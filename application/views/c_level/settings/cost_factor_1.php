
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Cost Factor</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cost Factor</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="p-1">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <div class="table-responsive">
                            <form action="<?php echo base_url(); ?>c-cost-factor-save" method="post">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Product List</th>
                                            <th class="text-right">Cost Factor %</th>
                                            <th class="text-right">Discount %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        if (empty($get_only_product_c_cost_factor)) {
                                            foreach ($get_product as $single_product) {
                                                $i++;
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $single_product->product_name; ?>
                                                        <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single_product->product_id; ?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="individual_cost_factor[]" id="individual_cost_factor_<?php echo $i; ?>" class="form-control text-right" onkeyup="cost_factor_calculation(this.value, '<?php echo $i; ?>')" value="<?php // echo $single_product->individual_cost_factor;       ?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="costfactor_discount[]" id="costfactor_discount_<?php echo $i; ?>" class="form-control text-right" value="<?php // echo $single_product->costfactor_discount;       ?>" readonly>
                                                    </td>
                                                    <?php // } ?>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            $product_ids = '';
                                            $i = 0;
                                            foreach ($get_only_product_c_cost_factor as $cost_factor) {
                                                $product_ids .= "'" . $cost_factor->product_id . "',";
                                                $i++;
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $cost_factor->product_name; ?>
                                                        <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $cost_factor->product_id; ?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="individual_cost_factor[]" id="individual_cost_factor_<?php echo $i; ?>" class="form-control text-right" onkeyup="cost_factor_calculation(this.value, '<?php echo $i; ?>')" value="<?php echo $cost_factor->individual_cost_factor; ?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="costfactor_discount[]" id="costfactor_discount_<?php echo $i; ?>" class="form-control text-right" value="<?php echo $cost_factor->costfactor_discount; ?>" readonly>
                                                    </td>
                                                    <?php // } ?>
                                                </tr>
                                                <?php
                                            }
                                            $pids = rtrim($product_ids, ',');
                                            $sql = "SELECT * FROM product_tbl WHERE product_id NOT IN ($pids)";
                                            $sql_result = $this->db->query($sql)->result();
                                            $j=$i;
                                            foreach ($sql_result as $cost_factor) {
                                                $j++;
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $cost_factor->product_name; ?>
                                                        <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $cost_factor->product_id; ?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="individual_cost_factor[]" id="individual_cost_factor_<?php echo $j; ?>" class="form-control text-right" onkeyup="cost_factor_calculation(this.value, '<?php echo $j; ?>')" value="0">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="costfactor_discount[]" id="costfactor_discount_<?php echo $j; ?>" class="form-control text-right" value="0" readonly>
                                                    </td>
                                                    <?php // }  ?>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
<!--                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($get_only_product_c_cost_factor as $single_product) {
                                        $i++;
                                        ?>
                                                                    <tr>
                                                                        <td>
                                        <?php echo $single_product->product_name; ?>
                                                                            <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single_product->product_id; ?>">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="individual_cost_factor[]" id="individual_cost_factor_<?php echo $i; ?>" class="form-control text-right" onkeyup="cost_factor_calculation(this.value, '<?php echo $i; ?>')" value="<?php echo $single_product->individual_cost_factor; ?>">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="costfactor_discount[]" id="costfactor_discount_<?php echo $i; ?>" class="form-control text-right" value="<?php echo $single_product->costfactor_discount; ?>" readonly>
                                                                        </td>
                                                                    </tr>
                                    <?php } ?>
                                    </tbody>-->
                                    <tfoot>
                                        <tr>
                                            <th colspan="3" class="text-right">
                                                <input type="submit" class="btn btn-success btn-sm" value="Save Change">
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </form>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</main>
<script type="text/javascript">
    function cost_factor_calculation(item, sl) {
        var cost_factor = $("#individual_cost_factor_" + sl).val();
        var discount = 100 - cost_factor;
        $("#costfactor_discount_" + sl).val(discount);
//    alert(discount);
    }
</script>