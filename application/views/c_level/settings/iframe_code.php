<!-- /*div.before_div_iframe
                            {
                            width:100%;
                            height:500px;
                            overflow-y:scroll;
                            -webkit-resize:vertical; 
                            -moz-resize:vertical;
                            resize:vertical;
                            }*/
                            <br>-->
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Web Iframe</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Iframe</li>
                        </ol>
                    </nav>
                </div>
                <!--<a href="<?php echo base_url(); ?>d-customer-form" class="btn btn-success w-md m-b-5"  style="margin-bottom: 10px;" target="_new">View D-Level Form</a>-->
                <!--<a href="<?php echo base_url(); ?>d-customer-iframe-form" class="btn btn-success w-md m-b-5"  style="margin-bottom: 10px;" target="_new">View IFrame</a>-->
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <h4>CSS</h4>
                        <textarea class="form-control" name="" rows="5" placeholder="" readonly> 
                            iframe#iframe_div
                            {
                            border:none;
                            width:100%;  
                            height:1200px; 
                            margin-left:0px;
                            margin-top:0px; 
                            overflow:hidden;
                            -webkit-resize:none;
                            -moz-resize:none;
                            resize:none;
                            }
                        </textarea>
                    </div>
                    <div class="card-body">
                        <h4>HTML</h4>
                        <form action="<?php echo base_url(); ?>iframe-code-save" method="post">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <?php
                                    // echo @$check_iframe_code[0]->iframe_code;  
//                                    echo $this->session->userdata('user_id');
//                                    echo $this->session->userdata('user_type');
                                    ?>

                                    <textarea class="form-control" name="iframe_code" rows="5" placeholder="" readonly> 
                                <div class="before_div_iframe">
                                    <iframe id="iframe_div" src="<?php echo base_url() . "d-customer-iframe-form-custom/" . $this->session->userdata('user_id') . "/" . $this->session->userdata('user_type'); ?>" scrolling="no"  frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen> </iframe>
                                  </div>
                                    </textarea>
                                </div>
                            </div>
                            <!--                            <div class="form-group text-right">
                                                            <button type="reset" class="btn btn-danger w-md m-b-5">Cancel</button>
                                                            <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
                                                        </div>-->
                        </form>
                    </div>                    
                </div>

            </div>
        </div>
    </div>
</main>
