
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Appointment Form</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Form</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Appointment</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-8 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointmentform" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Calendar</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="calendar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?php echo base_url(); ?>appointment-setup" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="customer_id" class="col-form-label">Client Info</label>
                                    <select class="form-control select2-single" name="customer_id" data-placeholder='-- select one --' required>
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_customer as $customer) {
                                            echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="productcolor" class="col-form-label">Responsible Staff</label>
                                    <select class="form-control select2-single" name="c_level_staff_id" data-placeholder='-- select one --' required>
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_users as $user) {
                                            echo "<option value='$user->id'>$user->first_name $user->last_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="setdata" class="col-form-label">Set Date</label>
                                    <input class="form-control datepicker" placeholder="Date" name="appointment_date" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="setime" class="col-form-label">Set Time</label>
                                    <input type="text" class="form-control" data-header-left="true" id="datepicker-left-header" name="appointment_time" required>
                                </div>
                                <div class="form-group col-md-12 text-left">
                                    <label for="remarks" class="col-form-label bfh-timepicker">Remarks</label>
                                    <input class="form-control" name="remarks" id="remarks" placeholder="Remarks">
                                </div>

                                <div class="form-group col-md-12 mb-0">
                                    <button type="submit" class="btn btn-primary mb-0 float-right">Send</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <button class="top-right" data-toggle="modal" data-target="#appointmentform"><i class="simple-icon-size-fullscreen"></i></button>
                        <div class="form-group">
                            <div class="date-inline"></div>
                        </div>
                    </div>
                </div>
                <?php
                foreach ($get_appointment_info as $singleDate) {
//                    echo $dateArray = date("d", strtotime($singleDate->appointment_date));
                }
                ?>

            </div>
        </div>

    </div>
</main>
<script type="text/javascript">
    $(document).ready(function () {
        //        ======== its timer =========== format: 'LT'
        $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
        $('#datepicker-left-header').datetimepicker({
            'format': "HH:mm", // HH:mm:ss
        });

//        ================ its for appointment date selected =============
        $('.date-inline').each(function(){
//            console.log($(this).datepicker());
//            var totalDay = $(".date-inline .day").length;
//            alert(totalDay);
        });
//        var totalDay = $(".datepicker-inline .day").length;
//        alert(totalDay);
//        $(".table-condensed tbody .day").each(function () {
//            console.log("Hi");
//        });
//$(".date-inline").datepicker().on("show",function(date){
//    var date = new Date(document.querySelector(".datepicker-days .datepicker-switch").textContent);
//    var startofmonth = new Date(date.getFullYear(), date.getMonth(),1);
//    var endofmonth = new Date(date.getFullYear(), date.getMonth()+1, 0);
//    
//    startofmonth.setDate(startofmonth.getDate()-$(".datepicker .old.day").length);
//    endofmonth.setDate(endofmonth.getDate()+$(".datepicker .new.day").length);
//    alert(totalDay);
//});
    });
//    $(document).load(function (){
//        var totalDay = $(".datepicker-inline .day").length;
//        alert(totalDay);
//    });
</script>