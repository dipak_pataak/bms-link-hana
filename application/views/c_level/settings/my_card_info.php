<!--<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/b_level/card/card_style.css">-->
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>My card info</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">My card info</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width collapse"  id="collapseExample">
            <div class="col-xl-12 mb-4">

                <div class="card mb-4">
                    <div class="card-body">

                        <form action="<?php echo base_url(); ?>c_level/setting_controller/card_info_update/"  method="post" enctype="multipart/form-data">


                            <div class="form-group row">
                                <label for="card_number" class="col-sm-3 col-form-label text-right">Card number <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="card_number" onkeyup="hypen_generate(this.value)" onkeypress="hypen_generate(this.value)" class="form-control card_number" type="text"  placeholder="Card number" maxlength="19" required>
                                    <!--<input type="text" id="hdn_card">-->
                                    <p>Please enter the valid card number</p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="expiry_month" class="col-sm-3 col-form-label text-right">Expiry month <span class="text-danger"> *</span></label>

                                <div class="col-sm-9">
                                    <select name="expiry_month" class="form-control select2-single" data-placeholder="-- select month --" required>
                                        <?php
                                        $i = 1;
                                        for ($i = 1; $i <= 12; $i++) {
                                            if ($i < 10) {
                                                $i = '0' . $i;
                                            }

                                            echo '<option value="' . $i . '">' . $i . '</option>';
                                        }
                                        ?> 
                                    </select>
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="expiry_year" class="col-sm-3 col-form-label text-right">Expiry year </label>
                                <div class="col-sm-9">
                                    <select name="expiry_year" class="form-control select2" data-placeholder="-- select Year --" required>
                                        <?php
                                        $y10 = date('Y') + 10;

                                        for ($i = date('Y'); $i <= $y10; $i++) {
                                            echo '<option value="' . substr($i, 2) . '">' . $i . '</option>';
                                        }
                                        ?> 
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="card_holder" class="col-sm-3 col-form-label text-right">Card holder   <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="card_holder" class="form-control" type="card_holder" placeholder="Card holder"  value="<?= @$info->card_holder ?>" required>
                                    <p>Card holder name</p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="is_active" class="col-sm-3 col-form-label text-right">Ative status  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <select name="is_active" class="form-control select2-single" data-placeholder="-- select is_active --" required>
                                        <option value="1">Active</option>
                                        <option value="0">In active</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-sm btn-success w-md m-b-5">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>


                <div class="card mb-4">

                    <div class="card-body" id="results_menu">

                        <button class="btn btn-primary btn-sm default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Add new card
                        </button>


                        <table class="table table-bordered table-hover">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Card Number</th>
                                    <th>Expiry month</th>
                                    <th>Expiry year</th>
                                    <th>Card Holder</th>
                                    <th>Active Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (!empty($info)) {
                                    $sl = 1;
                                    foreach ($info as $key => $value) {

                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->card_number; ?></td>
                                            <td><?php echo $value->expiry_month; ?></td>
                                            <td><?php echo $value->expiry_year; ?></td>
                                            <td><?php echo $value->card_holder ?></td>

                                            <td class="text-center">
                                                <?php
                                                $status = $value->is_active;
                                                if ($status == 1) {
                                                    ?>
                                                    Active
                                                    <?php
                                                }
                                                if ($status == 0) {
                                                    ?>
                                                    In active
                                                <?php } ?>
                                            </td>

                                            <td>
                                                <a href="javascript:void(0)" onclick="editData('<?php echo $value->id; ?>')" 
                                                   data-toggle="modal" data-backdrop="static"
                                                   data-target="#exampleModalRight"
                                                   title="" class="btn btn-info btn-xs simple-icon-note"></a>
                                                <a href="<?php echo base_url(); ?>c_level/setting_controller/card_delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs simple-icon-trash"></a>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if (empty($info)) { ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-danger  text-center" colspan="7"> Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table> 
                    </div>
                </div>
            </div>




        </div>

    </div>
</main>




<script type="text/javascript">

    function editData(id) {

        $("#myModal").modal("exampleModalRight");

        var formData = {'id': id};
        var submit_url = "<?php echo base_url(); ?>" + "c_level/setting_controller/get_card_info/" + id;

        $.ajax({
            url: submit_url,
            type: 'post',
            data: formData,
            success: function (data) {


                var obj = jQuery.parseJSON(data);


                $("#card_number").val(obj.card_number);
                $("#expiry_month").val(obj.expiry_month);
                $("#expiry_year").val(obj.expiry_year);
                $("#card_holder").val(obj.card_holder);
                $("#is_active").val(obj.is_active);
                $("#id").val(obj.id);

                $("#myModal").modal("exampleModalRight");

            }, error: function () {

            }

        });







    }

</script>



<div class="modal fade modal-right" id="exampleModalRight" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalRight" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Card</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>



            <div class="modal-body">

                <form action="<?php echo base_url(); ?>c_level/setting_controller/card_info_update/"  method="post" enctype="multipart/form-data">


                    <div class="form-group row">
                        <label for="card_number" class="col-sm-4 col-form-label text-right">Card number <span class="text-danger"> *</span></label>
                        <div class="col-sm-8">
                            <input name="card_number" class="form-control card_number" type="text" onkeyup="hypen_generate(this.value)" onkeypress="hypen_generate(this.value)" id="card_number" placeholder="Card number" maxlength="19" required>
                        </div>
                    </div>

                    <input type="hidden" name="id" id="id">

                    <div class="form-group row">
                        <label for="expiry_month" class="col-sm-4 col-form-label text-right">Expiry month <span class="text-danger"> *</span></label>

                        <div class="col-sm-8">
                            <select name="expiry_month" class="form-control select2-single" id="expiry_month" data-placeholder="-- select month --" required>
                                <?php
                                $i = 1;
                                for ($i = 1; $i <= 12; $i++) {
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }

                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                                ?> 
                            </select>
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="expiry_year" class="col-sm-4 col-form-label text-right">Expiry year </label>
                        <div class="col-sm-8">
                            <select name="expiry_year" class="form-control select2" id="expiry_year" data-placeholder="-- select Year --" required>
                                <?php
                                $y10 = date('Y') + 10;

                                for ($i = date('Y'); $i <= $y10; $i++) {
                                    echo '<option value="' . substr($i, 2) . '">' . $i . '</option>';
                                }
                                ?> 
                            </select>
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="card_holder" class="col-sm-4 col-form-label text-right">Card holder   <span class="text-danger"> *</span></label>
                        <div class="col-sm-8">
                            <input name="card_holder" class="form-control" type="card_holder" placeholder="Card holder" id="card_holder"  required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="is_active" class="col-sm-4 col-form-label text-right">Ative status  <span class="text-danger"> *</span></label>
                        <div class="col-sm-8">
                            <select name="is_active" class="form-control select2-single" id="is_active" data-placeholder="-- select is_active --" required>
                                <option value=""></option>
                                <option  value="1"> Active </option>
                                <option value="0"> In active </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-success w-md m-b-5">Update</button>
                    </div>

                </form>

            </div>


        </div>
    </div>

</div>

<script type="text/javascript">
    function hypen_generate(id) {
        var total_number = id.length;
        if (total_number == 4) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
        if (total_number == 9) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
        if (total_number == 14) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
    }
</script>