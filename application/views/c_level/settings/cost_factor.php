<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
<style>
    #result_search_filter {
        display:none;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Cost Factor</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cost Factor</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="p-1">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <div class="table-responsive">
                            <form id="frm_cost_factor" action="<?php echo base_url(); ?>c-cost-factor-save" method="post">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="txt_individual_cost_factor" class="mb-2">Cost Factor <span class="text-danger"> * </span></label>
                                        <input type="hidden" name="submit_type" id="submit_type" value="">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" name="txt_individual_cost_factor" class="form-control">
                                            </div>
                                            <div class="col-md-2">
                                                <input type="button" onClick="submitForm();" class="button btn btn-success" value="Save">
                                            </div>
                                        </div>
                                    </div>
                                    <label for="keyword" class="col-sm-2 col-form-label offset-2 text-right"></label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control pull-right" name="keyword" id="keyword" placeholder="Search..." tabindex="">
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover" id="result_search">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="SellectAll"/></th>
                                            <th>Product List</th>
                                            <th class="text-right">Cost Factor %</th>
                                            <th class="text-right">Discount %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($get_product as $single_product) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $single_product->product_id; ?>" class="checkbox_list">  
                                                </td>
                                                <td>
                                                    <?php echo $single_product->product_name; ?>
                                                    <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single_product->product_id; ?>">
                                                </td>
                                                <?php
                                                $cost_factor = $discount = 0;
                                                if (!empty($get_only_product_c_cost_factor[$i-1]->individual_cost_factor)) {
                                                    $cost_factor = $get_only_product_c_cost_factor[$i-1]->individual_cost_factor;
                                                    $discount = $get_only_product_c_cost_factor[$i-1]->costfactor_discount;
                                                }
                                                ?>
                                                <td>
                                                    <input type="text" name="individual_cost_factor[]" id="individual_cost_factor_<?php echo $i; ?>" class="form-control text-right" onkeyup="cost_factor_calculation(this.value, '<?php echo $i; ?>')" value="<?php echo $cost_factor; ?>">
                                                </td>
                                                <td>
                                                    <input type="text" name="costfactor_discount[]" id="costfactor_discount_<?php echo $i; ?>" class="form-control text-right" value="<?php echo $discount; ?>" readonly>
                                                </td>
                                            </tr>
                                        <?php $cost_factor = $discount = 0;
                                        
                                                } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3" class="text-right">
                                                <input type="submit" class="btn btn-success btn-sm" value="Save Change">
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </form>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</main>
<script type="text/javascript">
    function cost_factor_calculation(item, sl) {
        var cost_factor = $("#individual_cost_factor_" + sl).val();
        var discount = 100 - cost_factor;
        $("#costfactor_discount_" + sl).val(discount);
//    alert(discount);
    }
    function submitForm() {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        } else {
            $('#submit_type').val('bulk');
            $('#frm_cost_factor').submit();
        }
    }
</script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        oTable = $('#result_search').DataTable( {
            "paging": false,
            "bInfo": false,
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#keyword').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    } );
</script>