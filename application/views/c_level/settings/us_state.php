
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>US State</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">US State</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
                <button type="button" class="offset-2 btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importColor">Import US State</button>
                <br>

                <!-- Modal -->
                <div class="modal fade" id="importColor" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <!--<h4 class="modal-title">Modal Header</h4>-->
                            </div>
                            <div class="modal-body">
                                <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span>
                                <a href="<?php echo base_url('assets/c_level/csv/us_state_csv_sample.csv') ?>" class="btn btn-sm btn-primary" style="margin-bottom: 10px;"><i class="fa fa-download"></i> Download Sample File</a>
                                <?php echo form_open_multipart('import-c-us-state-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                                <div class="form-group row">
                                    <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                                    <div class="col-xs-6">
                                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group  text-right">
                                    <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Import</button>
                                </div>

                                </form>
                            </div>
                            <div class="modal-footer">
<!--                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <form action="<?php echo base_url('c-us-state-save'); ?>" method="post">
                            <div class="form-group row">
                                <label for="state_name" class="col-sm-3 col-form-label">State Name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="state_name" class="form-control select2-single" id="state_name" onchange="city_statename_wise_stateid(this.value)" data-placeholder="-- select one --" required>
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_city_state as $city_state) {
                                            echo "<option value='$city_state->state_name'>$city_state->state_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!--                            <div class="form-group row">
                                                            <label for="short_code" class="col-sm-3 col-form-label">Short Code <i class="text-danger">*</i></label>
                                                            <div class="col-sm-6">
                                                                <select name="short_code" class="form-control short_code select2-single" id="short_code" data-placeholder="-- select one --" required>
                                                                </select>
                                                            </div>
                                                        </div>-->
                            <input type="hidden" name="short_code" class="form-control short_code " id="short_code">
                            <div class="form-group row">
                                <label for="tax_rate" class="col-sm-3 col-form-label">Tax Rate <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="tax_rate" id="tax_rate" type="text" placeholder="Enter Tax Rate!" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9 text-right">
                                    <input type="submit" id="add-shipping_method btn-sm" class="btn btn-success btn-sm" name="" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <table class="table table-bordered mb-3">
                            <thead>
                                <tr>
                                    <th width="10%">Sl No.</th>
                                    <th width="35%">State Name</th>
                                    <th width="35%">Short Code</th>
                                    <th width="10%">Tax Rate</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $sl = 0 + $pagenum;
                                if (!empty($us_state_list)) {
                                    foreach ($us_state_list as $key => $val) {

                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?= $sl; ?></td>
                                            <td><?= $val->state_name; ?></td>
                                            <td><?= $val->shortcode; ?></td>
                                            <td><?= $val->tax_rate; ?></td>

                                            <td width="100">
                                                <a href="<?php echo base_url(); ?>c-us-state-edit/<?php echo $val->state_id; ?>" class="btn btn-success  btn-sm btn-xs simple-icon-note" data-toggle="tooltip" data-placement="top" data-original-title=""><i class="fa fa-edit"></i></a>
                                                <a href="<?php echo base_url(''); ?>c-us-state-delete/<?= $val->state_id ?>" onclick="return confirm('Are you sure want to delete it?')" class="btn btn-danger btn-xs simple-icon-trash" data-toggle="tooltip" data-placement="top" data-original-title=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if (empty($us_state_list)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="9" class="text-center text-danger">No record found!</th>
                                    </tr> 
                                </tfoot>
                            <?php } ?>
                        </table>
                        <?php echo $links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    function city_statename_wise_stateid(t) {
        $.ajax({
            url: "<?php echo base_url('c_level/Setting_controller/city_statename_wise_stateid'); ?>",
            type: 'post',
            data: {state_name: t},
            success: function (r) {
                r = JSON.parse(r);
//                    alert(r);
                $("#short_code").empty();
//                $("#short_code").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $("#short_code").val(typeval.state_id);
//                    $('#short_code').append($('<option>').text(typeval.state_id).attr('value', typeval.state_id));
                });
            }
        });
    }
</script>