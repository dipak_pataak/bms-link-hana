
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>US State Edit</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">US State</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <form action="<?php echo base_url('c-us-state-update/' . $us_state_edit[0]['state_id']); ?>" method="post">
                            <div class="form-group row">
                                <label for="state_name" class="col-sm-3 col-form-label">State Name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="state_name" class="form-control select2-single" id="state_name" onchange="city_statename_wise_stateid(this.value)" data-placeholder="-- select one --" required>
                                        <option value=""></option>
                                        <?php foreach ($get_city_state as $city_state) { ?>
                                            <option value='<?php echo $city_state->state_name; ?>' <?php
                                            if ($us_state_edit[0]['state_name'] == $city_state->state_name) {
                                                echo 'selected';
                                            }
                                            ?>>
                                                        <?php echo $city_state->state_name; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <!--<input class="form-control" name="state_name" id="state_name" type="text" value="<?php echo $us_state_edit[0]['state_name']; ?>" placeholder="Enter State Name!" required="">-->
                                </div>
                            </div>
<!--                            <div class="form-group row">
                                <label for="short_code" class="col-sm-3 col-form-label">Short Code <i class="text-danger">*</i></label>-->
                                <div class="col-sm-6">
                                    <input type="hidden" name="short_code" class="form-control short_code " id="short_code" value="<?php echo $us_state_edit[0]['shortcode']; ?>">
                                    <!--<input type="text" name="short_code" class="form-control short_code" id="short_code" value="<?php echo $us_state_edit[0]['shortcode']; ?>" placeholder="Enter Short Code">-->
                                </div>
                            <!--</div>-->

                            <div class="form-group row">
                                <label for="tax_rate" class="col-sm-3 col-form-label">Tax Rate <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="tax_rate" id="tax_rate" type="text" value="<?php echo $us_state_edit[0]['tax_rate']; ?>" placeholder="Enter Tax Rate!">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9 text-right">
                                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-sm text-white">Back</a>
                                    <input type="submit" id="add-shipping_method" class="btn btn-success btn-sm" name="" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
<script type="text/javascript">
    function city_statename_wise_stateid(t) {
        $.ajax({
            url: "<?php echo base_url('c_level/Setting_controller/city_statename_wise_stateid'); ?>",
            type: 'post',
            data: {state_name: t},
            success: function (r) {
                r = JSON.parse(r);
//                    alert(r);
                $("#short_code").empty();
//                $("#short_code").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $("#short_code").val(typeval.state_id);
//                    $('#short_code').append($('<option>').text(typeval.state_id).attr('value', typeval.state_id));
                });
            }
        });
    }
</script>