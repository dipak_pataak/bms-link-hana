<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>My Accounts</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">My Profile</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>my-account-update"  method="post" enctype="multipart/form-data">
                            <input type="hidden" name="user_id" value="<?= @$my_account[0]->id ?>" />
                            <input type="hidden" name="redirect_url" value="profile-setting" />

                            <div class="form-group row">
                                <label for="first_name" class="col-sm-3 col-form-label text-right">First Name <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="first_name" class="form-control" type="text" placeholder="First Name" id="company_name" value="<?= @$my_account[0]->first_name ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="last_name" class="col-sm-3 col-form-label text-right">Last Name  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="last_name" class="form-control" type="text" placeholder="Last Name" id="company_name" value="<?= @$my_account[0]->last_name ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-sm-3 col-form-label text-right">Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="image" id="image" aria-describedby="fileHelp">
                                    <input type="hidden" name="image_hdn" value="<?php echo $my_account[0]->user_image; ?>">
                                    <small id="fileHelp" class="text-muted"></small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="preview" class="col-sm-3 col-form-label text-right">Preview</label>
                                <div class="col-sm-9">
                                    <?php if ($my_account[0]->user_image) { ?>
                                        <img src="<?php echo base_url(); ?>assets/c_level/uploads/appsettings/<?php echo @$my_account[0]->user_image; ?>" class="img-thumbnail" width="125" height="100" id="prevImg">
                                    <?php } else { ?>
                                        <img src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l.jpg" class="img-thumbnail" width="125" height="100" id="prevImg">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-3 col-form-label text-right">Phone </label>
                                <div class="col-sm-9">
                                    <input name="phone" class="form-control phone" type="text" id="phone" value="<?= @$my_account[0]->phone ?>" placeholder="+1 (XXX)-XXX-XXXX">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label text-right">Email Address  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="email" class="form-control" type="email" placeholder="Email Address" id="email" value="<?= @$my_account[0]->email ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="language" class="col-sm-3 col-form-label text-right">Language  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <select name="language" class="form-control select2-single" id="language" data-placeholder="-- select language --" required>
                                        <option value=""></option>
                                        <option value="English" <?php if ($my_account[0]->language == 'English') {
                                        echo 'selected';
                                    } ?>>English</option>
                                        <option value="Korean" <?php if ($my_account[0]->language == 'Korean') {
                                        echo 'selected';
                                    } ?>>My Language</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-sm btn-success w-md m-b-5">Update</button>
                <a href="<?php echo base_url('c-my-account'); ?>" class="btn btn-danger btn-sm">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
// -------- Show Image Preview once File selected ----
    $("#image").change(function (e) {

        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

            var file = e.originalEvent.srcElement.files[i];

            var img = document.getElementById('prevImg');
            var reader = new FileReader();
            reader.onloadend = function () {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("image").after(img);
        }
    });
// -------- Image Preview Ends --------------
</script>