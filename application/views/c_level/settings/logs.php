
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Logs</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Logs</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>SL no.</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>IP Address</th>
                                        <th>Done</th>
                                        <th>Remarks</th>
                                        <th>Users</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sl = 0+$pagenum;
                                    foreach ($access_logs as $logs) {
                                        $sl++;
                                        $dateTimes = explode(" ", $logs->entry_date);
                                        $date = $dateTimes[0];
                                        $time = $dateTimes[1];
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo date('M-d-Y', strtotime($date)); ?></td>
                                            <td><?php echo date('H:i:s', strtotime($time)); ?></td>
                                            <td><?php echo $logs->ip_address; ?></td>
                                            <td><?php echo $logs->action_done; ?></td>
                                            <td><?php echo $logs->remarks; ?></td>
                                            <td><?php echo $logs->name; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <?php if(empty($access_logs)){ ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-center text-danger" colspan="7">Record not found!</th>
                                    </tr>
                                </tfoot>
                                <?php } ?>
                            </table>
                            <?php echo $links; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
