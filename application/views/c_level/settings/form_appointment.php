<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        /*top: 0px !important;*/
        top: 290px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Appointment Form</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Form</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Appointment</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-8 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div id="appointmentform" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Calendar</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="calendar_app"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div style="margin-right: 35px;" id="appointmentListBydate" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" style=" -webkit-box-shadow: 3px 3px 5px 6px #ccc;  /* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
                                     -moz-box-shadow:    3px 3px 5px 6px #ccc;  /* Firefox 3.5 - 3.6 */
                                     box-shadow:         3px 3px 5px 6px #ccc;  /* Opera 10.5, IE 9, Firefox 4+, Chrome 6+, iOS 5 */">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Calendar</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" >
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Customer Name</th>
                                                    <th>Staff Name</th>
                                                    <th>Remarks</th>
                                                    <th>Appointment Date</th>
                                                    <th>Appointment Time</th>
                                                </tr>
                                            </thead>
                                            <tbody id="showTr">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <form action="<?php echo base_url(); ?>appointment-setup" method="post">
                            <div class="form-row">
                                
                                <div class="form-group col-md-6">
                                    <label for="customer_id" class="col-form-label">Client Info</label>
                                    <select class="form-control select2-single" name="customer_id" data-placeholder='-- select one --' required>
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_customer as $customer) {
                                            echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="productcolor" class="col-form-label">Responsible Staff</label>
                                    <select class="form-control select2-single" name="c_level_staff_id" data-placeholder='-- select one --' required>
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_users as $user) {
                                            echo "<option value='$user->id'>$user->first_name $user->last_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="setdata" class="col-form-label">Set Date</label>
                                    <input class="form-control datepicker" placeholder="Date" name="appointment_date" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="setime" class="col-form-label">Set Time</label>
                                    <input type="text" class="form-control" data-header-left="true" id="datepicker-left-header" name="appointment_time" required>
                                </div>

                                <div class="form-group col-md-12 text-left">
                                    <label for="remarks" class="col-form-label bfh-timepicker">Remarks</label>
                                    <input class="form-control" name="remarks" id="remarks" placeholder="Remarks">
                                </div>

                                <div class="form-group col-md-12 mb-0">
                                    <button type="submit" class="btn btn-success btn-sm mb-0 float-right">Send</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <div class="col-lg-4">

                <div class="card">
                    <div class="card-body">
                        <button class="top-right" data-toggle="modal" data-target="#appointmentform"><i class="simple-icon-size-fullscreen"></i></button>
                        <div class="form-group">
                            <div class="date-inline" data-date-format="yyyy-mm-dd"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</main>





<style type="text/css">
    .datepicker table tr td.active.active, .datepicker table tr td.active.highlighted.active, .datepicker table tr td.active.highlighted:active, .datepicker table tr td.active:active {
        color: white;
        background-color: none;
        border-color: #145388;
        text-decoration: underline;
    }
</style>


<script type="text/javascript">
    $(document).ready(function () {

        $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
        $('#datepicker-left-header').datetimepicker({
            //'format': "HH:mm", // HH:mm:ss its for 24 hours format
            format: 'LT', /// its for 12 hours format
        });


    });
</script>