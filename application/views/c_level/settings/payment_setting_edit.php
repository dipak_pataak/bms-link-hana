
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Payment Settings</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Payment</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <form action="<?php echo base_url(); ?>c-update-payment-gateway/<?php echo $gateway_edit[0]['id']; ?>" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div class="form-group row">
                                <label for="payment_gateway" class="col-sm-3 col-form-label">Payment Gateway <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2-single" name="payment_gateway" id="payment_gateway" data-placeholder='-- select one --'>
                                        <option value="">-- select one --</option>
                                        <option value="Paypal" <?php
                                        if ($gateway_edit[0]['payment_gateway'] == 'Paypal') {
                                            echo 'selected';
                                        }
                                        ?>>Paypal</option>
                                        <!--                                        <option value="Sandbox" <?php
                                        if ($gateway_edit[0]['payment_gateway'] == 'Sandbox') {
                                            echo 'selected';
                                        }
                                        ?>>Sandbox</option>-->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="payment_mail" class="col-sm-3 col-form-label">Paypal Mail<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" name="payment_mail" id="payment_mail" class="form-control" value="<?php echo $gateway_edit[0]['payment_mail']; ?>" placeholder="Payment Mail">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="currency" class="col-sm-3 col-form-label">Currency <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2-single" name="currency" id="currency" data-placeholder="-- select one --">
                                        <option value=""></option>
                                        <option value="USD" <?php
                                        if ($gateway_edit[0]['currency'] == 'USD') {
                                            echo 'selected';
                                        }
                                        ?>>(USD) U.S. Dollar</option>
                                        <option value="EUR" <?php
                                        if ($gateway_edit[0]['currency'] == 'EUR') {
                                            echo 'selected';
                                        }
                                        ?>>(EUR) Euro</option>
                                        <option value="AUD" <?php
                                        if ($gateway_edit[0]['currency'] == 'AUD') {
                                            echo 'selected';
                                        }
                                        ?>>(AUD) Australian Dollar</option>
                                        <option value="CAD" <?php
                                        if ($gateway_edit[0]['currency'] == 'CAD') {
                                            echo 'selected';
                                        }
                                        ?>>(CAD) Canadian Dollar</option>
                                        <option value="CZK" <?php
                                        if ($gateway_edit[0]['currency'] == 'CZK') {
                                            echo 'selected';
                                        }
                                        ?>>(CZK) Czech Koruna</option>
                                        <option value="DKK" <?php
                                        if ($gateway_edit[0]['currency'] == 'DKK') {
                                            echo 'selected';
                                        }
                                        ?>>(DKK) Danish Krone</option>
                                        <option value="HKD" <?php
                                        if ($gateway_edit[0]['currency'] == 'HKD') {
                                            echo 'selected';
                                        }
                                        ?>>(HKD) Hong Kong Dollar</option>
                                        <option value="Yen" <?php
                                        if ($gateway_edit[0]['currency'] == 'Yen') {
                                            echo 'selected';
                                        }
                                        ?>>(YEN) Japanese</option>
                                        <option value="MXN" <?php
                                        if ($gateway_edit[0]['currency'] == 'MXN') {
                                            echo 'selected';
                                        }
                                        ?>>(MXN) Mexican Peso</option>
                                        <option value="NOK" <?php
                                        if ($gateway_edit[0]['currency'] == 'NOK') {
                                            echo 'selected';
                                        }
                                        ?>>(NOK) Norwegian Krone</option>
                                        <option value="NZD" <?php
                                        if ($gateway_edit[0]['currency'] == 'NZD') {
                                            echo 'selected';
                                        }
                                        ?>>(NZD) New Zealand Dollar</option>
                                        <option value="PHP" <?php
                                        if ($gateway_edit[0]['currency'] == 'PHP') {
                                            echo 'selected';
                                        }
                                        ?>>(PHP) Philippine Peso</option>
                                        <option value="PLN" <?php
                                        if ($gateway_edit[0]['currency'] == 'PLN') {
                                            echo 'selected';
                                        }
                                        ?>>(PLN) Polish Zloty</option>
                                        <option value="SGD" <?php
                                        if ($gateway_edit[0]['currency'] == 'SGD') {
                                            echo 'selected';
                                        }
                                        ?>>(SGD) Singapore Dollar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="is_active" class="col-sm-3 col-form-label">Is Active<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="is_active" id="is_active" class="form-control select2-single" data-placeholder="-- select one --">
                                        <option value="">-- select one --</option>
                                        <option value="1" <?php
                                        if ($gateway_edit[0]['default_status'] == 1) {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if ($gateway_edit[0]['default_status'] == 0) {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Mode <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                                        <option value="0" <?php
                                        if ($gateway_edit[0]['status'] == 0) {
                                            echo 'selected';
                                        }
                                        ?>>Development</option>
                                        <option value="1"<?php
                                        if ($gateway_edit[0]['status'] == 1) {
                                            echo 'selected';
                                        }
                                        ?>>Production</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6 text-right">
                                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white btn-sm">Back</a>
                                    <input type="submit" class="btn btn-success btn-sm" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
