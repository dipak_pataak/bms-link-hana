
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Payment Gateway</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Payment</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <form action="<?php echo base_url('payment-gateway-update'); ?>" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div class="form-group row">
                                <label for="url" class="col-sm-3 col-form-label text-right">URL<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" name="url" id="url" class="form-control" value="<?= @$gateway_edit->url; ?>" placeholder="URL" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_name" class="col-sm-3 col-form-label text-right">User Name<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" name="user_name" id="user_name" class="form-control" value="<?= @$gateway_edit->user_name ?>" placeholder="Username">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label text-right">Password<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="password" name="password" id="password" class="form-control" value="<?= @$gateway_edit->password ?>" placeholder="Payment Password">
                                </div>
                            </div>



                            <!-- <div class="form-group row">
                                <label for="mode" class="col-sm-3 col-form-label">Mode<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                                        <option value="1" <?php
                            if ($gateway_edit->mode == 1) {
                                echo 'selected';
                            }
                            ?>>Production</option>
                                        <option value="0" <?php
                            if ($gateway_edit->mode == 0) {
                                echo 'selected';
                            }
                            ?>>Development</option>
                                    </select>
                                </div>
                            </div> -->


                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6 text-right">
                                    <input type="submit" class="btn btn-success btn-sm" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>