<style type="text/css">
    .pac-container.pac-logo{
        top: 228px !important;
    }
</style>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Company Profile Settings</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Profile Settings</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>company-profile-update"  method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="company_name" class="col-sm-3 col-form-label">Company Name  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="company_name" class="form-control" type="text" placeholder="Company Name" id="company_name" value="<?php echo @$company_profile[0]->company_name; ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-sm-3 col-form-label">Address  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="address" class="form-control" type="text" placeholder="Address" id="address" value="<?php echo @$company_profile[0]->address; ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-sm-3 col-form-label">City</label>
                                <div class="col-sm-9">
                                    <input name="city" class="form-control" type="text" placeholder="city" id="city" value="<?php echo @$company_profile[0]->city; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="state" class="col-sm-3 col-form-label">State</label>
                                <div class="col-sm-9">
                                    <input name="state" class="form-control" type="text" placeholder="State" id="state" value="<?php echo @$company_profile[0]->state; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="zip" class="col-sm-3 col-form-label">Zip Code</label>
                                <div class="col-sm-9">
                                    <input name="zip" class="form-control" type="text" placeholder="Zip Code" id="zip" value="<?php echo @$company_profile[0]->zip_code; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="country_code" class="col-sm-3 col-form-label">Country Code</label>
                                <div class="col-sm-9">
                                    <input name="country_code" class="form-control" type="text" placeholder="Country Code" id="country_code" value="<?php echo @$company_profile[0]->country_code; ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email Address  <span class="text-danger"> *</span></label>
                                <div class="col-sm-9">
                                    <input name="email" class="form-control" type="text" placeholder="Email Address" id="email" value="<?php echo @$company_profile[0]->email; ?>" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                                <div class="col-sm-9">
                                    <input class="form-control phone" type="text" name="phone" value="<?php echo @$company_profile[0]->phone; ?>" placeholder="+1(XXX)-XXX-XXXX">
                                </div>
                            </div>

                            <!--                            <div class="form-group row">
                                                            <label for="password" class="col-sm-3 col-form-label">Change Password *</label>
                                                            <div class="col-sm-9">
                                                                <input name="password" class="form-control" type="password" id="password">
                                                            </div>
                                                        </div>-->
                            <h4 class="mb-3">Notification Preferences</h4>
                            <div class="form-group row">
                                <label for="email_notification" class="col-sm-3 col-form-label">Email Notification</label>
                                <div class="col-sm-5">
                                    <select name="email_notification" id="email_notification" class="form-control">
                                        <option value='2020101' <?php
                                        if (@$company_profile[0]->email_notification == '2020101') {
                                            echo 'selected';
                                        }
                                        ?>>All Updates</option>
                                        <option value="2020102" <?php
                                        if (@$company_profile[0]->email_notification == '2020102') {
                                            echo 'selected';
                                        }
                                        ?>>Incoming</option>
                                        <option value="2020103" <?php
                                        if (@$company_profile[0]->email_notification == '2020103') {
                                            echo 'selected';
                                        }
                                        ?>>Outgoing</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-5">
                                <label for="sms_notification" class="col-sm-3 col-form-label">SMS Notification</label>
                                <div class="col-sm-5">
                                    <select name="sms_notification" id="sms_notification" class="form-control">
                                        <option value='2020101' <?php
                                        if (@$company_profile[0]->sms_notification == '2020101') {
                                            echo 'selected';
                                        }
                                        ?>>All Updates</option>
                                        <option value="2020102" <?php
                                        if (@$company_profile[0]->sms_notification == '2020102') {
                                            echo 'selected';
                                        }
                                        ?>>Incoming</option>
                                        <option value="2020103" <?php
                                        if (@$company_profile[0]->sms_notification == '2020103') {
                                            echo 'selected';
                                        }
                                        ?>>Outgoing</option>
                                    </select>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label for="currency" class="col-sm-3 col-form-label">Choose Currency</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="currency" id="currency" tabindex="5">
                                        <option value="">-- select one --</option>
                                        <option value="$" <?php
                                        if (@$company_profile[0]->currency == '$') {
                                            echo "selected";
                                        }
                                        ?>>$ USD</option>
                                        <option value="AU$" <?php
                                        if (@$company_profile[0]->currency == 'AU$') {
                                            echo "selected";
                                        }
                                        ?>>$ AUD</option>
                                        <option value="ƒ" <?php
                                        if (@$company_profile[0]->currency == 'ƒ') {
                                            echo "selected";
                                        }
                                        ?>>ƒ AWD</option>
                                        <option value="R$" <?php
                                        if (@$company_profile[0]->currency == 'R$') {
                                            echo "selected";
                                        }
                                        ?>>R$ BRL</option>
                                        <option value="¥" <?php
                                        if (@$company_profile[0]->currency == '¥') {
                                            echo "selected";
                                        }
                                        ?>>¥ CNY</option>
                                        <option value="₡" <?php
                                        if (@$company_profile[0]->currency == '₡') {
                                            echo "selected";
                                        }
                                        ?>>₡ CRC</option>
                                        <option value="kn" <?php
                                        if (@$company_profile[0]->currency == 'kn') {
                                            echo "selected";
                                        }
                                        ?>>kn HRK</option>
                                        <option value="£" <?php
                                        if (@$company_profile[0]->currency == '£') {
                                            echo "selected";
                                        }
                                        ?>>£ EGP</option>
                                        <option value="€" <?php
                                        if (@$company_profile[0]->currency == '€') {
                                            echo "selected";
                                        }
                                        ?>>€ EUR</option>
                                        <option value="Rs" <?php
                                        if (@$company_profile[0]->currency == 'Rs') {
                                            echo "selected";
                                        }
                                        ?>>Rs INR</option>
                                        <option value="R" <?php
                                        if (@$company_profile[0]->currency == 'R') {
                                            echo "selected";
                                        }
                                        ?>>R ZAR</option>
                                        <option value="₩" <?php
                                        if (@$company_profile[0]->currency == '₩') {
                                            echo "selected";
                                        }
                                        ?>>₩ KRW</option>
                                        <option value="৳" <?php
                                        if (@$company_profile[0]->currency == '৳') {
                                            echo "selected";
                                        }
                                        ?>>৳ BDT</option>
                                        <option value="₨" <?php
                                        if (@$company_profile[0]->currency == '₨') {
                                            echo "selected";
                                        }
                                        ?>> PKR</option>
                                        <option value="₣" <?php
                                        if (@$company_profile[0]->currency == '₣') {
                                            echo "selected";
                                        }
                                        ?>>Swiss Franc ₣</option>
                                        <option value="ر.س" <?php
                                        if (@$company_profile[0]->currency == 'ر.س') {
                                            echo "selected";
                                        }
                                        ?>>Saudi Riyal ر.س</option>
                                        <option value="₣" <?php
                                        if (@$company_profile[0]->currency == '₣') {
                                            echo "selected";
                                        }
                                        ?>>₣</option>
                                        <option value="د.إ" <?php
                                        if (@$company_profile[0]->currency == 'د.إ') {
                                            echo "selected";
                                        }
                                        ?>>UAE Dirham د.إ</option>د.إ

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label for="logo" class="col-sm-3 col-form-label">Choose Logo</label>
                                <div class="col-sm-5">
                                    <input type="file" name="logo" id="logo">
                                    <input type="hidden" name="logo_hdn" value="<?php echo @$company_profile[0]->logo; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="preview" class="col-sm-3 col-form-label">Preview</label>
                                <div class="col-sm-9">
                                    <?php if ($company_profile[0]->logo) { ?>
                                        <img src="<?php echo base_url(); ?>assets/c_level/uploads/appsettings/<?php echo $company_profile[0]->logo; ?>" class="img-thumbnail" width="125" height="100" id="prevImg">
                                    <?php } else { ?>
                                        <img src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l.jpg" class="img-thumbnail" width="125" height="100" id="prevImg">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group text-right">
                                <input type="hidden" name="user_id" value="<?php echo $company_profile[0]->user_id; ?>">
                                <!--<button type="reset" class="btn btn-danger btn-sm w-md m-b-5">Cancel</button>-->
                                <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Update</button>
                                <a href="<?php echo base_url('company-profile'); ?>" class="btn btn-danger btn-sm">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">

// -------- Show Image Preview once File selected ----
    // $("#logo").change(function (e) {
    $("body").on("change", "#logo", function (e) {

        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

            var file = e.originalEvent.srcElement.files[i];

            var img = document.getElementById('prevImg');
            var reader = new FileReader();
            reader.onloadend = function () {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("logo").after(img);
            $("#prevImg").show();
        }
    });
// -------- Image Preview Ends --------------
//========== its for file reset starts =======
    function reset_html(id) {
        $('#' + id).html($('#' + id).html());
        $("#prevImg").hide();
    }
    var file_input_index = 0;
    $('input[type=file]').each(function () {
        file_input_index++;
        $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
        $(this).after('<input type="button" value="Clear" class="btn-danger" onclick="reset_html(\'file_input_container_' + file_input_index + '\')" style="float:right; margin-top:10px;" />');

    });
//========== its for file reset close=======
//        ----------------- google place api start  -------------
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);

                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });


    });
//        ----------------- google place api close  -------------
</script>