
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>BMS Link</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/iconsmind/style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/simple-line-icons/css/simple-line-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/fullcalendar.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/datatables.responsive.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/select2.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/select2-bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/perfect-scrollbar.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/owl.carousel.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-stars.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/nouislider.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-datepicker3.min.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/toster/toastr.css" media="screen">

        <!-- timepicker -->
        <link href="<?php echo base_url() ?>assets/c_level/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/main.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/custom_style.css" />

        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>

        <script type="text/javascript">
            var mybase_url = '<?php echo base_url(); ?>';
        </script>

        <style type="text/css">
            .pac-container:after{
                content:none !important;
            }

            .skiptranslate{
                display: none;

            }
            body{
                top:0px !important;
            }

        </style>

    </head>



    <body id="app-container" class="menu-default show-spinner">

        <nav class="navbar fixed-top">
            <a class="navbar-logo" href="<?php echo base_url(); ?>c-level-dashboard">
                <img src="<?php echo base_url(); ?>assets/c_level/uploads/appsettings/<?php echo $company_profile->logo; ?>" alt=""  style="width: 80%; margin-top: -15px !important; ">
            </a>
        </nav>


    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body ">
                            <h5 class="mb-4">Payment status</h5>
                            <?php
                                $message = $this->session->flashdata('message');
                                
                                if ($message != '') {
                                    echo $message;
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>






<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/Chart.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/chartjs-plugin-datalabels.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/perfect-scrollbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/progressbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery.barrating.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/select2.full.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/nouislider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/Sortable.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/mousetrap.min.js"></script>
<!--========== its for chart of account tree ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/treeview.js"></script>
<!--=========== its for close ================-->
<!--=========== its for new quotation ================-->
<script src="<?php echo base_url(); ?>assets/c_level/plugins/theia-sticky-sidebar-master/dist/theia-sticky-sidebar.min.js"></script>
<!--=========== its for new quotation close ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/dore.script.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/scripts.js"></script>
<!--=========== its for add customer phone format .js ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/phoneformat.js"></script>
<!--=========== its for add customer phone format .js close ================-->
<!-- date time picker -->
<script src="<?php echo base_url() ?>assets/c_level/js/bootstrap-datetimepicker.js"></script>
 <!--=========== its ckeditor start =============-->
        <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
        <script src="<?php echo base_url(); ?>assets/ckeditor/samples/js/sample.js"></script>
        <!--=========== its ckeditor close=============-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/b_level/toster/toastr.min.js"></script>
