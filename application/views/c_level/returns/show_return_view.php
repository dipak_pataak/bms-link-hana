
<div class="">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Return Qty</th>
                <th>Replace Qty</th>
            </tr>
        </thead>
        <tbody>
            <?php
            //echo '<pre>';                print_r($order_details);
            $i = 0;
            foreach ($order_details as $single) {
                $check_replace_qty = $this->db->select('a.*, sum(a.replace_qty) as replace_quantity')
                                ->from('order_return_details a')
                                ->where('a.return_id', $single->return_id)
                                ->where('a.product_id', $single->product_id)
                                ->where('a.replace_qty!=', 0)
                                ->get()->result();
//                    echo $check_replace_qty[0]->replace_quantity; echo "<br>";
//                    echo '<pre>';                    print_r($check_replace_qty);
                $i++;
                ?>
                <tr>
                    <td>
                        <?php echo $single->product_name; ?>
                    </td>
                    <td>
                        <?php echo $single->return_qty; ?>
                    </td>
                    <td>
                      <?php echo $check_replace_qty[0]->replace_quantity; ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>