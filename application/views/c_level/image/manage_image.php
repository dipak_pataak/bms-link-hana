<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .gallery-image-wrapper .gallery-image-outer img{
        width: 100%;
        height: 145px;
    }
    .gallery-image-wrapper .gallery-image-outer {
        position: relative;
        margin-bottom: 20px;
        overflow: hidden;
    }
   .gallery-image-wrapper .gallery-image-outer:before {
        content: '';
        position: absolute;
        top: -80px;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(0,0,0,0.4);
        opacity: 0;
         -webkit-transition: all 0.4s ease-in-out;
        -moz-transition: all 0.4s ease-in-out;
        -o-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }
    .gallery-image-wrapper .gallery-image-outer:hover:before {
        top: 0;
        opacity: 1;
         -webkit-transition: all 0.4s ease-in-out;
        -moz-transition: all 0.4s ease-in-out;
        -o-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }
    .gallery-image-wrapper .gallery-manage-btnmain {
        text-align: center;
        position: absolute;
        top: auto;
        bottom: -10px;
        margin: 0 auto;
        left: 0;
        right: 0;
        opacity: 0;        
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        bottom: 10px;
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper {
        text-align: center;
    }
    .gallery-image-wrapper a.load-more-btn {
        font-size: 14px;
        color: #ffffff;
        background-color: #336699;
        vertical-align: middle;
        padding: 0 15px;
        height: 40px;
        line-height: 40px;
        border-radius: 3px;
        margin: 30px auto 0 auto;
        text-decoration: none;
        display: none;
    }
    .gallery-image-wrapper i {
        display: inline-block;
        vertical-align: middle;
    }
    /* grid & list */
    .grid-list-wrapper button {
        font-size: 14px;
        background-color: #369;
        color: #ffffff;
        text-shadow: none;
        padding: 0;
        height: 30px;
        width: 30px;
        border: none;
    }
    .gallery-image-wrapper .some-list-load.row {
        margin-left: 0;
        margin-right: 0;
    }
    .gallery-image-wrapper .some-list-load.list {
        display: block;
        text-align: left;
    }
    .gallery-image-wrapper .some-list-load.grid .gallery-manage-btnmain-list {
        display: none;
    }
    .gallery-image-wrapper .product-grid .gallery-manage-btnmain a span {
        display: none;
    }
    .gallery-image-wrapper .product-list img {
        width: 25%;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain {
        right: 15px;
        bottom: auto;
        top: 0;
        left: auto;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a {
        display: block;
        margin: 5px 0;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a i {
        margin-right: 5px;
        display: inline-block;
        vertical-align: middle;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer .gallery-manage-btnmain ,
    .gallery-image-wrapper .product-list .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%); 
        -o-transform: translateY(-50%); 
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        bottom: auto;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer {
        border: 1px solid #eeeeee;
        padding: 15px;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer:hover:before {
        opacity: 0;
    }
    @media(max-width: 1199px){
        .gallery-image-wrapper .gallery-image-outer img {
            min-height: 175px;
        }
    }
</style>
<main>
    <div class="container-fluid gallery-details">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Image List</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Image List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <?php if(count($filter_arr) > 0) { ?>
        <div class="card mb-4">
            <div class="card-body">
                <div>
                    <div class="p-4 border mt-4">
                    <form class="form-horizontal" action="<?php echo base_url('manage_c_gallery_image'); ?>" method="post" id="galleryFilterFrm">
                    <div class="row">
                            <?php  foreach($filter_arr as $filter) { ?>
                            <div class="col-md-2">
                                <select class="form-control filter-select-box" name="tag_<?=$filter['tag_id']?>" id="tag_<?=$filter['tag_id']?>" >
                                    <option value="">--Select <?= $filter['tag_name']; ?>--</option>
                                    <?php foreach($filter['value'] as $value) { ?>
                                        <option value="<?= $value['tag_value']?>"><?= $value['tag_value']?></option>
                                    <?php }?>
                                </select>
                                <script>
                                    $('#tag_'+<?=$filter['tag_id']?>).val('<?= $filter['selected_val']; ?>');
                                </script>
                            </div>
                            <?php } ?>
                            <div class="col-md-2 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="filter_field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                </form>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="card mb-4">
                    <div class="card-body">
                    <div id="results_color">
            <form class="p-3">
                    <div class="gallery-image-wrapper">
                        <div class="row">
                        <?php
                        $i = 0 + $pagenum;
                        if (!empty($images)) {                          
                            foreach ($images as $key => $val) {
                                ?>
                                <div class="col-xl-3 col-lg-4 col-sm-4 col-md-12 content product-grid">
                                    <div class="gallery-image-outer">
                                        <img src="<?= base_url($val->image)?>" class='gallery_image'>
                                        <div class="gallery-manage-btnmain">
                                            <a href="javascript:void(0)" class="btn btn-primary default btn-sm view_image" id="view_image" data-data_id="<?= $val->id ?>"><i class="iconsmind-Eye-Visible" aria-hidden="true"></i><span>view image</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        </div>
                    </div>

                    <?php if (empty($images)) { ?>
                        <div class="text-center text-danger">No record found!</div>
                    <?php } ?>
                <?php echo $links; ?>
            </form>
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
<script>
function filter_field_reset() {
    $(".filter-select-box").val('');
    $('#galleryFilterFrm').submit();
}
$(document).ready(function(){
    $('.view_image').on('click', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "c_level/Image_controller/view_image/" + id;
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    var data = JSON.parse(res);
                    var tag_details = '';
                    $("#prevImg").attr('src',data[0].image);

                    $.each(data[2], function (key, value) {
                        tag_details += '<label class="col-sm-3 col-form-label">' + value.tag_name + ' :</label>';
                        tag_details += '<div class="col-sm-3 col-form-data"><span>' + value.tag_value + '</span></div>';
                    }); 
                    $("#tag-data").html(tag_details);
                    $('#myTag').modal('show');

                }, error: function () {

                }
            });
        });
});
</script>
<?php
$this->load->view('c_level/image/view_image');
?>
