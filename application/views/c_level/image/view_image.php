<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .pattern_select .select2-container {
        width: 100% !important;
    }
    .c-level-gallery-image-view .col-form-label {
        font-size: 13px;
        font-weight: 700;
        padding-top: 0;
        padding-bottom: 0;
        line-height: 30px;
    }
    .c-level-gallery-image-view .col-form-data span {
        line-height: 30px;
    }
</style>
<div id="myTag" class="modal fade c-level-gallery-image-view" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Image Info</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">



                <?php
                $attributes = array('id' => 'view_image', 'name' => 'view_image', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>


                <div class="col-lg-12 px-4">
                    <div class="form-group">
                        <img src="" class="img-thumbnail" id="prevImg">
                    </div>
                    <div class="row" id="tag-data"></div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>