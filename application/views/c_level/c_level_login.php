<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>BMS Link</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/iconsmind/style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/simple-line-icons/css/simple-line-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-float-label.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/main.css" />
    </head>
    <script type="text/javascript">
        var mybase_url = '<?php echo base_url(); ?>';
    </script>
    <style type="text/css">
        .fixed-background {
            background: none !important;
            /*background: url(../img/balloon.jpg) no-repeat center center fixed;*/
            background-size: cover;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <body class="background show-spinner">
        <div class="fixed-background"></div>
        <main>
            <div class="container">
                <div class="row h-100">
                    <div class="col-12 col-md-6 mx-auto my-auto">
                        <div class="card auth-card">
                            <div class="form-side">
                                <a href="" class="mb-5 text-center d-block">
                                    <img src="<?php echo base_url(); ?>assets/c_level/img/logo.png" alt="">
                                </a>
                                <div class="">
                                    <?php
                                    $exception = $this->session->flashdata('exception');
                                    if ($exception)
                                        echo $exception;
                                    ?>

                                    <?php
                                    $message = $this->session->flashdata('message');
                                    if ($message)
                                        echo $message;
                                    $success = $this->session->flashdata('success');
                                    if ($success)
                                        echo $success;
                                    ?>
                                </div>
                                <h6 class="mb-4">Login</h6>
                                <form action="<?php echo base_url(); ?>c_level/auth_controller/authentication" method="post">
                                    <label class="form-group has-float-label mb-4">
                                        <input class="form-control" type="text" name="email" value="" placeholder="E-mail / Username" required/>
                                        <span>E-mail / Username</span>
                                    </label>

                                    <label class="form-group has-float-label mb-4">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required />
                                        <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password" style="position: relative; left: 370px; top: -30px;"></span>
                                        <span>Password</span>
                                    </label>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a href="<?php echo base_url(); ?>c-level-forgot-password-form">Forgot password </a>
                                        <input type="submit" class="btn btn-primary btn-lg btn-shadow"  value="LOGIN">
<!--                                        <a href="<?php echo base_url(); ?>c-level-dashboard" class="btn btn-primary btn-lg btn-shadow">LOGIN</a>-->
                                    </div>
                                    <!--                                    <div class="d-block">
                                                                            <a href="<?php echo base_url(); ?>d-customer-form" class="d-block">D-Level Form</a>
                                                                        </div>-->

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/c_level/js/dore.script.js"></script>
        <script src="<?php echo base_url(); ?>assets/c_level/js/scripts.js"></script>
        <script type="text/javascript">
        $(document).ready(function () {
            //    ============ its for show password ===============
            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        });
        </script>
    </body>
</html>