
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Add User</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Add User</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>user-save" method="post" name="sameFrm">
                            <div class="form-group row">
                                <label for="first_name" class="col-sm-3 col-form-label">First Name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" onkeyup="required_validation()" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name" class="col-sm-3 col-form-label">Last Name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" onkeyup="required_validation()" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" onkeyup="required_validation()" required>
                                    <span id="error"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">Password <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required />
                                    <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
                                    <input type="button" class="button col-md-4 password_generate_btn btn btn-sm" value="Generate" onClick="generate();" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fixed_commission" class="col-sm-3 col-form-label">Fixed Commission </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control NumbersAndDot" name="fixed_commission" id="fixed_commission"  placeholder="Fixed Commission">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="percentage_commission" class="col-sm-3 col-form-label">Commission (%)</label>
                                <div class="col-sm-6">
                                     <input type="text" class="form-control NumbersAndDot percentage_valid" name="percentage_commission" id="percentage_commission"  placeholder="Commission (%)">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-6">
                                    <button type="submit" id="add-customer" class="btn btn-primary btn-sm" name="add_user">Save</button>
                                    <button type="submit" name="add_another_user" class="btn btn-success btn-sm" id="add-customer-another">Save And Add Another</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>


<script type="text/javascript">
    $(document).ready(function () {
        //    =========its for check mobile no unique ============
        $("body").on('keyup', '#email', function () {
            var email = $("#email").val();
            var email = encodeURIComponent(email);
            var data_string = "email=" + email;
            $.ajax({
                url: "user-email-check",
                type: 'post',
                data: data_string,
                success: function (r) {
//                    alert(r);
                    if (r != 0) {
//                        $('input[type=submit]').prop('disabled', true);
                        $("#error").html("Email already exists!");
                        $("#error").css({'color': 'red', 'font-weight': 'bold', 'display':'block'});
                        $("#email").css({'border': '2px solid red'}).focus();
                        return false;
                    } else {
                        $("#error").hide();
//                        $('input[type=submit]').prop('disabled', false);
                        $("#email").css({'border': '2px solid green'}).focus();
                    }
                }
            });
        });
    });


//    ============ its for show password ===============
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
//    ============ its for generate password ============
    function randomPassword(length = 6) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    function generate() {
        sameFrm.password.value = randomPassword(sameFrm.length.value);
    }
//    ============ close generate password =============

    //        ========== some field validation ============   
    $('button[type=submit]').prop('disabled', true);
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#email").val() !='') {
//            $("#first_name").css({'border': '1px solid red'}).focus();
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }

    $('.NumbersAndDot').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

    $('.percentage_valid').keyup(function(){
      if ($(this).val() > 100){
        $(this).val('100');
      }
    });
</script>