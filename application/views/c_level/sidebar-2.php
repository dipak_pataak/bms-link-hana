
<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="active" id="remove_active">
                    <a href="<?php echo base_url(); ?>c-level-dashboard">
                        <i class="iconsmind-Shop-4"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <?php
                $allmodule = $this->db->select('*')->from('menusetup_tbl')->group_by('module')->order_by('ordering', 'asc')->get()->result();
                //$i = 0;
                foreach ($allmodule as $module) {
                    // $i++;
                    if ($this->permission->module($module->module)->access()) {
                        ?>
                        <li id="<?php echo $module->module; ?>">
                            <a href="#<?php echo $module->module; ?>">
                                <i class="<?php echo $module->icon; ?>"></i> <?php echo ucfirst($module->menu_title); ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>
                <?php if ($this->session->userdata('isAdmin') == '1') { ?>
<!--                    <li>
                        <a href="#setting">
                            <i class="simple-icon-settings"></i> Settings
                        </a>
                    </li>-->
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <?php
            foreach ($allmodule as $module) {
                $menu_item = $this->db->select('*')
                                ->from('menusetup_tbl')
                                ->where('module', $module->module)
                                ->where('parent_menu =', $module->id)
                                ->order_by('ordering', 'asc')
                                ->get()->result();
                $module_id = $module->parent_menu;
//              
                ?>
                <ul class="list-unstyled" data-link="<?php echo $module->module; ?>">
                    <?php
                    foreach ($menu_item as $menu) {
//                        var_dump($this->permission->check_label($menu->menu_title)->access());exit();
                        if ($this->permission->check_label($menu->menu_title)->access()) {
                            $parent_id = $menu->id;
                            $sub_sub_menu = $this->db->select('*')
                                            ->from('menusetup_tbl')
                                            ->where('module', $menu->module)
                                            ->where('parent_menu =', $parent_id)
                                            ->order_by('ordering', 'asc')
                                            ->get()->result();
                            ?>
                            <li>
                                <?php if (!empty($sub_sub_menu)) { ?>
                                    <a href="#" class="menu-link"> <?php echo ucwords(str_replace('_', ' ', $menu->menu_title)); ?> <i class="simple-icon-arrow-down"></i></a>

                                    <ul class="list-unstyled">
                                        <li>
                                            <?php
                                            foreach ($sub_sub_menu as $child) {
//                                                var_dump($this->permission->check_label($child->menu_title)->access());exit();
                                                if ($this->permission->check_label($child->menu_title)->access()) {
                                                    ?>
                                                    <a href="<?php echo base_url(); ?><?php echo $child->page_url; ?>">
                                                        <?php echo ucwords(str_replace('_', ' ', $child->menu_title)); ?>
                                                    </a>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                <?php } else { ?>
                                    <a href="<?php echo base_url(); ?><?php echo $menu->page_url; ?>" class="menu-link"> <?php echo ucwords(str_replace('_', ' ', $menu->menu_title)); ?></a>
                                <?php } ?>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            <?php } ?>

            <?php if ($this->session->userdata('isAdmin') == '1') { ?>
<!--                <ul class="list-unstyled" data-link="setting">
                    <li>
                        <a href="<?php echo base_url(); ?>company-profile"> Company Profile Info</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>payment-setting"> Payment Option</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>c-cost-factor"> Cost Factor</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>iframe-code"> Web Iframe Code</a>
                    </li>
                    <li>
                        <a href="#" class="menu-link"> User &amp; Access<i class="simple-icon-arrow-down"></i></a>
                        <ul class="list-unstyled">
                            <li>
                                <a href="<?php echo base_url(); ?>users">Users</a>
                                <a href="<?php echo base_url(); ?>menu-setup">Menu Setup</a>
                                <a href="<?php echo base_url(); ?>role-permission">Role Permission</a>
                                <a href="<?php echo base_url(); ?>role-list">Role List</a>
                                <a href="<?php echo base_url(); ?>user-role">Add User Roles</a> 
                                <a href="<?php echo base_url(); ?>access-role">Access Role</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>c-us-state"> US State</a>
                    </li>
                </ul>-->
            <?php } ?>
        </div>
    </div>
</div>

