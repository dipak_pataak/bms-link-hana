<div class="row">
    <div class="col-lg-5 border-right">
        <h4 class="mb-3">Client Info:</h4>
        <div class="mb-3">
            <b>Client Name</b> : <?php echo $show_customer_record[0]['first_name'] . " " . $show_customer_record[0]['last_name']; ?> <br>
            <b>Sidemark</b> : <?php echo $show_customer_record[0]['side_mark']; ?><br>
            <b>Address</b> : 
            <a href="javascript:void(0)" id="address_<?php echo $show_customer_record[0]['customer_id']; ?>" class="address phone_email_link" onclick="show_address_map(<?php echo $show_customer_record[0]['customer_id']; ?>);">
            <?php echo $show_customer_record[0]['address'] . ", " . @$show_customer_record[0]['city'] . ", " . @$show_customer_record[0]['state'] . ", " . @$show_customer_record[0]['zip_code'] . ", " . @$show_customer_record[0]['country_code']; ?>
            </a> <br>
            <b>Email</b>: 
            <a href="mailto:<?php echo $show_customer_record[0]['email']; ?>" class="phone_email_link">
            <?php echo $show_customer_record[0]['email']; ?>
            </a> <br>
            <?php foreach ($get_customer_phones as $phones) { ?>
                <b><?php echo $phones->phone_type; ?></b> : 
                <a href="tel:<?php echo $phones->phone; ?>" class="phone_email_link"><?php echo $phones->phone; ?> </a> <br>
            <?php } ?>
        </div>
        <div style="margin-top: 10px;">
            <strong>Uploaded Files </strong>
            <?php
            $this->db->select('*');
            $this->db->from('customer_file_tbl');
            $this->db->where('customer_id', $show_customer_record[0]['customer_id']);
            $customer_uploded_files = $this->db->get()->result();
            foreach ($customer_uploded_files as $upload_file) {
                ?>
                <a href="<?php echo base_url(); ?>assets/c_level/uploads/customers/<?php echo $upload_file->file_upload; ?>">
                    <i class="simple-icon-folder" style="padding: 5px; font-size: 20px;" title="<?php echo $upload_file->file_upload; ?>"></i>
                </a>
            <?php } ?>
        </div>
        <div class="d-flex flex-row mb-3 py-3 border-bottom border-top">
            <!--            <a href="#">
                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-11.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall">
                        </a>-->
            <div class="pl-3 pr-2" style="overflow-y: scroll; width: 100%; height: 85px;">
                <?php
//                dd($remarks);
                if ($remarks) {
                    foreach ($remarks as $val) {
                        ?>
                        <p class="font-weight-medium mb-0"><?= $val->remarks; ?></p>
                        <p class="text-muted mb-1 text-small">
                            <?php
                            if ($val->level_from == 'd') {
                                echo $show_customer_record[0]['first_name'] . " " . $show_customer_record[0]['last_name'];
                            } else {
                                echo $show_customer_record[0]['comment_by'];
                            }
                            ?> |
                            <?= $val->appointment_date; ?> <?php if($val->appointment_time){ echo "-" .$val->appointment_time; } ?></p>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <?php
//    echo '<pre>';    print_r($show_customer_appointment_record); echo '</pre>'; 
    if ($scheduled == 're-schedule' && $show_customer_record[0]['now_status'] == 'scheduled') {
        $c_level_staff_id = @$show_customer_appointment_record[0]->c_level_staff_id;
        $appointment_date = @$show_customer_appointment_record[0]->appointment_date;
        $appointment_time = @$show_customer_appointment_record[0]->appointment_time;
        $appointment_remarks = @$show_customer_appointment_record[0]->remarks;
    } else {
        $c_level_staff_id = '';
        $appointment_date = '';
        $appointment_time = '';
        $appointment_remarks = '';
    }
    ?>
    <div class="col-lg-7">
        <form action="<?php echo base_url(); ?>appointment-setup" method="post">
            <div class="form-row">
                <div class="form-group col-md-12 text-left"><?php // dd($get_users);                             ?>
                    <label for="c_level_staff_id" class="col-form-label">Responsible Staff <?php // echo $show_customer_record[0]['customer_id']. "zzz ".$show_customer_record[0]['now_status'];                          ?></label>
                    <select class="form-control select2" id="c_level_staff_id" name="c_level_staff_id" data-placeholder="-- select one --" required>
                        <option value="">-- select one --</option>
                        <?php foreach ($get_users as $user) { ?>
                            <option value='<?php echo $user->id; ?>' <?php
                            if ($user->id == $c_level_staff_id) {
                                echo 'selected';
                            }
                            ?>>
                                        <?php echo $user->first_name . " " . $user->last_name; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-12 text-left">
                    <label for="setdata" class="col-form-label">Set Date</label>
                    <input class="form-control datepicker" placeholder="Date" name="appointment_date" value="<?php echo $appointment_date; ?>" required>
                </div>
                <div class="form-group col-md-12 text-left">
                    <label for="setime" class="col-form-label">Set Time</label>
                    <input type="text" class="form-control" data-header-left="true" id="datepicker-left-header" name="appointment_time" value="<?php echo $appointment_time; ?>" required>
                </div>
                <div class="form-group col-md-12 text-left">
                    <label for="remarks" class="col-form-label bfh-timepicker">Remarks</label>
<!--                    <input class="form-control" name="remarks" id="remarks" placeholder="Remarks" value="<?php echo $appointment_remarks; ?>">-->
                    <textarea class="form-control" name="remarks" id="remarks" placeholder="Remarks"><?php echo $appointment_remarks; ?></textarea>
                </div>
                <input type="hidden" name="status" id="status">

                <div class="form-group col-md-12 text-right mb-0">
                    <button type="button" class="btn btn-danger btn-sm default" data-dismiss="modal">Close</button>
                    <input type="hidden" name="customer_id" value="<?php echo $show_customer_record[0]['customer_id']; ?>">
                    <button type="submit" class="btn btn-success btn-sm default">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>