<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Customer List</h1>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="15%">Name</th>
                            <th width="15%">Phone</th>
                            <th width="15%">Email</th>
                            <th width="22%">Address</th>
                            <!--<th width="10%">Status</th>-->
                            <th width="15%" class="text-center">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $sl = 0;
                        foreach ($get_top_search_customer_info as $customer) {
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td> <a href="<?php echo base_url(); ?>customer-view/<?php echo $customer->customer_id; ?>"><?php echo $customer->first_name . " " . @$customer->last_name; ?></a></td>
                                <td>
                                    <a href="tel:<?php echo $customer->phone; ?>"><?php echo $customer->phone; ?></a>
                                </td>
                                <td>
                                    <a href="mailto:<?php echo $customer->email; ?>"><?php echo $customer->email; ?></a>
                                </td>

                                <td>
                                    <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address phone_email_link" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                                        <?php echo $customer->address . '<br>' . @$customer->city . ', ' . @$customer->state . ', ' . @$customer->zip_code . ', ' . @$customer->country_code; ?>
                                    </a>
                                </td>

                               <!--  <td>
                                    <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address phone_email_link" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                                        <?php echo $customer->address; ?><br />
                                        <?php echo $customer->city; ?>, <?php echo $customer->state; ?>, <?php echo $customer->zip_code; ?>, <?php echo $customer->country_code; ?>
                                    </a>
                                </td> -->

                                <td>
                                    <a href="<?php echo base_url(); ?>customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success btn-xs default" data-toggle="tooltip" data-placement="top" data-original-title="View">
                                        <i class="simple-icon-eye"></i>
                                    </a>
                                    <a href="<?php echo base_url(); ?>customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                    <a href="<?php echo base_url(); ?>c-customer-delete/<?php echo $customer->customer_id; ?>" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="glyph-icon simple-icon-trash"></i></a>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <?php if (empty($get_top_search_customer_info)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="8" class="text-center  text-danger">No record found!</th>
                            </tr> 
                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Order List</h1>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <table class="table table-bordered table-hover text-center">
                    <thead>
                        <tr>
                            <th>Order/Quote No</th>
                            <th>Client Name </th>
                            <th>Side mark</th>
                            <th>Order date</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (!empty($get_top_search_order_info)) {
                            foreach ($get_top_search_order_info as $key => $value) {

                                $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                                $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                                ?>
                                <tr>
                                    <td><?= $value->order_id; ?></td>
                                    <td><?= $value->customer_name; ?></td>
                                    <td><?= $value->side_mark; ?></td>
                                    <td><?= (@$value->order_date); ?></td>
                                    <td><?= $company_profile[0]->currency ?><?= $value->grand_total ?></td>

                                    <td>
                                        <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                            <option value="">--Select--</option>
                                            <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                            <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                            <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                            <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                            <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                        </select>
                                    </td>

                                    <td>
                                        <a href="<?= base_url('c_level/invoice_receipt/receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-xs default" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                            <i class="simple-icon-eye"></i>
                                        </a>
                                        <!-- <a href="delete-order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="simple-icon-pencil"></i></a> -->
                                        <a href="c_level/order_controller/delete_order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                    <?php if (empty($get_top_search_order_info)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="8" class="text-center text-danger">
                                    Record not found!
                                </th>
                            </tr>
                        </tfoot>
                    <?php } ?>
                </table>
            </div>



                    <div class="modal fade" id="customer_address_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Customer Address Map Show</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="customer_comment_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">All Comments</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_comment_info">
                                    <form class="" action="<?php echo base_url('b_level/Return_controller/return_resend_comment'); ?>" method="post">
                                        <textarea class="form-control" name="resend_comment" required ></textarea>
                                        <input type="hidden" name="return_id" id="return_id">
                                        <input type="submit" class="btn btn-info" style="margin-top: 10px;">
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>



        </div>
    </div>
</main>


<script>
//    $(document).ready(function () {
//        $(".inner_table").on("change", function () {
//            $modal = $('#appointschedule');
//            if ($(this).val() === 'schedule') {
////                alert("DD");
//                $modal.modal('show');
//            }
//        });
//    });
    function show_customer_record(t, statuss) {
//        alert(t);
//    $("#appointschedule").modal('show');
//var statuss = $("#sta").val();
//alert(statuss);
        $.post("<?php echo base_url(); ?>show-customer-record/" + t, 'status=' + statuss, function (t) {
            $("#customer_info").html(t);
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
//        ======== its timer =========== format: 'LT'
            $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
            $('#datepicker-left-header').datetimepicker({
//            'format': "HH:mm", // HH:mm:ss its for 24 hours format
                format: 'LT', /// its for 12 hours format
            });
            var st = $('#sta').val();
            $('#status').val(statuss);
            $("form :input").attr("autocomplete", "off");
            $("#appointschedule").modal('show');
        });
    }
    //    ========== its for customer search ======
    function customerkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>c-level-customer-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#result_search").html(r);
            }
        });
    }

    //============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)
//        alert(location);
        $.post("<?php echo base_url(); ?>c-show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }
//    ============ its for show_old_comment ============
    function show_old_comment(id) {
        $.ajax({
            url: "<?php echo base_url(); ?>show-old-c-cstomer-comment",
            type: "post",
            data: {customer_id: id},
            success: function (t) {
                $("#customer_comment_info").html(t);
                $('#customer_comment_modal_info').modal('show');
            }
        });
    }
</script>
<script type="text/javascript">
    $('#s1').hide();
    $('#c1').hide();
    function viewFild(id) {
        if (id == 1) {
            $('#s1').show();
            $('#c1').hide();
        } else if (id == 2) {
            $('#s1').show();
            $('#c1').show();

        } else {
            $('#s1').hide();
            $('#c1').hide();
        }
    }
</script>