<style>
    .pac-container.pac-logo{
        top: 400px !important;
    }
    .pac-container:after{
        content:none !important;
    }

    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Customer Info</h1>
                    <?php
//        echo '<pre>'; print_r($customer_edit); 
                    $customer_no = explode('-', $customer_edit[0]['customer_no']);
//        dd($customer_no);
                    ?>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Customer</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>customer-update/<?php echo $customer_edit[0]['customer_id']; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="first_name">First Name  <span class="text-danger"> * </span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="John" required value="<?php echo $customer_edit[0]['first_name']; ?>">
                                    <input type="hidden" class="form-control" id="customer_no" name="customer_no" value="<?php echo @$customer_no[0] . "-" . @$customer_no[1]; ?>">
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="last_name">Last Name  <span class="text-danger"> * </span></label>
                                    <input type="text" class="form-control" id="last_name" placeholder="Doe" name="last_name"required value="<?php echo $customer_edit[0]['last_name']; ?>">
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email <span class="text-danger"> * </span></label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="johndoe@yahoo.com" value="<?php echo $customer_edit[0]['email']; ?>" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <table class="" id="normalItem" style="margin-top: 25px;">
                                        <thead>
                <!--                            <tr>
                                                <th class="text-center">Phone</th>
                                                <th class="text-center">Action </th>
                                            </tr>-->
                                        </thead>
                                        <tbody id="addItem">
                                            <?php
                                            $this->db->select('*');
                                            $this->db->from('customer_phone_type_tbl');
                                            $this->db->where('customer_id', $customer_edit[0]['customer_id']);
                                            $customer_phone_types = $this->db->get()->result();
                                            $j = 0;
                                            foreach ($customer_phone_types as $phone_type) {
                                                $j++;
                                                ?>
                                                <tr>
                                                    <td>
                                                        <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                                                            <option value="">Type</option>
                                                            <option value="Phone" <?php
                                                            if ($phone_type->phone_type == 'Phone') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Phone</option>
                                                            <option value="Fax" <?php
                                                            if ($phone_type->phone_type == 'Fax') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Fax</option>
                                                            <option value="Mobile" <?php
                                                            if ($phone_type->phone_type == 'Mobile') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Mobile</option>
                                                        </select>
                                                        <input id="phone_1" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" value="<?php echo $phone_type->phone; ?>">
                                                    </td>
                                                    <td class="text-left">
                                                        <a style="font-size: 20px; cursor: pointer; " class="text-danger"  value="Delete" onclick="deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <button id="add-item" class=" btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 5px 15px 15px;"><span class="simple-icon-plus"></span></button>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="address">Address <span class="text-danger"> * </span></label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" value="<?php echo $customer_edit[0]['address']; ?>" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="company">Company</label>
                                    <input type="text" class="form-control" id="company" name="company" placeholder="ABC Tech" value="<?php echo $customer_edit[0]['company']; ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="state">State</label>
                                    <input type="text" class="form-control" id="state" name="state" placeholder="State" value="<?php echo $customer_edit[0]['state']; ?>">
<!--                                    <select class="form-control select2" id="state" name="state">
                                        <option value="">-- select one -- </option>
                                    <?php
                                    foreach ($get_states as $states) {
                                        if ($customer_edit[0]['state'] == $states->state_id) {
                                            echo "<option selected value='$states->state_id'>$states->state_name</option>";
                                        } else {
                                            echo "<option value='$states->state_id'>$states->state_name</option>";
                                        }
                                    }
                                    ?>
                                    </select>-->
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="city">City </label>
                                    <input type="text" class="form-control" id="city" name="city" value="<?php echo $customer_edit[0]['city']; ?>">
<!--                                    <select class="form-control select2" id="city" name="city" data-placeholder="-- select one --">
                                        <option value="">-- select one --</option>
                                    <?php
                                    foreach ($get_states_wise_city as $city) {
                                        echo $city->id;
                                        ?>
                                                                                                                        <option value='<?php echo $city->id; ?>' <?php
                                        if ($customer_edit[0]['city'] == $city->id) {
                                            echo 'selected';
                                        }
                                        ?>><?php echo $city->city; ?></option>
                                    <?php } ?>
                                    </select>-->
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="zip_code">Zip</label>
                                    <input type="text" class="form-control" id="zip_code" name="zip_code" value="<?php echo $customer_edit[0]['zip_code']; ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="country_code">Country Code</label>
                                    <input type="text" class="form-control" id="country_code" name="country_code" value="<?php echo $customer_edit[0]['country_code']; ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="reference">Referred By</label>
                                    <input type="text" class="form-control" id="reference" name="reference" placeholder="johndoe18" value="<?php echo $customer_edit[0]['reference']; ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <table class="" id="normalItem_file"  style="margin-bottom: 5px; margin-top: 25px;">
                    <!--                    <thead>
                                            <tr>
                                                <th class="text-center">Files</th>
                                                <th class="text-center">Action </th>
                                            </tr>
                                        </thead>-->
                                        <tbody id="addItem_file">
                                            <?php
                                            $this->db->select('*');
                                            $this->db->from('customer_file_tbl');
                                            $this->db->where('customer_id', $customer_edit[0]['customer_id']);
                                            $customer_uploded_files = $this->db->get()->result();
                                            foreach ($customer_uploded_files as $upload_file) {
                                                ?>
                                                <tr>
                                                    <td class="text-center">
                                                        <img src="<?php echo base_url(); ?>assets/c_level/uploads/customers/<?php echo $upload_file->file_upload; ?>" width="15%">
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo base_url(); ?>c-customer-file-delete/<?php echo $upload_file->id . "/" . $upload_file->customer_id; ?>" class="text-danger" style="font-size: 20px;" onclick="return confirm('Do you want to delete it?')">
                                                            <i class="simple-icon-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr id="1">
                                                <td>
                                                    <input id="file_upload_1" class="form-control" type="file" name="file_upload[]" multiple>
                                                </td>
                                                <td class="text-left" width="25%">
                                                    <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                    <a style="font-size: 20px;" class="text-danger" value="Delete" onclick="file_deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button id="add-item" class="btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" type="button" style="margin: 0px 15px 15px;"><span class="simple-icon-plus"></span> </button>
                    <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                    <span class="file_type_cls text-warning"> [ File size maximum 2 MB and  jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
                                </div>
                            </div>

                            <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-sm btn-danger d-block float-right">Cancel</a> &nbsp;
                            <button type="submit" class="btn btn-primary btn-sm d-block float-right">Update</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
<script type="text/javascript">
    $(document).ready(function () {
        //        ================== its for state wise city show ======================
        $('body').on('change', '#state', function () {
            var state = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>state-wise-city/" + state,
                type: 'get',
                success: function (r) {
                    r = JSON.parse(r);
//                    alert(r);
                    $("#city").empty();
                    $("#city").html("<option value=''>-- select one -- </option>");
                    $.each(r, function (ar, typeval) {
                        $('#city').append($('<option>').text(typeval.city).attr('value', typeval.id));
                    });
                }
            });
        });

    });
    //    ======= its for google place address geocomplete =============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip_code").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });


    });



    // function validFile(){
    //     var a = $('#file_upload_1').files[0].size;
    //     alert(a);
    //     var sizee = $("#fileup_1")[0].files[0].size;
    // }

        $("body").on("change", "#file_upload_1", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });




        $("body").on("change", "#file_upload_2", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });




        $("body").on("change", "#file_upload_3", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });


        $("body").on("change", "#file_upload_4", function (e) {
            
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr( (file.lastIndexOf('.') +1) );

            // check extention
            // check extention
            if(ext!=='jpg' && ext!=='JPG' && ext!=='png' && ext!=='PNG' && ext!=='jpeg' && ext!=='JPEG' && ext!=='pdf' && ext!=='doc' && ext!=='docx' && ext!=='xls' && ext!=='xlsx'){
                alert("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
               $(this).val('');
            }
            // chec size
            if(size > 2000000) {
               alert("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


        });





//    ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            alert("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count, e = document.createElement("tr");
            e.innerHTML = "\n\
                                 <td class='text-center'><select id='phone_type_" + count + "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value=''>Type</option><option value='Phone'>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" + count + "' class='phone form-control phone_no_type current_row' type='text' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    count++;
//=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function (e) {
                var key = e.charCode || e.keyCode || 0;
                var phone = $(this);
                if (phone.val().length === 0) {
                    phone.val(phone.val() + '+1 (');
                }
                // Auto-format- do not expose the mask as the user begins to type
                if (key !== 8 && key !== 9) {
                    //alert("D");
                    if (phone.val().length === 6) {

                        phone.val(phone.val());
                        phone = phone;
                    }
                    if (phone.val().length === 7) {
                        phone.val(phone.val() + ') ');
                    }
                    if (phone.val().length === 12) {
                        phone.val(phone.val() + '-');
                    }
                    if (phone.val().length >= 17) {
                        phone.val(phone.val().slice(0, 16));
                    }
                }
                // Allow numeric (and tab, backspace, delete) keys only
                return (key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
            })
                    .on('focus', function () {
                        phone = $(this);

                        if (phone.val().length === 0) {

                            phone.val('+1 (');
                        } else {
                            var val = phone.val();
                            phone.val('').val(val); // Ensure cursor remains at the end
                        }
                    })

                    .on('blur', function () {
                        $phone = $(this);

                        if ($phone.val() === '(') {
                            $phone.val('');
                        }
                    });

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
        }
    }
//    ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);

            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function () {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });

            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function () {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }
//    ======== close ===========
    //========== its for customer multiple file upload ==============
    function addInputFile(t) {
        var table_row_count = $("#normalItem_file > tbody > tr").length;
        var file_count = table_row_count + 1;
        file_limits = 5;
        if (file_count == file_limits) {
//            alert("You have reached the limit of adding " + file_count + " files");
            alert("You have reached the limit of adding 4 files");
        } else {

//            alert(table_row_count);
            var a = "file_upload_" + file_count, e = document.createElement("tr");
            e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                ",
                    document.getElementById(t).appendChild(e),
//                    document.getElementById(a).focus(),
                    file_count++;
        }
    }
    function file_deleteRow(t) {
        var a = $("#normalItem_file > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_file_count = 1;
            $("#normalItem_file > tbody > tr td input.current_file_count").each(function () {
                current_file_count++;
                $(this).attr('class', 'form-control');
                $(this).attr('id', 'file_upload_' + current_file_count);
                $(this).attr('name', 'file_upload_' + current_file_count);
            });

        }
    }
</script>