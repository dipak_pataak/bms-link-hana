
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Customer Bulk Upload</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Bulk</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4 bulk_upload">
                    <div class="row">
                        <div class="col-sm-7">
                            <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span>
                        </div>
                        <div class="col-sm-5">
                            <a href="<?php echo base_url('assets/c_level/csv/customer_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo form_open_multipart('c-customer-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="file" class="attatch_file" name="upload_csv_file" id="" required> 
                                <small id="fileHelp" class="text-muted"></small>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Submit</button>
                        </div>
                        </form>                
                    </div>
                </div>
                <!--                <div class="card mb-4">
                                    <a href="<?php echo base_url('assets/c_level/csv/customer_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                                    <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br>
                                    <div class="card-body">
                <?php echo form_open_multipart('c-customer-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                                        <div class="form-group text-center">
                                            <div class="col-sm-12">
                                                <input type="file" class="attatch_file" name="upload_csv_file" id="" required> 
                                                <small id="fileHelp" class="text-muted"></small>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Submit</button>
                                        </div>
                <?php echo form_close() ?>
                                    </div>
                                </div>-->
            </div>

        </div>
    </div>
</main>
