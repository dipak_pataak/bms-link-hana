<style type="text/css">    
    .phone_email_link{color: #007bff;}
    .highcharts-credits, .highcharts-button-symbol{
        display: none;
    }
</style>
<?php $currency = $company_profile[0]->currency; ?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Customer View</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">View</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-12 mb-4">
                <div class="card mb-4" style="height: 350px;">
                    <div class="position-absolute card-top-buttons">
                        <button class="btn btn-outline-white icon-button ">
                            <i class="simple-icon-pencil"></i>
                        </button>
                    </div>

                    <div class="card-body">
                        <h6 class="mb-3">Customer Info:</h6> 
                        <a href="<?php echo base_url(); ?>customer-edit/<?php echo $customer_view[0]['customer_id']; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title=""><i class="simple-icon-pencil"></i></a>
                        <p class="m-0">
                            <?php if ($customer_view[0]['company']) { ?><b>Company Name</b> : <?php echo $customer_view[0]['company']; ?><br><?php } ?>
                            <b>Customer Name</b> : <?php echo $customer_view[0]['first_name'] . " " . $customer_view[0]['last_name']; ?> <br>
                            <b>Side Mark </b>: 
                            <?php
//                            if (empty($customer_view[0]['company'])) {
                                echo $customer_view[0]['side_mark'];
//                            } else {
//                                echo $customer_view[0]['company_customer_id'];
//                            }
                            ?>  
                            <br>
                            <b>Address</b> : 
                            <a href="javascript:void(0)" id="address_<?php echo $customer_view[0]['customer_id']; ?>" class="address phone_email_link" onclick="show_address_map(<?php echo $customer_view[0]['customer_id']; ?>);">
                                <?php echo @$customer_view[0]['address'] . ", " . @$customer_view[0]['city'] . ", " . @$customer_view[0]['state'] . " " . @$customer_view[0]['zip_code'] . ", " . @$customer_view[0]['country_code']; ?> <br>
                            </a>
                            <b>Email</b> : <a href="mailto:<?php echo $customer_view[0]['email']; ?>" class="phone_email_link"><?php echo $customer_view[0]['email']; ?></a><br>
                            <?php foreach ($get_customer_phones as $phones) { ?>
                                <b><?php echo $phones->phone_type; ?></b> : 
                                <a href="tel:<?php echo $phones->phone; ?>" class="phone_email_link"><?php echo $phones->phone; ?> </a> <br>
                            <?php } ?>
                            <b>Referred By</b> : <?php echo $customer_view[0]['reference']; ?> <br>
                            <!--                            <b>Web Address</b>: www.johnvek.com <br>
                                                        <b>Status</b>: Active -->
                        </p>

                    </div>
                    <div class="modal fade" id="customer_address_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Customer Address Map Show</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div style="margin-top: 10px;">
                            <strong>Uploaded Files</strong>
                            <?php
                            $this->db->select('*');
                            $this->db->from('customer_file_tbl');
                            $this->db->where('customer_id', $customer_view[0]['customer_id']);
                            $customer_uploded_files = $this->db->get()->result();
                            foreach ($customer_uploded_files as $upload_file) {
                                ?>
                                <a href="<?php echo base_url(); ?>assets/c_level/uploads/customers/<?php echo $upload_file->file_upload; ?>" target="_new">
                                    <i class="simple-icon-folder" style="padding: 5px; font-size: 20px;" title="<?php echo $upload_file->file_upload; ?>"></i>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 mb-4">
                <div id="all_order" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
            </div>
            <div class="col-6 mb-4">
                <div id="paid_order" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
            </div>

        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover m-0">
                                <thead>
                                    <tr>
                                        <th>Order/Quote No</th>
                                        <th>Client Name </th>
                                        <th>Side mark</th>
                                        <th>Order date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (!empty($orderd)) {

                                        foreach ($orderd as $key => $value) {

                                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                                            ?>

                                            <tr>
                                                <td><?= $value->order_id; ?></td>
                                                <td><?= $value->customer_name; ?></td>
                                                <td><?= $value->side_mark; ?></td>
                                                <td><?= date_format(date_create($value->order_date), 'M-d-Y'); ?></td>
                                                <td><?= $currency; ?><?= $value->grand_total ?></td>

                                                <td>
                                                    <select class="form-control select2-single" data-placeholder="--select one--" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                                        <option value=""></option>
                                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Manufacturing</option>
                                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping</option>
                                                        <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
                                                        <option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>
                                                    </select>
                                                </td>

                                                <td>
                                                    <a href="<?= base_url('c_level/invoice_receipt/receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-xs default" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
                                                        <i class="simple-icon-eye"></i>
                                                    </a>
                                                    <!-- <a href="delete-order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="simple-icon-pencil"></i></a> -->
                                                    <a href="c_level/order_controller/delete_order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title=""><i class="glyph-icon simple-icon-trash"></i></a>
                                                </td>

                                                                                                                                                                                                                                                    <!-- <td class="width_140">
                                                                                                                                                                                                                                                        <a href="<?= base_url('b_level/invoice_receipt/receipt/') ?>/<?= $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a> 
                                                                                                                                                                                                                                                         <a href="delete-order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                                                                                                                                                                                                                                                    </td> -->

                                            </tr>

                                            <?php
                                        }
                                    } else {
                                        ?>
                                    <tfoot>
                                        <tr>
                                            <th colspan="7" class="text-danger text-center"> Record not found!</th>
                                        </tr>
                                    </tfoot>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--        <div class="row">
                    <div class="col-12 chat-app">
                        <div class="card mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Comments</h5>
                                <div>
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <a href="#">
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                        </a>
                                        <div class="pl-3 pr-2">
                                            <a href="#">
                                                <p class="font-weight-medium mb-0">Very tasty, thank you.
                                                </p>
                                                <p class="text-muted mb-1 text-small">Mayra Sibley |
                                                    17.09.2018 - 04:45</p>
                                            </a>
                                        </div>
                                    </div>
        
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <a href="#">
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-7.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                        </a>
                                        <div class="pl-3 pr-2">
                                            <a href="#">
                                                <p class="font-weight-medium mb-0">This cake was delightful
                                                    to eat. Please keep them coming.</p>
                                                <p class="text-muted mb-1 text-small">Barbera Castiglia |
                                                    15.08.2018 - 01:18</p>
                                            </a>
                                        </div>
                                    </div>
        
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <a href="#">
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-2.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                        </a>
                                        <div class="pl-3 pr-2">
                                            <a href="#">
                                                <p class="font-weight-medium mb-0">Your cake is bad and you
                                                    should feel bad.</p>
                                                <p class="text-muted mb-1 text-small">Bao Hathaway |
                                                    26.07.2018 - 11:14</p>
                                            </a>
                                        </div>
                                    </div>
        
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <a href="#">
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-3.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                        </a>
                                        <div class="pl-3 pr-2">
                                            <a href="#">
                                                <p class="font-weight-medium mb-0">Very original recipe!</p>
                                                <p class="text-muted mb-1 text-small">Lenna Majeed |
                                                    17.06.2018 - 09:20</p>
                                            </a>
                                        </div>
                                    </div>
        
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <a href="#">
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-5.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                        </a>
                                        <div class="pl-3 pr-2">
                                            <a href="#">
                                                <p class="font-weight-medium mb-0">This cake was delightful
                                                    to eat. Please keep them coming.</p>
                                                <p class="text-muted mb-1 text-small">Esperanza Lodge |
                                                    16.06.2018 - 16:45</p>
                                            </a>
                                        </div>
                                    </div>
        
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <a href="#">
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-4.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                        </a>
                                        <div class="pl-3 pr-2">
                                            <a href="#">
                                                <p class="font-weight-medium mb-0">Nah, did not like it
                                                    much.</p>
                                                <p class="text-muted mb-1 text-small">Brynn Bragg |
                                                    12.04.2018 - 12:45</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="d-block">
                                        <textarea class="form-control flex-grow-1 mb-4" rows="5" type="text" placeholder="Say something..."></textarea>
                                        <div class="text-right">
                                            <button class="btn btn-success default">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->

    </div>

</main>
<script src="<?php echo base_url(); ?>assets/highcharts/highcharts.js"></script>

<script type="text/javascript">
                                                function setOrderStage(stage_id, order_id) {
                                                $.ajax({
                                                url: "<?php echo base_url('c_level/order_controller/set_order_stage/'); ?>" + stage_id + "/" + order_id,
                                                        type: 'GET',
                                                        success: function (r) {
                                                        toastr.success('Success! - Order Stage Set Successfully');
                                                        setTimeout(function () {
                                                        window.location.href = window.location.href;
                                                        }, 2000);
                                                        }
                                                });
                                                }
                                                $(document).ready(function () {
//    ============= its for chart show =============
                                                Highcharts.chart('all_order', {
                                                chart: {
                                                type: 'spline'
                                                },
                                                        title: {
                                                        text: 'Monthly Order'
                                                        },
                                                        subtitle: {
//                                                                    text: 'Source: WorldClimate.com'
                                                        },
                                                        xAxis: {
                                                        categories: [<?php echo $monthly_sales_month; ?>]
//                                                                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                                        },
                                                        yAxis: {
                                                        title: {
                                                        text: 'Amount Range'
                                                        },
                                                                labels: {
                                                                formatter: function () {
//                                                                            return this.value + '°';
                                                                return this.value;
                                                                }
                                                                }
                                                        },
                                                        tooltip: {
                                                        crosshairs: true,
                                                                shared: true
                                                        },
                                                        plotOptions: {
                                                        spline: {
                                                        marker: {
                                                        radius: 4,
                                                                lineColor: '#666666',
                                                                lineWidth: 1
                                                        }
                                                        }
                                                        },
                                                        series: [{
                                                        name: 'Order Amount',
                                                                marker: {
                                                                symbol: 'square'
                                                                },
<?php
//echo $monthly_sales_amount;
//                                                                        $monthly_sales_amount = "7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6"
?>
                                                        data: [<?php echo $monthly_sales_amount; ?>]

                                                        },
//                                                                    {
//                                                                        name: 'London',
//                                                                        marker: {
//                                                                            symbol: 'diamond'
//                                                                        },
//
//                                                                    }
                                                        ]
                                                });
//                                                            ============ its for paid order chart ========
                                                Highcharts.chart('paid_order', {
                                                chart: {
                                                type: 'spline'
                                                },
                                                        title: {
                                                        text: 'Paid Order'
                                                        },
                                                        subtitle: {
//                                                                    text: 'Source: WorldClimate.com'
                                                        },
                                                        xAxis: {
                                                        categories: [<?php echo $monthly_sales_month; ?>]
//                                                                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                                        },
                                                        yAxis: {
                                                        title: {
                                                        text: 'Amount Range'
                                                        },
                                                                labels: {
                                                                formatter: function () {
//                                                                            return this.value + '°';
                                                                return this.value;
                                                                }
                                                                }
                                                        },
                                                        tooltip: {
                                                        crosshairs: true,
                                                                shared: true
                                                        },
                                                        plotOptions: {
                                                        spline: {
                                                        marker: {
                                                        radius: 4,
                                                                lineColor: '#666666',
                                                                lineWidth: 1
                                                        }
                                                        }
                                                        },
                                                        series: [{
                                                        name: 'Order Amount',
                                                                marker: {
                                                                symbol: 'square'
                                                                },
<?php
//echo $monthly_sales_amount;
//                                                                        $monthly_sales_amountss = "7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6"
?>
                                                        data: [<?php echo $paid_amount; ?>]

                                                        },
//                                                                    {
//                                                                        name: 'London',
//                                                                        marker: {
//                                                                            symbol: 'diamond'
//                                                                        },
//
//                                                                    }
                                                        ]
                                                });
                                                });
                                                //============== its for show_address_map ==========
                                                function show_address_map(id) {
                                                var address = $("#address_" + id).text();
                                                var location = $.trim(address)
//        alert(location);
                                                        $.post("<?php echo base_url(); ?>c-show-address-map/" + id, function (t) {
                                                        $("#customer_address_info").html(t);
                                                        $('#customer_address_modal_info').modal('show');
                                                        $(".modal-title").text(location);
                                                        });
                                                }
</script>