<style type="text/css">
    .right_side{
        float: right;
        font-weight: bold;
    }
    .left_side{
        float: left;
        font-weight: bold;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Comments List</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Comment List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="px-3">
                        <form action="<?php echo base_url('out-customer-comment'); ?>" method="post">
                            <div class="form-group row mt-3">
                                <!--<label for="comment" class="col-xs-3 col-form-label">Comments <span class="text-danger"> * </span></label>-->
                                <div class="col-xs-12">
                                    <textarea name="comment" class="form-control" placeholder="Write Your Comments" rows="7"  tabindex="1"></textarea>
                                </div>
                            </div>

                            <div class="form-group  text-right">
                                <input type="hidden" name="comment_from" value="<?php echo $customer_commet_results[0]->comment_to; ?>">
                                <input type="hidden" name="customer_to" value="<?php echo $customer_commet_results[0]->comment_from; ?>">
                                <button type="submit" class="btn btn-success w-md m-b-5" tabindex="2">Send</button>
                                <button type="reset" class="btn btn-primary w-md m-b-5"  tabindex="3">Reset</button>
                            </div>
                        </form>
                    </div>


                    <div class="card-body">
                        <?php // dd($get_customer); ?>
                        <div class="table-responsive" style="height: 200px; overflow-y: scroll;"">
                            <table class="datatable2 table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="10%">SL No.</th>
                                    <th width="40%">My Message</th>
                                    <th width="40%" class="text-right">BMS Window Decor</th>
                                    <th width="10%">Created Date</th>
                                    <!--<th width="15%">Action</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sl = 0;
                                foreach ($customer_commet_results as $single) {
                                    $sl++;
                                    ?>
                                    <tr>
                                        <td><?php echo $sl; ?></td>
                                        <td>
                                            <?php
                                             if ($single->status == 'OUT') {
                                                echo "<span class='left_side'>$single->comments</span>";
                                            }
//                                            echo $single->comments;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($single->status == 'IN') {
                                                echo "<span class='right_side'>$single->comments</span>";
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $single->created_at; ?></td>                                       
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
