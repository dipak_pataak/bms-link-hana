<table class="table table-bordered">
    <thead>
    <th>Sl</th>
    <th>Appointment Date</th>
    <th>Appointment Time</th>
    <th>Comments</th>
</thead>
<tbody>
    <?php
    $sl = 0;
    foreach ($old_comments as $comment) {
        $sl++;
        ?>
        <tr>
            <td><?php echo $sl; ?></td>
            <td><?php echo $comment->appointment_date; ?></td>
            <td><?php echo $comment->appointment_time; ?></td>
            <td><?php echo $comment->remarks; ?></td>
        </tr>
    <?php } ?>
</tbody>
<?php if (empty($old_comments)) { ?>
    <tfoot>
        <tr>
            <th colspan="4" class="text-danger text-center">Record not found!</th>
        </tr>
    </tfoot>
<?php } ?>
</table>