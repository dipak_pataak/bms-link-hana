<table class="datatable2 table table-bordered table-hover" id="result_search">
    <thead>
        <tr>
            <th width="4%">SL#</th>
            <th width="11%">Name</th>
            <th width="9%">Sidemark</th>
            <th width="14%">Phone</th>
            <th width="20%">Address</th>
            <th width="11%">Comment</th>
            <th width="10%">Status</th>
            <th width="15%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sl = 0;
        foreach ($get_customer as $customer) {
            $sl++;
            $pre = $this->db->select('remarks')->where('customer_id', $customer->customer_id)->order_by('appointment_id', 'DESC')->get('appointment_calendar')->row();
            ?>
            <tr>
                <td><?php echo $sl; ?></td>
                <td><?php echo $customer->first_name . " " . @$customer->last_name; ?></td>
                <td><?php echo $customer->side_mark; ?></td>
                <td>
                    <a href="tel:<?php echo $customer->phone; ?>" class="phone_email_link"><?php echo $customer->phone; ?></a>
                </td>
                <td>
                    <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address phone_email_link" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                        <?php echo $customer->address; ?><br />
                        <?php echo $customer->city; ?>, <?php echo $customer->state; ?>, <?php echo $customer->zip_code; ?>, <?php echo $customer->country_code; ?>
                    </a>
                </td>
                <td><?php echo @$pre->remarks; ?></td>
                <td>
                    <select class="form-control inner_table select2-single" data-placeholder="-- select one --" name="type" id="sta" onchange="show_customer_record(<?php echo $customer->customer_id; ?>, this.value);">
                        <option value=""></option>
                        <option value="enquiry" <?= ($customer->now_status == 'enquiry' ? 'selected' : ''); ?> >Enquiry</option>
                        <option value="scheduled" <?= ($customer->now_status == 'scheduled' ? 'selected' : ''); ?> >Scheduled</option>
                        <option value="re-schedule" <?= ($customer->now_status == 're-schedule' ? 'selected' : ''); ?>>Re-Scheduled</option>
                        <option value="cancelled" <?= ($customer->now_status == 'cancelled' ? 'selected' : ''); ?>>Cancelled</option>
                        <option value="active" <?= ($customer->now_status == 'active' ? 'selected' : ''); ?>>Active</option>
                        <option value="inactive" <?= ($customer->now_status == 'inactive' ? 'selected' : ''); ?>>Inactive</option>
                    </select>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success btn-xs default" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                        <i class="simple-icon-eye"></i>
                    </a>
                    <a href="<?php echo base_url(); ?>customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="simple-icon-pencil"></i></a>
                    <a href="<?php echo base_url(); ?>c-customer-delete/<?php echo $customer->customer_id; ?>" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="glyph-icon simple-icon-trash"></i></a>

                </td>
            </tr>
        <?php } ?>
    </tbody>
    <?php if (empty($get_customer)) { ?>
        <tfoot>
            <tr>
                <th class="text-center text-danger" colspan="8">Record not found!</th>
            </tr>
        </tfoot>
    <?php } ?>
</table>