<style type="text/css">
    .canvasjs-chart-credit {
        display: none;
    }
    .highcharts-credits, .highcharts-button-symbol{
        display: none;
    }
    .progressbar-text{
        font-size: 20px !important;
    }
</style>
<?php // echo $this->session->user_type; echo "<br>"; echo $this->session->is_admin; die(); ?>


<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Dashboard</h1>
                <div class="separator mb-5"></div>
            </div>
            <div class="col-12 mb-2" > 
                <div id="chart_div1"></div>
            </div>
           
        </div>

        <div class="row sortable">
            <div class="col-xl-3 col-lg-6 mb-4">
                <div class="card">
                    <div class="card-header p-0 position-relative">
                        <div class="position-absolute handle card-icon">
                            <i class="simple-icon-shuffle"></i>
                        </div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h6 class="mb-0">Pending Invoice</h6>
                        <div role="progressbar" data-color="#922c88"  class='position-relative' >   <?php echo @$pending_invoice; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
                <div class="card">
                    <div class="card-header p-0 position-relative">
                        <div class="position-absolute handle card-icon">
                            <i class="simple-icon-shuffle"></i>
                        </div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h6 class="mb-0">Pending Orders</h6>
                        <div role="progressbar" class=" position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="1000" aria-valuenow="" data-show-percent="false">
                            <?php echo @$pending_order; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
                <div class="card">
                    <div class="card-header p-0 position-relative">
                        <div class="position-absolute handle card-icon">
                            <i class="simple-icon-shuffle"></i>
                        </div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h6 class="mb-0">New Orders</h6>
                        <div role="progressbar" class="position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="1000" aria-valuenow="" data-show-percent="false"><?php echo $total_new_order_count; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
                <div class="card">
                    <div class="card-header p-0 position-relative">
                        <div class="position-absolute handle card-icon">
                            <i class="simple-icon-shuffle"></i>
                        </div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h6 class="mb-0">Synced Order</h6>
                        <div role="progressbar" class=" position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="1000" aria-valuenow="" data-show-percent="false"><?=$payment_done?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!--            <div class="col-md-6 col-sm-12 mb-4">
                            <div id="yearMonthWeekDay" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                        </div>-->
            <!--                    <div class="col-md-6 col-sm-12 mb-4">
                                    <div class="card dashboard-filled-line-chart">
                                        <div class="card-body">
                                            <div class="float-left float-none-xs">
                                                <div class="d-inline-block">
                                                    <h5 class="d-inline">Website Visits</h5>
                                                    <span class="text-muted text-small d-block">Unique Visitors</span>
                                                </div>
                                            </div>
                                            <div class="btn-group float-right float-none-xs mt-2">
                                                <button class="btn btn-outline-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    This Week
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">Last Week</a>
                                                    <a class="dropdown-item" href="#">This Month</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chart card-body pt-0">
                                            <canvas id="visitChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-sm-12 mb-4">
                                    <div class="card dashboard-filled-line-chart">
                                        <div class="card-body">
                                            <div class="float-left float-none-xs">
                                                <div class="d-inline-block">
                                                    <h5 class="d-inline">Conversion Rates</h5>
                                                    <span class="text-muted text-small d-block">Per Session</span>
                                                </div>
                                            </div>
                                            <div class="btn-group float-right mt-2 float-none-xs">
                                                <button class="btn btn-outline-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    This Week
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">Last Week</a>
                                                    <a class="dropdown-item" href="#">This Month</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chart card-body pt-0">
                                            <canvas id="conversionChart"></canvas>
                                        </div>
                                    </div>
                                </div>-->
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-12 col-xl-4">
                <div class="row">
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Printer mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white"></p>
                                        <p class="text-small text-white">Number of Customer</p>
                                    </div>
                                </div>

                                <div>
                                    <div role="progressbar" class="  position-relative" data-color="white" data-trail-color="rgba(255,255,255,0.2)" >
                                    <h3 class="text-white"><?php echo $total_customer_count; ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Male mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white"></p>
                                        <p class="text-small text-white">Pending Appointment</p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class=" position-relative" data-color="white" data-trail-color="rgba(255,255,255,0.2)" >
                                        <h3 class="text-white"><?php echo $pending_appointment[0]->appointment_count; ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $date = date('Y-m-d');
                    $user_id = $this->session->userdata('user_id');
                    $this->db->select("c_notification_tbl.*,CONCAT(user_info.first_name, '.', user_info.last_name) as fullname, user_info.user_image");
                    $this->db->join('user_info', 'user_info.id=c_notification_tbl.created_by', 'left');
                    $this->db->where('c_notification_tbl.date', $date);
                    $this->db->where('c_notification_tbl.created_by', $user_id);
                    $this->db->order_by('id','DESC');
                    $result = $this->db->get('c_notification_tbl')->result();

                    ?>
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <a href="#" class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Bell mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white"><?php echo count($result); ?> Alerts</p>
                                        <p class="text-small text-white">Waiting for notice</p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class=" position-relative" data-color="white" data-trail-color="rgba(255,255,255,0.2)">
                                    <h3 class="text-white"></h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                        
                    <?php
                    $this->db->where('user_id', $user_id);
                    $company_profile_data = $this->db->get('company_profile')->row_array();
                    $currency = isset($company_profile_data['currency'])?$company_profile_data['currency']:'';
                    ?>
                    <div class="col-xl-12 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Money mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white"></p>
                                        <p class="text-small text-white">Total Commission</p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class=" position-relative" data-color="white" data-trail-color="rgba(255,255,255,0.2)" >
                                        <h3 class="text-white"><?php echo $currency.$total_commission; ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-12 col-xl-8">
                <div class="card">
                    <div class="position-absolute card-top-buttons">
                        <button class="btn btn-header-light icon-button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="simple-icon-refresh"></i>
                        </button>

                        <div class="dropdown-menu dropdown-menu-right mt-3">
                            <a class="dropdown-item" href="#">Sales</a>
                            <a class="dropdown-item" href="#">Orders</a>
                            <a class="dropdown-item" href="#">Refunds</a>
                        </div>

                    </div>

                    <div class="card-body">
                        <div id="chart_div" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
            <!--            <div class="col-xl-4 col-lg-6 mb-4">
                            <div class="row">
                                <div class="col-6 mb-4">
                                    <div class="card dashboard-small-chart">
                                        <div class="card-body">
                                            <p class="lead color-theme-1 mb-1 value"></p>
                                            <p class="mb-0 label text-small"></p>
                                            <div class="chart">
                                                <canvas id="smallChart1"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 mb-4">
                                    <div class="card dashboard-small-chart">
                                        <div class="card-body">
                                            <p class="lead color-theme-1 mb-1 value"></p>
                                            <p class="mb-0 label text-small"></p>
                                            <div class="chart">
                                                <canvas id="smallChart2"></canvas>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                                <div class="col-6 mb-4">
                                    <div class="card dashboard-small-chart">
                                        <div class="card-body">
                                            <p class="lead color-theme-1 mb-1 value"></p>
                                            <p class="mb-0 label text-small"></p>
                                            <div class="chart">
                                                <canvas id="smallChart3"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 mb-4">
                                    <div class="card dashboard-small-chart">
                                        <div class="card-body">
                                            <p class="lead color-theme-1 mb-1 value"></p>
                                            <p class="mb-0 label text-small"></p>
                                            <div class="chart">
                                                <canvas id="smallChart4"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <button class="top-right" data-toggle="modal" data-target="#appointmentform"><i class="simple-icon-size-fullscreen"></i></button>
                                    <div class="form-group">
                                        <div class="date-inline"></div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
        </div>
    </div>



</main>


    <?php
        $company_profile = $this->db->select('*')
                            ->from('company_profile')
                            ->where('user_id', $this->session->userdata('user_id'))
                            ->get()->result();


    ?>


        <?php
        if($company_profile){
            if($company_profile[0]->setting_status==0){
        
        ?>

                <script type="text/javascript">

                    // $(window).on('load',function(){
                    //     alert('sdfsd');
                    //     $('#myModalt').modal('show');
                    // });

                    $(document).ready(function () {
                        
                        $('#myModalt').modal({
                            backdrop: 'static',
                            keyboard: false
                        })

                    });

                </script>


        <?php } 
         }
        ?>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>




<div id="myModalt" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>Welcome! to BMSLink system. Please setup your company profile first for the system to run smoothly. </p>
                <a href="<?=base_url('company-profile')?>" class="btn btn-success">Go</a>
            </div>
        </div>
    </div>
</div>







<script>
    window.onload = function () {


       //    ========== First chart start ===========
        Highcharts.chart('chart_div1', {
            
            chart: {
                zoomType: 'xy'
            },
            
            title: {
                text: ' Monthly Sales Amount & Order',
                align: 'left'
            },

            xAxis: [{
                    categories: [<?php echo $monthly_sales_month; ?>],
//                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
//                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    crosshair: true
                }],
            yAxis: [{// Primary yAxis
                    labels: {
                        format: '${value}',
                        style: {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    title: {
                        text: 'Sales Amount',
                        style: {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    opposite: true

                }, {// Secondary yAxis
                    gridLineWidth: 0,
                    title: {
                        text: 'Sales Order',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }

                }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
            },
            series: [{
                    name: 'Sales Order',
                    type: 'column',
                    yAxis: 1,
                    data: [<?php echo $monthlysaleorders; ?>],
                    tooltip: {
//                        valuePrefix: '$'
                    }

                }, {
                    name: 'Sales Amount',
                    type: 'spline',
                    data: [<?php echo $monthly_sales_amount; ?>],
                    tooltip: {
//                        valueSuffix: ' °C'
                        valuePrefix: ' $'
                    }
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                floating: false,
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 0
                            }
                        }
                    }]
            }
        });
//    ========== First chart Close ===========
//    ========== Second chart start ===========
        Highcharts.chart('chart_div', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Total sales in year, month, week, day'
            },
            subtitle: {
//                text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Current Sales Report'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
//                        format: '{point.y:.1f}%'
                        format: '{point.y:.1f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },

            series: [
                {
                    name: "Sales",
                    colorByPoint: true,
                    data: [
                        {
                            name: "Year",
                            y: <?php echo $year_wise_sale; ?>,
                            drilldown: "Year"
                        },
                        {
                            name: "Month",
                            y: <?php echo $month_wise_sale; ?>,
                            drilldown: "Month"
                        },
                        {
                            name: "Week",
                            y: <?php echo $week_wise_sale; ?>,
                            drilldown: "Week"
                        },
                        {
                            name: "Day",
                            y: <?php echo $day_wise_sale; ?>,
                            drilldown: "Day"
                        },
                    ]
                }
            ],
        });        
//    ========== Second chart Close ===========
    }
</script>
<script src="<?php echo base_url(); ?>assets/highcharts/highcharts.js"></script>