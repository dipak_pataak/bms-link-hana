<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
            top: 713.391px !important;
            left: 383.5px;
            z-index: 10;
            display: block;
    }
</style>
<main>
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Quotation</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New quotation</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <?php
            $message = $this->session->flashdata('message');
            
            if ($message != '') {
                echo $message;
            }
            
        ?>

        <div class="row">

            <div class="col-lg-12 mb-4">
                <div class="card mb-4">
                    <?=form_open('c_level/quotation_controller/add_to_cart',array('id' => 'AddToCart' ))?>
                    <div class="card-body">
                        <h5 class="mb-4">Add new item</h5>
                        <div class="separator mb-5"></div>


                        <table class="datatable2 table table-bordered table-hover">
                           
                            <div class="row">

                                <div class="form-group col-md-12">

                                    <div class="m-0">
                                        <label for="room_name" class="col-form-label col-sm-3">Room<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <select class="form-control select2" name="room_name" id="room_name" onchange="reload()" required="" data-placeholder='-- select one --'>
                                                <option value="">--Select room--</option>
                                                <?php
                                                    foreach ($rooms as $room) {
                                                        echo "<option value='$room->room_name'>$room->room_name</option>";
                                                    }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                
                                    <div class="m-0">
                                        <label for="room_name" class="col-form-label col-sm-3">Category<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <select class="form-control select2" name="category_id" id="category_id" onchange="reload()" required="" data-placeholder='-- select one --'>
                                                <option value="">--Select category-</option>
                                                <?php
                                                    foreach ($categories as $category) {
                                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="category_name" id="category_name">
                               
                                    <div class="m-0">
                                        <label for="" class="col-form-label col-sm-3"></label>
                                        <div class="col-sm-5">
                                            <button type="button" class="btn btn-xs btn-success" onclick="addInputField()">Add Item</button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Width</th>
                                    <th>Height</th>
                                    <th>Model</th>
                                    <th>Pattern</th>
                                    <th>Price</th>     
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody class="add_input">

                            </tbody>

                            <input type="hidden" name="room_sub_price" id="room_sub_price" value="0" class="room_sub_price" >
                            <input type="hidden" name="upcharge" id="upcharge" value="0" class="upcharge" >

                            <tfoot id="footer">
                            </tfoot> 

                        </table>

                        <div class="form-group col-md-12 mb-0">
                            <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn" >Add product to cart</button>
                        </div>
                        <br/>
                    </div>
                    <?=form_close()?>
                </div>
            </div>

   


        <?=form_open('c_level/quotation_controller/save_quotation',array('id'=>'','class'=>'col-lg-12 mb-4'))?>

            <div class="">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Customer Info</h5>
                        <div class="separator mb-5"></div>
                
                        <div class="form-row">
                          
                            <div class="form-group col-md-12">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-2">Date</label>
                                    <div class="col-sm-3">
                                        <p>
                                            <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">

                                <div class="row m-0">
                                    <label for="customer_id" class="col-form-label col-sm-2">Select Customer</label>

                                    <div class="col-sm-3">
                                        <!--<input type="text" class="form-control">-->
                                        <select class="form-control select2-single" name="customer_id" id="customer_id" required data-placeholder="-- select one --">
                                            <option value=""></option>
                                            <?php
                                            foreach ($get_customer as $customer) {
                                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                                            }
                                            ?>
                                        </select>

                                        <input type="hidden" name="customertype" id="customertype" value="">
                                    </div>

                                </div>

                            </div>

                        </div>


                        <h5>Order Details</h5>

                        <div class="separator mb-3"></div>

                        <div id="cartItems">

                            <table class="datatable2 table table-bordered table-hover">

                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>Room Name</th>

                                        <th>Category</th>
                                        <th>Room Price</th>
                                        <th>Upcharge</th>

                                        <th>List Amount</th>
                                        <th>Discount Amount (%) </th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php 
                                        $i = 1;?>
                                        <?php foreach ($this->cart->contents() as $items): ?>
                                        <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

                                        <input type="hidden" name="category_id[]" id="category_id_<?=$i?>" class="category_id" value='<?=$items['category_id']?>'>
                                        <input type="hidden" name="room_name[]" id="room_name_<?=$i?>" class="room_name" value='<?=$items['name']?>'>
                                        <input type="hidden" name="product_data[]" id="product_data_<?=$i?>" class="product_data" value='<?=$items['product_data']?>'>
                                        <input type="hidden" name="upchargeData[]" id="upchargeData_<?=$i?>" class="upchargeData" value='<?=$items['upchargeData']?>'>

                                        <tr>
                                            <td><?=$i?></td>
                                            <td>
                                                <?=$items['name']?>
                                                <input type="hidden" name="product_id[]" id="product_id_<?=$i?>" class="product_id" value="<?=$items['product_id']?>">
                                            </td>

                                            <td>
                                                <?=$items['category_name']?>
                                                <input type="hidden" name="qty[]"  onchange="customerWiseComission()" value="<?=$items['qty']?>" id="qty_<?=$i;?>" min="1" class="qty_input"> 
                                            </td>

                                            <td><input type="number" name="room_price[]" value="<?=$items['room_price']?>" readonly="" id="room_price_<?=$i?>" class="form-control text-right"></td>
                                            <td><input type="number" name="upcharge[]" value="<?=$items['upcharge']?>" readonly="" id="upcharge_<?=$i?>" class="form-control text-right"></td>
                                            <td><input type="number" name="list_price[]" value="<?=$items['price']?>" id="list_price_<?=$i;?>" readonly="" class="form-control text-right"></td>
                                            <td><input type="number" name="discount[]" value="0" id="discount_<?=$i?>" onchange="setDiscount(<?=$i;?>)" onclick="setDiscount(<?=$i;?>)" class="form-control text-right"></td>
                                            <td><input type="number" name="utprice[]" value="<?=$items['price']?>" id="utprice_<?=$i;?>" class="form-control utprice text-right" readonly="" ></td>
                                           
                                            <td>
                                                <a href="javascript:void(0)" onclick="deleteCartItem('<?=$items['rowid']?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="glyph-icon simple-icon-trash"></i></a>
                                                <!-- <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></button> -->
                                            </td>

                                        </tr>

                                        <?php $i++; ?>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                       
                    </div>
                </div>
            </div>


            <div class="col-lg-5 offset-lg-7">
                
                <div class="card mb-4">
                    
                    <div class="card-body">
                    
                        <table class="table table-bordered mb-4">

                            <tr>
                                <td>Sub Total (<?=@$company_profile[0]->currency;?>)</td>
                                <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                            </tr>

                            <tr>
                                <td>Sales Tax (%)</td>
                                <td><input type="text" name="tax" onchange="calculetsPrice()" onclick="calculetsPrice()" id="tax" value="0" class="form-control text-right" readonly=""></td>
                            </tr>

                            <tr>
                                <td>Grand Total (<?=@$company_profile[0]->currency;?>)</td>
                                <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly="" required></td>
                            </tr>

                        </table>

                    </div>

                </div>

            </div>


            <div class="col-lg-4 offset-lg-8 text-right">
                <button type="submit" class="btn btn-danger" id="clearCart" >Clear data</button>
                <button type="submit" class="btn btn-success" id="gq" >Submit</button>
            </div>

            <?php echo form_close();?>

        </div>
    </div>
</main>

<?php  $this->load->view($qtjs)?>

