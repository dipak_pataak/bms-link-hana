<?php $currency = $company_profile[0]->currency;?>
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="mb-3">
                        <h1>Manage Quotation/Order</h1>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Quotation</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Manage</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="separator mb-5"></div>
                </div>
            </div>



            <div class="row">
                
                <div class="col-xl-12 mb-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            
                            <div class="table-responsive">

                                    <table class="table table-bordered table-hover text-center">
                                        
                                        <thead>
                                            <tr>
                                                <th>Quote No</th>
                                                <th>Customer Name </th>
                                                <th>Quotation date</th>
                                                <th>Price</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php 
                                            if(!empty($quotation)){
                                                foreach ($quotation as $key => $value) {
                                                    
                                             ?>

                                                <tr>
                                                    <td><?=$value->quote_id;?></td>
                                                    <td><?=$value->customer_name;?></td>
                                                    <td><?=(@$value->created_date);?></td>
                                                    <td><?=$currency;?><?=$value->grand_total?></td>

                                                    <td>
                                                        <a href="<?=base_url('c_level/quotation_controller/quotation_receipt/')?><?=$value->quote_id;?>" class="btn btn-success btn-xs default"  title="View">
                                                            <i class="simple-icon-eye"></i>
                                                        </a>
                                                        
                                                        <a href="<?=base_url('c_level/quotation_controller/delete_quotation/').$value->quote_id?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>
                                                    </td>

                                                    
                                                </tr>

                                            <?php  
                                                } 
                                            }else{
                                            ?>

                                            <div class="alert alert-danger"> There have no order found..</div>
                                        <?php } ?>
                                           
                                        </tbody>
                                    </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>


