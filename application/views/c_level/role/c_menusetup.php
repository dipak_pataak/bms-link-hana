<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Employee Role</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Menu Setups</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <!--                    <div class="card-body">
                                            <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                                            </div>
                                            <form action="<?php echo base_url(); ?>menusetup-save" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="panel">
                                                    <div class="panel-body">
                                                        <div class="form-group row">
                                                            <label for="menu_name" class="col-sm-3 text-right">Menu Name</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="menu_name" id="menu_name" placeholder="English Name" required>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="korean_name" id="korean_name" placeholder="My Language" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="url" class="col-sm-3 text-right">Menu URL</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="url" id="url" placeholder="URL">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="module" class="col-sm-3 text-right">Module</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="module" id="module" placeholder="Enter Module">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="order" class="col-sm-3  text-right">Order</label>
                                                            <div class="col-sm-8">
                                                                <select  class="form-control select2-single" name="order" id="order" data-placeholder="-- select one --">
                                                                    <option value=""></option>
                    <?php
                    for ($i = 1; $i < 51; $i++) {
                        echo "<option value='$i'>$i</option>";
                    }
                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="menu_type" class="col-sm-3  text-right">Menu Type</label>
                                                            <div class="col-sm-8">
                                                                <select  class="form-control select2-single" name="menu_type" id="menu_type" onchange="menu_type_wise_parent_menu(this.value);"  data-placeholder="-- select one --" tabindex="6">
                                                                    <option value=""></option>
                                                                    <option value="1">Left Menu</option>
                                                                    <option value="2">System Menu</option>
                                                                    <option value="3">Top Menu</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="parent_menu" class="col-sm-3  text-right">Parent Menu</label>
                                                            <div class="col-sm-8">
                                                                <select  class="form-control select2-single" name="parent_menu" id="parent_menu" data-placeholder="-- select one --" tabindex="7">
                                                                    <option value=""></option>
                    <?php
//                                                foreach ($parent_menu as $parent) {
//                                                    echo "<option value='$parent->id'>" . ucwords(str_replace('_', ' ', $parent->menu_title)) . "</option>'";
//                                                }
                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="icon" class="col-sm-3  text-right">Icon</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="icon" id="icon" placeholder="Icon Class" tabindex="8">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                                                            <div class="col-sm-6">
                                                                <input type="submit" id="" class="btn btn-primary btn-sm" name="add-user" value="Save" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>-->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body" id="results_menu">
                        <div class="col-sm-12 text-right">
                            <div class="form-group row">
                                <label for="keyword" class="col-sm-2 col-form-label offset-7 text-right"></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="menukeyup_search()" placeholder="Search..." tabindex="">
                                </div>
                                <div class="col-sm-1 dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)" onClick="ExportMethod('<?php echo base_url(); ?>c-menu-export-csv')" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="javascript:void(0)"  data-toggle="modal" data-target="#importMenu" class="dropdown-item">Import from CSV</a></li>
                                    </ul>
                                </div>
                            </div>          
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>English</th>
                                    <th>My Language</th>
                                    <th>URL</th>
                                    <th>Module</th>
                                    <th>Parent Menu</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($menusetuplist)) {
                                    $sl = 0 + $pagenum;
                                    foreach ($menusetuplist as $key => $value) {
                                        $parent_menu = $this->db->select('*')->where('menu_id', $value->parent_menu)->where('level_id', $level_id)->get('c_menusetup_tbl')->row();
//                                        echo $this->db->last_query();
                                        $sl++;
                                        if ($value->menu_id != '59') {
                                            ?>
                                            <tr>
                                                <td><?php echo $sl; ?></td>
                                                <td><?php echo str_replace("_", " ", ucfirst($value->menu_title)); ?></td>
                                                <td><?php echo str_replace("_", " ", ucfirst($value->korean_name)); ?></td>
                                                <td><?php echo $value->page_url; ?></td>
                                                <td><?php echo $value->module ?></td>
                                                <td><?php echo str_replace("_", " ", ucfirst(@$parent_menu->menu_title)); ?></td>
                                                <td class="text-center">
                                                    <?php
                                                    $status = $value->status;
                                                    if ($status == 1) {
                                                        ?>
                                                        <a href="<?php echo base_url(); ?>c-menusetup-inactive/<?php echo $value->id; ?>" title="Inactive" onclick="return confirm('Are you sure inactive it ?')" class="btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                        <?php
                                                    }
                                                    if ($status == 0) {
                                                        ?>
                                                        <a href="<?php echo base_url(); ?>c-menusetup-active/<?php echo $value->id; ?>" title="Active" onclick="return confirm('Are you sure active it ?')" class="btn btn-xs btn-info"><i class="fa fa-check-circle"></i></a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url(); ?>c-menusetup-edit/<?php echo $value->id; ?>" title="" class="btn btn-info btn-xs simple-icon-note"></a>
                                                    <!--<a href="<?php echo base_url(); ?>c-menusetup-delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs simple-icon-trash"></a>-->
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if (empty($menusetuplist)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="text-center text-danger">Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table> 
                        <?php echo $links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="menu_exp" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> Export Menu</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="expUrl" action="#" method="post">

                        <div class="form-group row">
                            <label  class="col-xs-2 control-label">Start from *</label>
                            <div class="col-xs-6">
                                <input type="number" min="1" name="ofset" id="ofset" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-xs-2 control-label">Limit *</label>
                            <div class="col-xs-6">
                                <input type="number" min="1" name="limit" id="limit" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-xs-2 control-label"></label>
                            <div class="col-xs-6">
                                <button class="btn-sm btn-success" id="closeModal"> Export</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="importMenu" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Menu Bulk Update</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <span class="text-primary">The first line in exported csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('c-import-menu-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
               <!--  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    function ExportMethod(url){
        $("#expUrl").attr("action", url);
        $("#menu_exp").modal('show');
    }
    function menukeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>clevel-menu-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_menu").html(r);
            }
        });
    }

//    ========== its for menu_type_wise_parent_menu ===========
    function menu_type_wise_parent_menu(t) {
        $.ajax({
            url: "<?php echo base_url('c_level/Menusetup_controller/c_menu_type_wise_parent_menu'); ?>",
            type: 'post',
            data: {type_id: t},
            success: function (r) {
                r = JSON.parse(r);
//                    alert(r);
                $("#parent_menu").empty();
                $("#parent_menu").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $('#parent_menu').append($('<option>').text(typeval.menu_title).attr('value', typeval.id));
                });
            }
        });
    }
</script>
