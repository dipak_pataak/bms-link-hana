<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Assign user role update</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">User Roles</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="">
                <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
                ?>
            </div>
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <?php echo form_open_multipart("assign-user-role-update/" . $edit_user_access_role->role_acc_id, array('class' => 'form-horizontal')) ?>
                        <div class="form-group row">
                            <label for="user_id" class="col-sm-3 col-form-label control-label text-right">User Name</label>
                            <div class="col-sm-9">
                                <?php
//                echo '<pre>';
//                print_r($edit_user_access_role);
//                print_r($assign_role);
//                echo '</pre>';
                                ?>
                                <select class="form-control select2" name="user_id" id="user_id">
                                    <option value="">-- select one --</option>
                                    <?php
                                    foreach ($user_list as $user) {
                                        if ($edit_user_access_role->user_id == $user->id) {
                                            echo '<option selected value="' . $user->id . '">' . $user->first_name . " " . $user->last_name . " ->( " . $user->email . " ) " . '</option>';
                                        } else {
                                            echo '<option value="' . $user->id . '">' . $user->first_name . " " . $user->last_name . " ->( " . $user->email . " ) " . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        foreach ($assign_role as $role) {
                            $role_id[] = $role->role_id;
                        }
                        ?>
                        <div class="form-group row">
                            <label for="role_id" class="col-sm-3 col-form-label control-label text-right">Role</label>
                            <div class="col-sm-9">
                                <select class="selectpicker form-control role_id" id="role_id" name="role_id[]" multiple data-live-search="true" required>
                                   <?php foreach ($role_list as $val) { ?>
                                        <option value="<?= $val->id ?>" <?php
                                        if (in_array($val->id, $role_id)) {
                                            echo 'selected';
                                        }
                                        ?>>
                                                    <?= ucwords($val->role_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <?php foreach ($role_list as $val) { ?>
                                    <!--                                    <label class="radio-inline">
                                                                            <input type="checkbox" id="role_id" name="role_id[]" value="<?php echo $val->id; ?>"
                                    <?php
                                    if (in_array($val->id, $role_id)) {
                                        echo 'checked';
                                    }
                                    ?> >
                                    <?php echo $val->role_name; ?>
                                                                        </label> -->
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group text-right" style="padding-right: 20px;">
                            <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-sm text-white">Back</a>
                            <button type="submit" class="btn btn-success w-md m-b-5 btn-sm">Update</button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>