<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>User Roles</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Roles List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <table id="" class="table table-striped table-bordered language_list" style="width:100%">
                            <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($role_list)) {
                                    $sl = 0;
                                    foreach ($role_list as $key => $value) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->role_name; ?></td>
                                            <td><?php echo $value->description; ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url(); ?>role-edit/<?php echo $value->id; ?>" title=""class="btn btn-info btn-xs"><i class="simple-icon-pencil"></i></a>
                                                <a href="<?php echo base_url(); ?>role-delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs"><i class="glyph-icon simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if (empty($role_list)) { ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-center text-danger" colspan="6"> Record not found! </th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
