<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Assign User Role</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">User Role</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>assign-user-role-save" method="post" class="form-horizontal">
                            <input type="hidden" name="id" value="">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <label for="user" class="col-sm-3 col-form-label text-right">User <i class="text-danger">*</i></label>
                                        <div class="col-sm-9">
                                            <select name="user_id" class="form-control select2-single" id="user_id" onchange="userRole(this.value)" data-placeholder="Select Option">
                                                <option value=""></option>
                                                <?php
                                                foreach ($get_users as $user) {
                                                    echo '<option value="' . $user->user_id . '">' . $user->first_name . " " . @$user->last_name . " ->( " . $user->email . " ) " . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label for="role" class="col-sm-3 col-form-label text-right">Role <i class="text-danger">*</i></label>
                                        <div class="col-sm-9">
                                            <select class="selectpicker form-control role_id" id="role_id" name="role_id[]" multiple data-live-search="true" required>
                                                <?php foreach ($role_list as $val) { ?>
                                                    <option value="<?php echo $val->id; ?>">
                                                        <?php echo $val->role_name; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <?php foreach ($role_list as $val) { ?>
                                                <!--                                                <label class="radio-inline">
                                                                                                    <input type="checkbox" id="role_id" name="role_id[]" value="<?php echo $val->id; ?>"> <?php echo $val->role_name; ?>
                                                                                                </label> -->
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="border: 1px">
                                    <!--<h3>Exsisting Role</h3>-->
                                    <div id="existrole">
                                        <ul>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="reset" class="btn btn-primary btn-sm w-md m-b-5">Reset</button>
                                <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
    function userRole(t) {
        $.ajax({
            url: "<?php echo base_url(); ?>c-level-check-user-role",
            type: 'post',
            data: {user_id: t},
            success: function (r) {
                r = JSON.parse(r);
                $("#existrole ul").empty();
                $.each(r, function (ar, typeval) {
                    if (typeval.role_name == 'Not Found') {
                        $("#existrole ul").html("Not Found!");
                        $("#exitrole ul").css({'color': 'red'});
                    } else {
                        $("#existrole ul").append('<li>' + typeval.role_name + '</li>');
                    }
                });
            }

        });
    }
</script>