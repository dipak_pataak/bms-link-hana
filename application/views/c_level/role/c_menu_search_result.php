<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>SL No</th>
            <th>English Name</th>
            <th>My Language</th>
            <th>URL</th>
            <th>Module</th>
            <th>Parent Menu</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($get_menu_search_result)) {
            $sl = 0;
            foreach ($get_menu_search_result as $key => $value) {
                $parent_menu = $this->db->select('*')->where('menu_id', $value->parent_menu)->where('level_id', $level_id)->get('c_menusetup_tbl')->row();
                $sl++;
                if ($value->menu_id != '59') {
                    ?>
                    <tr>
                        <td><?php echo $sl; ?></td>
                        <td><?php echo str_replace("_", " ", ucfirst($value->menu_title)); ?></td>
                        <td><?php echo str_replace("_", " ", ucfirst($value->korean_name)); ?></td>
                        <td><?php echo $value->page_url; ?></td>
                        <td><?php echo $value->module ?></td>
                        <td><?php echo str_replace("_", " ", ucfirst(@$parent_menu->menu_title)); ?></td>
                        <td class="text-center">
                            <?php
                            $status = $value->status;
                            if ($status == 1) {
                                ?>
                                <a href="<?php echo base_url(); ?>c-menusetup-inactive/<?php echo $value->id; ?>" data-toggle='tooltip' data-placement='top' data-original-title='' onclick="return confirm('Are you sure inactive it ?')" class="btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i></a>
                                <?php
                            }
                            if ($status == 0) {
                                ?>
                                <a href="<?php echo base_url(); ?>c-menusetup-active/<?php echo $value->id; ?>" data-toggle='tooltip' data-placement='top' data-original-title='' onclick="return confirm('Are you sure active it ?')" class="btn btn-xs btn-info"><i class="fa fa-check-circle"></i></a>
                            <?php } ?>
                            <a href="<?php echo base_url(); ?>c-menusetup-edit/<?php echo $value->id; ?>" title="" class="btn btn-info btn-xs simple-icon-note"></a>
                            <!--<a href="<?php echo base_url(); ?>c-menusetup-delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs simple-icon-trash"></a>-->
                        </td>
                    </tr>
                    <?php
                }
            }
        }
        if (empty($get_menu_search_result)) {
            ?>
        <tfoot>
            <tr>
                <th colspan="7" class="text-center text-danger">Record not found!</th>
            </tr>
        </tfoot>
    <?php } ?>
</tbody>
</table> 