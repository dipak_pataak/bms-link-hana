<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>User Access Role</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>c-level-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Access Role</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>User Name</th>
                                    <th>Role Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                // echo "<pre>";                            print_r($user_access_role);
                                if (!empty($user_access_role)) {
                                    $sl = 0;
                                    foreach ($user_access_role as $key => $value) {
                                        $sql = "SELECT a.role_id, a.user_id, b.role_name FROM user_access_tbl a 
                                        JOIN role_tbl b ON b.id = a.role_id 
                                    WHERE a.user_id = '$value->user_id'";
//                            echo $sql;
                                        $query = $this->db->query($sql)->result();
//                            echo "<pre>";   print_r($query);
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->first_name . " " . @$value->last_name; ?></td>
                                            <td>
                                                <ul>
                                                    <?php
                                                    foreach ($query as $role) {
                                                        echo "<li style='list-style: none;'>" . $role->role_name . "</li>";
                                                    }
                                                    ?>
                                                </ul>
                                            </td>
                                            <td class="text-center">
                                                <!--<a href="#" class="btn btn-warning btn-xs">Inactive</a>-->
                                                <a href="<?php echo base_url(); ?>edit-user-access-role/<?php echo $value->role_acc_id; ?>" title="Edit"><i class="btn btn-xs btn-info simple-icon-note"></i></a>
                                                <a href="<?php echo base_url(); ?>delete-user-access-role/<?php echo $value->role_acc_id; ?>" title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="btn btn-xs btn-danger simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
<!--                                <tr class="odd gradeX">
            <td>1</td>
            <td>Super Admin</td>
            <td>Active</td>
            <td class="center">
                <a href="#" class="btn btn-warning btn-xs">Inactive</a>
                <a href="#" class="btn btn-danger btn-xs"><i class="simple-icon-trash" aria-hidden="true"></i></a>
                <a href="#" class="btn btn-info btn-xs"><i class="simple-icon-note" aria-hidden="true"></i></a>
            </td>
        </tr>-->
                            </tbody>
                            <?php if (empty($user_access_role)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="4" class="text-center text-danger">Record not found!</th>
                                    </tr>
                                <?php } ?>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
