<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>BMS Link</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/iconsmind/style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/font/simple-line-icons/css/simple-line-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/fullcalendar.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/datatables.responsive.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/select2.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/select2-bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/perfect-scrollbar.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/owl.carousel.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-stars.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/nouislider.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/vendor/bootstrap-datepicker3.min.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/toster/toastr.css" media="screen">
        <!--============ its for multiselects ============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap-select.css" />
        <!-- timepicker -->
        <link href="<?php echo base_url() ?>assets/c_level/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/main.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/css/custom_style.css" />

        <script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>

        <script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

        <script type="text/javascript">
            var mybase_url = '<?php echo base_url(); ?>';
        </script>

        <style type="text/css">
            .pac-container:after{
                content:none !important;
            }

            .skiptranslate{
                display: none;

            }
            body{
                top:0px !important;
            }
            .top_popup{
                top: 40px !important;
            }
            .gridtop_popup{
                top: 49px !important;
            }

        </style>


    </head>

    <body id="app-container" class="menu-default show-spinner">
        <?php
        //  echo $this->session->userdata('user_type');
        ?>
        <nav class="navbar fixed-top">

            <a href="#" class="menu-button d-none d-md-block">
                <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1" />
                <rect x="0.48" y="7.5" width="7" height="1" />
                <rect x="0.48" y="15.5" width="7" height="1" />
                </svg>
                <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1" />
                <rect x="1.56" y="7.5" width="16" height="1" />
                <rect x="1.56" y="15.5" width="16" height="1" />
                </svg>
            </a>

            <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1" />
                <rect x="0.5" y="7.5" width="25" height="1" />
                <rect x="0.5" y="15.5" width="25" height="1" />
                </svg>
            </a>

            <div class="col-sm-2">
                <form action="<?php echo base_url('c-top-search-customer-order-info'); ?>" method="post">
                    <input type="text" placeholder="Search.." class="form-control" name="keyword" required>
                    <!--<button type="submit" style="margin-left: -5px; height: 20px;"><i class="simple-icon-search"></i></button>-->
                </form>
            </div>

            <?php
            $user_id = $this->session->userdata('user_id');
            $user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();

            if ($this->session->userdata('isAdmin') == 1) {
                $level_id = $this->session->userdata('user_id');
            } else {
                $level_id = $this->session->userdata('admin_created_by');
            }

            $company_profile = $this->db->select('*')
                            ->from('company_profile')
                            ->where('user_id', $level_id)
                            ->get()->result();


            $created_by = @$company_profile[0]->created_by;
            $company_logo = $this->db->select('*')
                            ->from('company_profile')
                            ->where('user_id', $created_by)
                            ->get()->result();
                            
            ?>
            <a class="navbar-logo" href="<?php echo base_url(); ?>c-level-dashboard">
                <img src="<?php echo base_url(); ?>assets/b_level/uploads/appsettings/<?php echo @$company_logo[0]->logo; ?>" alt=""  style="width: 80%; margin-top: -15px !important; ">
            </a>


            <div class="ml-auto">

                <div class="header-icons d-inline-block align-middle">

                    <div class="user d-inline-block">
                        <button class="header-icon btn btn-empty" type="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <!-- <span class="name">Language</span> -->
                            <i class="iconsmind-Flag-4"></i>
                        </button>

                        <div class="dropdown-menu dropdown-menu-right mt-4" style="margin-left: 25px; top:40px;">
                            <a href="#googtrans(en|en)" class="lang-en lang-select dropdown-item" data-lang="en"><img src="<?= base_url('assets/b_level/uploads/img/flag-usa.png') ?>" alt="USA" style="height: 16px; width: 24px; border-radius: 0px !important;" > ENG</a>
                            <a href="#googtrans(ko|ko)" class="lang-es lang-select dropdown-item" data-lang="ko"><img src="<?= base_url('assets/b_level/uploads/img/korea.png') ?>" alt="Korea" style="height: 16px; width: 24px; border-radius: 0px !important;" > Korea</a>
                        </div>
                    </div>

                    <?php
                    $date = date('Y-m-d');
                    $user_id = $this->session->userdata('user_id');
                    $this->db->select("c_notification_tbl.*,CONCAT(user_info.first_name, '.', user_info.last_name) as fullname, user_info.user_image");
                    $this->db->join('user_info', 'user_info.id=c_notification_tbl.created_by', 'left');
                    $this->db->where('c_notification_tbl.date', $date);
                    $this->db->where('c_notification_tbl.created_by', $user_id);
                    $this->db->order_by('id','DESC');
                    $result = $this->db->get('c_notification_tbl')->result();

//                    ============ its for customer comment notification =============
                    $this->db->select('a.*, b.first_name, b.last_name');
                    $this->db->from('customer_commet_tbl a');
                    $this->db->join('customer_info b', 'b.customer_user_id = a.comment_to');
                    $this->db->where('a.is_visited', 0);
                    $this->db->where('a.comment_to', $this->session->userdata('user_id'));
//                    $this->db->group_by('a.comment_to');
                    $customer_commet_query = $this->db->get()->result();

                    $this->db->select('count(a.id) total_id');
                    $this->db->from('customer_commet_tbl a');
                    $this->db->where('a.is_visited', 0);
                    $this->db->where('a.comment_to', $this->session->userdata('user_id'));
                    $customer_commet_count = $this->db->get()->result();

                    $this->db->select('*');
                    $this->db->from('appointment_calendar');
                    $this->db->where('appointment_date', date('Y-m-d'));
                    $this->db->where('c_level_staff_id', $this->session->userdata('user_id'));
                    $staff_noti = $this->db->get()->result();

                    if ($this->session->userdata('isAdmin') == 1) {
                        $level_id = $this->session->userdata('user_id');
                    } else {
                        $level_id = $this->session->userdata('admin_created_by');
                    }
                    $user_id = $this->session->userdata('user_id');
                    ?>



                    <div class="position-relative d-inline-block">


                        <?php if ($user_id == $level_id) { ?>

                            <button class="header-icon btn btn-empty" type="button" id="notificationButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <i class="simple-icon-bell"></i>
                                <span class="n-count" id=""><?= count($result) + $customer_commet_count[0]->total_id; ?></span>

                            </button>
                            
                            <?php if (!empty($result) || !empty($customer_commet_query)) { ?>

                                <div class="dropdown-menu dropdown-menu-right position-absolute" id="notificationDropdown" style="height: auto; padding: 0; right: 0; margin-top: 9px;">
                                    <div class="scroll" style="height: 280px; padding: 1.5rem; margin-right: 0;">

                                        <?php
                                            foreach ($result as $key => $val) {
                                        ?>
                                            <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                                <!-- <a href="#">
                                                    <img src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-2.jpg" alt="Notification Image" class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                                </a> -->
                                                <div class="pl-3 pr-2">
                                                    <a href="<?= base_url() . $val->go_to_url ?>">
                                                        <p class="font-weight-medium mb-1"><?= $val->notification_text; ?></p>
                                                        <p class="text-muted mb-0 text-small"><?= $val->date; ?></p>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php
                                            }
                                        ?>

                                        <?php
                                            foreach ($customer_commet_query as $key => $val1) {

                                        ?>
                                            <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                               
                                                <div class="pl-3 pr-2">
                                                    <a href="<?php echo base_url(); ?>customer-comment-view/<?php echo @$val1->comment_to ?>">
                                                        <p class="font-weight-medium mb-1"><?= $val1->comments; ?></p>
                                                        <p class="text-muted mb-0 text-small"><?= $val1->created_at; ?></p>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php
                                            }
                                        ?>

                                    </div>

                                    <div class="text-center" style="padding: 6px 1.5rem; border-top: 1px solid rgba(33,33,33,.15);">
                                        <a href="<?= base_url('c_level/notification/notification_list') ?>">View More</a></div>
                                </div>

                            <?php } else { ?>

                                <div class="dropdown-menu dropdown-menu-right position-absolute" id="notificationDropdown" style="height: auto; padding: 0; right: 0; margin-top: 9px;">
                                    <div class="scroll" style="height: 280px; padding: 1.5rem; margin-right: 0;">
                                        <div class="text-center" style="padding: 6px 1.5rem; ">
                                            No Notification
                                        </div>
                                    </div>

                                    <div class="text-center" style="padding: 6px 1.5rem; border-top: 1px solid rgba(33,33,33,.15);">
                                        <a href="<?= base_url('c_level/notification/notification_list') ?>">View More</a></div>
                                </div>
                            <?php } ?>    



                        <?php } else { ?>

                            <button class="header-icon btn btn-empty" type="button" id="notificationButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <i class="simple-icon-bell"></i>
                                <span class="n-count" id=""><?= count($staff_noti) + $customer_commet_count[0]->total_id; ?></span>
                            </button>


                            <?php if (!empty($staff_noti)) { ?>

                                <div class="dropdown-menu dropdown-menu-right position-absolute" id="notificationDropdown" style="height: auto; padding: 0; right: 0; margin-top: 9px;">

                                    <div class="scroll" style="height: 280px; padding: 1.5rem; margin-right: 0;">

                                        <?php
                                        foreach ($staff_noti as $key => $val) {
                                            ?>
                                            <div class="d-flex flex-row mb-3 pb-3 border-bottom">

                                                <div class="pl-3 pr-2">
                                                    <a href="#">
                                                        <p class="font-weight-medium mb-1"><?= $val->remarks; ?></p>
                                                        <p class="text-muted mb-0 text-small"><?= $val->appointment_date; ?></p>
                                                    </a>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>


                                    <div class="text-center" style="padding: 6px 1.5rem; border-top: 1px solid rgba(33,33,33,.15);">
                                        <a href="<?= base_url('c_level/appointment_controller/staff_appointment') ?>">View More</a></div> 
                                </div>


                            <?php } else { ?>

                                <div class="dropdown-menu dropdown-menu-right position-absolute" id="notificationDropdown" style="height: auto; padding: 0; right: 0; margin-top: 9px;">
                                    <div class="scroll" style="height: 280px; padding: 1.5rem; margin-right: 0;">
                                        <div class="text-center" style="padding: 6px 1.5rem; ">
                                            No Notification
                                        </div>
                                    </div>

                                    <div class="text-center" style="padding: 6px 1.5rem; border-top: 1px solid rgba(33,33,33,.15);">
                                        <a href="<?= base_url('c_level/appointment_controller/staff_appointment') ?>">View More</a></div> 
                                </div>

                            <?php } ?>


                        <?php } ?>



                        <?php if ($customer_commet_query) { ?>

                            <div class="dropdown-menu dropdown-menu-right mt-3 scroll position-absolute" id="notificationDropdown">
                                <?php
                                foreach ($customer_commet_query as $key => $val) {
                                    ?>
                                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                                        <!--                                        <a href="#">
                                                                                    <img src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l-2.jpg" alt="Notification Image" class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                                                                </a>-->
                                        <div class="pl-3 pr-2">
                                            <a href="<?php echo base_url(); ?>customer-comment-view/<?php echo $val->comment_to ?>">
                                                <p class="font-weight-medium mb-1"><?= $val->first_name . " " . $val->last_name; ?></p>
                                                <p class="text-muted mb-0 text-small"><?= $val->created_at; ?></p>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                        <?php } ?>
                    </div>

                    <button class="header-icon btn btn-empty d-none d-sm-inline-block" type="button" id="fullScreenButton">
                        <i class="simple-icon-size-fullscreen"></i>
                        <i class="simple-icon-size-actual"></i>
                    </button>

                    <?php
                    $allsystem_menu = $this->db->select('*')->from('c_menusetup_tbl')->where('menu_type', 2)
                                    ->where('parent_menu', 44)->where('status', 1)->where('level_id', $level_id)->order_by('ordering', 'asc')->get()->result();
//                    echo $this->db->last_query();
                    $menu_title = '';
                    $i = 0;
                    foreach ($allsystem_menu as $system_menu) {
                        $i++;
                        if ($i == 1) {
                            ?>
                            <div class="position-relative d-none d-sm-inline-block">
                                <button class="header-icon btn btn-empty" type="button" id="iconMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <i class="<?php echo $system_menu->icon; ?>"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right mt-3 gridtop_popup  position-absolute" id="iconMenuDropdown">
                                    <?php
                                    $parent_id = $system_menu->menu_id;
                                    $sub_menus = $this->db->select('*')->from('c_menusetup_tbl')->where('parent_menu', $parent_id)
                                                    ->where('status', 1)->where('level_id', $level_id)->order_by('ordering', 'asc')->get()->result();
//                                    echo $this->db->last_query();
                                    foreach ($sub_menus as $submenu) {
                                        if ($user_info[0]->language == 'English') {
                                            $menu_title = $submenu->menu_title;
                                        } elseif ($user_info[0]->language == 'Korean') {
                                            $menu_title = $submenu->korean_name;
                                        } else {
                                            $menu_title = $submenu->menu_title;
                                        }
                                        if ($this->permission->check_label($submenu->menu_title)->access()) {
                                            ?>
                                            <a href="<?php echo base_url(); ?><?php echo $submenu->page_url; ?>" class="icon-menu-item">
                                                <i class="<?php echo $submenu->icon; ?>"></i>
                                                <span><?php echo str_replace("_", " ", ucwords($menu_title)); ?></span>
                                            </a>
                                            <?php
                                        }
                                    }
                                    ?>
        <!--                                    <a href="<?php echo base_url(); ?>customer-list" class="icon-menu-item">
        <i class="iconsmind-MaleFemale d-block"></i>
        <span>Customers</span>
        </a>
        <a href="<?php echo base_url(); ?>manage-order" class="icon-menu-item">
        <i class="iconsmind-Address-Book2 d-block"></i>
        <span>Orders</span>
        </a>
        <a href="<?php echo base_url(); ?>appointment-form" class="icon-menu-item">
        <i class="iconsmind-Clock d-block"></i>
        <span>Appointment</span>
        </a>
        <a href="<?php echo base_url(); ?>account-chart" class="icon-menu-item">
        <i class="iconsmind-Pencil d-block"></i>
        <span>Accounts</span>
        </a>
        <a href="<?php echo base_url(); ?>payment-setting" class="icon-menu-item">
        <i class="iconsmind-Settings-Window d-block"></i>
        <span>Payment Settings</span>
        </a>
        <a href="<?php echo base_url(); ?>faq" class="icon-menu-item">
        <i class="iconsmind-Suitcase d-block"></i>
        <span>Knowledge Base</span>
        </a>-->
                                </div>
                            </div>
                            <?php
                        }
                        if ($i == 2) {
                            ?>
                            <div class="user d-inline-block">
                                <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="name"><?php echo $this->session->userdata('name'); ?></span>
                                    <span>
                                        <?php
                                        $my_account = $this->db->select('*')->from('user_info')->where('id', $this->session->userdata('user_id'))
                                                        ->get()->result();
                                        if (@$my_account[0]->user_image) {
                                            ?>
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/uploads/appsettings/<?php echo $my_account[0]->user_image; ?>" />
                                        <?php } else { ?>
                                            <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/c_level/img/profile-pic-l.jpg" />
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right mt-4 top_popup">
                                    <?php
                                    $parent_id = $system_menu->menu_id;
                                    $sub_menus = $this->db->select('*')->from('c_menusetup_tbl')->where('parent_menu', $parent_id)
                                                    ->where('status', 1)->where('level_id', $level_id)->order_by('ordering', 'asc')->get()->result();
                                    foreach ($sub_menus as $submenu) {
                                        if ($user_info[0]->language == 'English') {
                                            $menu_title = $submenu->menu_title;
                                        } elseif ($user_info[0]->language == 'Korean') {
                                            $menu_title = $submenu->korean_name;
                                        } else {
                                            $menu_title = $submenu->menu_title;
                                        }
//                                        if ($this->permission->check_label($submenu->menu_title)->access()) {
                                        ?>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?><?php echo $submenu->page_url; ?>">
                                            <i class="<?php echo $submenu->icon; ?>"></i><?php echo str_replace("_", " ", ucwords($menu_title)); ?>
                                        </a>
                                    <?php } ?>
                                    <a class="dropdown-item" href="<?php echo base_url('c_level/auth_controller/c_level_logout'); ?>"><i class="glyph-icon simple-icon-action-redo"></i>Sign out</a>
        <!--                                    <a class="dropdown-item" href="<?php echo base_url(); ?>c-my-account"><i class="glyph-icon simple-icon-user"></i>My Account</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>c-password-change"><i class="glyph-icon simple-icon-user"></i>Change Password</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>my-orders"><i class="glyph-icon simple-icon-puzzle"></i>My Orders</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>logs"><i class="glyph-icon simple-icon-refresh"></i>Logs</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>c_level/auth_controller/c_level_logout"><i class="glyph-icon simple-icon-action-redo"></i>Sign out</a>-->
                                </div>
                            </div>
                            <?php
//                            }
                        }
                    }
                    ?>

                </div>


            </div>
        </nav>









        <script type="text/javascript">


            $(function () {
                setInterval(function () {

                    $.post("<?php echo base_url(); ?>c_level/Customer_controller/customer_comment_notification_count", function (data) {
                        $("#notification_count").html(data);
                    });
                    //                    $.post("<?php echo base_url(); ?>c_level/Customer_controller/customer_comment_notification_results", function (s) {
                    //                        $("#notificationDropdown").html(s);
                    //                    });
                }, 5000);
            });
        </script>

