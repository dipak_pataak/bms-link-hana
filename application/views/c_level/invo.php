<html>
    <!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Order Recover</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <style type="text/css">
            body{
                width: 80%;
                margin: 0 auto;
            }
            .welcome_header{
                background: #96588A;
                height: 80px;
            }
            .welcome_header h3{
                text-align: center;
                color: #fff;
                padding: 25px 0px;
                font-size: 25px;
            }
            .welcome_description{
                min-height: 100px;
                text-align: justify;
                margin: 20px 0px;
            }
            .welcome_description p{
                font-size: 16px;
            }
            .order_date{

            }
            .order_no{
                float: right;
            }
            .order_info{

            }
            table {
                border-collapse: collapse;
            }

            table, td, th {
                border: 1px solid black;
            }
            .payment{
                margin-bottom: 270px;
            }
            .payment_info h4, .payment_status h4{
                background: #DDDDDD;
                height: 30px;
                padding: 8px 0 0 15px;
                width: 78%;
            }
            .order_info table{
                width: 90%;
            }
            .payment_info table,.payment_status table{
                width: 80%;
            }
            .order_info thead, .payment_info thead,.payment_status thead{
                background: #DDDDDD;
            }
            .order_info tr,.payment_info tr, .payment_status tr{
                height: 40px;
            }
            .order_info th{

            }
        </style>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <div class="welcome_header">
                    <h3 class="text-center">Thank you for your order</h3>
                </div>
            </div>


            <div class="row" style="width: 100%; float: left; margin-top: 50px;" >

                <div class="form-group col-md-6" style="float: left;">
                    <div class="">
                        <img src="<?php echo base_url('assets/c_level/uploads/appsettings/').$company_profile->logo;?>">
                    </div>
                    <p><?=$company_profile->company_name;?></p>
                    <p><?=$company_profile->address;?></p>
                    <p><?=$company_profile->city;?>, 
                    <?=$company_profile->state;?>, <?=$company_profile->zip_code;?>, 
                    <?=$company_profile->country_code;?></p>
                    <p><?=$company_profile->phone;?></p>
                    <p><?=$company_profile->email;?></p>
                </div>

                <div class="form-group col-md-5" style="margin-left: 20px; float: right;">

                    <table class="table table-bordered mb-4">

                        <tr class="text-center">
                            <td>Order Date</td>
                            <td class="text-right"><?=date_format(date_create($orderd->order_date),'M-d-Y');?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Order Id</td>
                            <td class="text-right"><?=$orderd->order_id?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Customer Name</td>
                            <td ><?=$orderd->customer_name?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Customer Address</td>
                            <td ><?=$orderd->address?></td>
                        </tr>
                    </table>

                    <p>Please click the url, Pay for this order!</p>
                    <a href="<?php echo base_url(); ?>c_level/customer_payment/payment?order_id=<?=$orderd->order_id;?>&customer_id=<?=$customer_info->customer_id?>" >Click Here</a>


                </div>
            </div>


            <div class="row" style="width: 100%; float: left;">
        
                <h5>Order Details</h5>
                    <table class="table table-bordered mb-4">

                        <thead>
                            <tr>
                                <th>Item.</th>
                                <th>Qty</th>
                                <th width="400px;">Discription</th>
                                <th>List</th>
                                <th>Discount(%)</th>
                                <th>Price</th>
                                <th>Notes</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php $i = 1; ?>
                            <?php 
                            foreach ($order_details as $items): 
                                $width_fraction = $this->db->where('id',$items->width_fraction_id)->get('width_height_fractions')->row();
                                $height_fraction = $this->db->where('id',$items->height_fraction_id)->get('width_height_fractions')->row();
                               
                            ?>

                                <tr>

                                    <td><?=$i?></td>
                                    <td><?=$items->product_qty;?></td>
                                    <td >
                                    <strong><?=$items->product_name;?></strong><br/> 
                                    <?php 
                                    if($items->pattern_name){
                                        echo $items->pattern_name.'<br/>';
                                    }
                                    ?>
                                    W <?=$items->width;?> <?=@$width_fraction->fraction_value?>, 
                                    H <?=$items->height;?> <?=@$height_fraction->fraction_value?>, 
                                    <?=$items->color_number;?> 
                                    <?=$items->color_name;?>
                                    <?php
                                        $attributess = (json_decode($items->product_attribute));
                                
                                    ?>
                                    </td>
                                    
                                    <td><?=$company_profile->currency;?><?=number_format($items->list_price,2)?></td>
                                    <td><?=($items->discount)?></td>
                                    <td><?=$company_profile->currency;?> <?=@number_format($items->unit_total_price,2)?> </td>
                                    <td><?=$items->notes?></td>
                                    
                                </tr>

                                <?php $i++; ?>

                            <?php endforeach; ?>
                                
                        </tbody>
                    </table>

            </div>

            <br/>


            <div class="row" style="width: 100%; float: left; margin-top: 20px;">

                <table class="datatable2 table table-bordered mb-4" >

                    <thead>
                        <tr>
                           
                            <th>Sales Tax</th>
                            <th>Sub-Total</th>
                            <th>Installation Charge</th>
                            <th>Other Charge</th>
                            <th>Misc</th>
                            <th>Discount</th>
                            <th>Grand Total</th>
                            <th>Deposit</th>
                            <th>Due </th>
                        </tr>
                    </thead>

                    <tbody>
                        
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->state_tax,2)?></td>
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->subtotal,2)?> </td>
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->installation_charge,2)?> </td>
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->other_charge,2)?></td>
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->misc,2)?> </td>
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->invoice_discount,2)?></td>
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->grand_total,2)?> 
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->paid_amount,2)?> 
                        <td class="text-right"> <?=$company_profile->currency;?> <?=number_format($orderd->due,2)?> 
                    </tbody>
                </table>

            </div>


            <div class="row" style="float: left; width: 100%; margin-top: 50px;">
                <div class="">

                    <p>
                        Regards,<br>

                        <a href="https://www.bdtask.com/">
                            BDtask
                        </a><br>
                        Supports<br>
                        Mobile: +88-01817-584639<br>
                    </p>
                    
                </div>
            </div>

            <div class="row" style="float: left; width: 100%;">
                <div class="welcome_header"></div>
            </div>

        </div>
    </body>

</html>
